﻿using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Xml.Linq;

namespace FlightDeck.Services.IntegrationTests
{
    /// <summary>
    /// This test is pretty slow, it creates a db, imports all the stats and lots of schemes, performs calcs on all the schemes, then wipes the db and does it again!
    /// This test was written to help replace manual testing of this scenario, it wasn't written with the intention of being included in regular automated tests, though
    /// in theory there's no reason it couldn't be encorporated if required with a few tweaks.
    /// </summary>
    [TestFixture]
    public class IncrementalCalculationsTests
    {
        string testDataDir;
        string statsdir;
        string statsxmlname;
        string schemeXmlDir;
        bool keepstatscopies = false;
        string outputdir;
        IPrincipal principal;

        [SetUp]
        public void SetUp()
        {
            setupPrincipal();
            createDb();
            testDataDir = ConfigurationManager.AppSettings["testDataDir"];
            statsdir = Path.Combine(testDataDir, "stats");
            schemeXmlDir = Path.Combine(testDataDir, "schemes");
            statsxmlname = Path.GetFileName(Directory.GetFiles(statsdir, "*.xml").First());
            if (keepstatscopies)
            {
                prepareOutputDir();
            }
        }

        [TestCase(
            "The Space01 Test Scheme",
            "The ABC Pension Scheme (for v1.6 with LDI)",
            "The ABC Unfunded Pension Scheme (for v1.6)",
            "Corps of Commissionaires Pension Scheme Updated (v1.6 test)",
            "House of Fraser Pension Scheme - BJRB",
            "Johnston Press Pension Plan",
            "The Admenta Pension Scheme",
            "The London Stock Exchange Retirement Plan updated",
            "The Fujitsu Telecommunications Pension Plan (v1.6 test)")]
        public void Incremental_stats_upload_for_cached_schemes(params string [] schemes)
        {        
            var stats = getStatsFile();

            /*
             *first pass through use a full stats file and get all scheme's evolution results
             */

            var IndexImportDetailService = new IndexImportDetailService(true);

            IndexImportDetailService.ImportIndices(stats);

            var fullStatsResults = getImportSchemesAndGetTheirEvolutionResults(schemes, IndexImportDetailService.FinancialIndexRepository);

            /*
             *second pass through, create a new db to wipe out the stats, upload the stats incrementally adding schemes between increments to check end results are unaffected
             */

            //wipe db
            createDb();

            //get stats file in bits
            var splits = splitStatsFile(new List<DateTime> { new DateTime(2015, 11, 1), new DateTime(2015, 12, 1), new DateTime(2015, 12, 31), new DateTime(2016, 1, 1), new DateTime(2016, 1, 2), DateTime.MaxValue });

            IndexImportDetailService = new IndexImportDetailService(true);

            //upload the first stats file
            IndexImportDetailService.ImportIndices(splits[splits.Keys.Min(d => d)]);

            //run through all the schemes again
            foreach (var schemeName in schemes)
            {
                try
                {
                    var pensionSchemeSvc = new PensionSchemeService(principal, IndexImportDetailService.FinancialIndexRepository);

                    //get the scheme's xml file
                    var schemeXml = getSchemeXml(schemeName);

                    //import the scheme
                    pensionSchemeSvc.Save(null, new SchemeDetail { SchemeName = schemeName }, null, principal.Identity.Name, -1, schemeXml);

                    //get an instance of the newly created scheme
                    var scheme =
                        pensionSchemeSvc.GetSchemeWithDetails(
                            pensionSchemeSvc.GetSummaryByName(schemeName)
                                .SchemeDetailId);

                    //access their evolution calcs so their cache is created, don't save results yet as only interested in results after final stats upload
                    foreach (var basis in scheme.PensionScheme.GetAllBases())
                    {
                        var e = basis.Evolution;
                        var ed = basis.ExpectedDeficit;
                    }
                }
                catch (Exception ex)
                {
                    Assert.Fail($"Exception initialising scheme '{schemeName}' during second pass: {ex.ToString()}");
                }
            }

            //now upload the remaining stats and access the scheme results each time to update the cache
            foreach (var date in splits.Keys.OrderBy(d => d).Skip(1))
            {
                IndexImportDetailService = new IndexImportDetailService(true);

                IndexImportDetailService.ImportIndices(splits[date]);

                var pensionSchemeSvc = new PensionSchemeService(principal, IndexImportDetailService.FinancialIndexRepository);

                foreach (var schemeName in schemes)
                {
                    var scheme = pensionSchemeSvc.GetSchemeWithDetails(pensionSchemeSvc.GetSummaryByName(schemeName).SchemeDetailId);
                    foreach (var basis in scheme.PensionScheme.GetAllBases())
                    {
                        var e = basis.Evolution;
                        var ed = basis.ExpectedDeficit;
                    }
                }
            }

            /*
             *final stage, check that the current basis evolution results equal those from the first pass
             */

            IndexImportDetailService = new IndexImportDetailService(true);

            foreach (var expectedSchemeResult in fullStatsResults)
            { 
                var pensionSchemeSvc = new PensionSchemeService(principal, IndexImportDetailService.FinancialIndexRepository);

                var actualSchemeResult = pensionSchemeSvc.GetSchemeWithDetails(pensionSchemeSvc.GetSummaryByName(expectedSchemeResult.Name).SchemeDetailId);

                foreach (var expectedBasisResult in expectedSchemeResult.Bases)
                {
                    var actualBasisResult = actualSchemeResult.PensionScheme.GetBasis(expectedBasisResult.Id);

                    foreach(var date in expectedBasisResult.EvolutionData.Keys)
                    {
                        assertEvolutionDataItem(expectedSchemeResult.Name, expectedBasisResult.Id, date, expectedBasisResult.EvolutionData[date], actualBasisResult.Evolution[date]);         
                        assertExpectedDeficitDataItem(expectedSchemeResult.Name, expectedBasisResult.Id, date, expectedBasisResult.ExpectedDeficitData[date], actualBasisResult.ExpectedDeficit[date]);
                    }
                }
            }
        }

        [TestCase("The Space01 Test Scheme")]
        public void Incremental_stats_upload_change_historical_value_for_cached_scheme(string schemeName)
        {
            var stats = getStatsFile();

            // Generate a stats file that has amended historical index values, amended dates that are before the schemes effective dates (to ensure effectives dates are crossed when rebuilding calculation cache)
            var amendedStats = copyAndAmendStatsFile(stats, new List<DateTime> { new DateTime(2013, 10, 1) });

            // Import amended stats in to a clean database so we can get the expected results set

            var IndexImportDetailService = new IndexImportDetailService(true);

            IndexImportDetailService.ImportIndices(amendedStats);

            var expectedResults = getImportSchemesAndGetTheirEvolutionResults(new List<string> { schemeName }, IndexImportDetailService.FinancialIndexRepository);

            // Wipe the database and import a different stats file (ie, our usual stats)

            createDb();

            IndexImportDetailService = new IndexImportDetailService(true);

            IndexImportDetailService.ImportIndices(stats);

            // Get the evolution results so the scheme calcs are cached

            getImportSchemesAndGetTheirEvolutionResults(new List<string> { schemeName }, IndexImportDetailService.FinancialIndexRepository);

            // Now import the amended stats

            IndexImportDetailService.ImportIndices(amendedStats);
            
            // Mimic the call that occurs in the IndicesController to check the cache for now incorrect values
            new PreCalculationService(new DbInteractionScopeFactory(), IndexImportDetailService.FinancialIndexRepository, new Serializer()).ClearExistingToEarliestUpdate(IndexImportDetailService.IndexEarliestUpdateDate);

            // Get the evolution results which should now have been updated to match our very first results set
            var actualResult = new schemeResult();
            var pensionSchemeSvc = new PensionSchemeService(principal, IndexImportDetailService.FinancialIndexRepository);
            var scheme = pensionSchemeSvc.GetSchemeWithDetails(pensionSchemeSvc.GetSummaryByName(schemeName).SchemeDetailId);
            actualResult.Name = scheme.SchemeName;
            foreach (var basis in scheme.PensionScheme.GetAllBases())
            {
                actualResult.Bases.Add(new basisResult { Id = basis.MasterBasisId, EvolutionData = basis.Evolution, ExpectedDeficitData = basis.ExpectedDeficit });
            }

            // Check results match
            foreach(var schemeResult in expectedResults)
            {
                foreach(var basisResult in schemeResult.Bases)
                {
                    var actualBasisResult = actualResult.Bases.Single(b => b.Id == basisResult.Id);

                    foreach(var date in basisResult.EvolutionData.Keys)
                    {
                        assertEvolutionDataItem(schemeResult.Name, basisResult.Id, date, basisResult.EvolutionData[date], actualBasisResult.EvolutionData[date]);
                        assertExpectedDeficitDataItem(schemeResult.Name, basisResult.Id, date, basisResult.ExpectedDeficitData[date], actualBasisResult.ExpectedDeficitData[date]);
                    }
                }
            }
        }

        #region privates

        void setupPrincipal()
        {
            principal = Mock.Of<IPrincipal>(prin =>
                prin.Identity == Mock.Of<IIdentity>(id =>
                    id.Name == "testrunner@service.integrationtests" && id.IsAuthenticated == true));
        }

        void prepareOutputDir()
        {
            outputdir = Path.Combine(statsdir, "output");
            if (!Directory.Exists(outputdir))
                Directory.CreateDirectory(outputdir);
            else
                foreach (var file in Directory.GetFiles(outputdir, "*.xml"))
                    File.Move(file, Path.Combine(outputdir, Path.GetFileNameWithoutExtension(file) + "_" + DateTime.Now.Ticks.ToString() + ".xml"));
        }

        class schemeResult
        {
            public string Name;
            public List<basisResult> Bases = new List<basisResult>();
        }
        class basisResult
        {
            public int Id;
            public IDictionary<DateTime, EvolutionData> EvolutionData;
            public IDictionary<DateTime, ExpectedDeficitData> ExpectedDeficitData;
        }

        List<schemeResult> getImportSchemesAndGetTheirEvolutionResults(IEnumerable<string> schemes, Lazy<IFinancialIndexRepository> indices)
        {
            var schemeResults = new List<schemeResult>();

            foreach (var schemeName in schemes)
            {
                var schemeResult = new schemeResult { Name = schemeName };

                var pensionSchemeSvc = new PensionSchemeService(principal, indices);

                //get the scheme's xml file
                var schemeXml = getSchemeXml(schemeName);

                //import the scheme
                var importResult = pensionSchemeSvc.Save(null, new SchemeDetail { SchemeName = schemeName }, null, principal.Identity.Name, -1, schemeXml);

                Assert.IsTrue(importResult.Success, $"Error importing scheme: {schemeName}");

                //get an instance of the newly created scheme
                var scheme =
                    pensionSchemeSvc.GetSchemeWithDetails(
                        pensionSchemeSvc.GetSummaryByName(schemeName)
                            .SchemeDetailId);

                //run through all the bases on the scheme and save the evolution results
                foreach (var basis in scheme.PensionScheme.GetAllBases())
                {
                    schemeResult.Bases.Add(new basisResult
                    {
                        Id = basis.MasterBasisId,
                        EvolutionData = basis.Evolution,
                        ExpectedDeficitData = basis.ExpectedDeficit
                    });                    
                }
                schemeResults.Add(schemeResult);
            }

            return schemeResults;
        }

        IDictionary<DateTime, XDocument> splitStatsFile(IList<DateTime> cutoffs)
        {
            var source = getStatsFile();

            var dates = cutoffs;

            var copies = dates.ToDictionary(x => x, x => new XDocument());

            foreach (var date in dates)
            {
                var from = dates.Any(d => d < date) ? dates.Where(d => d < date).OrderByDescending(d => d).First() : DateTime.MinValue;
                var till = date;

                var Data =
                    new XElement("Data",
                        new XElement("AuditTrail",
                            source.Root.Element("AuditTrail").Descendants()));

                foreach (var index in source.Root.Elements("Index"))
                {
                    var dateValues =
                        index.Descendants().Where(el => el.Name == "Value" &&
                            (DateTime.Parse(el.Attributes("Date").Single().Value) >= from && DateTime.Parse(el.Attributes("Date").Single().Value) < till));

                    if (dateValues.Any())
                    {
                        var descendants = index.Descendants().Where(el => el.Name == "IndexName").Union(dateValues);

                        Data.Add(new XElement("Index",
                            index.Attributes(),
                            descendants));
                    }
                }

                copies[date].Add(Data);
            }

            if (keepstatscopies)
                foreach (var date in copies.Keys)
                    copies[date].Save(Path.Combine(outputdir, date.AddDays(-1).ToString("yyyy-MM-dd") + ".xml"));

            return copies;
        }

        XDocument copyAndAmendStatsFile(XDocument original, IEnumerable<DateTime> adjustOnTheseDays)
        {
            var copy = new XDocument(original);

            var indexValuesOnDates = 
                copy.Root
                    .Elements("Index").Where(x => !string.Equals("LEVEL INDEX", x.Element("IndexName").Value))
                        .Elements("Value")
                            .Where(el => adjustOnTheseDays.Contains(DateTime.Parse(el.Attributes("Date").Single().Value)));

            foreach (var valueEl in indexValuesOnDates)
            {
                valueEl.Value = (double.Parse(valueEl.Value) + 0.5).ToString();
            }

            return copy;
        }

        void createDb()
        {
            var versionManager = new Space01.Haymaker.Versioning.VersionManager();
            versionManager.DisableVersionTableCreation = true;
            if (!versionManager.Rebuild())
                throw new Exception("createDb failed");

            Services.Configuration.Bootstrapper.Init();
        }

        XDocument getStatsFile()
        {
            return XDocument.Load(Path.Combine(statsdir, statsxmlname));
        }

        XDocument getSchemeXml(string schemeName)
        {
            return XDocument.Load(Path.Combine(schemeXmlDir, schemeName + ".xml"));
        }

        void assertEvolutionDataItem(string schemeName, int basisKey, DateTime date, EvolutionData expected, EvolutionData actual)
        {
            Func<string, string> detail = key => $"Scheme: {schemeName} basisKey: {basisKey} date: {date.ToString("dd/MM/yyyy")} key: [{key}]";

            Assert.AreEqual(expected.ActDuration, actual.ActDuration, detail("ActDuration"));
            Assert.AreEqual(expected.Active, actual.Active, detail("Active]"));
            Assert.AreEqual(expected.CPI, actual.CPI, detail("CPI]"));
            Assert.AreEqual(expected.DefDuration, actual.DefDuration, detail("DefDuration]"));
            Assert.AreEqual(expected.Deferred, actual.Deferred, detail("Deferred]"));
            Assert.AreEqual(expected.DeferredBuyIn, actual.DeferredBuyIn, detail("DeferredBuyIn]"));
            Assert.AreEqual(expected.EmployeeContributions, actual.EmployeeContributions, detail("EmployeeContributions]"));
            Assert.AreEqual(expected.EmployerContributions, actual.EmployerContributions, detail("EmployerContributions]"));
            Assert.AreEqual(expected.FutDuration, actual.FutDuration, detail("FutDuration]"));
            Assert.AreEqual(expected.Future, actual.Future, detail("Future]"));
            Assert.AreEqual(expected.Growth, actual.Growth, detail("Growth]"));
            Assert.AreEqual(expected.NetBenefits, actual.NetBenefits, detail("NetBenefits]"));
            Assert.AreEqual(expected.NetIncome, actual.NetIncome, detail("NetIncome]"));
            Assert.AreEqual(expected.PenDuration, actual.PenDuration, detail("PenDuration]"));
            Assert.AreEqual(expected.Pensioner, actual.Pensioner, detail("Pensioner]"));
            Assert.AreEqual(expected.PensionerBuyIn, actual.PensionerBuyIn, detail("PensionerBuyIn]"));
            Assert.AreEqual(expected.PensionerDiscountRate, actual.PensionerDiscountRate, detail("PensionerDiscountRate]"));
            CollectionAssert.AreEquivalent(expected.PensionIncreases.Select(x => new { x.Key, x.Value }), actual.PensionIncreases.Select(x => new { x.Key, x.Value }), detail("PensionIncreases]"));
            Assert.AreEqual(expected.PostRetirementDiscountRate, actual.PostRetirementDiscountRate, detail("PostRetirementDiscountRate]"));
            Assert.AreEqual(expected.PreRetirementDiscountRate, actual.PreRetirementDiscountRate, detail("PreRetirementDiscountRate]"));
            Assert.AreEqual(expected.RPI, actual.RPI, detail("RPI]"));
            Assert.AreEqual(expected.SalaryIncrease, actual.SalaryIncrease, detail("alaryIncrease]"));
            Assert.AreEqual(expected.TotalAssets, actual.TotalAssets, detail("TotalAssets]"));
            Assert.AreEqual(expected.TotalAssetsPlusBuyin, actual.TotalAssetsPlusBuyin, detail("TotalAssetsPlusBuyin]"));
            Assert.AreEqual(expected.TotalBenefits, actual.TotalBenefits, detail("TotalBenefits]"));
            Assert.AreEqual(expected.TotalExistingBuyin, actual.TotalExistingBuyin, detail("TotalExistingBuyin]"));
            Assert.AreEqual(expected.TotalLiability, actual.TotalLiability, detail("TotalLiability]"));
        }

        void assertExpectedDeficitDataItem(string schemeName, int basisKey, DateTime date, ExpectedDeficitData expected, ExpectedDeficitData actual)
        {
            Func<string, string> detail = key => $"Scheme: {schemeName} basisKey: {basisKey} date: {date.ToString("dd/MM/yyyy")} key: [{key}]";
            
            Assert.AreEqual(expected.Assets, actual.Assets, detail("ed.Assets"));
            Assert.AreEqual(expected.Liabilities, actual.Liabilities, detail("ed.Liabilities"));
            Assert.AreEqual(expected.Deficit, actual.Deficit, detail("ed.Deficit"));
        }

        #endregion
    }
}
