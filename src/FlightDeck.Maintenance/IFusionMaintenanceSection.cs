﻿namespace FlightDeck.Maintenance
{
    public interface IFusionMaintenanceSection
    {
        string SqlCmdPath { get; }
        string DbName { get; }
        string Server { get; }
        string ScriptsPath { get; }
        string AlwaysRunPath { get; }
        string UserName { get; }
        string Password { get; }
    }
}
