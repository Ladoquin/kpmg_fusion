﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using log4net;

namespace FlightDeck.Maintenance
{
    public class DbVersionManager
    {
        private readonly string scriptLocation;
        private readonly string alwaysRunLocation;
        
        private readonly ScriptRunner cmd;
        private readonly ILog log;

        private Version currentVersion;
        private Version latestVersion;

        public DbVersionManager()
            : this((FusionMaintenanceSection)ConfigurationManager.GetSection("FusionMaintenanceSection"))
        {
        }

        public DbVersionManager(IFusionMaintenanceSection config)
        {
            log = LogManager.GetLogger(GetType());
            var path = Assembly.GetExecutingAssembly().Location;
            scriptLocation = Path.Combine(Path.GetDirectoryName(path), config.ScriptsPath);
            alwaysRunLocation = Path.Combine(Path.GetDirectoryName(path), config.AlwaysRunPath);
            cmd = new ScriptRunner(config);
        }

        private IList<string> GetScripts(Version version)
        {
            var d = Directory.GetDirectories(Path.GetFullPath(scriptLocation));
            var versionFolders = d.Where(x => 
                String.Compare(Path.GetFileName(x), currentVersion.ToString(), StringComparison.Ordinal) > 0
                && String.Compare(Path.GetFileName(x), version.ToString(), StringComparison.Ordinal) <= 0);
            var files = new List<string>();
            foreach (var versionFolder in versionFolders)
            {
                files.AddRange(Directory.GetFiles(versionFolder));
            }
            return files;
        }

        private Version GetLatest()
        {
            return GetNext(true);
        }

        private Version GetNext(bool latest = false)
        {
            var d = Directory.GetDirectories(Path.GetFullPath(scriptLocation)).Select(Path.GetFileName);
            var versionFolders = d.Where(x => String.Compare(x, currentVersion.ToString(), StringComparison.Ordinal) > 0).ToList();
            if (versionFolders.Count == 0)
                return currentVersion;
            var version = latest ? versionFolders.Last() : versionFolders[0];
            var v = version.Split('_');
            return new Version(int.Parse(v[0]), int.Parse(v[1]));
        }

        private Version GetCurrent()
        {
            var dbVersion = cmd.RunQuery("select * from [version]");
            if (string.IsNullOrWhiteSpace(dbVersion))
            {
                log.WarnFormat("Unable to get current db version.");
                return new Version(0, 0);
            }
            log.Debug(dbVersion);
            var v = ParseVersion(dbVersion);
            log.Debug(v);
            return v;
        }

        public bool Initialize()
        {
            currentVersion = GetCurrent();
            latestVersion = GetLatest();
            log.InfoFormat("Current database version is {0}.", currentVersion);
            var isCurrent = Equals(currentVersion, latestVersion);
            if(!isCurrent)
                log.InfoFormat("Latest available database version is {0}.", latestVersion);
            else
                log.Info("Database is up to date.");
            return isCurrent;
        }

        private static Version ParseVersion(string dbVersion)
        {
            var v = dbVersion.Trim().Split(',');
            return new Version(int.Parse(v[0].Trim()), int.Parse(v[1].Trim()));
        }

        public bool Update()
        {
            return Update(latestVersion);
        }

        public bool Update(Version version)
        {
            while (!Equals(currentVersion, version))
            {
                var v = GetNext();
                log.InfoFormat("Upgrading to version {0}...", v);

                var files = GetScripts(v);
                if (files.Count == 0)
                {
                    log.ErrorFormat("Version {0} contains no script files.", v);
                    return false;
                }
                if (cmd.RunScript(files) == ScriptRunner.ResultType.Error)
                    return false;
                SetDbVersion(v);
                currentVersion = v;
                

                if (v.Equals(version))
                    return true;
            }
            return false;
        }

        public void RunAlwaysRun()
        {
            var files = new List<string>();
            files.AddRange(Directory.GetFiles(Path.GetFullPath(alwaysRunLocation)));

            if (files.Count() > 0)
                log.Info("Running Always Run scripts");

            if (cmd.RunScript(files) == ScriptRunner.ResultType.Error)
                return;

            foreach (var file in files)
            {
                log.InfoFormat("Ran always run script: {0}", file.Substring(file.LastIndexOf('\\') + 1));
            }
        }

        private void SetDbVersion(Version version)
        {
            RunScript(String.Format("UPDATE [Version] SET Major = {0}, Minor = {1}", version.Major, version.Minor));
            log.InfoFormat("Successfully upgraded database version {0}.", version);
        }

        private void DropDatabase()
        {
            log.Warn("Dropping all the tables...");
            RunScript(@"WHILE EXISTS(SELECT * FROM sys.tables where is_ms_shipped = 0) 
                        EXEC sp_MSforeachtable 'DROP TABLE ?'");
            log.Warn("Dropped all the tables.");
        }

        internal void RunScript(string script)
        {
            cmd.RunQuery(script);
        }

        internal string RunQuery(string query)
        {
            return cmd.RunQuery(query);
        }

        public void Rebuild()
        {
            DropDatabase();
            Initialize();
            Update();
        }
    }
}