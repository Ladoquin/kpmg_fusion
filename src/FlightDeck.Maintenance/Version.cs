﻿using System;

namespace FlightDeck.Maintenance
{
    public class Version : IEquatable<Version>
    {
        public int Major { get; private set; }
        public int Minor { get; private set; }

        public Version(int major, int minor)
        {
            Minor = minor;
            Major = major;
        }

        public bool Equals(Version other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Major == other.Major && Minor == other.Minor;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Version) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Major*397) ^ Minor;
            }
        }

        public override string ToString()
        {
            return string.Format("{0:0000}_{1:0000}", Major, Minor);
        }
    }
}