﻿using System.Configuration;

namespace FlightDeck.Maintenance
{
    public class FusionMaintenanceSection : ConfigurationSection, IFusionMaintenanceSection
    {
        [ConfigurationProperty("sqlCmdPath", IsRequired = true)]
        public string SqlCmdPath
        {
            get
            {
                return (string)this["sqlCmdPath"];
            }
            set
            {
                this["sqlCmdPath"] = value;
            }
        }

        [ConfigurationProperty("dbName", IsRequired = true)]
        public string DbName
        {
            get
            {
                return (string)this["dbName"];
            }
            set
            {
                this["dbName"] = value;
            }
        }

        [ConfigurationProperty("server", IsRequired = true)]
        public string Server
        {
            get
            {
                return (string)this["server"];
            }
            set
            {
                this["server"] = value;
            }
        }

        [ConfigurationProperty("scriptsPath", IsRequired = true)]
        public string ScriptsPath
        {
            get
            {
                return (string)this["scriptsPath"];
            }
            set
            {
                this["scriptsPath"] = value;
            }
        }

        [ConfigurationProperty("alwaysRunPath", IsRequired = true)]
        public string AlwaysRunPath
        {
            get
            {
                return (string)this["alwaysRunPath"];
            }
            set
            {
                this["alwaysRunPath"] = value;
            }
        }

        [ConfigurationProperty("userName", IsRequired = false)]
        public string UserName
        {
            get
            {
                return (string)this["userName"];
            }
            set
            {
                this["userName"] = value;
            }
        }

        [ConfigurationProperty("password", IsRequired = false)]
        public string Password
        {
            get
            {
                return (string)this["password"];
            }
            set
            {
                this["password"] = value;
            }
        }
    }
}