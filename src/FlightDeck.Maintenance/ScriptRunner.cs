﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using log4net;

namespace FlightDeck.Maintenance
{
    public class ScriptRunner
    {
        private readonly string sqlCmdPath;
        private readonly string dbName;
        private readonly string server;
        private readonly string commandArguments;
        private readonly ILog log;

        public enum ResultType { Success, Warning, Error }

        public ScriptRunner()
            : this((FusionMaintenanceSection)ConfigurationManager.GetSection("FusionMaintenanceSection"))
        {
        }

        public ScriptRunner(IFusionMaintenanceSection config)
        {
            log = LogManager.GetLogger(GetType());

            sqlCmdPath = config.SqlCmdPath;
            dbName = config.DbName;
            server = config.Server;
            var authentication = string.IsNullOrWhiteSpace(config.UserName) ? "-E"
                : string.Format("-U {0} -P {1}", config.UserName, config.Password);

            commandArguments = string.Format("-b -r -h -1 -s , -S {0} -d {1} {2}", server, dbName, authentication);
        }

        public ResultType RunScript(IEnumerable<string> files)
        {
            List<string> quoteEnclosedFileNames = new List<string>();
            foreach (var fileName in files)
            {
                quoteEnclosedFileNames.Add(string.Format("{0}{1}{0}", "\"", fileName ));
            }

            return RunScript(quoteEnclosedFileNames.ToArray());
        }

        public ResultType RunScript(params string[] files)
        {
            const string fileSep = " -i ";
            var file = fileSep + string.Join(fileSep, files);

            var startInfo = new ProcessStartInfo
            {
                FileName = sqlCmdPath,
                Arguments = commandArguments + file,
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };
            var p = new Process {StartInfo = startInfo};
            p.Start();
            var stdOutput = p.StandardOutput.ReadToEnd();
            var error = p.StandardError.ReadToEnd();
            p.WaitForExit();
            var exitCode = p.ExitCode == 0;
            p.Close();

            var result = ResultType.Success;
            if (!string.IsNullOrEmpty(stdOutput) && stdOutput.StartsWith("***")) //Bit lame, to get your warnings reported throw them with a *** at the start of the string. This distinguishes them from normal standard output.
            {
                result = ResultType.Warning;
                log.Warn(stdOutput);
            }
            else if (!exitCode)
            {
                result = ResultType.Error;
                log.Error(error);
            }

            return result;
        }

        public string RunQuery(string query)
        {
            var startInfo = new ProcessStartInfo
            {
                FileName = sqlCmdPath,
                Arguments = commandArguments + " -Q \"SET NOCOUNT ON; "+ query+ ";\"",
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                
            };
            var p = new Process { StartInfo = startInfo };
            var output = new StringBuilder();
            var error = new StringBuilder();
            p.OutputDataReceived += (x, e) => output.Append(e.Data);
            p.ErrorDataReceived += (x, e) => { error.Append(e.Data); error.Append("\n"); };
            p.EnableRaisingEvents = true;
            p.Start();
            p.BeginOutputReadLine();
            p.BeginErrorReadLine();
            //var error = p.StandardError.ReadToEnd();
            p.WaitForExit();

            var exitCode = p.ExitCode;
            p.Close();

            if (exitCode != 0)
                log.Error(error.ToString());
            return output.ToString();
        }
    }
}
