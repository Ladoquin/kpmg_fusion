﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Security.Principal;
    using System.Xml.Linq;

    public interface IIndexImportDetailService
    {
        void ImportIndices(XDocument doc);
        IndexImportDetail GetLatest();
        IndexImportDetail Save(XDocument doc, IPrincipal importedBy);
        void FillEndGap(int IndexImportDetailId);
        Dictionary<string, DateTime> IndexEarliestUpdateDate { get; }
    }
}
