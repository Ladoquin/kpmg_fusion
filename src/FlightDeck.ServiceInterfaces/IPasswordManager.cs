﻿namespace FlightDeck.ServiceInterfaces
{
    public interface IPasswordManager
    {
        bool IsPasswordValid(string username, string oldpassword, string password);
        bool ChangePassword(string username, string oldpassword, string password);
        bool ResetPassword(string username, string passwordResetToken, string password);
    }
}
