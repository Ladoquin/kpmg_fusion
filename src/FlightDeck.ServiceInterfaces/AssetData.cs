﻿using FlightDeck.DomainShared;

namespace FlightDeck.ServiceInterfaces
{
    public class AssetData
    {
        public string ClassName { get; private set;}
        public int ClassId { get; private set; }
        public AssetCategory Category { get; private set; }
        public double Amount { get; private set; }
        public double Proportion { get; private set; }
        public AssetReturnSpec ReturnSpec { get; private set; }
        public double ActualReturn { get; private set; }
        public double DefaultReturn { get; private set; }

        public AssetData(int classId, string className, AssetCategory category, double amount, double proportion, double actualReturn, double defaultReturn, AssetReturnSpec returnSpec)
        {
            ActualReturn = actualReturn;
            ClassId = classId;
            ClassName = className;
            Category = category;
            Amount = amount;
            Proportion = proportion;
            ReturnSpec = returnSpec;
            DefaultReturn = defaultReturn;
        }
    }
}