﻿using FlightDeck.DomainShared;
using System.Collections.Generic;
using System.Xml.Linq;

namespace FlightDeck.ServiceInterfaces
{
    public interface IPensionSchemeService
    {
        IScheme GetActive();
        IScheme GetSummary(int id);
        IScheme GetSummaryByName(string name);
        IScheme GetSchemeWithDetails(int id);
        XDocument GetSchemeSource(int id);
        IEnumerable<IScheme> GetAllSummaries();
        DetailedResult<ISchemeAdminServiceResult, IScheme> Save(string schemeNameKey, SchemeDetail scheme, IEnumerable<SchemeDocument> documents, string updatedBy, int createdByUserId, XDocument schemeDataXml = null);
        IEnumerable<IScheme> GetImportHistory(string schemeName);
        void Delete(string schemeName);
        void DeleteById(int id);
        void DeleteDocument(int schemeId, int documentId);
        void ValidateResults(string schemeNameKey, SchemeDetail scheme, XDocument schemeDataXml = null);
        string GetActiveSchemeName();
    }
}
