﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.ServiceInterfaces
{
    public enum IUserProfileServiceResult : byte 
    { 
        SchemeNotFound,
        UsernameConflict
    };

    public enum ISchemeAdminServiceResult : byte
    {
        NameConflict,
        NameMismatch,
        ReferenceConflict,
        XMLError,
        LogoFileInvalid,
        XMLNameMissing,
        XMLReferenceMissing,
        XMLDateMissing,
        XMLDateInvalid,
        XMLUsernameMissing,
        XMLSpreadsheetPathMissing,
        XMLEffectiveDateMissing,
        XMLEffectiveDateInvalid,
        XMLDataCaptureVersionMissing

    }

    public enum IGlobalParametersServiceResult : byte
    {
        None,
        Success,
        XMLStructureInvalid,
        UnrecognisedAssetClassType,
        ValueTypeInvalid
    }
}
