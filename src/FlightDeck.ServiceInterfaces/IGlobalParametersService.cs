﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using System.Xml.Linq;

    public interface IGlobalParametersService
    {
        IEnumerable<ParametersImportHistory> GetImportHistory();
        IDictionary<AssetClassType, double> GetVolatilities();
        IDictionary<AssetClassType, IDictionary<AssetClassType, double>> GetVarCorrelations();
        ActionResult<IGlobalParametersServiceResult> ImportParameters(XDocument xdoc, int userId);
        GetResult<IGlobalParametersServiceResult, IDictionary<AssetClassType, double>> GetVolatilitiesFromXDoc(XElement root);
        GetResult<IGlobalParametersServiceResult, IDictionary<AssetClassType, IDictionary<AssetClassType, double>>> GetVarCorrelationsFromXDoc(XElement root);
    }
}
