﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public class JourneyPlanResult
    {
        public BeforeAfter<IList<LensData>> BeforePlan { get; set; }
        public BeforeAfter<IList<LensData>> AfterPlan { get; set; }
    }
}
