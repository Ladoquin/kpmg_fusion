﻿namespace FlightDeck.ServiceInterfaces
{
    using System;
    
    public class IndexDataSummary
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        public IndexDataSummary(int id, string name, DateTime start, DateTime end)
        {
            Id = id;
            Name = name;
            Start = start;
            End = end;
        }
    }
}
