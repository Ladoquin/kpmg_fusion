﻿namespace FlightDeck.ServiceInterfaces
{
    /// <summary>
    /// Interface to get the necessary functionality from the weblayer's password library to the service layer.
    /// </summary>
    public interface IAccountSecurityService
    {
        bool IsAuthenticated { get; }
        string CurrentUserName { get; }
        void Logout();
        bool UserExists(string username);
        int GetPasswordFailuresSinceLastSuccess(string username);
        bool Login(string username, string password);
        bool ResetPassword(string passwordResetToken, string newPassword);
        bool ChangePassword(string username, string oldPassword, string newPassword);
        string GeneratePasswordResetToken(string username);
    }
}
