﻿namespace FlightDeck.ServiceInterfaces
{
    using System;
    using System.Collections.Generic;
    
    public interface IEmailService
    {
        void SendEmail(string to, string toDisplay, string subject, string html, string plain, string fromDisplayName = "KPMG Fusion");
        void SendEmail(IEnumerable<Tuple<string, string>> recipients, string subject, string html, string plain, string fromDisplayName = "KPMG Fusion");
        void SendEmailAsync(string to, string toDisplay, string subject, string html, string plain, string fromDisplayName = "KPMG Fusion");
        void SendEmailAsync(IEnumerable<Tuple<string, string>> recipients, string subject, string html, string plain, string fromDisplayName = "KPMG Fusion");
    }
}
