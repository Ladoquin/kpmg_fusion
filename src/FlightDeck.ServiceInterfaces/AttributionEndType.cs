﻿namespace FlightDeck.ServiceInterfaces
{
    public enum AttributionEndType
    {
        Baseline,
        RefreshDate
    }
}
