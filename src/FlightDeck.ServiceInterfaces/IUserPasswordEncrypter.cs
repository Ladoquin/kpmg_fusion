﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;

    public interface IUserPasswordEncrypter
    {
        PasswordData GetEncrypted(string password);
        PasswordData GetEncrypted(string password, byte[] salt);
    }
}
