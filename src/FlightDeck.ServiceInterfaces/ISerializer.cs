﻿namespace FlightDeck.ServiceInterfaces
{
    public interface ISerializer
    {
        string SerializeObject<T>(T objectToSerialize);
        T DeserializeObject<T>(string str);
    }
}
