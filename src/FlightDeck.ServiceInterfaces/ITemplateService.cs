﻿namespace FlightDeck.ServiceInterfaces
{
    using System.Collections.Generic;
    
    public interface ITemplateService
    {
        string GenerateContent(string templatePath, Dictionary<string, string> tokenValues);
    }
}
