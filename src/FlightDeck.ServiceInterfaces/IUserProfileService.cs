﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public interface IUserProfileService
    {
        UserProfile GetById(int id);
        UserSearchResult Get(UserProfileSearchCriteria criteria, int pageIdx, int pageSize);
        UserProfile GetByUsername(string username, bool allowAnonymous = false);
        UserProfile GetByEmail(string email, bool allowAnonymous = false);
        Dictionary<string, int> GetSearchIndex();
        Dictionary<string, int> GetSearchIndex(IEnumerable<int> excludeIds);
        void DeleteUser(string username);
        void DeleteUsers(IEnumerable<int> ids);
        void DeleteSchemeAccess(int userId, int schemeId);
        ActionResult<IUserProfileServiceResult> ChangeActiveScheme(string username, string schemeName);
        ActionResult<IUserProfileServiceResult> Create(string username, RoleType role, IEnumerable<string> schemeNames = null, string firstname = null, string lastname = null, string email = null);
        ActionResult<IUserProfileServiceResult> Update(string username, RoleType? role = null, IEnumerable<string> schemeNames = null, string firstname = null, string lastname = null, string email = null, string updatedusername = null);
        bool AddFavouriteScheme(UserProfile user, int schemeDetailId);
        bool RemoveFavouriteScheme(UserProfile user, int schemeDetailId);
    }
}
