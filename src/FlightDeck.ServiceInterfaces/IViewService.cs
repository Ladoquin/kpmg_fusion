﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    public interface IViewService
    {
        SchemeSummaryView GetSummary(IPensionScheme scheme, DateTime date);
        SchemeAssetData GetSchemeAssetData(IPensionScheme scheme, AssumptionsSourceType assumptionsSource);
        SchemeAssetData GetSchemeAssetDataAtAnchor(IPensionScheme scheme);
        SchemeAssetData GetSchemeAssetDataAtRefreshDate(IPensionScheme scheme);
        IEnumerable<BreakdownItem> GetLiabilitiesSplit(IPensionScheme scheme, DateTime date);
        IEnumerable<BreakdownItem> GetLiabilitiesCustomSplit(IPensionScheme scheme);
        IEnumerable<BreakdownItem> GetAssetsBreakdown(IPensionScheme scheme, AssumptionsSourceType assumptionsSource);  
        IEnumerable<BreakdownItem> GetAssetsBreakdownAt(IPensionScheme scheme, DateTime at);
        IEnumerable<AssetFundGrowthAndValue> GetSchemeAssetDataWithGrowth(IPensionScheme scheme);
        IDictionary<EvolutionElapsedPeriodType, EvolutionDataItem> GetEvolution(IPensionScheme scheme, DateTime date);
        IEnumerable<KeyLabelValue<string, double>> GetLiabilityAssumptions(IPensionScheme scheme, DateTime date, IDictionary<string, string> defaultLabels);
        IEnumerable<PensionIncreaseDefinition> GetPensionIncreases(IPensionScheme scheme, DateTime date);
        IEnumerable<PensionIncreaseDefinition> GetPensionIncreasesAtAnchor(IPensionScheme scheme);
        IDictionary<PensionIncreaseDefinition, BeforeAfter<double>> GetPensionIncreasesCustom(IPensionScheme scheme);
        IEnumerable<KeyLabelValue<string, BeforeAfter<double>>> GetLiabilityAssumptionsCustom(IPensionScheme scheme, IDictionary<string, string> defaultLabels);
        IEnumerable<AssetReturnItem> GetAssetReturnsCustom(IPensionScheme scheme);
        double GetWeightedAssetReturn(IPensionScheme scheme);
        EvolutionSchemeData GetEvolutionSchemeData(IPensionScheme scheme);
        DateTime? GetBuyinStatus(IPensionScheme scheme);
    }
}
