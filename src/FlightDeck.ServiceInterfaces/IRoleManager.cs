﻿using FlightDeck.DomainShared;

namespace FlightDeck.ServiceInterfaces
{
    public interface IRoleManager
    {
        RoleType GetRole(string username);
        void Delete(string username);
        void AddUserToRole(string username, string roleName);
    }
}
