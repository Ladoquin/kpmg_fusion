﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    using System.Security.Principal;
    using System.Xml.Linq;
    
    public interface ICurveImportDetailService
    {
        void ImportCurves(XDocument doc);
        CurveImportDetail GetLatest();
        CurveImportDetail Save(XDocument doc, IPrincipal importedBy);
    }
}
