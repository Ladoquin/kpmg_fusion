﻿using FlightDeck.DomainShared;
using System.Collections.Generic;

namespace FlightDeck.ServiceInterfaces
{
    public interface IApplicationSettings
    {
        ImportantMessage GetImportantMessage();
        void SaveImportantMessage(string message);
        string GetConsultantDefaultSchemeName();
        double GetPensionIncreasesMaxValue();
        bool IsSchemeSupported(string dataCaptureVersion);
        string GetTrackerVersionSupported();
    }
}
