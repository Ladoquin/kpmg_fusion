﻿namespace FlightDeck.ServiceInterfaces
{
    using System.Collections.Generic;
    
    public class PropertyTreeItem
    {
        public string Key { get; private set; }
        public IDictionary<string, object> Properties { get; set; }
        public List<PropertyTreeItem> Children { get; set; }

        public PropertyTreeItem(string key, IDictionary<string, object> properties)
            : this(key, properties, null)
        {
        }
        public PropertyTreeItem(string key, IDictionary<string, object> properties, List<PropertyTreeItem> children)
        {
            Key = key;
            Properties = properties;
            Children = children ?? new List<PropertyTreeItem>();
        }
    }
}
