﻿namespace FlightDeck.ServiceInterfaces
{
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Linq;

    public interface IXmlSchemaValidator
    {
        List<string> Messages { get; }
        bool Validate(Stream xml, Stream schema);
    }
}
