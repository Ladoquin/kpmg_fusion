﻿namespace FlightDeck.ServiceInterfaces.Reports
{
    using FlightDeck.DomainShared;
    using System;
    using System.IO;
    
    public interface IAccountingReporter
    {
        MemoryStream Create(IScheme scheme, AccountingStandardType type, DateTime start, DateTime end, AccountingReportOutputType outputType = AccountingReportOutputType.Default);
    }
}
