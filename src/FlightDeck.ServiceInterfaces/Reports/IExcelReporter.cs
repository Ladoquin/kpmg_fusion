﻿namespace FlightDeck.ServiceInterfaces.Reports
{
    using FlightDeck.DomainShared;
    using System;
    using System.IO;

    public interface IExcelReporter
    {
        MemoryStream Create(IScheme scheme, DateTime start, DateTime end, bool useClientAssumptions, AccountingReportOutputType outputType = AccountingReportOutputType.Default);
    }
}
