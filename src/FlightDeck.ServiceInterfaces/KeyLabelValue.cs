﻿namespace FlightDeck.ServiceInterfaces
{
    public class KeyLabelValue<TKey, TValue>
    {
        public TKey Key { get; set; }
        public string Label { get; set; }
        public TValue Value { get; set; }

        public KeyLabelValue(TKey key, string label, TValue value)
        {
            Key = key;
            Label = label;
            Value = value;
        }
    }
}
