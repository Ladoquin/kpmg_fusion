﻿using System.Collections.Generic;

namespace FlightDeck.ServiceInterfaces
{
    public class BreakdownItem : KeyLabelValue<int, double>
    {
        public IEnumerable<BreakdownItem> SubItems { get; set; }

        public BreakdownItem()
            : this(string.Empty, 0)
        {
        }
        public BreakdownItem(string label, double value)
            : this(0, label, value)
        {
        }
        public BreakdownItem(int key, string label, double value)
            : base(key, label, value)
        {
            SubItems = new List<BreakdownItem>();
        }
    }
}
