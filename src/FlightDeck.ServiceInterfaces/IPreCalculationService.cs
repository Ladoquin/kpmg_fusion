﻿using System;
using System.Collections.Generic;

namespace FlightDeck.ServiceInterfaces
{
    public interface IPreCalculationService
    {
        void ClearExisting();
        void ClearExistingToEarliestUpdate(IDictionary<string, DateTime> indexEarliestUpdateDate);
        void Execute();
    }
}
