﻿namespace FlightDeck.ServiceInterfaces
{
    using System.Collections.Generic;
    using System.Collections.Specialized;
    
    public interface IIndicesService
    {
        IEnumerable<IndexDataSummary> GetAllSummaries();
        IOrderedDictionary GetIndexValues(int id);
    }
}
