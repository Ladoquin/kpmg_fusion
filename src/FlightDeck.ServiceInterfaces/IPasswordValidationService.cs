﻿namespace FlightDeck.ServiceInterfaces
{
    public interface IPasswordValidationService
    {
        bool IsValid(string username, string oldpassword, string password);
    }
}
