﻿namespace FlightDeck.ServiceInterfaces
{
    using System;

    public class EvolutionDataItem
    {
        public ValueDifference Asset { get; set; }
        public ValueDifference Liability { get; set; }
        public ValueDifference Balance { get; set; }
        public DateTime Date { get; set; }
    }
}
