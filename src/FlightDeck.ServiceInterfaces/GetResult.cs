﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.ServiceInterfaces
{
    public class GetResult<TStatus, TGet> : ActionResult<TStatus> where TStatus : struct
    {
        public TGet Get { get; set; }

        public GetResult(bool success)
            : this(default(TGet), success, new List<TStatus>())
        {
        }
        public GetResult(TGet get)
            : this(get, true, new List<TStatus>())
        {
        }
        public GetResult(TStatus status)
            : this(default(TGet), false, new List<TStatus> { status })
        {
        }
        public GetResult(TGet get, IEnumerable<TStatus> statuses)
            : this(get, statuses == null || !statuses.Any(), statuses)
        {
        }
        public GetResult(TGet get, bool success, IEnumerable<TStatus> statuses)
            : base (success, statuses)
        {
            Get = get;
        }
        public GetResult(Exception ex)
            : base(ex)
        {
        }
    }
}
