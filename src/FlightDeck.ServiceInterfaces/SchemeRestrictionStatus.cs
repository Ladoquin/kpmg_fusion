﻿namespace FlightDeck.ServiceInterfaces
{
    public enum SchemeRestrictionStatus
    {
        All,
        Restricted, 
        NotRestricted
    }
}
