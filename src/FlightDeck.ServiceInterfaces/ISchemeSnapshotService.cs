﻿namespace FlightDeck.ServiceInterfaces
{
    using System.IO;
    
    public interface ISchemeSnapshotService
    {
        MemoryStream GetSnapshot(IPensionScheme scheme);
    }
}
