﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.ServiceInterfaces
{
    public class ActionResult<T> where T : struct
    {
        public Exception Exception { get; private set; }
        public bool Success { get; private set; }
        public List<T> Statuses { get; private set; }
        public bool Has(T status)
        {
            return Statuses.Any(x => EqualityComparer<T>.Default.Equals(x, status));
        }

        public ActionResult(bool success)
            : this(success, new List<T>())
        {
        }
        public ActionResult(T status)
            : this(false, status)
        {
        }
        public ActionResult(IEnumerable<T> statuses)
            : this(statuses == null || !statuses.Any(), statuses)
        {
        }
        public ActionResult(bool success, T status)
            : this(success, new List<T> { status })
        {
        }
        public ActionResult(Exception ex)
            : this(ex == null, new List<T>())
        {
        }
        public ActionResult(IEnumerable<T> statuses, Exception ex)
            : this(statuses == null || !statuses.Any() && ex == null, statuses)
        {
            Exception = ex;
        }
        public ActionResult(bool success, IEnumerable<T> statuses)
        {
            Success = success;
            Statuses = statuses.ToList();
        }
    }
}
