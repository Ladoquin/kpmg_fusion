﻿namespace FlightDeck.ServiceInterfaces
{
    public struct ValueDifference
    {
        public double Value { get; set; }
        public double Difference { get; set; }
    }
}
