﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.ServiceInterfaces
{
    public interface IScheme
    {
        int SchemeDetailId { get; set; }
        IPensionScheme PensionScheme { get; set; }
        IList<SchemeDocument> Documents { get; set; }
        string SchemeName { get; set; }
        string CurrencySymbol { get; set; }
        string WelcomeMessage { get; set; }
        byte[] Logo { get; set; }
        string ContactName { get; set; }
        string ContactNumber { get; set; }
        string ContactEmail { get; set; }
        string SchemeData_SchemeRef { get; set; }
        DateTime? SchemeData_AnchorDate { get; set; }
        int? SchemeData_Id { get; set; }
        DateTime? SchemeData_ImportedOnDate { get; set; }
        string SchemeData_ImportedByUser { get; set; }
        DateTime? SchemeData_ProducedOnDate { get; set; }
        string SchemeData_ProducedByUser { get; set; }
        string SchemeData_Spreadsheet { get; set; }
        string SchemeData_Comment { get; set; }
        string SchemeData_ExportFile { get; set; }
        DateTime UpdatedOnDate { get; set; }
        string UpdatedByUser { get; set; }
        int? CreatedByUserId { get; set; }
        bool HasVolatilities { get; set; }
        bool HasVarCorrelations { get; set; }
        bool IsRestricted { get; set; }
        string AccountingDownloadsPassword { get; set; }
        string DataCaptureVersion { get; set; }
    }
}
