﻿namespace FlightDeck.ServiceInterfaces
{
    public class SchemeSummaryView
    {
        public double Balance { get; set; }
        public double FundingTarget { get; set; }
    }
}
