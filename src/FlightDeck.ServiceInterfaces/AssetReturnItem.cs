﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    
    public class AssetReturnItem : KeyLabelValue<int, BeforeAfter<double>>
    {
        public double UserMin { get; set; }
        public double UserMax { get; set; }
        public double UserIncrement { get; set; }

        public AssetReturnItem(int key, string label, BeforeAfter<double> value)
            : base(key, label, value)
        {
        }
    }
}
