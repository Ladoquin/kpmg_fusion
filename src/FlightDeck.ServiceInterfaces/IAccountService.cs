﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.ServiceInterfaces
{
    public interface IAccountService
    {
        UserProfile AcceptTerms(string username);
        UserProfile SetPasswordFlag(string username, bool passwordMustChange);
        UserProfile Unlock(string username);
    }
}
