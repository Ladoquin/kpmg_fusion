﻿namespace FlightDeck.ServiceInterfaces
{
    public class PensionIncreaseDefinition : KeyLabelValue<int, double>
    {
        public bool IsFixed { get; set; }
        public bool IsVisible { get; set; }

        public PensionIncreaseDefinition(int key, string label, double value, bool isFixed, bool isVisible)
            : base(key, label, value)
        {
            IsFixed = isFixed;
            IsVisible = isVisible;
        }
    }
}
