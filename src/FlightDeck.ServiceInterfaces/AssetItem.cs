﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    
    public class AssetItem
    {
        public AssetClassType Type { get; set; }
        public AssetCategory Category { get; set; }
        public double Value { get; set; }

        public AssetItem(AssetClassType type, AssetCategory category, double value)
        {
            Type = type;
            Category = category;
            Value = value;
        }
    }
}
