﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using System.Security.Principal;

    public interface IClientAssumptionService
    {
        int SaveCurrentOptions(string name, IScheme scheme, IPrincipal createdByUser);
        void RestoreOptionsToScheme(int id, IScheme scheme);
        void DeleteSavedOptions(int id, IScheme scheme);
        IEnumerable<ClientAssumption> GetAll(int schemeDetailId);
    }
}
