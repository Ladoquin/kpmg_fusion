﻿namespace FlightDeck.ServiceInterfaces
{
    using System.IO;
    
    public interface ISnapshotWriter
    {
        MemoryStream WriteSnapshot(PropertyTreeItem snapshot);
    }
}
