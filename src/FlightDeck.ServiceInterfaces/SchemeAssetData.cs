﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public class SchemeAssetData
    {
        public IEnumerable<BreakdownItem> Funds { get; set; }
        public SyntheticAssetInfo SyntheticAssetInfo { get; set; }
    }
}
