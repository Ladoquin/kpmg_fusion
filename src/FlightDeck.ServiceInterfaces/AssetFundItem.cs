﻿namespace FlightDeck.ServiceInterfaces
{
    public class AssetFundItem
    {
        public string Name { get; private set; }
        public AssetItem Asset { get; private set; }
        public double Amount { get; private set; }

        public AssetFundItem(string name, AssetItem asset, double amount)
        {
            Name = name;
            Asset = asset;
            Amount = amount;
        }
    }
}
