﻿namespace FlightDeck.ServiceInterfaces
{
    public class EvolutionSchemeData
    {
        public double RegularEmployee { get; set; }
        public double RegularEmployer { get; set; }
        public double Deficit { get; set; }
        public double Total { get; set; }
        public double Payment { get; set; }
        public double NetCashflow { get; set; }
    }
}
