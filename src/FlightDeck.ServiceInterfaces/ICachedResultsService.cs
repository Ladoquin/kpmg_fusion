﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.ServiceInterfaces
{
    public interface ICachedResultsService
    {
        void ClearAllForScheme(string schemeName);
    }
}
