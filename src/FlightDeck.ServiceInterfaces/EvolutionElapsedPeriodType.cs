﻿namespace FlightDeck.ServiceInterfaces
{
    public enum EvolutionElapsedPeriodType
    {
        NoElapse,
        Day1,
        Month1,
        Month3,
        Month6,
        Year1
    }
}
