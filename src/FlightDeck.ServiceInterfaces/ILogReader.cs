﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    
    public interface ILogReader
    {
        IEnumerable<Log> GetAll(DateTime date, string level);
    }
}
