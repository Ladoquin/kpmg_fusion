﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;

    public class AccountingSettings
    {
        public AccountingStandardType DefaultStandard { get; private set; }
        public bool IAS19Visible { get; private set; }
        public bool FRS17Visible { get; private set; }
        public bool FRS102Visible { get; private set; }
        public bool USGAAPVisible { get; private set; }

        public AccountingSettings(AccountingStandardType defaultStandard, bool ias19Visible, bool frs17Visible, bool usgaapVisible)
        {
            DefaultStandard = defaultStandard;
            IAS19Visible = ias19Visible;
            FRS17Visible = frs17Visible;
            FRS102Visible = frs17Visible;
            USGAAPVisible = usgaapVisible;
        }
    }
}
