﻿using System;
using System.Collections.Generic;
using FlightDeck.DomainShared;

namespace FlightDeck.ServiceInterfaces
{
    public interface IPensionScheme
    {
        int Id { get; }
        string Name { get; }
        string Reference { get; }
        DateTime RefreshDate { get; }
        SchemeDataSource DataSource { get; }
        IBasis CurrentBasis { get; }
        IDictionary<AssetClassType, double> Volatilities { get; }
        IDictionary<AssetClassType, IDictionary<AssetClassType, double>> VarCorrelations { get; }
        IDictionary<NamedPropertyGroupType, NamedPropertyGroup> NamedProperties { get; }
        AnalysisParameters AnalysisParameters { get; }
        AccountingSettings AccountingSettings { get; }
        IDictionary<string, Tuple<IDictionary<StrainType, double>, DateTime>> GetStrains();
        IDictionary<AssetClassType, AssetClass> AssetDefinitions { get; }
        DateTime? ContributionsEndDate { get; }
        bool Funded { get; }
        
        BeforeAfter<RecoveryPlanType> GetClientRecoveryPlanOptions();
        BeforeAfter<FROType> GetClientFroType();
        BeforeAfter<FlexibleReturnOptions> GetClientFroOptions();
        BeforeAfter<EmbeddedFlexibleReturnOptions> GetClientEFroOptions();
        BeforeAfter<EnhancedTransferValueOptions> GetClientEtvOptions();
        BeforeAfter<PieOptions> GetClientPieOptions();
        BeforeAfter<InsuranceParameters> GetClientInsuranceParameters();
        BeforeAfter<IDictionary<AssetClassType, double>> GetClientInvestmentStrategyAllocations();
        BeforeAfter<InvestmentStrategyOptions> GetClientInvestmentStrategyOptions();
        BeforeAfter<FutureBenefitsParameters> GetClientFutureBenefitsParameters();
        BeforeAfter<AbfOptions> GetClientAbfOptions();
        BeforeAfter<JourneyPlanOptions> GetClientJourneyPlanOptions();
        BeforeAfter<WaterfallOptions> GetClientWaterfallOptions();

        void SetClientRecoveryPlanOptions(RecoveryPlanType val);
        void SetClientFroType(FROType val);
        void SetClientFroOptions(FlexibleReturnOptions val);
        void SetClientEFroOptions(EmbeddedFlexibleReturnOptions val);
        void SetClientEtvOptions(EnhancedTransferValueOptions val);
        void SetClientPieOptions(PieOptions val);
        void SetClientInsuranceParameters(InsuranceParameters val);
        void SetClientInvestmentStrategyAllocations(IDictionary<AssetClassType, double> val);
        void SetClientInvestmentStrategyOptions(InvestmentStrategyOptions val);
        void SetClientFutureBenefitsParameters(FutureBenefitsParameters val);
        void SetClientAbfOptions(AbfOptions val);
        void SetClientJourneyPlanOptions(JourneyPlanOptions val);
        void SetClientWaterfallOptions(WaterfallOptions val);

        void ResetClientRecoveryPlanOptions();
        void ResetClientFroType();
        void ResetClientFroOptions();
        void ResetClientEFroOptions();
        void ResetClientEtvOptions();
        void ResetClientPieOptions();
        void ResetClientInsuranceParameters();
        void ResetClientInvestmentStrategyAllocations();
        void ResetClientInvestmentStrategyOptions();
        void ResetClientFutureBenefitsParameters();
        void ResetClientAbfOptions();
        void ResetClientJourneyPlanOptions();

        bool SetAttributionPeriod(DateTime start, DateTime end);
        void ResetAttributionPeriod();

        IBasis SwitchBasis(int masterBasisId);
        IBasis GetDefaultBasis(BasisType type);
        IEnumerable<IBasis> GetAllBases();
        IEnumerable<MasterBasisKey> GetMasterbasisKeys();
        IBasis GetBasis(int masterBasisId);
        IEnumerable<BasisIdentifier> GetBasesIdentifiers();
        IEnumerable<BasisType> GetBasisTypes();
        IEnumerable<DateTime> AssetEffectiveDates { get; }
        SyntheticAssetInfo GetSyntheticAssetInfo();
        SyntheticAssetInfo GetSyntheticAssetInfo(DateTime at);
        IEnumerable<AssetFundItem> GetAssetData(DateTime at);
        MarketInfo GetMarketInfo();
        IEnumerable<AssetFundGrowth> GetAssetsGrowthInfo();
        NewJPData GetNewJP();
        void ResetAssumptions();
        BuyinCost BuyinCost { get; }

        bool HasBasisOfType(BasisType type);
        LDIHedgingData GetLdiHedgingData(DateTime at);
    }
}