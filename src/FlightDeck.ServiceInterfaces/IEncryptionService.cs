﻿namespace FlightDeck.ServiceInterfaces
{
    public interface IEncryptionService
    {
        string Encrypt(string clearText);
        string Decrypt(string clearText);
    }
}
