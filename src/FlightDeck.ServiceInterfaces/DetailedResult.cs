﻿using System;
using System.Collections.Generic;

namespace FlightDeck.ServiceInterfaces
{
    public class DetailedResult<TValidationType, TResultType>
    {
        public List<TValidationType> ValidationErrors { get; set; }
        public string DataImportProgress { get; set; }
        public Exception DataImportException { get; set; }
        public bool Success { get; set; }
        public TResultType Result { get; set; }
    }
}
