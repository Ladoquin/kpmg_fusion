﻿namespace FlightDeck.ServiceInterfaces
{
    using FlightDeck.DomainShared;
    
    public class AssetFundGrowthAndValue : AssetFundGrowth
    {
        public double Amount { get; set; }

        public AssetFundGrowthAndValue(string name)
            : base(name)
        {
        }

        public AssetFundGrowthAndValue(AssetCategory category, AssetClassType type, string name)
            : base(category, type, name)
        {
        }
    }
}
