﻿using System;
using System.Collections.Generic;
using FlightDeck.DomainShared;
using FlightDeck.DomainShared.VaR;

namespace FlightDeck.ServiceInterfaces
{
    public interface IBasis
    {
        DateTime AnchorDate { get; }
        DateTime AttributionEnd { get; }
        DateTime AttributionStart { get; }
        BasisType BasisType { get; }
        BondYieldData GetBondYield();
        void CoupleClientPensionIncreasesToLiabilityAssumptions();
        void DecoupleClientPensionIncreasesFromLiabilityAssumptions();
        IEnumerable<DateTime> EffectiveDates { get; }
        IDictionary<DateTime, EvolutionData> Evolution { get; }
        DateTime? EvolutionEndDate { get; set; }
        DateTime? EvolutionStartDate { get; set; }
        IDictionary<DateTime, ExpectedDeficitData> ExpectedDeficit { get; }
        BeforeAfter<AbfData> GetAbfData();
        IEnumerable<DateTime> GetAccountingPeriods(bool includeToAnalysisDate = true);
        IDictionary<AssumptionType, string> GetAssumptionLabels(DateTime date);
        BeforeAfter<BalanceData> GetBalanceSheetEstimate();
        BeforeAfter<BuyinData> GetBuyinInfo();
        BeforeAfter<double> GetInsurancePointChangeImpact();
        BeforeAfter<List<LiabilityData>> GetCashFlowData();
        List<LiabilityData> GetCashFlowData(AttributionEndType at);
        BeforeAfter<IDictionary<AssetClassType, double>> GetClientAssetAssumptions();
        BeforeAfter<AssumptionData> GetClientLiabilityAssumptions();
        BeforeAfter<IDictionary<int, double>> GetClientPensionIncreases();
        BeforeAfter<RecoveryPlanAssumptions> GetClientRecoveryAssumptions();
        BeforeAfter<EtvData> GetEtvData();
        BeforeAfter<FroData> GetFroData();
        BeforeAfter<EFroData> GetEFroData();
        BeforeAfter<FRS17Disclosure> GetFRS17Disclosure(DateTime start, DateTime end, bool useClientAssumptions);
        BeforeAfter<FundingData> GetFundingLevelData();
        BeforeAfter<FutureBenefitsData> GetFutureBenefits();
        double GetGiltYield();
        BeforeAfter<IAS19Disclosure> GetIAS19Disclosure(DateTime start, DateTime end, bool useClientAssumptions);
        BeforeAfter<InvestmentStrategyData> GetInvestmentStrategyData();
        BeforeAfter<HedgeBreakdownData> GetHedgingBreakdown();
        InvestmentStrategyData GetInvestmentStrategyData(AttributionEndType at);
        HedgeData GetHedgingDataAtRefreshDate();
        HedgeBreakdownData GetHedgingDataAtAnchorDate();
        JourneyPlanResult GetJourneyPlan();
        IEnumerable<PensionIncreaseDefinition> GetPensionIncreaseDefinitions(DateTime date);
        BeforeAfter<PieData> GetPieData();
        BeforeAfter<IList<double>> GetRecoveryPlan();
        BeforeAfter<AttributionData> GetAttributionData();
        BeforeAfter<SurplusAnalysisData> GetSurplusAnalysisData();
        BeforeAfter<USGAAPDisclosure> GetUSGAAPDisclosure(DateTime start, DateTime end, bool useClientAssumptions);
        BeforeAfter<IList<VarFunnelData>> GetVarFunnel();
        BeforeAfter<WaterfallData> GetVarWaterfall();
        VaRInputs GetVaRInputs();
        WeightedReturnParameters GetWeightedReturnParameters();
        int MasterBasisId { get; }
        string Name { get; }
        void ResetAssumptions();
        void ResetClientAssetAssumptions();
        void ResetClientLiabilityAssumptions();
        void ResetClientPensionIncreasesAssumptions();
        void ResetClientRecoveryAssumptions();
        void SetClientAssetAssumptions(IDictionary<AssetClassType, double> assumptions);
        void SetClientLiabilityAssumptions(AssumptionData assumptions);
        void SetClientPensionIncreases(IDictionary<int, double> assumptions);
        void SetClientRecoveryAssumptions(RecoveryPlanAssumptions assumptions);        
        LiabilityData GetLiabilityData(AttributionEndType at);
        IEnumerable<AssetItem> GetAssetData();
        IEnumerable<AssetItem> GetAssetData(DateTime at);
        IDictionary<SimpleAssumptionType, KeyValuePair<string, double>> GetSimpleAssumptions();
        IDictionary<SimpleAssumptionType, KeyValuePair<string, double>> GetSimpleAssumptions(DateTime at);
        BeforeAfter<IAS19Disclosure.IAS19PAndLForecastData> GetIASData();
        BeforeAfter<IAS19Disclosure.IAS19PAndLForecastData> GetFRSData();
        BeforeAfter<USGAAPDisclosure.NetPeriodicPensionCostForecastData> GetUSGAAPData();
        TotalCosts TotalCostsAtAnchor { get; }
    }
}