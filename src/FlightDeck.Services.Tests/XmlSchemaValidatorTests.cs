﻿namespace FlightDeck.Services.Tests
{
    using NUnit.Framework;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    [TestFixture]
    public class XmlSchemaValidatorTests
    {
        string Xsd =
            @"<xsd:schema xmlns:xsd='http://www.w3.org/2001/XMLSchema'>
                <xsd:element name='Data'>
                  <xsd:complexType>
                    <xsd:sequence>
                      <xsd:element name='Index' minOccurs='1' maxOccurs='unbounded'>
                        <xsd:complexType>
                          <xsd:sequence>
                            <xsd:element name='IndexName' minOccurs='1' maxOccurs='1' type='xsd:string' />
                            <xsd:element name='Value' minOccurs='0' maxOccurs='unbounded' type='xsd:decimal' />
                          </xsd:sequence>
                       </xsd:complexType>
                     </xsd:element>
                   </xsd:sequence>
                </xsd:complexType>
              </xsd:element>
            </xsd:schema>";

        [Test]
        public void TestVaidatorReturnsErrorsForXmlWithInvalidData()
        {
            var xml = new XDocument(
                new XElement("Data",
                    new XElement("Index",
                        new XElement("IndexName", "TEST INDEX RPI"),
                        new XElement("Value", "x"),
                        new XElement("Value", 1.02)),
                    new XElement("Index",
                        new XElement("IndexName", "TEST INDEX CPI"),
                        new XElement("Value", "y"),
                        new XElement("Value", "z"))));

            var validator = new XmlSchemaValidator();
            var schema = new MemoryStream(Encoding.ASCII.GetBytes(Xsd));

            var result = validator.Validate(XDocumentToStream(xml), schema);
            var messages = validator.Messages;

            Assert.IsFalse(result);
            Assert.IsNotNull(messages);
            Assert.AreEqual(3, messages.Count);
        }

        [Test]
        public void TestVaidatorReturnsSuccessForValidXml()
        {
            var xml = new XDocument(
                new XElement("Data",
                    new XElement("Index",
                        new XElement("IndexName", "TEST INDEX RPI"),
                        new XElement("Value", 1.01),
                        new XElement("Value", 1.02)),
                    new XElement("Index",
                        new XElement("IndexName", "TEST INDEX CPI"),
                        new XElement("Value", 1.01),
                        new XElement("Value", 1.02))));

            var validator = new XmlSchemaValidator();
            var schema = new MemoryStream(Encoding.ASCII.GetBytes(Xsd));

            var result = validator.Validate(XDocumentToStream(xml), schema);
            var messages = validator.Messages;

            Assert.IsTrue(result);
            Assert.IsNotNull(messages);
            Assert.AreEqual(0, messages.Count);
        }

        [Test]
        public void TestVaidatorReturnsOneErrorForXmlWithInvalidSchema()
        {
            var xml = new XDocument(
                new XElement("Data",
                    new XElement("Index",
                        new XElement("Value", 1.01),
                        new XElement("Value", 1.02))));

            var validator = new XmlSchemaValidator();
            var schema = new MemoryStream(Encoding.ASCII.GetBytes(Xsd));

            var result = validator.Validate(XDocumentToStream(xml), schema);
            var messages = validator.Messages;

            Assert.IsFalse(result);
            Assert.IsNotNull(messages);
            Assert.AreEqual(1, messages.Count);
        }

        private Stream XDocumentToStream(XDocument doc)
        {
            var stream = new MemoryStream();
            doc.Save(stream);
            stream.Position = 0;
            return stream;
        }
    }
}
