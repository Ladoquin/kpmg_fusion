﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class PasswordManagerTests
    {
        [TestCase(true)]
        [TestCase(false)]
        public void TestIsValidReturnsResultOfValidator(bool passwordIsValid)
        {
            var svc = setupPasswordManager("username1", passwordIsValid: passwordIsValid);

            var actual = svc.IsPasswordValid("username1", "password1", "password2");

            Assert.AreEqual(passwordIsValid, actual);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void TestChangePasswordReturnsResultOfChange(bool changeIsSuccessful)
        {
            var svc = setupPasswordManager("username1", encryptionIsSuccessful: changeIsSuccessful);

            var actual = svc.ChangePassword("username1", "password1", "password2");

            Assert.AreEqual(changeIsSuccessful, actual);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void TestResetPasswordReturnsResultOfReset(bool resetIsSuccessful)
        {
            var svc = setupPasswordManager("username1", encryptionIsSuccessful: resetIsSuccessful);

            var actual = svc.ResetPassword("username1", "token", "password2");

            Assert.AreEqual(resetIsSuccessful, actual);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void TestChangePasswordSavesPasswordToHistoryWhenAppropriate(bool changeIsSuccessful)
        {
            // Can't use 'Throws' on mock for checking when change is NOT successful because Password Manager handles the error
            // So rather than using a counter for one case and Verifiable for another, just going to use the counter for both situations
            var savedCalls = 0;
            var userProfilePasswordRepositoryMock = new Mock<IUserProfilePasswordRepository>();
            userProfilePasswordRepositoryMock.Setup(x => x.Insert(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<byte[]>())).Callback(() => savedCalls++);

            var svc = setupPasswordManager("username1", encryptionIsSuccessful: changeIsSuccessful, userProfilePasswordRepositoryMock: userProfilePasswordRepositoryMock);

            var actual = svc.ChangePassword("username1", "oldpassword", "password");

            Assert.AreEqual(changeIsSuccessful ? 1 : 0, savedCalls, "changeIsSuccessful: " + changeIsSuccessful.ToString());
        }

        [TestCase(true)]
        [TestCase(false)]
        public void TestResetPasswordSavesPasswordToHistoryWhenAppropriate(bool resetSuccessful)
        {
            // Can't use 'Throws' on mock for checking when reset is NOT successful because Password Manager handles the error
            // So rather than using a counter for one case and Verifiable for another, just going to use the counter for both situations
            var savedCalls = 0;
            var userProfilePasswordRepositoryMock = new Mock<IUserProfilePasswordRepository>();
            userProfilePasswordRepositoryMock.Setup(x => x.Insert(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<byte[]>())).Callback(() => savedCalls++);

            var svc = setupPasswordManager("username1", encryptionIsSuccessful: resetSuccessful, userProfilePasswordRepositoryMock: userProfilePasswordRepositoryMock);

            var actual = svc.ResetPassword("username1", "token", "password");

            Assert.AreEqual(resetSuccessful ? 1 : 0, savedCalls, "resetSuccessful: " + resetSuccessful.ToString());
        }

        private PasswordManager setupPasswordManager(string username, bool passwordIsValid = true, bool encryptionIsSuccessful = true, Mock<IUserProfilePasswordRepository> userProfilePasswordRepositoryMock = null)
        {
            var accountSecurityService = new Mock<IAccountSecurityService>();
            accountSecurityService.Setup(x => x.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(encryptionIsSuccessful);
            accountSecurityService.Setup(x => x.ResetPassword(It.IsAny<string>(), It.IsAny<string>())).Returns(encryptionIsSuccessful);

            var passwordEncrypterMock = new Mock<IUserPasswordEncrypter>();
            passwordEncrypterMock.Setup(x => x.GetEncrypted(It.IsAny<string>())).Returns(new PasswordData(null, null));
            passwordEncrypterMock.Setup(x => x.GetEncrypted(It.IsAny<string>(), It.IsAny<byte[]>())).Returns(new PasswordData(null, null));

            if (userProfilePasswordRepositoryMock == null)
            {
                userProfilePasswordRepositoryMock = new Mock<IUserProfilePasswordRepository>();
            }

            var dbMock = new Mock<IDbInteractionScope>();
            dbMock.Setup(x => x.GetRepository<PasswordData>()).Returns(userProfilePasswordRepositoryMock.Object);
            var dbScopeMock = new Mock<IDbInteractionScopeFactory>();
            dbScopeMock.Setup(x => x.GetScope()).Returns(dbMock.Object);

            var passwordValidationServiceMock = new Mock<IPasswordValidationService>();
            passwordValidationServiceMock.Setup(x => x.IsValid(username, It.IsAny<string>(), It.IsAny<string>())).Returns(passwordIsValid);

            var svc = new PasswordManager(accountSecurityService.Object, passwordEncrypterMock.Object, passwordValidationServiceMock.Object, dbScopeMock.Object);

            return svc;
        }
    }
}
