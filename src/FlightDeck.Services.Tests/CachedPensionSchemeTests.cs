﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.ServiceInterfaces;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class CachedPensionSchemeTests
    {
        [TestCase("2000/01/01", "2099/01/01")]
        [TestCase("2013/03/31", "2099/01/01")]
        [TestCase("2000/01/01", "2014/03/31")]
        [TestCase("2014/03/31", "2014/01/31")]
        public void TestSetAttributionPeriodWithInvalidDatesReturnsFalse(string start, string end)
        {
            var cachedPensionScheme = getCachedPensionScheme(new DateTime(2013, 3, 31), new DateTime(2014, 12, 1));
            cachedPensionScheme.SwitchBasis(0);
            var s = DateTime.Parse(start);
            var e = DateTime.Parse(end);

            var result = cachedPensionScheme.SetAttributionPeriod(s, e);

            Assert.False(result);
        }

        [TestCase("2013/03/31", "2014/03/31")]
        [TestCase("2014/01/31", "2014/03/31")]
        public void TestSetAttributionPeriodWithValidDatesReturnsTrue(string start, string end)
        {
            var cachedPensionScheme = getCachedPensionScheme(new DateTime(2013, 3, 31), new DateTime(2014, 12, 1));
            cachedPensionScheme.SwitchBasis(0);
            var s = DateTime.Parse(start);
            var e = DateTime.Parse(end);

            var result = cachedPensionScheme.SetAttributionPeriod(s, e);

            Assert.True(result);
        }

        private CachedPensionScheme getCachedPensionScheme(DateTime anchorDate, DateTime refreshDate)
        {   
            var bases = new List<Basis> { new MockBasis(anchorDate) };
            var pensionScheme = new MockPensionScheme(bases, refreshDate);

            var basisContextMock = new Mock<IBasisContext>();
            basisContextMock.SetupGet(x => x.AnalysisDate).Returns(DateTime.Now.Date);
            basisContextMock.SetupGet(x => x.Data).Returns(bases.Last());
            basisContextMock.Setup(x => x.Results).Returns(new BasisAnalysisResults());
            var basisMock = new Mock<ICachedBasis>();
            basisMock.SetupGet(x => x.AnchorDate).Returns(anchorDate);
            basisMock.Setup(x => x.ToContext(CachedBasis.ToContextMode.WithAnalysisResults)).Returns(basisContextMock.Object);
            var cachedBasisManagerMock = new Mock<ICachedBasisManager>();
            cachedBasisManagerMock.SetupGet(x => x.CurrentBasis).Returns(basisMock.Object);
            cachedBasisManagerMock.SetupGet(x => x.CurrentBasis.AnchorDate).Returns(anchorDate);
            cachedBasisManagerMock.SetupGet(x => x.CurrentBasis.AttributionEnd).Returns(refreshDate);
            cachedBasisManagerMock.SetupGet(x => x.CurrentBasis.AttributionStart).Returns(anchorDate);

            var cachedPensionScheme = new CachedPensionScheme(pensionScheme, cachedBasisManagerMock.Object, new Serializer(), new DbInteractionScopeFactory());

            return cachedPensionScheme;
        }
    }
}
