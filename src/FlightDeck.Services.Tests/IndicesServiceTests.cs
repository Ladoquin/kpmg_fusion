﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    [TestFixture]
    public class IndicesServiceTests
    {
        [Test]
        public void SmokeTestGetAllSummaries()
        {
            var financialIndicesCache = new Mock<IFinancialIndexRepository>();
            financialIndicesCache.Setup(x => x.GetAll()).Returns(new List<FinancialIndex>
                {
                    new FinancialIndex(1, "one", new SortedList<DateTime,double> { { new DateTime(2015, 1, 1), 1 } }),
                    new FinancialIndex(2, "two", new SortedList<DateTime,double> { { new DateTime(2015, 3, 31), 3 }, { new DateTime(2013, 3, 31), 2 }})
                });
            var svc = new IndicesService(new Lazy<IFinancialIndexRepository>(() => financialIndicesCache.Object));

            var actual = svc.GetAllSummaries();

            Assert.IsNotNull(actual);
            Assert.AreEqual("one", actual.First().Name);
            Assert.AreEqual(new DateTime(2015, 1, 1), actual.First().Start);
            Assert.AreEqual(new DateTime(2015, 3, 31), actual.Last().End);
        }

        [Test]
        public void SmokeTestGetIndexValues()
        {
            var financialIndicesCache = new Mock<IFinancialIndexRepository>();
            financialIndicesCache.Setup(x => x.GetById(1)).Returns(
                new FinancialIndex(1, "one", new SortedList<DateTime, double> 
                { 
                    { new DateTime(2015, 1, 1), 1 },
                    { new DateTime(2015, 1, 2), 2 },
                    { new DateTime(2015, 1, 3), 3 }
                }));

            var svc = new IndicesService(new Lazy<IFinancialIndexRepository>(() => financialIndicesCache.Object));

            var actual = svc.GetIndexValues(1);

            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Keys.Count);
            Assert.AreEqual(3, actual[new DateTime(2015, 1, 3)]);
        }
    }
}
