﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    [TestFixture]
    public class SchemeSnapshotServiceTests
    {
        [Test]
        public void TestSimpleNonAccountingBasisSnapshotSuccessful()
        {
            var pensionScheme = getPensionScheme();

            var actual = new SchemeSnapshotService().GetInstanceSnapshot(pensionScheme.Object);

            Assert.NotNull(actual);
            Assert.NotNull(actual.Children);
            Assert.AreEqual(1, actual.Children.Where(x => x.Key == "inputs").Count());
            Assert.AreEqual(1, actual.Children.Where(x => x.Key == "outputs").Count());

            var inputs = actual.Children.Single(x => x.Key == "inputs");

            Assert.NotNull(inputs.Properties);
            Assert.AreEqual(RecoveryPlanType.Current, (RecoveryPlanType)inputs.Properties.Single(x => x.Key == "RecoveryPlanType").Value);
            Assert.NotNull(inputs.Children);
            Assert.AreEqual(0.032, (double)inputs.Children.Single(x => x.Key == "AssetAssumptions").Properties.Single(x => x.Key == "Cash").Value);

            var outputs = actual.Children.Single(x => x.Key == "outputs");

            Assert.NotNull(outputs);
            Assert.AreEqual(700000, (double)outputs.Children.Single(x => x.Key == "FundingPosition").Properties["Pensioners"]);
        }

        [Test]
        public void TestSnapshotOnlyReturnsAfterValues()
        {
            var pensionScheme = getPensionScheme();
            pensionScheme.Setup(x => x.CurrentBasis.GetFundingLevelData()).Returns(new BeforeAfter<FundingData>(new FundingData(1, 2, 3, 4, 5, 6), new FundingData(-1, -2, -3, -4, -5, -6)));
            
            var actual = new SchemeSnapshotService().GetInstanceSnapshot(pensionScheme.Object);

            Assert.AreEqual(-5, (double)actual.Children.Single(x => x.Key == "outputs").Children.Single(x => x.Key == "FundingPosition").Properties["Buyin"]);
        }

        [Test]
        public void TestMoreComplicatedOutputStructuresInSnapshot()
        {
            var pensionScheme = getPensionScheme();
            pensionScheme.Setup(x => x.CurrentBasis.GetJourneyPlan()).Returns(new JourneyPlanResult
                {
                    BeforePlan = new BeforeAfter<IList<LensData>>(new List<LensData> { new LensData(100, 100), new LensData(200, 200) }, new List<LensData> { new LensData(300, 300), new LensData(400, 400) }),
                    AfterPlan = new BeforeAfter<IList<LensData>>(new List<LensData> { new LensData(500, 500), new LensData(600, 600) }, new List<LensData> { new LensData(700, 700), new LensData(800, 800) })
                });
            pensionScheme.Setup(x => x.CurrentBasis.GetCashFlowData()).Returns(new BeforeAfter<List<LiabilityData>>(
                new List<LiabilityData> { new LiabilityData(1, 2, 3, 4), new LiabilityData(5, 6, 7, 8) },
                new List<LiabilityData> { new LiabilityData(11, 12, 13, 14), new LiabilityData(15, 16, 17, 18) }
                ));
            pensionScheme.Setup(x => x.GetStrains()).Returns(new Dictionary<string, Tuple<IDictionary<StrainType, double>, DateTime>>
                {
                    { "Accounting", new Tuple<IDictionary<StrainType,double>, DateTime>(new Dictionary<StrainType, double> { { StrainType.Min, -1.0 }, { StrainType.Current, 0.0 }, { StrainType.Max, 1.0 } }, new DateTime(2015, 3, 2)) },
                    { "Buyout", new Tuple<IDictionary<StrainType,double>, DateTime>(new Dictionary<StrainType, double> { { StrainType.Min, -523.0 }, { StrainType.Current, 253.34 }, { StrainType.Max, 4351.0 } }, new DateTime(2015, 3, 2)) }
                });

            var actual = new SchemeSnapshotService().GetInstanceSnapshot(pensionScheme.Object);

            Assert.NotNull(actual);
            Assert.AreEqual(400, (double)actual.Children.Single(x => x.Key == "outputs").Children.Single(x => x.Key == "FundingProgression").Children.Single(x => x.Key == "Before").Children.Single(x => x.Key == "year01").Properties["Assets"]);
            Assert.AreEqual(17, (double)actual.Children.Single(x => x.Key == "outputs").Children.Single(x => x.Key == "Cashflows").Children.Single(x => x.Key == "year01").Properties["Deferred"]);
            Assert.AreEqual(-523, (double)actual.Children.Single(x => x.Key == "outputs").Children.Single(x => x.Key == "Insurance").Children.Single(x => x.Key == "Strains").Children.Single(x => x.Key == "Buyout").Properties["MinStrain"]);
        }

        [Test]
        public void TestNonAccountingBasisSnapshotDoesntReturnAccountingValues()
        {
            var pensionScheme = getPensionScheme();
            pensionScheme.Setup(x => x.CurrentBasis.BasisType).Returns(BasisType.TechnicalProvision);
            pensionScheme.Setup(x => x.CurrentBasis.GetBalanceSheetEstimate()).Returns(new MutableBeforeAfter<BalanceData>(new BalanceData(1, 2))); // make sure this doesn't pass just because BalanceSheet returns empty

            var actual = new SchemeSnapshotService().GetInstanceSnapshot(pensionScheme.Object);

            Assert.False(actual.Children.Single(x => x.Key == "outputs").Children.Any(x => x.Key == "BalanceSheet")); // pick an accounting specific output
        }

        [Test]
        public void TestAccountingBasisSnapshotReturnsAccountingValues()
        {
            var pensionScheme = getPensionScheme();
            pensionScheme.Setup(x => x.CurrentBasis.BasisType).Returns(BasisType.Accounting);
            pensionScheme.Setup(x => x.CurrentBasis.GetBalanceSheetEstimate()).Returns(new MutableBeforeAfter<BalanceData>(new BalanceData(1, 2)));
            pensionScheme.SetupGet(x => x.AccountingSettings).Returns(new AccountingSettings(AccountingStandardType.IAS19, true, false, false));

            var actual = new SchemeSnapshotService().GetInstanceSnapshot(pensionScheme.Object);

            Assert.True(actual.Children.Single(x => x.Key == "outputs").Children.Any(x => x.Key == "BalanceSheet"));
            Assert.True(actual.Children.Single(x => x.Key == "outputs").Children.Any(x => x.Key == "IASProfitLoss"));
        }

        private Mock<IPensionScheme> getPensionScheme()
        {
            var pensionScheme = new Mock<IPensionScheme>();

            pensionScheme.SetupGet(x => x.Name).Returns("The Space01 Test Scheme");

            // mock inputs - PAINFUL having to set them all up, but it's the only way to get a proper test
            pensionScheme.Setup(x => x.CurrentBasis.GetClientLiabilityAssumptions()).Returns(new MutableBeforeAfter<AssumptionData>(new AssumptionData(0.05, 0.05, 0.05, 0.3, 0.2, 0.25, 21.5, 27.2)));
            pensionScheme.Setup(x => x.CurrentBasis.GetClientPensionIncreases()).Returns(new MutableBeforeAfter<IDictionary<int, double>>(new Dictionary<int, double> { { 1, 0.01 }, { 2, 0.02 } }));
            pensionScheme.Setup(x => x.CurrentBasis.GetClientAssetAssumptions())
                .Returns(new MutableBeforeAfter<IDictionary<AssetClassType, double>>(new Dictionary<AssetClassType, double>
                    {
                        { AssetClassType.Equity, 0.05 },
                        { AssetClassType.DiversifiedGrowth, 0.06 },
                        { AssetClassType.Cash, 0.032 }
                    }));
            pensionScheme.Setup(x => x.CurrentBasis.GetClientRecoveryAssumptions()).Returns(new MutableBeforeAfter<RecoveryPlanAssumptions>(new RecoveryPlanAssumptions(0.1, 0, 1000000, 99999999, 0, true, 0, 5)));
            pensionScheme.Setup(x => x.GetClientAbfOptions()).Returns(new MutableBeforeAfter<AbfOptions>(new AbfOptions(0)));
            pensionScheme.Setup(x => x.GetClientEFroOptions()).Returns(new MutableBeforeAfter<EmbeddedFlexibleReturnOptions>(new EmbeddedFlexibleReturnOptions(0, 0)));
            pensionScheme.Setup(x => x.GetClientEtvOptions()).Returns(new MutableBeforeAfter<EnhancedTransferValueOptions>(new EnhancedTransferValueOptions(0, 0, 0)));
            pensionScheme.Setup(x => x.GetClientFroOptions()).Returns(new MutableBeforeAfter<FlexibleReturnOptions>(new FlexibleReturnOptions(1000000, 0.5)));
            pensionScheme.Setup(x => x.GetClientFroType()).Returns(new MutableBeforeAfter<FROType>(FROType.Bulk));
            pensionScheme.Setup(x => x.GetClientFutureBenefitsParameters()).Returns(new MutableBeforeAfter<FutureBenefitsParameters>(new FutureBenefitsParameters(2000000, BenefitAdjustmentType.None, 0)));
            pensionScheme.Setup(x => x.GetClientInsuranceParameters()).Returns(new MutableBeforeAfter<InsuranceParameters>(new InsuranceParameters(0, 0)));
            pensionScheme.Setup(x => x.GetClientInvestmentStrategyAllocations()).Returns(new MutableBeforeAfter<IDictionary<AssetClassType, double>>(new Dictionary<AssetClassType, double>()));
            pensionScheme.Setup(x => x.GetClientInvestmentStrategyOptions()).Returns(new MutableBeforeAfter<InvestmentStrategyOptions>(new InvestmentStrategyOptions(0, 0, 0, false, 0, 0)));
            pensionScheme.Setup(x => x.GetClientPieOptions()).Returns(new MutableBeforeAfter<PieOptions>(new PieOptions(0, 0, 0)));
            pensionScheme.Setup(x => x.GetClientRecoveryPlanOptions()).Returns(new MutableBeforeAfter<RecoveryPlanType>(RecoveryPlanType.Current));

            // mock outputs - again, PAINFUL having to set them all up, but it's the only way to get a proper test
            pensionScheme.Setup(x => x.CurrentBasis.GetAbfData()).Returns(new MutableBeforeAfter<AbfData>(new AbfData(0, 0, 0, 0, 0, 0, 0, 0)));
            pensionScheme.Setup(x => x.CurrentBasis.GetBalanceSheetEstimate()).Returns(new MutableBeforeAfter<BalanceData>(new BalanceData(2000000, 1000000)));
            pensionScheme.Setup(x => x.CurrentBasis.GetBuyinInfo()).Returns(new MutableBeforeAfter<BuyinData>(new BuyinData(0, 0, 0, 0)));
            pensionScheme.Setup(x => x.CurrentBasis.GetCashFlowData()).Returns(new MutableBeforeAfter<List<LiabilityData>>(new List<LiabilityData>()));
            pensionScheme.Setup(x => x.CurrentBasis.GetEFroData()).Returns(new BeforeAfter<EFroData>());
            pensionScheme.Setup(x => x.CurrentBasis.GetEtvData()).Returns(new BeforeAfter<EtvData>());
            pensionScheme.Setup(x => x.CurrentBasis.GetFroData()).Returns(new BeforeAfter<FroData>());
            pensionScheme.Setup(x => x.CurrentBasis.GetFRSData()).Returns(new BeforeAfter<IAS19Disclosure.IAS19PAndLForecastData>());
            pensionScheme.Setup(x => x.CurrentBasis.GetFundingLevelData()).Returns(new MutableBeforeAfter<FundingData>(new FundingData(500000, 600000, 700000, 1000000, 20000, 780000)));
            pensionScheme.Setup(x => x.CurrentBasis.GetFutureBenefits()).Returns(new BeforeAfter<FutureBenefitsData>());
            pensionScheme.Setup(x => x.CurrentBasis.GetIASData()).Returns(new BeforeAfter<IAS19Disclosure.IAS19PAndLForecastData>());
            pensionScheme.Setup(x => x.CurrentBasis.GetInsurancePointChangeImpact()).Returns(new BeforeAfter<double>());
            pensionScheme.Setup(x => x.CurrentBasis.GetInvestmentStrategyData()).Returns(new BeforeAfter<InvestmentStrategyData>());
            pensionScheme.Setup(x => x.CurrentBasis.GetJourneyPlan()).Returns(new JourneyPlanResult { BeforePlan = new MutableBeforeAfter<IList<LensData>>(new List<LensData>()), AfterPlan = new MutableBeforeAfter<IList<LensData>>(new List<LensData>()) });
            pensionScheme.Setup(x => x.CurrentBasis.GetPieData()).Returns(new BeforeAfter<PieData>());
            pensionScheme.Setup(x => x.CurrentBasis.GetRecoveryPlan()).Returns(new MutableBeforeAfter<IList<double>>(new List<double>()));
            pensionScheme.Setup(x => x.CurrentBasis.GetUSGAAPData()).Returns(new BeforeAfter<USGAAPDisclosure.NetPeriodicPensionCostForecastData>());
            pensionScheme.Setup(x => x.CurrentBasis.GetVarFunnel()).Returns(new MutableBeforeAfter<IList<VarFunnelData>>(new List<VarFunnelData>()));
            pensionScheme.Setup(x => x.CurrentBasis.GetVarWaterfall()).Returns(new BeforeAfter<WaterfallData>());
            pensionScheme.Setup(x => x.GetStrains()).Returns(new Dictionary<string, Tuple<IDictionary<StrainType, double>, DateTime>>());

            return pensionScheme;
        }
    }
}
