﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    
    [TestFixture]
    public class DataImportServiceTests
    {
        private XDocument doc = new XDocument(
            new XElement("Data",
                new XElement("Index",
                    new XElement("IndexName", "TEST CURVE INDEX for DataImportServiceTests"),
                    new XElement("Date", new XAttribute("Value", "2014-01-01"),
                        new XElement("Spot", new XAttribute("Maturity", "1"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "2"), 0.2),
                        new XElement("Spot", new XAttribute("Maturity", "3"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "4"), 0.2),  
                        new XElement("Spot", new XAttribute("Maturity", "5"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "6"), 0.2),
                        new XElement("Spot", new XAttribute("Maturity", "7"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "8"), 0.2),    
                        new XElement("Spot", new XAttribute("Maturity", "9"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "10"), 0.2),
                        new XElement("Spot", new XAttribute("Maturity", "11"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "12"), 0.2),  
                        new XElement("Spot", new XAttribute("Maturity", "13"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "14"), 0.2),
                        new XElement("Spot", new XAttribute("Maturity", "15"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "16"), 0.2),  
                        new XElement("Spot", new XAttribute("Maturity", "17"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "18"), 0.2),
                        new XElement("Spot", new XAttribute("Maturity", "19"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "20"), 0.2),  
                        new XElement("Spot", new XAttribute("Maturity", "21"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "22"), 0.2),
                        new XElement("Spot", new XAttribute("Maturity", "23"), 0.1),
                        new XElement("Spot", new XAttribute("Maturity", "24"), 0.2),    
                        new XElement("Spot", new XAttribute("Maturity", "25"), 0.2)   
                    ))));

        [Test]
        public void TestImportCurveDataRunsWithExistingIndex()
        {
            var repo = new Mock<ICurveDataRepository>();
            repo.Setup(x => x.GetByName(It.IsAny<string>())).Returns(new CurveDataProjection(0, "TEST CURVE INDEX for DataImportServiceTests", new List<SpotRate>()));
            var scope = new Mock<IDbTransactionalScope>();
            scope.Setup(x => x.GetRepository<CurveDataProjection>()).Returns(repo.Object);
            var factory = new Mock<IDbInteractionScopeFactory>();
            factory.Setup(x => x.GetTransactionalScope()).Returns(scope.Object);
            var service = new DataImportService(factory.Object);

            service.ImportCurveData(doc);
        }

        [Test]
        public void TestImportCurveDataRunsWithNewIndex()
        {
            var repo = new Mock<ICurveDataRepository>();
            repo.Setup(x => x.GetByName(It.IsAny<string>())).Returns(default(CurveDataProjection));
            var scope = new Mock<IDbTransactionalScope>();
            scope.Setup(x => x.GetRepository<CurveDataProjection>()).Returns(repo.Object);
            var factory = new Mock<IDbInteractionScopeFactory>();
            factory.Setup(x => x.GetTransactionalScope()).Returns(scope.Object);
            var service = new DataImportService(factory.Object);

            service.ImportCurveData(doc);
        }

        [Test]
        public void CurveImportExtrapolatesUpTo50YearsOfData()
        {
            var repo = new Mock<ICurveDataRepository>();
            repo.Setup(x => x.GetByName(It.IsAny<string>())).Returns(default(CurveDataProjection));

            var scope = new Mock<IDbTransactionalScope>();
            scope.Setup(x => x.GetRepository<CurveDataProjection>()).Returns(repo.Object);

            var factory = new Mock<IDbInteractionScopeFactory>();
            factory.Setup(x => x.GetTransactionalScope()).Returns(scope.Object);

            var service = new Mock<DataImportService>(factory.Object);
            service.Setup(x => x.Fill50YearsCurveData(It.IsAny<List<SpotRate>>(), It.IsAny<DateTime>())).Verifiable();//once per Date in doc

            service.CallBase = true;
            service.Object.ImportCurveData(doc);

            service.Verify(x => x.Fill50YearsCurveData(It.IsAny<List<SpotRate>>(), It.IsAny<DateTime>()), Times.AtLeastOnce());
        }
    }
}