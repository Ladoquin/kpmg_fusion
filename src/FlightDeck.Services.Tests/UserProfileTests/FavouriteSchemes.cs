﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FlightDeck.Domain;
using Moq;
using System.Security.Principal;
using System.IO;
using WebMatrix.WebData;
using Fusion.Shared.Data;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;

namespace FlightDeck.Services.Tests.UserProfileTests
{
    [TestFixture]
    class FavouriteSchemes
    {

        [Test]
        public void UserCannotAddSameSchemeToFavouritesAgain()
        {
            // Arrange
            var user = GetTestUser();
            var userService = GetUserService();
            var scheme = GetTestScheme();
            
            // Act
            var result = userService.AddFavouriteScheme(user, scheme.SchemeDetailId);

            // Assert
            Assert.IsFalse(result);
            Assert.IsTrue(user.FavouriteSchemes.Count() == 1);
        }

        private UserProfile GetTestUser()
        {
            var schemeList = new List<SchemeDetail>();
            schemeList.Add(GetTestScheme());
            return new UserProfile
            {
                UserId = 1,
                UserName = "fusiontestuser",
                FavouriteSchemes = schemeList
            };
        }

        private SchemeDetail GetTestScheme()
        {
            return new SchemeDetail
            {
                SchemeDetailId = 1,
                SchemeName = "Test Scheme"
            };
        }

        private IUserProfileService GetUserService()
        {
            Mock<IUserProfileService> mockService = new Mock<IUserProfileService>();
            return mockService.Object;
        }

    }
}
