﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Text;

    [TestFixture]
    public class PasswordValidationServiceTests
    {
        [TestCase(null, null)]
        [TestCase(null, "")]
        [TestCase(null, "       ")]
        [TestCase("oldpassword1", null)]
        [TestCase("oldpassword1", "")]
        [TestCase("oldpassword1", "       ")]
        public void TestEmptyPasswordIsRejected(string oldpassword, string password)
        {
            var svc = setupPasswordValidationService("username1", false);

            var actual = svc.IsValid("username1", oldpassword, password);

            Assert.IsFalse(actual, string.Format("Password: {0}, old password: {1}", password, oldpassword));
        }

        [TestCase("username1", "username1")]
        [TestCase("username1", "USERNAME1")]
        [TestCase("!UserName1!", "username1")]
        [TestCase("password1", "word1")]
        [TestCase("word1", "password1")]
        public void TestPasswordSameAsUsernameIsRejected(string username, string password)
        {
            var svc = setupPasswordValidationService(username, false);

            var actual = svc.IsValid(username, "oldpassword", password);

            Assert.IsFalse(actual, string.Format("Username: {0}, password: {1}", username, password));
        }

        [TestCase("username1", "password1")]
        [TestCase("user1", "user2")]
        public void TestPasswordNotSameAsUsernameIsAccepted(string username, string password)
        {
            var svc = setupPasswordValidationService(username, false);

            var actual = svc.IsValid(username, "oldpassword", password);

            Assert.IsTrue(actual, string.Format("Username: {0}, password: {1}", username, password));
        }

        [TestCase("password1", "password1")]
        [TestCase("password1", "PASSWORD1")]
        [TestCase("PASSWORD1", "password1")]
        [TestCase("p455w0rd!", "p455w0rd!")]
        public void TestPasswordSameAsExistingPasswordIsRejected(string password, string oldpassword)
        {
            var svc = setupPasswordValidationService("username1", false);

            var actual = svc.IsValid("username1", oldpassword, password);

            Assert.IsFalse(actual);
        }

        [TestCase("username1", "username1", false)]
        [TestCase("user1", "user123", false)]
        [TestCase("username1", "password1", true)]
        public void TestOldPasswordAsNullStillRunsApplicableTests(string username, string password, bool expected)
        {
            var svc = setupPasswordValidationService(username, false);

            var actual = svc.IsValid(username, null, password);

            Assert.AreEqual(expected, actual, string.Format("Username: {0}, password: {1}", username, password));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void TestNewPasswordHistoryCheck(bool passwordIsInHistory)
        {
            var expected = !passwordIsInHistory;
            var svc = setupPasswordValidationService("username1", passwordIsInHistory);

            var actual = svc.IsValid("username1", "foo", "bar");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestNewPasswordHistoryCheckIsSuccessfulForExistingUsersWithoutHistory()
        {
            var svc = setupPasswordValidationService("username1", false, false);

            var actual = svc.IsValid("username1", "foo", "bar");

            Assert.IsTrue(actual);
        }

        private PasswordValidationService setupPasswordValidationService(string username, bool passwordIsInHistory, bool hasSalt = true)
        {
            var passwordHistory = new List<byte[]> { Encoding.UTF8.GetBytes("password1") };
            var passwordEncryptorMock = new Mock<IUserPasswordEncrypter>();
            passwordEncryptorMock.Setup(x => x.GetEncrypted(It.IsAny<string>())).Returns(new PasswordData(passwordIsInHistory ? passwordHistory[0] : Encoding.UTF8.GetBytes("password99"), new byte[0]));
            passwordEncryptorMock.Setup(x => x.GetEncrypted(It.IsAny<string>(), It.IsAny<byte[]>())).Returns(new PasswordData(passwordIsInHistory ? passwordHistory[0] : Encoding.UTF8.GetBytes("password99"), new byte[0]));
            
            var userProfilePasswordRepositoryMock = new Mock<IUserProfilePasswordRepository>();
            userProfilePasswordRepositoryMock.Setup(x => x.GetHistory(username)).Returns(passwordHistory);
            userProfilePasswordRepositoryMock.Setup(x => x.GetSalt(username)).Returns(hasSalt ? new byte[0] : null);
            var dbMock = new Mock<IDbInteractionScope>();
            dbMock.Setup(x => x.GetRepository<PasswordData>()).Returns(userProfilePasswordRepositoryMock.Object);
            var dbScopeMock = new Mock<IDbInteractionScopeFactory>();
            dbScopeMock.Setup(x => x.GetScope()).Returns(dbMock.Object);

            var svc = new PasswordValidationService(passwordEncryptorMock.Object, dbScopeMock.Object);

            return svc;
        }
    }
}
