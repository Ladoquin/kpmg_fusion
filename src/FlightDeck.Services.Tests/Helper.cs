﻿using System;
using System.IO;

namespace FlightDeck.Services.Tests
{
    class Helper
    {
        public static string GetTrackerDir()
        {
            return Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"..\..\..\..\test_data\"));
        }

    }
}
