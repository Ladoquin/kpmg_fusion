﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Assets;
    using FlightDeck.DomainShared;
    using Moq;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class MockPensionScheme : PensionScheme
    {
        private DateTime refreshDate;

        public MockPensionScheme(IEnumerable<Basis> bases, DateTime refreshDate, string name = null)
            : base(
                1,
                name ?? string.Empty,
                string.Empty,
                bases,
                new List<SchemeAsset> { new SchemeAsset(1, 1, bases.Last().EffectiveDate, false, "Cash Fund", 0.2, 0.1, 0.1, 0.5, 18, 15,
                    new List<Asset> { new Asset(1, 1, new AssetClass(1, "Cash", AssetCategory.Matched, new AssetReturnSpec(0, 1, 1), 0), true, 0.5,
                        new List<AssetFund> {new AssetFund(1, "Cash Fund", 1, new AssetClass(1, "Cash", AssetCategory.Matched, new AssetReturnSpec(0, 1, 1), 0), 
                            new FinancialIndex(1, "one", new SortedList<DateTime,double> { { new DateTime(2015, 1, 1), 1 } }), 1, true)}) }) },
                new List<Contribution>(),
                new List<RecoveryPayment>(),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                .0,
                false,
                .0,
                AccountingStandardType.IAS19,
                true,
                false,
                false,
                null,
                null,
                null,
                0.03,
                null,
                null,
                true,
                "Accounting"
            )
        {
            this.refreshDate = refreshDate;

            Bases = bases
                .GroupBy(b => b.DisplayName)
                .ToDictionary(
                    b => new MasterBasisKey(b.First().MasterBasisId, b.First().Type, b.Key, Id),
                    b => new MasterBasis(b));
        }

        public override DateTime RefreshDate
        {
            get
            {
                return refreshDate;
            }
        }

        protected override void SetBases(IEnumerable<Basis> bases)
        {
        }

        protected override void SetAssetEffectiveDates()
        {
        }

        protected override void SetRefreshDate(IEnumerable<Basis> bases)
        {
            RefreshDate = refreshDate;
        }
    }
}

