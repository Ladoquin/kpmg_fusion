﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using Moq;
    using System;
    using System.Collections.Generic;

    class MockBasis : Basis
    {
        public MockBasis(DateTime effectiveDate)
            : base(
                1,
                1,
                1,
                string.Empty,
                BasisType.Accounting,
                effectiveDate,
                effectiveDate,
                false,
                new List<SalaryProgression>(),
                new List<CashflowData>(),
                new Dictionary<AssumptionType, double>(),
                new Dictionary<int, double>(),
                new Dictionary<int, double>(),
                new BondYield(), //todo: real index needed here?
                0, 0, 0, 0,
                new Dictionary<SimpleAssumptionType, SimpleAssumption>()
                    {
                        { SimpleAssumptionType.LifeExpectancy65, new SimpleAssumption(SimpleAssumptionType.LifeExpectancy65, 21.5, "Life expectancy at 65 (male)", true) },
                        { SimpleAssumptionType.LifeExpectancy65_45, new SimpleAssumption(SimpleAssumptionType.LifeExpectancy65_45, 22.7, "Life expectancy at 65 (male currently 45)", true) },
                    },
                0,
                new Dictionary<AssumptionType, FinancialAssumption>(),
                new Dictionary<int, PensionIncreaseAssumption>(),
                getGiltIndex(effectiveDate)
            )
        {
        }

        private static FinancialIndex getGiltIndex(DateTime effectiveDate)
        {
            var values = new Dictionary<DateTime, double>();
            for (int i = 0; i <= (DateTime.Now - effectiveDate).Days; i++)
                values.Add(effectiveDate.AddDays(i), 1);

            return new FinancialIndex(1, string.Empty, new SortedList<DateTime, double>(values));
        }
    }
}
