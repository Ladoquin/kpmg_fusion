﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;
    
    [TestFixture]
    public class UserProfileServiceTests
    {
        int requesterId = 1;
        int targetId = 2;

        #region Tests with Installer as logged in user

        [Test]
        public void TestInstallerCanNotSeeAdmin()
        {
            var svc = setupService(new List<int> { 1 }, null, null, null, RoleType.Installer, RoleType.Admin, false);

            var actual = svc.GetById(targetId);

            Assert.IsNull(actual);
        }

        [Test]
        public void TestInstallerCanSeeAllInstallers()
        {
            var svc = setupService(null, null, null, null, RoleType.Installer, RoleType.Installer, false);

            var actual = svc.GetById(targetId);

            Assert.NotNull(actual);
            Assert.AreEqual(targetId, actual.UserId);
        }

        [Test]
        public void TestInstallerCanSeeAllConsultants()
        {
            var svc = setupService(null, new List<int> { 1, 2 }, new List<int> { 1 }, new List<int> { 2 }, RoleType.Installer, RoleType.Consultant, false);

            var actual = svc.GetById(targetId);

            Assert.NotNull(actual);
            Assert.AreEqual(targetId, actual.UserId);
        }

        [Test]
        public void TestInstallerCanSeeClientWithAccessToAtLeastOneUnrestrictedScheme()
        {
            var svc = setupService(new List<int> { 1 }, new List<int> { 2 }, null, new List<int> { 1, 2 }, RoleType.Installer, RoleType.Client, false);

            var actual = svc.GetById(targetId);

            Assert.NotNull(actual);
            Assert.AreEqual(targetId, actual.UserId);
        }

        [Test]
        public void TestInstallerCanSeeClientTheyCreated()
        {
            var svc = setupService(new List<int> { 1 }, new List<int> { 2 }, null, new List<int> { 2 }, RoleType.Installer, RoleType.Client, true);

            var actual = svc.GetById(targetId);

            Assert.NotNull(actual);
            Assert.AreEqual(targetId, actual.UserId);
        }

        [Test]
        public void TestInstallerCanSeeClientWithAccessToAtLeastOneRestrictedSchemeTheInstallerHasAccessTo()
        {
            var svc = setupService(new List<int> { 4, 5, 6 }, new List<int> { 1, 2, 3 }, new List<int> { 1, 2 }, new List<int> { 2, 3 }, RoleType.Installer, RoleType.Client, false);

            var actual = svc.GetById(targetId);

            Assert.NotNull(actual);
            Assert.AreEqual(targetId, actual.UserId);
        }

        [Test]
        public void TestInstallerCanNotSeeClientWithAccessToOnlyRestrictedSchemesTheInstallerHasNoAccessTo()
        {
            var svc = setupService(new List<int> { 5, 6, 7 }, new List<int> { 1, 2, 3, 4 }, new List<int> { 1, 2}, new List<int> { 3, 4 }, RoleType.Installer, RoleType.Client, false);

            var actual = svc.GetById(targetId);

            Assert.IsNull(actual);
        }

        [Test]
        public void TestInstallerCanSeeClientsUnrestrictedSchemes()
        {
            var svc = setupService(new List<int> { 1, 2, 3 }, new List<int> { 4, 5 }, new List<int> { 4 }, new List<int> { 1, 5 }, RoleType.Installer, RoleType.Client, false);

            var actual = svc.GetById(targetId);

            Assert.NotNull(actual);
            Assert.AreEqual(targetId, actual.UserId);
            Assert.Contains(1, actual.SchemeDetails.Select(x => x.SchemeDetailId).ToList());
        }

        [Test]
        public void TestInstallerCanSeeClientsRestrictedSchemesInstallerHasAccessTo()
        {
            var svc = setupService(new List<int> { 1, 2, 3 }, new List<int> { 4, 5, 6 }, new List<int> { 4, 5 }, new List<int> { 1, 5 }, RoleType.Installer, RoleType.Client, false);

            var actual = svc.GetById(targetId);

            Assert.NotNull(actual);
            Assert.AreEqual(targetId, actual.UserId);
            Assert.Contains(5, actual.SchemeDetails.Select(x => x.SchemeDetailId).ToList());
        }

        [Test]
        public void TestInstallerCanNotSeeClientsRestrictedSchemesInstallerHasNoAccessTo()
        {
            var svc = setupService(new List<int> { 1, 2, 3 }, new List<int> { 4, 5, 6 }, new List<int> { 4 }, new List<int> { 1, 5, 6 }, RoleType.Installer, RoleType.Client, false);

            var actual = svc.GetById(targetId);

            Assert.NotNull(actual);
            Assert.AreEqual(targetId, actual.UserId);
            Assert.False(actual.SchemeDetails.Any(x => x.SchemeDetailId == 5 || x.SchemeDetailId == 6)); 
        }

        #endregion

        UserProfileService setupService(IEnumerable<int> unrestrictedSchemes, IEnumerable<int> restrictedSchemes, IEnumerable<int> requesterSchemeAccess, IEnumerable<int> targetSchemeAccess, RoleType requesterRole, RoleType targetRole, bool targetCreatedByRequester)
        {
            unrestrictedSchemes = unrestrictedSchemes ?? new List<int>();
            restrictedSchemes = restrictedSchemes ?? new List<int>();
            var schemeList =
                unrestrictedSchemes.Union(restrictedSchemes)
                    .Select(x => new SchemeDetail { SchemeDetailId = x, IsRestricted = !unrestrictedSchemes.Contains(x) });
            var targetCreatedBy = targetCreatedByRequester ? requesterId : 99;

            var requester = new UserProfile
            {
                Role = requesterRole,
                UserId = requesterId,
                UserName = "requester",
                SchemeDetails = requesterSchemeAccess.IsNullOrEmpty() ? new List<SchemeDetail>() : schemeList.Where(x => requesterSchemeAccess.Contains(x.SchemeDetailId))
            };

            var target = new UserProfile
            {
                Role = targetRole,
                UserId = targetId,
                ActiveSchemeDetail = targetSchemeAccess.IsNullOrEmpty() ? null : schemeList.First(x => targetSchemeAccess.Contains(x.SchemeDetailId)),
                ActiveSchemeDetailId = targetSchemeAccess.IsNullOrEmpty() ? (int?)null : targetSchemeAccess.First(),
                CreatedByUserId = targetCreatedBy,
                SchemeDetails = targetSchemeAccess.IsNullOrEmpty() ? new List<SchemeDetail>() : schemeList.Where(x => targetSchemeAccess.Contains(x.SchemeDetailId))
            };

            var userProfileRepoMoq = new Mock<IUserProfileRepository>();
            userProfileRepoMoq.Setup(x => x.GetById(targetId)).Returns(target);
            userProfileRepoMoq.Setup(x => x.GetByUsername("requester")).Returns(requester);

            var schemeDetailRepoMoq = new Mock<ISchemeDetailRepository>();
            schemeDetailRepoMoq.Setup(x => x.GetAllUnrestricted()).Returns(schemeList.Where(x => !x.IsRestricted).Select(x => x.SchemeDetailId));

            var dbScopeMoq = new Mock<IDbInteractionScope>();
            dbScopeMoq.Setup(x => x.GetRepository<UserProfile>()).Returns(userProfileRepoMoq.Object);
            dbScopeMoq.Setup(x => x.GetRepository<SchemeDetail>()).Returns(schemeDetailRepoMoq.Object);
            var dbScopeFactoryMoq = new Mock<IDbInteractionScopeFactory>();
            dbScopeFactoryMoq.Setup(x => x.GetScope()).Returns(dbScopeMoq.Object);

            var identityMoq = new Mock<IIdentity>();
            identityMoq.SetupGet(x => x.Name).Returns("requester");
            var principalMoq = new Mock<IPrincipal>();
            principalMoq.Setup(x => x.Identity).Returns(identityMoq.Object);

            var roleManagerMoq = new Mock<IRoleManager>();
            var svc = new UserProfileService(principalMoq.Object, roleManagerMoq.Object, dbScopeFactoryMoq.Object);

            return svc;
        }

    }
}
