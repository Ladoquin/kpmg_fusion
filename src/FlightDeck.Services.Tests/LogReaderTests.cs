﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class LogReaderTests
    {
        [Test]
        public void GetAllReturnsLogs()
        {            
            var moqLogRepo = new Mock<ILogRepository>();
            moqLogRepo.Setup(repo => repo.GetAll(It.IsAny<DateTime>(), It.IsAny<string>())).Returns(new List<Log> { new Log(1, DateTime.Now, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty) });
            var moqDbScope = new Mock<ILogDbInteractionScope>();
            moqDbScope.Setup(db => db.GetRepository<Log>()).Returns(moqLogRepo.Object);
            var logReader = new LogReader();
            logReader.LogDbInteractionScope = moqDbScope.Object;
            
            var logEntries = logReader.GetAll(DateTime.Now.Date, null);

            Assert.IsTrue(logEntries is IEnumerable<Log>);
            Assert.IsNotNull(logEntries);
            Assert.IsTrue(logEntries.Any());
        }

        [Test]
        public void GetAllReturnsEmptyResultInsteadOfNullWhenNoLogsFound()
        {
            var moqLogRepo = new Mock<ILogRepository>();
            moqLogRepo.Setup(repo => repo.GetAll(It.IsAny<DateTime>(), It.IsAny<string>())).Returns<IEnumerable<Log>>(null);
            var moqDbScope = new Mock<ILogDbInteractionScope>();
            moqDbScope.Setup(db => db.GetRepository<Log>()).Returns(moqLogRepo.Object);
            var logReader = new LogReader();
            logReader.LogDbInteractionScope = moqDbScope.Object;

            var logEntries = logReader.GetAll(DateTime.Now.Date, null);

            Assert.IsNotNull(logEntries);
            Assert.IsFalse(logEntries.Any());
        }

        [Test]
        public void GetAllReturnsEmptyResultWithUnmatchedParameters()
        {
        }

        [Test]
        public void GetAllTreatsEmptyStringAndNullTheSame()
        {
        }
    }
}
