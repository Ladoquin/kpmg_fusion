﻿namespace FlightDeck.Services.Tests
{
    using NUnit.Framework;
    using System.Linq;
    using System.Text;

    [TestFixture]
    public class UserPasswordEncrypterTests
    {
        [TestCase("foo", "foo", true)]
        [TestCase("foo", "bar", false)]
        [TestCase("AbCdEfG123!!", "AbCdEfG123!!", true)]
        [TestCase("AbCdEfG123!!", "abcdefg123!!", false)]
        public void TestEncryptionIsConsistent(string pwd1, string pwd2, bool passwordsMatch)
        {
            var encrypted1 = new UserPasswordEncrypter().GetEncrypted(pwd1);
            var encrypted2 = new UserPasswordEncrypter().GetEncrypted(pwd2, encrypted1.Salt);

            var actual = encrypted1.EncryptedPassword.SequenceEqual(encrypted2.EncryptedPassword);

            Assert.AreEqual(passwordsMatch, actual);
        }

        [Test]
        public void TestEncryptionReturnsEncryptedPassword()
        {
            var svc = new UserPasswordEncrypter();

            var actual = svc.GetEncrypted("foo");

            Assert.NotNull(actual);
            Assert.NotNull(actual.EncryptedPassword);
            Assert.AreNotEqual(0, actual.EncryptedPassword.Length);
            Assert.False(string.Equals("foo", Encoding.UTF8.GetString(actual.EncryptedPassword)));
        }

        [Test]
        public void TestEncryptionReturnsSalt()
        {
            var svc = new UserPasswordEncrypter();

            var actual = svc.GetEncrypted("foo");

            Assert.NotNull(actual);
            Assert.NotNull(actual.Salt);
            Assert.AreNotEqual(0, actual.Salt.Length);
        }
    }
}
