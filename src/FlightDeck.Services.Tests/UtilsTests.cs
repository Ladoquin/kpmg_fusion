﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.DomainShared;
    using NUnit.Framework;
    
    [TestFixture]
    public class UtilsTests
    {
        [TestCase(0, 0)]
        [TestCase(0.0000000, 0.0000000)]
        [TestCase(1, 1)]
        [TestCase(1.0, 1.0)]
        [TestCase(100000000, 100000000.000000000)]
        [TestCase(123.456, 123.456)]
        [TestCase(double.MaxValue, double.MaxValue)]
        [TestCase(double.MinValue, double.MinValue)]
        [TestCase(-1, -1)]
        [TestCase(-1.0, -1.0)]
        [TestCase(-123.456, -123.456)]
        [TestCase(-double.MaxValue, -double.MaxValue)]
        [TestCase(-double.MinValue, -double.MinValue)]
        public void TestEqualityCheckIsTrueWithExactPrecision(double a, double b)
        {
            Assert.True(Utils.EqualityCheck(a, b, Utils.TestPrecisionType.Exact));
        }

        [TestCase(1, 0)]
        [TestCase(1, -1)]
        [TestCase(100000000, 100000000.0000001)]
        [TestCase(-100000000, -100000000.0000001)]
        public void TestEqualityCheckIsFalseWithExactPrecision(double a, double b)
        {
            Assert.False(Utils.EqualityCheck(a, b, Utils.TestPrecisionType.Exact));
        }

        [TestCase(0, 0)]
        [TestCase(0.0000000, 0.0000000)]
        [TestCase(1, 1)]        
        [TestCase(1.9, 1.99)]
        [TestCase(1.1, 1.4)]
        [TestCase(-1, -1)]
        [TestCase(-1.9, -1.99)]
        [TestCase(-1.1, -1.4)]
        public void TestEqualityCheckIsTrueWithToPoundsPrecision(double a, double b)
        {
            Assert.True(Utils.EqualityCheck(a, b, Utils.TestPrecisionType.ToPounds));
        }

        [TestCase(0.9, 0.4)]
        [TestCase(0.9, 1.9)]
        [TestCase(-100000.4999, -100000.5001)]
        public void TestEqualityCheckIsFalseWithToPoundsPrecision(double a, double b)
        {
            Assert.False(Utils.EqualityCheck(a, b, Utils.TestPrecisionType.ToPounds));
        }

        [TestCase(0.001, 1)]
        [TestCase(10000.001, 10000)]
        [TestCase(10001.23, 10002.23)]
        [TestCase(-10001.23, -10002.23)]     
        public void TestEqualityCheckIsTrueWithToWithinAPoundPrecision(double a, double b)
        {
            Assert.True(Utils.EqualityCheck(a, b, Utils.TestPrecisionType.ToWithinAPound));
        }

        [TestCase(10000.99, 10002)]
        [TestCase(10001.23, 10002.24)]
        [TestCase(-10001.23, -10002.24)]   
        public void TestEqualityCheckIsFalseWithToWithinAPoundPrecision(double a, double b)
        {
            Assert.False(Utils.EqualityCheck(a, b, Utils.TestPrecisionType.ToWithinAPound));
        }

        [Test]
        public void TestGetObjectPropertyValues()
        {
            var instance = new FlexibleReturnOptions(0.5, 0.6);

            var actual = Utils.GetObjectPropertyValues<FlexibleReturnOptions>(instance);

            Assert.AreEqual(2, actual.Keys.Count);
            Assert.True(actual.ContainsKey("EquivalentTansferValue"));
            Assert.True(actual.ContainsKey("TakeUpRate"));
            Assert.AreEqual(0.5, (double)actual["EquivalentTansferValue"]);
            Assert.AreEqual(0.6, (double)actual["TakeUpRate"]);
        }
    }
}
