﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace FlightDeck.Services.Tests
{
    [TestFixture]
    public class EncryptionTests
    {
        private string _inputString = "Plain String";

        [Test]
        public void EncryptingString()
        {
            // Arrange
            var encryption = new EncryptionService();

            // Act
            var encryptedString = encryption.Encrypt(_inputString);

            // Assert
            Assert.AreNotEqual(encryptedString, _inputString);
        }

        [Test]
        public void DecryptedStringMatchesInputString()
        {
            // Arrange
            var encryption = new EncryptionService();

            // Act
            var encryptedString = encryption.Encrypt(_inputString);
            var decryptedString = encryption.Decrypt(encryptedString);

            // Assert
            Assert.AreEqual(_inputString, decryptedString);
        }
    }
}
