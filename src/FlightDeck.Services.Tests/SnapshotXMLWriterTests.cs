﻿namespace FlightDeck.Services.Tests
{
    using FlightDeck.ServiceInterfaces;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    
    [TestFixture]
    public class SnapshotXMLWriterTests
    {
        [Test]
        public void TestWriteSnapshot()
        {
            var propertyTree = new PropertyTreeItem("Space01",
                new Dictionary<string, object>
                    {
                        { "PropB", "valueB" },
                        { "PropA", "valueA" }
                    },
                new List<PropertyTreeItem>
                    {
                        new PropertyTreeItem("ChildA", new Dictionary<string, object> { { "SubProp-B", 2 }, { "SubProp-A", 100 } }),
                        new PropertyTreeItem("ChildB", new Dictionary<string, object> { { "AnotherSubProp-A", 223 }, { "AnotherSubProp-B", 123500 } })
                    });

            var actual = XDocument.Load(new SnapshotXDocWriter().WriteSnapshot(propertyTree));

            Assert.NotNull(actual);
            Assert.AreEqual("Space01", actual.Root.Name.LocalName);
            Assert.AreEqual("PropA", actual.Root.Elements().Skip(2).First().Name.LocalName); // ChildA and ChildB come before alphabetically
            Assert.AreEqual("valueB", actual.Root.Elements().Last().Value);
            Assert.AreEqual(2, actual.Root.Elements().First().Elements().Count());
            Assert.AreEqual("100", actual.Root.Elements().First().Elements().First().Value);
        }

        [Test]
        public void TestWriteSnapshotWithNulls()
        {
            var propertyTree = new PropertyTreeItem("Space01",
                null,
                null);

            var actual = XDocument.Load(new SnapshotXDocWriter().WriteSnapshot(propertyTree));

            Assert.NotNull(actual);
            Assert.AreEqual("Space01", actual.Root.Name.LocalName);
            Assert.AreEqual(0, actual.Root.Elements().Count());
        }

        [Test]
        public void TestWriteSnapshotHandlesSpacesInKeys()
        {
            var propertyTree = new PropertyTreeItem("Space01",
                new Dictionary<string, object> { { "item 1", "a value" } },
                new List<PropertyTreeItem> 
                    {
                        new PropertyTreeItem("a key", null, null)
                    });

            var actual = XDocument.Load(new SnapshotXDocWriter().WriteSnapshot(propertyTree));

            Assert.NotNull(actual);
            Assert.AreEqual(2, actual.Root.Elements().Count());
            Assert.AreEqual("a value", actual.Root.Elements().Last().Value);
        }

        [Test]
        public void TestWriteSnapshotHandlesSpecialCharacters()
        {
            var propertyTree = new PropertyTreeItem("Space01", 
                new Dictionary<string, object> { { "item 1*", "@ <> £ value!!" } },
                null);

            var actual = XDocument.Load(new SnapshotXDocWriter().WriteSnapshot(propertyTree));

            Assert.NotNull(actual);
            Assert.AreEqual(1, actual.Root.Elements().Count());
            Assert.AreEqual("@ <> £ value!!", actual.Root.Elements().Last().Value);
        }
    }
}
