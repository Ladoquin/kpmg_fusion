﻿using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightDeck.Services.Tests
{
    [TestFixture]
    public class ApplicationSettingsTests
    {
        [Test]
        public void GetConsultantDefaultSchemeName_should_return_a_string()
        {
            var appVal = new ApplicationValue
            {
                Key = "testKey",
                Value = "testValue"
            };

            var moqAppRepo = new Mock<IApplicationRepository>();
            moqAppRepo.Setup(repo => repo.GetByKey(It.IsAny<string>())).Returns(appVal);

            var moqDbScope = new Mock<IDbInteractionScope>();
            moqDbScope.Setup(db => db.GetRepository<ApplicationValue>()).Returns(moqAppRepo.Object);
            
            var appSettings = new ApplicationSettings();

            appSettings.DbInteractionScope = moqDbScope.Object;


            var schemeName = appSettings.GetConsultantDefaultSchemeName();

            Assert.IsNotNull(schemeName);
            Assert.IsTrue(schemeName == "testValue");
        }
    }
}
