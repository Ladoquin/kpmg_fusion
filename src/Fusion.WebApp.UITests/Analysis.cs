﻿namespace Fusion.WebApp.UITests
{
    using Fusion.WebApp.UITests.TestValues;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Interactions;
    using System;
    using System.Collections.Generic;

    [TestFixture]
    public class Analysis
    {
        FirefoxDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new FirefoxDriver();

            driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TearDown()
        {
            driver.Logout();

            driver.Quit();
        }

        [Test]
        public void Space01Test()
        {
            driver.Login();

            driver.SelectScheme(Space01.Name);

            driver.GoToAnalysis();

            #region Accounting

            driver.SelectBasis(Space01.Accounting.Name);
            
            driver.SelectAnalysisEndDate(Space01.DateX);
            
            Assert.AreEqual(Space01.Accounting.ValuesAtX.Balance, driver.FindElement(By.Id("analysisPeriodBalance")).Text);
   
            AssertCashLens
            (
                Space01.Accounting.ValuesAtX.AssetTotal, 
                Space01.Accounting.ValuesAtX.LiabilityTotal, 
                Space01.Accounting.ValuesAtX.Balance,
                new RecoveryPlanValues(new YearValue[] 
                {
                    new YearValue("2014", "£1M"),
                    new YearValue("2015", "£1M"),
                    new YearValue("2016", "£1M"),
                    new YearValue("2017", "£750K"),
                    new YearValue("2018", "£0") { Visible = false },
                    new YearValue("2019", "£0") { Visible = false }
                })
            );

            AssertTimeLens
            (
                new JourneyPlanValues(new JourneyPlanDataItem[] 
                {
                    new JourneyPlanDataItem("2014", "88%", "£110M", "£97.2M"),
                    new JourneyPlanDataItem("2015", "90%", "£108M", "£96.9M"),
                    new JourneyPlanDataItem("2016", "91%", "£106M", "£96.3M"),
                    new JourneyPlanDataItem("2017", "92%", "£103M", "£95M"), 
                    new JourneyPlanDataItem("2018", "93%", "£101M", "£93.9M"),
                    new JourneyPlanDataItem("2019", "93%", "£98.4M", "£91.9M"),
                    new JourneyPlanDataItem("2020", "94%", "£95.6M", "£89.5M") 
                }), 
                new CashflowValues(
                    new CashflowDataItem("2021", "£372K", "£374K", "£6.37M", "£7.11M"),
                    new CashflowDataItem("2031", "£478K", "£585K", "£5.08M", "£6.14M"))
            );

            AssertRiskLens
            (
                new BeforeAfter
                (
                    new string[] { "£26.2M", "£10.2M", "£5.82M", "£1.09M", "£25.5M", "£3.54M", "£29.4M", "£31.3M" }
                ),
                new FunnelValues
                (
                    new string[] { "2014", "2017", "2020" },
                    new FunnelLines()
                        {
                            Perc95 = new FunnelLine("£12.8M deficit", "£22.8M surplus", "£38.2M surplus"),
                            Perc75 = new FunnelLine("£12.8M deficit", "£4.35M surplus", "£12.1M surplus"),
                            Perc50 = new FunnelLine("£12.8M deficit", "£8.5M deficit", "£6.05M deficit"),
                            Perc25 = new FunnelLine("£12.8M deficit", "£21.3M deficit", "£24.2M deficit"),
                            Perc05 = new FunnelLine("£12.8M deficit", "£39.8M deficit", "£50.3M deficit"),
                        }
                )
            );

            AssertAccountingLens
            (
                Space01.DateX,
                new string[] { "£97.2M", "£110M", "£12.8M" },
                new BeforeAfter(new string[] { "£176K", "£972K", "£5.09M", "£4.52M", "£1.71M" }),
                new BeforeAfter(new string[] { "£176K", "£0", "£5.09M", "£5.48M", "£218K" })
            );
                
            AssertLiablityPanel(Space01.Accounting.ValuesAtX.LiabilityTotal, Space01.Accounting.ValuesAtX.LiabilityBreakdown, Space01.Accounting.ValuesAtX.LiabilityAssumptions, "£240K", "17.20%", "5.00%", "12.20%");
            AssertAssetPanel(Space01.Accounting.ValuesAtX.AssetTotal, Space01.Accounting.ValuesAtX.AssetBreakdown, Space01.Accounting.ValuesAtX.InvestmentReturns);
            AssertRecoveryPlanPanel("0.50%", "5.88%", "2.42%", "4.77%", "1.31%", "3.46%", "5", "0.00%", "0", "12");            
            AssertInvestmentStrategyPanel(Space01.Accounting.ValuesAtX.AssetTotal, addToArray(Space01.Accounting.ValuesAtX.AssetBreakdown, new { idx = 5, val = "Property (£0)" }), "2.42%", "2.42%", "0.00%", "1.31%", "3.46%",
                new InvestmentMix()
                {
                    DGFs = new PercentAmout("11.11%", "£9.94M"),
                    Equities = new PercentAmout("50.00%", "£44.7M"),
                    Property = new PercentAmout("0.00%", "£0"),
                    CorporateBonds = new PercentAmout("11.11%", "£9.94M"),
                    FixedIntersetGilts = new PercentAmout("16.67%", "£14.9M"),
                    IndexLinkedGilts = new PercentAmout("5.56%", "£4.97M"),
                    Cash = new PercentAmout("5.56%", "£4.97M"),
                    Subtotal = new PercentAmout { Amount = "£89.4M", Percent = "92.00%" },
                    Buyin = new PercentAmout { Amount = "£7.78M", Percent = "8.00%" },
                    Total = "£97.2M"
                });
            AssertFROPanel("£4.01M", "80.00%", "£3.21M", "0.00%", "£0", "£0", "£0");
            AssertETVPanel("£4.15M", "80.00%", "£3.32M", "30.00%", "£4.32M", "0.00%", "£0", "£0", "£0", "£0", "£0");
            AssertPIEPanel("£91.5M", "0.00%", "£0", "£0", "£0", "80.00%", "0.00%", "£91.5M", "£91.5M", "£0");
            AssertFutureBenefitsPanel("0.00%", "£240K", "17.20%", "5.00%", "12.20%", "£0", "-0.00%", "£0");
            AssertABFPanel("£12.8M", "10 years", "£1.6M", "£5.04M", "0.00%", "£0", "20 years", "£0", "£5.04M", "£0");

            #endregion
            
            #region Technical Provisions

            driver.SelectBasis(Space01.TechnicalProvisions.Name);

            driver.SelectAnalysisEndDate(Space01.DateX);

            Assert.AreEqual(Space01.TechnicalProvisions.ValuesAtX.Balance, driver.FindElement(By.Id("analysisPeriodBalance")).Text);

            AssertCashLens
            (
                Space01.TechnicalProvisions.ValuesAtX.AssetTotal,
                Space01.TechnicalProvisions.ValuesAtX.LiabilityTotal,
                Space01.TechnicalProvisions.ValuesAtX.Balance,
                new RecoveryPlanValues(new YearValue[] 
                {
                    new YearValue("2014", "£1M"),
                    new YearValue("2015", "£1M"),
                    new YearValue("2016", "£1M"),
                    new YearValue("2017", "£750K"),
                    new YearValue("2018", "£0") { Visible = false },
                    new YearValue("2019", "£0") { Visible = false }
                })
            );

            AssertTimeLens
            (
                new JourneyPlanValues(new JourneyPlanDataItem[] 
                {
                    new JourneyPlanDataItem("2014", "76%", "£129M", "£98.6M"),
                    new JourneyPlanDataItem("2015", "77%", "£127M", "£97.7M"),
                    new JourneyPlanDataItem("2016", "78%", "£124M", "£97.3M"),
                    new JourneyPlanDataItem("2017", "79%", "£122M", "£96.4M"), 
                    new JourneyPlanDataItem("2018", "80%", "£119M", "£94.8M"),
                    new JourneyPlanDataItem("2019", "80%", "£116M", "£92.8M"),
                    new JourneyPlanDataItem("2020", "80%", "£113M", "£90.5M")
                }),
                new CashflowValues(
                    new CashflowDataItem("2021", "£439K", "£718K", "£6.41M", "£7.56M"),
                    new CashflowDataItem("2031", "£477K", "£617K", "£5.33M", "£6.42M"))
            );

            AssertRiskLens
            (
                new BeforeAfter
                (
                    new string[] { "£30.7M", "£12M", "£5.87M", "£1.05M", "£25.5M", "£3.54M", "£33.2M", "£33.7M" }
                ),
                new FunnelValues
                (
                    new string[] { "2014", "2017", "2020" },
                    new FunnelLines()
                    {
                        Perc95 = new FunnelLine("£30.5M deficit", "£8.16M surplus", "£25.2M surplus"),
                        Perc75 = new FunnelLine("£30.5M deficit", "£11.7M deficit", "£2.89M deficit"),
                        Perc50 = new FunnelLine("£30.5M deficit", "£25.5M deficit", "£22.4M deficit"),
                        Perc25 = new FunnelLine("£30.5M deficit", "£39.3M deficit", "£41.9M deficit"),
                        Perc05 = new FunnelLine("£30.5M deficit", "£59.2M deficit", "£70M deficit"),
                    }
                )
            );

            AssertLiablityPanel(Space01.TechnicalProvisions.ValuesAtX.LiabilityTotal, Space01.TechnicalProvisions.ValuesAtX.LiabilityBreakdown, Space01.TechnicalProvisions.ValuesAtX.LiabilityAssumptions, "£284K", "20.70%", "5.00%", "15.70%");
            AssertAssetPanel(Space01.TechnicalProvisions.ValuesAtX.AssetTotal, Space01.TechnicalProvisions.ValuesAtX.AssetBreakdown, Space01.TechnicalProvisions.ValuesAtX.InvestmentReturns);
            AssertRecoveryPlanPanel("0.50%", "5.88%", "2.42%", "3.85%", "0.39%", "3.46%", "5", "0.00%", "0", "12");
            AssertInvestmentStrategyPanel(Space01.TechnicalProvisions.ValuesAtX.AssetTotal, addToArray(Space01.TechnicalProvisions.ValuesAtX.AssetBreakdown, new { idx = 5, val = "Property (£0)" }, new { idx = 6, val = "Asset Backed Fund (£0)" }), "2.42%", "2.42%", "0.00%", "0.39%", "3.46%",
                new InvestmentMix()
                {
                    DGFs = new PercentAmout("11.11%", "£9.94M"),
                    Equities = new PercentAmout("50.00%", "£44.7M"),
                    Property = new PercentAmout("0.00%", "£0"),
                    CorporateBonds = new PercentAmout("11.11%", "£9.94M"),
                    FixedIntersetGilts = new PercentAmout("16.67%", "£14.9M"),
                    IndexLinkedGilts = new PercentAmout("5.56%", "£4.97M"),
                    Cash = new PercentAmout("5.56%", "£4.97M"),
                    ABF = new PercentAmout("0.00%", "£0"),
                    Subtotal = new PercentAmout { Amount = "£89.4M", Percent = "90.69%" },
                    Buyin = new PercentAmout { Amount = "£9.18M", Percent = "9.31%" },
                    Total = "£98.6M"
                });
            AssertFROPanel("£4.59M", "80.00%", "£3.67M", "0.00%", "£0", "£0", "£0");
            AssertETVPanel("£4.45M", "80.00%", "£3.56M", "30.00%", "£4.63M", "0.00%", "£0", "£0", "£0", "£0", "£0");
            AssertPIEPanel("£108M", "0.00%", "£0", "£0", "£0", "80.00%", "0.00%", "£108M", "£108M", "£0");
            AssertFutureBenefitsPanel("0.00%", "£284K", "20.70%", "5.00%", "15.70%", "£0", "-0.00%", "£0");
            AssertABFPanel("£30.5M", "10 years", "£3.66M", "£11.6M", "0.00%", "£0", "20 years", "£0", "£11.6M", "£0");

            #endregion

            #region Buyout

            driver.SelectBasis(Space01.Buyout.Name);

            driver.SelectAnalysisEndDate(Space01.DateX);

            Assert.AreEqual(Space01.Buyout.ValuesAtX.Balance, driver.FindElement(By.Id("analysisPeriodBalance")).Text);

            AssertCashLens
            (
                Space01.Buyout.ValuesAtX.AssetTotal,
                Space01.Buyout.ValuesAtX.LiabilityTotal,
                Space01.Buyout.ValuesAtX.Balance,
                new RecoveryPlanValues(new YearValue[] 
                {
                    new YearValue("2014", "£1M"),
                    new YearValue("2015", "£1M"),
                    new YearValue("2016", "£1M"),
                    new YearValue("2017", "£750K"),
                    new YearValue("2018", "£0") { Visible = false },
                    new YearValue("2019", "£0") { Visible = false }
                })
            );

            AssertTimeLens
            (
                new JourneyPlanValues(new JourneyPlanDataItem[] 
                {
                    new JourneyPlanDataItem("2014", "63%", "£160M", "£101M"),
                    new JourneyPlanDataItem("2015", "63%", "£159M", "£100M"),
                    new JourneyPlanDataItem("2016", "63%", "£157M", "£99.6M"),
                    new JourneyPlanDataItem("2017", "63%", "£155M", "£98.5M"), 
                    new JourneyPlanDataItem("2018", "63%", "£153M", "£97.1M"),
                    new JourneyPlanDataItem("2019", "63%", "£151M", "£94.6M"),
                    new JourneyPlanDataItem("2020", "62%", "£148M", "£91.7M")
                }),
                new CashflowValues(
                    new CashflowDataItem("2021", "£451K", "£570K", "£7.1M", "£8.12M"),
                    new CashflowDataItem("2031", "£776K", "£995K", "£6.64M", "£8.41M"))
            );

            AssertRiskLens
            (
                new BeforeAfter
                (
                    new string[] { "£38.3M", "£14.9M", "£5.94M", "£1.01M", "£25.5M", "£3.54M", "£38.7M", "£38.6M" }
                ),
                new FunnelValues
                (
                    new string[] { "2014", "2017", "2020" },
                    new FunnelLines()
                    {
                        Perc95 = new FunnelLine("£59.7M deficit", "£18.2M deficit", "£1.66M deficit"),
                        Perc75 = new FunnelLine("£59.7M deficit", "£41M deficit", "£33.9M deficit"),
                        Perc50 = new FunnelLine("£59.7M deficit", "£56.9M deficit", "£56.3M deficit"),
                        Perc25 = new FunnelLine("£59.7M deficit", "£72.7M deficit", "£78.7M deficit"),
                        Perc05 = new FunnelLine("£59.7M deficit", "£95.5M deficit", "£111M deficit"),
                    }
                )
            );

            AssertLiablityPanel(Space01.Buyout.ValuesAtX.LiabilityTotal, Space01.Buyout.ValuesAtX.LiabilityBreakdown, Space01.Buyout.ValuesAtX.LiabilityAssumptions, "£436K", "31.00%", "5.00%", "26.00%");
            AssertAssetPanel(Space01.Buyout.ValuesAtX.AssetTotal, Space01.Buyout.ValuesAtX.AssetBreakdown, Space01.Buyout.ValuesAtX.InvestmentReturns);
            AssertRecoveryPlanPanel("0.50%", "5.88%", "2.42%", "3.54%", "0.08%", "3.46%", "5", "0.00%", "0", "12");
            AssertInvestmentStrategyPanel(Space01.Buyout.ValuesAtX.AssetTotal, addToArray(Space01.Buyout.ValuesAtX.AssetBreakdown, new { idx = 5, val = "Property (£0)" }, new { idx = 6, val = "Asset Backed Fund (£0)" }), "2.42%", "2.42%", "0.00%", "0.08%", "3.46%",
                new InvestmentMix()
                {
                    DGFs = new PercentAmout("11.11%", "£9.95M"),
                    Equities = new PercentAmout("50.00%", "£44.8M"),
                    Property = new PercentAmout("0.00%", "£0"),
                    CorporateBonds = new PercentAmout("11.11%", "£9.95M"),
                    FixedIntersetGilts = new PercentAmout("16.67%", "£14.9M"),
                    IndexLinkedGilts = new PercentAmout("5.56%", "£4.98M"),
                    Cash = new PercentAmout("5.56%", "£4.98M"),
                    ABF = new PercentAmout("0.00%", "£0"),
                    Subtotal = new PercentAmout { Amount = "£89.6M", Percent = "89.11%" },
                    Buyin = new PercentAmout { Amount = "£10.9M", Percent = "10.89%" },
                    Total = "£101M"
                });
            AssertFROPanel("£6.65M", "80.00%", "£5.32M", "0.00%", "£0", "£0", "£0");
            AssertETVPanel("£7.84M", "80.00%", "£6.27M", "30.00%", "£8.16M", "0.00%", "£0", "£0", "£0", "£0", "£0");
            AssertPIEPanel("£127M", "0.00%", "£0", "£0", "£0", "80.00%", "0.00%", "£127M", "£127M", "£0");
            AssertFutureBenefitsPanel("0.00%", "£436K", "31.00%", "5.00%", "26.00%", "£0", "0.00%", "£0");
            AssertABFPanel("£59.7M", "10 years", "£7.06M", "£22.3M", "0.00%", "£0", "20 years", "£0", "£22.3M", "£0");
            AssertInsurancePanel("£127M", "£33.4M", new PercentAmout("100%", "£127M"), new PercentAmout("0%", "£0"), new string[] { "£21.1M", "£30.9M", "£16.3M" }, new string[] { "£11.5M", "£12.3M", "£9.09M" }, new string[] { "£6.04M", "£6.85M", "£3.6M" }, "-£2.44M");

            #endregion

            /*
                        driver.MoveSlider("salaryIncreases", 100);
                        driver.MoveSlider("CPI", 100);
                        driver.MoveSlider("assumptionLife45", 100);

                        Assert.AreEqual("£64M", driver.FindElement(By.Id("analysisPeriodBalance")).Text);
                        AssertCashLens
                        (
                            "£100M",
                            "£164M",
                            "£64M",
                            new RecoveryPlanValues
                            (
                                new YearValue[] 
                                {
                                    new YearValue("2014", "£1M"),
                                    new YearValue("2015", "£1M"),
                                    new YearValue("2016", "£1M"),
                                    new YearValue("2017", "£750K"),
                                    new YearValue("2018", "£0") { Visible = false },
                                    new YearValue("2019", "£0") { Visible = false }
                                }
                            )
                            {
                                After = new YearValue[] 
                                {
                                    new YearValue("2014", "£1M"),
                                    new YearValue("2015", "£14.1M"),
                                    new YearValue("2016", "£14.1M"),
                                    new YearValue("2017", "£14.1M"),
                                    new YearValue("2018", "£14.1M"),
                                    new YearValue("2019", "£14.1M")
                                }
                            }
                        );
                   

            
                        AssertJourneyPlan(new JourneyPlanValues(new JourneyPlanDataItem[] {
                            new JourneyPlanDataItem("2014", "88%", "£110M", "£97.2M"),
                            new JourneyPlanDataItem("2015", "90%", "£108M", "£96.9M"),
                            new JourneyPlanDataItem("2016", "91%", "£106M", "£96.3M"),
                            new JourneyPlanDataItem("2017", "92%", "£103M", "£95M"), 
                            new JourneyPlanDataItem("2018", "93%", "£101M", "£93.9M"),
                            new JourneyPlanDataItem("2019", "93%", "£98.4M", "£91.9M"),
                            new JourneyPlanDataItem("2020", "94%", "£95.6M", "£89.5M") 
                        }));

                        var slider = driver.FindElement(By.Id("preRetirement"));

                        new Actions(driver).DragAndDropToOffset(slider, 10, 0).Perform();
                        */
        }

        #region Models

        private struct BeforeAfter
        {
            public string[] Before { get; set; }
            public string[] After { get; set; }
            public BeforeAfter(string[] before)
                : this()
            {
                Before = before;
            }
            public BeforeAfter(string[] before, string[] after)
                : this()
            {
                Before = before;
                After = after;
            }
        }
        private struct PercentAmout
        {
            public string Percent { get; set; }
            public string Amount { get; set; }
            public PercentAmout(string percent, string amount)
                : this()
            {
                Percent = percent;
                Amount = amount;
            }
        }
        private struct YearValue
        {
            public string Year { get; set; }
            public string Value { get; set; }
            public bool Visible { get; set; }
            public YearValue(string year, string value)
                : this()
            {
                Year = year;
                Value = value;
                Visible = true;
            }
        }
        private struct CashflowDataItem
        {
            public string Year { get; set; }
            public string Deferreds { get; set; }
            public string Actives { get; set; }
            public string Pensioners { get; set; }
            public string Total { get; set; }
            public CashflowDataItem(string year, string deferreds, string actives, string pensioners, string total)
                : this()
            {
                Year = year;
                Deferreds = deferreds;
                Actives = actives;
                Pensioners = pensioners;
                Total = total;
            }
        }
        private struct RecoveryPlanValues
        {
            public YearValue[] Before { get; set; }
            public YearValue[] After { get; set; }
            public RecoveryPlanValues(YearValue[] before)
                : this()
            {
                Before = before;
            }
        }
        private struct JourneyPlanDataItem
        {
            public string Year { get; set; }
            public string Percent { get; set; }
            public string Liabilities { get; set; }
            public string Assets { get; set; }
            public JourneyPlanDataItem(string year, string percent, string liabilities, string assets)
                : this()
            {
                Year = year;
                Percent = percent;
                Liabilities = liabilities;
                Assets = assets;
            }
        }
        private struct JourneyPlanValues
        {
            public JourneyPlanDataItem[] Before { get; set; }
            public JourneyPlanDataItem[] After { get; set; }
            public JourneyPlanValues(JourneyPlanDataItem[] before)
                : this()
            {
                Before = before;
            }
        }
        private struct CashflowValues
        {
            public CashflowDataItem Before2021 { get; set; }
            public CashflowDataItem Before2031 { get; set; }
            public CashflowDataItem? After2021 { get; set; }
            public CashflowDataItem? After2031 { get; set; }
            public CashflowValues(CashflowDataItem before2021, CashflowDataItem before2031)
                : this()
            {
                Before2021 = before2021;
                Before2031 = before2031;
            }
        }
        private struct FunnelLine
        {
            public string Year1Value { get; set; }
            public string Year2Value { get; set; }
            public string Year3Value { get; set; }
            public FunnelLine(string y1, string y2, string y3)
                : this()
            {
                Year1Value = y1;
                Year2Value = y2;
                Year3Value = y3;
            }
        }
        private struct FunnelLines
        {
            public FunnelLine Perc95 { get; set; }
            public FunnelLine Perc75 { get; set; }
            public FunnelLine Perc50 { get; set; }
            public FunnelLine Perc25 { get; set; }
            public FunnelLine Perc05 { get; set; }
        }
        private struct FunnelValues
        {
            public string[] Years { get; set; }
            public FunnelLines Before { get; set; }
            public FunnelLines After { get; set; }
            public FunnelValues(string[] years, FunnelLines before)
                : this()
            {
                Years = years;
                Before = before;
            }
        }
        private struct InvestmentMix
        {
            public PercentAmout DGFs { get; set; }
            public PercentAmout Equities { get; set; }
            public PercentAmout Property { get; set; }
            public PercentAmout CorporateBonds { get; set; }
            public PercentAmout FixedIntersetGilts { get; set; }
            public PercentAmout IndexLinkedGilts { get; set; }
            public PercentAmout Cash { get; set; }
            public PercentAmout? ABF { get; set; }
            public PercentAmout Subtotal { get; set; }
            public PercentAmout Buyin { get; set; }
            public string Total { get; set; }
        }
        #endregion

        #region Page components

        private void AssertTimeLens(JourneyPlanValues journeyPlanValues, CashflowValues cashflowValues)
        {
            SelectLens("Time");
            driver.FindElement(By.Id("timeLensTabs")).FindElement(By.TagName("ul")).FindElements(By.TagName("li"))[0].Click();
            AssertJourneyPlanChart(journeyPlanValues);
            driver.ScrollToTop();
            driver.FindElement(By.Id("timeLensTabs")).FindElement(By.TagName("ul")).FindElements(By.TagName("li"))[1].Click();
            AssertCashflowChart(cashflowValues);
        }

        private void AssertCashLens(string assetTotal, string liabilityTotal, string balance, RecoveryPlanValues recoveryPlanValues)
        {
            SelectLens("Cash");
            AssertFundingPositionChart(assetTotal, liabilityTotal, balance);
            AssertRecoveryPlanChart(recoveryPlanValues);
        }

        private void AssertRiskLens(BeforeAfter waterfallValues, FunnelValues funnelValues)
        {
            SelectLens("Risk");
            driver.FindElement(By.Id("riskLensTabs")).FindElement(By.TagName("ul")).FindElements(By.TagName("li"))[0].Click();
            AssertWaterfallChart(waterfallValues);
            driver.ScrollToTop();
            driver.FindElement(By.Id("riskLensTabs")).FindElement(By.TagName("ul")).FindElements(By.TagName("li"))[1].Click();
            AssertFunnelChart(funnelValues);
        }

        private void AssertAccountingLens(string anaylsisEndDate, string[] balanceSheetValues, BeforeAfter ias19Values, BeforeAfter frs17Values)
        {
            SelectLens("Accounting");
            Assert.AreEqual(string.Format("P&L forecast (1 year from {0})", anaylsisEndDate), driver.FindElement(By.CssSelector("div#accountingLensTabs > h4")).Text);
            AssertBalanceSheet(balanceSheetValues);
            AssertPandLForecasts(ias19Values, frs17Values);
        }

        private void SelectLens(string name)
        {
            var tabs = driver.FindElement(By.CssSelector("ul.tabMenu"));

            driver.ScrollToTop();

            tabs.FindElement(By.XPath("li/a[div/div/h2[text()='" + name + "']]")).Click();

            driver.WaitForAjaxCompletion();
        }

        private void AssertFundingPositionChart(string assets, string liabilities, string balance)
        {
            var chart = driver.FindElement(By.Id("fundingChartDiv"));

            Assert.AreEqual(assets, driver.FindElements(By.CssSelector("div.highcharts-container > div.highcharts-stack-labels > span"))[0].Text);
            Assert.AreEqual(liabilities, driver.FindElements(By.CssSelector("div.highcharts-container > div.highcharts-stack-labels > span"))[1].Text);
            Assert.AreEqual(balance, driver.FindElements(By.CssSelector("div.highcharts-container > div.highcharts-stack-labels > span"))[2].Text);
        }

        private void AssertRecoveryPlanChart(RecoveryPlanValues values)
        {
            var id = "recoveryPlanChartDiv";

            driver.ScrollToElement(id);

            var chart = driver.FindElement(By.Id(id));

            var tooltip = chart.FindElement(By.CssSelector("div.highcharts-container > div.highcharts-tooltip > span"));

            for (int i = 0; i < values.Before.Length; i++)
            {
                var expected = string.Format("Year {0}\r\n\r\nCurrent plan: {1}", values.Before[i].Year, values.Before[i].Value);

                if (values.After != null)
                {
                    expected += string.Format("\r\nNew plan: {0}", values.After[i].Value);
                }

                var bar = chart.FindElements(By.CssSelector("div.highcharts-container > svg > g.highcharts-series-group > g > rect"))[i];
                
                if (values.Before[i].Visible)
                {
                    bar.Click();
                    Assert.AreEqual(expected, tooltip.Text);
                }
                else
                {
                    Assert.IsFalse(bar.Displayed);
                }
            }
        }

        private void AssertJourneyPlanChart(JourneyPlanValues values)
        {
            var id = "journeyPlanChartDiv";

            driver.ScrollToElement(id);

            var chart = driver.FindElement(By.Id(id));

            var tooltip = chart.FindElement(By.CssSelector("div.highcharts-container > div.highcharts-tooltip > span"));

            for(int i = 0; i < values.Before.Length; i++)
            {
                var expected = string.Format("Year {0}\r\n\r\nBefore: {1}\r\nLiabilities: {2}\r\nAssets: {3}", values.Before[i].Year, values.Before[i].Percent, values.Before[i].Liabilities, values.Before[i].Assets);

                if (values.After != null)
                {
                    expected += string.Format("\r\nAfter: {0}\r\nLiabilities: {1}\r\nAssets: {2}", values.After[i].Year, values.After[i].Liabilities, values.After[i].Assets);
                }

                chart.FindElements(By.CssSelector("div.highcharts-container > svg > g.highcharts-series-group > g > rect"))[i].Click();

                Assert.AreEqual(expected, tooltip.Text);
            }
        }

        private void AssertCashflowChart(CashflowValues values)
        {
            var id = "cashflowChartDiv";

            driver.ScrollToElement(id);

            var chart = driver.FindElement(By.Id(id));

            var tooltip = chart.FindElement(By.CssSelector("div.highcharts-container > div.highcharts-tooltip > span"));

            var bar2021 = chart.FindElements(By.CssSelector("div.highcharts-container > svg > g.highcharts-series-group > g.highcharts-series"))[1].FindElements(By.TagName("rect"))[6];
            var bar2031 = chart.FindElements(By.CssSelector("div.highcharts-container > svg > g.highcharts-series-group > g.highcharts-series"))[1].FindElements(By.TagName("rect"))[16];

            Func<CashflowDataItem, CashflowDataItem?, string> toExpectedString = (before, after) =>
                {
                    var s = string.Format("Year {0}\r\n\r\nBefore:\r\n\r\nDeferreds: {1}\r\nActives: {2}\r\nPensioners: {3}\r\nTotal: {4}", before.Year, before.Deferreds, before.Actives, before.Pensioners, before.Total);
                    if (after != null)
                    {
                        s += string.Format("\r\n\r\nAfter:\r\n\r\nDeferreds: {0}\r\nActives: {1}\r\nPensioners: {2}\r\nTotal: {3}", after.Value.Deferreds, after.Value.Actives, after.Value.Pensioners, after.Value.Total);
                    }
                    return s;
                };

            var expected = toExpectedString(values.Before2021, values.After2021);
            bar2021.Click();
            Assert.AreEqual(expected, tooltip.Text);

            expected = toExpectedString(values.Before2031, values.After2031);
            bar2031.Click();
            Assert.AreEqual(expected, tooltip.Text);
        }

        private void AssertWaterfallChart(BeforeAfter values)
        {
            assertChartPointLabels("varWaterfallChartDiv", values.Before);
        }

        private void AssertFunnelChart(FunnelValues values)
        {
            var id = "varFunnelChartDiv";

            driver.ScrollToTop();

            var chart = driver.FindElement(By.Id(id));

            var tooltip = chart.FindElement(By.CssSelector("div.highcharts-container > div.highcharts-tooltip > span"));

            var lines = chart.FindElements(By.CssSelector("div.highcharts-container > svg > g.highcharts-series-group > g.highcharts-markers"));

            // starting point for all lines should be the same value
            // don't Assert the percentile it's picked to show in the tooltip as that's a bit random due to all the percentiles being on that spot
            // so just Assert the balance shown is as expected
            lines[0].FindElements(By.TagName("path"))[2].Click();
            Assert.IsTrue(tooltip.Text.IndexOf("Before: " + values.Before.Perc05.Year1Value) > 0);

            Action<int, string, string[], FunnelLine> assertLine = (lineIdx, name, years, yearValues) =>
                {
                    var points = lines[lineIdx].FindElements(By.TagName("path"));
                    points[0].Click();
                    Assert.AreEqual(string.Format("{0}\r\n\r\n{1}\r\n\r\nBefore: {2}", years[2], name, yearValues.Year3Value), tooltip.Text);
                    points[1].Click();
                    Assert.AreEqual(string.Format("{0}\r\n\r\n{1}\r\n\r\nBefore: {2}", years[1], name, yearValues.Year2Value), tooltip.Text);
                };

            assertLine(0, "95th percentile", values.Years, values.Before.Perc95);
            assertLine(1, "75th percentile", values.Years, values.Before.Perc75);
            assertLine(2, "Median", values.Years, values.Before.Perc50);
            assertLine(3, "25th percentile", values.Years, values.Before.Perc25);
            assertLine(4, "5th percentile", values.Years, values.Before.Perc05);
        }

        private void AssertBalanceSheet(string[] values)
        {
            assertStackedChartLabels("accountingBalanceChartDiv", values);
        }

        private void AssertPandLForecasts(BeforeAfter ias19values, BeforeAfter frs17values)
        {
            driver.FindElement(By.Id("accountingLensTabs")).FindElement(By.TagName("ul")).FindElements(By.TagName("li"))[0].Click();
            assertChartPointLabels("accountingIASChartDiv", ias19values.Before, false);
            driver.FindElement(By.Id("accountingLensTabs")).FindElement(By.TagName("ul")).FindElements(By.TagName("li"))[1].Click();
            assertChartPointLabels("accountingFRSChartDiv", frs17values.Before, false);
        }

        private void assertStackedChartLabels(string id, string[] values)
        {
            assertChartPointLabels(id, values, true);
        }

        private void assertChartPointLabels(string id, string[] values, bool isStacked = false)
        {
            driver.ScrollToTop();

            var chart = driver.FindElement(By.Id(id));

            var datalabels = isStacked
                ? chart.FindElements(By.CssSelector("div.highcharts-container > div.highcharts-stack-labels span"))
                : chart.FindElements(By.CssSelector("div.highcharts-container > div.highcharts-data-labels div span"));

            Assert.AreEqual(values.Length, datalabels.Count);

            for (int i = 0; i < values.Length; i++)
            {
                Assert.AreEqual(values[i], datalabels[i].Text);
            }
        }

        private void SelectPanel(string name)
        {
            driver.ScrollToTop();
            driver.FindElement(By.XPath("//ul[contains(concat(' ', @class, ' '), 'tabMenu')]/li/a[text()='" + name + "']")).Click();
        }

        private void AssertLiablityPanel(string pieTotal, string[] pieBreakdown, string[,] assumptions, string annualCost, string contributionRate, string employeeRate, string employerRate)
        {
            SelectPanel("Liability assumptions");
            driver.AssertValuesTable("liabilityAssumptionsTable", assumptions, TDIdxOfValue: 1);
            driver.AssertPie("liabilityBreakdownPie", pieTotal, pieBreakdown);
            driver.AssertValuesTable(driver.FindElement(By.CssSelector("table.futureServiceRelated")), new string[] { annualCost, contributionRate, employeeRate, employerRate }, TDIdxOfValue: 1);
        }

        private void AssertAssetPanel(string pieTotal, string[] pieBreakdown, string[,] investmentReturns)
        {
            SelectPanel("Asset assumptions");
            driver.AssertPie("assetBreakdownPie", pieTotal, pieBreakdown);
            driver.AssertValuesTable("assetAssumptionsTable", investmentReturns, TDIdxOfValue: 1);
        }

        private void AssertRecoveryPlanPanel(string discountRate, string weightedAR, string weightedARRelative, string weighted1Yr, string weighted1YrRelative, string FTSE, string length, string increase, string lumpSum, string commencement)
        {
            SelectPanel("Recovery plan");
            Assert.AreEqual(discountRate, driver.FindElement(By.CssSelector("#outperformanceTypeTable > tbody > tr:nth-child(2) > td.values")).Text);
            driver.AssertValuesTable(
                driver.FindElement(By.CssSelector("#analysisdashboard_recoveryplan > div > div.tabPanelColumn.left.fifty > div > table:nth-child(6)")),
                new string[] { weightedAR, weightedARRelative, weighted1Yr, weighted1YrRelative, FTSE },
                TDIdxOfValue: 1);
            driver.AssertValuesTable("recoveryPlanTable", new string[] { length, increase, lumpSum, commencement }, TDIdxOfValue: 1);
        }

        private void AssertInvestmentStrategyPanel(string assetTotal, string[] assetBreakdown, string currentWAR, string newWAR, string change, string weighted1yr, string FTSE, InvestmentMix mix)
        {
            SelectPanel("Investment strategy");
            
            driver.AssertPie("assetMixPie", assetTotal, assetBreakdown);
            
            Assert.AreEqual(currentWAR, driver.FindElement(By.Id("originalAssetReturn")).Text);
            Assert.AreEqual(newWAR, driver.FindElement(By.Id("investmentStrategryWeightedAssetReturn")).Text);
            Assert.AreEqual(change, driver.FindElement(By.Id("changeInExpectedReturn")).Text);
            Assert.AreEqual(weighted1yr, driver.FindElement(By.Id("investmentStrategyWeightedDiscountRateRelative")).Text);
            Assert.AreEqual(FTSE, driver.FindElement(By.CssSelector("#investmentStrategy_refGiltYieldRow > td.values")).Text);
            
            var mixTableTRs = driver.FindElements(By.CssSelector("table#assetMixTable > tbody > tr")); // Are the assets in this table fixed? If not, will have to re-write this....
            Action<PercentAmout, int> assertMixRow = (value, i) =>
                {
                    Assert.AreEqual(value.Percent, mixTableTRs[i].FindElements(By.TagName("td"))[1].Text);
                    Assert.AreEqual(value.Amount, mixTableTRs[i].FindElements(By.TagName("td"))[2].Text);
                };            
            assertMixRow(mix.DGFs, 0);
            assertMixRow(mix.Equities, 1);
            assertMixRow(mix.Property, 3);
            assertMixRow(mix.CorporateBonds, 6);
            assertMixRow(mix.FixedIntersetGilts, 7);
            assertMixRow(mix.IndexLinkedGilts, 9);
            assertMixRow(mix.Cash, 11);
            if (mix.ABF != null)
            {
                assertMixRow(mix.ABF.Value, 12);
            }
            else
            {
                Assert.IsFalse(mixTableTRs[12].Displayed);
            }
            Assert.AreEqual(mix.Subtotal.Amount, driver.FindElement(By.CssSelector("#assetMixTable > tfoot > tr > td:nth-child(4)")).Text);
            Assert.AreEqual(mix.Subtotal.Percent, driver.FindElement(By.Id("assetMixNonBuyinProportion")).Text);
            Assert.AreEqual(mix.Buyin.Amount, driver.FindElement(By.Id("assetMixExistingBuyin")).Text);
            Assert.AreEqual(mix.Buyin.Amount, driver.FindElement(By.Id("assetMixTotalBuyin")).Text);
            Assert.AreEqual(mix.Buyin.Percent, driver.FindElement(By.Id("assetMixTotalBuyinProportion")).Text);
            Assert.AreEqual(mix.Total, driver.FindElement(By.Id("assetMixTotalAssets")).Text);
        }

        private void AssertFROPanel(string deferredLiability, string sizeOfTransfer, string equivalentTransfer, string takeup, string transferValue, string liabilityDischarge, string change)
        {
            SelectPanel("FRO");
            driver.AssertValuesTable("froInputTable", new string[] { deferredLiability, sizeOfTransfer, equivalentTransfer, takeup }, TDIdxOfValue: 1);
            Assert.AreEqual(transferValue, driver.FindElement(By.Id("froCetvPaid")).Text);
            Assert.AreEqual(liabilityDischarge, driver.FindElement(By.Id("froLiabilityDischarged")).Text);
            Assert.AreEqual(change, driver.FindElement(By.Id("froFundingImprovement")).Text);
        }

        private void AssertETVPanel(string deferredLiability, string sizeOfTransfer, string equivalentTransfer, string enhancementToTransfer, string equivalentETV, string takeup, string transferPaid, string ETVpaid, string liabilityDischarge, string cost, string change)
        {
            SelectPanel("ETV");
            driver.AssertValuesTable("etvInputTable", new string[] { deferredLiability, sizeOfTransfer, equivalentTransfer, enhancementToTransfer, equivalentETV, takeup }, TDIdxOfValue: 1);
            Assert.AreEqual(transferPaid, driver.FindElement(By.Id("etvCetvPaid")).Text);
            Assert.AreEqual(ETVpaid, driver.FindElement(By.Id("etvEtvPaid")).Text);
            Assert.AreEqual(liabilityDischarge, driver.FindElement(By.Id("etvLiabilityDischarged")).Text);
            Assert.AreEqual(cost, driver.FindElement(By.Id("etvCostToCompany")).Text);
            Assert.AreEqual(change, driver.FindElement(By.Id("etvChangeInPosition")).Text);
        }

        private void AssertPIEPanel(string liability, string exchangePerc, string exchangeTotal, string zeroIncreases, string valueIncreases, string valueShared, string takeup, string before, string after, string change)
        {
            SelectPanel("PIE");
            driver.AssertValuesTable("pieInputTable", new string[] { liability, exchangePerc, exchangeTotal, zeroIncreases, valueIncreases, " " /* empty row */, valueShared, takeup }, TDIdxOfValue: 1);
            Assert.AreEqual(before, driver.FindElement(By.CssSelector("#pieOutputTable > tbody > tr:nth-child(1) > td.values.piePensionerLiability")).Text);
            Assert.AreEqual(after, driver.FindElement(By.Id("pieLiabilityAfterPie")).Text);
            Assert.AreEqual(change, driver.FindElement(By.Id("pieChangeInLiabilities")).Text);
        }

        private void AssertFutureBenefitsPanel(string memberContributions, string annualCost, string contributionRate, string employeeRate, string employerRate, string annualCostChange, string employerRateChange, string positionChange)
        {
            SelectPanel("Future benefits");
            Assert.AreEqual(memberContributions, driver.FindElement(By.CssSelector("#futureBenefitsInputTable > tbody > tr:nth-child(1) > td.values > span")).Text);

            var table = driver.FindElement(By.Id("futureBenefitsOutputTable"));
            Assert.AreEqual(annualCost, table.FindElement(By.XPath("tbody/tr[1]/td[2]")).Text);
            Assert.AreEqual(contributionRate, table.FindElement(By.XPath("tbody/tr[2]/td[2]")).Text);
            Assert.AreEqual(employeeRate, table.FindElement(By.XPath("tbody/tr[3]/td[2]")).Text);
            Assert.AreEqual(employerRate, table.FindElement(By.XPath("tbody/tr[4]/td[2]")).Text);
            Assert.AreEqual(annualCostChange, driver.FindElement(By.Id("futureBenefitsChangeInAnnualCost")).Text);
            Assert.AreEqual(employerRateChange, driver.FindElement(By.Id("futureBenefitsChangeInEmployerRate")).Text);
            Assert.AreEqual(positionChange, driver.FindElement(By.Id("futureBenefitsChangeInLiabilities")).Text);
        }

        private void AssertABFPanel(string deficit, string RPlength, string RPcontributions, string RPoutflow, string valuePerc, string valueTotal, string length, string annual, string outflow, string cummulative)
        {
            SelectPanel("ABF");
            Assert.AreEqual(deficit, driver.FindElement(By.Id("abfFundingDeficit")).Text);
            Assert.AreEqual(RPlength, driver.FindElement(By.Id("abfLengthOfRecoveryPlan")).Text);
            Assert.AreEqual(RPcontributions, driver.FindElement(By.Id("abfAnnualPayments")).Text);
            Assert.AreEqual(RPoutflow, driver.FindElement(By.Id("abfNetCashPosition")).Text);
            Assert.AreEqual(valuePerc, driver.FindElement(By.CssSelector("#abfInputTable > tbody > tr:nth-child(1) > td.values")).Text);
            Assert.AreEqual(valueTotal, driver.FindElement(By.Id("abfValueOfAbf")).Text);
            Assert.AreEqual(length, driver.FindElement(By.Id("abfLengthOfAbf")).Text);
            Assert.AreEqual(annual, driver.FindElement(By.Id("abfAnnualContribution")).Text);
            Assert.AreEqual(outflow, driver.FindElement(By.Id("abfNewNetCashPosition")).Text);
            Assert.AreEqual(cummulative, driver.FindElement(By.Id("abfCumulativeCashSaving")).Text);
        }

        private void AssertInsurancePanel(string penLiab, string nonpenLiab, PercentAmout penLiabInsured, PercentAmout nonpenLiabInsured, string[] vsAccounting, string[] vsFunding, string[] vsGilts, string impact)
        {
            SelectPanel("Insurance");
            Assert.AreEqual(penLiab, driver.FindElement(By.Id("td-pensioner-liability")).Text);
            Assert.AreEqual(nonpenLiab, driver.FindElement(By.Id("td-non-pensioner-liability")).Text);
            Assert.AreEqual(penLiabInsured.Percent, driver.FindElement(By.CssSelector("#insuranceInputTable > tbody > tr:nth-child(1) > td.values")).Text);
            Assert.AreEqual(penLiabInsured.Amount, driver.FindElement(By.Id("insurance-pensioner-liability-insured-amount")).Text);
            Assert.AreEqual(nonpenLiabInsured.Percent, driver.FindElement(By.CssSelector("#insuranceInputTable > tbody > tr:nth-child(3) > td.values")).Text);
            Assert.AreEqual(nonpenLiabInsured.Amount, driver.FindElement(By.Id("insurance-non-pensioner-liability-insured-amount")).Text);
            Assert.AreEqual(vsAccounting[0], driver.FindElement(By.Id("insurance-accounting-strain")).Text);
            Assert.AreEqual(vsAccounting[1], driver.FindElement(By.Id("insurance-accounting-max")).Text);
            Assert.AreEqual(vsAccounting[2], driver.FindElement(By.Id("insurance-accounting-min")).Text);
            Assert.AreEqual(vsFunding[0], driver.FindElement(By.Id("insurance-funding-strain")).Text);
            Assert.AreEqual(vsFunding[1], driver.FindElement(By.Id("insurance-funding-max")).Text);
            Assert.AreEqual(vsFunding[2], driver.FindElement(By.Id("insurance-funding-min")).Text);
            Assert.AreEqual(vsGilts[0], driver.FindElement(By.Id("insurance-gilts-strain")).Text);
            Assert.AreEqual(vsGilts[1], driver.FindElement(By.Id("insurance-gilts-max")).Text);
            Assert.AreEqual(vsGilts[2], driver.FindElement(By.Id("insurance-gilts-min")).Text);
            Assert.AreEqual(impact, driver.FindElement(By.Id("insurance-impact-of-50-basis")).Text);
        }

        #endregion

        private string[] addToArray(string[] source, params object[] insertions)
        {
            var s = new List<string>(source);
            foreach (var obj in insertions)
            {
                s.Insert(((dynamic)obj).idx, ((dynamic)obj).val);
            }
            return s.ToArray();
        }
    }
}
