﻿namespace Fusion.WebApp.UITests
{
    using Fusion.WebApp.UITests.TestValues;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Interactions;
    
    [TestFixture]
    public class Baseline
    {
        FirefoxDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new FirefoxDriver();

            driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TearDown()
        {
            driver.Logout();

            driver.Quit();
        }

        [Test]
        public void Space01Test()
        {
            driver.Login();

            driver.SelectScheme(Space01.Name);

            driver.GoToBaseline();

            var valuationResultTooltips = new string[,]
                {
                    { Space01.Accounting.Name, Space01.Accounting.ValuesAtAnchorDate.Balance },
                    { Space01.TechnicalProvisions.Name, Space01.TechnicalProvisions.ValuesAtAnchorDate.Balance },
                    { Space01.Buyout.Name, Space01.Buyout.ValuesAtAnchorDate.Balance }
                };

            /**************************
             * Accounting basis
             * *******/ 
            AssertBasis
            (
                Space01.Accounting.Name,
                Space01.Accounting.AnchorDate,
                valuationResultTooltips,
                Space01.Accounting.ValuesAtAnchorDate.LiabilityAssumptions,
                "IAS P&L Charge 2012",
                new string[,] 
                {
                    { "Service cost", "£8.5m" },
                    { "Other", "£3m" },
                    { "Total cost", "£11.5m" }
                },
                Space01.Accounting.ValuesAtAnchorDate.AssetTotal,
                Space01.Accounting.ValuesAtAnchorDate.AssetBreakdown,
                Space01.Accounting.ValuesAtAnchorDate.LiabilityTotal,
                Space01.Accounting.ValuesAtAnchorDate.LiabilityBreakdown
            );

            /**************************
             * Technical Provisions basis 
             * *******/ 
            AssertBasis
            (
                Space01.TechnicalProvisions.Name,
                Space01.TechnicalProvisions.AnchorDate,
                valuationResultTooltips,
                Space01.TechnicalProvisions.ValuesAtAnchorDate.LiabilityAssumptions,
                "SOC agreed for 20XX valuation",
                new string[,] 
                {
                    { "Deficit contributions", "£1m" },
                    { "Length of Recovery Plan", "10 years" },
                    { "Current employer rate", "20%" },
                    { "Current employee rate", "3%" }
                },
                Space01.TechnicalProvisions.ValuesAtAnchorDate.AssetTotal,
                Space01.TechnicalProvisions.ValuesAtAnchorDate.AssetBreakdown,
                Space01.TechnicalProvisions.ValuesAtAnchorDate.LiabilityTotal,
                Space01.TechnicalProvisions.ValuesAtAnchorDate.LiabilityBreakdown
                );

            /**************************
             * Buyout basis 
             * *******/
            AssertBasis
            (
                Space01.Buyout.Name,
                Space01.Buyout.AnchorDate,
                valuationResultTooltips,
                Space01.Buyout.ValuesAtAnchorDate.LiabilityAssumptions,
                "SOC agreed for 20XX valuation",
                new string[,] 
                {
                    { "Deficit contributions", "£1m" },
                    { "Length of Recovery Plan", "10 years" },
                    { "Current employer rate", "20%" },
                    { "Current employee rate", "3%" }
                },
                Space01.Buyout.ValuesAtAnchorDate.AssetTotal,
                Space01.Buyout.ValuesAtAnchorDate.AssetBreakdown,
                Space01.Buyout.ValuesAtAnchorDate.LiabilityTotal,
                Space01.Buyout.ValuesAtAnchorDate.LiabilityBreakdown
            );
        }

        [Test]
        public void ABCTest()
        {
            driver.Login();

            driver.SelectScheme(ABC.Name);

            driver.GoToBaseline();

            var valuationResultTooltips = new string[,]
                {
                    { ABC.Accounting.Name, ABC.Accounting.ValuesAtAnchorDate.Balance },
                    { ABC.Funding.Name, ABC.Funding.ValuesAtAnchorDate.Balance },
                    { ABC.Solvency.Name, ABC.Solvency.ValuesAtAnchorDate.Balance }
                };

            AssertBasis(
                ABC.Accounting.Name,
                ABC.Accounting.AnchorDate,
                valuationResultTooltips,
                ABC.Accounting.ValuesAtAnchorDate.LiabilityAssumptions,
                "IAS P&L Charge 2012",
                new string[,]
                {
                    { "Service cost", "£8.5m" },
                    { "Other", "£3m" },
                    { "Total cost", "£11.5m" }
                },
                ABC.Accounting.ValuesAtAnchorDate.AssetTotal,
                ABC.Accounting.ValuesAtAnchorDate.AssetBreakdown,
                ABC.Accounting.ValuesAtAnchorDate.LiabilityTotal,
                ABC.Accounting.ValuesAtAnchorDate.LiabilityBreakdown
                );

            AssertBasis(
                ABC.Funding.Name,
                ABC.Funding.AnchorDate,
                valuationResultTooltips,
                ABC.Funding.ValuesAtAnchorDate.LiabilityAssumptions,
                "SOC agreed for 2012 valuation",
                new string[,]
                            {
                    { "Deficit contributions", "£10.7m" },
                    { "Length of Recovery Plan", "10 years" },
                    { "Current employer rate", "19%" },
                    { "Current employee rate", "5%" }
                },
                ABC.Funding.ValuesAtAnchorDate.AssetTotal,
                ABC.Funding.ValuesAtAnchorDate.AssetBreakdown,
                ABC.Funding.ValuesAtAnchorDate.LiabilityTotal,
                ABC.Funding.ValuesAtAnchorDate.LiabilityBreakdown
                );

            AssertBasis(
                ABC.Solvency.Name,
                ABC.Solvency.AnchorDate,
                valuationResultTooltips,
                ABC.Solvency.ValuesAtAnchorDate.LiabilityAssumptions,
                "SOC agreed for 2012 valuation",
                new string[,]
                {
                    { "Deficit contributions", "£10.7m" },
                    { "Length of Recovery Plan", "10 years" },
                    { "Current employer rate", "19%" },
                    { "Current employee rate", "5%" }
                },
                ABC.Solvency.ValuesAtAnchorDate.AssetTotal,
                ABC.Solvency.ValuesAtAnchorDate.AssetBreakdown,
                ABC.Solvency.ValuesAtAnchorDate.LiabilityTotal,
                ABC.Solvency.ValuesAtAnchorDate.LiabilityBreakdown
                );
        }

        [Test]
        [Ignore]
        public void MixedBag01()
        {
            driver.Login();

            driver.SelectScheme("The Mixed Bag Test Scheme");

            driver.GoToBaseline();

            /**************************
             * Technical Provisions
             * *******/
            driver.SelectBasis("Technical Provisions");

            Assert.AreEqual("31/03/2013", driver.FindElement(By.Id("valuation-date")).Text);

            driver.AssertValuesTable("liability-assumptions-table", new string[,]
                {
                    { "Pre retirement discount rate XXX1", "1.25%" },
                    { "Post retirement discount rate XXX1", "1.50%" },
                    { "Inflation - CPI XXX1", "9.90%" },
                    { "Life expectancy at 65 (male)", "20 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "21 yrs" },
                    { "Pension commuted for cash", "20%" } //BUG shouldn't this be 30%? This figure does behave differently from other assumptions, so may be not
                });

            driver.AssertPie("assetBreakdownPie", "£108M",
                new string[]
                {
                    "Growth (£62M)",
                    "Matched (£38.9M)",
                    "Other (£7M)",
                    "DGFs (£11M)",
                    "Equities (£45M)",
                    "Property (£6M)",
                    "Corporate Bonds (£10M)",
                    "Fixed Interest Gilts (£12M)",
                    "Index Linked Gilts (£5M)",
                    "Buy in (£11.9M)",
                    "Cash (£7M)"
                });

            /**************************
             * Buyout basis 
             * *******/
            driver.SelectBasis("Buyout");

            Assert.AreEqual("31/12/2013", driver.FindElement(By.Id("valuation-date")).Text);

            driver.AssertValuesTable("liability-assumptions-table", new string[,] 
                {
                    { "Pre retirement discount rate", "2.98%" },
                    { "Post retirement discount rate", "2.98%" },
                    { "Pensioner discount rate", "2.98%" },
                    { "Salary increases", "3.60%" },
                    { "Inflation (RPI)", "3.60%" },
                    { "Inflation (CPI)", "3.60%" },
                    { "Life expectancy at 65 (male)", "22 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "24 yrs" },
                    { "Pension commuted for cash", "20%" }
                });
        }

        private void AssertBasis(
            string name, 
            string valuationDate, 
            string[,] valuationBarchartTooltips,
            string[,] liabilitAssumptions,
            string namedPropertyGroupName,
            string[,] namedPropertyGroupValues,
            string assetPieTotal,
            string[] assetPieLegendValues,
            string liabilityPieTotal,
            string[] liabilityPieLegendValues)
        {
            driver.SelectBasis(name);

            Assert.AreEqual(valuationDate, driver.FindElement(By.Id("valuation-date")).Text);

            for (int i = 0; i < valuationBarchartTooltips.GetLength(0); i++)
            {
                AssertValuationChartBarTooltip(i + 1, valuationBarchartTooltips[i, 0], valuationBarchartTooltips[i, 1]);
            }

            driver.AssertValuesTable("liability-assumptions-table", liabilitAssumptions);

            Assert.AreEqual(namedPropertyGroupName, driver.FindElement(By.Id("namedpropertygroupname")).Text);

            driver.AssertValuesTable("namedpropertygrouptable", namedPropertyGroupValues);

            driver.AssertPie("assetBreakdownPie", assetPieTotal, assetPieLegendValues);

            driver.AssertPie("liabilityBreakdownPie", liabilityPieTotal, liabilityPieLegendValues);

            AssertAssetsAndLiabilitiesChartBarTooltip(true, "Assets: " + assetPieTotal);
            AssertAssetsAndLiabilitiesChartBarTooltip(false, "Liabilities: " + liabilityPieTotal);
        }

        /// <summary>
        /// Check the tooltip value that appears when hovering over the basis bar
        /// </summary>
        /// <param name="barIdx">1 based index</param>
        /// <param name="name"></param>
        /// <param name="total"></param>
        private void AssertValuationChartBarTooltip(int barIdx, string name, string total)
        {
            driver.ScrollToTop(); // else inconsistent tooltip behaviour

            var chart = driver.FindElement(By.Id("valuationResultBalanceChartDiv"));
            var bar = chart
                .FindElement(By.XPath(".//div[@class='highcharts-container']"))
                    .FindElement(By.TagName("svg"))
                        .FindElement(By.ClassName("highcharts-series-group"))
                            .FindElement(By.TagName("g"))
                                .FindElements(By.TagName("rect"))[barIdx - 1];

            new Actions(driver).MoveToElement(bar).Perform();
            var tooltip = chart.FindElement(By.XPath(".//div[@class='highcharts-container']/div[@class='highcharts-tooltip']/span"));
            if (!string.IsNullOrEmpty(tooltip.Text))
            {
                if (string.Equals(name, tooltip.Text.Substring(0, name.Length)) &&
                    string.Equals(total, tooltip.Text.Substring(tooltip.Text.IndexOf("£"))))
                {
                    return;
                }
            }

            Assert.Fail(string.Format("Could not verify Valuation chart bar tooltip. Expected: {0}-{1}, Actual: {2}", name, total, tooltip.Text));
        }

        private void AssertAssetsAndLiabilitiesChartBarTooltip(bool isAsset, string text)
        {
            var chart = driver.FindElement(By.Id("valuationResultValuationChartDiv"));
            var bar = chart
                .FindElement(By.XPath(".//div[@class='highcharts-container']"))
                    .FindElement(By.TagName("svg"))
                        .FindElement(By.ClassName("highcharts-series-group"))
                            .FindElement(By.TagName("g"))
                                .FindElements(By.TagName("rect"))[isAsset ? 0 : 1];

            driver.ScrollToElement("valuation");
            bar.Click();
            Assert.AreEqual(text, driver.FindElement(By.CssSelector("div#valuationResultValuationChartDiv > div.highcharts-container > div.highcharts-tooltip > span")).Text);
        }
    }
}
