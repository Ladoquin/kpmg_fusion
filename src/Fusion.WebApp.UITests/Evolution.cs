﻿namespace Fusion.WebApp.UITests
{
    using Fusion.WebApp.UITests.TestValues;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Firefox;
    using System;
    using System.Linq;

    //TODO check if figures correctly displaying as a positive or negative value
    //TODO bother to check difference between before and after balance? 

    [TestFixture]
    public class Evolution
    {
        FirefoxDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new FirefoxDriver();
        }        

        [TearDown]
        public void TearDown()
        {
            driver.Logout();

            driver.Quit();
        }

        [Test]
        public void Space01Test()
        {
            driver.Login();

            driver.SelectScheme(Space01.Name);

            driver.GoToEvolution();

           AssertBasis
            (
                Space01.Accounting.Name,
                Space01.Accounting.AnchorDate,
                Space01.Accounting.ValuesAtAnchorDate.Balance,
                Space01.Accounting.ValuesAtAnchorDate.LiabilityAssumptions,
                Space01.DateX,
                Space01.Accounting.ValuesAtX.Balance,
                Space01.Accounting.ValuesAtX.LiabilityAssumptions,
                Space01.Accounting.ValuesAtX.LiabilityTotal,
                Space01.Accounting.ValuesAtX.LiabilityBreakdown,
                Space01.Accounting.ValuesAtX.AssetTotal,
                Space01.Accounting.ValuesAtX.AssetBreakdown,
                Space01.Accounting.ValuesAtX.InvestmentReturns,
                new string[] { "3.54%", "3.04%", "-0.25%", "4.07%" },
                new string[] { "3.56%", "3.46%", "-0.02%", "4.19%" },
                new string[] { "8.81%", "9.04%", "17.04%", "4.28%", "-3.14%", "-3.36%", "1.08%" },
                new string[] { "£67.7K", "£271K", "£1M", "£1.34M" },
                new string[] { "£7M", "-£5.66M" },
                new string[] { "£19.6M", "£5.04M", "£6.29M", "£0", "£1M", "£3.44M", "£1.14M", "£12.8M" }
            );

            AssertBasis
            (
                Space01.TechnicalProvisions.Name,
                Space01.TechnicalProvisions.AnchorDate,
                Space01.TechnicalProvisions.ValuesAtAnchorDate.Balance,
                Space01.TechnicalProvisions.ValuesAtAnchorDate.LiabilityAssumptions,
                Space01.DateX,
                Space01.TechnicalProvisions.ValuesAtX.Balance,
                Space01.TechnicalProvisions.ValuesAtX.LiabilityAssumptions,
                Space01.TechnicalProvisions.ValuesAtX.LiabilityTotal,
                Space01.TechnicalProvisions.ValuesAtX.LiabilityBreakdown,
                Space01.TechnicalProvisions.ValuesAtX.AssetTotal,
                Space01.TechnicalProvisions.ValuesAtX.AssetBreakdown,
                Space01.TechnicalProvisions.ValuesAtX.InvestmentReturns,
                new string[] { "3.54%", "3.04%", "-0.25%", "4.07%" },
                new string[] { "3.56%", "3.46%", "-0.02%", "4.19%" },
                new string[] { "8.81%", "9.04%", "17.04%", "4.28%", "-3.14%", "-3.36%", "1.08%" },
                new string[] { "£68.5K", "£274K", "£1M", "£1.34M" },
                new string[] { "£7.01M", "-£5.67M" },
                new string[] { "£29.3M", "£5.01M", "£9.41M", "£11.8M", "£1M", "£3.18M", "£1.97M", "£30.5M" }
            );

            AssertBasis
            (
                Space01.Buyout.Name,
                Space01.Buyout.AnchorDate,
                Space01.Buyout.ValuesAtAnchorDate.Balance,
                Space01.Buyout.ValuesAtAnchorDate.LiabilityAssumptions,
                Space01.DateX,
                Space01.Buyout.ValuesAtX.Balance,
                Space01.Buyout.ValuesAtX.LiabilityAssumptions,
                Space01.Buyout.ValuesAtX.LiabilityTotal,
                Space01.Buyout.ValuesAtX.LiabilityBreakdown,
                Space01.Buyout.ValuesAtX.AssetTotal,
                Space01.Buyout.ValuesAtX.AssetBreakdown,
                Space01.Buyout.ValuesAtX.InvestmentReturns,
                new string[] { "3.54%", "3.04%", "-0.25%", "4.07%" },
                new string[] { "3.56%", "3.46%", "-0.02%", "4.19%" },
                new string[] { "8.81%", "9.04%", "17.04%", "4.28%", "-3.14%", "-3.36%", "1.08%" },
                new string[] { "£68.1K", "£273K", "£1M", "£1.34M" },
                new string[] { "£6.48M", "-£5.13M" },
                new string[] { "£75.1M", "£5.19M", "£16.1M", "£0", "£1M", "£2.84M", "£732K", "£59.7M" }
            );
        }

        [Test]
        public void ABCTest()
        {
            driver.Login();

            driver.SelectScheme(ABC.Name);

            driver.GoToEvolution();
            
            AssertBasis
            (
                ABC.Accounting.Name,
                ABC.Accounting.AnchorDate,
                ABC.Accounting.ValuesAtAnchorDate.Balance,
                ABC.Accounting.ValuesAtAnchorDate.LiabilityAssumptions,
                ABC.DateX,
                ABC.Accounting.ValuesAtX.Balance,
                ABC.Accounting.ValuesAtX.LiabilityAssumptions,
                ABC.Accounting.ValuesAtX.LiabilityTotal,
                ABC.Accounting.ValuesAtX.LiabilityBreakdown,
                ABC.Accounting.ValuesAtX.AssetTotal,
                ABC.Accounting.ValuesAtX.AssetBreakdown,
                ABC.Accounting.ValuesAtX.InvestmentReturns,
                new string[] { "3.60%", "3.61%", "0.10%", "4.34%" },
                new string[] { "3.56%", "3.46%", "-0.02%", "4.19%" },
                new string[] { "-0.63%", "0.73%", "5.58%", "1.14%", "3.43%", "4.04%", "3.18%" },
                new string[] { "£232K", "£979K", "£2.67M", "£3.89M" },
                new string[] { "£2.44M", "£1.45M" },
                new string[] { "£39.1M", "£4.35M", "£9.82M", "£0", "£2.67M", "£6.64M", "£0", "£44M" }
            );

            AssertBasis
            (
                ABC.Funding.Name,
                ABC.Funding.AnchorDate,
                ABC.Funding.ValuesAtAnchorDate.Balance,
                ABC.Funding.ValuesAtAnchorDate.LiabilityAssumptions,
                ABC.DateX,
                ABC.Funding.ValuesAtX.Balance,
                ABC.Funding.ValuesAtX.LiabilityAssumptions,
                ABC.Funding.ValuesAtX.LiabilityTotal,
                ABC.Funding.ValuesAtX.LiabilityBreakdown,
                ABC.Funding.ValuesAtX.AssetTotal,
                ABC.Funding.ValuesAtX.AssetBreakdown,
                ABC.Funding.ValuesAtX.InvestmentReturns,
                new string[] { "3.54%", "3.04%", "-0.25%", "4.07%" },
                new string[] { "3.56%", "3.46%", "-0.02%", "4.19%" },
                new string[] { "8.81%", "9.04%", "17.04%", "4.28%", "-3.14%", "-3.36%", "1.08%" },
                new string[] { "£939K", "£3.97M", "£10.7M", "£15.6M" },
                new string[] { "£10M", "£5.61M" },
                new string[] { "£85.4M", "£16.8M", "£33.7M", "£5.57M", "£10.7M", "£14.6M", "£787K", "£36.8M" }
            );

            AssertBasis
            (
                ABC.Solvency.Name,
                ABC.Solvency.AnchorDate,
                ABC.Solvency.ValuesAtAnchorDate.Balance,
                ABC.Solvency.ValuesAtAnchorDate.LiabilityAssumptions,
                ABC.DateX,
                ABC.Solvency.ValuesAtX.Balance,
                ABC.Solvency.ValuesAtX.LiabilityAssumptions,
                ABC.Solvency.ValuesAtX.LiabilityTotal,
                ABC.Solvency.ValuesAtX.LiabilityBreakdown,
                ABC.Solvency.ValuesAtX.AssetTotal,
                ABC.Solvency.ValuesAtX.AssetBreakdown,
                ABC.Solvency.ValuesAtX.InvestmentReturns,
                new string[] { "3.54%", "3.04%", "-0.25%", "4.07%" },
                new string[] { "3.56%", "3.46%", "-0.02%", "4.19%" },
                new string[] { "8.81%", "9.04%", "17.04%", "4.28%", "-3.14%", "-3.36%", "1.08%" },
                new string[] { "£0", "£0", "£10.7M", "£10.7M" },
                new string[] { "£9.95M", "£745K" },
                new string[] { "£195M", "£17.2M", "£29.3M", "£558K", "£10.7M", "£14.6M", "£4.51M", "£153M" }
            );
        }

        private void AssertBasis(
            string name,
            string anchorDate,
            string anchorDateBalance,
            string[,] anchorDateLiabilityAssumptions,
            string analysisDate,
            string balance,
            string[,] liabilityAssumptions,
            string liabilityTotal,
            string[] liabilityBreakdown,
            string assetTotal,
            string[] assetBreakdown,
            string[,] assetAsumptions,
            string[] marketDataYieldsAtAnchorDate,
            string[] marketDataYields,
            string[] marketDataAssetReturns,
            string[] schemeDataContributions,
            string[] schemeDataBenefits,
            string[] analysisOfChangeValues
            )
        {
            driver.SelectBasis(name);

            driver.SelectAnalysisStartDate(anchorDate);
            driver.SelectAnalysisEndDate(analysisDate);

            Assert.AreEqual(balance, driver.FindElement(By.Id("analysisPeriodBalance")).Text);

            AssertAnalysisOfChange(analysisOfChangeValues);

            Assert.AreEqual(anchorDateBalance, driver.FindElement(By.Id("startDateBalance")).Text);
            Assert.AreEqual(balance, driver.FindElement(By.Id("endDateBalance")).Text);

            ToggleAssumptionsPanel();

            SelectPanelTab("Liabilities");

            AssertLiabilitiesTab(anchorDate, analysisDate, anchorDateLiabilityAssumptions, liabilityAssumptions, liabilityTotal, liabilityBreakdown);

            SelectPanelTab("Assets");

            driver.AssertPie("assetBreakdownPie", assetTotal, assetBreakdown);

            driver.AssertValuesTable("assetAssumptionsTable", assetAsumptions);

            SelectPanelTab("Market data");

            driver.AssertValuesTable("marketdata_yields", marketDataYieldsAtAnchorDate);
            driver.AssertValuesTable("marketdata_yields", marketDataYields, TDIdxOfValue: 1);

            driver.AssertValuesTable("marketdata_assetreturns", marketDataAssetReturns);            

            SelectPanelTab("Scheme data");

            driver.AssertValuesTable("evolution-scheme-data-contributions", schemeDataContributions);
            driver.AssertValuesTable("evolution-scheme-data-benefits", schemeDataBenefits);

            ToggleAssumptionsPanel();
        }

        private void AssertLiabilitiesTab(string anchorDate, string analysisEndDate, string[,] assumptionsAtAnchorDate, string[,] assumptionsAtAnalysisEndDate, string liabilityTotal, string[] liabilityBreakdown)
        {
            var tab = driver.FindElement(By.Id("evolutiondashboard_liabilities"));
            var table = tab.FindElement(By.ClassName("assumptionsTable"));

            Assert.AreEqual(anchorDate, table.FindElement(By.ClassName("analysisPeriodStart")).Text);
            Assert.AreEqual(analysisEndDate, table.FindElement(By.ClassName("analysisPeriodEnd")).Text);

            driver.AssertValuesTable(table, assumptionsAtAnchorDate);
            driver.AssertValuesTable(table, assumptionsAtAnalysisEndDate, TDIdxOfValue: 1);

            driver.AssertPie("liabilityBreakdownPie", liabilityTotal, liabilityBreakdown);
        }

        private void SelectPanelTab(string tabname)
        {
            var dashboard = driver.FindElement(By.Id("evolutiondashboard"));

            dashboard.FindElement(By.XPath("div/div/ul/li[a/h5[text()='" + tabname + "']]")).Click();
        }

        private void ToggleAssumptionsPanel()
        {
            driver.FindElement(By.CssSelector("div.cog")).Click();
        }

        private void AssertAnalysisOfChange(params string[] values)
        {
            var chart = driver.FindElement(By.Id("analysisOfSurplusChartDiv"));

            var i = 0;
            foreach (var value in values)
            {
                var actual = chart
                    .FindElement(By.ClassName("highcharts-container"))
                        .FindElement(By.CssSelector("div.highcharts-data-labels"))
                            .FindElements(By.TagName("span"))[i].Text;

                Assert.AreEqual(value, actual);

                i++;
            }
        }
    }
}
