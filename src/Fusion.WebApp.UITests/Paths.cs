﻿namespace Fusion.WebApp.UITests
{
    using System.Configuration;
    
    class Paths
    {
        const string Login = "/Account/Login";
        const string Baseline = "/Baseline";
        const string Evolution = "/Evolution";
        const string Analysis = "/Analysis";

        public static string GetLoginUrl() { return ConfigurationManager.AppSettings["url"] + Login; }
        public static string GetBaselineUrl() { return ConfigurationManager.AppSettings["url"] + Baseline; }
        public static string GetEvolutionUrl() { return ConfigurationManager.AppSettings["url"] + Evolution; }
        public static string GetAnalysisUrl() { return ConfigurationManager.AppSettings["url"] + Analysis; }
    }
}
