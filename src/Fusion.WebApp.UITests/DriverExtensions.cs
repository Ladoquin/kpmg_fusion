﻿namespace Fusion.WebApp.UITests
{
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Interactions;
    using OpenQA.Selenium.Support.UI;
    using System;
    using System.Configuration;
    using System.Linq;

    public static class DriverExtensions
    {
        public static void ScrollToTop(this FirefoxDriver driver)
        {
            driver.ExecuteScript("scroll(0, 0)");
            var wait = DateTime.Now.AddSeconds(1);
            while (DateTime.Now < wait)
            {
            }
        }

        public static void ScrollToElement(this FirefoxDriver driver, string id)
        {
            ScrollToElement(driver, driver.FindElement(By.Id(id)));
        }

        public static void ScrollToElement(this FirefoxDriver driver, IWebElement el)
        {
            driver.ExecuteScript(string.Format("window.scrollTo(0, {0})", el.Location.Y.ToString()));
        }

        public static void Login(this FirefoxDriver driver)
        {
            Login(driver, ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["password"]);
        }

        public static void Login(this FirefoxDriver driver, string username, string password)
        {
            driver.Navigate().GoToUrl(Paths.GetLoginUrl());

            var usernameEl = driver.FindElement(By.Id("UserName"));
            usernameEl.SendKeys(username);
            var passwordEl = driver.FindElement(By.Id("Password"));
            passwordEl.SendKeys(password);

            passwordEl.Submit();
        }

        public static void Logout(this FirefoxDriver driver)
        {
            try
            {
                driver.ScrollToTop();
            }
            catch { }
            driver.FindElement(By.Id("signin")).Click();
        }

        public static void SelectScheme(this FirefoxDriver driver, string schemeName)
        {
            try
            {
                driver.ScrollToTop();
            }
            catch { }
            var schemeSelectorSpans = driver.FindElement(By.Id("schemeSelector")).FindElements(By.TagName("span"));
            if (!string.Equals(schemeName, schemeSelectorSpans[1].Text))
            {
                schemeSelectorSpans[0].Click();
                driver.FindElement(By.Id("schemeSelectorMenu")).FindElement(By.XPath(".//li/a[text()='" + schemeName + "']")).Click();
                driver.WaitForAjaxCompletion();
            }
        }

        public static void SelectAnalysisStartDate(this FirefoxDriver driver, string date)
        {
            var field = driver.FindElement(By.XPath("//div[@class='headertitlewrap']/input[contains(concat(' ', @class, ' '), ' analysisPeriodStart ')]"));

            selectAnalysisDate(driver, field, date);
        }
        public static void SelectAnalysisEndDate(this FirefoxDriver driver, string date)
        {
            var field = driver.FindElement(By.XPath("//div[@class='headertitlewrap']/input[contains(concat(' ', @class, ' '), ' analysisPeriodEnd ')]"));

            selectAnalysisDate(driver, field, date);
        }

        private static void selectAnalysisDate(FirefoxDriver driver, IWebElement field, string date)
        {
            var arrow = field.FindElement(By.XPath("following::button"));
            arrow.Click();
            while (field.GetAttribute("value").Length > 0)
            {
                field.SendKeys(Keys.Home);
                field.SendKeys(Keys.Delete);
            };
            field.SendKeys(date);
            driver.FindElement(By.ClassName("ui-datepicker-current-day")).Click();
            driver.WaitForAjaxCompletion();
        }

        public static void WaitForAjaxCompletion(this FirefoxDriver driver)
        {
            var ok = false;
            var timeout = DateTime.Now.AddSeconds(10);
            while (DateTime.Now < timeout && !ok)
            {
                var ajaxIsComplete = (bool)(driver as IJavaScriptExecutor).ExecuteScript("return jQuery.active == 0");
                if (ajaxIsComplete)
                {
                    ok = true;         
                }
            }
            if (ok)
            {
                var wait = DateTime.Now.AddMilliseconds(500);
                while (DateTime.Now < wait)
                {
                }
            }
            else
            {
                throw new Exception("Ajax completion timed out");
            }
        }

        public static void GoToBaseline(this FirefoxDriver driver)
        {
            driver.Navigate().GoToUrl(Paths.GetBaselineUrl());
        }

        public static void GoToEvolution(this FirefoxDriver driver)
        {
            driver.Navigate().GoToUrl(Paths.GetEvolutionUrl());
        }

        public static void GoToAnalysis(this FirefoxDriver driver)
        {
            driver.Navigate().GoToUrl(Paths.GetAnalysisUrl());
        }

        public static void SelectBasis(this FirefoxDriver driver, string basisName)
        {
            var basisMenu = driver.FindElement(By.Id("currentBasis"));
                        
            basisMenu.Click();

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(1));
            wait.Until(x => driver.FindElement(By.Id("currentBasisMenu")).FindElement(By.XPath(".//li/a[text()='" + basisName + "']")).Displayed);

            var li = driver.FindElement(By.Id("currentBasisMenu")).FindElement(By.XPath(".//li/a[text()='" + basisName + "']"));
            li.Click();

            driver.WaitForAjaxCompletion();
        }

        public static void AssertPie(this FirefoxDriver driver, string pieId, string total, params string[] legendValues)
        {
            var assetPie = driver.FindElement(By.Id(pieId)).FindElement(By.ClassName("highcharts-container"));

            Assert.AreEqual(total, assetPie.FindElement(By.ClassName("highcharts-title")).Text);

            AssertPieLegends(assetPie, legendValues);
        }

        public static void AssertPieLegends(IWebElement pie, params string[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                Assert.AreEqual(values[i], pie.FindElements(By.XPath(".//div[@class='highcharts-legend-item']"))[i].Text);
            }
        }

        /// <summary>
        /// Check labels and values in table.
        /// </summary>
        /// <param name="TDIdxOfValue">If empty, the value in the first TD element in the TR will be checked.</param>
        public static void AssertValuesTable(this FirefoxDriver driver, string tableId, string[,] values, int? TDIdxOfValue = null)
        {
            AssertValuesTable(driver, driver.FindElement(By.Id(tableId)), values, TDIdxOfValue: TDIdxOfValue);
        }

        /// <summary>
        /// Check labels and values in table.
        /// </summary>
        /// <param name="TDIdxOfValue">If empty, the value in the first TD element in the TR will be checked.</param>
        public static void AssertValuesTable(this FirefoxDriver driver, IWebElement table, string[] values, int? TDIdxOfValue = null)
        {
            var v = new string[values.Length, 2];
            for (int i = 0; i < values.Length; i++)
            {
                v[i, 0] = string.Empty;
                v[i, 1] = values[i];
            }

            AssertValuesTable(driver, table, v, TDIdxOfValue: TDIdxOfValue, ignoreLabels: true);
        }

        /// <summary>
        /// Check labels and values in table.
        /// </summary>
        /// <param name="TDIdxOfValue">If empty, the value in the first TD element in the TR will be checked.</param>
        public static void AssertValuesTable(this FirefoxDriver driver, string tableId, string[] values, int? TDIdxOfValue = null)
        {
            AssertValuesTable(driver, driver.FindElement(By.Id(tableId)), values, TDIdxOfValue: TDIdxOfValue);
        }

        /// <summary>
        /// Check labels and values in table.
        /// </summary>
        /// <param name="TDIdxOfValue">If empty or 0, the value in the first TD element in the TR will be checked.</param>
        /// <param name="ignoreLabels">Toggle assertion of label text.</param>
        public static void AssertValuesTable(this FirefoxDriver driver, IWebElement table, string[,] values, int? TDIdxOfValue = null, bool ignoreLabels = false)
        {
            var trs = table.FindElements(By.TagName("tr")).Where(tr => tr.Displayed && !tr.GetAttribute("class").Split(' ').Contains("head"));

            Assert.AreEqual(values.GetLength(0), trs.Count(), "Table row count is not same length as values supplied");

            var i = 0;
            foreach (IWebElement tr in trs)
            {
                if (!ignoreLabels)
                {
                    Assert.AreEqual(values[i, 0], tr.FindElement(By.TagName("th")).Text);
                }
                Assert.AreEqual(values[i, 1], tr.FindElements(By.TagName("td"))[TDIdxOfValue.GetValueOrDefault(0)].Text);
                i++;
            }
        }

        public static void MoveSlider(this FirefoxDriver driver, string id, int offset)
        {
            new Actions(driver).ClickAndHold(driver.FindElement(By.Id(id))).MoveByOffset(offset, 0).Release().Perform();

            driver.WaitForAjaxCompletion();
        }
    }
}
