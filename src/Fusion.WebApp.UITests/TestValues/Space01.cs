﻿namespace Fusion.WebApp.UITests.TestValues
{
    using System.Collections.Generic;

    class Space01
    {
        public static string Name = "The Space01 Test Scheme";

        public static string DateX = "31/03/2014";

        #region Accounting

        public static BasisValues Accounting = new BasisValues
        {
            Name = "Accounting",
            AnchorDate = "31/03/2013",
            ValuesAtAnchorDate = new BasisValuesAtPeriod
            {
                Balance = "£19.6M",
                LiabilityAssumptions = new string[,] 
                    {
                        { "Pre retirement discount rate", "4.40%" },
                        { "Post retirement discount rate", "4.40%" },
                        { "Pensioner discount rate", "4.40%" },
                        { "Salary increases", "3.00%" },
                        { "Inflation (RPI)", "3.20%" },
                        { "Inflation (CPI)", "2.20%" },
                        { "Life expectancy at 65 (male)", "21.5 yrs" },
                        { "Life expectancy at 65 (male currently 45)", "22.7 yrs" },
                        { "Pension commuted for cash", "20%" }
                    },
                AssetTotal = "£98.3M",
                AssetBreakdown = new string[] 
                    {
                        "Growth (£55M)",
                        "Matched (£38.3M)",
                        "Other (£5M)",
                        "DGFs (£10M)",
                        "Equities (£45M)",
                        "Buy in (£8.3M)",
                        "Corporate Bonds (£10M)",
                        "Fixed Interest Gilts (£15M)",
                        "Index Linked Gilts (£5M)",                        
                        "Cash (£5M)"
                    },
                LiabilityTotal = "£118M",
                LiabilityBreakdown = new string[] { "Pensioners (£98.1M)", "Actives (£11.1M)", "Deferreds (£8.69M)" }
            },
            ValuesAtX = new BasisValuesAtPeriod
            {
                Balance = "£12.8M",
                LiabilityAssumptions = new string[,] 
                    {
                        { "Pre retirement discount rate", "4.77%" },
                        { "Post retirement discount rate", "4.77%" },
                        { "Pensioner discount rate", "4.77%" },
                        { "Salary increases", "2.90%" },
                        { "Inflation (RPI)", "3.10%" },
                        { "Inflation (CPI)", "2.10%" },
                        { "Life expectancy at 65 (male)", "21.5 yrs" },
                        { "Life expectancy at 65 (male currently 45)", "22.7 yrs" },
                        { "Pension commuted for cash", "20%" }
                    },
                InvestmentReturns = new string[,] 
                {
                    { "DGFs", "3.80%" },
                    { "Equities", "4.00%" },
                    { "Property", "3.50%" },
                    { "Corporate Bonds", "1.20%" },
                    { "Fixed Interest Gilts", "0.00%" },
                    { "Index Linked Gilts", "0.00%" },
                    { "Cash", "-2.50%" },
                    { "Weighted asset return", "2.42%" }
                },
                AssetTotal = "£97.2M",
                AssetBreakdown = new string[] 
                    {
                        "Growth (£54.7M)",
                        "Matched (£37.6M)",
                        "Other (£4.97M)",
                        "DGFs (£9.94M)",
                        "Equities (£44.7M)",
                        "Buy in (£7.78M)",
                        "Corporate Bonds (£9.94M)",
                        "Fixed Interest Gilts (£14.9M)",
                        "Index Linked Gilts (£4.97M)",
                        "Cash (£4.97M)"
                    },
                LiabilityTotal = "£110M",
                LiabilityBreakdown = new string[] { "Pensioners (£91.5M)", "Actives (£10.3M)", "Deferreds (£8.16M)" }
            }
        };

        #endregion

        #region Technical Provisions

        public static BasisValues TechnicalProvisions = new BasisValues
        {
            Name = "Technical Provisions",
            AnchorDate = "31/03/2013",
            ValuesAtAnchorDate = new BasisValuesAtPeriod
            {
                Balance = "£29.3M",
                LiabilityAssumptions = new string[,]
                    {
                        { "Pre retirement discount rate", "5.25%" },
                        { "Post retirement discount rate", "3.75%" },
                        { "Pensioner discount rate", "3.75%" },
                        { "Salary increases", "4.20%" },
                        { "Inflation (RPI)", "3.20%" },
                        { "Inflation (CPI)", "2.20%" },
                        { "Life expectancy at 65 (male)", "22 yrs" },
                        { "Life expectancy at 65 (male currently 45)", "24 yrs" },
                        { "Pension commuted for cash", "20%" }
                    },
                AssetTotal = "£99.1M",
                AssetBreakdown = new string[]
                    {
                        "Growth (£55M)",
                        "Matched (£39.1M)",
                        "Other (£5M)",
                        "DGFs (£10M)",
                        "Equities (£45M)",
                        "Buy in (£9.12M)",
                        "Corporate Bonds (£10M)",
                        "Fixed Interest Gilts (£15M)",
                        "Index Linked Gilts (£5M)",
                        "Cash (£5M)"
                    },
                LiabilityTotal = "£128M",
                LiabilityBreakdown = new string[] { "Pensioners (£107M)", "Actives (£12.3M)", "Deferreds (£8.88M)" }
            },
            ValuesAtX = new BasisValuesAtPeriod
            {
                Balance = "£30.5M",
                LiabilityAssumptions = new string[,]
                {
                    { "Pre retirement discount rate", "5.10%" },
                    { "Post retirement discount rate", "3.60%" },
                    { "Pensioner discount rate", "3.60%" },
                    { "Salary increases", "4.10%" },
                    { "Inflation (RPI)", "3.10%" },
                    { "Inflation (CPI)", "2.10%" },
                    { "Life expectancy at 65 (male)", "22 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "24 yrs" },
                    { "Pension commuted for cash", "20%" }
                },
                InvestmentReturns = new string[,] 
                {
                    { "DGFs", "3.80%" },
                    { "Equities", "4.00%" },
                    { "Property", "3.50%" },
                    { "Asset Backed Fund", "1.20%" },
                    { "Corporate Bonds", "1.20%" },
                    { "Fixed Interest Gilts", "0.00%" },
                    { "Index Linked Gilts", "0.00%" },
                    { "Cash", "-2.50%" },
                    { "Weighted asset return", "2.42%" }
                },
                AssetTotal = "£98.6M",
                AssetBreakdown = new string[]
                    {
                        "Growth (£54.7M)",
                        "Matched (£39M)",
                        "Other (£4.97M)",
                        "DGFs (£9.94M)",
                        "Equities (£44.7M)",
                        "Buy in (£9.18M)",
                        "Corporate Bonds (£9.94M)",
                        "Fixed Interest Gilts (£14.9M)",
                        "Index Linked Gilts (£4.97M)",
                        "Cash (£4.97M)"
                    },
                LiabilityTotal = "£129M",
                LiabilityBreakdown = new string[] { "Pensioners (£108M)", "Actives (£12.5M)", "Deferreds (£9.04M)" }
            }
        };

        #endregion

        #region Buyout

        public static BasisValues Buyout = new BasisValues
        {
            Name = "Buyout",
            AnchorDate = "31/03/2013",
            ValuesAtAnchorDate = new BasisValuesAtPeriod
            {
                Balance = "£75.1M",
                LiabilityAssumptions = new string[,] 
                {
                    { "Pre retirement discount rate", "2.98%" },
                    { "Post retirement discount rate", "2.98%" },
                    { "Pensioner discount rate", "2.98%" },
                    { "Salary increases", "3.60%" },
                    { "Inflation (RPI)", "3.60%" },
                    { "Inflation (CPI)", "3.60%" },
                    { "Life expectancy at 65 (male)", "22 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "24 yrs" },
                    { "Pension commuted for cash", "20%" }
                },
                AssetTotal = "£102M",
                AssetBreakdown = new string[]
                {
                    "Growth (£55M)",
                    "Matched (£42.1M)",
                    "Other (£5M)",
                    "DGFs (£10M)",
                    "Equities (£45M)",
                    "Buy in (£12.1M)",
                    "Corporate Bonds (£10M)",
                    "Fixed Interest Gilts (£15M)",
                    "Index Linked Gilts (£5M)",                    
                    "Cash (£5M)"
                },
                LiabilityTotal = "£177M",
                LiabilityBreakdown = new string[] { "Pensioners (£140M)", "Actives (£21M)", "Deferreds (£16.2M)" }
            },
            ValuesAtX = new BasisValuesAtPeriod
            {
                Balance = "£59.7M",
                LiabilityAssumptions = new string[,] 
                {
                    { "Pre retirement discount rate", "3.54%" },
                    { "Post retirement discount rate", "3.54%" },
                    { "Pensioner discount rate", "3.54%" },
                    { "Salary increases", "3.50%" },
                    { "Inflation (RPI)", "3.50%" },
                    { "Inflation (CPI)", "3.50%" },
                    { "Life expectancy at 65 (male)", "22 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "24 yrs" },
                    { "Pension commuted for cash", "20%" }
                },
                InvestmentReturns = new string[,] 
                {
                    { "DGFs", "3.80%" },
                    { "Equities", "4.00%" },
                    { "Property", "3.50%" },
                    { "Asset Backed Fund", "1.20%" },
                    { "Corporate Bonds", "1.20%" },
                    { "Fixed Interest Gilts", "0.00%" },
                    { "Index Linked Gilts", "0.00%" },
                    { "Cash", "-2.50%" },
                    { "Weighted asset return", "2.42%" }
                },
                AssetTotal = "£101M",
                AssetBreakdown = new string[]
                {
                    "Growth (£54.7M)",
                    "Matched (£40.8M)",
                    "Other (£4.98M)",
                    "DGFs (£9.95M)",
                    "Equities (£44.8M)",
                    "Buy in (£10.9M)",
                    "Corporate Bonds (£9.95M)",
                    "Fixed Interest Gilts (£14.9M)",
                    "Index Linked Gilts (£4.98M)",                    
                    "Cash (£4.98M)"
                },
                LiabilityTotal = "£160M",
                LiabilityBreakdown = new string[] { "Pensioners (£127M)", "Actives (£18.9M)", "Deferreds (£14.5M)" }
            }
        };

        #endregion
    }
}
