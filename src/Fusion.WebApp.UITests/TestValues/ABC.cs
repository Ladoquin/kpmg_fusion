﻿namespace Fusion.WebApp.UITests.TestValues
{
    using System.Collections.Generic;

    class ABC
    {
        public static string Name = "The ABC Pension Scheme";

        public static string DateX = "31/03/2014";

        #region Accounting

        public static BasisValues Accounting = new BasisValues
        {
            Name = "Accounting",
            AnchorDate = "31/12/2013",
            ValuesAtAnchorDate = new BasisValuesAtPeriod
            {
                Balance = "£39.1M",
                LiabilityAssumptions = new string[,]
                {
                    { "Pre retirement discount rate (Corporate Bond Yield)", "4.70%" },
                    { "Post retirement discount rate (Corporate Bond Yield)", "4.70%" },
                    { "Pensioner discount rate (Corporate Bond Yield)", "4.70%" },
                    { "Salary increases", "3.30%" },
                    { "Inflation (RPI)", "3.30%" },
                    { "Inflation (CPI)", "2.60%" },
                    { "Life expectancy at 65 (male)", "23.6 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "25.1 yrs" },
                    { "Pension commuted for cash", "25%" }
                },
                AssetTotal = "£344M",
                AssetBreakdown = new string[]
                {
                    "Growth (£165M)",
                    "Matched (£172M)",
                    "Other (£6.88M)",
                    "Equities (£131M)",
                    "Property (£34.4M)",
                    "Corporate Bonds (£68.8M)",
                    "Fixed Interest Gilts (£51.6M)",
                    "Index Linked Gilts (£51.6M)",
                    "Cash (£6.88M)"
                },
                LiabilityTotal = "£383M",
                LiabilityBreakdown = new string[] { "Pensioners (£87.7M)", "Actives (£85.4M)", "Deferreds (£210M)" }
            },
            ValuesAtX = new BasisValuesAtPeriod
            {
                Balance = "£44M",
                LiabilityAssumptions = new string[,]
                {
                    { "Pre retirement discount rate (Corporate Bond Yield)", "4.53%" },
                    { "Post retirement discount rate (Corporate Bond Yield)", "4.53%" },
                    { "Pensioner discount rate (Corporate Bond Yield)", "4.53%" },
                    { "Salary increases", "3.26%" },
                    { "Inflation (RPI)", "3.26%" },
                    { "Inflation (CPI)", "2.56%" },
                    { "Life expectancy at 65 (male)", "23.6 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "25.1 yrs" },
                    { "Pension commuted for cash", "25%" }
                },
                InvestmentReturns = new string[,] {
                    { "DGFs", "3.80%" },
                    { "Equities", "4.00%" },
                    { "Property", "3.50%" },
                    { "Corporate Bonds", "1.20%" },
                    { "Fixed Interest Gilts", "0.00%" },
                    { "Index Linked Gilts", "0.00%" },
                    { "Cash", "-2.50%" },
                    { "Weighted asset return", "2.06%" }
                },
                AssetTotal = "£352M",
                AssetBreakdown = new string[]
                {
                    "Growth (£169M)",
                    "Matched (£176M)",
                    "Other (£7.04M)",
                    "Equities (£134M)",
                    "Property (£35.2M)",
                    "Corporate Bonds (£70.4M)",
                    "Fixed Interest Gilts (£52.8M)",
                    "Index Linked Gilts (£52.8M)",
                    "Cash (£7.04M)"
                },
                LiabilityTotal = "£396M",
                LiabilityBreakdown = new string[] { "Pensioners (£88.1M)", "Actives (£90.7M)", "Deferreds (£217M)" }
            }
        };

        #endregion

        #region Funding

        public static BasisValues Funding = new BasisValues
        {
            Name = "Funding",
            AnchorDate = "31/03/2013",
            ValuesAtAnchorDate = new BasisValuesAtPeriod
            {
                Balance = "£85.4M",
                LiabilityAssumptions = new string[,]
                {
                    { "Pre retirement discount rate (Gilts plus 1%)", "4.05%" },
                    { "Post retirement discount rate (Gilts plus 1%)", "4.05%" },
                    { "Pensioner discount rate (Gilts plus 1%)", "4.05%" },
                    { "Salary increases", "3.00%" },
                    { "Inflation (RPI)", "3.30%" },
                    { "Inflation (CPI)", "2.20%" },
                    { "Life expectancy at 65 (male)", "23.6 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "25.1 yrs" },
                    { "Pension commuted for cash", "25%" }
                },
                AssetTotal = "£331M",
                AssetBreakdown = new string[]
                            {
                    "Growth (£159M)",
                    "Matched (£166M)",
                    "Other (£6.62M)",
                    "Equities (£126M)",
                    "Property (£33.1M)",
                    "Corporate Bonds (£66.2M)",
                    "Fixed Interest Gilts (£49.6M)",
                    "Index Linked Gilts (£49.6M)",
                    "Cash (£6.62M)"
                },
                LiabilityTotal = "£416M",
                LiabilityBreakdown = new string[] { "Pensioners (£91.7M)", "Actives (£96.5M)", "Deferreds (£228M)" }
            },
            ValuesAtX = new BasisValuesAtPeriod
            {
                Balance = "£36.8M",
                LiabilityAssumptions = new string[,]
                {
                    { "Pre retirement discount rate (Gilts plus 1%)", "4.45%" },
                    { "Post retirement discount rate (Gilts plus 1%)", "4.45%" },
                    { "Pensioner discount rate (Gilts plus 1%)", "4.45%" },
                    { "Salary increases", "2.96%" },
                    { "Inflation (RPI)", "3.26%" },
                    { "Inflation (CPI)", "2.16%" },
                    { "Life expectancy at 65 (male)", "23.6 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "25.1 yrs" },
                    { "Pension commuted for cash", "25%" }
                },
                InvestmentReturns = new string[,] {
                    { "DGFs", "3.80%" },
                    { "Equities", "4.00%" },
                    { "Property", "3.50%" },
                    { "Asset Backed Fund", "1.20%" },
                    { "Corporate Bonds", "1.20%" },
                    { "Fixed Interest Gilts", "0.00%" },
                    { "Index Linked Gilts", "0.00%" },
                    { "Cash", "-2.50%" },
                    { "Weighted asset return", "2.06%" }
                },
                AssetTotal = "£352M",
                AssetBreakdown = new string[]
                {
                    "Growth (£169M)",
                    "Matched (£176M)",
                    "Other (£7.04M)",
                    "Equities (£134M)",
                    "Property (£35.2M)",
                    "Corporate Bonds (£70.4M)",
                    "Fixed Interest Gilts (£52.8M)",
                    "Index Linked Gilts (£52.8M)",
                    "Cash (£7.04M)"
                },
                LiabilityTotal = "£389M",
                LiabilityBreakdown = new string[] { "Pensioners (£89.1M)", "Actives (£88.3M)", "Deferreds (£211M)" }
            }
        };

        #endregion

        #region Solvency

        public static BasisValues Solvency = new BasisValues
        {
            Name = "Solvency",
            AnchorDate = "31/03/2013",
            ValuesAtAnchorDate = new BasisValuesAtPeriod
            {
                Balance = "£195M",
                LiabilityAssumptions = new string[,]
                {
                    { "Pre retirement discount rate (Gilts)", "3.30%" },
                    { "Post retirement discount rate (Gilts)", "3.30%" },
                    { "Pensioner discount rate (Gilts)", "3.30%" },
                    { "Salary increases", "0.00%" },
                    { "Inflation (RPI)", "3.75%" },
                    { "Inflation (CPI)", "2.65%" },
                    { "Life expectancy at 65 (male)", "24.5 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "26 yrs" },
                    { "Pension commuted for cash", "0%" }
                },
                AssetTotal = "£331M",
                AssetBreakdown = new string[]
                {
                    "Growth (£159M)",
                    "Matched (£166M)",
                    "Other (£6.62M)",
                    "Equities (£126M)",
                    "Property (£33.1M)",
                    "Corporate Bonds (£66.2M)",
                    "Fixed Interest Gilts (£49.6M)",
                    "Index Linked Gilts (£49.6M)",
                    "Cash (£6.62M)"
                },
                LiabilityTotal = "£526M",
                LiabilityBreakdown = new string[] { "Pensioners (£101M)", "Deferreds (£425M)" }
            },
            ValuesAtX = new BasisValuesAtPeriod
            {
                Balance = "£153M",
                LiabilityAssumptions = new string[,]
                {
                    { "Pre retirement discount rate (Gilts)", "3.55%" },
                    { "Post retirement discount rate (Gilts)", "3.55%" },
                    { "Pensioner discount rate (Gilts)", "3.55%" },
                    { "Salary increases", "0.00%" },
                    { "Inflation (RPI)", "3.71%" },
                    { "Inflation (CPI)", "2.61%" },
                    { "Life expectancy at 65 (male)", "24.5 yrs" },
                    { "Life expectancy at 65 (male currently 45)", "26 yrs" },
                    { "Pension commuted for cash", "0%" }
                },
                InvestmentReturns = new string[,] 
                {
                    { "DGFs", "3.80%" },
                    { "Equities", "4.00%" },
                    { "Property", "3.50%" },
                    { "Asset Backed Fund", "1.20%" },
                    { "Corporate Bonds", "1.20%" },
                    { "Fixed Interest Gilts", "0.00%" },
                    { "Index Linked Gilts", "0.00%" },
                    { "Cash", "-2.50%" },
                    { "Weighted asset return", "2.06%" }
                },
                AssetTotal = "£351M",
                AssetBreakdown = new string[]
                {
                    "Growth (£168M)",
                    "Matched (£175M)",
                    "Other (£7.02M)",
                    "Equities (£133M)",
                    "Property (£35.1M)",
                    "Corporate Bonds (£70.2M)",
                    "Fixed Interest Gilts (£52.6M)",
                    "Index Linked Gilts (£52.6M)",
                    "Cash (£7.02M)"
                },
                LiabilityTotal = "£504M",
                LiabilityBreakdown = new string[] { "Pensioners (£99.4M)", "Deferreds (£404M)" }
            }
        };

        #endregion
    }
}
