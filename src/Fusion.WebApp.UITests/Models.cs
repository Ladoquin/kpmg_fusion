﻿namespace Fusion.WebApp.UITests
{
    class BasisValues
    {
        public string Name { get; set; }
        public string AnchorDate { get; set; }
        public BasisValuesAtPeriod ValuesAtAnchorDate { get; set; }
        public BasisValuesAtPeriod ValuesAtX { get; set; }
    }
    class BasisValuesAtPeriod
    {
        public string Balance { get; set; }
        public string[,] LiabilityAssumptions { get; set; }
        public string[,] InvestmentReturns { get; set; }
        public string AssetTotal { get; set; }
        public string[] AssetBreakdown { get; set; }
        public string LiabilityTotal { get; set; }
        public string[] LiabilityBreakdown { get; set; }        
    }
}
