﻿namespace FlightDeck.Domain.UnitTests
{
    using FlightDeck.Domain.UnitTests.Framework;
    using FlightDeck.DomainShared;
    using NUnit.Framework;
    using System.Linq;

    public class SchemeTests : TestsBase
    {
        public SchemeTests(TestModeType mode)
            : base(mode)
        {
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestRefreshDateIsSmallestOfAllMaxIndexDates(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.RefreshDate;
                var actual = t.Scheme.RefreshDate;
                Assert.That(actual, Is.EqualTo(expected), t.ToString());
            }
        }

        #region Composite Indices

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestCompositeIndexCalculation(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                // Get component indices for 'My total return' and 'My yield' (See A138 on Main tab on Tracker)
                CustomIndexService customIndexService = new CustomIndexService();

                var totalReturnName = "My total return";
                var totalReturnParams = t.Results.CompositeIndexParameters.Where(i => i.CiName == totalReturnName).Single();

                // Call CreateCustomIndex() with parameters read from Main tab. (We'll have to load the component stats)
                var totalReturnCompositeIndex = customIndexService.CreateCustomIndex(true, totalReturnParams.SourceIndexProportion.ToDictionary(x => t.Indices.Get(x.IndexName), x => x.Value), totalReturnParams.Rounding);

                // Compare CreateCustomIndex() results with Trackers from the Testing tab
                var trackerReadTotalReturnCompositeIndex = t.Results.CustomIndicesEvolution.Where(i => i.Name == totalReturnName).Single();
                foreach (var dailyItem in trackerReadTotalReturnCompositeIndex.Values)
                {
                    Assert.That(dailyItem.Value, Is.EqualTo(totalReturnCompositeIndex[dailyItem.Key]).Within(Utils.TestPrecision), string.Format("{0} composite index does't match date: {1}.", totalReturnName, dailyItem.Key.ToString("dd/MM/yyyy")));
                }

                var yieldName = "My yield";
                var yieldParams = t.Results.CompositeIndexParameters.Where(i => i.CiName == yieldName).Single();

                // Call CreateCustomIndex() with parameters read from Main tab. (We'll have to load the component stats)
                var yieldCompositeIndex = customIndexService.CreateCustomIndex(false, yieldParams.SourceIndexProportion.ToDictionary(x => t.Indices.Get(x.IndexName), x => x.Value), yieldParams.Rounding);

                // Compare CreateCustomIndex() results with Trackers from the Testing tab
                var trackerReadYieldCompositeIndex = t.Results.CustomIndicesEvolution.Where(i => i.Name == yieldName).Single();
                foreach (var dailyItem in trackerReadYieldCompositeIndex.Values)
                {
                    Assert.That(dailyItem.Value, Is.EqualTo(yieldCompositeIndex[dailyItem.Key]).Within(Utils.TestPrecision), string.Format("{0} composite index does't match year: {1}.", yieldName, dailyItem.Key.ToString("dd/MM/yyyy")));
                }

            }
        }

        #endregion
    }
}
