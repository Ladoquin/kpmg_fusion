﻿namespace FlightDeck.Domain.UnitTests
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.Domain.UnitTests.Framework;
    using FlightDeck.DomainShared;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    public class JourneyPlanTests : TestsBase
    {
        public JourneyPlanTests(TestModeType mode)
            : base(mode)
        {
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestNewJP(BasisType basisType)
        {
            AssertRelevance(basisType);

            var stopwatch = new Stopwatch();

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (!t.Scheme.Funded)
                    continue;

                var basisLookup = new Dictionary<int, IBasisContext>();

                var refresh = t.Results.RefreshDate;
                var start = t.Results.AttributionStart;
                var end = t.Results.AttributionEnd;

                IBasisContext currentBasis = null;

                var newJpBases = new Dictionary<int, int>
                {
                    { 1, t.Scheme.UnderlyingBases.First(x => x.DisplayName == t.Assumptions.JourneyPlanAssumptions.FundingBasisName).MasterBasisId }
                };
                if (!string.IsNullOrEmpty(t.Assumptions.JourneyPlanAssumptions.SelfSufficiencyBasisName))
                    newJpBases.Add(2, t.Scheme.UnderlyingBases.First(x => x.DisplayName == t.Assumptions.JourneyPlanAssumptions.SelfSufficiencyBasisName).MasterBasisId);
                if (!string.IsNullOrEmpty(t.Assumptions.JourneyPlanAssumptions.BuyoutBasisName))
                    newJpBases.Add(3, t.Scheme.UnderlyingBases.First(x => x.DisplayName == t.Assumptions.JourneyPlanAssumptions.BuyoutBasisName).MasterBasisId);

                foreach (var bkey in newJpBases.Keys)
                {
                    stopwatch.Reset();
                    stopwatch.Start();

                    var masterbasis = new MasterBasis(t.Scheme.UnderlyingBases.Where(b => b.MasterBasisId == newJpBases[bkey]));

                    var basesDataManager = new BasesDataManager(masterbasis.Bases.Values);
                    var basis = basesDataManager.GetBasis(end);
                    var assets = t.Scheme.SchemeAssets.Where(a => a.EffectiveDate == t.Scheme.SchemeAssets.Select(x => x.EffectiveDate).Distinct().Where(x => x <= end).OrderBy(x => x).Last());
                    var portfolioManager = new PortfolioManager(t.Scheme, basesDataManager);
                    var pf = portfolioManager.GetPortfolio(end);

                    var assumptionEvolution = new LiabilityAssumptionEvolutionService(basesDataManager).Calculate(masterbasis.AnchorDate, refresh);
                    var pincsEvolution = new PensionIncreaseEvolutionService(basesDataManager).Calculate(masterbasis.AnchorDate, refresh);
                    var liabilityEvolution = new LiabilityEvolutionService().Calculate(masterbasis.AnchorDate, refresh, basesDataManager, assumptionEvolution, pincsEvolution).ToDictionary(x => x.Date);
                    var exp = new LiabilityExperienceService().CalculateLiabilityStepChange(basesDataManager, liabilityEvolution);
                    var liabilityExperience = exp[LiabilityExperienceService.LiabilityExperienceType.Liab];
                    var buyinExperience = exp[LiabilityExperienceService.LiabilityExperienceType.Buyin];

                    var evolution = new BasisEvolutionData(liabilityEvolution, null, liabilityExperience, buyinExperience, null);

                    var storedAssumptions = t.Assumptions.StoredUserAssumptions.Any(x => x.Key == basis.DisplayName) ? t.Assumptions.StoredUserAssumptions.First(x => x.Key == basis.DisplayName).Value : null;

                    var context = new BasisContext(
                        t.Results.AttributionStart,
                        t.Results.AttributionEnd,
                        basis.EffectiveDate,
                        basis,
                        pf,
                        null,
                        evolution,
                        new BasisAssumptions(
                            storedAssumptions != null ? storedAssumptions.LiabilityAssumptions.ToDictionary(x => x.Key, x => x.Value) : evolution.LiabilityEvolution[t.Results.AttributionEnd].ToAssumptionsDictionary(),
                            storedAssumptions != null ? storedAssumptions.LifeExpectancy : basis.LifeExpectancy,
                            storedAssumptions != null ? storedAssumptions.PensionIncreases.ToDictionary(x => x.Key, x => x.Value) : evolution.LiabilityEvolution[t.Results.AttributionEnd].ToPensionIncreasesDictionary(),
                            pf.AssetData.Assets.ToDictionary(x => x.Class.Type, x => x.DefaultReturn),
                            t.Assumptions.BasisAssumptions.RecoveryPlanOptions));

                    basisLookup.Add(bkey, context);
                }

                var schemeCtx = TestContextHelper.GetSchemeContext(t);
                schemeCtx.Calculated.BuyinCost = new BuyinCost(t.Results.InsPenCost, t.Results.InsNonPenCost);
                var ctx = TestContextHelper.GetBasisContext(t);
                ctx.Results = new BasisAnalysisService(schemeCtx).Calculate(ctx, BasisAnalysisService.CalculationMode.CalculateEverything);

                var client = t.Assumptions.JourneyPlanAssumptions;
                var options = new NewJPParameters
                {
                    AnalysisDate = t.Results.AttributionEnd,
                    CashflowBasis = currentBasis,
                    FundingBasis = basisLookup[1],
                    SelfSufficiencyBasis = basisLookup.ContainsKey(2) ? basisLookup[2] : null,
                    BuyoutBasis = basisLookup.ContainsKey(3) ? basisLookup[3] : null,
                    CurveDataProjection = new CurveDataProjection(1, "", t.Results.GiltSpotRates.Select(x => new SpotRate(x.Key, x.Value, t.Results.AttributionEnd.AddYears(x.Key - 1))).ToList()),
                    RecoveryPlan = client.RecoveryPlan,
                    InvGrowthRateBefore = client.CurrentReturn.Value,
                    SelfSufficiencyDiscountRate = client.SelfSufficiencyDiscountRate.Value,
                    SalaryInflationType = client.SalaryInflationType,
                    TriggerSteps = client.TriggerSteps.Value,
                    TriggerTimeStartYear = client.TriggerTimeStartYear.Value,
                    TriggerTransitionPeriod = client.TriggerTransitionPeriod.Value,
                    UseCurves = client.UseCurves.Value,
                    IncludeBuyIns = client.IncludeBuyIns,
                    InitialGrowthRate = client.InitialReturn.Value,
                    TargetGrowthRate = client.FinalReturn.Value
                };

                var jp = new NewJP(schemeCtx, ctx).Calculate(options);

                var expected = t.Results.NewJP;
                var actual = jp;

                Assert.That(actual.Liab1.Keys, Is.EquivalentTo(expected.Liab1.Select(x => x.Key)), t.ToString());
                for (int year = expected.Liab1.Min(x => x.Key); year <= expected.Liab1.Max(x => x.Key); year++)
                {
                    Assert.That(actual.Liab1[year], Is.EqualTo(expected.Liab1.Single(x => x.Key == year).Value).Within(Utils.TestPrecision), t.ToString(string.Format("Liab1, year: {0}", year.ToString())));
                    Assert.That(actual.Liab2[year], Is.EqualTo(expected.Liab2.Single(x => x.Key == year).Value).Within(Utils.TestPrecision), t.ToString(string.Format("Liab2, year: {0}", year.ToString())));
                    Assert.That(actual.Liab3[year], Is.EqualTo(expected.Liab3.Single(x => x.Key == year).Value).Within(Utils.TestPrecision), t.ToString(string.Format("Liab3, year: {0}", year.ToString())));
                    Assert.That(actual.Assets1[year], Is.EqualTo(expected.Assets1.Single(x => x.Key == year).Value).Within(Utils.TestPrecision), t.ToString(string.Format("Assets1, year: {0}", year.ToString())));
                    Assert.That(actual.Assets2[year], Is.EqualTo(expected.Assets2.Single(x => x.Key == year).Value).Within(Utils.TestPrecision), t.ToString(string.Format("Assets2, year: {0}", year.ToString())));
                }

                if (expected.FundingPremiumPre.HasValue)
                {
                    Assert.That(jp.DefaultPremiums[BasisType.TechnicalProvision].Before, Is.EqualTo(expected.FundingPremiumPre.Value).Within(Utils.Precision), t.ToString("FundingPremiumPre"));
                    Assert.That(jp.DefaultPremiums[BasisType.TechnicalProvision].After, Is.EqualTo(expected.FundingPremiumPost.Value).Within(Utils.Precision), t.ToString("FundingPremiumPost"));
                }
                if (expected.SelfSufficiencyPremiumPre.HasValue)
                {
                    Assert.That(jp.DefaultPremiums[BasisType.SelfSufficiency].Before, Is.EqualTo(expected.SelfSufficiencyPremiumPre.Value).Within(Utils.Precision), t.ToString("SelfSufficiencyPremiumPre"));
                    Assert.That(jp.DefaultPremiums[BasisType.SelfSufficiency].After, Is.EqualTo(expected.SelfSufficiencyPremiumPost.Value).Within(Utils.Precision), t.ToString("SelfSufficiencyPremiumPost"));
                }
                if (expected.BuyoutPremiumPre.HasValue)
                {
                    Assert.That(jp.DefaultPremiums[BasisType.Buyout].Before, Is.EqualTo(expected.BuyoutPremiumPre.Value).Within(Utils.Precision), t.ToString("BuyoutPremiumPre"));
                    Assert.That(jp.DefaultPremiums[BasisType.Buyout].After, Is.EqualTo(expected.BuyoutPremiumPost.Value).Within(Utils.Precision), t.ToString("BuyoutPremiumPost"));
                }
            }
        }
    }
}
