﻿namespace FlightDeck.Domain.UnitTests
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.UnitTests.Framework;
    using FlightDeck.DomainShared;
    using NUnit.Framework;
    using System.Linq;

    public class AssetTests : TestsBase
    {
        public AssetTests(TestModeType mode)
            : base(mode)
        {
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestGrowthOverPeriodInfo(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (!t.Scheme.Funded)
                    continue;

                var expected = t.Results.GrowthOverPeriodInfo.OrderBy(x => x.Key).ToList();
                var actual = t.Scheme.GetGrowthOverPeriod(t.Results.AttributionStart, t.Results.AttributionEnd).OrderBy(x => x.Name).ToList();

                Assert.AreEqual(expected.Count, actual.Count, t.ToString("List count different"));
                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Key, actual[i].Name, t.ToString("Name"));
                    Assert.That(actual[i].Growth, Is.EqualTo(expected[i].Value).Within(Utils.TestPrecision), t.ToString("Value"));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestAssetHedgeBreakdownData(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (!t.Scheme.Funded)
                    continue;

                var schemeContext = TestContextHelper.GetSchemeContext(t);
                foreach (var assets in schemeContext.Data.SchemeAssets)
                {
                    if (assets.EffectiveDate >= t.Scheme.UnderlyingBases.Where(b => b.Type == basisType).Min(b => b.EffectiveDate) && // asset sheet is not before the basis starts
                        assets.EffectiveDate <= t.AttributionEnd) // asset sheet is before the current analysis period
                    {
                        var atDate = assets.EffectiveDate;
                        var expected = t.Results.AssetHedgeBreakdownData.Single(x => x.Date == atDate).ToHedgeBreakdownData();
                        var basisCtx = TestContextHelper.GetBasisContext(t, atDate: atDate);

                        var actualHedgeData = new AssetHedgeCalculator(TestContextHelper.GetSchemeContext(t)).CalculateExistingHedge(atDate, basisCtx.Data, TestContextHelper.GetPortfolioManager(t));

                        Assert.That(actualHedgeData.CurrentBasis[HedgeType.Interest].Total,
                                    Is.EqualTo(expected.CurrentBasis[HedgeType.Interest].Total).Within(Utils.TestPrecision), t.ToString());
                        Assert.That(actualHedgeData.CurrentBasis[HedgeType.Inflation].Total,
                                    Is.EqualTo(expected.CurrentBasis[HedgeType.Inflation].Total).Within(Utils.TestPrecision), t.ToString());
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Interest].Total,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Interest].Total).Within(Utils.TestPrecision), t.ToString());
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Inflation].Total,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Inflation].Total).Within(Utils.TestPrecision), t.ToString());
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Interest].Buyin,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Interest].Buyin).Within(Utils.TestPrecision), t.ToString());
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Inflation].Buyin,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Inflation].Buyin).Within(Utils.TestPrecision), t.ToString());
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Interest].PhysicalAssets,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Interest].PhysicalAssets).Within(Utils.TestPrecision), t.ToString());
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Inflation].PhysicalAssets,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Inflation].PhysicalAssets).Within(Utils.TestPrecision), t.ToString());
                    }
                }
            }
        }
    }
}
