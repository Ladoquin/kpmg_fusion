﻿namespace FlightDeck.Domain.UnitTests
{
    using FlightDeck.Domain.LDI;
    using FlightDeck.Domain.UnitTests.Framework;
    using FlightDeck.Domain.VaR;
    using FlightDeck.DomainShared;
    using NUnit.Framework;
    using System.Linq;
    using FlightDeck.Tracker.Models;
    
    public class VaRTests : TestsBase
    {
        public VaRTests(TestModeType mode)
            : base(mode)
        {
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestGiltBasisLiability(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expectedLdiGilt = t.Results.GiltBasisLiability;
                var actualLdiGilt = new LDI.GiltLiabilityCalculator(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t));
                Assert.That(actualLdiGilt.TotalLiab, Is.EqualTo(expectedLdiGilt.TotalLiab).Within(Utils.TestPrecision), t.ToString("TotalLiab"));
                Assert.That(actualLdiGilt.LiabDuration, Is.EqualTo(expectedLdiGilt.LiabDuration).Within(Utils.TestPrecision), t.ToString("LiabDuration"));
                Assert.That(actualLdiGilt.LiabPV01, Is.EqualTo(expectedLdiGilt.LiabPV01).Within(Utils.TestPrecision), t.ToString("LiabPV01"));
                Assert.That(actualLdiGilt.LiabIE01, Is.EqualTo(expectedLdiGilt.LiabIE01).Within(Utils.TestPrecision), t.ToString("LiabIE01"));
                Assert.That(actualLdiGilt.BuyInPV01, Is.EqualTo(expectedLdiGilt.BuyInPV01).Within(Utils.TestPrecision), t.ToString("BuyInPV01"));
                Assert.That(actualLdiGilt.BuyInIE01, Is.EqualTo(expectedLdiGilt.BuyInIE01).Within(Utils.TestPrecision), t.ToString("BuyInIE01"));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestVarWaterfall(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.Waterfall;
                var expectedLiabDuration = t.Results.JPAfterLiabDuration;

                var schemeCtx = TestContextHelper.GetSchemeContext(t);
                var ctx = TestContextHelper.GetBasisContext(t);
                ctx.Results = new BasisAnalysisResults
                    {
                        PIEInit = t.Results.PIEInit,
                        RecoveryPlanPaymentRequired = t.Results.RecoveryPlanPaymentRequired,
                        FundingLevel = t.Results.FundingLevel,
                        InvestmentStrategy = t.Results.InvestmentStrategyResults,
                        GiltBasisLiabilityData = t.Results.GiltBasisLiability
                    };
                ctx.Results.Hedging = new HedgeAnalysisCalculator(schemeCtx, ctx).Calculate(); //not great, can we get HedgeData directly from tracker? 
                ctx.Results.VaRInputs = new VaRInputsCalculator(schemeCtx).Calculate(ctx); //not great, can we get VaRInputs directly from tracker? 
                var actual = new WaterfallCalculator(schemeCtx, ctx).Calculate();

                Assert.AreEqual(expected, actual, t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestVarFunnel(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.VarFunnel;

                var ctx = TestContextHelper.GetBasisContext(t);
                ctx.Results = new BasisAnalysisResults
                {
                    AssetsAtSED = t.Results.AssetsAtSED,
                    FundingLevel = t.Results.FundingLevel,
                    VarWaterfall = t.Results.Waterfall,
                    JourneyPlanAfter = t.Results.JourneyPlanAfter.ToDictionary(x => x.Year, x => x.ToBalanceData()),
                    InvestmentStrategy = t.Results.InvestmentStrategyResults
                };
                var actual = new FunnelCalculator(TestContextHelper.GetSchemeContext(t)).Calculate(ctx);

                Assert.AreEqual(expected.Count, actual.Count, t.ToString());
                var i = 0;
                foreach(var exp in expected.OrderBy(x => x.Year))
                    Assert.AreEqual(exp, actual[i++], t.ToString("idx: " + exp.Year.ToString()));
            }
        }
    }
}
