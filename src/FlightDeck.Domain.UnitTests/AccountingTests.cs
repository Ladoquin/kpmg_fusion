﻿namespace FlightDeck.Domain.UnitTests
{
    using FlightDeck.Domain.Accounting;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.Domain.UnitTests.Framework;
    using FlightDeck.DomainShared;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AccountingTests : TestsBase
    {
        public AccountingTests(TestModeType mode)
            : base(mode)
        {
        }

        [TestCase(BasisType.Accounting)]
        public void TestBalanceSheetEstimate(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.BalanceSheetEstimate;
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).BalanceSheet;

                Assert.AreEqual(expected.ToBalanceData(), actual, t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestIAS19ProfitAndLoss(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (t.Scheme.AccountingIAS19Visible)
                {
                    var expected = t.Results.ProfitLossForecast.Single(x => x.ValuationBasis == ValuationBasisType.Ias19);
                    var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).IAS19;

                    Assert.That(actual.InterestCharge, Is.EqualTo(expected.InterestCharge).Within(Utils.Precision), t.ToString("InterestCharge"));
                    Assert.That(actual.InterestCredit, Is.EqualTo(expected.InterestCredit).Within(Utils.Precision), t.ToString("InterestCredit"));
                    Assert.That(actual.SchemeAdminExpenses, Is.EqualTo(expected.SchemeExpenses).Within(Utils.Precision), t.ToString("SchemeAdminExpenses"));
                    Assert.That(actual.CurrentServiceCost, Is.EqualTo(expected.ServiceCost).Within(Utils.Precision), t.ToString("CurrentServiceCost"));
                    Assert.That(actual.Total, Is.EqualTo(expected.Total).Within(Utils.Precision), t.ToString("Total"));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestFRS17ProfitAndLoss(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (t.Scheme.AccountingFRS17Visible)
                {
                    var expected = t.Results.ProfitLossForecast.Single(x => x.ValuationBasis == ValuationBasisType.Ias19); //frs17 is now frs102 which should give the same results as ias19
                    var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).FRS17;

                    Assert.That(actual.InterestCharge, Is.EqualTo(expected.InterestCharge).Within(Utils.Precision), t.ToString("InterestCharge"));
                    Assert.That(actual.InterestCredit, Is.EqualTo(expected.InterestCredit).Within(Utils.Precision), t.ToString("InterestCredit"));
                    Assert.That(actual.SchemeAdminExpenses, Is.EqualTo(expected.SchemeExpenses).Within(Utils.Precision), t.ToString("SchemeAdminExpenses"));
                    Assert.That(actual.CurrentServiceCost, Is.EqualTo(expected.ServiceCost).Within(Utils.Precision), t.ToString("CurrentServiceCost"));
                    Assert.That(actual.Total, Is.EqualTo(expected.Total).Within(Utils.Precision), t.ToString("Total"));
                }
            }
        }        

        [TestCase(BasisType.Accounting)]
        public void TestIAS19Disclosure(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (t.Scheme.AccountingIAS19Visible)
                {
                    var schemeCtx = TestContextHelper.GetSchemeContext(t);
                    var analysisResults = new BasisAnalysisService(schemeCtx).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything);

                    var historicContext = TestContextHelper.GetBasisContext(t, atDate: t.Results.AttributionStart);
                    var context = TestContextHelper.GetBasisContext(t, r: analysisResults);

                    var actual = new BeforeAfter<IAS19Disclosure>(
                        new IAS19Calculator(schemeCtx, historicContext, t.Results.AttributionStart).Get(IAS19Calculator.CalculationMode.SummaryOnly),
                        new IAS19Calculator(schemeCtx, context, t.Results.AttributionEnd, t.Results.AttributionStart).Get(IAS19Calculator.CalculationMode.Full, false));

                    Action<IAS19Disclosure, IAS19Disclosure, bool> test = (e, r, before) =>
                    {
                        var name = before ? "Before" : "After";
                        Assert.AreEqual(e.Date, r.Date, t.ToString());
                        Assert.That(r.Assumptions, Is.EqualTo(e.Assumptions), t.ToString("Assumptions " + name));
                        Assert.That(r.BalanceSheetAmounts, Is.EqualTo(e.BalanceSheetAmounts), t.ToString("BalanceSheetAmounts " + name));
                        if (!before)
                        {
                            Assert.That(r.DefinedBenefitObligation, Is.EqualTo(e.DefinedBenefitObligation), t.ToString("DefinedBenefitObligation " + name));
                            Assert.That(r.FairValueSchemeAssetChanges, Is.EqualTo(e.FairValueSchemeAssetChanges), t.ToString("FairValueSchemeAssetChanges " + name));
                            Assert.That(r.SchemeSurplusChanges, Is.EqualTo(e.SchemeSurplusChanges), t.ToString("SchemeSurplusChanges " + name));
                            Assert.That(r.PAndLForecast, Is.EqualTo(e.PAndLForecast), t.ToString("PAndLForecast " + name));
                        }
                    };

                    test(t.Results.IAS19DisclosureBefore, actual.Before, true);
                    test(t.Results.IAS19DisclosureAfter, actual.After, false);
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestFRS17Disclosure(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (t.Scheme.AccountingFRS17Visible)
                {
                    var schemeCtx = TestContextHelper.GetSchemeContext(t);
                    var analysisResults = new BasisAnalysisService(schemeCtx).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything);
                    var context = TestContextHelper.GetBasisContext(t, r: analysisResults);
                    var calculator = new FRS17Calculator(schemeCtx, context, TestContextHelper.GetBasesDataManager(t), TestContextHelper.GetPortfolioManager(t), t.Results.AttributionStart, t.Results.AttributionEnd);

                    var actual = new BeforeAfter<FRS17Disclosure>(
                        calculator.GetStartPositionSummary(),
                        calculator.Get(false));

                    Action<FRS17Disclosure, FRS17Disclosure, bool> test = (e, r, before) =>
                    {
                        var name = before ? "Before" : "After";
                        Assert.AreEqual(e.Date, r.Date, t.ToString());
                        Assert.That(r.Assumptions, Is.EqualTo(e.Assumptions), t.ToString("Assumptions " + name));
                        Assert.That(r.BalanceSheetAmounts, Is.EqualTo(e.BalanceSheetAmounts), t.ToString("BalanceSheetAmounts " + name));
                        if (!before)
                        {
                            Assert.That(r.PresentValueSchemeLiabilities, Is.EqualTo(e.PresentValueSchemeLiabilities), t.ToString("PresentValueSchemeLiabilities " + name));
                            Assert.That(r.FairValueSchemeAssetChanges, Is.EqualTo(e.FairValueSchemeAssetChanges), t.ToString("FairValueSchemeAssetChanges " + name));
                            Assert.That(r.SchemeSurplusChanges, Is.EqualTo(e.SchemeSurplusChanges), t.ToString("SchemeSurplusChanges " + name));
                            Assert.That(r.PAndLForecast, Is.EqualTo(e.PAndLForecast), t.ToString("PAndLForecast " + name));
                        }
                    };

                    test(t.Results.FRS17DisclosureBefore, actual.Before, true);
                    test(t.Results.FRS17DisclosureAfter, actual.After, false);
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestUSGAAPInitialisation(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (t.Scheme.AccountingUSGAAPVisible)
                {
                    var expected = new USGAAPInitData(t.Results.USGAAPTable.ToDictionary(x => x.Date, x => x.AOCIData)) { ExpectedReturns = t.Results.USGAAPExpectedReturns.ToDictionary(x => x.USGAAPExpectedReturnsType, x => x.Value) };
                    var portfolioManager = TestContextHelper.GetPortfolioManager(t);
                    var masterBasis = new MasterBasis(t.Scheme.UnderlyingBases.Where(x => x.Type == basisType));
                    var ctx = TestContextHelper.GetBasisContext(t);
                    ctx.Results = new BasisAnalysisResults { UserPenBuyIn = t.Results.UserLiabs.PensionerBuyin, UserDefBuyIn = t.Results.UserLiabs.DeferredBuyin };
                    var actual = new USGAAPInitCalculator(TestContextHelper.GetSchemeContext(t), ctx, masterBasis, portfolioManager).Calculate(t.Results.AttributionStart, t.Results.AttributionEnd);

                    //USGAAPTable
                    Assert.That(actual.AOCITable.Keys, Is.EquivalentTo(expected.AOCITable.Keys), t.ToString());
                    foreach (var key in expected.AOCITable.Keys)
                        Assert.AreEqual(expected.AOCITable[key], actual.AOCITable[key], t.ToString("key: " + key.ToString()));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestUSGAAPProfitAndLoss(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (t.Scheme.AccountingUSGAAPVisible)
                {
                    var context = TestContextHelper.GetBasisContext(t);
                    context.Results = new BasisAnalysisResults
                    {
                        UserPenBuyIn = t.Results.UserLiabs.PensionerBuyin,
                        UserDefBuyIn = t.Results.UserLiabs.DeferredBuyin,
                        Cashflow = t.Results.CashflowsAfter.ToDictionary(cf => cf.Year, cf => (IDictionary<MemberStatus, double>)cf.MemberStatuses.ToDictionary(ms => ms.Key, ms => ms.Value)),
                        JPInvGrowth = t.Results.JPInvGrowth,
                        JP2InvGrowth = t.Results.JP2InvGrowth,
                        UserActLiabAfter = t.Results.UserActLiabAfter,
                        UserDefLiabAfter = t.Results.UserDefLiabAfter,
                        UserPenLiabAfter = t.Results.UserPenLiabAfter,
                        InvestmentStrategy = t.Results.InvestmentStrategyResults,
                        ProfitLoss = t.Results.ProfitLoss
                    };
                    var basesDataManager = TestContextHelper.GetBasesDataManager(t);
                    var portfolioManager = TestContextHelper.GetPortfolioManager(t);
                    var evolutionData = new BasisEvolutionData
                        (
                        t.Results.LiabilityEvolution.ToDictionary(x => x.Date, x => (LiabilityEvolutionItem)x),
                        t.Results.AssetEvolution.ToDictionary(x => x.Date, x => (AssetEvolutionItem)x),
                        t.Results.LiabilityExperience.ToDictionary(x => x.Key, x => x.Value),
                        t.Results.BuyinExperience.ToDictionary(x => x.Key, x => x.Value),
                        t.Results.AssetExperience.ToDictionary(x => x.Key, x => x.Value)
                        );
                    var init = new USGAAPInitData(t.Results.USGAAPTable.ToDictionary(x => x.Date, x => x.AOCIData)) { ExpectedReturns = t.Results.USGAAPExpectedReturns.ToDictionary(x => x.USGAAPExpectedReturnsType, x => x.Value) };
                    var calculator = new USGAAPCalculator(TestContextHelper.GetSchemeContext(t), init, evolutionData, basesDataManager, portfolioManager, context.Results);

                    var actual = new BeforeAfter<USGAAPDisclosure>(
                        calculator.GetStartPositionSummary(t.Results.AttributionStart, t.Results.AttributionEnd),
                        calculator.Get(context, true));

                    Action<USGAAPDisclosure, USGAAPDisclosure, string> test = (e, r, name) =>
                    {
                        Assert.That(r.NetPeriodPensionCostForecast ?? new USGAAPDisclosure.NetPeriodicPensionCostForecastData(0, 0, 0, 0, 0, 0),
                            Is.EqualTo(e.NetPeriodPensionCostForecast), t.ToString("NetPeriodPensionCostForecast" + name));
                    };

                    test(t.Results.USGAAPDisclosureBefore, actual.Before, t.ToString("[Before]"));
                    test(t.Results.USGAAPDisclosureAfter, actual.After, t.ToString("[After]"));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestUSGAAPDisclosure(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (t.Scheme.AccountingUSGAAPVisible)
                {
                    var context = TestContextHelper.GetBasisContext(t);
                    context.Results = new BasisAnalysisResults
                    {
                        UserPenBuyIn = t.Results.UserLiabs.PensionerBuyin,
                        UserDefBuyIn = t.Results.UserLiabs.DeferredBuyin,
                        Cashflow = t.Results.CashflowsBefore.ToDictionary(cf => cf.Year, cf => (IDictionary<MemberStatus, double>)cf.MemberStatuses.ToDictionary(ms => ms.Key, ms => ms.Value)),
                        JPInvGrowth = t.Results.JPInvGrowth,
                        JP2InvGrowth = t.Results.JP2InvGrowth,
                        UserActLiabAfter = t.Results.UserActLiabAfter,
                        UserDefLiabAfter = t.Results.UserDefLiabAfter,
                        UserPenLiabAfter = t.Results.UserPenLiabAfter,
                        InvestmentStrategy = t.Results.InvestmentStrategyResults,
                        ProfitLoss = t.Results.ProfitLoss
                    };
                    var basesDataManager = TestContextHelper.GetBasesDataManager(t);
                    var portfolioManager = TestContextHelper.GetPortfolioManager(t);
                    var evolutionData = new BasisEvolutionData
                        (
                        t.Results.LiabilityEvolution.ToDictionary(x => x.Date, x => (LiabilityEvolutionItem)x),
                        t.Results.AssetEvolution.ToDictionary(x => x.Date, x => (AssetEvolutionItem)x),
                        t.Results.LiabilityExperience.ToDictionary(x => x.Key, x => x.Value),
                        t.Results.BuyinExperience.ToDictionary(x => x.Key, x => x.Value),
                        t.Results.AssetExperience.ToDictionary(x => x.Key, x => x.Value)
                        );
                    var init = new USGAAPInitData(t.Results.USGAAPTable.ToDictionary(x => x.Date, x => x.AOCIData)) { ExpectedReturns = t.Results.USGAAPExpectedReturns.ToDictionary(x => x.USGAAPExpectedReturnsType, x => x.Value) };
                    var calculator = new USGAAPCalculator(TestContextHelper.GetSchemeContext(t), init, evolutionData, basesDataManager, portfolioManager, context.Results);

                    var actual = new BeforeAfter<USGAAPDisclosure>(
                        calculator.GetStartPositionSummary(t.Results.AttributionStart, t.Results.AttributionEnd),
                        calculator.Get(context, true));

                    Action<USGAAPDisclosure, USGAAPDisclosure, bool> test = (e, r, before) =>
                    {
                        var name = before ? "Before" : "After";
                        Assert.AreEqual(e.Date, r.Date, t.ToString());
                        Assert.That(r.Assumptions1, Is.EqualTo(e.Assumptions1), t.ToString("Assumptions1 " + name));
                        Assert.That(r.BalanceSheetAmounts, Is.EqualTo(e.BalanceSheetAmounts), t.ToString("BalanceSheetAmounts " + name));
                        if (!before)
                        {
                            Assert.That(r.Assumptions2, Is.EqualTo(e.Assumptions2), t.ToString("Assumptions2 " + name));
                            Assert.That(r.ChangesInPresentValue, Is.EqualTo(e.ChangesInPresentValue), t.ToString("ChangesInPresentValue " + name));
                            Assert.That(r.ChangesInFairValue, Is.EqualTo(e.ChangesInFairValue), t.ToString("ChangesInFairValue " + name));
                            Assert.That(r.AccumulatedBenefitObligation, Is.EqualTo(e.AccumulatedBenefitObligation), t.ToString("AccumulatedBenefitObligation " + name));
                            Assert.That(r.EstimatedPayments, Is.EqualTo(e.EstimatedPayments), t.ToString("EstimatedPayments " + name));
                            Assert.That(r.NetPeriodicPensionCost, Is.EqualTo(e.NetPeriodicPensionCost), t.ToString("NetPeriodicPensionCost " + name));
                            Assert.That(r.ComprehensiveIncome, Is.EqualTo(e.ComprehensiveIncome), t.ToString("ComprehensiveIncome " + name));
                            Assert.That(r.ComprehensiveAccumulatedIncome, Is.EqualTo(e.ComprehensiveAccumulatedIncome), t.ToString("ComprehensiveAccumulatedIncome " + name));
                            Assert.That(r.NetPeriodPensionCostForecast, Is.EqualTo(e.NetPeriodPensionCostForecast), t.ToString("NetPeriodPensionCostForecast " + name));
                        }
                    };

                    test(t.Results.USGAAPDisclosureBefore, actual.Before, true);
                    test(t.Results.USGAAPDisclosureAfter, actual.After, false);
                }
            }
        }
    }
}
