﻿namespace FlightDeck.Domain.UnitTests.LiabilityCashflowTests
{
    using FlightDeck.Domain.Accounting;
    using FlightDeck.Domain.Analysis;
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.Domain.JourneyPlan;
    using FlightDeck.Domain.LDI;
    using FlightDeck.Domain.UnitTests.TestParameterTypes;
    using FlightDeck.Domain.VaR;
    using FlightDeck.DomainShared;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

#if DEBUG
    [TestFixture(false, BasisType.Accounting, false)]
#else
    [TestFixture(true, null, false)]
#endif
    public class DomainCalculationTests
    {
        #region setup

        private class SchemeTestStructure
        {
            public TrackerReader Reader { get; set; }
            public PensionScheme CachedScheme { get; set; }
            public int TestCaseID { get; set; }

            public SchemeTestStructure(TrackerReader reader, PensionScheme cachedPensionScheme, int testCaseID)
            {
                Reader = reader;
                CachedScheme = cachedPensionScheme;
                TestCaseID = testCaseID;
            }
        }

        private readonly bool testcasesEnabled;
        private readonly BasisType? basisType;
        private readonly bool leaveCopy;
        private List<string> trackerFilePaths = new List<string>();
        private Dictionary<string, BasisType> pathBasisTypes = new Dictionary<string, BasisType>();

        bool useCache = false;
        private Dictionary<BasisType, List<SchemeTestStructure>> cache;


        /// <summary>
        /// Check the BasisType is available on ANY path.
        /// </summary>
        private bool IsBasisTypeAvailable(BasisType basisType)
        {
            return pathBasisTypes.Values.Contains(basisType);
        }

        /// <summary>
        /// Check the BasisType is available for the specified path
        /// </summary>
        private bool IsBasisTypeAvailable(string path, BasisType basisType)
        {
            return pathBasisTypes.ContainsKey(path) && pathBasisTypes[path] == basisType;
        }

        private IEnumerable<SchemeTestStructure> GetTestStructures(BasisType basisType)
        {
            var testStructures = new List<SchemeTestStructure>();

            foreach (var path in trackerFilePaths)
            {
                if (!IsBasisTypeAvailable(path, basisType))
                    continue;

                var rdr = new TrackerReader(path);
                var testCaseID = getTestCaseId(path);
                var testStructure = new SchemeTestStructure(rdr, rdr.GetPensionScheme(), testCaseID);

                yield return testStructure;
            }
        }

        private int getTestCaseId(string path)
        {
            return int.Parse(path.Substring(path.LastIndexOf("-ID") + "-ID".Length).Replace(".xlsm", ""));
        }

        public DomainCalculationTests(bool testcasesEnabled, BasisType? basisType = null, bool leaveCopy = false)
        {
            this.testcasesEnabled = testcasesEnabled;
            this.basisType = basisType;
            this.leaveCopy = leaveCopy;
        }

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            /*
             * Ok, what's going on here isn't immediately obvious....
             * These tests can be run against one single instance of a Tracker file using one specific selected basis type, 
             * ie, in the case of developing locally and testing against the Tracker you're working with. 
             * But if we want to run against multiple schemes with multiple basis types then we still want to group everything
             * by basis type because that's how our tests are setup, ie, [TestCase(BasisType.Accounting)]. So in the case
             * of multiple scenarios we're creating a new Tracker file for each scheme-basis-testcase scenario, then putting
             * them in to the cache grouped by basis type. 
             * One other thing happening is that initialising a scheme takes a while, so rather than having each test do the initialisation
             * and duplicating the effort (and time), we're calling the initialisation just once here in the setup, ie rdr.GetPensionScheme().
             */

            bool.TryParse(ConfigurationManager.AppSettings["UseCache"], out useCache);

            if (useCache)
                cache = new Dictionary<BasisType, List<SchemeTestStructure>>();

            if (testcasesEnabled)
            {
                var testCasesByScheme = new TestParameterReader().ReadAll().ToLookup(x => x.SchemeName);

                foreach (var testCases in testCasesByScheme)
                {
                    var schemeName = testCases.Key;

                    var wtr = new TrackerBulkWriter(TestUtils.GetTrackerFilePath());

                    var schemeXmlPath = Path.Combine(TestUtils.GetTrackerDir(), "schemes", schemeName + ".xml");

                    wtr.InitialiseScheme(schemeXmlPath);

                    foreach (var testCasesForBasis in testCases.GroupBy(x => x.BasisName))
                    {
                        var basisName = testCasesForBasis.Key;

                        wtr.InitialiseBasis(basisName);

                        foreach (var testCase in testCasesForBasis)
                        {
                            wtr.LoadParameters(testCase);

                            var testCasePath = Path.Combine(TestUtils.GetTrackerDir(), string.Format("{0}({1})-ID{2}.xlsm", schemeName, basisName, testCase.Id.ToString()));

                            wtr.Save(testCasePath);

                            trackerFilePaths.Add(testCasePath);
                            pathBasisTypes.Add(testCasePath, new TrackerReader(testCasePath).BasisType);
                        }
                    }

                    wtr.Quit();
                }

                if (useCache)
                    foreach (var path in trackerFilePaths)
                    {
                        var rdr = new TrackerReader(path);
                        var testCaseID = getTestCaseId(path);
                        var testStructure = new SchemeTestStructure(rdr, rdr.GetPensionScheme(), testCaseID);

                        if (!cache.ContainsKey(rdr.BasisType))
                            cache.Add(rdr.BasisType, new List<SchemeTestStructure> { testStructure });
                        else
                            cache[rdr.BasisType].Add(testStructure);
                    }
            }
            else if (useCache)
            {
                var rdr = new TrackerReader(TestUtils.GetTrackerFilePath());
                cache.Add(basisType.GetValueOrDefault(BasisType.Accounting),
                    new List<SchemeTestStructure> { new SchemeTestStructure(rdr, rdr.GetPensionScheme(), 1) });
            }
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            if (testcasesEnabled)
            {
                foreach (var path in trackerFilePaths)
                {
                    if (leaveCopy)
                    {
                        var copypath = string.Format("{0}-{1}.{2}", Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path)), DateTime.Now.ToString("yyyyMMddHHmm"), Path.GetExtension(path));
                        File.Copy(path, copypath);
                    }
                    File.Delete(path);
                }
            }
        }

        #endregion

        #region main

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestGrowthOverPeriodInfo(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (!t.Reader.IsFunded)
                    continue;

                var expected = t.Reader.GrowthOverPeriodInfo.OrderBy(x => x.Key).ToList();
                var actual = t.CachedScheme.GetGrowthOverPeriod(t.Reader.AttributionStart, t.Reader.AttributionEnd).OrderBy(x => x.Name).ToList();

                Assert.AreEqual(expected.Count, actual.Count, testcaseToString(t, "List count different"));
                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Key, actual[i].Name, testcaseToString(t, "Name"));
                    Assert.That(actual[i].Growth, Is.EqualTo(expected[i].Value).Within(Utils.TestPrecision), testcaseToString(t, "Growth"));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestTotalCosts(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var basesManager = getBasesDataManager(t, basisType);
                foreach(var basis in t.Reader.Bases.Where(x => x.Type == basisType))
                {
                    var expected = basis.TotalCosts;
                    var effectiveDate = basis.EffectiveDate;
                    var actual = basesManager.GetBasis(effectiveDate).TotalCosts;
                    Assert.AreEqual(expected, actual, testcaseToString(t, "effective date: " + effectiveDate));
                }
            }
        }

        #endregion

        #region evolution

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestLiabilityAssumptionEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.LiabilityAssumptionEvolution;
                var actual = new LiabilityAssumptionEvolutionService(getBasesDataManager(t, basisType)).Calculate(expected.Keys.First(), expected.Keys.Last());

                Assert.AreEqual(expected.Count, actual.Count, testcaseToString(t));
                foreach (var key in expected.Keys)
                    foreach (var type in expected[key].Keys)
                        Assert.That(actual[key][type], Is.EqualTo(expected[key][type]).Within(Utils.Precision), testcaseToString(t, "key: " + key.ToString()));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestPensionIncreaseEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.PensionIncreaseEvolution;
                var actual = new PensionIncreaseEvolutionService(getBasesDataManager(t, basisType)).Calculate(expected.Keys.First(), expected.Keys.Last());

                Assert.AreEqual(expected.Count, actual.Count, testcaseToString(t));
                foreach (var key in expected.Keys)
                    foreach (var type in expected[key].Keys)
                        Assert.That(actual[key][type], Is.EqualTo(expected[key][type]).Within(Utils.Precision), testcaseToString(t, "key: " + key.ToString()));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestLiabilityEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.LiabilityEvolution;
                var actual = new LiabilityEvolutionService().Calculate(expected.Keys.First(), expected.Keys.Last(), getBasesDataManager(t, basisType), t.Reader.LiabilityAssumptionEvolution, t.Reader.PensionIncreaseEvolution)
                    .ToDictionary(x => x.Date);

                Assert.AreEqual(expected.Count, actual.Count(), testcaseToString(t));
                foreach (var key in expected.Keys)
                    Assert.AreEqual(expected[key], actual[key], testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestGiltBasisLiability(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expectedLdiGilt = t.Reader.GiltBasisLiability;
                var actualLdiGilt = new LDI.GiltLiabilityCalculator(getSchemeContext(t)).Calculate(getBasisContext(t, basisType));
                Assert.That(actualLdiGilt.TotalLiab, Is.EqualTo(expectedLdiGilt.TotalLiab).Within(Utils.TestPrecision), testcaseToString(t, "TotalLiab"));
                Assert.That(actualLdiGilt.LiabDuration, Is.EqualTo(expectedLdiGilt.LiabDuration).Within(Utils.TestPrecision), testcaseToString(t, "LiabDuration"));
                Assert.That(actualLdiGilt.LiabPV01, Is.EqualTo(expectedLdiGilt.LiabPV01).Within(Utils.TestPrecision), testcaseToString(t, "LiabPV01"));
                Assert.That(actualLdiGilt.LiabIE01, Is.EqualTo(expectedLdiGilt.LiabIE01).Within(Utils.TestPrecision), testcaseToString(t, "LiabIE01"));
                Assert.That(actualLdiGilt.BuyInPV01, Is.EqualTo(expectedLdiGilt.BuyInPV01).Within(Utils.TestPrecision), testcaseToString(t, "BuyInPV01"));
                Assert.That(actualLdiGilt.BuyInIE01, Is.EqualTo(expectedLdiGilt.BuyInIE01).Within(Utils.TestPrecision), testcaseToString(t, "BuyInIE01"));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestLDIEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            var candidates = 0;
            var ignoredCandidates = 0;
            foreach (var t in getTestCandidates(basisType))
            {
                if (t.Reader.SchemeAssets.Any(x => x.LDIInForce))
                {
                    candidates++;

                    var readerData = t.Reader.LDIEvolutionData;
                    var expectedLdiEvolution = readerData.ToDictionary(x => x.Date);

                    var ldiBasesDataManager = getBasesDataManager(t, t.Reader.LDIBasis);

                    var liabHedgingData = new LDI.LDIEvolutionService(ldiBasesDataManager, t.CachedScheme).GetLiabilityHedgingData(t.Reader.RefreshDate);
                    var actualLdiEvolution = liabHedgingData.ToDictionary(x => x.Date);

                    // fusion works out ldi data from ldi basis anchor date, tracker only starts at selected basis anchor date, so need to adjust fusion results accordingly
                    var anchorDate = t.Reader.Bases.Where(b => b.Type == basisType).Min(b => b.EffectiveDate);
                    if (actualLdiEvolution.Keys.Min() < anchorDate)
                    {
                        while (actualLdiEvolution.Keys.Min() < anchorDate)
                            actualLdiEvolution.Remove(actualLdiEvolution.Keys.Min());
                        actualLdiEvolution[actualLdiEvolution.Keys.Min()] = new LDIEvolutionItem(actualLdiEvolution.Keys.Min(), 0, 0);
                    }

                    // testing ldi evolution data                    
                    Assert.AreEqual(expectedLdiEvolution.Count, actualLdiEvolution.Count(), testcaseToString(t));
                    foreach (var key in expectedLdiEvolution.Keys)
                    {
                        var debug = "key: " + key.ToString();
                        Assert.AreEqual(expectedLdiEvolution[key].Date, actualLdiEvolution[key].Date, testcaseToString(t, debug));
                        Assert.That(actualLdiEvolution[key].LDIInflationSwap, Is.EqualTo(expectedLdiEvolution[key].LDIInflationSwap).Within(Utils.TestPrecision), testcaseToString(t, debug + " [LDIInflationSwap]"));
                        Assert.That(actualLdiEvolution[key].LDIInterestSwap, Is.EqualTo(expectedLdiEvolution[key].LDIInterestSwap).Within(Utils.TestPrecision), testcaseToString(t, debug + " [LDIInterestSwap]"));
                    }
                }
                else
                    ignoredCandidates++;
            }
            if (ignoredCandidates > 0 && candidates == 0)
                    Assert.Ignore("LDI not in force for {0} test candidates", ignoredCandidates);
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestAssetEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.AssetEvolution;

                var actual = new AssetEvolutionFactory().GetAssetEvolutionService(t.CachedScheme).Calculate(expected.Keys.First(), expected.Keys.Last(), getPortfolioManager(t, basisType), t.Reader.LDIEvolutionData.ToDictionary(x => x.Date))
                                .ToDictionary(x => x.Date);

                Assert.AreEqual(expected.Count, actual.Count(), testcaseToString(t));
                foreach (var key in expected.Keys)
                    Assert.AreEqual(expected[key], actual[key], testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestLiabilityExperience(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.LiabilityExperience;
                var actual = new LiabilityExperienceService().CalculateLiabilityStepChange(getBasesDataManager(t, basisType), t.Reader.LiabilityEvolution)[LiabilityExperienceService.LiabilityExperienceType.Liab];

                Assert.AreEqual(expected.Count, actual.Count, testcaseToString(t));
                foreach (var key in expected.Keys)
                    Assert.True(Utils.EqualityCheck(expected[key], actual[key]), testcaseToString(t, " key: " + key.ToString()));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestBuyinExperience(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.BuyinExperience;
                var actual = new LiabilityExperienceService().CalculateLiabilityStepChange(getBasesDataManager(t, basisType), t.Reader.LiabilityEvolution)[LiabilityExperienceService.LiabilityExperienceType.Buyin];

                Assert.AreEqual(expected.Count, actual.Count, testcaseToString(t));
                foreach (var key in expected.Keys)
                    Assert.True(Utils.EqualityCheck(expected[key], actual[key]), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestAssetExperience(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.AssetExperience;
                var actual = new AssetExperienceService().CalculateStepChange(getPortfolioManager(t, basisType), t.Reader.AssetEvolution, t.Reader.LDIEvolutionData.ToDictionary(x => x.Date));

                Assert.AreEqual(expected.Count, actual.Count, testcaseToString(t));
                foreach (var key in expected.Keys)
                    Assert.True(Utils.EqualityCheck(expected[key], actual[key]), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestExpectedDeficit(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.ExpectedDeficit;

                var b = t.Reader.Bases.Where(x => x.DisplayName == t.Reader.ChosenBasis).OrderBy(x => x.EffectiveDate).First();
                var schemeAssets = t.Reader.SchemeAssets.Where(x => x.EffectiveDate == (t.Reader.SchemeAssets.Min(y => y.EffectiveDate))).First();
                var assets = schemeAssets.Assets.ToDictionary(x => x.Class.Type, x => x.DefaultReturn);
                var actual = new ExpectedDeficitService(t.CachedScheme).Calculate(
                    t.Reader.ExpectedDeficit.Keys.First(),
                    t.Reader.ExpectedDeficit.Keys.Last(),
                    b,
                    getPortfolioManager(t, basisType).GetPortfolio(t.Reader.ExpectedDeficit.Keys.First()),
                    new BasisEvolutionData(t.Reader.LiabilityEvolution, t.Reader.AssetEvolution, t.Reader.LiabilityExperience, t.Reader.BuyinExperience, t.Reader.AssetExperience),
                    new BasisAssumptions(b.Assumptions, b.LifeExpectancy, b.PensionIncreases, assets, t.Reader.RecoveryPlanAssumptions));

                Assert.AreEqual(expected.Keys.Count, actual.Keys.Count, testcaseToString(t));
                foreach (var key in expected.Keys)
                {
                    Assert.That(actual[key].Assets, Is.EqualTo(expected[key].Assets).Within(1), testcaseToString(t, "key: " + key.ToString()));
                    Assert.That(actual[key].Liabilities, Is.EqualTo(expected[key].Liabilities).Within(1), testcaseToString(t, "key: " + key.ToString()));
                    Assert.That(actual[key].FundingLevel, Is.EqualTo(expected[key].FundingLevel).Within(1), testcaseToString(t, "key: " + key.ToString()));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestRefreshDateIsSmallestOfAllMaxIndexDates(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.RefreshDate;
                var actual = t.CachedScheme.RefreshDate;
                Assert.That(actual, Is.EqualTo(expected), testcaseToString(t));
            }
        }

        #endregion

        #region analysis

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestUserLiabs(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.UserLiabs;
                var actual = new UserLiabsCalculator().Calculate(getBasisContext(t, basisType));

                Assert.AreEqual(expected, actual, testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestOldJourneyPlanBefore(BasisType basisType) // Old Journey Plan = Funding Progression
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.JourneyPlan.Before;
                var ctx = getBasisContext(t, basisType);
                var actual = new JourneyPlanBeforeService(getSchemeContext(t)).Calculate(ctx);

                //yearly totals
                foreach (var key in expected.Keys)
                    Assert.AreEqual(expected[key], new BalanceData(actual.YearlyTotals[key].TotalAssets, actual.YearlyTotals[key].TotalLiability), testcaseToString(t, "year: " + key.ToString()));
                //jpinvgrowth
                Assert.That(actual.YearlyTotals.Last().Value.Growth, Is.EqualTo(t.Reader.JPInvGrowth).Within(Utils.TestPrecision), testcaseToString(t, "JPInvGrowth"));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestRecoveryPlan(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (!t.Reader.IsFunded)
                    continue;

                var expected = t.Reader.RecoveryPlanLens;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).RecoveryPlanPayments;

                // test existing recovery plan
                var oldRP = actual.Values.Select(x => x.Before).ToList();
                Assert.That(oldRP.Sum(), Is.EqualTo(expected.Before.Sum()).Within(Utils.TestPrecision), testcaseToString(t, "Existing recovery plan total"));
                for (int i = 0; i < expected.Before.Count; i++)
                    Assert.That(oldRP[i], Is.EqualTo(expected.Before[i]).Within(Utils.Precision), testcaseToString(t, "Existing recovery plan"));

                // test new recovery plan
                var newRP = actual.Values.Select(x => x.After).ToList();
                Assert.That(newRP.Sum(), Is.EqualTo(expected.After.Sum()).Within(Utils.TestPrecision), testcaseToString(t, "New recovery plan total"));
                for (int i = 0; i < expected.After.Count; i++)
                    Assert.That(newRP[i], Is.EqualTo(expected.After[i]).Within(Utils.Precision), testcaseToString(t, "New recovery plan"));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestOutPerformanceMaxReturn(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.OutPerformanceMaxReturn;

                var b = getBasisContext(t, basisType);
                var s = getSchemeContext(t);

                b.Results = new BasisAnalysisResults
                {
                    UserPenBuyIn = t.Reader.UserPenBuyIn,
                    UserDefBuyIn = t.Reader.UserDefBuyIn,
                    InvestmentStrategy = t.Reader.InvestmentStrategyData,
                    Insurance = t.Reader.BuyinInfo
                };

                var actual = new WeightedReturnsCalculator().GetMaxReturn(s, b);
                Assert.That(actual, Is.EqualTo(expected).Within(Utils.Precision), testcaseToString(t, "Outperformance return"));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestOldJourneyPlanAfter(BasisType basisType) // Old Journey Plan = Funding Progression
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.JourneyPlan.After;
                var expectedLiabDuration = t.Reader.JPAfterLiabDuration;
                var ctx = getBasisContext(t, basisType);
                ctx.Results = new BasisAnalysisResults
                {
                    PIEInit = t.Reader.PIEInit,
                    RecoveryPlanPaymentRequired = t.Reader.RecoveryPlanPaymentRequired,
                    InvestmentStrategy = t.Reader.InvestmentStrategyData, //bypass the SyntheticConversion cyclical reference bug in Tracker
                    EFRO = t.Reader.EFroData
                };

                var actual = new JourneyPlanAfterService(getSchemeContext(t)).Calculate(ctx);

                foreach (var key in expected.Keys)
                    Assert.AreEqual(expected[key], new BalanceData(actual.YearlyTotals[key].TotalAssets, actual.YearlyTotals[key].TotalLiability), testcaseToString(t, "year " + key.ToString()));

                Assert.That(actual.LiabDuration, Is.EqualTo(expectedLiabDuration).Within(Utils.Precision), testcaseToString(t, "JP2LiabDuration"));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestCashflowLens(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.CashFlowLens.After;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).Cashflow;

                foreach (var key in expected.Keys)
                {
                    Assert.That(actual[key][MemberStatus.ActivePast], Is.EqualTo(expected[key][MemberStatus.ActivePast]).Within(Utils.TestPrecision), testcaseToString(t));
                    Assert.That(actual[key][MemberStatus.Deferred], Is.EqualTo(expected[key][MemberStatus.Deferred]).Within(Utils.TestPrecision), testcaseToString(t));
                    Assert.That(actual[key][MemberStatus.Pensioner], Is.EqualTo(expected[key][MemberStatus.Pensioner]).Within(Utils.TestPrecision), testcaseToString(t));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestSurplusAnalysis(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.SurplusAnalysisData;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).SurplusAnalysis;

                Assert.That(actual, Is.EqualTo(expected), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestFundingLevelData(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.FundingLevel;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).FundingLevel;

                Assert.That(actual, Is.EqualTo(expected).Within(Utils.Precision), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestInvestmentStrategy(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.InvestmentStrategyData;
                var ctx = getBasisContext(t, basisType);
                ctx.Results = new BasisAnalysisResults
                {
                    AssetsAtSED = t.Reader.AssetsAtSED,
                    BuyInAtAnalysisDate = t.Reader.BuyInAtAnalysisDate,
                    UserPenBuyIn = t.Reader.UserPenBuyIn,
                    UserDefBuyIn = t.Reader.UserDefBuyIn,
                    FRO = t.Reader.FroData,
                    ETV = t.Reader.EtvData,
                    Insurance = t.Reader.BuyinInfo
                };
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).InvestmentStrategy;

                Assert.That(actual, Is.EqualTo(expected), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestHedgeBreakdown(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.HedgeBreakdown;
                var ctx = getBasisContext(t, basisType);
                ctx.Results = new BasisAnalysisResults
                {
                    FundingLevel = t.Reader.FundingLevel,
                    InvestmentStrategy = t.Reader.InvestmentStrategyData,
                    GiltBasisLiabilityData = t.Reader.GiltBasisLiability
                };

                var actual = new HedgeAnalysisCalculator(getSchemeContext(t), ctx).Calculate().Breakdown;
                Assert.That(actual, Is.EqualTo(expected), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestAssetHedgeBreakdownData(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (!t.Reader.IsFunded)
                    continue;

                var schemeContext = getSchemeContext(t);
                foreach (var assets in schemeContext.Data.SchemeAssets)
                {
                    if (assets.EffectiveDate >= t.Reader.Bases.Where(b => b.Type == basisType).Min(b => b.EffectiveDate) && // asset sheet is not before the basis starts
                        assets.EffectiveDate <= t.Reader.AttributionEnd) // asset sheet is before the current analysis period
                    {
                        var atDate = assets.EffectiveDate;
                        var expected = t.Reader.AssetHedgeBreakdownData[atDate];
                        var basisCtx = getBasisContext(t, basisType, null, null, atDate);

                        var actualHedgeData = new AssetHedgeCalculator(getSchemeContext(t)).CalculateExistingHedge(atDate, basisCtx.Data, getPortfolioManager(t, basisType));

                        Assert.That(actualHedgeData.CurrentBasis[HedgeType.Interest].Total,
                                    Is.EqualTo(expected.CurrentBasis[HedgeType.Interest].Total).Within(Utils.TestPrecision), testcaseToString(t, "Date: " + atDate.ToString()));
                        Assert.That(actualHedgeData.CurrentBasis[HedgeType.Inflation].Total,
                                    Is.EqualTo(expected.CurrentBasis[HedgeType.Inflation].Total).Within(Utils.TestPrecision), testcaseToString(t, "Date: " + atDate.ToString()));
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Interest].Total,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Interest].Total).Within(Utils.TestPrecision), testcaseToString(t, "Date: " + atDate.ToString()));
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Inflation].Total,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Inflation].Total).Within(Utils.TestPrecision), testcaseToString(t, "Date: " + atDate.ToString()));
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Interest].Buyin,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Interest].Buyin).Within(Utils.TestPrecision), testcaseToString(t, "Date: " + atDate.ToString()));
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Inflation].Buyin,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Inflation].Buyin).Within(Utils.TestPrecision), testcaseToString(t, "Date: " + atDate.ToString()));
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Interest].PhysicalAssets,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Interest].PhysicalAssets).Within(Utils.TestPrecision), testcaseToString(t, "Date: " + atDate.ToString()));
                        Assert.That(actualHedgeData.Cashflows[HedgeType.Inflation].PhysicalAssets,
                                    Is.EqualTo(expected.Cashflows[HedgeType.Inflation].PhysicalAssets).Within(Utils.TestPrecision), testcaseToString(t, "Date: " + atDate.ToString()));
                    }
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestFRO(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.FroData;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).FRO;

                Assert.That(actual, Is.EqualTo(expected), t.Reader.Name);
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestEmbeddedFRO(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.EFroData;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).EFRO;

                Assert.That(actual, Is.EqualTo(expected), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestETV(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.EtvData;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).ETV;

                Assert.That(actual, Is.EqualTo(expected), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestPIE(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.PieData;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).PIE;

                Assert.That(actual, Is.EqualTo(expected), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestFutureBenefits(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (t.Reader.SchemeAssumptions.FROType != FROType.Embedded)
                {
                    var expected = t.Reader.FutureBenefitsData;
                    var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).FutureBenefits;

                    Assert.That(actual, Is.EqualTo(expected), testcaseToString(t));
                }
            }
        }

        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestABF(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.AbfData;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).ABF;

                Assert.That(actual, Is.EqualTo(expected), testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestVarWaterfall(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.Waterfall;

                var schemeCtx = getSchemeContext(t);
                var ctx = getBasisContext(t, basisType);
                ctx.Results = new BasisAnalysisResults
                {
                    PIEInit = t.Reader.PIEInit,
                    RecoveryPlanPaymentRequired = t.Reader.RecoveryPlanPaymentRequired,
                    FundingLevel = t.Reader.FundingLevel,
                    InvestmentStrategy = t.Reader.InvestmentStrategyData,
                    GiltBasisLiabilityData = t.Reader.GiltBasisLiability
                };
                ctx.Results.Hedging = new HedgeAnalysisCalculator(schemeCtx, ctx).Calculate(); //not great, can we get HedgeData directly from tracker? 
                ctx.Results.VaRInputs = new VaRInputsCalculator(schemeCtx).Calculate(ctx); //not great, can we get VaRInputs directly from tracker? 
                var actual = new WaterfallCalculator(schemeCtx, ctx).Calculate();

                Assert.AreEqual(expected, actual, testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestVarFunnel(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.VarFunnelData;

                var ctx = getBasisContext(t, basisType);
                ctx.Results = new BasisAnalysisResults
                {
                    AssetsAtSED = t.Reader.AssetsAtSED,
                    FundingLevel = t.Reader.FundingLevel,
                    VarWaterfall = t.Reader.Waterfall,
                    JourneyPlanAfter = t.Reader.JourneyPlan.After,
                    InvestmentStrategy = t.Reader.InvestmentStrategyData
                };
                var actual = new FunnelCalculator(getSchemeContext(t)).Calculate(ctx);

                Assert.AreEqual(expected.Count, actual.Count, testcaseToString(t));
                for (int i = 0; i < expected.Count; i++)
                    Assert.AreEqual(expected[i], actual[i], testcaseToString(t, "idx: " + i.ToString()));
            }
        }


        [TestCase(BasisType.Buyout)]
        public void TestInsurance(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader;
                var schemeCtx = getSchemeContext(t);
                var actual = new BasisAnalysisService(schemeCtx).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything);

                Assert.That(actual.Insurance, Is.EqualTo(expected.BuyinInfo), testcaseToString(t));
                Assert.That(schemeCtx.Calculated.BuyinCost.Total, Is.EqualTo(expected.TotalInsurancePremiumPaid).Within(Utils.Precision), testcaseToString(t));
                Assert.That(actual.InsurancePointChangeImpact, Is.EqualTo(expected.InsurancePointIncreaseAmount).Within(Utils.Precision), testcaseToString(t));

                var refresh = t.Reader.RefreshDate;
                var start = t.Reader.AttributionStart;
                var end = t.Reader.AttributionEnd;

                var basesEvolution = new Dictionary<MasterBasisKey, IDictionary<DateTime, EvolutionData>>();

                var masterBasisKeys = new List<MasterBasisKey>();

                foreach (var underlyingBasis in t.CachedScheme.UnderlyingBases)
                {
                    var masterbasis = new MasterBasis(t.Reader.Bases.Where(b => b.MasterBasisId == underlyingBasis.MasterBasisId));

                    var basesDataManager = new BasesDataManager(masterbasis.Bases.Values);
                    var basis = basesDataManager.GetBasis(end);
                    var assets = t.Reader.SchemeAssets.Where(a => a.EffectiveDate == t.Reader.SchemeAssets.Select(x => x.EffectiveDate).Distinct().Where(x => x <= end).OrderBy(x => x).Last());
                    var portfolioManager = new PortfolioManager(t.CachedScheme, basesDataManager);
                    var pf = portfolioManager.GetPortfolio(end);

                    var assumptionEvolution = new LiabilityAssumptionEvolutionService(basesDataManager).Calculate(masterbasis.AnchorDate, refresh);
                    var pincsEvolution = new PensionIncreaseEvolutionService(basesDataManager).Calculate(masterbasis.AnchorDate, refresh);
                    var liabilityEvolution = new LiabilityEvolutionService().Calculate(masterbasis.AnchorDate, refresh, basesDataManager, assumptionEvolution, pincsEvolution).ToDictionary(x => x.Date);
                    var exp = new LiabilityExperienceService().CalculateLiabilityStepChange(basesDataManager, liabilityEvolution);
                    var liabilityExperience = exp[LiabilityExperienceService.LiabilityExperienceType.Liab];
                    var buyinExperience = exp[LiabilityExperienceService.LiabilityExperienceType.Buyin];
                    var assetEvolution = new AssetEvolutionService().Calculate(masterbasis.AnchorDate, refresh, portfolioManager);

                    var evolutionData =
                        liabilityEvolution.Values.Zip(
                            assetEvolution,
                                (liab, asset) => new EvolutionData
                                    (
                                        liab.Date,
                                        liab.Act,
                                        liab.Def,
                                        liab.Pen,
                                        liab.Fut,
                                        liab.Liabs,
                                        liab.DefBuyIn,
                                        liab.PenBuyIn,
                                        liab.DiscPre,
                                        liab.DiscPost,
                                        liab.DiscPen,
                                        liab.SalInc,
                                        liab.RPI,
                                        liab.CPI,
                                        liab.Pinc1,
                                        liab.Pinc2,
                                        liab.Pinc3,
                                        liab.Pinc4,
                                        liab.Pinc5,
                                        liab.Pinc6,
                                        liab.ActDuration,
                                        liab.DefDuration,
                                        liab.PenDuration,
                                        liab.FutDuration,
                                        asset.EmployeeContributions,
                                        asset.EmployerContributions,
                                        asset.TotalBenefits,
                                        asset.NetBenefits,
                                        asset.NetIncome,
                                        asset.Growth,
                                        asset.AssetValue
                                    ))
                        .ToDictionary(x => x.Date);


                    var mbKey = new MasterBasisKey(underlyingBasis.MasterBasisId, underlyingBasis.Type, underlyingBasis.DisplayName, underlyingBasis.Id);

                    if (basesEvolution.ContainsKey(mbKey) == false)
                    {
                        masterBasisKeys.Add(mbKey);
                        basesEvolution.Add(mbKey, evolutionData);
                    }

                }

                var actualStrains = new InsuranceStrainCalculator(schemeCtx, t.Reader.AttributionEnd).Calculate(basesEvolution);

                //link expected strains to master basis ids
                var expectedStrains = new Dictionary<int, IDictionary<StrainType, double>>();
                foreach (var strain in expected.BuyoutStrains)
                {
                    expectedStrains.Add(masterBasisKeys.Where(k => k.Name == strain.Key).First().Id, strain.Value);
                }

                if (expected.BuyoutStrains != null && expected.BuyoutStrains.Count > 0)
                {
                    foreach (var actualStrain in actualStrains)
                    {
                        foreach (var k in actualStrain.Value.Keys)
                        {
                            Assert.That(actualStrain.Value[k], Is.EqualTo(expectedStrains[actualStrain.Key][k]).Within(Utils.Precision), testcaseToString(t) + " Strains. ");
                        }
                    }
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestBalanceSheetEstimate(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                var expected = t.Reader.BalanceSheetEstimateLens;
                var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).BalanceSheet;

                Assert.AreEqual(expected, actual, testcaseToString(t));
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestIAS19ProfitAndLoss(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (t.Reader.IAS19DisclosureEnabled)
                {
                    var expected = t.Reader.ProfitLossForecast[ValuationBasisType.Ias19];
                    var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).IAS19;

                    Assert.That(actual.InterestCharge, Is.EqualTo(expected.InterestCharge).Within(Utils.Precision), testcaseToString(t, "InterestCharge"));
                    Assert.That(actual.InterestCredit, Is.EqualTo(expected.InterestCredit).Within(Utils.Precision), testcaseToString(t, "InterestCredit"));
                    Assert.That(actual.SchemeAdminExpenses, Is.EqualTo(expected.SchemeExpenses).Within(Utils.Precision), testcaseToString(t, "SchemeAdminExpenses"));
                    Assert.That(actual.CurrentServiceCost, Is.EqualTo(expected.ServiceCost).Within(Utils.Precision), testcaseToString(t, "CurrentServiceCost"));
                    Assert.That(actual.Total, Is.EqualTo(expected.Total).Within(Utils.Precision), testcaseToString(t, "Total"));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestFRS17ProfitAndLoss(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (t.Reader.FRS17DisclosureEnabled)
                {
                    var expected = t.Reader.ProfitLossForecast[ValuationBasisType.Ias19]; //frs17 is now frs102 which should give the same results as ias19
                    var actual = new BasisAnalysisService(getSchemeContext(t)).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything).FRS17;

                    Assert.That(actual.InterestCharge, Is.EqualTo(expected.InterestCharge).Within(Utils.Precision), testcaseToString(t, "InterestCharge"));
                    Assert.That(actual.InterestCredit, Is.EqualTo(expected.InterestCredit).Within(Utils.Precision), testcaseToString(t, "InterestCredit"));
                    Assert.That(actual.SchemeAdminExpenses, Is.EqualTo(expected.SchemeExpenses).Within(Utils.Precision), testcaseToString(t, "SchemeAdminExpenses"));
                    Assert.That(actual.CurrentServiceCost, Is.EqualTo(expected.ServiceCost).Within(Utils.Precision), testcaseToString(t, "CurrentServiceCost"));
                    Assert.That(actual.Total, Is.EqualTo(expected.Total).Within(Utils.Precision), testcaseToString(t, "Total"));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestUSGAAPInitialisation(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (t.Reader.USGAAPDisclosureEnabled)
                {
                    var expected = new USGAAPInitData(t.Reader.USGAAPTable) { ExpectedReturns = t.Reader.USGAAPExpectedReturns };
                    var portfolioManager = getPortfolioManager(t, basisType);
                    var masterBasis = new MasterBasis(t.Reader.Bases.Where(x => x.Type == basisType));
                    var ctx = getBasisContext(t, basisType);
                    ctx.Results = new BasisAnalysisResults { UserPenBuyIn = t.Reader.UserPenBuyIn, UserDefBuyIn = t.Reader.UserDefBuyIn };
                    var actual = new USGAAPInitCalculator(getSchemeContext(t), ctx, masterBasis, portfolioManager).Calculate(t.Reader.AttributionStart, t.Reader.AttributionEnd);

                    //USGAAPTable
                    Assert.That(actual.AOCITable.Keys, Is.EquivalentTo(expected.AOCITable.Keys), testcaseToString(t));
                    foreach (var key in expected.AOCITable.Keys)
                        Assert.AreEqual(expected.AOCITable[key], actual.AOCITable[key], testcaseToString(t, "key: " + key.ToString()));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestUSGAAPProfitAndLoss(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (t.Reader.USGAAPDisclosureEnabled)
                {
                    var context = getBasisContext(t, basisType);
                    context.Results = new BasisAnalysisResults
                    {
                        UserPenBuyIn = t.Reader.UserPenBuyIn,
                        UserDefBuyIn = t.Reader.UserDefBuyIn,
                        Cashflow = t.Reader.JPCashflows,
                        JPInvGrowth = t.Reader.JPInvGrowth,
                        JP2InvGrowth = t.Reader.JP2InvGrowth,
                        UserActLiabAfter = t.Reader.UserActLiabAfter,
                        UserDefLiabAfter = t.Reader.UserDefLiabAfter,
                        UserPenLiabAfter = t.Reader.UserPenLiabAfter,
                        InvestmentStrategy = t.Reader.InvestmentStrategyData,
                        ProfitLoss = t.Reader.ProfitLoss
                    };
                    var basesDataManager = getBasesDataManager(t, basisType);
                    var portfolioManager = getPortfolioManager(t, basisType);
                    var evolutionData = new BasisEvolutionData(t.Reader.LiabilityEvolution, t.Reader.AssetEvolution, t.Reader.LiabilityExperience, t.Reader.BuyinExperience, t.Reader.AssetExperience);
                    var init = new USGAAPInitData(t.Reader.USGAAPTable) { ExpectedReturns = t.Reader.USGAAPExpectedReturns };
                    var calculator = new USGAAPCalculator(getSchemeContext(t), init, evolutionData, basesDataManager, portfolioManager, context.Results);

                    var expected = t.Reader.USGAAPDisclosure;
                    var actual = new BeforeAfter<USGAAPDisclosure>(
                        calculator.GetStartPositionSummary(t.Reader.AttributionStart, t.Reader.AttributionEnd),
                        calculator.Get(context, true));

                    Action<USGAAPDisclosure, USGAAPDisclosure, string> test = (e, r, name) =>
                    {
                        Assert.That(r.NetPeriodPensionCostForecast, Is.EqualTo(e.NetPeriodPensionCostForecast), string.Format("{0}, {1}: NetPeriodPensionCostForecast", t.Reader.Name, name));
                    };

                    test(expected.After, actual.After, "[After]");
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestIAS19Disclosure(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (t.Reader.IAS19DisclosureEnabled)
                {
                    var schemeCtx = getSchemeContext(t);
                    var analysisResults = new BasisAnalysisService(schemeCtx).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything);

                    var historicContext = getHistoricBasisContext(t, basisType, t.Reader.AttributionStart);
                    var context = getBasisContext(t, basisType, analysisResults);

                    var expected = t.Reader.IAS19Disclosure;
                    var actual = new BeforeAfter<IAS19Disclosure>(
                        new IAS19Calculator(schemeCtx, historicContext, t.Reader.AttributionStart).Get(IAS19Calculator.CalculationMode.SummaryOnly),
                        new IAS19Calculator(schemeCtx, context, t.Reader.AttributionEnd, t.Reader.AttributionStart).Get(IAS19Calculator.CalculationMode.Full, false));

                    Action<IAS19Disclosure, IAS19Disclosure, string> test = (e, r, name) =>
                    {
                        Assert.AreEqual(e.Date, r.Date, testcaseToString(t));
                        Assert.That(r.Assumptions, Is.EqualTo(e.Assumptions), testcaseToString(t, "Assumptions " + name));
                        Assert.That(r.BalanceSheetAmounts, Is.EqualTo(e.BalanceSheetAmounts), testcaseToString(t, "BalanceSheetAmounts " + name));
                        Assert.That(r.DefinedBenefitObligation, Is.EqualTo(e.DefinedBenefitObligation), testcaseToString(t, "DefinedBenefitObligation " + name));
                        Assert.That(r.FairValueSchemeAssetChanges, Is.EqualTo(e.FairValueSchemeAssetChanges), testcaseToString(t, "FairValueSchemeAssetChanges " + name));
                        Assert.That(r.SchemeSurplusChanges, Is.EqualTo(e.SchemeSurplusChanges), testcaseToString(t, "SchemeSurplusChanges " + name));
                        Assert.That(r.PAndLForecast, Is.EqualTo(e.PAndLForecast), testcaseToString(t, "PAndLForecast " + name));
                    };

                    test(expected.Before, actual.Before, "Before");
                    test(expected.After, actual.After, "After");
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestFRS17Disclosure(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (t.Reader.FRS17DisclosureEnabled)
                {
                    var schemeCtx = getSchemeContext(t);
                    var analysisResults = new BasisAnalysisService(schemeCtx).Calculate(getBasisContext(t, basisType), BasisAnalysisService.CalculationMode.CalculateEverything);
                    var context = getBasisContext(t, basisType, analysisResults);
                    var calculator = new FRS17Calculator(schemeCtx, context, getBasesDataManager(t, basisType), getPortfolioManager(t, basisType), t.Reader.AttributionStart, t.Reader.AttributionEnd);

                    var expected = t.Reader.FRS17Disclosure;
                    var actual = new BeforeAfter<FRS17Disclosure>(
                        calculator.GetStartPositionSummary(),
                        calculator.Get(false));

                    Action<FRS17Disclosure, FRS17Disclosure, string> test = (e, r, name) =>
                    {
                        Assert.AreEqual(e.Date, r.Date, testcaseToString(t));
                        Assert.That(r.Assumptions, Is.EqualTo(e.Assumptions), testcaseToString(t, "Assumptions " + name));
                        Assert.That(r.BalanceSheetAmounts, Is.EqualTo(e.BalanceSheetAmounts), testcaseToString(t, "BalanceSheetAmounts " + name));
                        Assert.That(r.PresentValueSchemeLiabilities, Is.EqualTo(e.PresentValueSchemeLiabilities), testcaseToString(t, "PresentValueSchemeLiabilities " + name));
                        Assert.That(r.FairValueSchemeAssetChanges, Is.EqualTo(e.FairValueSchemeAssetChanges), testcaseToString(t, "FairValueSchemeAssetChanges " + name));
                        Assert.That(r.SchemeSurplusChanges, Is.EqualTo(e.SchemeSurplusChanges), testcaseToString(t, "SchemeSurplusChanges " + name));
                        Assert.That(r.PAndLForecast, Is.EqualTo(e.PAndLForecast), testcaseToString(t, "PAndLForecast " + name));
                    };

                    test(expected.Before, actual.Before, "Before");
                    test(expected.After, actual.After, "After");
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        public void TestUSGAAPDisclosure(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                if (t.Reader.USGAAPDisclosureEnabled)
                {
                    var context = getBasisContext(t, basisType);
                    context.Results = new BasisAnalysisResults
                    {
                        UserPenBuyIn = t.Reader.UserPenBuyIn,
                        UserDefBuyIn = t.Reader.UserDefBuyIn,
                        Cashflow = t.Reader.JPCashflows,
                        JPInvGrowth = t.Reader.JPInvGrowth,
                        JP2InvGrowth = t.Reader.JP2InvGrowth,
                        UserActLiabAfter = t.Reader.UserActLiabAfter,
                        UserDefLiabAfter = t.Reader.UserDefLiabAfter,
                        UserPenLiabAfter = t.Reader.UserPenLiabAfter,
                        InvestmentStrategy = t.Reader.InvestmentStrategyData,
                        ProfitLoss = t.Reader.ProfitLoss
                    };
                    var basesDataManager = getBasesDataManager(t, basisType);
                    var portfolioManager = getPortfolioManager(t, basisType);
                    var evolutionData = new BasisEvolutionData(t.Reader.LiabilityEvolution, t.Reader.AssetEvolution, t.Reader.LiabilityExperience, t.Reader.BuyinExperience, t.Reader.AssetExperience);
                    var init = new USGAAPInitData(t.Reader.USGAAPTable) { ExpectedReturns = t.Reader.USGAAPExpectedReturns };
                    var calculator = new USGAAPCalculator(getSchemeContext(t), init, evolutionData, basesDataManager, portfolioManager, context.Results);

                    var expected = t.Reader.USGAAPDisclosure;
                    var actual = new BeforeAfter<USGAAPDisclosure>(
                        calculator.GetStartPositionSummary(t.Reader.AttributionStart, t.Reader.AttributionEnd),
                        calculator.Get(context, true));

                    Action<USGAAPDisclosure, USGAAPDisclosure, string> test = (e, r, name) =>
                    {
                        Assert.AreEqual(e.Date, r.Date, testcaseToString(t));
                        Assert.That(r.Assumptions1, Is.EqualTo(e.Assumptions1), testcaseToString(t, "Assumptions1 " + name));
                        Assert.That(r.Assumptions2, Is.EqualTo(e.Assumptions2), testcaseToString(t, "Assumptions2 " + name));
                        Assert.That(r.BalanceSheetAmounts, Is.EqualTo(e.BalanceSheetAmounts), testcaseToString(t, "BalanceSheetAmounts " + name));
                        Assert.That(r.ChangesInPresentValue, Is.EqualTo(e.ChangesInPresentValue), testcaseToString(t, "ChangesInPresentValue " + name));
                        Assert.That(r.ChangesInFairValue, Is.EqualTo(e.ChangesInFairValue), testcaseToString(t, "ChangesInFairValue " + name));
                        Assert.That(r.AccumulatedBenefitObligation, Is.EqualTo(e.AccumulatedBenefitObligation), testcaseToString(t, "AccumulatedBenefitObligation " + name));
                        Assert.That(r.EstimatedPayments, Is.EqualTo(e.EstimatedPayments), testcaseToString(t, "EstimatedPayments " + name));
                        Assert.That(r.NetPeriodicPensionCost, Is.EqualTo(e.NetPeriodicPensionCost), testcaseToString(t, "NetPeriodicPensionCost " + name));
                        Assert.That(r.ComprehensiveIncome, Is.EqualTo(e.ComprehensiveIncome), testcaseToString(t, "ComprehensiveIncome " + name));
                        Assert.That(r.ComprehensiveAccumulatedIncome, Is.EqualTo(e.ComprehensiveAccumulatedIncome), testcaseToString(t, "ComprehensiveAccumulatedIncome " + name));
                        Assert.That(r.NetPeriodPensionCostForecast, Is.EqualTo(e.NetPeriodPensionCostForecast), testcaseToString(t, "NetPeriodPensionCostForecast " + name));
                    };

                    test(expected.Before, actual.Before, "[Before]");
                    test(expected.After, actual.After, "[After]");
                }
            }
        }

        #endregion

        #region newJP

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestNewJP(BasisType basisType)
        {
            AssertRelevance(basisType);

            var stopwatch = new Stopwatch();

            foreach (var t in getTestCandidates(basisType))
            {
                if (!t.Reader.IsFunded)
                    continue;

                var basisLookup = new Dictionary<int, IBasisContext>();

                var refresh = t.Reader.RefreshDate;
                var start = t.Reader.AttributionStart;
                var end = t.Reader.AttributionEnd;

                IBasisContext currentBasis = null;

                var newJpBases = new Dictionary<int, int>
                {
                    { 1, t.Reader.ClientJourneyPlanOptions.FundingBasisKey }
                };
                if (t.Reader.ClientJourneyPlanOptions.SelfSufficiencyBasisKey > 0)
                    newJpBases.Add(2, t.Reader.ClientJourneyPlanOptions.SelfSufficiencyBasisKey);
                if (t.Reader.ClientJourneyPlanOptions.BuyoutBasisKey > 0)
                    newJpBases.Add(3, t.Reader.ClientJourneyPlanOptions.BuyoutBasisKey);

                foreach (var bkey in newJpBases.Keys)
                {
                    stopwatch.Reset();
                    stopwatch.Start();

                    var masterbasis = new MasterBasis(t.Reader.Bases.Where(b => b.MasterBasisId == newJpBases[bkey]));

                    var basesDataManager = new BasesDataManager(masterbasis.Bases.Values);
                    var basis = basesDataManager.GetBasis(end);
                    var assets = t.Reader.SchemeAssets.Where(a => a.EffectiveDate == t.Reader.SchemeAssets.Select(x => x.EffectiveDate).Distinct().Where(x => x <= end).OrderBy(x => x).Last());
                    var portfolioManager = new PortfolioManager(t.CachedScheme, basesDataManager);
                    var pf = portfolioManager.GetPortfolio(end);

                    var assumptionEvolution = new LiabilityAssumptionEvolutionService(basesDataManager).Calculate(masterbasis.AnchorDate, refresh);
                    var pincsEvolution = new PensionIncreaseEvolutionService(basesDataManager).Calculate(masterbasis.AnchorDate, refresh);
                    var liabilityEvolution = new LiabilityEvolutionService().Calculate(masterbasis.AnchorDate, refresh, basesDataManager, assumptionEvolution, pincsEvolution).ToDictionary(x => x.Date);
                    var exp = new LiabilityExperienceService().CalculateLiabilityStepChange(basesDataManager, liabilityEvolution);
                    var liabilityExperience = exp[LiabilityExperienceService.LiabilityExperienceType.Liab];
                    var buyinExperience = exp[LiabilityExperienceService.LiabilityExperienceType.Buyin];

                    var evolution = new BasisEvolutionData(liabilityEvolution, null, liabilityExperience, buyinExperience, null);

                    var context = new BasisContext(
                        t.Reader.AttributionStart,
                        t.Reader.AttributionEnd,
                        basis.EffectiveDate,
                        basis,
                        pf,
                        null,
                        evolution,
                        new BasisAssumptions(
                            t.Reader.UserAssumptionsTable.ContainsKey(basis.DisplayName) ? t.Reader.UserAssumptionsTable[basis.DisplayName].LiabilityAssumptions : evolution.LiabilityEvolution[t.Reader.AttributionEnd].ToAssumptionsDictionary(),
                            t.Reader.UserAssumptionsTable.ContainsKey(basis.DisplayName) ? t.Reader.UserAssumptionsTable[basis.DisplayName].LifeExpectancy : basis.LifeExpectancy,
                            t.Reader.UserAssumptionsTable.ContainsKey(basis.DisplayName) ? t.Reader.UserAssumptionsTable[basis.DisplayName].PensionIncreases : evolution.LiabilityEvolution[t.Reader.AttributionEnd].ToPensionIncreasesDictionary(),
                            pf.AssetData.Assets.ToDictionary(x => x.Class.Type, x => x.DefaultReturn),
                            t.Reader.RecoveryPlanAssumptions));

                    basisLookup.Add(bkey, context);
                }

                var schemeCtx = getSchemeContext(t);
                schemeCtx.Calculated.BuyinCost = new BuyinCost(t.Reader.InsPenCost, t.Reader.InsNonPenCost);
                var ctx = getBasisContext(t, t.Reader.BasisType);
                ctx.Results = new BasisAnalysisService(schemeCtx).Calculate(ctx, BasisAnalysisService.CalculationMode.CalculateEverything);

                var client = t.Reader.ClientJourneyPlanOptions;
                var options = new NewJPParameters
                {
                    AnalysisDate = t.Reader.AttributionEnd,
                    CashflowBasis = currentBasis,
                    FundingBasis = basisLookup[1],
                    SelfSufficiencyBasis = basisLookup.ContainsKey(2) ? basisLookup[2] : null,
                    BuyoutBasis = basisLookup.ContainsKey(3) ? basisLookup[3] : null,
                    CurveDataProjection = new CurveDataProjection(1, "", t.Reader.GiltSpotRates.Select(x => new SpotRate(x.Key, x.Value, t.Reader.AttributionEnd.AddYears(x.Key - 1))).ToList()),
                    RecoveryPlan = client.RecoveryPlan,
                    InvGrowthRateBefore = client.CurrentReturn,
                    SelfSufficiencyDiscountRate = client.SelfSufficiencyDiscountRate,
                    SalaryInflationType = client.SalaryInflationType,
                    TriggerSteps = client.TriggerSteps,
                    TriggerTimeStartYear = client.TriggerTimeStartYear,
                    TriggerTransitionPeriod = client.TriggerTransitionPeriod,
                    UseCurves = client.UseCurves,
                    IncludeBuyIns = client.IncludeBuyIns,
                    InitialGrowthRate = client.InitialReturn,
                    TargetGrowthRate = client.FinalReturn
                };

                var jp = new NewJP(schemeCtx, ctx).Calculate(options);

                var expected = t.Reader.NewJP;
                var actual = jp;

                Assert.That(actual.Liab1.Keys, Is.EquivalentTo(expected.Liab1.Keys), testcaseToString(t));
                foreach (var year in expected.Liab1.Keys)
                {
                    Assert.That(actual.Liab1[year], Is.EqualTo(expected.Liab1[year]).Within(Utils.TestPrecision), testcaseToString(t, string.Format("Liab1, year: {0}", year.ToString())));                 
                    Assert.That(actual.Liab2[year], Is.EqualTo(expected.Liab2[year]).Within(Utils.TestPrecision), testcaseToString(t, string.Format("Liab2, year: {0}", year.ToString())));

                    if (actual.Liab3 != null) //Liab3 => Buyout basis on Tracker NewJP
                        Assert.That(actual.Liab3[year], Is.EqualTo(expected.Liab3[year]).Within(Utils.TestPrecision), testcaseToString(t, string.Format("Liab3, year: {0}", year.ToString())));
                    
                    Assert.That(actual.Assets1[year], Is.EqualTo(expected.Assets1[year]).Within(Utils.TestPrecision), testcaseToString(t, string.Format("Assets1, year: {0}", year.ToString())));
                    Assert.That(actual.Assets2[year], Is.EqualTo(expected.Assets2[year]).Within(Utils.TestPrecision), testcaseToString(t, string.Format("Assets2, year: {0}", year.ToString())));
                }

                foreach (var key in expected.DefaultPremiums.Keys)
                {
                    if (actual.DefaultPremiums.ContainsKey(key))
                    {
                        Assert.That(jp.DefaultPremiums[key].Before, Is.EqualTo(expected.DefaultPremiums[key].Before).Within(Utils.Precision), testcaseToString(t, "Default premium [Before]: " + key.ToString()));
                        Assert.That(jp.DefaultPremiums[key].After, Is.EqualTo(expected.DefaultPremiums[key].After).Within(Utils.Precision), testcaseToString(t, "Default premium [After]: " + key.ToString()));
                    }
                }
            }
        }

        #endregion

        #region Composite Indices

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Buyout)]
        public void TestCompositeIndexCalculation(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in getTestCandidates(basisType))
            {
                // Get component indices for 'My total return' and 'My yield' (See A138 on Main tab on Tracker)
                CustomIndexService customIndexService = new CustomIndexService();

                var totalReturnName = "My total return";
                var totalReturnParams = t.Reader.CompositeIndexParameters.Where(i => i.CiName == totalReturnName).Single();

                // Call CreateCustomIndex() with parameters read from Main tab. (We'll have to load the component stats)
                var totalReturnCompositeIndex = customIndexService.CreateCustomIndex(true, totalReturnParams.SourceIndexProportion, totalReturnParams.Rounding);

                // Compare CreateCustomIndex() results with Trackers from the Testing tab
                var trackerReadTotalReturnCompositeIndex = t.Reader.indices.Where(i => i.Key == totalReturnName).Single();
                foreach (var year in trackerReadTotalReturnCompositeIndex.Value.RawValues.Keys)
                {
                    Assert.That(trackerReadTotalReturnCompositeIndex.Value.RawValues[year], Is.EqualTo(totalReturnCompositeIndex[year]).Within(Utils.TestPrecision), string.Format("{0} composite index does't match year: {1}.", totalReturnName, year.Year));
                }

                var yieldName = "My yield";
                var yieldParams = t.Reader.CompositeIndexParameters.Where(i => i.CiName == yieldName).Single();

                // Call CreateCustomIndex() with parameters read from Main tab. (We'll have to load the component stats)
                var yieldCompositeIndex = customIndexService.CreateCustomIndex(false, yieldParams.SourceIndexProportion, yieldParams.Rounding);

                // Compare CreateCustomIndex() results with Trackers from the Testing tab
                var trackerReadYieldCompositeIndex = t.Reader.indices.Where(i => i.Key == yieldName).Single();
                foreach (var year in trackerReadYieldCompositeIndex.Value.RawValues.Keys)
                {
                    Assert.That(trackerReadYieldCompositeIndex.Value.RawValues[year], Is.EqualTo(yieldCompositeIndex[year]).Within(Utils.TestPrecision), string.Format("{0} composite index does't match year: {1}.", yieldName, year.Year));
                }

            }
        }

        #endregion

        private void AssertRelevance(BasisType basisType)
        {
            if (useCache && !cache.ContainsKey(basisType))
            {
                Assert.Ignore();
                return;
            }
            else if (useCache)
            {
                return;
            }

            if (IsBasisTypeAvailable(basisType) == false)
            {
                Assert.Ignore();
            }
        }

        private IEnumerable<SchemeTestStructure> getTestCandidates(BasisType basisType)
        {
            if (useCache && !cache.ContainsKey(basisType))
                throw new Exception("Basis type not found. If debugging, make sure you have set the BasisType to the same one you have selected in the Tracker excel workbook");

            if (useCache)
                foreach (var test in cache[basisType])
                    yield return test;
            else
                foreach (var testStructure in GetTestStructures(basisType))
                    yield return testStructure;
        }

        /// <summary>
        /// Get all liab data for a basis type - NOTE, if you want a specific basis you should use getBasesDataManager with a basisName.
        /// </summary>
        /// <remarks>
        /// Don't use this if you care that there might be different bases for the same type.
        /// </remarks>
        private IBasesDataManager getBasesDataManager(SchemeTestStructure t, BasisType bt)
        {
            return new BasesDataManager(t.Reader.Bases.Where(b => b.Type == bt));
        }

        /// <summary>
        /// Get all liab data for a basis.
        /// </summary>
        private IBasesDataManager getBasesDataManager(SchemeTestStructure t, string basisName)
        {
            return new BasesDataManager(t.Reader.Bases.Where(b => b.DisplayName == basisName));
        }

        private IPortfolioManager getPortfolioManager(SchemeTestStructure t, BasisType bt)
        {
            return new PortfolioManager(t.CachedScheme, getBasesDataManager(t, bt));
        }

        private BasisContext getHistoricBasisContext(SchemeTestStructure t, BasisType bt, DateTime at, DateTime? start = null)
        {
            var bdm = getBasesDataManager(t, bt);
            var pf = new PortfolioManager(t.CachedScheme, bdm).GetPortfolio(at);
            var b = bdm.GetBasis(at);
            var ctx = new BasisContext(
                start.GetValueOrDefault(at), at,
                b.EffectiveDate,
                b,
                pf,
                new BasisAnalysisResults(),
                new BasisEvolutionData(t.Reader.LiabilityEvolution, t.Reader.AssetEvolution, t.Reader.LiabilityExperience, t.Reader.BuyinExperience, t.Reader.AssetExperience),
                t.Reader.BasisAssumptions
                );

            return ctx;
        }

        private SchemeContext getSchemeContext(SchemeTestStructure t)
        {
            return new SchemeContext(
                t.CachedScheme,
                t.Reader.SchemeAssumptions,
                new SchemeResults { RefreshDate = t.Reader.RefreshDate, BuyinCost = new BuyinCost(t.Reader.InsPenCost, t.Reader.InsNonPenCost), LDIEvolutionData = t.Reader.LDIEvolutionData },
                t.Reader
                );
        }

        private BasisContext getBasisContext(SchemeTestStructure t, BasisType bt, BasisAnalysisResults r = null, IPortfolioManager portfolioManager = null, DateTime? atDate = null)
        {
            if (!atDate.HasValue)
                atDate = t.Reader.AttributionEnd;

            var bdm = getBasesDataManager(t, bt);
            var pf = (portfolioManager ?? new PortfolioManager(t.CachedScheme, bdm)).GetPortfolio(atDate.Value);
            var b = bdm.GetBasis(atDate.Value);
            var ctx = new BasisContext(
                t.Reader.AttributionStart, atDate.Value,
                b.EffectiveDate,
                b,
                pf,
                r,
                new BasisEvolutionData(t.Reader.LiabilityEvolution, t.Reader.AssetEvolution, t.Reader.LiabilityExperience, t.Reader.BuyinExperience, t.Reader.AssetExperience),
                t.Reader.BasisAssumptions
                );

            return ctx;
        }

        private string testcaseToString(SchemeTestStructure t, string extraInfo = null)
        {
            return string.Format("{0} [{1}], test case: {2}{3}", t.Reader.Name, t.Reader.BasisType.ToString(), t.TestCaseID.ToString(),
                !string.IsNullOrEmpty(extraInfo) ? " - " + extraInfo : string.Empty);
        }

        private DateTime _startTime;
        private DateTime _endTime;
        private string _testName;
        [SetUp]
        public void Init()
        {
            _testName = TestContext.CurrentContext.Test.Name;
            _startTime = DateTime.Now;
        }

        [TearDown]
        public void Cleanup()
        {
            _endTime = DateTime.Now;
            bool logToConsole = false;
            bool.TryParse(ConfigurationManager.AppSettings["LogTestTimings"], out logToConsole);

            if (logToConsole)
                Console.WriteLine("{0} test started at {1}, time taken: {2} seconds",
                    _testName, _startTime.ToLongTimeString(), (_endTime - _startTime).TotalSeconds);
        }
    }
}
