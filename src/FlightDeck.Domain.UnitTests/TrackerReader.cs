﻿namespace FlightDeck.Domain.UnitTests
{
    using FlexCel.Core;
    using FlexCel.XlsAdapter;
    using FlightDeck.Domain.Accounting;
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.Domain.LDI;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.Domain.VaR;
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using Moq;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    public class TrackerReader : IBasesAssumptionsProvider
    {
        public Dictionary<string, FinancialIndex> indices;
        
        private XlsFile xls;
        private Dictionary<string, int> masterBasisMockIds = new Dictionary<string, int>();

        public SchemeAssumptions SchemeAssumptions { get; private set; }
        public BasisAssumptions BasisAssumptions { get; private set; }

        public IList<Basis> Bases { get; private set; }
        public IList<SchemeAsset> SchemeAssets { get; private set; }
        private IDictionary<AssetClassType, double> Volatilities { get; set; }
        private IDictionary<AssetClassType, IDictionary<AssetClassType, double>> VarCorrelations { get; set; }
        public IList<Contribution> ContributionRates { get; private set; }
        public IList<RecoveryPayment> RecoveryPlan { get; private set; }
        private double ReturnOnAssets;
        private AccountingStandardType AccountingDefaultStandard;
        private bool AccountingIAS19Visible;
        private bool AccountingFRS17Visible;
        private bool AccountingUSGAAPVisible;
        private SalaryType? ABOSalaryType;
        private double ContractingOutContributionRate;        

        public string Name { get; private set; }
        public string ChosenBasis { get; private set; }
        public BasisType BasisType { get; private set; }
        public bool IsFunded { get; private set; }
        public IDictionary<DateTime, LiabilityEvolutionItem> LiabilityEvolution { get; private set; }
        public IDictionary<DateTime, AssetEvolutionItem> AssetEvolution { get; private set; }
        public IEnumerable<LDI.LDIEvolutionItem> LDIEvolutionData { get; private set; }
        public string LDIBasis { get; private set; }
        public bool LDIOverrideAssumptions { get; private set; }
        public FinancialIndex LDIDiscRateIndex { get; private set; }
        public double LDIDiscRatePremium { get; private set; }
        public FinancialIndex LDIInflationIndex { get; private set; }
        public double LDIInflationGap { get; private set; }
        public Dictionary<DateTime, double> LiabilityExperience { get; private set; }
        public Dictionary<DateTime, double> BuyinExperience { get; private set; }
        public Dictionary<DateTime, double> AssetExperience { get; private set; }
        public Dictionary<DateTime, ExpectedDeficitData> ExpectedDeficit { get; private set; }
        public bool IsGrowthFixed { get; private set; }
        public double Ias19ExpenseLoading { get; private set; }
        public FundingData FundingLevel { get; private set; }
        public BeforeAfter<Dictionary<int, Dictionary<MemberStatus, double>>> CashFlowLens { get; private set; }
        public BeforeAfter<IList<LensData>> JourneyPlanLens { get; private set; }
        public BeforeAfter<Dictionary<int, BalanceData>> JourneyPlan { get; private set; }
        public BeforeAfter<IList<double>> RecoveryPlanLens { get; private set; }
        public RecoveryPlanType RecoveryPlanType { get; private set; }
        public DateTime AttributionEnd { get; private set; }
        public DateTime AttributionStart { get; private set; }
        public DateTime RefreshDate { get; private set; }
        public RecoveryPlanAssumptions RecoveryPlanAssumptions { get; private set; }
        public Dictionary<AssetClassType, double> ClientAssetAssumptions { get; private set; }
        public Dictionary<AssetClassType, double> Allocations { get; private set; }
        public BalanceData BalanceSheetEstimateLens { get; private set; }
        public FutureServiceCostData FutureServiceCostData { get; private set; }
        public IDictionary<ValuationBasisType, ProfitLossForecast> ProfitLossForecast { get; private set; }
        public FroData FroData { get; private set; }
        public EFroData EFroData { get; private set; }
        public InvestmentStrategyData InvestmentStrategyData { get; private set; }
        public EtvData EtvData { get; private set; }
        public PIEInitData PIEInit { get; private set; }
        public PieData PieData { get; private set; }
        public SurplusAnalysisData SurplusAnalysisData { get; private set; }
        public Dictionary<AssetClassType, double> DefaultVolatilities { get; private set; }
        public VolatilityAssumptionResults VolatilityAssumptions { get; private set; }
        public double MarketChange { get; private set; }
        public BuyinData BuyinInfo { get; private set; }
        public double TotalInsurancePremiumPaid { get; private set; }
        public double InsurancePointIncreaseAmount { get; private set; }
        public Dictionary<string, IDictionary<StrainType, double>> BuyoutStrains { get; private set; }
        public IList<VarFunnelData> VarFunnelData { get; private set; }
        public AbfData AbfData { get; private set; }
        public FutureBenefitsParameters FutureBenefitsParameters { get; private set; }
        public FutureBenefitsData FutureBenefitsData { get; private set; }
        public BeforeAfter<IAS19Disclosure> IAS19Disclosure { get; private set; }
        public BeforeAfter<FRS17Disclosure> FRS17Disclosure { get; private set; }
        public BeforeAfter<USGAAPDisclosure> USGAAPDisclosure { get; private set; }
        public bool IAS19DisclosureEnabled { get; private set; }
        public bool FRS17DisclosureEnabled { get; private set; }
        public bool USGAAPDisclosureEnabled { get; private set; }
        public double RecoveryPlanPaymentRequired { get; private set; }
        public double OutPerformanceMaxReturn { get; private set; }
        public double UserActLiab { get; private set; }
        public double UserDefLiab { get; private set; }
        public double UserPenLiab { get; private set; }
        public double UserServiceCost { get; private set; }
        public double UserActDuration { get; private set; }
        public double UserDefDuration { get; private set; }
        public double UserPenDuration { get; private set; }
        public double UserDefBuyIn { get; private set; }
        public double UserPenBuyIn { get; private set; }
        public double UserActLiabAfter { get; private set; }
        public double UserDefLiabAfter { get; private set; }
        public double UserPenLiabAfter { get; private set; }
        public Dictionary<int, IDictionary<MemberStatus, double>> JPCashflows { get; private set; }
        public IEnumerable<CashflowData> JP2Cashflows { get; private set; }
        public Dictionary<int, double> GiltSpotRates { get; private set; }
        public NewJPData NewJP { get; private set; }
        public JourneyPlanOptions ClientJourneyPlanOptions { get; private set; }
        public double InsPenCost { get; private set; }
        public double InsNonPenCost { get; private set; }
        public double AssetsAtSED { get; private set; }
        public double BuyInAtAnalysisDate { get; private set; }
        public double JPInvGrowth { get; private set; }
        public double JP2InvGrowth { get; private set; }
        public double JPAfterLiabDuration { get; private set; }
        public GiltBasisLiabilityData GiltBasisLiability { get; private set; }
        public Dictionary<string, BasisAssumptions> UserAssumptionsTable { get; private set; }
        public List<CompositeIndexParameters> CompositeIndexParameters { get; private set; }
        public WaterfallData Waterfall { get; private set; }
        public Dictionary<DateTime, LDISwapsEvolutionItem> LDISwapsEvolution { get; private set; }
        public Dictionary<DateTime, AOCIData> USGAAPTable { get; private set; }
        public Dictionary<USGAAPExpectedReturnsType, double> USGAAPExpectedReturns { get; private set; }
        public ProfitLossData ProfitLoss { get; private set; }
        public List<KeyValuePair<string, double?>> GrowthOverPeriodInfo { get; private set; }
        public HedgeBreakdownData HedgeBreakdown { get; private set; }
        public IDictionary<DateTime, HedgeBreakdownData> AssetHedgeBreakdownData { get; private set; }
        public IDictionary<string, IBasisAssumptions> BasisAssumptionsCollection { get; private set; }
        public Dictionary<DateTime, IDictionary<AssumptionType, double>> LiabilityAssumptionEvolution
        {
            get
            {
                return LiabilityEvolution.ToDictionary(
                    x => x.Key,
                    x => (IDictionary<AssumptionType, double>)new Dictionary<AssumptionType, double>
                        {
                            { AssumptionType.PreRetirementDiscountRate, x.Value.DiscPre },
                            { AssumptionType.PostRetirementDiscountRate, x.Value.DiscPost },
                            { AssumptionType.PensionDiscountRate, x.Value.DiscPen },
                            { AssumptionType.SalaryIncrease, x.Value.SalInc },
                            { AssumptionType.InflationRetailPriceIndex, x.Value.RPI },
                            { AssumptionType.InflationConsumerPriceIndex, x.Value.CPI }
                        });
            }
        }

        public Dictionary<DateTime, IDictionary<int, double>> PensionIncreaseEvolution
        {
            get
            {
                return LiabilityEvolution.ToDictionary(
                    x => x.Key,
                    x => (IDictionary<int, double>)new Dictionary<int, double>
                        {
                            { 1, x.Value.Pinc1 },
                            { 2, x.Value.Pinc2 },
                            { 3, x.Value.Pinc3 },
                            { 4, x.Value.Pinc4 },
                            { 5, x.Value.Pinc5 },
                            { 6, x.Value.Pinc6 },
                        });
            }
        }

        public UserLiabilityData UserLiabs
        {
            get
            {
                return new UserLiabilityData(UserActLiab, UserDefLiab, UserPenLiab, UserServiceCost, UserActDuration, UserDefDuration, UserPenDuration, UserDefBuyIn, UserPenBuyIn);
            }
        }

        public TrackerReader(string trackerPath)
        {
            xls = new XlsFile(trackerPath, false);

            GetName();

            ChosenBasis = GetChosenBasis();
            BasisType = getBasisTypeFromLookupValue();

            GetUserLiabs();
            GetSchemeParameters();
            GetJourneyPlan();
            GetJourneyPlanResults();
            GetRecoveryPlanLensResults();

            GetContributions();

            GetRecoveryPlan();
            GetRecoveryPlanPayment();
            GetCashFlowLensData();

            GetIndexData();

            GetSchemeLDIParamters();

            GetCompositeIndexData();
            GetCompositeIndexComponents();

            GetDefaultVolatilities();
            GetSchemeAssets();

            GetClientAssumptions();

            Bases = GetBases();

            GetGrowthOverPeriodInfo();
            GetLiabilityEvolutionResults();            
            GetAssetEvolutionResults();
            GetExperienceResults();
            GetExpectedDeficit();
            GetFundingLevelResults();
            GetBalanceEstimateResults();
            GetFutureServiceCostResults();
            GetProfitLossForecastResults();
            GetPanelResults();
            GetSurplusAnalysisResults();
            GetVolatilityAssumptions();
            GetVarWaterfallResults();
            GetVarFunnelResults();
            GetInsurancePanelResults();
            GetInvestmentStrategyResults();
            //TODO test strains on insurance panel
            GetAbf();
            GetFutureBenefits();
            GetProfitLossData();
            GetIAS19Disclosure();
            GetFRS17Disclosure();
            GetUSGAAP();
            GetAccountingParams();

            GetCashflows();
            GetAdjustedCashflows();
            GetNewJPGiltRates();
            GetNewJPResults();

            GetClientJourneyPlanOptions();
            GetUserAssumptionsTable();
            GetBasisAssumptionsCollection();

            GetRangeValues();

            GetAssetHedgeBreakdownData();
        }

        private void GetName()
        {
            xls.ActiveSheetByName = "Main";
            var coords = GetRangeCoords("NewSchemeName");
            Name = GetString(coords.Row, coords.Col);
        }

        private void GetSchemeParameters()
        {
            xls.ActiveSheetByName = "Main";
            double d;
            if (TryGetDouble("COutContRate", out d, "Main"))
                ContractingOutContributionRate = d;
            else
                ContractingOutContributionRate = 0.03;
            IsFunded = GetBool("IsFunded");
        }

        private void GetSchemeLDIParamters()
        {
            xls.ActiveSheetByName = "Main";
            LDIBasis = GetString("LDIBasis");

            LDIOverrideAssumptions = GetString("LDIOverrideAssumptions").Equals("Yes", StringComparison.OrdinalIgnoreCase);

            if (LDIOverrideAssumptions)
            {
                FinancialIndex ldiIndex = null;
                indices.TryGetValue(GetString("LDIDiscRateIndex"), out ldiIndex);
                LDIDiscRateIndex = ldiIndex;

                LDIDiscRatePremium = GetDouble("LDIDiscRatePremium");

                FinancialIndex inflationIndex = null;
                indices.TryGetValue(GetString("LDIInflationIndex"), out inflationIndex);
                LDIInflationIndex = inflationIndex;

                LDIInflationGap = GetDouble("LDIInflationGap");
            }
        }

        private void GetUserLiabs()
        {
            xls.ActiveSheetByName = "Panels";
            UserActLiab = GetDouble("UserActLiab");
            UserDefLiab = GetDouble("UserDefLiab");
            UserPenLiab = GetDouble("UserPenLiab");
            UserServiceCost = GetDouble("UserServiceCost");
            UserActDuration = GetDouble("UserActDuration");
            UserDefDuration = GetDouble("UserDefDuration");
            UserPenDuration = GetDouble("UserPenDuration");
            UserDefBuyIn = GetDouble("UserDefBuyIn");
            UserPenBuyIn = GetDouble("UserPenBuyIn");
            xls.ActiveSheetByName = "FundingLevel";
            UserActLiabAfter = GetDouble("UserActLiabAfter");
            UserDefLiabAfter = GetDouble("UserDefLiabAfter");
            UserPenLiabAfter = GetDouble("UserPenLiabAfter");
        }

        private void GetProfitLossData()
        {
            xls.ActiveSheetByName = "Accounting";

            ProfitLoss = new ProfitLossData
                (
                GetDouble(28, "H"),
                GetDouble(29, "H"),
                GetDouble(30, "H"),
                GetDouble(32, "H"),
                GetDouble(33, "H"),
                GetDouble(34, "H"),
                GetDouble(41, "H"),
                GetDouble(42, "H"),
                GetDouble(43, "H"),
                GetDouble(44, "H"),
                GetDouble(45, "H"),
                GetDouble(46, "H")
                );
        }

        private void GetIAS19Disclosure()
        {
            IAS19DisclosureEnabled = GetBool("IAS19Visible", "Main");

            if (!IAS19DisclosureEnabled)
                return;

            const int multi = 1000;

            Func<string, IAS19Disclosure> getreport = col =>
                {
                    xls.ActiveSheetByName = "Accounting";
                    var interestCredit = GetDouble(32, "M");
                    var interestCharge = GetDouble(32, "N");

                    xls.ActiveSheetByName = "Disclosures";

                    int row = 11;
                    Func<int, int> inc = x => { row += x; return row; };
                    return new IAS19Disclosure(
                        GetDateSlow(row, col),
                        new DisclosureAssumptions
                            {
                                DiscountRate = GetDouble(inc(1), col, 0),
                                FutureSalaryGrowth = GetDouble(inc(1), col, 0),
                                RPIInflation = GetDouble(inc(1), col, 0),
                                CpiInflation = GetDouble(inc(1), col, 0),
                                MaleLifeExpectancy65 = GetDouble(inc(2), col, 0),
                                MaleLifeExpectancy65_45 = GetDouble(inc(1), col, 0)
                            },
                        new IAS19Disclosure.BalanceSheetAmountData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new IAS19Disclosure.DefinedBenefitObligationData(
                            GetDouble(inc(6), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new IAS19Disclosure.FairValueSchemeAssetChangesData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new IAS19Disclosure.SchemeSurplusChangesData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(2), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(2), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(2), col, 0) * multi,
                            GetDouble(inc(2), col, 0) * multi),
                        new IAS19Disclosure.PAndLForecastData(
                            GetDouble(inc(6), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            interestCredit,
                            interestCharge,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi)
                            );
                };

            var before = getreport("D");
            before.DefinedBenefitObligation = null;
            before.FairValueSchemeAssetChanges = null;
            before.SchemeSurplusChanges = null;
            before.PAndLForecast = null;

            var after = getreport("C");

            IAS19Disclosure = new BeforeAfter<IAS19Disclosure>(before, after);
        }

        private void GetFRS17Disclosure()
        {
            FRS17DisclosureEnabled = GetBool("FRS17Visible", "Main");

            if (!FRS17DisclosureEnabled)
                return;

            xls.ActiveSheetByName = "Disclosures";
            const int multi = 1000;

            Func<string, FRS17Disclosure> getreport = col =>
                {
                    int row = 11;
                    Func<int, int> inc = x => { row += x; return row; };
                    return new FRS17Disclosure(
                        GetDateSlow(row, col),
                        new DisclosureAssumptions
                            {
                                DiscountRate = GetDouble(inc(1), col, 0),
                                FutureSalaryGrowth = GetDouble(inc(1), col, 0),
                                RPIInflation = GetDouble(inc(1), col, 0),
                                CpiInflation = GetDouble(inc(1), col, 0),
                                ExpectedRateOfReturnOnAssets = GetDouble(inc(1), col, 0),
                                MaleLifeExpectancy65 = GetDouble(inc(2), col, 0),
                                MaleLifeExpectancy65_45 = GetDouble(inc(1), col, 0)
                            },
                        new FRS17Disclosure.BalanceSheetAmountData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new FRS17Disclosure.PresentValueSchemeLiabilityData(
                            GetDouble(inc(6), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new FRS17Disclosure.FairValueSchemeAssetChangesData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new FRS17Disclosure.SchemeSurplusChangesData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(2), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(2), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(2), col, 0) * multi,
                            GetDouble(inc(2), col, 0) * multi),
                        new FRS17Disclosure.PAndLForecastData(
                            GetDouble(inc(6), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi)
                        );
                };

            var before = getreport("J");
            before.PresentValueSchemeLiabilities = null;
            before.FairValueSchemeAssetChanges = null;
            before.SchemeSurplusChanges = null;
            before.PAndLForecast = null;

            var after = getreport("I");

            FRS17Disclosure = new BeforeAfter<FRS17Disclosure>(before, after);
        }

        private void GetUSGAAP()
        {
            GetUSGAAPTable();
            GetUSGAAPExpectedReturns();
            GetUSGAAPDisclosure();
        }

        private void GetUSGAAPTable()
        {
            USGAAPTable = new Dictionary<DateTime, AOCIData>();

            USGAAPDisclosureEnabled = GetBool("USGAAPVisible", "Main");

            if (!USGAAPDisclosureEnabled)
                return;

            xls.ActiveSheetByName = "Disclosures";

            var coords = GetRangeCoords("USGAAPTable");
            var row = coords.Row;

            while (xls.GetCellValue(row, coords.Col) != null)
            {
                USGAAPTable.Add(GetDate(row, coords.Col), new AOCIData(GetDouble(row, coords.Col + 1), GetDouble(row, coords.Col + 2), GetDouble(row, coords.Col + 3), (int)GetDouble(row, coords.Col + 4)));
                row++;
            }
        }

        private void GetUSGAAPExpectedReturns()
        {
            USGAAPExpectedReturns = new Dictionary<USGAAPExpectedReturnsType, double>();

            if (!USGAAPDisclosureEnabled)
                return;

            xls.ActiveSheetByName = "Disclosures";

            USGAAPExpectedReturns.Add(USGAAPExpectedReturnsType.AnalysisEnd, GetDouble(21, "S", 0));
            USGAAPExpectedReturns.Add(USGAAPExpectedReturnsType.AnalysisStart, GetDouble(21, "T", 0));
            USGAAPExpectedReturns.Add(USGAAPExpectedReturnsType.UsersInputs, GetDouble(21, "U", 0));
        }

        private void GetUSGAAPDisclosure()
        {
            if (!USGAAPDisclosureEnabled)
                return;

            xls.ActiveSheetByName = "Disclosures";
            const int multi = 1000;

            Func<string, USGAAPDisclosure> getreport = col =>
                {
                    int row = 11;
                    Func<int, int> inc = x => { row += x; return row; };
                    return new USGAAPDisclosure(
                        GetDateSlow(row, col),
                        new DisclosureAssumptions
                            {
                                DiscountRate = GetDouble(inc(1), col, 0),
                                FutureSalaryGrowth = GetDouble(inc(1), col, 0),
                                RPIInflation = GetDouble(inc(1), col, 0),
                                CpiInflation = GetDouble(inc(1), col, 0),
                                ExpectedRateOfReturnOnAssets = GetDouble(inc(1), col, 0),
                                MaleLifeExpectancy65 = GetDouble(inc(2), col, 0),
                                MaleLifeExpectancy65_45 = GetDouble(inc(1), col, 0)
                            },
                        new DisclosureAssumptions
                            {
                                DiscountRate = GetDouble(inc(4), col, 0),
                                ExpectedRateOfReturnOnAssets = GetDouble(inc(1), col, 0),
                                FutureSalaryGrowth = GetDouble(inc(1), col, 0),
                                RPIInflation = GetDouble(inc(1), col, 0),
                                CpiInflation = GetDouble(inc(1), col, 0),
                                MaleLifeExpectancy65 = GetDouble(inc(2), col, 0),
                                MaleLifeExpectancy65_45 = GetDouble(inc(1), col, 0)
                            },
                        new USGAAPDisclosure.BalanceSheetAmountData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new USGAAPDisclosure.ChangesInPresentValueData(
                            GetDouble(inc(6), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new USGAAPDisclosure.ChangesInFairValueData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new USGAAPDisclosure.AccumulatedBenefitObligationData(
                            GetDouble(inc(4), col, 0) * multi),
                        new USGAAPDisclosure.EstimatedPaymentData(
                            GetDouble(inc(3), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new USGAAPDisclosure.NetPeriodicPensionCostData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new USGAAPDisclosure.ComprehensiveIncomeData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi),
                        new USGAAPDisclosure.ComprehensiveAccumulatedIncomeData(
                            GetDouble(inc(5), col, null) * multi,
                            GetDouble(inc(1), col, null) * multi,
                            GetDouble(inc(1), col, null) * multi,
                                GetDouble(inc(1), col, 0) * multi),
                        new USGAAPDisclosure.NetPeriodicPensionCostForecastData(
                            GetDouble(inc(5), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi,
                            GetDouble(inc(1), col, 0) * multi));
                };

            var before = getreport("P");
            before.Assumptions2 = null;
            before.AccumulatedBenefitObligation = null;
            before.ChangesInPresentValue = null;
            before.ChangesInFairValue = null;
            before.EstimatedPayments = null;
            before.NetPeriodicPensionCost = null;
            before.ComprehensiveIncome = null;
            before.ComprehensiveAccumulatedIncome = null;
            before.NetPeriodPensionCostForecast = null;

            var after = getreport("O");

            USGAAPDisclosure = new BeforeAfter<DomainShared.USGAAPDisclosure>(before, after);
        }

        private void GetFutureBenefits()
        {
            xls.ActiveSheetByName = "Panels";
            var column = "BM";
            var c = "BI";
            FutureBenefitsData = new FutureBenefitsData(
                GetDouble(13, column, 0),
                GetDouble(14, column, 0),
                GetDouble(15, column, 0),
                GetDouble(16, column, 0),
                GetDouble(18, column, 0),
                GetDouble(19, column, 0),
                GetDouble(20, column, 0),
                GetDouble(11, c, 0),
                GetDouble(12, c, 0),
                GetDouble(14, c, 0),
                GetDouble(22, column, 0),
                GetDouble(23, column, 0)
                );
        }

        private BenefitAdjustmentType GetBenefitAdjustment(int t, out double rate)
        {
            const string column = "BJ";
            const int row = 20;
            Func<double> getRate = () => GetDouble(row + t, column);
            switch (t)
            {
                case 1:
                    rate = 0;
                    return BenefitAdjustmentType.None;
                case 2:
                    rate = getRate();
                    return BenefitAdjustmentType.BenefitReduction;
                case 3:
                    rate = getRate();
                    return BenefitAdjustmentType.SalaryLimit;
                case 4:
                    rate = getRate();
                    return BenefitAdjustmentType.CareRevaluation;
                case 5:
                    rate = getRate();
                    return BenefitAdjustmentType.DcImplementation;
            }
            throw new ArgumentException("Uknown benefit adjustment type");
        }

        private void GetAbf()
        {
            xls.ActiveSheetByName = "Panels";
            AbfData = new AbfData(GetDouble("ABFDeficit"), GetDouble("ABFRPConts", 0), GetDouble("ABFRPNetCash", 0), GetDouble("ABFValueProp"),
                GetDouble("ABFConts", 0), GetDouble("ABFNetCash", 0), GetDouble("ABFSaving", 0), GetDouble("ABFValue", 0));
        }

        private void GetVarFunnelResults()
        {
            xls.ActiveSheetByName = "VaRCalcs";
            const int row = 122;
            VarFunnelData = new List<VarFunnelData>();
            Func<int, string, VarFunnelData> getData = (r, c) =>
                new VarFunnelData(GetDouble(r++, c, 0),
                GetDouble(r++, c, 0), GetDouble(r++, c, 0),
                GetDouble(r++, c, 0), GetDouble(r, c, 0));
            VarFunnelData.Add(getData(row, "C"));
            VarFunnelData.Add(getData(row, "D"));
            VarFunnelData.Add(getData(row, "E"));
            VarFunnelData.Add(getData(row, "F"));
            VarFunnelData.Add(getData(row, "G"));
            VarFunnelData.Add(getData(row, "H"));
        }

        private void GetInsurancePanelResults()
        {
            xls.ActiveSheetByName = "Panels";

            BuyinInfo = new BuyinData(
                GetDouble(12, TrackerReaderUtilities.GetCol("BZ"), 0),
                GetDouble(13, TrackerReaderUtilities.GetCol("BZ"), 0),
                GetDouble("InsPenAsset", 0),
                GetDouble("InsNonPenAsset", 0));

            TotalInsurancePremiumPaid = GetDouble(23, TrackerReaderUtilities.GetCol("BZ"), 0);

            var strainsCoords = GetRangeCoords("BuyoutStrains");

            BuyoutStrains = new Dictionary<string, IDictionary<StrainType, double>>(10);

            var i = 0;
            var strainsCol = TrackerReaderUtilities.GetCol("CB");

           // var otherKeys = basesEvolution.Keys.Where(key => key.BasisType != BasisType.Buyout);

            while (xls.GetCellValue(strainsCoords.Row + i, strainsCol) != null)
            {
                var basisName = GetString(strainsCoords.Row + i, strainsCol); 
                var currentstrain = GetDouble(strainsCoords.Row + i, strainsCol + 2);
                var maxstrain = GetDouble(strainsCoords.Row + i, strainsCol + 3); ;
                var minstrain = GetDouble(strainsCoords.Row + i, strainsCol + 4); ;

                var matrixItem = new Dictionary<StrainType, double>
                        {
                            { StrainType.Current, currentstrain},
                            { StrainType.Max, maxstrain },
                            { StrainType.Min, minstrain }
                        };

                BuyoutStrains.Add(basisName, matrixItem);

                i++;
            }

            InsurancePointIncreaseAmount = GetDouble("InsImpact50bps", 0, "Panels");
        }

        public void GetInvestmentStrategyResults()
        {
            xls.ActiveSheetByName = "Panels";

            InvestmentStrategyData = new InvestmentStrategyData
                (
                GetDouble(132, "AE"),
                GetDouble(137, "AE", 0),
                GetDouble(138, "AE", 0),
                GetDouble(117, "AD", 0)
                );

            HedgeBreakdown = new HedgeBreakdownData
                {
                    CurrentBasis = new Dictionary<HedgeType, HedgeBreakdownItem>
                        {
                            { HedgeType.Interest, new HedgeBreakdownItem(GetDouble(56, "AB"), GetDouble(57, "AB"), GetDouble(58, "AB"), GetDouble(59, "AB")) },
                            { HedgeType.Inflation, new HedgeBreakdownItem(GetDouble(56, "AC"), GetDouble(57, "AC"), GetDouble(58, "AC"), GetDouble(59, "AC")) }
                        },
                    Cashflows = new Dictionary<HedgeType, HedgeBreakdownItem>
                        {
                            { HedgeType.Interest, new HedgeBreakdownItem(GetDouble(56, "AD"), GetDouble(57, "AD"), GetDouble(58, "AD"), GetDouble(59, "AD")) },
                            { HedgeType.Inflation, new HedgeBreakdownItem(GetDouble(56, "AE"), GetDouble(57, "AE"), GetDouble(58, "AE"), GetDouble(59, "AE")) }
        }
                };
        }

        private void GetVolatilityAssumptions()
        {
            xls.ActiveSheetByName = "VolatilityAssumptions";

            var col = "G";
            var row = 6;

            VolatilityAssumptions = new VolatilityAssumptionResults(
                new Dictionary<AssetClassType, double>
                {
                    { AssetClassType.LiabilityInterest, GetDouble(row++, col) },
                    { AssetClassType.FixedInterestGilts, GetDouble(row++, col) },
                    { AssetClassType.IndexLinkedGilts, GetDouble(row++, col) },
                    { AssetClassType.CorporateBonds, GetDouble(row++, col) },
                    { AssetClassType.LiabilityInflation, GetDouble(row++, col) },
                    { AssetClassType.IndexLinkedGiltsInflation, GetDouble(row++, col) },
                    { AssetClassType.Abf, GetDouble(row++, col) },
                    { AssetClassType.CreditSpread, GetDouble(row++, col) },
                    { AssetClassType.DiversifiedCredit, GetDouble(row++, col) },
                    { AssetClassType.Equity, GetDouble(row++, col) },
                    { AssetClassType.DiversifiedGrowth, GetDouble(row++, col) },
                    { AssetClassType.Property, GetDouble(row++, col) },
                    { AssetClassType.PrivateMarkets, GetDouble(row++, col) },
                    { AssetClassType.LiabilityLongevity, GetDouble(row++, col) }
                },
                GetDouble("Rate_Inflation_Correlation"));
        }

        private void GetVarWaterfallResults()
        {
            xls.ActiveSheetByName = "VaRCalcs";
            var row = 81;
            var col = "K";
            Waterfall = new WaterfallData(
                GetDouble(row++, col, 0),
                GetDouble(row++, col, 0),
                GetDouble(row++, col, 0),
                GetDouble(row++, col, 0),
                GetDouble(row++, col, 0),
                GetDouble(row++, col, 0),
                GetDouble(row++, col, 0),
                GetDouble(row++, col, 0),
                GetDouble(row++, col, 0),
                GetDouble(row + 1, col, 0),
                GetDouble(row + 2, col, 0));
        }

        private void GetSurplusAnalysisResults()
        {
            xls.ActiveSheetByName = "Tracker";
            const string column = "BF";
            SurplusAnalysisData = new SurplusAnalysisData(
                GetDouble(92, column),
                GetDouble(93, column),
                GetDouble(94, column),
                GetDouble(95, column),
                GetDouble(96, column),
                GetDouble(97, column),
                GetDouble(98, column),
                GetDouble(99, column)
                );
            MarketChange = GetDouble(94, column);
        }

        private void GetPanelResults()
        {
            xls.ActiveSheetByName = "Panels";
            FroData = new FroData(GetDouble("FROBasisChange"), GetDouble("FROTVsPaid", 0), GetDouble("FROLiabsDischarged", 0), GetDouble("UserDefLiab"), GetDouble("PreFRODefLiab"));

            EFroData = new EFroData(new Dictionary<int, double>(), new Dictionary<int, double>(), 0, 0, 0, 0, 0);//todo: empty data objects.
            if (GetString("FROType").ToLower() != "bulk")
            {
                var EFROTransferValuesCoords = GetRangeCoords("EFROTransferValues");

                var ActiveTransferValuesPayable = new Dictionary<int, double>();
                var DeferredTransferValuesPayable = new Dictionary<int, double>();
                for (int i = 1; i <= 100; i++)
                {
                    ActiveTransferValuesPayable.Add(i, GetDouble(EFROTransferValuesCoords.Row + i, EFROTransferValuesCoords.Col + 1, 0));
                    DeferredTransferValuesPayable.Add(i, GetDouble(EFROTransferValuesCoords.Row + i, EFROTransferValuesCoords.Col + 2, 0));
                }

                EFroData = new EFroData(ActiveTransferValuesPayable, DeferredTransferValuesPayable, GetDouble("EFROCETVsPaid"), GetDouble("EFROLiabsDischarged"), GetDouble("EFROChangeInActLiab"), GetDouble("EFROChangeInDefLiab"), GetDouble("UserActLiab") + GetDouble("UserDefLiab"));
            }

            var etvChanged = GetDouble("ETVUnenhancedValue", 0) > 0 ? GetDouble("ETVBasisChange") : 0;

            EtvData = new EtvData(GetDouble("PreETVDefLiab", 0), etvChanged, GetDouble(17, "AR", 0),
                GetDouble("ETVUnenhancedTVsPaid", 0), GetDouble("ETVEnhancedTVsPaid", 0), GetDouble("ETVLiabsDischarged", 0), GetDouble(17, "AU", 0), GetDouble(18, "AU", 0), GetDouble("ETVUnenhancedTVsPaid", 0));

            PIEInit = new PIEInitData(GetDouble("PIEPenLiab"), GetDouble("PIEValueOfIncs"));

            PieData = new PieData(new BeforeAfter<double>(GetDouble("PIELiabBefore"), GetDouble("PIELiabAfter")),
                GetDouble("PIEPenLiab"), GetDouble(15, "BD"), GetDouble("PIEPenLiabEligible"), GetDouble("PIEPenLiabNilIncs"), GetDouble("PIEValueOfIncs"), GetDouble(22, "BA"),
                0, GetDouble("PIEMaxUplift"), GetDouble("PIEValueShared"));
        }

        private void GetProfitLossForecastResults()
        {
            xls.ActiveSheetByName = "Accounting";
            ProfitLossForecast = new Dictionary<ValuationBasisType, ProfitLossForecast>
            {
                {
                    ValuationBasisType.Ias19, new ProfitLossForecast(GetDouble(32, "P", 0),
                        GetDouble(32, "O", 0), GetDouble(32, "N", 0), GetDouble(32, "M", 0))
                },

                {
                    ValuationBasisType.Frs17, new ProfitLossForecast(GetDouble(32, "U", 0),
                        GetDouble(32, "R", 0), GetDouble(32, "T", 0), GetDouble(32, "S", 0))
                }
            };
        }

        private void GetFutureServiceCostResults()
        {
            xls.ActiveSheetByName = "FundingLevel";
            FutureServiceCostData = new FutureServiceCostData(GetDouble(38, "N", 0), GetDouble(39, "N", 0),
                GetDouble(40, "N", 0), GetDouble(41, "N", 0), GetDouble(42, "N", 0));
        }

        private void GetBalanceEstimateResults()
        {
            xls.ActiveSheetByName = "Accounting";
            BalanceSheetEstimateLens = new BalanceData(GetDouble(29, "B"), GetDouble(30, "C"));
        }

        private void GetFundingLevelResults()
        {
            xls.ActiveSheetByName = "FundingLevel";
            double totalBuyin = 0;
            try
            {
                double.TryParse(((TFormula)xls.GetCellValue(33, 4)).Result.ToString(), out totalBuyin);
            }
            catch { }
            FundingLevel = new FundingData(
                GetDouble(29, "C"), GetDouble(30, "C"), GetDouble(31, "C"),
                GetDouble(32, "D"), GetDouble(33, "D"), GetDouble(36, "D") - GetDouble(36, "C"));
        }

        private void GetClientAssumptions()
        {
            xls.ActiveSheetByName = "Tracker";
            AttributionEnd = GetDate("AnalysisDate");
            AttributionStart = GetDate("AttrStartDate");
            RefreshDate = GetDateSlow(5, "B");

            xls.ActiveSheetByName = "Panels";

            const string c = "C";
            var r = 13;
            Func<double> getData = () => { r++; return GetDouble(r, c); };
            var clientLiabilityAssumptions = new Dictionary<AssumptionType, double>
                {
                    { AssumptionType.PreRetirementDiscountRate, getData() },
                    { AssumptionType.PostRetirementDiscountRate, getData() },
                    { AssumptionType.PensionDiscountRate, getData() },
                    { AssumptionType.SalaryIncrease, getData() },
                    { AssumptionType.InflationRetailPriceIndex, getData() },
                    { AssumptionType.InflationConsumerPriceIndex, getData() }
                };
            var clientLifeExpectancyAssumptions = new LifeExpectancyAssumptions(getData(), getData());
            var userPinc1Coords = GetRangeCoords("UserPinc1");
            var clientPensionIncreases = new Dictionary<int, double>
                {
                    { 1, GetDouble(userPinc1Coords.Row, userPinc1Coords.Col) },
                    { 2, GetDouble(userPinc1Coords.Row + 1, userPinc1Coords.Col) },
                    { 3, GetDouble(userPinc1Coords.Row + 2, userPinc1Coords.Col) },
                    { 4, GetDouble(userPinc1Coords.Row + 3, userPinc1Coords.Col) },
                    { 5, GetDouble(userPinc1Coords.Row + 4, userPinc1Coords.Col) },
                    { 6, GetDouble(userPinc1Coords.Row + 5, userPinc1Coords.Col) },
                };

            var investmentGrowth = GetDouble(20, "S", 0);
            var discountRate = GetDouble(21, "S", 0);
            RecoveryPlanAssumptions = new RecoveryPlanAssumptions(investmentGrowth, discountRate,
                GetDouble(22, "W"), 100000000, GetDouble(21, "W"), Math.Abs(investmentGrowth) > Utils.Precision, (int)GetDouble(19, "W"), (int)GetDouble(20, "W"));

            OutPerformanceMaxReturn = GetDouble(25, "S", .0);

            double fbrate;
            var fbtype = GetBenefitAdjustment((int)GetDouble("FBOptionNo"), out fbrate);            

            SchemeAssumptions = new SchemeAssumptions
                (
                    GetString("FROType").ToLower() == "bulk" ? FROType.Bulk : FROType.Embedded, //TODO
                    new FlexibleReturnOptions(GetDouble(14, "AJ", 0), GetDouble(16, "AJ")),
                    new EmbeddedFlexibleReturnOptions(GetDouble("EFROTakeUpRate"), GetDouble("EFROBasisChange")),
                    new EnhancedTransferValueOptions(GetDouble(14, "AR", 0), GetDouble(16, "AR"), GetDouble(18, "AR")),
                    new PieOptions(GetDouble(19, "BA"), GetDouble(27, "BA"), GetDouble(30, "BA")),
                    new FutureBenefitsParameters(GetDouble("FBExtraEeConts"), fbtype, fbrate),
                    Allocations,
                    new InvestmentStrategyOptions(GetDouble("CashInjection", 0), GetDouble("UserPropSyntheticCredit"), GetDouble("UserPropSyntheticEquity"), GetBool("LDIInForceAtAnalysisDate", "Panels"), GetDouble("UserInterestHedge"), GetDouble("UserInflationHedge")),
                    new AbfOptions(GetDouble("ABFValue", 0)),
                    new InsuranceParameters(GetDouble("InsPenProp"), GetDouble("InsNonPenProp")),                    
                    ParseRecoveryPlanType(),
                    new WaterfallOptions(GetDouble("CI", "VaRCalcs"), (int)GetDouble("Time", "VaRCalcs"))
                );

            BasisAssumptions = new BasisAssumptions
                (
                    clientLiabilityAssumptions,
                    clientLifeExpectancyAssumptions,
                    clientPensionIncreases,
                    ClientAssetAssumptions,
                    RecoveryPlanAssumptions
                );
        }

        private void GetRangeValues()
        {
            xls.ActiveSheetByName = "Tracker";
            BuyInAtAnalysisDate = GetDouble("BuyInAtAnalysisDate");

            xls.ActiveSheetByName = "JourneyPlan";
            AssetsAtSED = GetDouble("AssetsAtSED");
            JPInvGrowth = GetDouble("JPInvGrowth", 0);
            JP2InvGrowth = GetDouble("JP2InvGrowth", 0);

            xls.ActiveSheetByName = "Panels";
            InsPenCost = GetDouble("InsPenCost");
            InsNonPenCost = GetDouble("InsNonPenCost");
            
            xls.ActiveSheetByName = "VolatilityAssumptions";
            var GiltLiabDuration = GetDouble("GiltLiabDuration");
            var GiltLiabPV01 = GetDouble("GiltLiabPV01");
            var GiltLiabIE01 = GetDouble("GiltLiabIE01");
            var GiltBuyInPV01 = GetDouble("GiltBuyInPV01");
            var GiltBuyInIE01 = GetDouble("GiltBuyInIE01");
            xls.ActiveSheetByName = "LDIHedge";
            var GiltTotLiab = GetDouble("GiltTotLiab");

            GiltBasisLiability = new GiltBasisLiabilityData(GiltTotLiab, GiltLiabDuration, GiltLiabPV01, GiltLiabIE01, GiltBuyInPV01, GiltBuyInIE01, null);
        }

        private void GetAssetHedgeBreakdownData()
        {
            var assetHedgeBreakdownData = new Dictionary<DateTime, HedgeBreakdownData>();
            for (var i = 1; i <= xls.SheetCount; i++)
            {
                var sheet = xls.GetSheetName(i);
                if (!sheet.ToLower().StartsWith("assets"))
                    continue;

                xls.ActiveSheetByName = sheet;

                var effectiveDate = GetDate("EffectiveDate");
                var builtInHedge = GetRangeCoords("BuiltInHedge");
                var builtInHedgeRow = builtInHedge.Row;
                var builtInHedgeCol = builtInHedge.Col;
                var hedgeSummary = GetRangeCoords("HedgeSummary");
                var hedgeSummaryRow = hedgeSummary.Row;
                var hedgeSummaryCol = hedgeSummary.Col;
                var hedgeBreakdownData = new HedgeBreakdownData
                {
                    CurrentBasis = new Dictionary<HedgeType, HedgeBreakdownItem>
                        {
                            { HedgeType.Interest, new HedgeBreakdownItem(0, 0, 0, GetDouble(hedgeSummaryRow, hedgeSummaryCol, 0)) },
                            { HedgeType.Inflation, new HedgeBreakdownItem(0, 0, 0, GetDouble(hedgeSummaryRow + 1, hedgeSummaryCol, 0)) }
                        },
                    Cashflows = new Dictionary<HedgeType, HedgeBreakdownItem>
                        {
                            { HedgeType.Interest, new HedgeBreakdownItem(GetDouble(builtInHedgeRow, builtInHedgeCol, 0), GetDouble(builtInHedgeRow, builtInHedgeCol + 1, 0), 0, GetDouble(hedgeSummaryRow, hedgeSummaryCol + 1, 0)) },
                            { HedgeType.Inflation, new HedgeBreakdownItem(GetDouble(builtInHedgeRow + 1, builtInHedgeCol, 0), GetDouble(builtInHedgeRow + 1, builtInHedgeCol + 1, 0), 0, GetDouble(hedgeSummaryRow + 1, hedgeSummaryCol + 1, 0)) }
                        }
                };
                assetHedgeBreakdownData.Add(effectiveDate, hedgeBreakdownData);
            }

            AssetHedgeBreakdownData = assetHedgeBreakdownData;
        }

        private void GetClientJourneyPlanOptions()
        {
            xls.ActiveSheetByName = "NewJP";

            var cfbasis = GetString("CashflowBasis");
            var tpbasis = xls.GetCellValue(9, TrackerReaderUtilities.GetCol("B")) != null ? GetString(9, TrackerReaderUtilities.GetCol("B")) : string.Empty;
            var ssbasis = xls.GetCellValue(10, TrackerReaderUtilities.GetCol("B")) != null ? GetString(10, TrackerReaderUtilities.GetCol("B")) : string.Empty;
            var bobasis = xls.GetCellValue(11, TrackerReaderUtilities.GetCol("B")) != null ? GetString(11, TrackerReaderUtilities.GetCol("B")) : string.Empty;

            var SalaryInflation = GetString("SalaryInflation");
            var InvGrowthBefore = Double.Parse(GetString("InvGrowthBefore").Replace("%", ""));
            var SelfSuffDiscountRate = Double.Parse(GetString("SelfSuffDiscountRate").Replace("%", ""));
            var XCStartYear = GetDouble("XCStartYear");
            var XCInitialConts = GetDouble("XCInitialConts");
            var XCTerm = GetDouble("XCTerm");
            var XCIncs = GetDouble("XCIncs");
            var NewJPUseCurves = GetBool("NewJPUseCurves", "NewJP");
            var NewJPIncludeBuyIns = GetBool("NewJPIncludeBuyIn", "NewJP");
            var NewJPAddRP = GetBool("NewJPAddRP", "NewJP");
            var NewJPAddXC = GetBool("NewJPAddXC", "NewJP");
            var TriggerType = GetString("TriggerType");
            var TriggerSteps = GetDouble("TriggerSteps");
            var Trigger1Time = GetDouble("Trigger1Time");
            var TriggerTransitionPeriod = GetDouble("TriggerTransitionPeriod");

            var InvGrowthAfterCoords = GetRangeCoords("InvGrowthAfter");
            var initialGrowthAfter = GetDouble(InvGrowthAfterCoords.Row, InvGrowthAfterCoords.Col);
            var targetGrowthAfter = GetDouble(InvGrowthAfterCoords.Row + 1, InvGrowthAfterCoords.Col);

            ClientJourneyPlanOptions = new JourneyPlanOptions
                {
                    FundingBasisKey = Bases.First(x => x.DisplayName == tpbasis).MasterBasisId,
                    SelfSufficiencyBasisKey = Bases.First(x => x.DisplayName == ssbasis).MasterBasisId,
                    BuyoutBasisKey = Bases.Any(x => x.DisplayName == bobasis) ? Bases.First(x => x.DisplayName == bobasis).MasterBasisId : 0,
                    SalaryInflationType = (SalaryType)Enum.Parse(typeof(SalaryType), SalaryInflation),
                    CurrentReturn = InvGrowthBefore,
                    SelfSufficiencyDiscountRate = SelfSuffDiscountRate,
                    RecoveryPlan = new NewJPExtraContributionOptions((int)XCStartYear, XCInitialConts, (int)XCTerm, XCIncs, NewJPAddRP, NewJPAddXC),
                    TriggerSteps = (int)TriggerSteps,
                    TriggerTimeStartYear = (int)Trigger1Time,
                    TriggerTransitionPeriod = (int)TriggerTransitionPeriod,
                    UseCurves = NewJPUseCurves ,
                    InitialReturn = initialGrowthAfter,
                    FinalReturn = targetGrowthAfter,
                    IncludeBuyIns = NewJPIncludeBuyIns
                };
        }

        private void GetUserAssumptionsTable()
        {
            UserAssumptionsTable = new Dictionary<string, BasisAssumptions>();
            xls.ActiveSheetByName = "CALCS";
            var userAssumptionsTableRange = GetRangeCoords("UserAssumptionsTable");
            var row = userAssumptionsTableRange.Row;
            var col = userAssumptionsTableRange.Col;
            while (xls.GetCellValue(row, ++col) != null)
            {
                var basisName = GetString(row, col);
                var assumptions = new BasisAssumptions
                    (
                        new Dictionary<AssumptionType, double>
                            {
                                { AssumptionType.PreRetirementDiscountRate, GetDouble(row + 1, col) },
                                { AssumptionType.PostRetirementDiscountRate, GetDouble(row + 2, col) },
                                { AssumptionType.PensionDiscountRate, GetDouble(row + 3, col) },
                                { AssumptionType.SalaryIncrease, GetDouble(row + 4, col) },
                                { AssumptionType.InflationRetailPriceIndex, GetDouble(row + 5, col) },
                                { AssumptionType.InflationConsumerPriceIndex, GetDouble(row + 6, col) }
                            },
                        new LifeExpectancyAssumptions(GetDouble(row + 7, col), GetDouble(row + 8, col)),
                        new Dictionary<int, double>
                            {
                                { 1, GetDouble(row + 10, col) },
                                { 2, GetDouble(row + 11, col) },
                                { 3, GetDouble(row + 12, col) },
                                { 4, GetDouble(row + 13, col) },
                                { 5, GetDouble(row + 14, col) },
                                { 6, GetDouble(row + 15, col) }
                            },
                        null,
                        null
                    );
                UserAssumptionsTable.Add(basisName, assumptions);
            };
        }

        private void GetBasisAssumptionsCollection()
        {
            BasisAssumptionsCollection = new Dictionary<string, IBasisAssumptions>();

            foreach (var bName in Bases.Select(x => x.DisplayName).Distinct())
            {
                var b = Bases.Where(x => x.DisplayName == bName).OrderBy(x => x.EffectiveDate).Last();

                BasisAssumptions assumptions = null;

                if (b.DisplayName == ChosenBasis)
                {
                    assumptions = BasisAssumptions;
                }
                else if (UserAssumptionsTable.ContainsKey(b.DisplayName))
                {
                    assumptions = UserAssumptionsTable[b.DisplayName];
                }
                else
                {
                    var assumptionIndexEvolutionService = new AssumptionIndexEvolutionService();
                    var pensionIncreaseIndexEvolutionService = new PensionIncreaseIndexEvolutionService();

                    Func<AssumptionType, double> liabs = t => assumptionIndexEvolutionService.GetAssumptionIndexValue(t, AttributionEnd, b);

                    var liabilityAssumptions = Utils.EnumToArray<AssumptionType>().ToDictionary(x => x, x => liabs(x));
                    var lifeExpectancy = new LifeExpectancyAssumptions(b.SimpleAssumptions[SimpleAssumptionType.LifeExpectancy65].Value, b.SimpleAssumptions[SimpleAssumptionType.LifeExpectancy65_45].Value);
                    var pensionIncreases = pensionIncreaseIndexEvolutionService.GetPensionIncreaseIndexValue(AttributionEnd, b);

                    assumptions = new BasisAssumptions
                        (
                            liabilityAssumptions,
                            lifeExpectancy,
                            pensionIncreases,
                            null, //these aren't currently needed for testing but should include at some point
                            null
                        );
                }

                BasisAssumptionsCollection.Add(b.DisplayName, assumptions);
            }
        }

        private RecoveryPlanType ParseRecoveryPlanType()
        {
            var rp = GetString(24, 23);
            if (rp.Equals("Current plan", StringComparison.InvariantCultureIgnoreCase))
                return RecoveryPlanType.Current;
            return rp.Equals("New plan", StringComparison.InvariantCultureIgnoreCase) ?
                RecoveryPlanType.NewDynamic : RecoveryPlanType.NewFixed;
        }

        private void GetCashFlowLensData()
        {
            var before = new Dictionary<int, Dictionary<MemberStatus, double>>(50);
            var after = new Dictionary<int, Dictionary<MemberStatus, double>>(50);
            xls.ActiveSheetByName = "Cashflow";

            double year;
            var row = 30;
            var beforeCol = 1;
            var afterCol = 11;
            Func<int, int, Dictionary<MemberStatus, double>> getData = (r, c) =>
                new Dictionary<MemberStatus, double>
                    {
                        { MemberStatus.ActivePast, GetDouble(r, c) },
                        { MemberStatus.Deferred, GetDouble(r, c + 1) },
                        { MemberStatus.Pensioner, GetDouble(r, c + 2) }
                    };
            while (TryGetDouble(row, beforeCol, out year))
            {
                before.Add((int)year, getData(row, beforeCol + 1));
                after.Add((int)year, getData(row, afterCol));
                row++;
            }

            CashFlowLens = new BeforeAfter<Dictionary<int, Dictionary<MemberStatus, double>>>(before, after);
        }

        private void GetJourneyPlan()
        {
            JourneyPlan = new BeforeAfter<Dictionary<int, BalanceData>>
                (new Dictionary<int, BalanceData>(51), new Dictionary<int, BalanceData>(51));

            xls.ActiveSheetByName = "JourneyPlan";

            var beforeCoords = GetRangeCoords("JPOutputA1");
            var afterCoords = GetRangeCoords("JP2OutputA1");

            for (int i = 0; i <= 50; i++)
            {
                JourneyPlan.Before.Add(i, new BalanceData(GetDouble(beforeCoords.Row + i, beforeCoords.Col + 2), GetDouble(beforeCoords.Row + i, beforeCoords.Col + 1)));
                JourneyPlan.After.Add(i, new BalanceData(GetDouble(afterCoords.Row + i, afterCoords.Col + 2), GetDouble(afterCoords.Row + i, afterCoords.Col + 1)));
            }

            JPAfterLiabDuration = GetDouble("JP2LiabDuration");
        }

        private void GetJourneyPlanResults()
        {
            JourneyPlanLens = new BeforeAfter<IList<LensData>>(new List<LensData>(), new List<LensData>());
            xls.ActiveSheetByName = "JourneyPlan";

            var i = 31;
            var max = Utils.MaxPlanTermInYears + i;
            while (xls.GetCellValue(i, 10) != null && xls.GetCellValue(i, 11) != null && i <= max)
            {
                JourneyPlanLens.Before.Add(new LensData(GetDouble(i, "K"), GetDouble(i, "L")));
                JourneyPlanLens.After.Add(new LensData(GetDouble(i, "O"), GetDouble(i, "P")));
                i++;
            }
        }

        private void GetRecoveryPlan()
        {
            xls.ActiveSheetByName = "Contributions";
            RecoveryPlan = new List<RecoveryPayment>();
            for (var row = 40; xls.GetCellValue(row, 2) != null; row++)
            {
                RecoveryPlan.Add(new RecoveryPayment(1, GetDate(row, "B"), GetDate(row, "C"), GetDouble(row, "D"), (int)GetDouble(row, "E")));
            }
        }

        private void GetRecoveryPlanPayment()
        {
            xls.ActiveSheetByName = "RecoveryPlan";
            RecoveryPlanPaymentRequired = GetDouble("RPAmount");
        }

        private IDictionary<SimpleAssumptionType, SimpleAssumption> GetSimpleAssumptions()
        {
            var simpleAssumptions = new Dictionary<SimpleAssumptionType, SimpleAssumption>();
            var t = SimpleAssumptionType.LifeExpectancy65;
            simpleAssumptions.Add(t, new SimpleAssumption(t, GetDouble("LifeExp65"), "Pensioner", true));
            t = SimpleAssumptionType.LifeExpectancy65_45;
            simpleAssumptions.Add(t, new SimpleAssumption(t, GetDouble("LifeExp65_45"), "Act/Def", true));

            return simpleAssumptions;
        }

        private Dictionary<DateTime, double> GetDelta(int row, string column)
        {
            var result = new Dictionary<DateTime, double>();
            var r = row;
            while (xls.GetCellValue(r, TrackerReaderUtilities.GetCol(column)) != null)
            {
                result.Add(GetDate(r, column), GetDouble(r, IncrementCol(column)));
                r++;
            }
            return result;
        }

        private void GetLiabilityEvolutionResults()
        {
            xls.ActiveSheetByName = "Tracker";

            LiabilityEvolution = new Dictionary<DateTime, LiabilityEvolutionItem>(1000);

            var startCell = GetRangeCoords("LiabTrackingA1");
            var col = startCell.Col;
            var row = startCell.Row;

            DateTime date;

            while (TryGetDate(row, col, out date))
            {
                LiabilityEvolution.Add(
                    date,
                    new LiabilityEvolutionItem
                        (
                            date,
                            GetDouble(row, col + 1, 0),
                            GetDouble(row, col + 2, 0),
                            GetDouble(row, col + 3, 0),
                            GetDouble(row, col + 4, 0),
                            GetDouble(row, col + 5, 0),
                            GetDouble(row, col + 6, 0),
                            GetDouble(row, col + 7, 0),
                            GetDouble(row, col + 8, 0),
                            GetDouble(row, col + 9, 0),
                            GetDouble(row, col + 10, 0),
                            GetDouble(row, col + 11, 0),
                            GetDouble(row, col + 12, 0),
                            GetDouble(row, col + 13, 0),
                            GetDouble(row, col + 14, 0),
                            GetDouble(row, col + 15, 0),
                            GetDouble(row, col + 16, 0),
                            GetDouble(row, col + 17, 0),
                            GetDouble(row, col + 18, 0),
                            GetDouble(row, col + 19, 0),
                            GetDouble(row, col + 20, 0),
                            GetDouble(row, col + 21, 0),
                            GetDouble(row, col + 22, 0),
                            GetDouble(row, col + 23, 0)
                        ));

                row++;
            }
        }

        private void GetExperienceResults()
        {
            xls.ActiveSheetByName = "Tracker";

            LiabilityExperience = new Dictionary<DateTime, double>();

            DateTime date;
            var startCell = GetRangeCoords("LiabExperience");
            var col = startCell.Col;
            var row = startCell.Row;

            while (TryGetDate(row, col, out date))
            {
                LiabilityExperience.Add(date, GetDouble(row, col + 1, 0));
                row++;
            }

            BuyinExperience = new Dictionary<DateTime, double>();

            startCell = GetRangeCoords("BuyInExperience");
            col = startCell.Col;
            row = startCell.Row;

            while (TryGetDate(row, col, out date))
            {
                BuyinExperience.Add(date, GetDouble(row, col + 1, 0));
                row++;
            }

            AssetExperience = new Dictionary<DateTime, double>();

            startCell = GetRangeCoords("AssetExperience");
            col = startCell.Col;
            row = startCell.Row;

            while (TryGetDate(row, col, out date))
            {
                AssetExperience.Add(date, GetDouble(row, col + 1, 0));
                row++;
            }
        }

        private void GetExpectedDeficit()
        {
            xls.ActiveSheetByName = "ExpectedDeficit";

            ExpectedDeficit = new Dictionary<DateTime, ExpectedDeficitData>(1000);

            var col = TrackerReaderUtilities.GetCol("O");
            var row = 6;

            DateTime date;

            while (TryGetDate(row, col, out date) && date <= LiabilityEvolution.Keys.Max())
            {
                ExpectedDeficit.Add(date,
                    new ExpectedDeficitData(
                        date,
                        GetDouble(row, TrackerReaderUtilities.GetCol("AI")),
                        GetDouble(row, TrackerReaderUtilities.GetCol("AH")),
                        GetDouble(row, TrackerReaderUtilities.GetCol("S")),
                        GetDouble(row, TrackerReaderUtilities.GetCol("AJ"))));
                row++;
            }
        }

        private void GetAssetEvolutionResults()
        {
            xls.ActiveSheetByName = "Tracker";

            AssetEvolution = new Dictionary<DateTime, AssetEvolutionItem>(1000);
            var LDIEvolution = new List<LDI.LDIEvolutionItem>();

            var startCell = GetRangeCoords("AssetA1");
            var col = startCell.Col;
            var row = startCell.Row;

            var assetVal = .0;

            //don't start adding LDI data until we reach the start date of the basis LDI is linked to
            var ldiStart = 
                string.IsNullOrEmpty(this.LDIBasis)
                ? DateTime.MaxValue
                : Bases.Where(b => b.DisplayName == this.LDIBasis).Select(b => b.EffectiveDate).Min();

            while (TryGetDouble(row, col + 9, out assetVal))
            {
                var date = GetDate(row, col);

                AssetEvolution.Add(
                    date,
                    new AssetEvolutionItem
                        (
                            date,
                            GetDouble(row, col + 1, 0),
                            GetDouble(row, col + 2, 0),
                            GetDouble(row, col + 4, 0),
                            GetDouble(row, col + 5, 0),
                            GetDouble(row, col + 6, 0),
                            GetDouble(row, col + 7, 0),
                            GetDouble(row, col + 8, 0),
                            GetDouble(row, col + 9, 0),
                            GetDouble(row, col + 10, 0)
                        ));

                if (date >= ldiStart)
                {
                    LDIEvolution.Add(
                        new LDIEvolutionItem
                        (
                            date,
                                GetDouble(row, col + 8, 0),
                                GetDouble(row, col + 9, 0)
                        )
                    );
                }

                row++;
            }

            LDIEvolutionData = LDIEvolution;
        }

        private void GetRecoveryPlanLensResults()
        {
            xls.ActiveSheetByName = "RecoveryPlan";

            var oldRecoveryPlan = new List<double>();
            var newRecoveryPlan = new List<double>();

            var rangeOld = new RangeCoords(TrackerReaderUtilities.GetCol("S"), 31);

            GetPartRecoveryPlan(oldRecoveryPlan, rangeOld.Row, rangeOld.Col);
            GetPartRecoveryPlan(newRecoveryPlan, rangeOld.Row, rangeOld.Col + 1);

            RecoveryPlanLens = new BeforeAfter<IList<double>>(oldRecoveryPlan, newRecoveryPlan);
        }

        private void GetPartRecoveryPlan(List<double> recoveryPlanPart, int startRow, int column)
        {
            for (int i = 0; i < 50; i++)
            {
                var row = startRow + i;
                var value = GetDouble(row, column, 0);
                recoveryPlanPart.Add(value);
            }
            // recovery plan might start with an empty and have values later in the timeline
            // but if you can't find any values at any point, then clear it out.
            if (recoveryPlanPart.All(x => Math.Abs(x) < Utils.TestPrecision))
            {
                recoveryPlanPart.Clear();
            }
            // or clear any trailing zeros
            for (int i = recoveryPlanPart.Count - 1; i >= 0; i--)
            {
                if (recoveryPlanPart[i] < 0.00000001 && recoveryPlanPart[i] > -0.00000001)
                    recoveryPlanPart.RemoveAt(i);
                else
                    break;
            }
        }

        private static RevaluationType ParseRevaluationType(string value)
        {
            if (String.IsNullOrEmpty(value))
                return RevaluationType.None;
            if (value.ToLower().StartsWith("c"))
                return RevaluationType.Cpi;
            if (value.ToLower().StartsWith("r"))
                return RevaluationType.Rpi;
            return RevaluationType.Fixed;
        }

        private IEnumerable<CashflowData> GetCashFlows(string sheetName)
        {
            var cashflowData = new List<CashflowData>();

            var cashflowRange = GetRangeCoords("LiabCashflows");

            var offset = cashflowRange.Row - 1;

            return parseCashflowRange(sheetName, cashflowRange.Col, offset);
        }

        private void GetCashflows()
        {
            GetJPCashflows();
            GetJP2Cashflows();
        }

        private void GetJPCashflows()
        {
            xls.ActiveSheetByName = "Cashflow";
            var col = 1;
            var row = 30;

            JPCashflows = new Dictionary<int, IDictionary<MemberStatus, double>>();

            while (xls.GetCellValue(row, col) != null)
            {
                JPCashflows.Add((int)GetDouble(row, col), new Dictionary<MemberStatus, double>
                    {
                        { MemberStatus.ActivePast, GetDouble(row, col + 1) },
                        { MemberStatus.Deferred, GetDouble(row, col + 2) },
                        { MemberStatus.Pensioner, GetDouble(row, col + 3) },
                        { MemberStatus.ActiveFuture, GetDouble(row, col + 5) },
                    });
                row++;
            }
        }

        private void GetJP2Cashflows()
        {
            xls.ActiveSheetByName = "Cashflow";

            var cashflowRange = GetRangeCoords("JP2CashflowsA1");

            var offset = cashflowRange.Row + 2;

            JP2Cashflows = parseCashflowRange("Cashflow", cashflowRange.Col + 1, cashflowRange.Row + 2, true);
        }

        private IEnumerable<CashflowData> parseCashflowRange(string sheetName, int col, int startRow, bool allowZeros = false)
        {
            xls.ActiveSheetByName = sheetName;

            while(xls.GetCellValue(startRow + 1, col) !=  null)
            {
                var status = ParseMemberStatus(GetString(startRow + 1, col));
                var minAge = (int)GetDouble(startRow + 2, col);
                var maxAge = (int)GetDouble(startRow + 3, col);
                var s = xls.GetCellValue(startRow + 8, col) ?? string.Empty;
                var revaluation = ParseRevaluationType(s.ToString());
                var retirementAge = (int)GetDouble(startRow + 4, col);
                var benefit = (GetString(startRow + 5, col)).ToLower() == "pension"
                    ? BenefitType.Pension
                    : BenefitType.LumpSum;
                var pensionIncreaseId = (int?)GetDouble(startRow + 6, col);
                if (benefit == BenefitType.LumpSum)
                    pensionIncreaseId = null;
                var buyinEntry = GetStringWithDefault(startRow + 9, col);
                var isBuyin = buyinEntry.Length > 0 && buyinEntry.ToLower().Trim() != "false";

                var yearData = new Dictionary<int, double>();
                var emptyYears = new List<int>();
                var i = 1;
                for (var r = 15; xls.GetCellValue(startRow + r, col) != null; r++)
                {
                    var cash = GetDouble(startRow + r, col);
                    if (Math.Abs(cash) > Utils.Precision || allowZeros)
                        yearData[i] = cash;
                    else
                        emptyYears.Add(i);
                    i++;
                }
                foreach (var emptyYear in emptyYears.Where(x => x > yearData.Keys.Min() && x < yearData.Keys.Max()))
                    yearData[emptyYear] = 0;

                var cashflows = yearData;

                yield return new CashflowData(col - 1, 1, status, minAge, maxAge, revaluation, retirementAge, benefit, cashflows, cashflows.Keys.Min(), cashflows.Keys.Max(), isBuyin, pensionIncreaseId);

                col++;
            }
        }

        private static MemberStatus ParseMemberStatus(string statusString)
        {
            switch (statusString.ToLower())
            {
                case "act":
                    return MemberStatus.ActivePast;
                case "def":
                case "pup":
                    return MemberStatus.Deferred;
                case "pen":
                    return MemberStatus.Pensioner;
                case "fut":
                    return MemberStatus.ActiveFuture;
                default:
                    throw new ArgumentException(string.Format("Unable to parse {0}.", statusString));
            }
        }

        private void GetIndexData()
        {
            indices = new Dictionary<string, FinancialIndex>();

            xls.ActiveSheetByName = "Stats";
            var dates = new List<DateTime>();
            const int rowOffset = 5;

            for (var column = 2; column <= xls.ColCount; column++)
            {
                var values = new SortedList<DateTime, double>();
                for (var row = rowOffset; xls.GetCellValue(row, 1) != null; row++)
                {
                    if (dates.Count < xls.RowCount)
                        dates.Add(GetDate(row, "A"));

                    var potentialValue = xls.GetCellValue(row, column) ?? .0;

                    var value = (double)potentialValue;
                    var date = dates[row - rowOffset];

                    values.Add(date, value);
                }
                var name = (string)xls.GetCellValue(4, column);
                if (!string.IsNullOrWhiteSpace(name))
                    indices.Add(name, new FinancialIndex(-1, name, values));
            }
        }

        private void GetCompositeIndexComponents()
        {
            CompositeIndexParameters = new List<UnitTests.CompositeIndexParameters>();

            xls.ActiveSheetByName = "Main";

            var compositeIndexCoords = GetRangeCoords("SSISetup");
            var sSIProportions = GetRangeCoords("SSIProportions");

            var totalReturnCompositeIndexParams = new CompositeIndexParameters()
            {
                CiName = (string)xls.GetCellValue(compositeIndexCoords.Row, compositeIndexCoords.Col),
                CustomIndexType = (string)xls.GetCellValue(compositeIndexCoords.Row, compositeIndexCoords.Col + 1),
                Rounding = (double)xls.GetCellValue(compositeIndexCoords.Row, compositeIndexCoords.Col + 2),
                SourceIndexProportion = new Dictionary<FinancialIndex, double>()
                {
                    {
                        indices.Where(i => i.Key == (string)xls.GetCellValue(sSIProportions.Row, sSIProportions.Col + 2)).Single().Value,
                        (double)xls.GetCellValue(sSIProportions.Row, sSIProportions.Col + 1)
                    },
                    {
                        indices.Where(i => i.Key == (string)xls.GetCellValue(sSIProportions.Row + 1, sSIProportions.Col + 2)).Single().Value,
                        (double)xls.GetCellValue(sSIProportions.Row + 1, sSIProportions.Col + 1)
                    }
                }
            };

            CompositeIndexParameters.Add(totalReturnCompositeIndexParams);

            var yieldCompositeIndexParams = new CompositeIndexParameters()
            {
                CiName = (string)xls.GetCellValue(compositeIndexCoords.Row + 1, compositeIndexCoords.Col),
                CustomIndexType = (string)xls.GetCellValue(compositeIndexCoords.Row + 1, compositeIndexCoords.Col + 1),
                Rounding = (double)xls.GetCellValue(compositeIndexCoords.Row + 1, compositeIndexCoords.Col + 2),
                SourceIndexProportion = new Dictionary<FinancialIndex, double>()
                {
                    {
                        indices.Where(i => i.Key == (string)xls.GetCellValue(sSIProportions.Row + 2, sSIProportions.Col + 2)).Single().Value,
                        (double)xls.GetCellValue(sSIProportions.Row + 2, sSIProportions.Col + 1)
                    },
                    {
                        indices.Where(i => i.Key == (string)xls.GetCellValue(sSIProportions.Row + 3, sSIProportions.Col + 2)).Single().Value,
                        (double)xls.GetCellValue(sSIProportions.Row + 3, sSIProportions.Col + 1)
                    }
                }
            };

            CompositeIndexParameters.Add(yieldCompositeIndexParams);
        }

        private void GetCompositeIndexData()
        {
            int nonCompositeIndicesCount = indices.Count();
            xls.ActiveSheetByName = "Testing";
            var dates = new List<DateTime>();
            const int rowOffset = 6;

            for (var column = nonCompositeIndicesCount + 2; column <= xls.ColCount; column++) 
            {
                var values = new SortedList<DateTime, double>();
                for (var row = rowOffset; xls.GetCellValue(row, 1) != null; row++)
                {
                    if (dates.Count < xls.RowCount)
                        dates.Add(GetDate(row, "A"));

                    var potentialValue = xls.GetCellValue(row, column) ?? .0;

                    var value = (double)potentialValue;
                    var date = dates[row - rowOffset];

                    values.Add(date, value);
                }
                var name = (string)xls.GetCellValue(5, column);
                if (!string.IsNullOrWhiteSpace(name))
                    indices.Add(name, new FinancialIndex(-1, name, values));
            }
        }

        private List<SalaryProgression> GetSalaryProgression()
        {
            var salaryProgressions = new List<SalaryProgression>();

            var salaryProgressionRange = GetRangeCoords("SalaryProgression");

            var rowOffset = salaryProgressionRange.Row - 1;

            var col = "A";
            for (var c = 2; xls.GetCellValue(rowOffset + 1, c) != null; c++)
            {
                col = IncrementCol(col);
                var salaryRoll = new Dictionary<int, double>();
                for (var r = 11; xls.GetCellValue(rowOffset + r, c) != null; r++)
                {
                    salaryRoll.Add(r - 10, GetDouble(rowOffset + r, col));
                }
                salaryProgressions.Add(new SalaryProgression(c - 1, 1,
                    (int)GetDouble(rowOffset + 1, col),
                    (int)GetDouble(rowOffset + 2, col),
                    (int)GetDouble(rowOffset + 3, col), salaryRoll));
            }
            return salaryProgressions;
        }

        private void GetContributions()
        {
            ContributionRates = new List<Contribution>();
            const int rowOffset = 74;
            xls.ActiveSheetByName = "Contributions";

            for (var r = 1; xls.GetCellValue(rowOffset + r, 2) != null; r++)
            {
                ContributionRates.Add(new Contribution(1, GetDateSlow(rowOffset + r, "B"), GetDouble(rowOffset + r, "C", 0), GetDouble(rowOffset + r, "D", 0)));
            }

            ReturnOnAssets = GetDouble(28, "B");
            IsGrowthFixed = GetString(25, 2).ToLower().StartsWith("fixed rate");

        }

        private BasisType ParseBasisType(string b)
        {
            switch (b.ToLower())
            {
                case "accounting":
                    return BasisType.Accounting;
                case "buyout":
                    return BasisType.Buyout;
                case "technical provisions":
                    return BasisType.TechnicalProvision;
                case "gilts":
                    return BasisType.Gilts;
                default:
                    throw new ApplicationException(string.Format("Unknown basis {0}", b));
            }
        }

        private string IncrementCol(string col, int inc = 1)
        {
            var alphabet = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z".Split(',');

            var candidate = col.Substring(col.Length - 1, 1);
            var first = col.Length == 1 ? string.Empty : col.Substring(0, 1);

            var idx = Array.IndexOf(alphabet, candidate);

            if (idx + inc >= alphabet.Length)
            {
                candidate = alphabet[alphabet.Length - idx - 1];
                first = alphabet[Array.IndexOf(alphabet, first) + 1];
            }
            else
            {
                candidate = alphabet[idx + inc];
            }
            return string.Format("{0}{1}", first, candidate);
        }

        private IList<Basis> GetBases()
        {
            xls.ActiveSheetByName = "JourneyPlan";
            var giltIndex = indices[GetString(49, 3)];

            xls.ActiveSheetByName = "Main";
            var volatility = GetDouble("InflationVol");
            Ias19ExpenseLoading = GetDouble("IAS19ExpenseLoading");

            var increaseDefinitions = GetIncreaseDefinitions();

            var result = new List<Basis>();
            for (var i = 1; i <= xls.SheetCount; i++)
            {
                var sheet = xls.GetSheetName(i);
                if (!sheet.ToLower().StartsWith("liab"))
                    continue;

                xls.ActiveSheetByName = sheet;
                var t = ParseBasisType(GetString(6, 2));

                var name = GetString(7, TrackerReaderUtilities.GetCol("B"));
                var anchorDate = GetDate(8, "B");
                var lastPensionIncrease = GetDate(9, "B");
                var linkedToCorpBonds = GetBool("LinkedToCorpBonds", sheet);

                var simpleAssumptions = GetSimpleAssumptions();
                var salaryProgressions = GetSalaryProgression();

                var liabIncreases = GetLiabIncreases();

                var cashflows = GetCashFlows(sheet);

                var assumptions = new List<FinancialAssumption>();
                var pensionIncreases = new List<PensionIncreaseAssumption>();
                GetAssumptions(assumptions, pensionIncreases, volatility, increaseDefinitions);

                var totalCosts = new TotalCosts(0,
                        GetDouble("ActLiab"),
                        GetDouble("DefLiab"),
                        GetDouble("PenLiab"),
                        GetDouble("ServiceCost")
                    );

                var aoci = GetDouble("USGAAP_AOCI", 0);
                var unrecognisedPriorServiceCost = GetDouble("USGAAP_UnrecognisedPriorService", 0);
                var amortisationPriorServiceCost = GetDouble("USGAAP_AmortisationOfPriorService", 0);
                var amortisationPeriodForActGain = GetDouble("USGAAP_AmortisationPeriodForActGain", (int?)null);

                if (!masterBasisMockIds.ContainsKey(name))
                    masterBasisMockIds.Add(name, masterBasisMockIds.Any() ? masterBasisMockIds.Values.Max() + 1 : 1);
                var mockMasterBasisId = masterBasisMockIds[name];
                var bondYield = new BondYield();//todo: populate

                var ab = new Basis(1, 1, mockMasterBasisId, name, t, anchorDate, lastPensionIncrease, linkedToCorpBonds, salaryProgressions, cashflows,
                    assumptions.ToDictionary(x => x.Type, x => x.InitialValue),
                    pensionIncreases.ToDictionary(x => x.Id, x => x.InitialValue),
                    liabIncreases,
                    bondYield,                    
                    aoci, unrecognisedPriorServiceCost, amortisationPriorServiceCost, (int)amortisationPeriodForActGain.GetValueOrDefault(), simpleAssumptions, giltIndex.Id,
                    assumptions.ToDictionary(x => x.Type),
                    pensionIncreases.ToDictionary(x => x.Id, x => x),
                    giltIndex) { TotalCosts = totalCosts };
                result.Add(ab);
            }
            return result;
        }

        private IDictionary<int, double> GetLiabIncreases()
        {
            var result = new Dictionary<int, double>();

            var mortAdjRange = GetRangeCoords("MortAdj");

            for (var row = mortAdjRange.Row; row <= mortAdjRange.EndRow; row++)
            {
                result.Add((int)GetDouble(row, "K"), GetDouble(row, "L"));
            }
            return result;
        }

        private void GetAccountingParams()
        {
            xls.ActiveSheetByName = "Main";
            AccountingDefaultStandard = new FlightDeck.DomainShared.AccountingStandardHelper().ParseAccountingStandardType(GetString("DefaultStandard"));
            AccountingIAS19Visible = string.Equals(GetString("IAS19Visible"), "Yes", StringComparison.CurrentCultureIgnoreCase);
            AccountingFRS17Visible = string.Equals(GetString("FRS17Visible"), "Yes", StringComparison.CurrentCultureIgnoreCase);
            AccountingUSGAAPVisible = string.Equals(GetString("USGAAPVisible"), "Yes", StringComparison.CurrentCultureIgnoreCase);
            switch (GetString("USGAAPSalaryOption").ToLower())
            {
                case "rpi": ABOSalaryType = FlightDeck.DomainShared.SalaryType.RPI; break;
                case "cpi": ABOSalaryType = FlightDeck.DomainShared.SalaryType.CPI; break;
                case "zero": ABOSalaryType = FlightDeck.DomainShared.SalaryType.Zero; break;
                default:
                    break;
            }
        }

        private void GetAdjustedCashflows()
        {

        }

        private void GetNewJPGiltRates()
        {
            xls.ActiveSheetByName = "NewJP";
            GiltSpotRates = new Dictionary<int, double>();
            var curveTableCoords = GetRangeCoords("CurveTable");
            var year = 1.0;
            while (TryGetDouble(curveTableCoords.Row + (int)year, curveTableCoords.Col, out year))
            {
                GiltSpotRates.Add((int)year, GetDouble(curveTableCoords.Row + (int)year, curveTableCoords.Col + 1));
                year++;
            }
        }

        private void GetNewJPResults()
        {
            var defaultPremiums = new Dictionary<string, BeforeAfter<double>>();

            xls.ActiveSheetByName = "NewJP";

            var range = GetRangeCoords("NewJPBases");

            for (int row = range.Row; row <= range.EndRow; row++)
            {
                var basisName = GetString(row, range.Col);
                if (!string.IsNullOrEmpty(basisName))
                {
                    double d;
                    if (TryGetDouble(row, range.Col + 1, out d)) 
                        if (!defaultPremiums.ContainsKey(basisName))
                            defaultPremiums.Add(basisName, new BeforeAfter<double>(GetDouble(row, range.Col + 1), GetDouble(row, range.Col + 2)));
                }
            }

            var chartData = GetNewJPChartData();

            NewJP = new NewJPData
                (
                    chartData.ToDictionary(x => x.Key, x => x.Value.TechnicalProvision),
                    chartData.ToDictionary(x => x.Key, x => x.Value.SelfSufficiency),
                    chartData.ToDictionary(x => x.Key, x => x.Value.Buyout),
                    chartData.ToDictionary(x => x.Key, x => x.Value.Asset1),
                    chartData.ToDictionary(x => x.Key, x => x.Value.Asset2),
                    getDefaultPremiums(defaultPremiums),
                    new Dictionary<BasisType, int>(),
                    new Dictionary<BasisType, int>(),
                    new List<int>()
                );
        }


        /// <summary>
        /// The NewJP sheet still has values for Basis that aren't necessarility loaded so we need to check the 
        /// basis is actually present before we give it some default premiums as part of the NewJPData
        /// </summary>
        private IDictionary<BasisType, BeforeAfter<double>> getDefaultPremiums(Dictionary<string, BeforeAfter<double>> defaultPremiums)
        {
            var defaultPremiumsByBasisType = new Dictionary<BasisType, BeforeAfter<double>>();

            foreach (var premium in defaultPremiums)
            {
                if (Bases.Any(b => b.DisplayName == premium.Key))
                {
                    defaultPremiumsByBasisType.Add(Bases.First(b => b.DisplayName == premium.Key).Type, premium.Value);
                }
            }

            return defaultPremiumsByBasisType;
        }

        private IDictionary<int, NewJPChartDataItem> GetNewJPChartData()
        {
            xls.ActiveSheetByName = "NewJP";

            var NewJPChartData = new Dictionary<int, NewJPChartDataItem>(50);

            var startCell = GetRangeCoords("NewJPChartA1");
            var col = startCell.Col;
            var row = startCell.Row + 1;

            for (int i = 0; i <= 50; i++ )
            {
                NewJPChartData.Add(i,
                    new NewJPChartDataItem
                        (
                            i,
                            GetDouble(row, col + 1, 0),
                            GetDouble(row, col + 2, 0),
                            GetDouble(row, col + 3, 0),
                            GetDouble(row, col + 4, 0),
                            GetDouble(row, col + 5, 0)
                        ));
                row++;
            }

            return NewJPChartData;
        }

        private void GetGrowthOverPeriodInfo()
        {
            GrowthOverPeriodInfo = new List<KeyValuePair<string, double?>>();

            xls.ActiveSheetByName = "Misc";
            var GrowthOverPeriodCoords = GetRangeCoords("GrowthOverPeriod");
            var i = 0;
            while (xls.GetCellValue(GrowthOverPeriodCoords.Row + i, GrowthOverPeriodCoords.Col) != null)
            {
                GrowthOverPeriodInfo.Add(new KeyValuePair<string, double?>(
                    GetString(GrowthOverPeriodCoords.Row + i, GrowthOverPeriodCoords.Col),
                    xls.GetCellValue(GrowthOverPeriodCoords.Row + i, GrowthOverPeriodCoords.Col + 1).ToString() == "n/a"
                        ? (double?)null
                        : GetDouble(GrowthOverPeriodCoords.Row + i, GrowthOverPeriodCoords.Col + 1)));
                i++;
            }
        }

        public PensionScheme GetPensionScheme()
        {
            var schemeDataSource = new SchemeDataSource(DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);

            var namedPropertyGroups = new Dictionary<NamedPropertyGroupType, NamedPropertyGroup>
            {
                {NamedPropertyGroupType.RecoveryPlan, new NamedPropertyGroup(NamedPropertyGroupType.RecoveryPlan, "RP", 
                    new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>(NamedRecoveryPlanPropertyTypes.MaxLumpSum.ToString(), RecoveryPlanAssumptions.MaxLumpSumPayment.ToString()),
                        new KeyValuePair<string, string>(NamedRecoveryPlanPropertyTypes.AnnualIncs.ToString(), RecoveryPlanAssumptions.Increases.ToString()),
                        new KeyValuePair<string, string>(NamedRecoveryPlanPropertyTypes.Length.ToString(), RecoveryPlanAssumptions.Term.ToString())
                    })}
            };

            xls.ActiveSheetByName = "Main";
            if (RangeExists("IsFunded", "Main"))
            {
                IsFunded = GetBool("IsFunded");
            }
            else
            {
                IsFunded = true;
            }
            var AssetGrowthIndices = GetRangeCoords("AssetGrowthIndices");
            var i = 0;
            var assetsGrowthNamedProperties = new List<KeyValuePair<string, string>>();
            while (xls.GetCellValue(AssetGrowthIndices.Row + i, AssetGrowthIndices.Col) != null)
            {
                assetsGrowthNamedProperties.Add(new KeyValuePair<string, string>(GetString(AssetGrowthIndices.Row + i, AssetGrowthIndices.Col), GetString(AssetGrowthIndices.Row + i, AssetGrowthIndices.Col + 1)));
                i++;
            }
            namedPropertyGroups.Add(NamedPropertyGroupType.AssetGrowth, new NamedPropertyGroup(NamedPropertyGroupType.AssetGrowth, NamedPropertyGroupType.AssetGrowth.DisplayName(), assetsGrowthNamedProperties));

            xls.ActiveSheetByName = "VolatilityAssumptions";
            var giltDurationDefault = (int)GetDouble("DefaultGiltDuration");
            var corpDurationDefault = (int)GetDouble("DefaultCorpDuration");
            var applicationParameters = new PensionSchemeApplicationParameters(0.002, 0.5, 1.2, 0.5, 1.2, giltDurationDefault, corpDurationDefault);
            xls.ActiveSheetByName = "Main";
            var LDIDiscRateIndex = GetString("LDIDiscRateIndex");
            var LDIInflationIndex = GetString("LDIInflationIndex");
            var LDIBasis = new LDICashflowBasis
                (
                    GetString("LDIBasis"),
                    GetBool("LDIOverrideAssumptions", "Main"),
                    string.IsNullOrEmpty(LDIDiscRateIndex) ? null : indices[LDIDiscRateIndex],
                    GetDouble("LDIDiscRatePremium", 0),
                    string.IsNullOrEmpty(LDIInflationIndex) ? null : indices[LDIInflationIndex],
                    GetDouble("LDIInflationGap", 0)
                );
            var SyntheticEquityIncomeIndex = GetString("SyntheticEquityIncomeIndex");
            var SyntheticEquityOutgoIndex = GetString("SyntheticEquityOutgoIndex");
            var SyntheticCreditIncomeIndex = GetString("SyntheticCreditIncomeIndex");
            var SyntheticCreditOutgoIndex = GetString("SyntheticCreditOutgoIndex");
            var syntheticAssets = new SyntheticAssetIndices
                (
                    string.IsNullOrEmpty(SyntheticEquityIncomeIndex) ? null : indices[SyntheticEquityIncomeIndex],
                    string.IsNullOrEmpty(SyntheticEquityOutgoIndex) ? null : indices[SyntheticEquityOutgoIndex],
                    string.IsNullOrEmpty(SyntheticCreditIncomeIndex) ? null : indices[SyntheticCreditIncomeIndex],
                    string.IsNullOrEmpty(SyntheticCreditOutgoIndex) ? null : indices[SyntheticCreditOutgoIndex]
                );

            var indexRepoMock = new Mock<IFinancialIndexRepository>();
            indexRepoMock.Setup(x => x.GetByName(It.IsAny<string>())).Returns((string name) => indices[name]);

            var assetsGrowthProvider = new AssetsGrowthProvider(indexRepoMock.Object);

            var pensionScheme = new PensionScheme(1, Name, "Ref",
                Bases, SchemeAssets,
                ContributionRates, RecoveryPlan, schemeDataSource,
                Volatilities, DefaultVolatilities, VarCorrelations, namedPropertyGroups, null, assetsGrowthProvider, ReturnOnAssets,
                IsGrowthFixed, Ias19ExpenseLoading,
                AccountingDefaultStandard, AccountingIAS19Visible, AccountingFRS17Visible, AccountingUSGAAPVisible,
                ABOSalaryType, applicationParameters, null, ContractingOutContributionRate, LDIBasis, syntheticAssets, IsFunded,
                null);

            return pensionScheme;
        }

        private FinancialAssumption GetAssumption(int id, AssumptionType type)
        {
            var idx = GetRangeCoords("AssumptionsTable").Row + id - 1;
            return new FinancialAssumption(id, 1, GetDouble(idx, "D"), indices[GetString(idx, 5)], type, string.Empty);
        }

        private List<dynamic> GetIncreaseDefinitions()
        {
            xls.ActiveSheetByName = "Main";
            var result = new List<dynamic>();
            const int offset = 40;
            const int labelIdx = 3;
            double d;
            for (var i = offset; i < offset + 6; i++)
            {
                var label = GetString(i, labelIdx);
                result.Add(new
                {
                    Label = label,
                    Index = GetString(i, 4),
                    Minimum = GetDouble(i, "E", 0),
                    Maximum = GetDouble(i, "F", 0),
                    Increase = GetDouble(i, "G", 0),
                    IsFixed = TryGetDouble(i, 7, out d)
                });
            }
            return result;
        }

        private PensionIncreaseAssumption GetPensionIncrease(int id, IList<dynamic> definitions, double volatility)
        {
            var penIncsValuesRow = GetRangeCoords("AssumptionsTable").Row + 6;

            var idx = penIncsValuesRow + id - 1;
            var category = GetString(idx, 3).ToLower() == "fixed"
                ? AssumptionCategory.Fixed
                : AssumptionCategory.BlackScholes;
            var isVisible = GetBool(idx, 9);
            var def = definitions[id - 1];

            InflationType inflationType;
            var inflationindex = def.Index.ToLower();
            if (inflationindex == "rpi")
                inflationType = InflationType.Rpi;
            else if (inflationindex == "cpi")
                inflationType = InflationType.Cpi;
            else inflationType = InflationType.None;

            var index = inflationType == InflationType.None ? null : indices[GetString(idx, 5)];

            return new PensionIncreaseAssumption(id, 1, GetDouble(idx, "D"), index, id, def.Label, def.Minimum,
                def.Maximum, def.Increase, category, volatility, inflationType, def.IsFixed, isVisible);
        }

        private void GetAssumptions(ICollection<FinancialAssumption> assumptions,
            ICollection<PensionIncreaseAssumption> pensionIncreases, double volatility, IList<dynamic> increaseDefinitions)
        {
            assumptions.Add(GetAssumption(1, AssumptionType.PreRetirementDiscountRate));
            assumptions.Add(GetAssumption(2, AssumptionType.PostRetirementDiscountRate));
            assumptions.Add(GetAssumption(3, AssumptionType.PensionDiscountRate));
            assumptions.Add(GetAssumption(4, AssumptionType.SalaryIncrease));
            assumptions.Add(GetAssumption(5, AssumptionType.InflationRetailPriceIndex));
            assumptions.Add(GetAssumption(6, AssumptionType.InflationConsumerPriceIndex));

            for (var i = 1; i < 7; i++)
            {
                pensionIncreases.Add(GetPensionIncrease(i, increaseDefinitions, volatility));
            }
        }

        private static AssetCategory ParseCategory(string category)
        {
            if (category.Equals("growth", StringComparison.InvariantCultureIgnoreCase))
                return AssetCategory.Growth;
            if (category.Equals("matching", StringComparison.InvariantCultureIgnoreCase))
                return AssetCategory.Matched;
            return AssetCategory.Other;
        }

        private AssetReturnSpec GetAssetReturnSpec(AssetClassType type)
        {
            return new AssetReturnSpec(0, 0, 0);
        }

        private void GetSchemeAssets()
        {
            GetInvestment();
            SchemeAssets = new List<SchemeAsset>();
            GetCorrelationMatrix();
            GetVolatilities();

            var defaultGiltDuration = (int)GetDouble("DefaultGiltDuration", "VolatilityAssumptions");
            var defaultCorpDuration = (int)GetDouble("DefaultCorpDuration", "VolatilityAssumptions");

            for (var i = 1; i <= xls.SheetCount; i++)
            {
                var sheet = xls.GetSheetName(i);
                if (!sheet.ToLower().StartsWith("assets"))
                    continue;

                xls.ActiveSheetByName = sheet;

                var assetClassData = GetAssetAllocationDataForAssetSheet(sheet);

                var assetFunds = GetAssetFunds(sheet);

                var assets = assetFunds
                    .GroupBy(x => x.AssetClass.Type)
                    .Select(x => new Asset(0, i, assetClassData[x.Key].Class, assetClassData[x.Key].IncludeInIas, assetClassData[x.Key].DefaultReturn, x));

                SchemeAssets.Add(new SchemeAsset(i, 1, GetDate("EffectiveDate"), GetBool("LDIInForce"), GetString("LDIFund"),
                        GetDouble("InterestHedge", 0), GetDouble("InflationHedge", 0), GetDouble("PropSyntheticEquity", 0), GetDouble("PropSyntheticCredit", 0),
                        (int)GetDouble("GiltDuration", 0), (int)GetDouble("CorpDuration", 0), assets));
            }
        }

        private Dictionary<AssetClassType, Asset> GetAssetAllocationDataForAssetSheet(string sheetName)
        {
            var assetClassData = new Dictionary<AssetClassType, Asset>();

            xls.ActiveSheetByName = sheetName;

            var coords = GetRangeCoords("AssetClassData");

            for (int row = coords.Row; row <= coords.EndRow; row++)
            {
                var name = GetString(row, coords.Col);
                var assetClassType = Utils.MapAssetClassNameToType(name).GetValueOrDefault();
                var assetCategory = ParseCategory(GetString(row, coords.Col + 1));
                var defaultReturn = GetDouble(row, coords.Col + 2);
                var includeInIAS = GetBool(row, coords.Col + 3);
                var amount = GetDouble(row, coords.Col + 4);

                assetClassData.Add(assetClassType, new Asset(0, 0, new AssetClass((int)assetClassType, name, assetCategory, null, 0), includeInIAS, defaultReturn, null));
            }

            return assetClassData;
        }

        private List<AssetFund> GetAssetFunds(string sheetName)
        {
            var assetFunds = new List<AssetFund>();
            xls.ActiveSheetByName = sheetName;
            var coords = GetRangeCoords("AssetAllocation");
            var ldiFund = GetString("LDIFund");

            Func<AssetClassType, AssetClass> getAssetClass = type =>
                {
                    return new AssetClass((int)type, type.ToString(), Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(type).Category, new AssetReturnSpec(0, 0, 0), DefaultVolatilities[type]);
                };

            for (int row = coords.Row; row <= coords.EndRow; row++)
            {
                if (xls.GetCellValue(row, coords.Col + 1) != null)
                {
                    var assetClassType = Utils.MapAssetClassNameToType(GetString(row, coords.Col + 1)).GetValueOrDefault();

                    assetFunds.Add(new AssetFund(row, GetString(row, coords.Col), 0, getAssetClass(assetClassType),
                                    indices[GetString(row, coords.Col + 2)], GetDouble(row, coords.Col + 3), GetString(row, coords.Col).Equals(ldiFund, StringComparison.OrdinalIgnoreCase)));
                }
            }

            return assetFunds;
        }

        private void GetInvestment()
        {
            ClientAssetAssumptions = readAssetAssumptions("L", 15);
            Allocations = readAssetAssumptions("AD", 14);
        }

        private Dictionary<AssetClassType, double> readAssetAssumptions(string col, int row)
        {
            xls.ActiveSheetByName = "Panels";
            var assetAssumptions = new Dictionary<AssetClassType, double>();
            Action<AssetClassType> getData = type =>
            {
                assetAssumptions.Add(type, GetDouble(row++, col));
            };
            getData(AssetClassType.Equity);
            getData(AssetClassType.Property);
            getData(AssetClassType.DiversifiedGrowth);
            getData(AssetClassType.DiversifiedCredit);
            getData(AssetClassType.PrivateMarkets);
            getData(AssetClassType.CorporateBonds);
            getData(AssetClassType.FixedInterestGilts);
            getData(AssetClassType.IndexLinkedGilts);
            getData(AssetClassType.Cash);
            getData(AssetClassType.Abf);

            return assetAssumptions;
        }

        private void GetCorrelationMatrix()
        {
            VarCorrelations = new Dictionary<AssetClassType, IDictionary<AssetClassType, double>>
                {
                    {
                        AssetClassType.Equity, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.35},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts, 0.35},
                            {AssetClassType.IndexLinkedGilts, 0.25},
                            {AssetClassType.Abf, 0.5},
                            {AssetClassType.CorporateBonds, 0.5},
                            {AssetClassType.Equity, 1},
                            {AssetClassType.DiversifiedGrowth, 0.8},
                            {AssetClassType.Property, 0.25},
                        }
                    },
                    {
                        AssetClassType.FixedInterestGilts, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.9},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  1},
                            {AssetClassType.IndexLinkedGilts,   0.75},
                            {AssetClassType.Abf,   0.75},
                            {AssetClassType.CorporateBonds,   0.75},
                            {AssetClassType.Equity,  0.35},
                            {AssetClassType.DiversifiedGrowth,  0.25},
                            {AssetClassType.Property,   0.20}
                        }
                    },
                    {
                        AssetClassType.CorporateBonds, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.75},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.75},
                            {AssetClassType.IndexLinkedGilts,   0.55},
                            {AssetClassType.Abf,   1},
                            {AssetClassType.CorporateBonds,   1},
                            {AssetClassType.Equity,  0.5},
                            {AssetClassType.DiversifiedGrowth,  0.4},
                            {AssetClassType.Property,   0.2},
                        }
                    },
                    {
                        AssetClassType.Property, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.15},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.2},
                            {AssetClassType.IndexLinkedGilts,   0.2},
                            {AssetClassType.Abf,   0.2},
                            {AssetClassType.CorporateBonds,   0.2},
                            {AssetClassType.Equity,  0.25},
                            {AssetClassType.DiversifiedGrowth,  0.2},
                            {AssetClassType.Property,   1},
                        }
                    },
                    {
                        AssetClassType.DiversifiedGrowth, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.15},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.25},
                            {AssetClassType.IndexLinkedGilts,   0.2},
                            {AssetClassType.Abf,   0.4},
                            {AssetClassType.CorporateBonds,   0.4},
                            {AssetClassType.Equity,  0.8},
                            {AssetClassType.DiversifiedGrowth,  1},
                            {AssetClassType.Property,   0.2},
                        }
                    },
                    {
                        AssetClassType.IndexLinkedGilts, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.9},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.75},
                            {AssetClassType.IndexLinkedGilts,   1},
                            {AssetClassType.Abf,   0.55},
                            {AssetClassType.CorporateBonds,   0.55},
                            {AssetClassType.Equity,  0.25},
                            {AssetClassType.DiversifiedGrowth,  0.2},
                            {AssetClassType.Property,   0.2},
                        }
                    },
                    {
                        AssetClassType.Abf, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.75},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.75},
                            {AssetClassType.IndexLinkedGilts,   0.55},
                            {AssetClassType.Abf,   1},
                            {AssetClassType.CorporateBonds,   1},
                            {AssetClassType.Equity,  0.5},
                            {AssetClassType.DiversifiedGrowth,  0.4},
                            {AssetClassType.Property,   0.2},
                        }
                    },
                    {
                        AssetClassType.LiabilityInterest, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 1},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.9},
                            {AssetClassType.IndexLinkedGilts,   0.9},
                            {AssetClassType.Abf,   0.75},
                            {AssetClassType.CorporateBonds,       0.75},
                            {AssetClassType.Equity,  0.35},
                            {AssetClassType.DiversifiedGrowth,  0.15},
                            {AssetClassType.Property,   0.15},
                        }
                    },
                    {
                        AssetClassType.LiabilityLongevity, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0},
                            {AssetClassType.LiabilityLongevity, 1},
                            {AssetClassType.FixedInterestGilts,  0},
                            {AssetClassType.IndexLinkedGilts,   0},
                            {AssetClassType.Abf,   0},
                            {AssetClassType.CorporateBonds,   0},
                            {AssetClassType.Equity,  0},
                            {AssetClassType.DiversifiedGrowth,  0},
                            {AssetClassType.Property,   0},
                        }
                    },
                    {AssetClassType.Cash, null}
                };

        }

        private void GetVolatilities()
        {
            Volatilities = new Dictionary<AssetClassType, double>
                {
                    {AssetClassType.Equity,             0.2},
                    {AssetClassType.FixedInterestGilts, 0.09},
                    {AssetClassType.CorporateBonds,     0.105},
                    {AssetClassType.Property,           0.13},
                    {AssetClassType.DiversifiedGrowth,               0.125},
                    {AssetClassType.Cash,               0.0},
                    {AssetClassType.IndexLinkedGilts,   0.07},
                    {AssetClassType.Abf,                0.105},
                    {AssetClassType.LiabilityInterest, 0.09},
                    {AssetClassType.LiabilityLongevity, 0.035}
                };
        }

        private void GetDefaultVolatilities()
        {
            DefaultVolatilities = new Dictionary<AssetClassType, double>();

            xls.ActiveSheetByName = "VolatilityAssumptions";
            var coords = GetRangeCoords("DefaultVolatilities");
            var r = coords.Row;
            var c = coords.Col;
            DefaultVolatilities.Add(AssetClassType.Equity, GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.Property, GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.DiversifiedGrowth, GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.DiversifiedCredit, GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.PrivateMarkets, GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.CorporateBonds, GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.FixedInterestGilts, GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.IndexLinkedGilts, GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.Cash, GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.Abf, GetDouble(r++, c));
        }

        private string GetChosenBasis()
        {
            xls.ActiveSheetByName = "CALCS";
            return GetString("ChosenBasis");
        }

        private BasisType getBasisTypeFromLookupValue()
        {
            var obj = GetCellValue("ChosenBasisType", "CALCS");
            if (obj != null)
            {
                switch (obj.ToString())
                {
                    case "Technical Provisions":
                        return BasisType.TechnicalProvision;
                    case "Accounting":
                        return BasisType.Accounting;
                    case "Buyout":
                        return BasisType.Buyout;
                    case "Gilts":
                        return BasisType.Gilts;
                    default:
                        break;
                }
            }
            throw new Exception("Could not read basis type from CALCS worksheet");
        }

        #region Cell value readers

        private DateTime GetDate(string namedRange, string sheetName = null)
        {
            double d;
            if (TryGetDouble(namedRange, out d, sheetName))
            {
                return DateTime.FromOADate(d);
            }
            return DateTime.MinValue;
        }

        private DateTime GetDateSlow(int row, string column)
        {
            return DateTime.FromOADate(GetDouble(row, column));
        }

        private DateTime GetDate(int row, string column)
        {
            return DateTime.FromOADate(GetDouble(row, column));
        }

        private DateTime GetDate(int row, int column)
        {
            return DateTime.FromOADate(GetDouble(row, column));
        }

        private DateTime GetDateFormula(int row, string column)
        {
            return DateTime.FromOADate(GetDouble(row, column));
        }

        private double GetDouble(int row, string column, double dflt)
        {
            double? d = GetDouble(row, column, (double?)null);
            return d.GetValueOrDefault(dflt);
        }

        private double GetDouble(string rangeName, double dflt, string sheetName = null)
        {
            double? d = GetDouble(rangeName, (double?)null, sheetName);
            return d.GetValueOrDefault(dflt);
        }

        private double? GetDouble(int row, string column, double? dflt)
        {
            var d = .0;
            if (TryGetDouble(row, column, out d))
            {
                return d;
            }
            return dflt;
        }

        private double GetDouble(int row, int column, double dflt)
        {
            var d = .0;
            if (TryGetDouble(row, column, out d))
            {
                return d;
            }
            return dflt;
        }

        private double? GetDouble(string rangeName, double? dflt, string sheetName = null)
        {
            var d = .0;
            if (TryGetDouble(rangeName, out d, sheetName))
            {
                return d;
            }
            return dflt;
        }

        private double GetDouble(int row, int column)
        {
            var d = .0;
            if (TryGetDouble(row, column, out d))
            {
                return d;
            }
            throw new Exception(string.Format("Could not get double at {0} col {1}, row {2}, file {3}", xls.ActiveSheetByName, column.ToString(), row.ToString(), xls.ActiveFileName));
        }

        private double GetDouble(int row, string column)
        {
            var d = .0;
            if (TryGetDouble(row, column, out d))
            {
                return d;
            }
            throw new Exception(string.Format("Could not get double at {0}!{1}{2}, file {3}", xls.ActiveSheetByName, column, row.ToString(), xls.ActiveFileName));
        }

        private double GetDouble(string rangeName, string sheetName = null)
        {
            var d = .0;
            if (TryGetDouble(rangeName, out d, sheetName))
            {
                return d;
            }
            throw new Exception(string.Format("Could not get double at {0}!{1}, file {2}", xls.ActiveSheetByName, rangeName, xls.ActiveFileName));
        }

        private bool TryGetDouble(string rangeName, out double result, string sheetName = null)
        {
            return TryGetDouble(GetCellValue(rangeName, sheetName ?? xls.ActiveSheetByName), out result);
        }

        private bool TryGetDouble(int row, string column, out double result)
        {
            return TryGetDouble(xls.GetCellValue(row, TrackerReaderUtilities.GetCol(column)), out result);
        }

        private bool TryGetDouble(int row, int column, out double result)
        {
            return TryGetDouble(xls.GetCellValue(row, column), out result);
        }

        private bool TryGetDouble(object val, out double result)
        {
            var ok = false;
            result = .0;
            if (val != null)
            {
                if (val.GetType() == typeof(TFormula))
                {
                    ok = double.TryParse((val as TFormula).Result.ToString(), out result);
                }
                else
                {
                    ok = double.TryParse(val.ToString(), out result);
                }
            }
            return ok;
        }

        private bool TryGetDate(int row, int col, out DateTime d)
        {
            d = DateTime.MinValue;
            double dbl;
            if (xls.GetCellValue(row, col) != null && TryGetDouble(row, col, out dbl))
            {
                try
                {
                    d = DateTime.FromOADate(dbl);
                    return true;
                }
                catch
                {
                }
            }
            return false;
        }

        private bool GetBool(int row, string col)
        {
            return getBoolFromVal(xls.GetCellValue(row, TrackerReaderUtilities.GetCol(col)) as string);
        }

        private bool GetBool(int row, int col)
        {
            return getBoolFromVal(xls.GetCellValue(row, col) as string);
        }

        private bool GetBool(string namedRange, string sheetName = null)
        {
            var istrue = false;
            var val = GetCellValue(namedRange, sheetName ?? xls.ActiveSheetByName);
            if (val != null)
            {
                istrue = getBoolFromVal(val.ToString());
            }
            return istrue;
        }

        private object GetCellValue(string namedRange, string sheetName)
        {
            var currentlyActiveSheetName = xls.ActiveSheetByName;
            xls.ActiveSheetByName = sheetName;
            var cell = xls.GetNamedRange(namedRange, xls.GetSheetIndex(sheetName));
            var value = xls.GetCellValue(cell.Top, cell.Left);
            xls.ActiveSheetByName = currentlyActiveSheetName;
            return value;
        }

        private bool RangeExists(string namedRange, string sheetName)
        {
            return xls.GetNamedRange(namedRange, xls.GetSheetIndex(sheetName)) != null;
        }

        private RangeCoords GetRangeCoords(string namedRange, string sheetName = null)
        {
            var cell = xls.GetNamedRange(namedRange, xls.GetSheetIndex(sheetName ?? xls.ActiveSheetByName));
            if (cell == null)
                throw new Exception(string.Format("NamedRange not found at: '{0}'" + namedRange));
            var range = new RangeCoords(cell.Left, cell.Top, cell.Right, cell.Bottom);
            return range;
        }

        private bool getBoolFromVal(string val)
        {
            var istrue = false;
            istrue = !string.IsNullOrEmpty(val) && (string.Equals(val, "yes", StringComparison.CurrentCultureIgnoreCase) || string.Equals(val, "true", StringComparison.CurrentCultureIgnoreCase));
            return istrue;
        }

        private string GetString(string namedRange)
        {
            var coords = GetRangeCoords(namedRange);

            return GetStringWithDefault(coords.Row, coords.Col);
        }

        private string GetString(int row, int column)
        {
            return xls.GetCellValue(row, column).ToString();
        }

        private string GetStringWithDefault(int row, int column)
        {
            var v = xls.GetCellValue(row, column);
            return v == null ? string.Empty : v.ToString();
        }

        #endregion
    }
}