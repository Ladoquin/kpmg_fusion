﻿using System;
using System.Collections.Generic;
using FlightDeck.DomainShared;
using NUnit.Framework;

namespace FlightDeck.Domain.UnitTests
{
    [TestFixture]
    public class MarketInfoTests
    {
        private MarketInfoProvider infoProvider;

        [SetUp]
        public void SetUp()
        {
            infoProvider = new MarketInfoProvider(new IndexRepositoryStub());
        }

        [Test]
        public void GetMarketInfo()
        {
            var result = infoProvider.GetInfo(new DateTime(2011, 1, 1), new DateTime(2013, 1, 1));
            
            Assert.That(result.Yields.Inflation,        Is.EqualTo(new BeforeAfter<double>(3.504376397, 2.843332871)).Within(Utils.TestPrecision));
            Assert.That(result.Yields.Gilts,            Is.EqualTo(new BeforeAfter<double>(3.7322, 2.4954)).Within(Utils.TestPrecision));
            Assert.That(result.Yields.IndexLinkedGilts, Is.EqualTo(new BeforeAfter<double>(0.5407, -0.06)).Within(Utils.TestPrecision));
            Assert.That(result.Yields.CorporateBonds,   Is.EqualTo(new BeforeAfter<double>(5.3607, 3.5906)).Within(Utils.TestPrecision));

            Assert.That(result.Returns.UkEquity,        Is.EqualTo(8.41801).Within(Utils.TestPrecision));
            Assert.That(result.Returns.GlobalEquity,    Is.EqualTo(6.61971).Within(Utils.TestPrecision));
            Assert.That(result.Returns.Property,        Is.EqualTo(9.63752).Within(Utils.TestPrecision));
            Assert.That(result.Returns.Dgfs,            Is.EqualTo(0.69468).Within(Utils.TestPrecision));
            Assert.That(result.Returns.Gilts,           Is.EqualTo(18.68968).Within(Utils.TestPrecision));
            Assert.That(result.Returns.IndexLinkedGilts,Is.EqualTo(20.69152).Within(Utils.TestPrecision));
            Assert.That(result.Returns.CorporateBonds,  Is.EqualTo(19.78964).Within(Utils.TestPrecision));
        }

        private class IndexRepositoryStub : IFinancialIndexRepository
        {
            private readonly Dictionary<string, FinancialIndex> indices;
            public IndexRepositoryStub()
            {
                indices = new Dictionary<string, FinancialIndex>();
                Action<string, double, double> addIndex
                    = (name, startValue, endValue) => indices.Add(name,
                        new FinancialIndex(1, name, new SortedList<DateTime, double>
                            {
                                {new DateTime(2011, 1, 1), startValue},
                                {new DateTime(2013, 1, 1), endValue}
                            }));

                addIndex("BOE SPOT INFLATION - 20 YEAR MATURITY",                   3.504376397, 2.843332871);
                addIndex("FTSE BRIT GOVT FIXED OVER 15 YEARS - ANNUAL GRY",         3.7322, 2.4954);
                addIndex("FTSE BRIT.GOVT. 0% INFL.OVER 15 YRS - ANNUAL RRY",        0.5407, -0.06);
                addIndex("IBOXX £ NON-GILTS 15+ YEARS AA - RED. YIELD",             5.3607, 3.5906);
                addIndex("FTSE ALL SHARE - TOT RETURN IND",                         4111.9, 4458.04);
                addIndex("MSCI WORLD - TOT RETURN IND",                             2740.105, 2921.492);
                addIndex("FTSE ALL UK PROPERTY (GAV) - TOT RETURN IND",             4597.45, 5040.53);
                addIndex("BLACKROCK LX.BSF EURO DYNAMIC DIVR.GROWTH X2 - TOT RETURN IND", 149.71, 150.75);
                addIndex("FTSE BRIT GOVT FIXED OVER 15 YEARS - TOT RETURN IND",     2452.37, 2910.71);
                addIndex("FTSE BRIT.GOVT.INDEX LINK.OVER 15 YRS - TOT RETURN IND",  2698.69, 3257.09);
                addIndex("IBOXX £ NON-GILTS 15+ YEARS AA - TOT RETURN IND",         204.1447, 244.5442);

                GiltsIndex = indices["FTSE BRIT GOVT FIXED OVER 15 YEARS - ANNUAL GRY"];
            }
     
            public FinancialIndex GetById(int id)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<FinancialIndex> GetAll()
            {
                throw new NotImplementedException();
            }

            public int Insert(FinancialIndex item)
            {
                throw new NotImplementedException();
            }

            public bool Update(FinancialIndex item)
            {
                throw new NotImplementedException();
            }

            public void InsertOrUpdate(FinancialIndex item)
            {
                throw new NotImplementedException();
            }

            public FinancialIndex GetByName(string name)
            {
                return indices[name];
            }

            public IEnumerable<dynamic> GetIndicesByImportId(int importDetailId)
            {
                throw new NotImplementedException();
            }

            public FinancialIndex GiltsIndex { get; private set; }

            public void ClearCache()
            {
                throw new NotImplementedException();
            }

            public int FillEndGap(FinancialIndex item, DateTime EndDate)
            {
                throw new NotImplementedException();
            }
        }
    }
}