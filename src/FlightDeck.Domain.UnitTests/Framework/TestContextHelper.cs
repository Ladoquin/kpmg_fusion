﻿namespace FlightDeck.Domain.UnitTests.Framework
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    class TestContextHelper
    {
        public static IBasesDataManager GetBasesDataManager(TestScenario t)
        {
            return new BasesDataManager(t.Scheme.UnderlyingBases.Where(b => b.DisplayName == t.BasisName));
        }

        public static SchemeContext GetSchemeContext(TestScenario t)
        {
            return new SchemeContext(
                t.Scheme,
                t.Assumptions.SchemeAssumptions.ToSchemeAssumptions(),
                new SchemeResults { RefreshDate = t.Results.RefreshDate, BuyinCost = new BuyinCost(t.Results.InsPenCost, t.Results.InsNonPenCost), LDIEvolutionData = t.Results.LDIEvolutionData },
                new TestBusinessAssumptionsProvider(t)
                );
        }

        public static BasisContext GetBasisContext(TestScenario t, BasisAnalysisResults r = null, IPortfolioManager portfolioManager = null, DateTime? atDate = null, DateTime? startDate = null)
        {
            if (!atDate.HasValue)
                atDate = t.Results.AttributionEnd;

            var bdm = GetBasesDataManager(t);
            var pf = (portfolioManager ?? new PortfolioManager(t.Scheme, bdm)).GetPortfolio(atDate.Value);
            var b = bdm.GetBasis(atDate.Value);
            var ctx = new BasisContext
                (
                startDate.GetValueOrDefault(t.Results.AttributionStart),
                atDate.Value,
                b.EffectiveDate,
                b,
                pf,
                r,
                new BasisEvolutionData
                    (
                    t.Results.LiabilityEvolution.ToDictionary(x => x.Date, x => (LiabilityEvolutionItem)x),
                    t.Results.AssetEvolution.ToDictionary(x => x.Date, x => (AssetEvolutionItem)x),
                    t.Results.LiabilityExperience.ToDictionary(x => x.Key, x => x.Value),
                    t.Results.BuyinExperience.ToDictionary(x => x.Key, x => x.Value),
                    t.Results.AssetExperience.ToDictionary(x => x.Key, x => x.Value)
                    ),
                t.Assumptions.BasisAssumptions.ToBasisAssumptions()
                );

            return ctx;
        }

        public static IPortfolioManager GetPortfolioManager(TestScenario t)
        {
            return new PortfolioManager(t.Scheme, GetBasesDataManager(t));
        }
    }
}
