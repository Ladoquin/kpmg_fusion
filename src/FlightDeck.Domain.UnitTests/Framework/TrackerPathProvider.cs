﻿namespace FlightDeck.Domain.UnitTests.Framework
{
    using System;
    using System.Configuration;
    using System.IO;
    
    class TrackerPathProvider
    {
        public static string GetTrackerDir()
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TrackerInstanceDir"]))
                return ConfigurationManager.AppSettings["TrackerInstanceDir"];
            return Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"..\..\..\..\test_data\"));
        }

        public static string GetTrackerFilePath()
        {
            var name = "Fusion tracker.xlsm";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TrackerInstanceName"]))
                name = ConfigurationManager.AppSettings["TrackerInstanceName"];
            return Path.Combine(GetTrackerDir(), name);
        }
    }
}
