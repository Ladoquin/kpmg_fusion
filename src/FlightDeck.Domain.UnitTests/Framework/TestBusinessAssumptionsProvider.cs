﻿namespace FlightDeck.Domain.UnitTests.Framework
{
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using System.Linq;

    class TestBusinessAssumptionsProvider : IBasesAssumptionsProvider
    {
        readonly TestScenario testScenario;
        readonly IAssumptionIndexEvolutionService assumptionIndexEvolutionService;
        readonly IPensionIncreaseIndexEvolutionService pensionIncreaseIndexEvolutionService;

        public TestBusinessAssumptionsProvider(TestScenario t)
        {
            testScenario = t;
            assumptionIndexEvolutionService = new AssumptionIndexEvolutionService();
            pensionIncreaseIndexEvolutionService = new PensionIncreaseIndexEvolutionService();
        }
        
        public IDictionary<string, IBasisAssumptions> BasisAssumptionsCollection
        {
            get 
            {
                var collection = new Dictionary<string, IBasisAssumptions>();

                foreach(var basisName in testScenario.Scheme.UnderlyingBases.Select(x => x.DisplayName).Distinct())
                {
                    IBasisAssumptions assumptions = null;

                    if (testScenario.BasisName == basisName)
                    {
                        assumptions = new BasisAssumptions
                            (
                                testScenario.Assumptions.BasisAssumptions.LiabilityAssumptions.ToDictionary(x => x.Key, x => x.Value),
                                testScenario.Assumptions.BasisAssumptions.LifeExpectancy,
                                testScenario.Assumptions.BasisAssumptions.PensionIncreases.ToDictionary(x => x.Key, x => x.Value),
                                testScenario.Assumptions.BasisAssumptions.AssetAssumptions.ToDictionary(x => x.Key, x => x.Value),
                                testScenario.Assumptions.BasisAssumptions.RecoveryPlanOptions
                            );
                    }
                    else if (testScenario.Assumptions.StoredUserAssumptions.Any(x => x.Key == basisName))
                    {
                        assumptions = testScenario.Assumptions.StoredUserAssumptions.First(x => x.Key == basisName).Value.ToBasisAssumptions();
                    }
                    else
                    {
                        var b = testScenario.Scheme.UnderlyingBases.Where(x => x.DisplayName == basisName && x.EffectiveDate <= testScenario.AttributionEnd).OrderBy(x => x.EffectiveDate).Last();
                        var liabilityAssumptions = Utils.EnumToArray<AssumptionType>().ToDictionary(x => x, x => assumptionIndexEvolutionService.GetAssumptionIndexValue(x, testScenario.AttributionEnd, b));
                        var pensionIncreases = pensionIncreaseIndexEvolutionService.GetPensionIncreaseIndexValue(testScenario.AttributionEnd, b);

                        assumptions = new BasisAssumptions(liabilityAssumptions, b.LifeExpectancy, pensionIncreases, null, null);
                    }

                    collection.Add(basisName, assumptions);
                }

                return collection;
            }
        }
    }
}
