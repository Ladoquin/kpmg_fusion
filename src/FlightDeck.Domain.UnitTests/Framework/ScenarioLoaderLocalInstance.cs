﻿namespace FlightDeck.Domain.UnitTests.Framework
{
    using FlexCel.XlsAdapter;
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Db;
    using FlightDeck.Tracker.Reader;
    using System.Collections.Generic;

    class ScenarioLoaderLocalInstance
    {
        public List<TestScenario> Get()
        {
            var trackerFilePath = TrackerPathProvider.GetTrackerFilePath();

            var xlsInstance = new XlsFile(trackerFilePath, false);

            var indexReader = new IndexReader();

            indexReader.Initialise(xlsInstance);

            var schemeReader = new TrackerSchemeReader(xlsInstance, indexReader);

            var scheme = schemeReader.ReadScheme();

            var assumptions = new TrackerAssumptionsReader(xlsInstance).Read();

            var results = new TrackerResultsReader(xlsInstance, indexReader).Read();

            return new List<TestScenario>
                {
                    new TestScenario
                        {
                            Indices = indexReader,
                            BasisName = schemeReader.ReadSelectedBasisName(),
                            BasisType = schemeReader.ReadSelectedBasisType(),
                            Results = results,
                            ScenarioId = 1,
                            SchemeName = scheme.Name,
                            SchemeRef = scheme.Reference,
                            Scheme = scheme,
                            Assumptions = assumptions,
                            AttributionStart = schemeReader.ReadAttributionStartDate(),
                            AttributionEnd = schemeReader.ReadAttributionEndDate()
                        }
                };
        }
    }
}
