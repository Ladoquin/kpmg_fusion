﻿namespace FlightDeck.Domain.UnitTests.Framework
{
    public enum TestModeType 
    { 
        UseLocalTrackerInstance, 
        UseTestParametersScenarios 
    }
}
