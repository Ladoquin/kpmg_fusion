﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Domain.UnitTests.Framework
{
    class ScenarioParameterParser
    {

        public IDictionary<string, List<int>> GetParsedScenarios(string scenarioParameter)
        {
            if (scenarioParameter != null)
            {
                // parse the environmental variable
                return parseScenariosParam(scenarioParameter);
            }
            else
                return new Dictionary<string, List<int>>();
        }

        private IDictionary<string, List<int>> parseScenariosParam(string param)
        {
            var scenariosDictionary = new Dictionary<string, List<int>>();
            var schemeSeparator = new string[] { "{|}" };
            var scenarioSeparator = new string[] { "{:}" };
            var scenarioList = param.Split(schemeSeparator, StringSplitOptions.None).ToList();

            Action<string, string> addOrUpdateScenario = (schemeReference, scenarioIds) =>
            {
                var schemeFound = scenariosDictionary.ContainsKey(schemeReference);
                var scenariosList = schemeFound ? scenariosDictionary[schemeReference] : new List<int>();

                int scenarioId;
                if (scenarioIds.Contains('-'))
                {
                    // range of scenarios passed, iterate & add to the list
                    var scenarioRange = getScenarioRange(scenarioIds);
                    if (scenarioRange == null || !scenarioRange.Any())
                        return;
                    scenariosList.AddRange(scenarioRange);
                }
                else if (scenarioIds.Contains(','))
                {
                    // multiple scenarios passed, iterate & add to the list
                    foreach (var i in scenarioIds.Split(','))
                    {
                        if (int.TryParse(i, out scenarioId))
                            scenariosList.Add(scenarioId);
                    }
                }
                else if (int.TryParse(scenarioIds, out scenarioId))
                {
                    // single scenario passed, add to the list
                    scenariosList.Add(scenarioId);
                }
                else
                    return; // don't add anything

                if (!schemeFound)
                    scenariosDictionary.Add(schemeReference, scenariosList);
            };

            foreach (var s in scenarioList.Where(x => x.Contains("{:}")))
            {
                var scenario = s.Split(scenarioSeparator, StringSplitOptions.None);

                if (scenario.Length == 2)
                {
                    var schemeRef = scenario[0].ToString();
                    var scenarioIds = scenario[1].ToString();

                    if (!string.IsNullOrWhiteSpace(schemeRef) && !string.IsNullOrWhiteSpace(scenarioIds))
                    {
                        addOrUpdateScenario(schemeRef, scenarioIds);
                    }
                }
            }

            return scenariosDictionary;
        }

        private List<int> getScenarioRange(string scenarioRange)
        {
            var range = new List<int>();
            if (scenarioRange.Split('-').Length == 2)
            {
                int start;
                int end;
                if (int.TryParse(scenarioRange.Split('-')[0], out start) && int.TryParse(scenarioRange.Split('-')[1], out end))
                    for (var i = start; i <= end; i++)
                        range.Add(i);
            }
            return range;
        }

    }
}
