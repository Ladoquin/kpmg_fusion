﻿namespace FlightDeck.Domain.UnitTests.Framework
{
    using System;
    using FlightDeck.Tracker.Models;
    using System.Collections.Generic;
using FlightDeck.Tracker.Reader;
    
    public class TestScenario : TrackerData
    {
        public IIndexReader Indices { get; set; }
        public PensionScheme Scheme { get; set; }
        public DateTime AttributionStart { get; set; }
        public DateTime AttributionEnd { get; set; }

        public override string ToString()
        {
            return ToString(null);
        }
        public string ToString(string extraInfo)
        {
            return string.Format("{0} [{1}], test case: {2}{3}", Scheme.Name, BasisType.ToString(), ScenarioId.ToString(),
                !string.IsNullOrEmpty(extraInfo) ? " - " + extraInfo : string.Empty);
        }
    }
}
