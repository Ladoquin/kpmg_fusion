﻿namespace FlightDeck.Domain.UnitTests.Framework
{
    using FlexCel.XlsAdapter;
    using FlightDeck.Tracker.Reader;
    using System.Collections.Generic;
    using System.Linq;
    
    public class IndexReader : IIndexReader
    {
        private static IList<FinancialIndex> indices;

        public bool Initialised { get { return indices != null; } }

        public IIndexReader Initialise(XlsFile xls)
        {
            indices = new TrackerIndicesReader(xls).Read();

            return this;
        }

        public FinancialIndex Get(string name)
        {
            if (Initialised)
            {
                return indices.SingleOrDefault(x => x.Name == name);
            }

            return null;
        }
    }
}
