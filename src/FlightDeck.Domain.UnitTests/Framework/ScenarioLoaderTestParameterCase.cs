﻿namespace FlightDeck.Domain.UnitTests.Framework
{
    using FlexCel.XlsAdapter;
    using FlightDeck.Tracker.Models;
    using FlightDeck.Tracker.Reader;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    
    class ScenarioLoaderTestParameterCase
    {

        public List<TestScenario> Get()
        {
            IDictionary<string, List<int>> scenariosParam = null;
            if (Environment.GetEnvironmentVariable("SCENARIOS") != null)
                scenariosParam = new ScenarioParameterParser().GetParsedScenarios(Environment.GetEnvironmentVariable("SCENARIOS").ToString());
                
            if (scenariosParam != null && !scenariosParam.Any())
                throw new Exception("Error: Invalid scenario parameter");
                        
            var testparameters = (scenariosParam != null && scenariosParam.Any()) ? getTestSummaries(scenariosParam) : getTestSummaries();
            
            if (scenariosParam != null && scenariosParam.Any())
            {
                // console output to show summary of scheme names and scenario count
                Console.WriteLine("\nRunning tests against the following schemes:");
                foreach (var testParam in testparameters.GroupBy(x => x.SchemeName))
                {
                    Console.WriteLine(string.Format("Scheme {0} - scenario count: {1}", testParam.Key, testParam.Count()));
                }
                Console.WriteLine("");
            }

            var scenarios = new List<TestScenario>(100);

            var indexReader = new IndexReader();

            var db = new Tracker.Db.TrackerDataRepository();

            foreach (var testsGroupedByScheme in testparameters.GroupBy(x => x.SchemeName))
            {
                //use writer to import scheme in to tracker (until refactor means we can load direct from xml using DataImportService)
                var writer = new FlightDeck.Tracker.Writer.TrackerWriter(TrackerPathProvider.GetTrackerFilePath());

                var xmlSourcePath = Path.Combine(TrackerPathProvider.GetTrackerDir(), "schemes", testsGroupedByScheme.Key + ".xml");

                writer.InitialiseScheme(xmlSourcePath);

                var path = Path.Combine(TrackerPathProvider.GetTrackerDir(), testsGroupedByScheme.Key + "-workingcopy.xlsm");

                writer.Save(path);

                var xlsInstance = new XlsFile(path, false);

                //take opportunity of first tracker load up to grab the indices
                if (!indexReader.Initialised)
                {
                    indexReader.Initialise(xlsInstance);
                }

                //use reader to load the scheme from tracker in to memory
                var schemeReader = new TrackerSchemeReader(xlsInstance, indexReader);

                var scheme = schemeReader.ReadScheme();

                foreach (var testcase in testsGroupedByScheme)
                {
                    //get stored results from database
                    var data = db.Get(testcase.ScenarioId, testcase.SchemeName, testcase.DateCreated);
                    
                    scenarios.Add(new TestScenario
                    {
                        Indices = indexReader,
                        BasisName = data.BasisName,
                        BasisType = data.BasisType,
                        Results = data.Results,
                        ScenarioId = data.ScenarioId,
                        SchemeName = data.SchemeName,
                        SchemeRef = data.SchemeRef,
                        Scheme = scheme,
                        Assumptions = data.Assumptions,
                        AttributionStart = data.Results.AttributionStart,
                        AttributionEnd = data.Results.AttributionEnd
                    });
                }

                writer.Quit();

                File.Delete(path);
            }

            return scenarios;
        }

        private IList<TrackerData> getTestSummaries()
        {
            var db = new Tracker.Db.TrackerDataRepository();

            var summaries = db.GetAllSummaries();

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestCaseSchemeFilter"]))
            {
                var schemeNames = ConfigurationManager.AppSettings["TestCaseSchemeFilter"].Split(',').Select(x => x.Trim());

                summaries = summaries.Where(x => schemeNames.Contains(x.SchemeName));
            }

            return summaries.GroupBy(x => new { x.SchemeName, x.ScenarioId })
                            .Select(g => g.OrderByDescending(p => p.DateCreated).First())
                            .ToList();
        }

        private IList<TrackerData> getTestSummaries(IDictionary<string, List<int>> scenarios)
        {
            var db = new Tracker.Db.TrackerDataRepository();

            var summaries = db.GetSummaries(scenarios);
            
            return summaries.ToList();
        }

    }
}
