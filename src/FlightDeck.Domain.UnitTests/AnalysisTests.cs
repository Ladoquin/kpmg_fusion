﻿namespace FlightDeck.Domain.UnitTests
{
    using FlightDeck.Domain.Analysis;
    using FlightDeck.Domain.JourneyPlan;
    using FlightDeck.Domain.LDI;
    using FlightDeck.Domain.UnitTests.Framework;
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Models;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AnalysisTests : TestsBase
    {
        public AnalysisTests(TestModeType mode)
            : base(mode)
        {
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestUserLiabs(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.UserLiabs;
                var actual = new UserLiabsCalculator().Calculate(TestContextHelper.GetBasisContext(t));

                Assert.AreEqual(expected, actual, t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestOldJourneyPlanBefore(BasisType basisType) // Old Journey Plan = Funding Progression
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.JourneyPlanBefore;
                var actual = new JourneyPlanBeforeService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t));

                //yearly totals
                foreach (var exp in expected)
                    Assert.AreEqual(exp.ToBalanceData(), new BalanceData(actual.YearlyTotals[exp.Year].TotalAssets, actual.YearlyTotals[exp.Year].TotalLiability), t.ToString("year: " + exp.Year.ToString()));
                //jpinvgrowth
                Assert.That(actual.YearlyTotals.Last().Value.Growth, Is.EqualTo(t.Results.JPInvGrowth).Within(Utils.TestPrecision), t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestRecoveryPlan(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (!t.Scheme.Funded)
                    continue;

                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).RecoveryPlanPayments;

                // test existing recovery plan
                var expectedBefore = t.Results.RecoveryPlanBefore.OrderBy(x => x.Key).Select(x => x.Value).ToList();
                var oldRP = actual.Values.Select(x => x.Before).ToList();
                Assert.That(oldRP.Sum(), Is.EqualTo(expectedBefore.Sum()).Within(Utils.TestPrecision), t.ToString("Existing recovery plan total"));
                for (int i = 0; i < expectedBefore.Count; i++)
                    Assert.That(oldRP[i], Is.EqualTo(expectedBefore[i]).Within(Utils.Precision), t.ToString("Existing recovery plan"));

                // test new recovery plan
                var expectedAfter = t.Results.RecoveryPlanAfter.OrderBy(x => x.Key).Select(x => x.Value).ToList();
                var newRP = actual.Values.Select(x => x.After).ToList();
                Assert.That(newRP.Sum(), Is.EqualTo(expectedAfter.Sum()).Within(Utils.TestPrecision), t.ToString("New recovery plan total"));
                for (int i = 0; i < expectedAfter.Count; i++)
                    Assert.That(newRP[i], Is.EqualTo(expectedAfter[i]).Within(Utils.Precision), t.ToString("New recovery plan"));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestOldJourneyPlanAfter(BasisType basisType) // Old Journey Plan = Funding Progression
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.JourneyPlanAfter;
                var expectedLiabDuration = t.Results.JPAfterLiabDuration;
                var ctx = TestContextHelper.GetBasisContext(t);
                ctx.Results = new BasisAnalysisResults
                    {
                        PIEInit = t.Results.PIEInit,
                        RecoveryPlanPaymentRequired = t.Results.RecoveryPlanPaymentRequired,
                        InvestmentStrategy = t.Results.InvestmentStrategyResults //bypass the SyntheticConversion cyclical reference bug in Tracker
                    };

                var actual = new JourneyPlanAfterService(TestContextHelper.GetSchemeContext(t)).Calculate(ctx);

                foreach (var exp in expected.OrderBy(x => x.Year))
                    Assert.AreEqual(exp.ToBalanceData(), new BalanceData(actual.YearlyTotals[exp.Year].TotalAssets, actual.YearlyTotals[exp.Year].TotalLiability), t.ToString("year " + exp.Year.ToString()));

                Assert.That(actual.LiabDuration, Is.EqualTo(expectedLiabDuration).Within(Utils.Precision), t.ToString("JP2LiabDuration"));
            }
        }

#if !DEBUG
        [Ignore("For debugging only (results not pre-calculated because it takes too long to save to db)")]
#endif
        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestJP2Cashflows(BasisType basisType)
        {
            AssertRelevance(basisType);

            Func<IEnumerable<CashflowData>, IOrderedEnumerable<CashflowData>> order = cashflows => 
                cashflows.OrderBy(x => x.MemberStatus).ThenBy(x => x.MinAge).ThenBy(x => x.MaxAge).ThenBy(x => x.RetirementAge).ThenBy(x => x.BenefitType).ThenBy(x => x.PensionIncreaseReference).ThenBy(x => x.RevaluationType).ThenBy(x => x.IsBuyIn);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expectedCashflows = t.Results.AdjustedCashflows;
                var actualCashflows = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).AdjustedCashflow;

                Assert.AreEqual(expectedCashflows.Count(), actualCashflows.Count(), t.ToString("Collection sizes differ"));

                var orderedExpected = order(expectedCashflows).ToList();
                var orderedActuals = order(actualCashflows).ToList();

                for (var i = 0; i < orderedExpected.Count; i++)
                {
                    var expected = orderedExpected[i];
                    var actual = orderedActuals.Skip(i).First();

                    Assert.AreEqual(expected, actual, t.ToString("Cashflow not equal: " + expected.ToString()));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestCashflowLens(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.CashflowsAfter;
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).Cashflow;

                foreach (var exp in expected)
                {
                    Assert.That(actual[exp.Year][MemberStatus.ActivePast], Is.EqualTo(exp.MemberStatuses.Single(ms => ms.Key == MemberStatus.ActivePast).Value).Within(Utils.TestPrecision), t.ToString());
                    Assert.That(actual[exp.Year][MemberStatus.Deferred], Is.EqualTo(exp.MemberStatuses.Single(ms => ms.Key == MemberStatus.Deferred).Value).Within(Utils.TestPrecision), t.ToString());
                    Assert.That(actual[exp.Year][MemberStatus.Pensioner], Is.EqualTo(exp.MemberStatuses.Single(ms => ms.Key == MemberStatus.Pensioner).Value).Within(Utils.TestPrecision), t.ToString());
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestFundingLevelData(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.FundingLevel;
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).FundingLevel;

                Assert.AreEqual(expected, actual, t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestInvestmentStrategy(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.InvestmentStrategyResults;
                var ctx = TestContextHelper.GetBasisContext(t);
                ctx.Results = new BasisAnalysisResults
                    {
                        AssetsAtSED = t.Results.AssetsAtSED,
                        BuyInAtAnalysisDate = t.Results.BuyInAtAnalysisDate,
                        UserPenBuyIn = t.Results.UserLiabs.PensionerBuyin,
                        UserDefBuyIn = t.Results.UserLiabs.DeferredBuyin,
                        FRO = t.Results.FRO,
                        ETV = t.Results.ETV,
                        Insurance = t.Results.BuyIn
                    };
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).InvestmentStrategy;

                Assert.That(actual, Is.EqualTo(expected), t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestHedgeBreakdown(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.HedgeBreakdown.ToHedgeBreakdownData();
                var ctx = TestContextHelper.GetBasisContext(t);
                ctx.Results = new BasisAnalysisResults
                    {
                        FundingLevel = t.Results.FundingLevel,
                        InvestmentStrategy = t.Results.InvestmentStrategyResults,
                        GiltBasisLiabilityData = t.Results.GiltBasisLiability
                    };
                var actual = new HedgeAnalysisCalculator(TestContextHelper.GetSchemeContext(t), ctx).Calculate().Breakdown;
                Assert.That(actual, Is.EqualTo(expected), t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestFRO(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.FRO;
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).FRO;

                Assert.That(actual, Is.EqualTo(expected), t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestEmbeddedFRO(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.EFRO.ToEfroData();              
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).EFRO;

                Assert.That(actual, Is.EqualTo(expected), t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestETV(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.ETV;
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).ETV;

                Assert.That(actual, Is.EqualTo(expected), t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestPIE(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.PIE.ToPieData();
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).PIE;

                Assert.That(actual, Is.EqualTo(expected), t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestFutureBenefits(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (t.Assumptions.SchemeAssumptions.FroType != FROType.Embedded)
                {
                    var expected = t.Results.FutureBenefits;
                    var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).FutureBenefits;

                    if (expected.ServiceCostEstimate == 0)
                        Assert.AreEqual(0, actual.ServiceCostEstimate);
                    else
                        Assert.That(actual, Is.EqualTo(expected), t.ToString());
                }
            }
        }

        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestABF(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.ABF;
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).ABF;

                Assert.That(actual, Is.EqualTo(expected), t.ToString());
            }
        }

        [TestCase(BasisType.Buyout)]
        public void TestInsurance(BasisType basisType)
        {
            AssertRelevance(basisType);
            
            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results;
                var schemeCtx = TestContextHelper.GetSchemeContext(t);
                var actual = new BasisAnalysisService(schemeCtx).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything);

                Assert.That(actual.Insurance, Is.EqualTo(expected.BuyIn), t.ToString());
                Assert.That(schemeCtx.Calculated.BuyinCost.Total, Is.EqualTo(expected.TotalInsurancePremiumPaid).Within(Utils.Precision), t.ToString());
                Assert.That(actual.InsurancePointChangeImpact, Is.EqualTo(expected.InsurancePointIncreaseAmount).Within(Utils.Precision), t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestOutPerformanceMaxReturn(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.OutPerformanceMaxReturn;

                var b = TestContextHelper.GetBasisContext(t);
                var s = TestContextHelper.GetSchemeContext(t);

                b.Results = new BasisAnalysisResults
                {
                    UserPenBuyIn = t.Results.UserLiabs.PensionerBuyin,
                    UserDefBuyIn = t.Results.UserLiabs.DeferredBuyin,
                    InvestmentStrategy = t.Results.InvestmentStrategyResults,
                    Insurance = t.Results.BuyIn
                };

                var actual = new WeightedReturnsCalculator().GetMaxReturn(s, b);
                Assert.That(actual, Is.EqualTo(expected).Within(Utils.Precision), t.ToString("Outperformance return"));
            }
        }
    }
}
