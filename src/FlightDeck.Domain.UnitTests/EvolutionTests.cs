﻿namespace FlightDeck.Domain.UnitTests
{
    using FlightDeck.Domain.Evolution;
    using FlightDeck.Domain.LDI;
    using FlightDeck.Domain.UnitTests.Framework;
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Models;
    using NUnit.Framework;
    using System.Linq;
    
    public class EvolutionTests : TestsBase
    {
        public EvolutionTests(TestModeType mode)
            : base(mode)
        {
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestLDIEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            var candidates = 0;
            var ignoredCandidates = 0;
            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                if (t.Scheme.SchemeAssets.Any(x => x.LDIInForce))
                {
                    candidates++; 
                    
                    var readerData = t.Results.LDIEvolutionData;
                    var expectedLdiEvolution = readerData.ToDictionary(x => x.Date);

                    var ldiBasesDataManager = new BasesDataManager(t.Scheme.UnderlyingBases.Where(b => b.DisplayName == t.Scheme.LDICashflowBasis.BasisName));

                    var liabHedgingData = new LDI.LDIEvolutionService(ldiBasesDataManager, t.Scheme).GetLiabilityHedgingData(t.Scheme.RefreshDate);
                    var actualLdiEvolution = liabHedgingData.ToDictionary(x => x.Date);

                    // fusion works out ldi data from ldi basis anchor date, tracker only starts at selected basis anchor date, so need to adjust fusion results accordingly
                    var anchorDate = t.Scheme.UnderlyingBases.Where(b => b.DisplayName == t.BasisName).Min(b => b.EffectiveDate);
                    if (actualLdiEvolution.Keys.Min() < anchorDate)
                    {
                        while (actualLdiEvolution.Keys.Min() < anchorDate)
                            actualLdiEvolution.Remove(actualLdiEvolution.Keys.Min());
                        actualLdiEvolution[actualLdiEvolution.Keys.Min()] = new TrackerLDIEvolutionItem(actualLdiEvolution.Keys.Min(), 0, 0);
                    }

                    // testing ldi evolution data                    
                    Assert.AreEqual(expectedLdiEvolution.Count, actualLdiEvolution.Count(), t.ToString());
                    foreach (var key in expectedLdiEvolution.Keys)
                    {
                        var debug = "key: " + key.ToString();
                        Assert.AreEqual(expectedLdiEvolution[key].Date, actualLdiEvolution[key].Date, t.ToString(debug));
                        Assert.That(actualLdiEvolution[key].LDIInflationSwap, Is.EqualTo(expectedLdiEvolution[key].LDIInflationSwap).Within(Utils.TestPrecision), t.ToString(debug + " [LDIInflationSwap]"));
                        Assert.That(actualLdiEvolution[key].LDIInterestSwap, Is.EqualTo(expectedLdiEvolution[key].LDIInterestSwap).Within(Utils.TestPrecision), t.ToString(debug + " [LDIInterestSwap]"));
                    }
                }
                else
                    ignoredCandidates++;
            }
            if (ignoredCandidates > 0 && candidates == 0)
                Assert.Ignore("LDI not in force for {0} test candidates", ignoredCandidates);
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestLiabilityAssumptionEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.LiabilityAssumptionEvolution;
                var actual = new LiabilityAssumptionEvolutionService(TestContextHelper.GetBasesDataManager(t)).Calculate(expected.Min(e => e.Date), expected.Max(e => e.Date));

                Assert.AreEqual(expected.Count, actual.Count, t.ToString());
                foreach (var e in expected)
                    foreach (var assumption in e.Assumptions)
                        Assert.That(actual[e.Date][assumption.Key], Is.EqualTo(assumption.Value).Within(Utils.Precision), t.ToString("key: " + e.Date.ToString("dd/MM/yyyy")));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestPensionIncreaseEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.PensionIncreaseEvolution;
                var actual = new PensionIncreaseEvolutionService(TestContextHelper.GetBasesDataManager(t)).Calculate(expected.Min(x => x.Date), expected.Max(x => x.Date));

                Assert.AreEqual(expected.Count, actual.Count, t.ToString());
                foreach (var exp in expected)
                    foreach (var pinc in exp.Pincs)
                        Assert.That(actual[exp.Date][pinc.PincReference], Is.EqualTo(pinc.Value).Within(Utils.Precision), t.ToString("key: " + exp.Date.ToString("dd/MM/yyyy")));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestLiabilityEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.LiabilityEvolution;
                var actual = new LiabilityEvolutionService().Calculate(expected.Min(x => x.Date), expected.Max(x => x.Date), TestContextHelper.GetBasesDataManager(t), t.Results.LiabilityAssumptionEvolution.ToDictionary(), t.Results.PensionIncreaseEvolution.ToDictionary())
                    .ToDictionary(x => x.Date);

                Assert.AreEqual(expected.Count, actual.Count(), t.ToString());
                foreach (var exp in expected)
                    Assert.AreEqual(exp, actual[exp.Date], t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestAssetEvolution(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.AssetEvolution;

                var actual = new AssetEvolutionFactory().GetAssetEvolutionService(TestContextHelper.GetSchemeContext(t).Data).Calculate(expected.Min(x => x.Date), expected.Max(x => x.Date), TestContextHelper.GetPortfolioManager(t), t.Results.LDIEvolutionData.ToDictionary(x => x.Date, x => (LDIEvolutionItem)x))
                                    .ToDictionary(x => x.Date);

                Assert.AreEqual(expected.Count, actual.Count(), t.ToString());
                foreach (var exp in expected)
                    Assert.AreEqual(exp, actual[exp.Date], t.ToString());
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestLiabilityExperience(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.LiabilityExperience;
                var actual = new LiabilityExperienceService().CalculateLiabilityStepChange(TestContextHelper.GetBasesDataManager(t), t.Results.LiabilityEvolution.ToDictionary(x => x.Date, x => (LiabilityEvolutionItem)x))[LiabilityExperienceService.LiabilityExperienceType.Liab];

                Assert.AreEqual(expected.Count, actual.Count, t.ToString());
                foreach (var exp in expected)
                    Assert.That(actual[exp.Key], Is.EqualTo(exp.Value).Within(Utils.TestPrecision), t.ToString("key: " + exp.Key.ToString("dd/MM/yyyy")));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestBuyinExperience(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.BuyinExperience;
                var actual = new LiabilityExperienceService().CalculateLiabilityStepChange(TestContextHelper.GetBasesDataManager(t), t.Results.LiabilityEvolution.ToDictionary(x => x.Date, x => (LiabilityEvolutionItem)x))[LiabilityExperienceService.LiabilityExperienceType.Buyin];

                Assert.AreEqual(expected.Count, actual.Count, t.ToString());
                foreach (var exp in expected)
                    Assert.That(actual[exp.Key], Is.EqualTo(exp.Value).Within(Utils.TestPrecision), t.ToString("key: " + exp.Key.ToString("dd/MM/yyyy")));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestAssetExperience(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.AssetExperience;
                var actual = new AssetExperienceService().CalculateStepChange(TestContextHelper.GetPortfolioManager(t), t.Results.AssetEvolution.ToDictionary(x => x.Date, x => (AssetEvolutionItem)x), t.Results.LDIEvolutionData.ToDictionary(x => x.Date, x => (LDIEvolutionItem)x));

                Assert.AreEqual(expected.Count, actual.Count, t.ToString());
                foreach (var exp in expected)
                    Assert.That(actual[exp.Key], Is.EqualTo(exp.Value).Within(Utils.TestPrecision), t.ToString("key: " + exp.Key.ToString("dd/MM/yyyy")));
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestExpectedDeficit(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.ExpectedDeficit;

                var start = t.Results.ExpectedDeficit.Min(x => x.Date);
                var b = t.Scheme.UnderlyingBases.Where(x => x.DisplayName == t.BasisName).OrderBy(x => x.EffectiveDate).First();
                var schemeAssets = t.Scheme.SchemeAssets.Where(x => x.EffectiveDate == (t.Scheme.SchemeAssets.Min(y => y.EffectiveDate))).First();
                var assets = schemeAssets.Assets.ToDictionary(x => x.Class.Type, x => x.DefaultReturn);
                var actual = new ExpectedDeficitService(t.Scheme).Calculate(
                    start,
                    t.Results.ExpectedDeficit.Max(x => x.Date),
                    b,
                    TestContextHelper.GetPortfolioManager(t).GetPortfolio(start),
                    new BasisEvolutionData
                        (
                        t.Results.LiabilityEvolution.ToDictionary(x => x.Date, x => (LiabilityEvolutionItem)x),
                        t.Results.AssetEvolution.ToDictionary(x => x.Date, x => (AssetEvolutionItem)x),
                        t.Results.LiabilityExperience.ToDictionary(x => x.Key, x => x.Value),
                        t.Results.BuyinExperience.ToDictionary(x => x.Key, x => x.Value),
                        t.Results.AssetExperience.ToDictionary(x => x.Key, x => x.Value)
                        ));

                Assert.AreEqual(expected.Count, actual.Keys.Count, t.ToString());
                foreach (var exp in expected)
                {
                    Assert.That(actual[exp.Date].Assets, Is.EqualTo(exp.Assets).Within(1), t.ToString("key: " + exp.Date.ToString("dd/MM/yyyy")));
                    Assert.That(actual[exp.Date].Liabilities, Is.EqualTo(exp.Liabilities).Within(1), t.ToString("key: " + exp.Date.ToString("dd/MM/yyyy")));
                    Assert.That(actual[exp.Date].FundingLevel, Is.EqualTo(exp.FundingLevel).Within(1), t.ToString("key: " + exp.Date.ToString("dd/MM/yyyy")));
                }
            }
        }

        [TestCase(BasisType.Accounting)]
        [TestCase(BasisType.TechnicalProvision)]
        [TestCase(BasisType.Gilts)]
        [TestCase(BasisType.Buyout)]
        public void TestSurplusAnalysis(BasisType basisType)
        {
            AssertRelevance(basisType);

            foreach (var t in GetTestCandidatesByBasisType(basisType))
            {
                var expected = t.Results.SurplusAnalysisData;
                var actual = new BasisAnalysisService(TestContextHelper.GetSchemeContext(t)).Calculate(TestContextHelper.GetBasisContext(t), BasisAnalysisService.CalculationMode.CalculateEverything).SurplusAnalysis;

                Assert.That(actual, Is.EqualTo(expected), t.ToString());
            }
        }
    }
}
