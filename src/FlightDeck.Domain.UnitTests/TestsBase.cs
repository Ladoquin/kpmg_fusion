﻿namespace FlightDeck.Domain.UnitTests
{
    using FlightDeck.Domain.UnitTests.Framework;
    using FlightDeck.DomainShared;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;

#if DEBUG
    [TestFixture(TestModeType.UseLocalTrackerInstance)]
#else
    [TestFixture(TestModeType.UseTestParametersScenarios)]
#endif
    public class TestsBase
    {
        protected Stopwatch stopwatch;
        protected static List<TestScenario> scenarios;

        public TestsBase(TestModeType mode)
        {
            if (scenarios == null) //allow all subclasses to use same pre-loaded scenarios
            {
                if (mode == TestModeType.UseTestParametersScenarios)
                {
                    scenarios = new ScenarioLoaderTestParameterCase().Get();
                }
                else
                {
                    scenarios = new ScenarioLoaderLocalInstance().Get();
                }
            }
        }

        [SetUp]
        public void Init()
        {
            stopwatch = new Stopwatch();
            stopwatch.Start();
        }

        [TearDown]
        public void Cleanup()
        {
            stopwatch.Stop();

            bool logToConsole = false;
            bool.TryParse(ConfigurationManager.AppSettings["LogTestTimings"], out logToConsole);

            if (logToConsole)
                Console.WriteLine("{0} test, duration: {1} milliseconds", TestContext.CurrentContext.Test.Name, stopwatch.Elapsed.TotalMilliseconds.ToString());
        }

        protected void AssertRelevance(BasisType basisType)
        {
            if (scenarios != null && !scenarios.Any(s => s.BasisType == basisType))
                Assert.Ignore("No test scenario found for basisType " + basisType.ToString());
        }

        protected IEnumerable<TestScenario> GetTestCandidatesByBasisType(BasisType basisType)
        {
            if (scenarios == null)
                return new List<TestScenario>();
            return scenarios.Where(s => s.BasisType == basisType).ToList();
        }
    }
}
