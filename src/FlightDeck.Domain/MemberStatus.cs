﻿namespace FlightDeck.Domain
{
    public enum MemberStatus
    {
        ActivePast = 1,
        ActiveFuture,
        Deferred,
        Pensioner,
    }
}