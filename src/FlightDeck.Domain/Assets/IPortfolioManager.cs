﻿namespace FlightDeck.Domain.Assets
{
    using System;
    using System.Collections.Generic;

    public interface IPortfolioManager
    {
        IEnumerable<DateTime> GetEffectiveDates();
        IPortfolio GetPortfolio(DateTime date);
    }
}
