﻿namespace FlightDeck.Domain.Assets
{
    using FlightDeck.Domain.LDI;
    using FlightDeck.DomainShared;
    using log4net;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Asset portfolio manages the collection of assets for a specific basis and effective date.
    /// </summary>
    /// <remarks>
    /// Mimics Tracker PortfolioClass
    /// </remarks>
    class AssetPortfolio : IPortfolio
    {
        readonly SchemeAsset schemeAsset;
        readonly SyntheticAssetIndices syntheticAssets;
        readonly IList<Contribution> contributionRates;
        readonly IDictionary<DateTime, RecoveryPayment> recoveryPlan;
        readonly IBasesDataManager basesDataManager;
        readonly BasisType basisType;
        readonly IDictionary<int, DateTime> benefitOutgoDates;
        readonly IIndexService indexService;
        
        List<PortfolioAllocation> assets;
        Dictionary<AssetClassType, AssetCategory> classifications;
        Dictionary<AssetClassType, double> defaultReturns;
        Dictionary<AssetClassType, double> defaultProportions;
        Dictionary<AssetClassType, bool> include;
        Lazy<IDictionary<int, double>> _cachedTotalCashflow;
        Lazy<IDictionary<int, double>> _cachedNetCashflow;
        Lazy<IList<SalaryProgression>> _cachedSalaryProgressions;
        IDictionary<int, double> cachedTotalCashflow { get { return _cachedTotalCashflow.Value; }}
        IDictionary<int, double> cachedNetCashflow { get { return _cachedNetCashflow.Value; } }
        IList<SalaryProgression> cachedSalaryProgressions { get { return _cachedSalaryProgressions.Value; } }        
        FinancialIndex ldiIndex;

        public FinancialIndex LevelIndex { get; set; }
        public double MarketValue { get; private set; }
        public DateTime MarketValueAt { get; private set; }
        public SchemeAsset AssetData { get { return schemeAsset; } }

        public SchemeAsset MarketAssets
        {
            get
            {
                return new SchemeAsset(schemeAsset.Id, schemeAsset.PensionSchemeId, schemeAsset.EffectiveDate, schemeAsset.LDIInForce, schemeAsset.LDIFund,
                                       schemeAsset.InterestHedge, schemeAsset.InflationHedge, schemeAsset.PropSyntheticEquity, schemeAsset.PropSyntheticCredit,
                                       schemeAsset.GiltDuration, schemeAsset.CorpDuration, schemeAsset.Assets);
            }
        }

        public bool ABFEnabled
        {
            get
            {
                return basisType != BasisType.Accounting && assets.Any(a => a.Type == AssetClassType.Abf && a.Proportion > 0);
            }
        }

        public AssetPortfolio(SchemeAsset schemeAsset, SyntheticAssetIndices syntheticAssets, IList<Contribution> contributionRates, IDictionary<DateTime, RecoveryPayment> recoveryPlan, IBasesDataManager basesDataManager)
            : this(schemeAsset, syntheticAssets, contributionRates, recoveryPlan, basesDataManager, new IndexService())
        {
        }

        public AssetPortfolio(SchemeAsset schemeAsset, SyntheticAssetIndices syntheticAssets, IList<Contribution> contributionRates, IDictionary<DateTime, RecoveryPayment> recoveryPlan, IBasesDataManager basesDataManager, IIndexService indexService)
        {
            this.schemeAsset = initialiseSchemeAsset(schemeAsset);
            this.syntheticAssets = syntheticAssets;
            this.contributionRates = contributionRates;
            this.recoveryPlan = recoveryPlan;
            this.basesDataManager = basesDataManager;
            this.basisType = basesDataManager.BasisType;
            this.indexService = indexService;

            LoadMarketValue();
            LoadAssets();

            benefitOutgoDates = InitialiseBenefitOutgoDates();
            _cachedSalaryProgressions = new Lazy<IList<SalaryProgression>>(() => InitialiseCachedSalaryProgressions());
            _cachedTotalCashflow = new Lazy<IDictionary<int, double>>(() => InitialiseTotalCashflow());
            _cachedNetCashflow = new Lazy<IDictionary<int, double>>(() => InitialiseNetCashflow());
        }

        private SchemeAsset initialiseSchemeAsset(SchemeAsset data)
        {
            //Take a clone of the asset data
            //Can then manipulate to make sure it contains all editable asset classes
            var assets = new List<Asset>(data.Assets.Select(asset => new Asset
                    (
                        asset.Id, 
                        asset.SchemeAssetId, 
                        asset.Class, 
                        asset.IncludeInIas, 
                        asset.DefaultReturn,
                        new List<AssetFund>(asset.AssetFunds.Select(fund => new AssetFund(
                            fund.Id, 
                            fund.Name, 
                            fund.AssetId, 
                            fund.AssetClass, 
                            fund.Index, 
                            fund.Amount, 
                            fund.IsLDIFund)))
                    )));

            foreach (var assetClass in Utils.EnumToArray<AssetClassType>())
            {
                var attr = Utils.GetEnumAttribute<AssetClassType, AssetClassTypeClientVisibleAttribute>(assetClass);
                if (attr != null && attr.ClientVisible)
                {
                    if (!assets.Any(asset => asset.Class.Type == assetClass))
                        assets.Add(new Asset(0, data.Id, 
                            new AssetClass((int)assetClass, assetClass.DisplayName(), Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(assetClass).Category, AssetDefaults.DefaultAssetReturnSpec, 0), true, AssetDefaults.DefaultAssetReturns[assetClass], null));
                }
            }

            var s = new SchemeAsset
                (
                data.Id,
                data.PensionSchemeId,
                data.EffectiveDate,
                data.LDIInForce,
                data.LDIFund,
                data.InterestHedge,
                data.InflationHedge,
                data.PropSyntheticEquity,
                data.PropSyntheticCredit,
                data.GiltDuration,
                data.CorpDuration,
                assets
                );

            return s;
        }

        private void LoadMarketValue()
        {
            MarketValue = schemeAsset.Value;
            MarketValueAt = schemeAsset.EffectiveDate;
        }

        private void LoadAssets()
        {
            classifications = new Dictionary<AssetClassType, AssetCategory>();
            defaultReturns = new Dictionary<AssetClassType, double>();
            include = new Dictionary<AssetClassType, bool>();

            foreach (var asset in schemeAsset.Assets)
            {
                var type = asset.Class.Type;
                classifications.Add(type, asset.Class.Category);
                defaultReturns.Add(type, asset.DefaultReturn);
                include.Add(type, asset.IncludeInIas);
            }

            var amountExcluded = .0;
            assets = new List<PortfolioAllocation>();

            //Fund Breakdown
            foreach (var fund in schemeAsset.Assets.SelectMany(x => x.AssetFunds))
            {
                var allocation = new PortfolioAllocation
                    {
                        Type = fund.AssetClass.Type,
                        ExpectedReturn = defaultReturns[fund.AssetClass.Type],
                        Index = fund.Index
                    };

                if (basisType == BasisType.Accounting && !include[fund.AssetClass.Type])
                {
                    amountExcluded += fund.Amount;
                }
                else
                {
                    allocation.Proportion = fund.Amount / MarketValue;
                }
                assets.Add(allocation);
            }

            if (amountExcluded > 0)
            {
                foreach (var asset in assets)
                {
                    asset.Proportion /= (1 - amountExcluded / MarketValue);
                }
                MarketValue -= amountExcluded;
            }

            //Summarise Default Mix
            defaultProportions = schemeAsset.Assets.ToDictionary(x => x.Class.Type, x => getProportion(x.Class.Type));

            //Synthetic exposure is expressed as a proportion of assets INCLUDING ABF, where applicable.
            //Hence we must scale up the proportions to keep the £ exposures consistent.
            var propSyntheticEquity = schemeAsset.PropSyntheticEquity / (1 - amountExcluded / (MarketValue + amountExcluded));
            var propSyntheticCredit = schemeAsset.PropSyntheticCredit / (1 - amountExcluded / (MarketValue + amountExcluded));

            //Add Synthetic Assets
            defaultProportions.Add(AssetClassType.SyntheticEquityOutgo, -propSyntheticEquity);
            defaultProportions.Add(AssetClassType.SyntheticEquityIncome, propSyntheticEquity);
            defaultProportions.Add(AssetClassType.SyntheticCreditOutgo, -propSyntheticCredit);
            defaultProportions.Add(AssetClassType.SyntheticCreditIncome, propSyntheticCredit);

            addSyntheticAssetsToPortfolio(ref assets);

            //Liability hedging
            ldiIndex = schemeAsset.Assets.SelectMany(x => x.AssetFunds).Any(x => x.IsLDIFund)
               ? schemeAsset.Assets.SelectMany(x => x.AssetFunds).First(x => x.IsLDIFund).Index
               : null;
        }

        private void addSyntheticAssetsToPortfolio(ref List<PortfolioAllocation> portfolio)
        {
            portfolio.Add(new PortfolioAllocation { Type = AssetClassType.SyntheticEquityIncome, Index = syntheticAssets.EquityIncomeIndex, Proportion = defaultProportions[AssetClassType.SyntheticEquityIncome], ExpectedReturn = defaultReturns[AssetClassType.Equity] });
            portfolio.Add(new PortfolioAllocation { Type = AssetClassType.SyntheticEquityOutgo, Index = syntheticAssets.EquityOutgoIndex, Proportion = defaultProportions[AssetClassType.SyntheticEquityOutgo], ExpectedReturn = defaultReturns[AssetClassType.Cash] });
            portfolio.Add(new PortfolioAllocation { Type = AssetClassType.SyntheticCreditIncome, Index = syntheticAssets.CreditIncomeIndex, Proportion = defaultProportions[AssetClassType.SyntheticCreditIncome], ExpectedReturn = defaultReturns[AssetClassType.CorporateBonds] });
            portfolio.Add(new PortfolioAllocation { Type = AssetClassType.SyntheticCreditOutgo, Index = syntheticAssets.CreditOutgoIndex, Proportion = defaultProportions[AssetClassType.SyntheticCreditOutgo], ExpectedReturn = defaultReturns[AssetClassType.FixedInterestGilts] });
        }

        private IDictionary<int, DateTime> InitialiseBenefitOutgoDates()
        {
            var benefitOutgoStartDate = basesDataManager.EffectiveDates.Where(d => d <= schemeAsset.EffectiveDate).Max();
            var benefitOutgoDates = new Dictionary<int, DateTime>(Utils.MaxYear + 1);
            for (int y = 0; y <= Utils.MaxYear; y++)
                benefitOutgoDates.Add(y, benefitOutgoStartDate.AddYears(y));
            return benefitOutgoDates;
        }

        private IList<SalaryProgression> InitialiseCachedSalaryProgressions()
        {
            IList<SalaryProgression> cachedSalaryProgressions = null;

            foreach (var basisEffectiveDate in basesDataManager.EffectiveDates.OrderByDescending(d => d))
            {
                if (basisEffectiveDate <= schemeAsset.EffectiveDate)
                {
                    cachedSalaryProgressions = basesDataManager.GetBasis(basisEffectiveDate).SalaryProgression ?? new List<SalaryProgression>();
                    break;
                }
            }

            return cachedSalaryProgressions;
        }

        private Dictionary<int, double> InitialiseTotalCashflow()
        {
            var totalCashflow = new Dictionary<int, double>();
            var cashflows = basesDataManager.GetBasis(schemeAsset.EffectiveDate).Cashflows;

            foreach (var c in cashflows)
            {
                if (c.MemberStatus != MemberStatus.ActiveFuture)
                {
                    for (int y = 1; y <= Utils.MaxYear; y++)
                    {
                        if (!totalCashflow.ContainsKey(y))
                            totalCashflow.Add(y, 0);

                        if (!c.IsBuyIn)
                            totalCashflow[y] += c.Cashflow.ContainsKey(y) ? c.Cashflow[y] : 0;
                    }
                }
            }

            return totalCashflow;
        }

        private Dictionary<int, double> InitialiseNetCashflow()
        {
            var netCashflow = new Dictionary<int, double>();
            var cashflows = basesDataManager.GetBasis(schemeAsset.EffectiveDate).Cashflows;

            foreach (var c in cashflows)
            {
                if (c.MemberStatus != MemberStatus.ActiveFuture)
                {
                    for (int y = 1; y <= cashflows.First().MaxAge; y++)
                    {
                        if (!netCashflow.ContainsKey(y))
                            netCashflow.Add(y, 0);

                        var b = c.IsBuyIn ? -1 : 1;

                        netCashflow[y] += c.Cashflow.ContainsKey(y) ? (c.Cashflow[y] * b) : 0;
                    }
                }
            }

            return netCashflow;
        }

        private double GetContributionRate(DateTime at, ContributorType contributor)
        {
            var applicableRates = contributionRates.Where(r => r.Date <= at);

            if (applicableRates.Any())
            {
                var rate = applicableRates.OrderBy(r => r.Date).Last();

                return contributor == ContributorType.Employee ? rate.Employee : rate.Employer;
            }

            return 0;
        }

        private double GetTotalSalary(DateTime at)
        {
            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                if (benefitOutgoDates[y] > at)
                {
                    if (cachedSalaryProgressions.Any())
                        return cachedSalaryProgressions.Sum(prog => prog.SalaryRoll.ContainsKey(y) ? prog.SalaryRoll[y] : 0);
                    else
                        return 0;
                }
            }

            return 0;
        }

        private double GetRecoveryPlanPayment(DateTime at)
        {
            foreach (var payment in recoveryPlan.OrderBy(x => x.Key))
            {
                if (payment.Key == at)
                {
                    return payment.Value.Amount;
                }
            }

            return 0;
        }

        public double GetGrowth(DateTime start, DateTime end)
        {
            var growth = .0;

            foreach (var asset in assets)
            {
                if (asset.Proportion != 0)
                {
                    growth += asset.Proportion * indexService.GetGrowth(asset.Index, start, end);
                }
            }

            return growth;
        }

        public double GetNetIncome(DateTime at)
        {
            var netIncome = GetRecoveryPlanPayment(at) + GetEmployeeContributions(at) + GetEmployerContributions(at) - GetBenefitOutgo(at, ProfitType.Net);

            return netIncome;
        }

        public double GetEmployeeContributions(DateTime at)
        {
            var contribution = GetContributionRate(at, ContributorType.Employee) * GetTotalSalary(at) / 365;

            return contribution;
        }

        public double GetEmployerContributions(DateTime at)
        {
            var contribution = GetContributionRate(at, ContributorType.Employer) * GetTotalSalary(at) / 365;

            return contribution;
        }

        public double GetBenefitOutgo(DateTime at, ProfitType profitType)
        {
            if (benefitOutgoDates.Any(d => d.Value > at))
            {
                var date = benefitOutgoDates.Where(d => d.Value > at).OrderBy(d => d.Key).First();
                var year = date.Key;

                var cashflow = profitType == ProfitType.Gross && cachedTotalCashflow.ContainsKey(year)
                                ? cachedTotalCashflow[year]
                                : (profitType != ProfitType.Gross && cachedNetCashflow.ContainsKey(year) ? cachedNetCashflow[year] : 0);

                var benefitOutgo = cashflow / ((benefitOutgoDates[year] - benefitOutgoDates[year - 1]).TotalDays);

                return benefitOutgo;
            }

            return 0;
        }

        public double GetTotalRecoveryPlanPayment(DateTime start, DateTime end)
        {
            var totalRecoveryPlanPayment = .0;

            if (start <= end && start <= (recoveryPlan.Keys.Any() ? recoveryPlan.Keys.Max() : DateTime.MinValue))
            {
                foreach (var rp in recoveryPlan)
                {
                    if (rp.Key >= start && rp.Key <= end)
                    {
                        totalRecoveryPlanPayment += rp.Value.Amount;
                    }
                }
            }

            return totalRecoveryPlanPayment;
        }

        public void Disinvest(double amount)
        {
            var carProp = assets.Any(x => x.Type == AssetClassType.Abf) ? assets.First(x => x.Type == AssetClassType.Abf).Proportion : 0;
            var carValue = carProp * MarketValue;

            if (carValue == 0)
            {
                MarketValue -= amount;
            }
            else
            {
                var newVal = MarketValue - amount;
                for (int i = 0; i < assets.Count; i++)
                {
                    if (assets[i].Type == AssetClassType.Abf)
                    {
                        assets[i].Proportion = carValue / newVal;
                    }
                    else
                    {
                        var before = assets[i].Proportion * MarketValue;
                        var after = before - amount * assets[i].Proportion / (1 - carProp);
                        assets[i].Proportion = after / newVal;
                    }
                }
                MarketValue = newVal;
            }
        }

        public void SetMarketValue(double amount, DateTime at)
        {
            MarketValue = amount;
            MarketValueAt = at;
        }


        public IPortfolio SetStrategy(IBasisContext ctx, IDictionary<AssetClassType, double> expectedReturns = null, IDictionary<AssetClassType, double> allocations = null, double? syntheticEquityAllocation = null, double? syntheticCreditAllocation = null)
        {
            GroupAssetsByClass();

            if (!allocations.IsNullOrEmpty())
            {
                //Load asset allocations - ie, investment strategy asset mix

                var proportionExcluded = .0;
                foreach (var type in include.Keys)
                {
                    if (allocations.ContainsKey(type))
                    {
                        var proportion = allocations[type];
                        if (basisType == BasisType.Accounting && include[type] == false)
                        {                            
                            proportionExcluded += proportion;
                            proportion = 0;
                        }
                        var idx = assets.IndexOf(assets.Find(x => x.Type == type));
                        assets[idx].Proportion = proportion;
                    }
                }

                var totProp = .0;
                for (int i = 0; i < assets.Count; i++)
                {
                    assets[i].Proportion /= (1 - proportionExcluded);
                    totProp += assets[i].Proportion;
                }

                if (Math.Round(totProp, 12) != 1)
                {
                    if (Math.Round(totProp, 5) != 1)
                    {
                        throw new Exception("Loaded asset allocation total is invalid");
                    }
                    else
                    {
                        LogManager.GetLogger(this.GetType()).ErrorFormat("Loaded asset allocation total is invalid: '{0}'", totProp.ToString());
                    }
                }
            }

            if (!expectedReturns.IsNullOrEmpty())
            {
                //Load expected returns - ie, asset assumptions

                foreach (var type in expectedReturns.Keys)
                {
                    var idx = assets.IndexOf(assets.Find(x => x.Type == type));
                    if (idx > -1)
                        assets[idx].ExpectedReturn = expectedReturns[type];
                }

                assets.Find(x => x.Type == AssetClassType.SyntheticEquityIncome).ExpectedReturn = expectedReturns[AssetClassType.Equity];
                assets.Find(x => x.Type == AssetClassType.SyntheticEquityOutgo).ExpectedReturn = expectedReturns[AssetClassType.Cash];
                assets.Find(x => x.Type == AssetClassType.SyntheticCreditIncome).ExpectedReturn = expectedReturns[AssetClassType.CorporateBonds];
                assets.Find(x => x.Type == AssetClassType.SyntheticCreditOutgo).ExpectedReturn = expectedReturns[AssetClassType.FixedInterestGilts];

                var syntheticConversion = getSyntheticConversion(ctx);

                if (syntheticEquityAllocation.HasValue)
                {
                    var propSynEquity = syntheticEquityAllocation.Value.MathSafeDiv(syntheticConversion);

                    assets.Find(x => x.Type == AssetClassType.SyntheticEquityOutgo).Proportion = -propSynEquity;
                    assets.Find(x => x.Type == AssetClassType.SyntheticEquityIncome).Proportion = propSynEquity;
                }
                if (syntheticCreditAllocation.HasValue)
                {
                    var propSynCredit = syntheticCreditAllocation.Value.MathSafeDiv(syntheticConversion);

                    assets.Find(x => x.Type == AssetClassType.SyntheticCreditOutgo).Proportion = -propSynCredit;
                    assets.Find(x => x.Type == AssetClassType.SyntheticCreditIncome).Proportion = propSynCredit;
                }
            }

            return this;
        }

        private double getSyntheticConversion(IBasisContext ctx)
        {
            var syntheticConversion = 1.0;
            if (ctx.Results != null && ctx.Results.InvestmentStrategy != null)
            {
                var ad = ctx.AnalysisDate;
                var dividend = ctx.EvolutionData.AssetEvolution[ad].AssetValue; //AssetsAtSED-(TrackDefBuyIn+TrackPenBuyIn)
                var divisor = ctx.Results.InvestmentStrategy.TotalAssets; //TotalAssetsAtAnalysisDate
                if (dividend != 0 && divisor != 0)
                    syntheticConversion = dividend / divisor;
            }
            return syntheticConversion;
        }

        public IPortfolio SetNewABF(double value)
        {
            var totProp = .0;
            var oldMV = MarketValue;
            var newMV = oldMV + value;
            for (int i = 0; i < assets.Count; i++)
            {
                assets[i].Proportion *= oldMV / newMV;
                totProp += assets[i].Proportion;
            }

            for (int i = 0; i < assets.Count; i++)
            {
                if (assets[i].Type == AssetClassType.Abf)
                {
                    assets[i].Proportion += (1 - totProp);
                    break;
                }
            }
            MarketValue = newMV;

            return this;
        }

        public double GetOutperformance(bool includeABF = false)
        {
            return GetOutperformance(includeABF, null);
        }

        public double GetOutperformance(bool includeABF = false, IDictionary<AssetClassType, double> clientExpectedReturns = null)
        {
            var num = .0;
            var den = .0;

            var abfProp = assets.Any(x => x.Type == AssetClassType.Abf) ? assets.Single(x => x.Type == AssetClassType.Abf).Proportion : 0;

            for (int i = 0; i < assets.Count; i++)
            {
                if (assets[i].Type != AssetClassType.Abf || includeABF)
                {
                    var attr = Utils.GetEnumAttribute<AssetClassType, AssetClassTypeSyntheticAttribute>(assets[i].Type);
                    var synthetic = attr != null && attr.IsSynthetic;
                    var prop = synthetic && !includeABF ? assets[i].Proportion / (1 - abfProp) : assets[i].Proportion;

                    num += prop * assets[i].ExpectedReturn;
                    den += prop;
                }
            }

            var outperformance = num / den;

            return outperformance;
        }

        public double GetExpectedReturn(double giltYield, IDictionary<AssetClassType, double> clientExpectedReturns = null)
        {
            var growth = giltYield;
            var totProp = .0;
            var returns = clientExpectedReturns ?? defaultReturns;

            foreach (var type in returns.Keys)
            {
                var prop = getProportion(type);
                totProp += prop;
                growth = growth + prop * returns[type];
            }

            return growth;
        }

        private void GroupAssetsByClass()
        {
            assets = schemeAsset.Assets
                .Select(x => new PortfolioAllocation
                    {
                        Type = x.Class.Type,
                        ExpectedReturn = x.DefaultReturn,
                        Proportion = getProportion(x.Class.Type),
                        Index = LevelIndex
                    })
                .ToList();

            addSyntheticAssetsToPortfolio(ref assets);
        }

        private double getProportion(AssetClassType type)
        {
            var prop = .0;

            if (assets.Any(a => a.Type == type))
                prop = assets.Where(a => a.Type == type).Sum(a => a.Proportion);

            return prop;
        }

        public double GetLDICashIndexGrowth(DateTime startDate, DateTime endDate)
        {
            if (ldiIndex != null)
                return (ldiIndex.GetValue(endDate) / ldiIndex.GetValue(startDate)) - 1;
            else
                return 0;
        }

        public double GetAssetValue(AssetClassType assetClassType)
        {
            return getProportion(assetClassType) * MarketValue;
        }
    }
}