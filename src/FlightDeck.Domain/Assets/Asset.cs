﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Domain.Assets
{
    public class Asset
    {
        public int Id { get; private set; }
        public int SchemeAssetId { get; private set; }
        public AssetClass Class { get; private set; }
        public bool IncludeInIas { get; private set; }
        public double DefaultReturn { get; private set; }
        public IEnumerable<AssetFund> AssetFunds { get; private set; }

        public double Value
        {
            get
            {
                return AssetFunds == null
                    ? 0
                    : AssetFunds.Sum(x => x.Amount);
            }
        }

        public Asset(int id, int schemeAssetId, AssetClass assetClass, bool includeInIas, double defaultReturn, IEnumerable<AssetFund> assetFunds)
        {
            Id = id;
            SchemeAssetId = schemeAssetId;
            IncludeInIas = includeInIas;
            Class = assetClass;
            DefaultReturn = defaultReturn;
            AssetFunds = assetFunds == null ? new List<AssetFund>() : assetFunds;
        }
    }
}