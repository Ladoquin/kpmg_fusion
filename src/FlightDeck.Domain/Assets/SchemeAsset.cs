﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Domain.Assets
{
    public class SchemeAsset
    {
        public int Id { get; private set; }
        public int PensionSchemeId { get; set; }
        public DateTime EffectiveDate { get; private set; }
        public bool LDIInForce { get; private set; }
        public string LDIFund { get; set; }
        public double InterestHedge { get; private set; }
        public double InflationHedge { get; private set; }
        public double PropSyntheticEquity { get; private set; }
        public double PropSyntheticCredit { get; private set; }
        public int GiltDuration { get; private set; }
        public int CorpDuration { get; private set; }
        public IEnumerable<Asset> Assets { get; private set; }

        public double Value { 
            get 
            {
                return Assets == null
                    ? 0
                    : Assets.Sum(x => x.Value);
            } 
        }

        public IEnumerable<AssetClassType> SchemeAssetClasses
        {
            get
            {
                return Assets.Select(x => x.Class.Type).Distinct();
            }
        }

        public SchemeAsset(int id, int pensionSchemeId, DateTime effectiveDate, bool ldiInForce, string ldiFund, double interestHedge, double inflationHedge,
            double propSyntheticEquity, double propSyntheticCredit, int giltDuration, int corpDuration, IEnumerable<Asset> assets)
        {
            Id = id;
            PensionSchemeId = pensionSchemeId;
            EffectiveDate = effectiveDate;
            LDIInForce = ldiInForce;
            LDIFund = ldiFund;
            InterestHedge = interestHedge;
            InflationHedge = inflationHedge;
            PropSyntheticEquity = propSyntheticEquity;
            PropSyntheticCredit = propSyntheticCredit;
            GiltDuration = giltDuration;
            CorpDuration = corpDuration;
            Assets = assets == null ? new List<Asset>() : assets;
        }
     }
}
