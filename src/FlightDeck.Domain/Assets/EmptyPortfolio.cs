﻿namespace FlightDeck.Domain.Assets
{
    using System;
    using System.Collections.Generic;
    
    class EmptyPortfolio : IPortfolio
    {
        public SchemeAsset AssetData
        {
            get { return new EmptySchemeAsset(); }
        }

        public SchemeAsset MarketAssets
        {
            get { throw new NotImplementedException(); }
        }

        public double MarketValue
        {
            get { return 0; }
        }

        public double GetGrowth(DateTime start, DateTime end)
        {
            return .0;
        }

        public double GetNetIncome(DateTime at)
        {
            return .0;
        }

        public double GetEmployeeContributions(DateTime at)
        {
            return .0;
        }

        public double GetEmployerContributions(DateTime at)
        {
            return .0;
        }

        public double GetExpectedReturn(double giltYield, IDictionary<DomainShared.AssetClassType, double> clientExpectedReturns = null)
        {
            return .0;
        }

        public double GetBenefitOutgo(DateTime at, ProfitType profitType)
        {
            return .0;
        }

        public double GetTotalRecoveryPlanPayment(DateTime start, DateTime end)
        {
            return .0;
        }

        public double GetOutperformance(bool includeABF = false)
        {
            return .0;
        }

        public double GetOutperformance(bool includeABF = false, IDictionary<DomainShared.AssetClassType, double> clientExpectedReturns = null)
        {
            return .0;
        }

        public double GetLDICashIndexGrowth(DateTime startDate, DateTime endDate)
        {
            return .0;
        }

        public void Disinvest(double amount)
        {            
        }

        public void SetMarketValue(double amount, DateTime at)
        {
        }

        public IPortfolio SetStrategy(IBasisContext ctx, IDictionary<DomainShared.AssetClassType, double> expectedReturns = null, IDictionary<DomainShared.AssetClassType, double> allocations = null, double? syntheticEquityAllocation = null, double? syntheticCreditAllocation = null)
        {
            return this;
        }

        public IPortfolio SetNewABF(double value)
        {
            return this;
        }

        public double GetAssetValue(DomainShared.AssetClassType assetClassType)
        {
            return .0;
        }
    }
}
