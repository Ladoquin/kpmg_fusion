﻿namespace FlightDeck.Domain.Assets
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    public interface IPortfolio
    {
        SchemeAsset AssetData { get; }
        SchemeAsset MarketAssets { get; }
        double MarketValue { get; }
        double GetGrowth(DateTime start, DateTime end);
        double GetNetIncome(DateTime at);
        double GetEmployeeContributions(DateTime at);
        double GetEmployerContributions(DateTime at);
        double GetExpectedReturn(double giltYield, IDictionary<AssetClassType, double> clientExpectedReturns = null);
        double GetBenefitOutgo(DateTime at, ProfitType profitType);        
        double GetTotalRecoveryPlanPayment(DateTime start, DateTime end);
        double GetOutperformance(bool includeABF = false);
        double GetOutperformance(bool includeABF = false, IDictionary<AssetClassType, double> clientExpectedReturns = null);
        double GetLDICashIndexGrowth(DateTime startDate, DateTime endDate);
        void Disinvest(double amount);
        void SetMarketValue(double amount, DateTime at);
        IPortfolio SetStrategy(IBasisContext ctx, IDictionary<AssetClassType, double> expectedReturns = null, IDictionary<AssetClassType, double> allocations = null, double? syntheticEquityAllocation = null, double? syntheticCreditAllocation = null);
        IPortfolio SetNewABF(double value);
        double GetAssetValue(AssetClassType assetClassType);
    }
}
