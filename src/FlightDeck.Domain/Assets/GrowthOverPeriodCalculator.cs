﻿namespace FlightDeck.Domain.Assets
{
    using FlightDeck.Domain.LDI;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class GrowthOverPeriodCalculator
    {
        const string SYN_EQUITY = "Synthetic Equity";
        const string SYN_CREDIT = "Synthetic Credit";

        public IEnumerable<AssetFundGrowth> Calculate(IEnumerable<SchemeAsset> schemeAssets, SyntheticAssetIndices syntheticAssets, DateTime startDate, DateTime endDate)
        { 
            var assetFunds = new List<AssetFundGrowth>(20);

            if (endDate < startDate)
                return assetFunds;

            var effectiveDates = schemeAssets.Select(asset => asset.EffectiveDate).OrderBy(ed => ed).ToList();
            var periodCount = 0;
            var periodEnd = startDate;
            var periodStart = DateTime.MinValue;
            var count = new Dictionary<string, int>();
            var minDate = new Dictionary<string, DateTime>();
            var maxDate = new Dictionary<string, DateTime>();
            do
            {
                periodCount++;
                periodStart = periodEnd;
                periodEnd = effectiveDates.Any(ed => ed > periodStart) ? effectiveDates.Where(ed => ed > periodStart).OrderBy(ed => ed).First() : DateTime.MinValue;
                if (periodEnd == DateTime.MinValue || periodEnd > endDate)
                    periodEnd = endDate;

                var fundsSource = effectiveDates.Contains(periodStart)
                    ? schemeAssets.Single(asset => asset.EffectiveDate == periodStart)
                    : schemeAssets.Where(sa => sa.EffectiveDate < periodStart).OrderBy(sa => sa.EffectiveDate).Last();


                var funds =
                    fundsSource.Assets
                        .SelectMany(asset => asset.AssetFunds)
                        .ToDictionary(x => new AssetFundGrowth(x.AssetClass.Category, x.AssetClass.Type, x.Name), x => x.Index);

                //Not using the portfolio like Tracker is, so add the synthetic indices to the funds manually
                if (syntheticAssets.EquityIncomeIndex != null)
                    funds.Add(new AssetFundGrowth(AssetCategory.None, AssetClassType.SyntheticEquityIncome, AssetClassType.SyntheticEquityIncome.ToString()), syntheticAssets.EquityIncomeIndex);
                if (syntheticAssets.CreditIncomeIndex != null)
                    funds.Add(new AssetFundGrowth(AssetCategory.None, AssetClassType.SyntheticCreditIncome, AssetClassType.SyntheticCreditIncome.ToString()), syntheticAssets.CreditIncomeIndex);
                if (syntheticAssets.EquityOutgoIndex != null)
                    funds.Add(new AssetFundGrowth(AssetCategory.None, AssetClassType.SyntheticEquityOutgo, AssetClassType.SyntheticEquityOutgo.ToString()), syntheticAssets.EquityOutgoIndex);
                if (syntheticAssets.CreditOutgoIndex != null)
                    funds.Add(new AssetFundGrowth(AssetCategory.None, AssetClassType.SyntheticCreditOutgo, AssetClassType.SyntheticCreditOutgo.ToString()), syntheticAssets.CreditOutgoIndex);

                foreach (var fund in funds)
                {
                    var key = fund.Key.Name;
                    var index = fund.Value;
                    if (assetFunds.Any(x => x.Name == key))
                    {
                        var f = assetFunds.Single(x => x.Name == key);
                        if (f.Growth.HasValue)
                        {
                            f.Growth = (1 + f.Growth) * getGrowth(index, periodStart, periodEnd) - 1;
                            count[key] = count[key] + 1;
                        }
                    }
                    else
                    {
                        assetFunds.Add(
                            new AssetFundGrowth(fund.Key.Category, fund.Key.Class, fund.Key.Name)
                                {
                                    Growth = index == null ? (double?)null : getGrowth(index, periodStart, periodEnd) - 1
                                });
                        count.Add(key, 1);
                    }
                }

            }
            while (periodEnd < endDate);

            //group the synthetics
            Func<AssetClassType, double?> getSyntheticGrowth = type => assetFunds.Any(x => x.Class == type) ? assetFunds.Single(x => x.Class == type).Growth : 0;
            assetFunds.Add(new AssetFundGrowth(SYN_EQUITY) { IsSynthetic = true, Growth = getSyntheticGrowth(AssetClassType.SyntheticEquityIncome) - getSyntheticGrowth(AssetClassType.SyntheticEquityOutgo) });
            assetFunds.Add(new AssetFundGrowth(SYN_CREDIT) { IsSynthetic = true, Growth = getSyntheticGrowth(AssetClassType.SyntheticCreditIncome) - getSyntheticGrowth(AssetClassType.SyntheticCreditOutgo) });
            count.Add(SYN_EQUITY, count.ContainsKey(AssetClassType.SyntheticEquityIncome.ToString()) ? count[AssetClassType.SyntheticEquityIncome.ToString()] : 0);
            count.Add(SYN_CREDIT, count.ContainsKey(AssetClassType.SyntheticCreditIncome.ToString()) ? count[AssetClassType.SyntheticCreditIncome.ToString()] : 0);

            foreach (var syntheticType in new List<AssetClassType> { AssetClassType.SyntheticEquityIncome, AssetClassType.SyntheticCreditIncome, AssetClassType.SyntheticEquityOutgo, AssetClassType.SyntheticCreditOutgo })
            {
                if (assetFunds.Any(x => x.Class == syntheticType))
                    assetFunds.RemoveAll(x => x.Class == syntheticType);
                if (count.ContainsKey(syntheticType.ToString()))
                    count.Remove(syntheticType.ToString());
            }

            //remove the growth for all funds that were not present for every asset (effective date) period included in the range
            for(int i = 0; i < assetFunds.Count; i++)
                if (count[assetFunds[i].Name] < periodCount)
                    assetFunds[i].Growth = null;

            return assetFunds;
        }

        private double getGrowth(FinancialIndex index, DateTime startDate, DateTime endDate)
        {
            if (startDate < index.BeginDate || endDate > index.EndDate || startDate > endDate)
                return 0;
            else if (startDate == endDate)
                return 1;
            
            return index.GetValue(endDate.SubtractToDate(index.BeginDate)) / index.GetValue(startDate.SubtractToDate(index.BeginDate));
        }
    }
}
