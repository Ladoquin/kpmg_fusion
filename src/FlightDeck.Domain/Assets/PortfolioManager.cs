﻿namespace FlightDeck.Domain.Assets
{
    using FlightDeck.Domain.LDI;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class PortfolioManager : IPortfolioManager
    {
        private readonly bool funded;
        private readonly Dictionary<DateTime, IPortfolio> portfolios;

        public PortfolioManager(ISchemeData scheme, IBasesDataManager basesDataManager)
            : this(scheme.SchemeAssets, scheme.SyntheticAssets, scheme.ContributionRates, scheme.Funded, scheme.RecoveryPlan, basesDataManager)
        {            
        }
        public PortfolioManager(IEnumerable<SchemeAsset> schemeAssets, SyntheticAssetIndices syntheticAssets, IList<Contribution> contributionRates, bool funded, IDictionary<DateTime, RecoveryPayment> recoveryPlan, IBasesDataManager basesDataManager)
        {
            this.funded = funded;

            portfolios = new Dictionary<DateTime, IPortfolio>(10);

            if (funded)
            {
                foreach (var portfolio in schemeAssets.GroupBy(asset => asset.EffectiveDate))
                {
                    var effectiveDate = portfolio.Key;

                    if (effectiveDate >= basesDataManager.AnchorDate)
                    {
                        var assets = schemeAssets.Where(asset => asset.EffectiveDate == effectiveDate).First();

                        portfolios.Add(portfolio.Key, new AssetPortfolio(assets, syntheticAssets, contributionRates, recoveryPlan, basesDataManager));
                    }
                }
            }
        }

        public IEnumerable<DateTime> GetEffectiveDates()
        {
            return funded ? portfolios.Keys.ToList() : new List<DateTime>();
        }

        public IPortfolio GetPortfolio(DateTime date)
        {
            if (!funded)
                return new EmptyPortfolio();

            if (date < portfolios.Keys.Min())
                return null;

            return portfolios
                .Where(portfolio => portfolio.Key <= date)
                .OrderBy(portfolio => portfolio.Key)
                .Last()
                .Value;
        }
    }
}
