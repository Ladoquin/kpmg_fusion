﻿namespace FlightDeck.Domain.Assets
{
    using FlightDeck.DomainShared;
    
    class PortfolioAllocation
    {
        public AssetClassType Type { get; set; }
        public double Proportion { get; set; }
        public double ExpectedReturn { get; set; }
        public FinancialIndex Index { get; set; }
    }
}
