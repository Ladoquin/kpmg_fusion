﻿namespace FlightDeck.Domain.Assets
{
    using System;

    struct ExpectedPortfolioGrowth
    {
        public double Growth { get; set; }
        public double LumpSum { get; set; }
        public int Term { get; set; }
        public double Incs { get; set; }
        public double Amount { get; set; }
        public DateTime Start { get; set; }
        public int StartMonth { get; set; }
        public int StartYear { get; set; }
        public int EndYear { get; set; }
        public double DiscountAdd { get; set; }
        public double PartYear { get; set; }
    }
}
