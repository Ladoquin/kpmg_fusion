﻿namespace FlightDeck.Domain.Assets
{
    using System;
    using System.Collections.Generic;
    
    class EmptySchemeAsset : SchemeAsset
    {
        public EmptySchemeAsset()
            : base(0, 0, DateTime.MinValue, false, string.Empty, .0, .0, .0, .0, 0, 0, new List<Asset>())
        {
        }
    }
}
