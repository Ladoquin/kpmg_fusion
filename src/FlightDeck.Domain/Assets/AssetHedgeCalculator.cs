﻿namespace FlightDeck.Domain.Assets
{
    using FlightDeck.Domain.Evolution;
    using FlightDeck.Domain.LDI;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AssetHedgeCalculator
    {
        private readonly ISchemeContext scheme;

        public AssetHedgeCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public HedgeBreakdownData CalculateExistingHedge(DateTime at, Basis activeBasis, IPortfolioManager portfolioManager)
        {
            if (!scheme.Data.Funded)
                return null;

            if (scheme.Data.LDICashflowBasis == null || string.IsNullOrEmpty(scheme.Data.LDICashflowBasis.BasisName))
                throw new Exception("The LDI basis does not exist");

            if (!scheme.Data.SchemeAssets.Any(x => x.EffectiveDate <= at))
                throw new Exception(string .Format("Assets not found for date '{0}'", at.ToString("dd/MM/yyyy")));

            var effectiveDate = scheme.Data.SchemeAssets.Select(x => x.EffectiveDate).Where(ed => ed <= at).OrderBy(ed => ed).Last();

            if (effectiveDate < scheme.Data.LDIBasesDataManager.AnchorDate)
            {
                //throw new Exception(string.Format("The LDI basis does not exist on '{0}'", at.ToString("dd/MM/yyyy")));
                return new HedgeBreakdownData
                {
                    CurrentBasis = new Dictionary<HedgeType, HedgeBreakdownItem>
                        {
                            { HedgeType.Interest, new HedgeBreakdownItem(0, 0, 0, 0) },
                            { HedgeType.Inflation, new HedgeBreakdownItem(0, 0, 0, 0) }
                        },
                    Cashflows = new Dictionary<HedgeType, HedgeBreakdownItem>
                        {
                            { HedgeType.Interest, new HedgeBreakdownItem(0, 0, 0, 0) },
                            { HedgeType.Inflation, new HedgeBreakdownItem(0, 0, 0, 0) }
                        }
                };
            }

            var ldiProvider = scheme.Data.GetLDIBasisProvider(at);
            var ldiBasis = ldiProvider.LDIBasis.Clone();
            var ldiOverrideAssumptions = scheme.Data.LDICashflowBasis.OverrideAssumptions;

            ldiBasis = ldiBasis.Adjust(
                pensionIncreases: new PensionIncreaseIndexEvolutionService().GetPensionIncreaseIndexValue(ldiBasis.EffectiveDate, ldiBasis)
                );            

            var giltLcf = new LiabilityCashflowStatefulService(ldiBasis, null, at, new BasisLiabilityAssumptions(ldiBasis.Assumptions, ldiBasis.LifeExpectancy, ldiBasis.PensionIncreases));

            if (giltLcf.Basis.EffectiveDate < at && ldiOverrideAssumptions)
                giltLcf.AdjustBase(giltLcf.Basis.Clone(null, null, null, null, at));

            var liabAssumptions = new AssumptionIndexEvolutionService().GetAllAssumptionIndexValues(at, giltLcf.Basis);
            var giltAssumptions = new BasisLiabilityAssumptions(
                                        liabAssumptions,
                                        giltLcf.Basis.LifeExpectancy,
                                        new PensionIncreasesTrackerService().calculatePensionIncreases(at, giltLcf.Basis.EffectiveDate, giltLcf.Basis.UnderlyingPensionIncreaseIndices, giltLcf.Basis.UnderlyingAssumptionIndices, liabAssumptions, null, false)
                                        );

            if (scheme.Data.LDICashflowBasis.OverrideAssumptions)
            {
                giltLcf.ConvertToLDI(scheme.Data, effectiveDate, ldiProvider.DiscRate, ldiProvider.InflationRate);
                giltAssumptions = new BasisLiabilityAssumptions
                    (
                        new AssumptionIndexEvolutionService().GetAllAssumptionIndexValues(at, giltLcf.Basis),
                        giltLcf.Basis.LifeExpectancy,
                        new PensionIncreasesTrackerService().CalculateVariablePensionIncreases(at, at, giltLcf.Basis.UnderlyingPensionIncreaseIndices, giltLcf.Basis.PensionIncreases, giltLcf.Basis.Assumptions[AssumptionType.InflationRetailPriceIndex], giltLcf.Basis.Assumptions[AssumptionType.InflationConsumerPriceIndex])
                    );
            }

            
            var giltConv = .0;
            if (activeBasis == null || activeBasis.EffectiveDate > at)
            {
                giltConv = 1;
            }
            else
            {
                var giltNPV = giltLcf.AdjustTo(at, giltAssumptions);

                var currAssumptions = new BasisLiabilityAssumptions
                    (
                        new AssumptionIndexEvolutionService().GetAllAssumptionIndexValues(at, activeBasis),
                        activeBasis.LifeExpectancy, 
                        new PensionIncreaseIndexEvolutionService().GetPensionIncreaseIndexValue(at, activeBasis)
                    );

                var currBasis = activeBasis.Clone();

                currBasis = currBasis.Adjust(
                    pensionIncreases: new PensionIncreaseIndexEvolutionService().GetPensionIncreaseIndexValue(activeBasis.EffectiveDate, activeBasis));

                var currLcf = new LiabilityCashflowStatefulService(currBasis, null, at, currAssumptions);

                var currNPV = currLcf.AdjustTo(at, currAssumptions);
                
                giltConv = giltNPV.Total / currNPV.Total;
            }

            giltLcf.CalculatePV01AndIE01(at, giltAssumptions, false);
            var giltLiabPV01 = giltLcf.GetPV01(false);
            var giltLiabIE01 = giltLcf.GetIE01(false);
            var buyinPV01 = giltLcf.GetPV01(true);
            var buyinIE01 = giltLcf.GetIE01(true);

            var pf = portfolioManager.GetPortfolio(at);

            var hc = new HedgeEvolutionCalculator(pf, giltLiabPV01, giltLiabIE01, buyinPV01, buyinIE01, giltConv);
            var hedgeBreakdown = hc.Calculate().Breakdown;

            return hedgeBreakdown;
        }
    }
}
