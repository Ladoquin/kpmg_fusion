﻿using FlightDeck.DomainShared;
using System;

namespace FlightDeck.Domain.Assets
{
    public class AssetFund
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int AssetId { get; private set; }
        public AssetClass AssetClass { get; private set; }
        public FinancialIndex Index { get; private set; }
        public double Amount { get; private set; }
        public bool IsLDIFund { get; set; }

        public AssetFund(int id, string assetFundName, int assetId, AssetClass assetClass, FinancialIndex index, double amount, bool isLDIFund)
        {
            Id = id;
            Name = assetFundName;
            AssetId = assetId;
            AssetClass = assetClass;
            Index = index;
            Amount = amount;
            IsLDIFund = isLDIFund;
        }
    }
}
