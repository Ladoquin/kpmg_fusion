﻿using System;
using FlightDeck.DomainShared;
using System.Collections.Generic;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.Domain
{
    public class MarketInfoProvider
    {
        private readonly FinancialIndex inflationRpi20YieldIndex;
        private readonly FinancialIndex gilts15YieldIndex;
        private readonly FinancialIndex indexLinkedGilts15YieldIndex;
        private readonly FinancialIndex corporateBondsAa15YieldIndex;

        private readonly FinancialIndex ukEquityReturnIndex;
        private readonly FinancialIndex globalEquityReturnIndex;
        private readonly FinancialIndex propertyUkReturnIndex;
        private readonly FinancialIndex dgfsReturnIndex;
        private readonly FinancialIndex gilts15ReturnIndex;
        private readonly FinancialIndex indexLinketGilts15ReturnIndex;
        private readonly FinancialIndex corporateBondsAa15ReturnIndex;

        private IFinancialIndexRepository indexRepo;

        public MarketInfoProvider(IFinancialIndexRepository indexRepository)
        {
            indexRepo = indexRepository;

            inflationRpi20YieldIndex = indexRepository.GetByName("BOE SPOT INFLATION - 20 YEAR MATURITY");
            gilts15YieldIndex = indexRepository.GiltsIndex;
            indexLinkedGilts15YieldIndex = indexRepository.GetByName("FTSE BRIT.GOVT. 0% INFL.OVER 15 YRS - ANNUAL RRY");
            corporateBondsAa15YieldIndex = indexRepository.GetByName("IBOXX £ NON-GILTS 15+ YEARS AA - RED. YIELD");

            ukEquityReturnIndex = indexRepository.GetByName("FTSE ALL SHARE - TOT RETURN IND");
            globalEquityReturnIndex = indexRepository.GetByName("MSCI WORLD - TOT RETURN IND");
            propertyUkReturnIndex = indexRepository.GetByName("UK IPD - TOT RETURN IND");
            dgfsReturnIndex = indexRepository.GetByName("KPMG Default DGF");
            gilts15ReturnIndex = indexRepository.GetByName("FTSE BRIT GOVT FIXED OVER 15 YEARS - TOT RETURN IND");
            indexLinketGilts15ReturnIndex = indexRepository.GetByName("FTSE BRIT.GOVT.INDEX LINK.OVER 15 YRS - TOT RETURN IND");
            corporateBondsAa15ReturnIndex = indexRepository.GetByName("IBOXX £ NON-GILTS 15+ YEARS AA - TOT RETURN IND");
        }

        public MarketInfo GetInfo(DateTime attributionStart, DateTime attributionEnd, IList<KeyValuePair<string, string>> marketDataIndexItems)
        {
            Func<FinancialIndex, BeforeAfter<double>> getYield = index => new BeforeAfter<double>(
                index.GetValue(attributionStart),
                index.GetValue(attributionEnd));

            var yields = new MarketInfo.MarketYields(
                getYield(inflationRpi20YieldIndex),
                getYield(gilts15YieldIndex),
                getYield(indexLinkedGilts15YieldIndex),
                getYield(corporateBondsAa15YieldIndex));

            Func<FinancialIndex, double> getReturns =
                index => (index.GetValue(attributionEnd) / index.GetValue(attributionStart) - 1) * 100;
            
            var returns = new MarketInfo.AssetReturns(
                getReturns(ukEquityReturnIndex),
                getReturns(globalEquityReturnIndex),
                getReturns(propertyUkReturnIndex),
                getReturns(dgfsReturnIndex),
                getReturns(gilts15ReturnIndex),
                getReturns(indexLinketGilts15ReturnIndex),
                getReturns(corporateBondsAa15ReturnIndex));

            var marketDataIndices = new List<MarketInfo.MarketDataIndex>();

            if (marketDataIndexItems != null && indexRepo != null)
            {
                foreach (var marketDataIndex in marketDataIndexItems)
                {
                    var index = indexRepo.GetByName(marketDataIndex.Value);

                    if (index != null)
                    {
                        marketDataIndices.Add(new MarketInfo.MarketDataIndex { 
                            Label = marketDataIndex.Key,
                            IndexName = marketDataIndex.Value,
                            IndexValue = getYield(index)
                        });
                    }
                }
            }

            return new MarketInfo(yields, returns, marketDataIndices);
        }
    }
}