﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Domain
{
    public class FinancialIndex
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateTime BeginDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public bool Custom { get; private set; }
        
        public readonly IDictionary<DateTime, double> RawValues;

        public FinancialIndex(int id, string name, SortedList<DateTime, double> values, bool custom = false)
        {
            Name = name;
            RawValues = InterpolateIndexValues(values);
            Id = id;
            BeginDate = RawValues.Count > 0 ? values.Keys[0] : DateTime.MinValue;
            EndDate = RawValues.Count > 0 ? values.Keys[values.Count - 1] : DateTime.MinValue;
            Custom = custom;
        }

        public double GetValue(DateTime date)
        {
            return RawValues[date];
        }

        public bool ContainsValue(DateTime date)
        {
            return RawValues.ContainsKey(date);
        }

        private IDictionary<DateTime, double> InterpolateIndexValues(SortedList<DateTime, double> indexValues)
        {
            var result = new Dictionary<DateTime, double>();
            if (indexValues == null || indexValues.Count == 0)
                return result;
            var i = 0;

            foreach (var indexValue in indexValues)
            {
                result.Add(indexValue.Key, indexValue.Value);
                if (i < indexValues.Count - 1) // && indexValues.Keys[i + 1] > indexValue.Key.AddDays(1))
                {
                    var diff = (indexValues.Keys[i + 1] - indexValue.Key).Days;
                    for (var k = 1; k < diff; k++)
                        result.Add(indexValue.Key.AddDays(k), indexValue.Value);
                }
                i++;
            }
            return result;
        }
    }
}
