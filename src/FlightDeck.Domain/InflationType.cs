﻿namespace FlightDeck.Domain
{
    public enum InflationType
    {
        None = 0,
        Rpi,
        Cpi
    }
}