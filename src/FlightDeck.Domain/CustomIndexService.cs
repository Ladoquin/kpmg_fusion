﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Domain
{
    public class CustomIndexService : ICustomIndexService
    {
        //maps to CreateSchemeSpecificIndex in Tracker
        public Dictionary<DateTime, double> CreateCustomIndex(bool isTotalReturn, Dictionary<FinancialIndex, double> components, double rounding)
        {
            double DailyGrowth; //daily growth in a total return index (only used when IsTotalReturn = True)
            bool roundIt = rounding > 0 ? true : false; //True if rounding is required, otherwise False

            var indicesYearCount = components.ElementAt(0).Key.RawValues.Count;
            var sourceValues = new double[indicesYearCount];
            var values = new double[indicesYearCount];

            //Build weighted values
            var customIndices = new Dictionary<DateTime, double>();
            foreach (var component in components)
            {
                sourceValues = component.Key.RawValues.Values.ToArray();

                //1) Populate the daily index values. For total return indices we start with weighted daily growth.
                if (isTotalReturn)
                {
                    for (int i = 1; i < sourceValues.Length; i++)
                    {
                        if (sourceValues[i] > 0 && sourceValues[i - 1] > 0)
                        {
                            DailyGrowth = sourceValues[i] / sourceValues[i - 1] - 1;
                            values[i] = values[i] + component.Value * DailyGrowth;
                        }
                    }

                    for (int i = 0; i < values.Length; i++)
                    {
                        var year = component.Key.RawValues.Keys.ElementAt(i);
                        if (!customIndices.ContainsKey(year))
                        {
                            customIndices.Add(year, values[i]);
                        }
                        else
                        {
                            customIndices[year] = values[i];
                        }
                    }
                }
                else
                {
                    foreach (var index in component.Key.RawValues)
                    {
                        if (!customIndices.ContainsKey(index.Key))
                        {
                            customIndices.Add(index.Key, component.Value * index.Value);
                        }
                        else
                        {
                            customIndices[index.Key] = customIndices[index.Key] + component.Value * index.Value;
                        }
                    }
                }
            }

            //2) For total return indices, convert weighted growth to a total return index
            if (isTotalReturn)
            {
                double previousIndexValue = 1000;
                for (int i = 0; i < customIndices.Keys.ToArray().Length; i++)
                {
                    var indexDate = customIndices.Keys.ToArray()[i];
                    customIndices[indexDate] = previousIndexValue * (1 + customIndices[indexDate]);
                    previousIndexValue = customIndices[indexDate];
                }
            }

            //3) Rounding if applicable
            for (int i = 0; i < customIndices.Keys.ToArray().Length; i++)
            {
                if (roundIt)
                {
                    var indexDate = customIndices.Keys.ToArray()[i];
                    customIndices[indexDate] = Math.Sign(customIndices[indexDate]) * Math.Floor(Math.Abs(customIndices[indexDate]) / rounding + 0.5) * rounding;
                }
            }

            return customIndices;
        }
    }
}
