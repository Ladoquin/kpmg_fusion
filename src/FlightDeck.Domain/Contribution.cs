﻿using System;

namespace FlightDeck.Domain
{
    public class Contribution
    {
        public int PensionSchemeId { get; private set; }
        public DateTime Date { get; private set; }
        public double Employer { get; set; }
        public double Employee { get; set; }
        public double Combined { get { return Employer + Employee; }}

        public Contribution(int pensionSchemeId, DateTime date, double employer, double employee)
        {
            PensionSchemeId = pensionSchemeId;
            Date = date;
            Employee = employee;
            Employer = employer;
        }
    }
}