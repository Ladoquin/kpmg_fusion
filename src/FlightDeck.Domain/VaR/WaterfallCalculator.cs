﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.Domain.LDI;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    
    class WaterfallCalculator
    {
        private readonly ISchemeContext scheme;
        private readonly IBasisContext ctx;
        private readonly IVaRCalculator VaR;
        private readonly ICorrelationMatrixSwapsCalculator correlationMatrixSwapsCalculator;
        private readonly ICorrelationMatrixNoSwapsCalculator correlationMatrixNoSwapsCalculator;

        double LiabRisk;
        double LongevityRisk;
        double LiabRiskIncHedge;
        double LongevityRiskIncBuyIn;
        double CreditRisk;
        double EquityRisk;
        double OtherGrowthRisk;
        double TotalVaR;
        double TotalStdDev;
        double InterestInflationHedge;
        double LongevityHedgeRisk;        

        public WaterfallCalculator(ISchemeContext scheme, IBasisContext ctx)
            : this(scheme, ctx, new VaRCalculator(scheme), new CorrelationMatrixSwapsCalculator(scheme), new CorrelationMatrixNoSwapsCalculator())
        {
        }
        public WaterfallCalculator(ISchemeContext scheme, IBasisContext ctx, IVaRCalculator VaR, ICorrelationMatrixSwapsCalculator correlationMatrixSwapsCalculator, ICorrelationMatrixNoSwapsCalculator correlationMatrixNoSwapsCalculator)
        {
            this.scheme = scheme;
            this.ctx = ctx;
            this.VaR = VaR;
            this.correlationMatrixSwapsCalculator = correlationMatrixSwapsCalculator;
            this.correlationMatrixNoSwapsCalculator = correlationMatrixNoSwapsCalculator;
        }

        public WaterfallData Calculate()
        {
            excSwaps();
            incSwaps();

            InterestInflationHedge = LiabRisk - LiabRiskIncHedge;
            LongevityHedgeRisk = LongevityRisk - LongevityRiskIncBuyIn;

            var waterfall = new WaterfallData
                (
                    LiabRisk,
                    LiabRiskIncHedge,
                    InterestInflationHedge,
                    LongevityRisk,
                    LongevityRiskIncBuyIn,
                    LongevityHedgeRisk,
                    CreditRisk,
                    EquityRisk,
                    OtherGrowthRisk,
                    TotalVaR,
                    TotalStdDev
                );

            return waterfall;
        }

        private void excSwaps()
        {
            var totLiab = ctx.Results.FundingLevel.Liabilities;
            var giltLiability = ctx.Results.GiltBasisLiabilityData; 
            var inflationLink = giltLiability.LiabIE01.MathSafeDiv(giltLiability.LiabPV01, 1);

            VaR.Correlations = correlationMatrixNoSwapsCalculator.Calculate(ctx);
            VaR.Order = new List<AssetClassType> { AssetClassType.LiabilityInterest, AssetClassType.LiabilityInflation, AssetClassType.LiabilityLongevity };

            LiabRisk = VaR.CalculateRisk(AssetClassType.LiabilityInterest, AssetClassType.LiabilityInflation);
            LongevityRisk = VaR.CalculateRisk(AssetClassType.LiabilityLongevity);
        }

        private void incSwaps()
        {
            VaR.Correlations = correlationMatrixSwapsCalculator.Calculate(ctx);
            VaR.Order = new List<AssetClassType> { AssetClassType.LiabilityInterest, AssetClassType.FixedInterestGilts, AssetClassType.IndexLinkedGilts, AssetClassType.CorporateBonds, AssetClassType.LiabilityInflation, AssetClassType.IndexLinkedGiltsInflation, AssetClassType.Abf, AssetClassType.CreditSpread, AssetClassType.DiversifiedCredit, AssetClassType.Equity, AssetClassType.DiversifiedGrowth, AssetClassType.Property, AssetClassType.PrivateMarkets, AssetClassType.LiabilityLongevity };

            LiabRiskIncHedge = VaR.CalculateRisk(AssetClassType.LiabilityInterest, AssetClassType.FixedInterestGilts, AssetClassType.IndexLinkedGilts, AssetClassType.CorporateBonds, AssetClassType.LiabilityInflation, AssetClassType.IndexLinkedGiltsInflation);
            LongevityRiskIncBuyIn = VaR.CalculateRisk(AssetClassType.LiabilityLongevity);
            CreditRisk = VaR.CalculateRisk(AssetClassType.Abf, AssetClassType.CreditSpread, AssetClassType.DiversifiedCredit);
            EquityRisk = VaR.CalculateRisk(AssetClassType.Equity);
            OtherGrowthRisk = VaR.CalculateRisk(AssetClassType.DiversifiedGrowth, AssetClassType.Property, AssetClassType.PrivateMarkets);

            TotalVaR = VaR.CalculateRisk();
            TotalStdDev = TotalVaR / (VaR.TailPoint * Math.Pow(VaR.Time, 0.5));
        }
    }
}
