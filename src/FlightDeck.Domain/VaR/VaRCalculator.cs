﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using Meta.Numerics.Statistics.Distributions;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class VaRCalculator : IVaRCalculator
    {
        public double TailPoint { get { return new NormalDistribution().InverseLeftProbability(CI); } }        

        public IDictionary<AssetClassType, SwapImpact> Correlations { private get; set; }
        public IList<AssetClassType> Order { private get; set; }

        private double _ci;
        private int _time;

        public double CI
        {
            get { return _ci; }
            set { _ci = value; }
        }

        public int Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public VaRCalculator(ISchemeContext scheme)
        {
            _ci = scheme.Assumptions.WaterfallOptions.CI;
            _time = scheme.Assumptions.WaterfallOptions.Time;
        }

        public double CalculateRisk(params AssetClassType[] assets)
        {
            var exposure = calculateExposure(assets);

            var riskCalc = .0;

            for(int r = 0; r < Order.Count; r++)
            {
                for(int c = 0; c < Order.Count; c++)
                {
                    riskCalc = riskCalc + Correlations[Order[r]].CorrelationMatrix[Order[c]] * exposure[Order[r]] * exposure[Order[c]];
                }
            }

            riskCalc = Math.Pow(riskCalc * Time, 0.5) * TailPoint;

            return riskCalc;
        }

        private IDictionary<AssetClassType, double> calculateExposure(params AssetClassType[] assets)
        {
            var exposure = new Dictionary<AssetClassType, double>();

            var includeAll = assets.IsNullOrEmpty();

            for (int i = 0; i < Order.Count; i++)
            {
                var assetType = Order[i];

                exposure.Add(assetType,
                    assets.Contains(assetType) || includeAll
                        ? Correlations[assetType].Volatility * Correlations[assetType].Allocation * Correlations[assetType].Notional
                        : 0);
            }

            return exposure;
        }
    }
}
