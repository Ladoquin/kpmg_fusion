﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared.VaR;

    class VaRInputsCalculator
    {
        readonly ISchemeContext scheme;
        readonly IVolatilityAssumptionCalculator volatilityAssumptionCalculator;

        public VaRInputsCalculator(ISchemeContext scheme)
            : this(scheme, new VolatilityAssumptionCalculator(scheme))
        {
        }
        public VaRInputsCalculator(ISchemeContext scheme, IVolatilityAssumptionCalculator volatilityAssumptionCalculator)
        {
            this.scheme = scheme;
            this.volatilityAssumptionCalculator = volatilityAssumptionCalculator;
        }

        public VaRInputs Calculate(IBasisContext ctx)
        {
            ctx.Results.VaRInputs = new VaRInputs();
            ctx.Results.VaRInputs.VolatilityAssumptions = volatilityAssumptionCalculator.Calculate(ctx);
            ctx.Results.VaRInputs.NoSwapsCorrelationMatrixInputs = new CorrelationMatrixNoSwapsInputsService().Get(ctx);
            ctx.Results.VaRInputs.SwapsCorrelationMatrixInputs = new CorrelationMatrixSwapsInputsService().Get(ctx);

            return ctx.Results.VaRInputs;
        }
    }
}
