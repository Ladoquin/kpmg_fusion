﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Very similar to the defaults stored in CorrelationMatrixNoSwapsProvider.Defaults but this class
    /// ensures the ordering and applies any overrides. 
    /// </summary>
    class CorrelationMatrixNoSwapsInputsService
    {
        public IDictionary<AssetClassType, IDictionary<AssetClassType, double>> Get(IBasisContext ctx)
        {
            var rateInflationCorrelation = ctx.Results.VaRInputs.VolatilityAssumptions.RateInflationCorrelation;

            var defaults = CorrelationMatrixNoSwapsProvider.Defaults;

            AssetClassType[] c = 
            { 
                AssetClassType.LiabilityInterest,
                AssetClassType.LiabilityInflation,
                AssetClassType.LiabilityLongevity
            };

            var correlationMatrixInputs = c.ToDictionary(x => x, x => (IDictionary<AssetClassType, double>)new Dictionary<AssetClassType, double>());

            correlationMatrixInputs[c[0]].Add(c[0], defaults[c[0]][c[0]]);
            correlationMatrixInputs[c[0]].Add(c[1], rateInflationCorrelation);
            correlationMatrixInputs[c[0]].Add(c[2], defaults[c[0]][c[2]]);

            correlationMatrixInputs[c[1]].Add(c[0], rateInflationCorrelation);
            correlationMatrixInputs[c[1]].Add(c[1], defaults[c[1]][c[1]]);
            correlationMatrixInputs[c[1]].Add(c[2], defaults[c[1]][c[2]]);

            correlationMatrixInputs[c[2]].Add(c[0], defaults[c[2]][c[0]]);
            correlationMatrixInputs[c[2]].Add(c[1], defaults[c[2]][c[1]]);
            correlationMatrixInputs[c[2]].Add(c[2], defaults[c[2]][c[2]]);

            return correlationMatrixInputs;
        }
    }
}
