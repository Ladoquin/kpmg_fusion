﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using System.Collections.Generic;

    class CorrelationMatrixNoSwapsCalculator : ICorrelationMatrixNoSwapsCalculator
    {
        public IDictionary<AssetClassType, SwapImpact> Calculate(IBasisContext ctx)
        {
            var TotLiab = ctx.Results.FundingLevel.Liabilities;
            var giltLiability = ctx.Results.GiltBasisLiabilityData;
            var volatilityAssumptions = ctx.Results.VaRInputs.VolatilityAssumptions;
            var volatilities = volatilityAssumptions.Volatilities;
            var inflationLink = giltLiability.LiabIE01.MathSafeDiv(giltLiability.LiabPV01, 1);

            var correlationMatrix = ctx.Results.VaRInputs.NoSwapsCorrelationMatrixInputs;

            var matrix = new Dictionary<AssetClassType, SwapImpact>(3);

            matrix.Add(AssetClassType.LiabilityLongevity,
                new SwapImpact
                    {
                        Allocation = -1,
                        Notional = TotLiab,
                        Volatility = volatilities[AssetClassType.LiabilityLongevity],
                        VolatilityProduct = volatilities[AssetClassType.LiabilityLongevity] * -1 * TotLiab,
                        CorrelationMatrix = correlationMatrix[AssetClassType.LiabilityLongevity]
                    });

            matrix.Add(AssetClassType.LiabilityInflation, 
                new SwapImpact
                    {
                        Allocation = -inflationLink,
                        Notional = TotLiab,
                        Volatility = volatilities[AssetClassType.LiabilityInflation],
                        VolatilityProduct = volatilities[AssetClassType.LiabilityInflation] * -0.85 * TotLiab,
                        CorrelationMatrix = correlationMatrix[AssetClassType.LiabilityInflation]
                    });

            matrix.Add(AssetClassType.LiabilityInterest, 
                new SwapImpact
                    {
                        Allocation = -1,
                        Notional = TotLiab,
                        Volatility = volatilities[AssetClassType.LiabilityInterest],
                        VolatilityProduct = volatilities[AssetClassType.LiabilityInterest] * -1 * TotLiab,
                        CorrelationMatrix = correlationMatrix[AssetClassType.LiabilityInterest]
                    });

            return matrix;
        }
    }
}
