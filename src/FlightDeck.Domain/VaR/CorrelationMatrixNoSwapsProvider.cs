﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public static class CorrelationMatrixNoSwapsProvider
    {
        public static IDictionary<AssetClassType, IDictionary<AssetClassType, double>> Defaults =
            new Dictionary<AssetClassType, IDictionary<AssetClassType, double>>
                {
                    { AssetClassType.LiabilityInterest, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 1 },
                            { AssetClassType.LiabilityInflation, -0.25 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.LiabilityInflation, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, -0.25 },
                            { AssetClassType.LiabilityInflation, 1 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.LiabilityLongevity, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 0 },
                            { AssetClassType.LiabilityInflation, 0 },
                            { AssetClassType.LiabilityLongevity, 1 }
                        }
                    }
                };
    }
}
