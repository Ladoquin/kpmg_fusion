﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Very similar to the defaults stored in CorrelationMatrixSwapsProvider.Defaults but this class
    /// ensures the ordering and applies any overrides. 
    /// </summary>
    class CorrelationMatrixSwapsInputsService
    {
        public IDictionary<AssetClassType, IDictionary<AssetClassType, double>> Get(IBasisContext ctx)
        {
            var rateInflationCorrelation = ctx.Results.VaRInputs.VolatilityAssumptions.RateInflationCorrelation;

            var defaults = CorrelationMatrixSwapsProvider.Defaults;

            var correlationMatrixInputs = new Dictionary<AssetClassType, IDictionary<AssetClassType, double>>();
            
            //set the ordering
            AssetClassType[] c = 
            { 
                AssetClassType.LiabilityInterest,
                AssetClassType.FixedInterestGilts,
                AssetClassType.IndexLinkedGilts,
                AssetClassType.CorporateBonds,
                AssetClassType.LiabilityInflation,
                AssetClassType.IndexLinkedGiltsInflation,
                AssetClassType.Abf,
                AssetClassType.CreditSpread,
                AssetClassType.DiversifiedCredit,
                AssetClassType.Equity,
                AssetClassType.DiversifiedGrowth,
                AssetClassType.Property,
                AssetClassType.PrivateMarkets,
                AssetClassType.LiabilityLongevity 
            };

            //get the defaults in to the matrix
            for (int i = 0; i < c.Length; i++)
            {
                correlationMatrixInputs.Add(c[i], new Dictionary<AssetClassType, double>());

                for (int j = 0; j < c.Length; j++)
                {
                    correlationMatrixInputs[c[i]].Add(c[j], CorrelationMatrixSwapsProvider.Defaults[c[i]][c[j]]);
                }
            }
            
            Action<int, int, double> overrideMatrix = (i, j, val) =>
                {
                    correlationMatrixInputs[c[i]][c[j]] = val;
                    correlationMatrixInputs[c[j]][c[i]] = val;
                };

            //apply any overrides to the matrix
            overrideMatrix(0, 4, rateInflationCorrelation);
            overrideMatrix(0, 5, rateInflationCorrelation);
            overrideMatrix(1, 4, rateInflationCorrelation);
            overrideMatrix(1, 5, rateInflationCorrelation);
            overrideMatrix(2, 4, rateInflationCorrelation);
            overrideMatrix(2, 5, rateInflationCorrelation);
            overrideMatrix(3, 4, rateInflationCorrelation);
            overrideMatrix(3, 5, rateInflationCorrelation);
            overrideMatrix(4, 6, CorrelationMatrixSwapsProvider.Defaults[c[0]][c[4]]);
            overrideMatrix(5, 6, CorrelationMatrixSwapsProvider.Defaults[c[0]][c[4]]);           

            return correlationMatrixInputs;
        }
    }
}
