﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.Domain.JourneyPlan;
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Implements Tracker v47b VolatilityClass
    /// </summary>
    /// <remarks>
    /// The VolatilityAssumptions worksheet functionality is overriden by the VBA when feeding in to the Waterfall calcs.
    /// </remarks>
    class VolatilityAssumptionCalculator : IVolatilityAssumptionCalculator
    {
        const double LongevityVolatility = 0.035;

        private readonly ISchemeContext scheme;

        public VolatilityAssumptionCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public VolatilityAssumptionResults Calculate(IBasisContext ctx)
        {
            var defaults = scheme.Data.DefaultVolatilityAssumptions;
            var giltLiability = ctx.Results.GiltBasisLiabilityData;

            var jpAfter = new JourneyPlanAfterService(scheme).Calculate(ctx);
            var liabDuration = jpAfter.LiabDuration;
            var defaultGiltDuration = scheme.Data.GiltDurationDefault;
            var giltDuration = scheme.Data.Funded ? ctx.Portfolio.AssetData.GiltDuration : defaultGiltDuration;
            var gilt = defaults[AssetClassType.FixedInterestGilts];
            var ilg = defaults[AssetClassType.IndexLinkedGilts];
            var defaultCorpDuration = scheme.Data.CorpDurationDefault;
            var corpDuration = scheme.Data.Funded ? ctx.Portfolio.AssetData.CorpDuration : defaultCorpDuration;

            var liabInterest = (gilt * liabDuration).MathSafeDiv((double)defaultGiltDuration);
            var giltInterest = (gilt * (double)giltDuration).MathSafeDiv((double)defaultGiltDuration);
            var ilgInterest = (ilg * (double)giltDuration).MathSafeDiv((double)defaultGiltDuration);
            var creditDuration = gilt * (((double)corpDuration).MathSafeDiv((double)defaultGiltDuration));
            var desiredInflation = ((ilg * (double)giltDuration).MathSafeDiv((double)defaultGiltDuration)).MathSafeDiv(2);
            var liabInflationWithSwaps = (desiredInflation * liabDuration).MathSafeDiv((double)giltDuration);
            var ilgInflation = desiredInflation;
            var abf = defaults[AssetClassType.Abf];
            var corporate = (defaults[AssetClassType.CorporateBonds] * (double)corpDuration).MathSafeDiv((double)defaultCorpDuration);
            var creditSpread = Math.Pow((Math.Pow(corporate, 2) - Math.Pow(creditDuration, 2)), 0.5);
            var divCredit = defaults[AssetClassType.DiversifiedCredit];
            var equity = defaults[AssetClassType.Equity];
            var divGrowth = defaults[AssetClassType.DiversifiedGrowth];
            var propertyUK = defaults[AssetClassType.Property];
            var privateMarket = defaults[AssetClassType.PrivateMarkets];
            var longevity = LongevityVolatility;

            var volatilities = new Dictionary<AssetClassType, double>
                {
                    { AssetClassType.LiabilityInterest, liabInterest },
                    { AssetClassType.FixedInterestGilts, giltInterest },
                    { AssetClassType.IndexLinkedGilts, ilgInterest },
                    { AssetClassType.CorporateBonds, creditDuration },
                    { AssetClassType.LiabilityInflation, liabInflationWithSwaps },
                    { AssetClassType.IndexLinkedGiltsInflation, ilgInflation },
                    { AssetClassType.Abf, abf },
                    { AssetClassType.CreditSpread, creditSpread },
                    { AssetClassType.DiversifiedCredit, divCredit },
                    { AssetClassType.Equity, equity },
                    { AssetClassType.DiversifiedGrowth, divGrowth },
                    { AssetClassType.Property, propertyUK },
                    { AssetClassType.PrivateMarkets, privateMarket },
                    { AssetClassType.LiabilityLongevity, longevity }
                };

            //Rate_Inflation_Correlation implementation from the worksheet - is it used anywhere though? 
            var C13 = defaults[AssetClassType.IndexLinkedGilts];
            var D13 = (C13 * giltDuration).MathSafeDiv(defaultGiltDuration);
            var B59 = D13.MathSafeDiv(2);

            var rateInflationCorrelation = -(Math.Pow(B59, 2)).MathSafeDiv(2 * D13 * B59);

            var results = new VolatilityAssumptionResults(volatilities, rateInflationCorrelation);

            return results;
        }
    }
}
