﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using Meta.Numerics.Statistics.Distributions;
    using System;
    using System.Collections.Generic;

    class FunnelCalculator
    {
        private readonly ISchemeContext scheme;

        public FunnelCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public IList<VarFunnelData> Calculate(IBasisContext ctx)
        {
            var funnel = new List<VarFunnelData>(6);

            var startingBalance = ctx.Results.AssetsAtSED - ctx.Results.FundingLevel.Liabilities;
            var totalStdDeviation = ctx.Results.VarWaterfall.TotalStdDeviation;
            var norminv = new NormalDistribution();

            funnel.Add(new VarFunnelData(startingBalance, startingBalance, startingBalance, startingBalance, startingBalance));

            for (int y = 1; y <= 5; y++)
            {
                var median = ctx.Results.JourneyPlanAfter[y].SurplusDeficit;
                var nintyfifthDev = totalStdDeviation * Math.Sqrt(y) * norminv.InverseLeftProbability(0.95);
                var eightiethDev = totalStdDeviation * Math.Sqrt(y) * norminv.InverseLeftProbability(0.8);

                var ninetyfifth = median + nintyfifthDev;
                var eightieth = median + eightiethDev;
                var twentieth = median - eightiethDev;
                var fifth = median - nintyfifthDev;

                funnel.Add(new VarFunnelData(ninetyfifth, eightieth, median, twentieth, fifth));
            }

            return funnel;
        }
    }
}
