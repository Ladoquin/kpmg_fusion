﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using System.Collections.Generic;

    class CorrelationMatrixSwapsCalculator : ICorrelationMatrixSwapsCalculator
    {
        private readonly ISchemeContext scheme;

        public CorrelationMatrixSwapsCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }    

        public IDictionary<AssetClassType, SwapImpact> Calculate(IBasisContext ctx)
        {
            var volatilityAssumptions = ctx.Results.VaRInputs.VolatilityAssumptions;
            var volatilities = volatilityAssumptions.Volatilities;

            var clientAssetAllocations = scheme.Assumptions.InvestmentStrategyAssetAllocation;
            var totLiab = ctx.Results.FundingLevel.Liabilities;
            var giltLiability = ctx.Results.GiltBasisLiabilityData; 
            var inflationLink = giltLiability.LiabIE01.MathSafeDiv(giltLiability.LiabPV01, 1);
            var totABF = ctx.Results.InvestmentStrategy.ABF;
            var assetsIncOthers = ctx.Results.InvestmentStrategy.TotalAssets - totABF;
            var assetsExcOthers = ctx.Results.InvestmentStrategy.TotalCoreAssets;
            var linkedToCorp = ctx.Data.LinkedToCorpBonds;
            var propSyntheticCredit = scheme.Assumptions.InvestmentStrategyOptions.SyntheticCredit;
            var propSyntheticEquity = scheme.Assumptions.InvestmentStrategyOptions.SyntheticEquity;

            var assetMix = new Dictionary<AssetClassType, double>();
            foreach (var assetClassType in Utils.EnumToArray<AssetClassType>())
            {
                assetMix.Add(assetClassType, clientAssetAllocations.ContainsKey(assetClassType) ? ctx.Results.InvestmentStrategy.ABFAdjustment * clientAssetAllocations[assetClassType] : 0);
            }

            var correlationMatrix = ctx.Results.VaRInputs.SwapsCorrelationMatrixInputs;

            var matrix = new Dictionary<AssetClassType, SwapImpact>(20);

            var hedge = ctx.Results.Hedging;

            matrix.Add(AssetClassType.LiabilityInterest, new SwapImpact
                {
                    Allocation = -(1 - hedge.CurrLDIOverlayInterestHedge - hedge.BuyInInterestHedge),
                    Notional = totLiab,
                    Volatility = volatilities[AssetClassType.LiabilityInterest],
                    CorrelationMatrix = correlationMatrix[AssetClassType.LiabilityInterest]
                });
            matrix.Add(AssetClassType.FixedInterestGilts, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.FixedInterestGilts],
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.FixedInterestGilts],
                    CorrelationMatrix = correlationMatrix[AssetClassType.FixedInterestGilts]
                });
            matrix.Add(AssetClassType.IndexLinkedGilts, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.IndexLinkedGilts],
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.IndexLinkedGilts],
                    CorrelationMatrix = correlationMatrix[AssetClassType.IndexLinkedGilts]
                });
            matrix.Add(AssetClassType.CorporateBonds, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.CorporateBonds],
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.CorporateBonds],
                    CorrelationMatrix = correlationMatrix[AssetClassType.CorporateBonds]
                });
            matrix.Add(AssetClassType.LiabilityInflation, new SwapImpact
                {
                    Allocation = -inflationLink * (1 - hedge.CurrLDIOverlayInflationHedge - hedge.BuyInInflationHedge),
                    Notional = totLiab,
                    Volatility = volatilities[AssetClassType.LiabilityInflation],
                    CorrelationMatrix = correlationMatrix[AssetClassType.LiabilityInflation]
                });
            matrix.Add(AssetClassType.IndexLinkedGiltsInflation, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.IndexLinkedGilts],
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.IndexLinkedGiltsInflation],
                    CorrelationMatrix = correlationMatrix[AssetClassType.IndexLinkedGiltsInflation]
                });
            matrix.Add(AssetClassType.Abf, new SwapImpact
                {
                    Allocation = 1,
                    Notional = totABF,
                    Volatility = volatilities[AssetClassType.Abf],
                    CorrelationMatrix = correlationMatrix[AssetClassType.Abf]
                });
            matrix.Add(AssetClassType.CreditSpread, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.CorporateBonds] + propSyntheticCredit - (linkedToCorp ? totLiab.MathSafeDiv(assetsExcOthers) : 0),
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.CreditSpread],
                    CorrelationMatrix = correlationMatrix[AssetClassType.CreditSpread]
                });
            matrix.Add(AssetClassType.DiversifiedCredit, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.DiversifiedCredit],
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.DiversifiedCredit],
                    CorrelationMatrix = correlationMatrix[AssetClassType.DiversifiedCredit]
                });
            matrix.Add(AssetClassType.Equity, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.Equity] + propSyntheticEquity,
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.Equity],
                    CorrelationMatrix = correlationMatrix[AssetClassType.Equity]
                });
            matrix.Add(AssetClassType.DiversifiedGrowth, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.DiversifiedGrowth],
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.DiversifiedGrowth],
                    CorrelationMatrix = correlationMatrix[AssetClassType.DiversifiedGrowth]
                });
            matrix.Add(AssetClassType.Property, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.Property],
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.Property],
                    CorrelationMatrix = correlationMatrix[AssetClassType.Property]
                });
            matrix.Add(AssetClassType.PrivateMarkets, new SwapImpact
                {
                    Allocation = assetMix[AssetClassType.PrivateMarkets],
                    Notional = assetsExcOthers,
                    Volatility = volatilities[AssetClassType.PrivateMarkets],
                    CorrelationMatrix = correlationMatrix[AssetClassType.PrivateMarkets]
                });
            matrix.Add(AssetClassType.LiabilityLongevity, new SwapImpact
                {
                    Allocation = -(1 - hedge.BuyInLongevityHedge),
                    Notional = totLiab,
                    Volatility = volatilities[AssetClassType.LiabilityLongevity],
                    CorrelationMatrix = correlationMatrix[AssetClassType.LiabilityLongevity]
                });

            return matrix;
        }
    }
}
