﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public class CorrelationMatrixSwapsProvider
    {
        public static IDictionary<AssetClassType, IDictionary<AssetClassType, double>> Defaults =
            new Dictionary<AssetClassType, IDictionary<AssetClassType, double>>
                {
                    { AssetClassType.LiabilityInterest, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 1 },
                            { AssetClassType.FixedInterestGilts, 1 },
                            { AssetClassType.IndexLinkedGilts, 1 },
                            { AssetClassType.CorporateBonds, 1 },
                            { AssetClassType.LiabilityInflation, -0.25 },
                            { AssetClassType.IndexLinkedGiltsInflation, -0.25 },
                            { AssetClassType.Abf, 0.75 },
                            { AssetClassType.CreditSpread, 0 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 0 },
                            { AssetClassType.DiversifiedGrowth, 0 },
                            { AssetClassType.Property, 0 },
                            { AssetClassType.PrivateMarkets, 0 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.FixedInterestGilts, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 1 },
                            { AssetClassType.FixedInterestGilts, 1 },
                            { AssetClassType.IndexLinkedGilts, 1 },
                            { AssetClassType.CorporateBonds, 1 },
                            { AssetClassType.LiabilityInflation, -0.25 },
                            { AssetClassType.IndexLinkedGiltsInflation, -0.25 },
                            { AssetClassType.Abf, 0.75 },
                            { AssetClassType.CreditSpread, 0 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 0 },
                            { AssetClassType.DiversifiedGrowth, 0 },
                            { AssetClassType.Property, 0 },
                            { AssetClassType.PrivateMarkets, 0 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.IndexLinkedGilts, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 1 },
                            { AssetClassType.FixedInterestGilts, 1 },
                            { AssetClassType.IndexLinkedGilts, 1 },
                            { AssetClassType.CorporateBonds, 1 },
                            { AssetClassType.LiabilityInflation, -0.25 },
                            { AssetClassType.IndexLinkedGiltsInflation, -0.25 },
                            { AssetClassType.Abf, 0.75 },
                            { AssetClassType.CreditSpread, 0 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 0 },
                            { AssetClassType.DiversifiedGrowth, 0 },
                            { AssetClassType.Property, 0 },
                            { AssetClassType.PrivateMarkets, 0 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.CorporateBonds, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 1 },
                            { AssetClassType.FixedInterestGilts, 1 },
                            { AssetClassType.IndexLinkedGilts, 1 },
                            { AssetClassType.CorporateBonds, 1 },
                            { AssetClassType.LiabilityInflation, -0.25 },
                            { AssetClassType.IndexLinkedGiltsInflation, -0.25 },
                            { AssetClassType.Abf, 0.75 },
                            { AssetClassType.CreditSpread, 0 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 0.25 },
                            { AssetClassType.DiversifiedGrowth, 0.25 },
                            { AssetClassType.Property, 0.25 },
                            { AssetClassType.PrivateMarkets, 0.25 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.LiabilityInflation, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, -0.25 },
                            { AssetClassType.FixedInterestGilts, -0.25 },
                            { AssetClassType.IndexLinkedGilts, -0.25 },
                            { AssetClassType.CorporateBonds, -0.25 },
                            { AssetClassType.LiabilityInflation, 1 },
                            { AssetClassType.IndexLinkedGiltsInflation, 1 },
                            { AssetClassType.Abf, -0.25 },
                            { AssetClassType.CreditSpread, 0 },
                            { AssetClassType.DiversifiedCredit, 0 },
                            { AssetClassType.Equity, 0.25 },
                            { AssetClassType.DiversifiedGrowth, 0.25 },
                            { AssetClassType.Property, 0.25 },
                            { AssetClassType.PrivateMarkets, 0.25 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.IndexLinkedGiltsInflation, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, -0.25 },
                            { AssetClassType.FixedInterestGilts, -0.25 },
                            { AssetClassType.IndexLinkedGilts, -0.25 },
                            { AssetClassType.CorporateBonds, -0.25 },
                            { AssetClassType.LiabilityInflation, 1 },
                            { AssetClassType.IndexLinkedGiltsInflation, 1 },
                            { AssetClassType.Abf, -0.25 },
                            { AssetClassType.CreditSpread, 0 },
                            { AssetClassType.DiversifiedCredit, 0 },
                            { AssetClassType.Equity, 0.25 },
                            { AssetClassType.DiversifiedGrowth, 0.25 },
                            { AssetClassType.Property, 0.25 },
                            { AssetClassType.PrivateMarkets, 0.25 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.Abf, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 0.75 },
                            { AssetClassType.FixedInterestGilts, 0.75 },
                            { AssetClassType.IndexLinkedGilts, 0.75 },
                            { AssetClassType.CorporateBonds, 0.75 },
                            { AssetClassType.LiabilityInflation, -0.25 },
                            { AssetClassType.IndexLinkedGiltsInflation, -0.25 },
                            { AssetClassType.Abf, 1 },
                            { AssetClassType.CreditSpread, 0.5 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 0.5 },
                            { AssetClassType.DiversifiedGrowth, 0.5 },
                            { AssetClassType.Property, 0.5 },
                            { AssetClassType.PrivateMarkets, 0.5 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.CreditSpread, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 0 },
                            { AssetClassType.FixedInterestGilts, 0 },
                            { AssetClassType.IndexLinkedGilts, 0 },
                            { AssetClassType.CorporateBonds, 0 },
                            { AssetClassType.LiabilityInflation, 0 },
                            { AssetClassType.IndexLinkedGiltsInflation, 0 },
                            { AssetClassType.Abf, 0.5 },
                            { AssetClassType.CreditSpread, 1 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 0.5 },
                            { AssetClassType.DiversifiedGrowth, 0.5 },
                            { AssetClassType.Property, 0.5 },
                            { AssetClassType.PrivateMarkets, 0.5 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.DiversifiedCredit, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 0.5 },
                            { AssetClassType.FixedInterestGilts, 0.5 },
                            { AssetClassType.IndexLinkedGilts, 0.5 },
                            { AssetClassType.CorporateBonds, 0.5 },
                            { AssetClassType.LiabilityInflation, 0 },
                            { AssetClassType.IndexLinkedGiltsInflation, 0 },
                            { AssetClassType.Abf, 0.5 },
                            { AssetClassType.CreditSpread, 0.5 },
                            { AssetClassType.DiversifiedCredit, 1 },
                            { AssetClassType.Equity, 0.5 },
                            { AssetClassType.DiversifiedGrowth, 0.5 },
                            { AssetClassType.Property, 0.5 },
                            { AssetClassType.PrivateMarkets, 0.5 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.Equity, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 0 },
                            { AssetClassType.FixedInterestGilts, 0 },
                            { AssetClassType.IndexLinkedGilts, 0 },
                            { AssetClassType.CorporateBonds, 0.25 },
                            { AssetClassType.LiabilityInflation, 0.25 },
                            { AssetClassType.IndexLinkedGiltsInflation, 0.25 },
                            { AssetClassType.Abf, 0.5 },
                            { AssetClassType.CreditSpread, 0.5 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 1 },
                            { AssetClassType.DiversifiedGrowth, 0.5 },
                            { AssetClassType.Property, 0.5 },
                            { AssetClassType.PrivateMarkets, 0.5 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.DiversifiedGrowth, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 0 },
                            { AssetClassType.FixedInterestGilts, 0 },
                            { AssetClassType.IndexLinkedGilts, 0 },
                            { AssetClassType.CorporateBonds, 0.25 },
                            { AssetClassType.LiabilityInflation, 0.25 },
                            { AssetClassType.IndexLinkedGiltsInflation, 0.25 },
                            { AssetClassType.Abf, 0.5 },
                            { AssetClassType.CreditSpread, 0.5 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 0.5 },
                            { AssetClassType.DiversifiedGrowth, 1 },
                            { AssetClassType.Property, 0.5 },
                            { AssetClassType.PrivateMarkets, 0.5 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.Property, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 0 },
                            { AssetClassType.FixedInterestGilts, 0 },
                            { AssetClassType.IndexLinkedGilts, 0 },
                            { AssetClassType.CorporateBonds, 0.25 },
                            { AssetClassType.LiabilityInflation, 0.25 },
                            { AssetClassType.IndexLinkedGiltsInflation, 0.25 },
                            { AssetClassType.Abf, 0.5 },
                            { AssetClassType.CreditSpread, 0.5 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 0.5 },
                            { AssetClassType.DiversifiedGrowth, 0.5 },
                            { AssetClassType.Property, 1 },
                            { AssetClassType.PrivateMarkets, 0.5 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.PrivateMarkets, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 0 },
                            { AssetClassType.FixedInterestGilts, 0 },
                            { AssetClassType.IndexLinkedGilts, 0 },
                            { AssetClassType.CorporateBonds, 0.25 },
                            { AssetClassType.LiabilityInflation, 0.25 },
                            { AssetClassType.IndexLinkedGiltsInflation, 0.25 },
                            { AssetClassType.Abf, 0.5 },
                            { AssetClassType.CreditSpread, 0.5 },
                            { AssetClassType.DiversifiedCredit, 0.5 },
                            { AssetClassType.Equity, 0.5 },
                            { AssetClassType.DiversifiedGrowth, 0.5 },
                            { AssetClassType.Property, 0.5 },
                            { AssetClassType.PrivateMarkets, 1 },
                            { AssetClassType.LiabilityLongevity, 0 }
                        }
                    },
                    { AssetClassType.LiabilityLongevity, 
                        new Dictionary<AssetClassType, double> 
                        {
                            { AssetClassType.LiabilityInterest, 0 },
                            { AssetClassType.FixedInterestGilts, 0 },
                            { AssetClassType.IndexLinkedGilts, 0 },
                            { AssetClassType.CorporateBonds, 0 },
                            { AssetClassType.LiabilityInflation, 0 },
                            { AssetClassType.IndexLinkedGiltsInflation, 0 },
                            { AssetClassType.Abf, 0 },
                            { AssetClassType.CreditSpread, 0 },
                            { AssetClassType.DiversifiedCredit, 0 },
                            { AssetClassType.Equity, 0 },
                            { AssetClassType.DiversifiedGrowth, 0 },
                            { AssetClassType.Property, 0 },
                            { AssetClassType.PrivateMarkets, 0 },
                            { AssetClassType.LiabilityLongevity, 1 }
                        }
                    }
                };
    }
}
