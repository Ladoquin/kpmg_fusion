﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using System.Collections.Generic;
    
    interface ICorrelationMatrixNoSwapsCalculator
    {
        IDictionary<AssetClassType, SwapImpact> Calculate(IBasisContext ctx);
    }
}
