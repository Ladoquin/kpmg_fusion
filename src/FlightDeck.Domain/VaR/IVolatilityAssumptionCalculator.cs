﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared.VaR;
    
    interface IVolatilityAssumptionCalculator
    {
        VolatilityAssumptionResults Calculate(IBasisContext ctx);
    }
}
