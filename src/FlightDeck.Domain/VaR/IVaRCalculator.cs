﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using System.Collections.Generic;
    using System.Linq;
    
    interface IVaRCalculator
    {
        IDictionary<AssetClassType, SwapImpact> Correlations { set; }
        IList<AssetClassType> Order { set; }
        double CalculateRisk(params AssetClassType[] assets);
        double CI { get; set; }
        double TailPoint { get; }
        int Time { get; set; }
    }
}
