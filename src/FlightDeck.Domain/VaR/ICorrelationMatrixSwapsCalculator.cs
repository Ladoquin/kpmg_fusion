﻿namespace FlightDeck.Domain.VaR
{
    using FlightDeck.Domain.LDI;
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using System.Collections.Generic;
    
    interface ICorrelationMatrixSwapsCalculator
    {
        IDictionary<AssetClassType, SwapImpact> Calculate(IBasisContext ctx);
    }
}
