﻿namespace FlightDeck.Domain
{
    using System.Collections.Generic;
    using System.Linq;
       
    class NPV
    {
        public IDictionary<MemberStatus, double> Liabilities { get; set; }
        public IDictionary<MemberStatus, double> Buyin { get; set; }
        public IDictionary<MemberStatus, double> Duration { get; set; }
        public double Total { get; set; }

        public NPV()
        {
            Liabilities = new Dictionary<MemberStatus, double>
                {
                    { MemberStatus.ActiveFuture, 0 },
                    { MemberStatus.ActivePast, 0 },
                    { MemberStatus.Deferred, 0 },
                    { MemberStatus.Pensioner, 0 }
                };
            Buyin = new Dictionary<MemberStatus, double>
                {
                    { MemberStatus.ActiveFuture, 0 },
                    { MemberStatus.ActivePast, 0 },
                    { MemberStatus.Deferred, 0 },
                    { MemberStatus.Pensioner, 0 }
                };
            Duration = new Dictionary<MemberStatus, double>
                {
                    { MemberStatus.ActiveFuture, 0 },
                    { MemberStatus.ActivePast, 0 },
                    { MemberStatus.Deferred, 0 },
                    { MemberStatus.Pensioner, 0 }
                };
        }
    }
}
