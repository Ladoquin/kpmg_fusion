﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Basis
    {
        public int Id { get; private set; }
        public int PensionSchemeId { get; set; }
        public int MasterBasisId { get; set; }
        public string DisplayName { get; private set; }
        public BasisType Type { get; private set; }
        public DateTime EffectiveDate { get; private set; }
        public DateTime LastPensionIncreaseDate { get; private set; }
        public bool LinkedToCorpBonds { get; private set; }
        public IList<SalaryProgression> SalaryProgression { get; private set; }
        public IEnumerable<CashflowData> Cashflows { get; private set; }
        public IDictionary<AssumptionType, double> Assumptions { get; private set; }
        public IDictionary<int, double> PensionIncreases { get; private set; }
        public LifeExpectancyAssumptions LifeExpectancy { get; private set; }
        public IDictionary<int, double> MortalityAdjustments { get; private set; }
        public BondYield BondYield { get; private set; }
        public double ActuarialBalanceInAOCI { get; private set; }
        public double PriorServiceBalanceInAOCI { get; private set; }
        public double AmortisationOfPriorService { get; private set; }
        public int AmortisationPeriod { get; set; }
        public int RefGiltYieldId { get; private set; }
        public IDictionary<SimpleAssumptionType, SimpleAssumption> SimpleAssumptions { get; private set; }
        public IDictionary<AssumptionType, AssumptionLabel> CustomAssumptionLabels { get; private set; }

        public IDictionary<AssumptionType, FinancialAssumption> UnderlyingAssumptionIndices { get; private set; }
        public IDictionary<int, PensionIncreaseAssumption> UnderlyingPensionIncreaseIndices { get; private set; }
        public FinancialIndex UnderlyingGiltYieldIndex { get; private set; }

        public TotalCosts TotalCosts { get; set; }

        public Basis(int id, int pensionSchemeId, int masterBasisId, string displayName, BasisType type, DateTime effectiveDate, DateTime lastPensionIncreaseDate, bool linkedToCorpBonds, IList<SalaryProgression> salaryProgression, IEnumerable<CashflowData> cashflows, IDictionary<AssumptionType, double> assumptions, IDictionary<int, double> pensionIncreases, IDictionary<int, double> mortalityAdjustments, BondYield bondYield, double actuarialBalanceInAOCI, double priorServiceBalanceInAOCI, double amortisationOfPriorService, int amortisationPeriod, IDictionary<SimpleAssumptionType, SimpleAssumption> simpleAssumptions, int refGiltYieldId)
            : this(id, pensionSchemeId, masterBasisId, displayName, type, effectiveDate, lastPensionIncreaseDate, linkedToCorpBonds, salaryProgression, cashflows, assumptions, pensionIncreases, mortalityAdjustments, bondYield, actuarialBalanceInAOCI, priorServiceBalanceInAOCI, amortisationOfPriorService, amortisationPeriod, simpleAssumptions, refGiltYieldId, null, null, null)
        {
        }
        public Basis(int id, int pensionSchemeId, int masterBasisId, string displayName, BasisType type, DateTime effectiveDate, DateTime lastPensionIncreaseDate, bool linkedToCorpBonds, IList<SalaryProgression> salaryProgression, IEnumerable<CashflowData> cashflows, IDictionary<AssumptionType, double> assumptions, IDictionary<int, double> pensionIncreases, IDictionary<int, double> mortalityAdjustments, BondYield bondYield, double actuarialBalanceInAOCI, double priorServiceBalanceInAOCI, double amortisationOfPriorService, int amortisationPeriod, IDictionary<SimpleAssumptionType, SimpleAssumption> simpleAssumptions, int refGiltYieldId, IDictionary<AssumptionType, FinancialAssumption> underlyingAssumptionIndices, IDictionary<int, PensionIncreaseAssumption> underlyingPensionIncreaseIndices, FinancialIndex underlyingGiltYieldIndex)
        {
            Id = id;
            PensionSchemeId = pensionSchemeId;
            MasterBasisId = masterBasisId;
            DisplayName = displayName;
            Type = type;
            EffectiveDate = effectiveDate;
            LastPensionIncreaseDate = lastPensionIncreaseDate;
            LinkedToCorpBonds = linkedToCorpBonds;
            SalaryProgression = salaryProgression;
            Cashflows = cashflows;
            Assumptions = assumptions;
            PensionIncreases = pensionIncreases;            
            MortalityAdjustments = mortalityAdjustments;
            BondYield = bondYield;
            ActuarialBalanceInAOCI = actuarialBalanceInAOCI;
            PriorServiceBalanceInAOCI = priorServiceBalanceInAOCI;
            AmortisationOfPriorService = amortisationOfPriorService;
            AmortisationPeriod = amortisationPeriod;
            SimpleAssumptions = simpleAssumptions;
            RefGiltYieldId = refGiltYieldId;

            LifeExpectancy = new LifeExpectancyAssumptions(simpleAssumptions[SimpleAssumptionType.LifeExpectancy65].Value, simpleAssumptions[SimpleAssumptionType.LifeExpectancy65_45].Value);

            CustomAssumptionLabels =
                underlyingAssumptionIndices
                    .Where(x => !string.IsNullOrEmpty(x.Value.Name))
                    .ToDictionary(x => x.Key, x => new AssumptionLabel(x.Value.Name, x.Value.IsVisible));

            UnderlyingAssumptionIndices = underlyingAssumptionIndices;
            UnderlyingPensionIncreaseIndices = underlyingPensionIncreaseIndices;
            UnderlyingGiltYieldIndex = underlyingGiltYieldIndex;
        }

    }
}
