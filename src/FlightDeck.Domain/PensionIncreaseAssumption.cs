﻿using System;
using FlightDeck.DomainShared;

namespace FlightDeck.Domain
{
    public class PensionIncreaseAssumption : AssumptionBase
    {
        public int ReferenceNumber { get; private set; }
        public string Label { get; private set; }
        public double Minimum { get; private set; }
        public double Maximum { get; private set; }
        public double FixedIncrease { get; private set; }
        public double InflationVolatility { get; private set; }
        public InflationType InflationType { get; private set; }
        public bool IsFixed { get; private set; }
        public bool IsNilIncrease { get { return Math.Abs(FixedIncrease) < Utils.Precision && InflationType == InflationType.None; }}

        private double notionalInflation;

        public PensionIncreaseAssumption(int id, int basisId, double initialValue, FinancialIndex index,
            int referenceNumber, string label, double minimum, double maximum, double fixedIncrease,
            AssumptionCategory assumptionCategory, double inflationVolatility, InflationType inflationType, bool isFixed, bool isVisible) :
            base(id, basisId, initialValue, index, isVisible, assumptionCategory)
        {
            ReferenceNumber = referenceNumber;
            Label = label;
            Maximum = maximum;
            Minimum = minimum;
            FixedIncrease = fixedIncrease;
            InflationVolatility = inflationVolatility;
            InflationType = inflationType;
            IsFixed = isFixed;

            notionalInflation = Category == AssumptionCategory.BlackScholes ? new BlackScholesCalculator().GetNotionalInflation(InitialValue, InflationVolatility, Minimum, Maximum) : 0;
        }

        public override double GetValue(DateTime date, DateTime effectiveDate, double? inflationAdjustment = null)
        {
            if (Category != AssumptionCategory.BlackScholes)
                return base.GetValue(date, effectiveDate, inflationAdjustment);
            var currentInflation = notionalInflation + (Index.GetValue(date) - Index.GetValue(effectiveDate))*multiplyer;
            if (inflationAdjustment.HasValue)
                currentInflation += inflationAdjustment.Value;

            return Utils.GetBlackScholesValue(currentInflation, InflationVolatility, 1, Minimum, Maximum);
        }

        public double GetValue(DateTime date, DateTime effectiveDate, double? inflationAdjustment = null, double? notionalInflationOverride = null)
        {
            if (Category != AssumptionCategory.BlackScholes)
                return base.GetValue(date, effectiveDate, inflationAdjustment);
            var currentInflation = notionalInflationOverride.GetValueOrDefault(notionalInflation) + (Index.GetValue(date) - Index.GetValue(effectiveDate)) * multiplyer;
            if (inflationAdjustment.HasValue)
                currentInflation += inflationAdjustment.Value;

            return Utils.GetBlackScholesValue(currentInflation, InflationVolatility, 1, Minimum, Maximum);
        }

        public void RealignBlackScholes(double notionalInflation)
        {
            this.notionalInflation = notionalInflation;
        }

        public PensionIncreaseAssumption Clone()
        {
            return new PensionIncreaseAssumption(Id, BasisId, InitialValue, Index, ReferenceNumber, 
                                                Label, Minimum, Maximum, FixedIncrease, Category, 
                                                InflationVolatility, InflationType, IsFixed, IsVisible);
        }
    }
}