﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;

    class IndexService : IIndexService
    {
        public double GetGrowth(FinancialIndex index, DateTime start, DateTime end)
        {
            if (start < index.BeginDate || end > index.EndDate)
                return 0;
            else if (start == end)
                return 1;
            else
                return index.GetValue(end.SubtractToDate(index.BeginDate)).MathSafeDiv(index.GetValue(start.SubtractToDate(index.BeginDate)));
        }
    }
}
