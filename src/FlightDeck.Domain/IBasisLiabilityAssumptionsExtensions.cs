﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain
{
    static class IBasisLiabilityAssumptionsExtensions
    {
        public static IBasisLiabilityAssumptions Clone(this IBasisLiabilityAssumptions assumptions)
        {
            return new BasisLiabilityAssumptions
                (
                new Dictionary<AssumptionType, double>(assumptions.LiabilityAssumptions),
                assumptions.LifeExpectancy != null ? new LifeExpectancyAssumptions(assumptions.LifeExpectancy.PensionerLifeExpectancy, assumptions.LifeExpectancy.DeferredLifeExpectancy) : null,
                new Dictionary<int, double>(assumptions.PensionIncreases)
                );
        }
    }
}
