﻿using System;
namespace FlightDeck.Domain
{
    public interface IDbInteractionScope : IDisposable
    {
        FlightDeck.Domain.IRepository<T> GetRepository<T>() where T : class;
        int GetVersionNumber();
    }
}
