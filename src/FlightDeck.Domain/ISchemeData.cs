﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.LDI;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    public interface ISchemeData
    {
        IList<Contribution> ContributionRates { get; }
        double ContractingOutContributionRate { get; }
        IEnumerable<SchemeAsset> SchemeAssets { get; }
        IDictionary<DateTime, RecoveryPayment> RecoveryPlan { get; } 
        IEnumerable<RecoveryPayment> RecoveryPlanData { get; } //TODO Is there a difference between these two?
        IDictionary<AssetClassType, double> DefaultVolatilityAssumptions { get; }
        double Ias19ExpenseLoading { get; }
        SalaryType? ABOSalaryType { get; }
        IDictionary<NamedPropertyGroupType, NamedPropertyGroup> NamedProperties { get; }
        bool IsGrowthFixed { get; }
        double ReturnOnAssets { get; }
        bool AccountingIAS19Visible { get; }
        bool AccountingFRS17Visible { get; }
        bool AccountingUSGAAPVisible { get; }
        PensionSchemeApplicationParameters ApplicationParameters { get; }
        int GiltDurationDefault { get; }
        int CorpDurationDefault { get; }
        LDICashflowBasis LDICashflowBasis { get; }
        SyntheticAssetIndices SyntheticAssets { get; }
        bool Funded { get; }
        IBasesDataManager LDIBasesDataManager { get; }

        SchemeAsset GetSchemeAsset(DateTime at);
        ILDIBasisProvider GetLDIBasisProvider(DateTime at);
    }
}
