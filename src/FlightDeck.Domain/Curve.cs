﻿namespace FlightDeck.Domain
{
    using System.Collections.Generic;
    
    class Curve
    {
        public IDictionary<int, double> Spot { get; private set; }
        public IDictionary<int, double> Forward { get; set; }

        public Curve(int capacity)
        {
            Spot = new Dictionary<int, double>();
            Forward = new Dictionary<int, double>();

            for (var i = 0; i < capacity; i++)
            {
                Spot.Add(i + 1, 0);
                Forward.Add(i + 1, 0);
            }
        }
        public Curve(IDictionary<int, double> spot, IDictionary<int, double> forward)
        {
            Spot = spot;
            Forward = forward;
        }
    }
}
