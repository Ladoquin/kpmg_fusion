﻿namespace FlightDeck.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class MasterBasisQueries
    {
        public IEnumerable<DateTime> GetAccountingPeriods(DateTime schemeRefreshDate, MasterBasis m, DateTime? optionalEndDate = null)
        {
            var effectiveDates = m.Bases.Select(x => x.Key).OrderBy(x => x);

            while (effectiveDates.Last().AddYears(1) < schemeRefreshDate)
            {
                effectiveDates = effectiveDates.Union(new List<DateTime> { effectiveDates.Last().AddYears(1) }).OrderBy(x => x);
            }

            if (optionalEndDate.HasValue)
            {
                effectiveDates = effectiveDates.Except(effectiveDates.Where(d => d > optionalEndDate.Value)).OrderBy(x => x);

                if (!effectiveDates.Contains(optionalEndDate.Value))
                {
                    effectiveDates = effectiveDates.Union(new List<DateTime> { optionalEndDate.Value }).OrderBy(x => x);
                }
            }

            return effectiveDates;
        }
    }
}
