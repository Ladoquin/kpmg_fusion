﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Evolution;
    using FlightDeck.Domain.JourneyPlan;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class NewJP
    {
        private const bool CheckSpotRates = false;

        private readonly ISchemeContext scheme;
        private readonly IBasisContext currentBasis;
        private List<CashflowData> AdjCashflows;
        private IDictionary<AssumptionType, double> oldLiabilityAssumptions;
        private IDictionary<int, double> oldPensionIncreases;
        private IDictionary<BasisType, BeforeAfter<double>> DefaultPremiums;
        private IDictionary<int, IDictionary<CurveDataType, double>> curveTable;
        private Curve giltYield;
        private Curve rpi;
        private Curve cpi;
        private CurvedBasis cashflowCurveBasis;
        private IDictionary<int, double> cashflowSalary;
        private IDictionary<int, NewJPTrigger> triggers;
        private int nextTriggerIndex;
        private Curve investmentGrowth;
        private JourneyPlanAfterResult.EFROImpact eFroImpact;
        private Dictionary<int, Dictionary<MemberStatus, double>> eFROProjLiabs;

        public NewJP(ISchemeContext scheme, IBasisContext currentBasis)
        {
            this.scheme = scheme;
            this.currentBasis = currentBasis;
            this.DefaultPremiums = new Dictionary<BasisType, BeforeAfter<double>>();
            this.triggers = new Dictionary<int, NewJPTrigger>();
            this.eFROProjLiabs = null;
        }

        public NewJPData Calculate(NewJPParameters parms)
        {
            LoadCashflows();
            LoadOldBasis();
            LoadNewBasis(parms.UseCurves, parms.CurveDataProjection);
            ChangeBasis(parms.SalaryInflationType);
            GroupCashflowData();

            LoadSalary(parms.SalaryInflationType);
            LoadTriggers(parms);

            var bases = GetTargetBases(parms);

            bool includeRP = parms.RecoveryPlan.AddCurrentRecoveryPlan;
            bool includeXC = parms.RecoveryPlan.AddExtraContributions;

            var liabResults = new IDictionary<int, double>[3];
            IDictionary<int, double> assets1 = null;
            IDictionary<int, double> assets2 = null;
            IDictionary<BasisType, int> currentCrossOver = new Dictionary<BasisType, int>();
            IDictionary<BasisType, int> newCrossOver = new Dictionary<BasisType, int>();
            IDictionary<string, NPV> knownLiabs = new Dictionary<string, NPV>();

            CurvedBasis cb = null;
            var liabResultIdx = 0;
            foreach (var bkey in bases.Keys)
            {
                var b = bases[bkey];

                var basis = b;

                //Only basis 1 can be a TP basis (flat assumptions here)
                var isTP = bkey == 1 && basis.Data.Type == BasisType.TechnicalProvision;

                if (knownLiabs.ContainsKey(b.Data.DisplayName) == false)
                    knownLiabs.Add(b.Data.DisplayName, LoadKnownPV(basis, parms.AnalysisDate));
                var knownLiab = knownLiabs[b.Data.DisplayName];
                cb = CalibrateToKnownPV(basis, cb, knownLiab, bkey == 2);

                if (bkey == 2) // Tracker explicitly referencing the "2nd basis"
                {
                    cb.DiscPre = GetGiltsPlus(cb.GiltYield, parms.SelfSufficiencyDiscountRate);
                    cb.DiscPost = cb.DiscPre;
                    cb.DiscPen = cb.DiscPre;
                }
                else
                {
                    CalculateDefaultPremium(cb, isTP, knownLiab);
                }

                // Run calcs on current basis and assets in the "before" setup (no triggers, existing RP only)
                IDictionary<int, BalanceData> totals = Project(parms, basis, cb, false, true, false);
                if (bkey == 1) // Tracker referencing ASSET_BASIS of 1
                    assets1 = totals.ToDictionary(x => x.Key, x => x.Value.Assets);

                liabResults[liabResultIdx++] = totals.ToDictionary(x => x.Key, x => x.Value.Liabilities);

                // On the chosen basis create the assets "after" (all user options enabled)
                if (bkey == 1) // Tracker referencing ASSET_BASIS of 1
                {
                    totals = Project(parms, basis, cb, true, includeRP, includeXC);
                    assets2 = totals.ToDictionary(x => x.Key, x => x.Value.Assets);
                }
            }

            // crossover points - the liabs are hardcoded as per their index. i.e. 0 = funding, 1 = self-suff and 2 = buyout
            liabResultIdx = 0;
            foreach (var b in bases.Values)
            {
                BasisType bType = BasisType.TechnicalProvision;
                switch (liabResultIdx)
                {
                    case 0:
                        bType = BasisType.TechnicalProvision;
                        break;
                    case 1:
                        bType = BasisType.SelfSufficiency;
                        break;
                    case 2:
                        bType = BasisType.Buyout;
                        break;
                }
                if (!currentCrossOver.ContainsKey(bType) && assets1 != null)
                    currentCrossOver.Add(bType,
                                    GetCrossOverPoint(new SortedList<int, double>(assets1),
                                                      new SortedList<int, double>(liabResults[liabResultIdx])));
                if (!newCrossOver.ContainsKey(bType) && assets2 != null)
                    newCrossOver.Add(bType,
                                    GetCrossOverPoint(new SortedList<int, double>(assets2),
                                                      new SortedList<int, double>(liabResults[liabResultIdx])));
                liabResultIdx++;
            }

            var triggerYears = triggers == null || triggers.Count == 0
                ? new List<int>()
                : triggers.Where(x => x.Key > 0).Select(x => x.Value.TimeInterval);

            return new NewJPData(liabResults[0], liabResults[1], liabResults[2], assets1, assets2, DefaultPremiums, currentCrossOver, newCrossOver, triggerYears);
        }

        private IDictionary<int, BalanceData> Project(NewJPParameters parms, IBasisContext basis, CurvedBasis cb, bool enableTrigger, bool includeRecoveryPlan, bool includeExtraConts)
        {
            int yMax = 50; //max projection period
            double insPenAssets = currentBasis.Results.Insurance.PensionerLiabilityToBeInsured;
            double insNonPenAssets = currentBasis.Results.Insurance.NonPensionerLiabilityToBeInsured;
            double assetsAtEnd = 0;
            double assetsAtStart;
            double totalAssets;
            double totalLiabs;
            double oldBuyin = 0;
            double newBuyin = 0;
            double penLiabInsured = 0;
            double nonPenLiabInsured = 0;
            double eeConts = 0;
            double erConts = 0;
            double extraConts = 0;
            double recoveryPlanConts = 0;
            NPV benefits;
            double outgoingBenefits = 0;
            double insIncome = 0;
            double netIncome = 0;
            double growth = 0;

            //from LoadOther() in VBA.. we have to call this in Project to reset assetsAtEnd for existing and new assets
            if (parms.IncludeBuyIns)
            {
                penLiabInsured = scheme.Assumptions.InsuranceOptions.PensionerLiabilityPercInsured;
                nonPenLiabInsured = scheme.Assumptions.InsuranceOptions.NonpensionerLiabilityPercInsured;
                assetsAtEnd = currentBasis.Results.InvestmentStrategy.TotalAssets - currentBasis.Results.InvestmentStrategy.Buyin;
            }
            else
            {
                penLiabInsured = 0;
                nonPenLiabInsured = 0;
                assetsAtEnd = currentBasis.Results.InvestmentStrategy.TotalAssets + scheme.Calculated.BuyinCost.Total
                    - (currentBasis.Results.UserDefBuyIn + currentBasis.Results.UserPenBuyIn + insPenAssets + insNonPenAssets);
            }


            var TotalData = new Dictionary<int, BalanceData>();
            investmentGrowth = GetGiltsPlus(cb.GiltYield, enableTrigger ? triggers[0].Growth : parms.InvGrowthRateBefore);
            NPV pv;

            for (int y = 0; y <= yMax; y++)
            {
                assetsAtStart = assetsAtEnd;

                pv = GetPresentValue(y, cb, true);

                if (eFROProjLiabs != null)
                {
                    pv.Liabilities[MemberStatus.ActivePast] += eFROProjLiabs.ContainsKey(y) && eFROProjLiabs[y].ContainsKey(MemberStatus.ActivePast)
                                                                ? eFROProjLiabs[y][MemberStatus.ActivePast]
                                                                : 0;
                    pv.Liabilities[MemberStatus.Deferred] += eFROProjLiabs.ContainsKey(y) && eFROProjLiabs[y].ContainsKey(MemberStatus.Deferred)
                                                                ? eFROProjLiabs[y][MemberStatus.Deferred]
                                                                : 0;
                }

                if (parms.IncludeBuyIns)
                {
                    totalLiabs = pv.Liabilities[MemberStatus.ActivePast] + pv.Liabilities[MemberStatus.Deferred] + pv.Liabilities[MemberStatus.Pensioner];
                    oldBuyin = pv.Buyin[MemberStatus.Deferred] + pv.Buyin[MemberStatus.Pensioner];
                    newBuyin = (nonPenLiabInsured * (pv.Liabilities[MemberStatus.Deferred] - pv.Buyin[MemberStatus.Deferred]))
                                + (penLiabInsured * (pv.Liabilities[MemberStatus.Pensioner] - pv.Buyin[MemberStatus.Pensioner]));
                    totalAssets = assetsAtEnd + oldBuyin + newBuyin;
                }
                else
                {
                    totalLiabs = (pv.Liabilities[MemberStatus.ActivePast] + pv.Liabilities[MemberStatus.Deferred] + pv.Liabilities[MemberStatus.Pensioner])
                        - (pv.Buyin[MemberStatus.Deferred] + pv.Buyin[MemberStatus.Pensioner]);
                    oldBuyin = 0;
                    newBuyin = 0;
                    totalAssets = assetsAtEnd;
                }

                if (y > 0)
                {
                    eeConts = 0;
                    erConts = 0;
                    if (includeExtraConts)
                        extraConts = GetExtraContributions(y, parms.RecoveryPlan);
                    if (includeRecoveryPlan)
                        recoveryPlanConts = GetRecoveryPlanContributions(y);

                    benefits = GetTotalCashFlow(y);

                    if (parms.IncludeBuyIns)
                    {
                        outgoingBenefits = benefits.Liabilities[MemberStatus.ActivePast] + benefits.Liabilities[MemberStatus.Deferred] + benefits.Liabilities[MemberStatus.Pensioner];
                        insIncome = benefits.Buyin[MemberStatus.Deferred]
                            + (nonPenLiabInsured * (benefits.Liabilities[MemberStatus.Deferred] - benefits.Buyin[MemberStatus.Deferred]))
                            + benefits.Buyin[MemberStatus.Pensioner]
                            + (penLiabInsured * (benefits.Liabilities[MemberStatus.Pensioner] - benefits.Buyin[MemberStatus.Pensioner]));
                    }
                    else
                    {
                        outgoingBenefits = (benefits.Liabilities[MemberStatus.ActivePast] + benefits.Liabilities[MemberStatus.Deferred] + benefits.Liabilities[MemberStatus.Pensioner])
                            - (benefits.Buyin[MemberStatus.Deferred] + benefits.Buyin[MemberStatus.Pensioner]);
                        insIncome = 0;
                    }

                    if (eFroImpact != null)
                        outgoingBenefits += eFroImpact.AdjustedActiveTransferValuesPayable[y] + eFroImpact.AdjustedDeferredTransferValuesPayable[y];

                    netIncome = eeConts + erConts + recoveryPlanConts + extraConts + insIncome - outgoingBenefits;

                    growth = investmentGrowth.Forward[y];

                    assetsAtEnd = (assetsAtStart * (1 + growth))
                                  + (netIncome * (1 + growth / 2));

                    totalAssets = assetsAtEnd + oldBuyin + newBuyin;

                    if (enableTrigger)
                        ApplyFundingTrigger(y, totalAssets / totalLiabs, cb.GiltYield);
                }

                NewJPLiabilityOutput liabOutput = null;

                if (parms.IncludeBuyIns)
                {
                    liabOutput = new NewJPLiabilityOutput(pv.Liabilities[MemberStatus.ActivePast], pv.Liabilities[MemberStatus.Deferred],
                                                 pv.Liabilities[MemberStatus.Pensioner], pv.Liabilities[MemberStatus.ActiveFuture],
                                                 y * pv.Liabilities[MemberStatus.ActiveFuture], pv.Buyin[MemberStatus.Deferred], pv.Buyin[MemberStatus.Pensioner]);
                }
                else
                {
                    //todo: not sure whats happening here...liabOutput isn't used?
                }


                NewJPAssetOutput assetOutput = new NewJPAssetOutput(assetsAtStart, eeConts, erConts, recoveryPlanConts, extraConts, insIncome, outgoingBenefits, netIncome, growth, assetsAtEnd);

                //LiabilityOutputs.Add(y, liabOutput);
                //AssetOutputs.Add(y, assetOutput);
                TotalData.Add(y, new BalanceData(totalAssets, totalLiabs));
            }

            return TotalData;
        }

        private double GetEmployeeContributions(int yearNo)
        {
            return cashflowSalary[yearNo] * scheme.Data.ContributionRates[0].Employee;
        }

        private double GetEmployerContributions(int yearNo)
        {
            return cashflowSalary[yearNo] * scheme.Data.ContributionRates[0].Employer;
        }

        private double GetExtraContributions(int yearNo, NewJPExtraContributionOptions rpOptions)
        {
            int endYear = rpOptions.StartYear + rpOptions.Term - 1;
            if (yearNo < rpOptions.StartYear || yearNo > endYear)
                return 0;
            else
                return rpOptions.AnnualContributions * Math.Pow((1 + rpOptions.AnnualIncrements), yearNo - rpOptions.StartYear);
        }

        private double GetRecoveryPlanContributions(int yearNo)
        {
            bool isCurrentPlan = scheme.Assumptions.RecoveryPlanOptions == RecoveryPlanType.Current;
            if (currentBasis.Results.RecoveryPlanPayments.Count >= yearNo)
                return isCurrentPlan
                    ? currentBasis.Results.RecoveryPlanPayments.Values.ToList()[yearNo - 1].Before
                    : currentBasis.Results.RecoveryPlanPayments.Values.ToList()[yearNo - 1].After;
            else
                return 0;
        }

        private NPV GetTotalCashFlow(int y)
        {
            NPV tfc = new NPV();
            foreach (var cf in AdjCashflows)
            {
                if (cf.IsBuyIn && cf.Cashflow.ContainsKey(y))
                    tfc.Buyin[cf.MemberStatus] += cf.Cashflow[y];
                else if (cf.Cashflow.ContainsKey(y))
                    tfc.Liabilities[cf.MemberStatus] += cf.Cashflow[y];
            }

            return tfc;
        }

        private void ApplyFundingTrigger(int year, double assetsLiabPropotion, Curve giltYield)
        {
            if (!triggers.Any() || nextTriggerIndex > triggers.Keys.Max())
                return;

            if ((triggers[nextTriggerIndex].TriggerType == NewJPTriggerType.FundingLevel && assetsLiabPropotion >= triggers[nextTriggerIndex].FundingLevel)
                || (triggers[nextTriggerIndex].TriggerType == NewJPTriggerType.TimeBased && year == triggers[nextTriggerIndex].TimeInterval))
            {
                investmentGrowth = GetGiltsPlus(giltYield, triggers[nextTriggerIndex].Growth);
                nextTriggerIndex++;
            }
        }

        /// <summary>
        /// Convert BofE projection to curved data table.
        /// </summary>
        /// <param name="data"></param>
        private void loadCurveTable(DateTime from, CurveDataProjection data)
        {
            curveTable = new Dictionary<int, IDictionary<CurveDataType, double>>(Utils.MaxYear);

            var cpiAdjustment = currentBasis.Assumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex] - currentBasis.Assumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex];

            var lastKnownGiltSpotRate = data.SpotRates.Last().Rate;

            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                var giltSpotRate = lastKnownGiltSpotRate;
                if (data.SpotRates.Any(x => x.Maturity == y))
                {
                    giltSpotRate = data.SpotRates.Single(x => x.Maturity == y).Rate;
                    lastKnownGiltSpotRate = giltSpotRate;
                }

                curveTable.Add(y,
                    new Dictionary<CurveDataType, double>
                        {
                            { CurveDataType.Gilt, giltSpotRate },
                            { CurveDataType.RPI, currentBasis.Assumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex] },
                            { CurveDataType.CPI, currentBasis.Assumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex] - cpiAdjustment }
                        });
            }
        }

        private void LoadCashflows()
        {
            AdjCashflows = currentBasis.Results.AdjustedCashflow.Select(x => x.Clone()).ToList();
        }

        private void LoadOldBasis()
        {
            oldLiabilityAssumptions = currentBasis.Assumptions.LiabilityAssumptions;
            oldPensionIncreases = currentBasis.Assumptions.PensionIncreases;
        }

        private void LoadNewBasis(bool useCurves, CurveDataProjection curveDataProjection = null)
        {
            CurvedBasis cb = null;

            var pincDef = currentBasis.Data.UnderlyingPensionIncreaseIndices;

            if (useCurves)
            {
                loadCurveTable(currentBasis.AnalysisDate, curveDataProjection);

                Func<CurveDataType, Curve> loadcurve = type =>
                    {
                        var spot = curveTable.ToDictionary(x => x.Key, x => x.Value[type]);
                        var forward = GetSpotToForward(spot);
                        return new Curve(spot, forward);
                    };

                giltYield = loadcurve(CurveDataType.Gilt);
                rpi = loadcurve(CurveDataType.RPI);
                cpi = loadcurve(CurveDataType.CPI);

                cb = new CurvedBasis
                {
                    GiltYield = giltYield,
                    RPI = rpi,
                    CPI = cpi,
                    Pinc = pincDef.ToDictionary(x => x.Key, x => new Curve(Utils.MaxYear))
                };

                for (int i = 1; i < pincDef.Count; i++)
                {
                    IDictionary<int, double> inf = null;
                    switch (pincDef[i].InflationType)
                    {
                        case InflationType.Cpi:
                            inf = cb.CPI.Forward;
                            break;
                        case InflationType.Rpi:
                            inf = cb.RPI.Forward;
                            break;
                        default:
                            break;
                    }

                    if (inf != null)
                    {
                        for (int y = 1; y <= Utils.MaxYear; y++)
                        {
                            cb.Pinc[i].Forward[y] = Math.Max(pincDef[i].Minimum, Math.Min(pincDef[i].Maximum, inf[y]));
                        }
                    }
                }
            }
            else
            {
                var jpGiltYield = new GiltYieldService(currentBasis.Data).GetGiltYield(currentBasis.AnalysisDate);
                giltYield = GetCurveFromFlatRate(jpGiltYield);
                cb = new CurvedBasis
                {
                    GiltYield = giltYield,
                    RPI = GetCurveFromFlatRate(currentBasis.Assumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex]),
                    CPI = GetCurveFromFlatRate(currentBasis.Assumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex]),
                    Pinc = currentBasis.Assumptions.PensionIncreases.ToDictionary(x => x.Key, x => GetCurveFromFlatRate(x.Value))
                };
            }

            cashflowCurveBasis = cb;
        }

        private void ChangeBasis(SalaryType salaryInflationType)
        {
            var futCount = 0;
            var mortImpact = .0;
            var isCpiRev = false;
            var isRpiRev = false;

            var adjusted = new List<CashflowData>(AdjCashflows.Count);

            foreach (var cf in AdjCashflows)
            {
                var oldCf1 = new Dictionary<int, double>(Utils.MaxYear);
                var newCf1 = new Dictionary<int, double>(Utils.MaxYear);
                var oldCf2 = new Dictionary<int, double>(Utils.MaxYear);
                var newCf2 = new Dictionary<int, double>(Utils.MaxYear);
                var totCf2 = new Dictionary<int, double>(Utils.MaxYear);

                for (int i = 1; i < 101; i++)
                {
                    oldCf1.Add(i, 0.0);
                    newCf1.Add(i, 0.0);
                    oldCf2.Add(i, 0.0);
                    newCf2.Add(i, 0.0);
                    totCf2.Add(i, 0.0);
                }

                var oldPinc = .0;
                IDictionary<int, double> newPinc = null;

                if (cf.MemberStatus == MemberStatus.ActiveFuture)
                    futCount++;

                if (cf.MemberStatus == MemberStatus.Deferred)
                {
                    isCpiRev = cf.RevaluationType == RevaluationType.None || cf.RevaluationType == RevaluationType.Cpi;
                    isRpiRev = cf.RevaluationType == RevaluationType.Rpi;
                }

                if (cf.BeginYear > 0)
                {
                    if (cf.PensionIncreaseReference > 0)
                    {
                        oldPinc = oldPensionIncreases[cf.PensionIncreaseReference];
                        newPinc = cashflowCurveBasis.Pinc[cf.PensionIncreaseReference].Forward;
                    }

                    for (int y = cf.BeginYear; y <= cf.EndYear; y++)
                    {
                        var adjCpi = Math.Pow((1 + cashflowCurveBasis.CPI.Spot[y]) / (1 + oldLiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex]), y - 0.5);
                        var adjRpi = Math.Pow((1 + cashflowCurveBasis.RPI.Spot[y]) / (1 + oldLiabilityAssumptions[AssumptionType.InflationRetailPriceIndex]), y - 0.5);
                        var adjSal = salaryInflationType == SalaryType.CPI ? adjCpi : adjRpi;

                        if (cf.BenefitType == BenefitType.LumpSum)
                        {
                            if (cf.MemberStatus == MemberStatus.Deferred && isCpiRev)
                            {
                                cf.Cashflow[y] *= adjCpi;
                            }
                            else if (cf.MemberStatus == MemberStatus.Deferred && isRpiRev)
                            {
                                cf.Cashflow[y] *= adjRpi;
                            }
                            else if (cf.MemberStatus == MemberStatus.Deferred)
                            {
                            }
                            else
                            {
                                cf.Cashflow[y] *= adjSal;
                            }
                        }
                        else if (cf.MemberStatus == MemberStatus.Pensioner || cf.MinAge >= cf.RetirementAge)
                        {
                            //New treatment, allowing a "new" part to exist (albeit ignoring pre-ret indexation)
                            //and dealing with zeroes in early years in a more robust way.
                            mortImpact = 1;
                            if (y == 1)
                            {
                                oldCf1[y] = cf.Cashflow[y];
                                newCf1[y] = 0;
                                oldCf2[y] = oldCf1[y] * Math.Pow((1 + newPinc[y]) / (1 + oldPinc), 0.5);
                            }
                            else
                            {
                                oldCf1[y] = Math.Min(cf.Cashflow[y], cf.Cashflow[y - 1] * (1 + oldPinc));
                                newCf1[y] = cf.Cashflow[y] - oldCf1[y];
                                if (oldCf1[y - 1] > 0)
                                {
                                    mortImpact = oldCf1[y] / (oldCf1[y - 1] * (1 + oldPinc));
                                }
                                oldCf2[y] = oldCf2[y - 1] * (1 + newPinc[y]) * mortImpact;
                            }
                            newCf2[y] = newCf1[y];
                            cf.Cashflow[y] = oldCf2[y] + newCf2[y];
                        }
                        else
                        {
                            if (y == cf.BeginYear)
                            {
                                newCf1[y] = cf.Cashflow[y];
                                oldCf2[y] = 0;
                            }
                            else
                            {
                                oldCf1[y] = Math.Min(cf.Cashflow[y], cf.Cashflow[y - 1] * (1 + oldPinc));
                                newCf1[y] = cf.Cashflow[y] - oldCf1[y];
                            }

                            if (y > cf.BeginYear && oldCf1[y] > 0 && newCf1[y] == 0)
                            {
                                mortImpact = cf.Cashflow[y] / (cf.Cashflow[y - 1] * (1 + oldPinc));
                            }
                            else
                            {
                                mortImpact = 1;
                            }

                            if (oldCf1[y] == 0)
                            {
                                oldCf2[y] = 0;
                            }
                            else
                            {
                                oldCf2[y] = totCf2[y - 1] * (1 + newPinc[y]) * mortImpact;
                            }
                            if (newCf1[y] == 0)
                            {
                                newCf2[y] = 0;
                            }
                            else if (cf.MemberStatus == MemberStatus.Deferred && isCpiRev)
                            {
                                newCf2[y] = newCf1[y] * adjCpi;
                            }
                            else if (cf.MemberStatus == MemberStatus.Deferred && isRpiRev)
                            {
                                newCf2[y] = newCf1[y] * adjRpi;
                            }
                            else if (cf.MemberStatus == MemberStatus.Deferred)
                            {
                                newCf2[y] = newCf1[y];
                            }
                            else if (cf.MemberStatus == MemberStatus.ActivePast || cf.MemberStatus == MemberStatus.ActiveFuture)
                            {
                                newCf2[y] = newCf1[y] * adjSal;
                            }
                            totCf2[y] = oldCf2[y] + newCf2[y];

                            cf.Cashflow[y] = totCf2[y];
                        }
                    }
                }

                adjusted.Add(cf);
            }

            AdjCashflows = adjusted;
        }

        private void GroupCashflowData()
        {
            var groupedCf = new List<CashflowData>();
            var bandSize = 5;

            Func<CashflowData, CashflowData> matchingGroupedData = cashflow =>
            {
                return groupedCf.FirstOrDefault(x =>
                        x.MemberStatus == cashflow.MemberStatus &&
                        x.BenefitType == cashflow.BenefitType &&
                        x.PensionIncreaseReference == cashflow.PensionIncreaseReference &&
                        x.IsBuyIn == cashflow.IsBuyIn &&
                        x.MinAge == cashflow.MinAge &&
                        x.MaxAge == cashflow.MaxAge
                    );
            };

            foreach (var cf in AdjCashflows)
            {
                if (cf.BeginYear > 0)
                {
                    //Adjust the min/max age for the given age band size
                    cf.MinAge = bandSize * Math.Floor(cf.MinAge / bandSize);
                    var bandMax = bandSize * Math.Floor(cf.MaxAge / bandSize);
                    cf.MaxAge = bandMax + (bandMax < cf.MaxAge ? bandSize : 0);

                    var matchedCashflow = matchingGroupedData(cf);
                    //Group it or create a new entry in GroupedCf() as required
                    if (matchedCashflow != null)
                    {
                        for (int y = cf.BeginYear; y <= cf.EndYear; y++)
                        {
                            matchedCashflow.Cashflow[y] = matchedCashflow.Cashflow[y] + cf.Cashflow[y];
                        }
                        matchedCashflow.CalibrateBeginAndEndYear();
                    }
                    else
                        groupedCf.Add(cf);
                }
            }

            AdjCashflows = groupedCf;
        }

        private void LoadSalary(SalaryType salaryInflationType)
        {
            if (salaryInflationType == SalaryType.Zero)
                salaryInflationType = SalaryType.CPI;

            cashflowSalary = new Dictionary<int, double>(Utils.MaxYear);

            var oldSalary = new Dictionary<int, double>(Utils.MaxYear);
            for (int y = 1; y <= Utils.MaxYear; y++)
                oldSalary.Add(y, currentBasis.Data.SalaryProgression.Sum(sp => sp.SalaryRoll.ContainsKey(y) ? sp.SalaryRoll[y] : 0));

            var timeAdj = (currentBasis.AnalysisDate - currentBasis.Data.EffectiveDate).TotalDays / Utils.DaysInAYear;
            var addYear = (int)Math.Floor(timeAdj);
            var partYear = timeAdj - addYear;
            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                cashflowSalary.Add(y, 0);
                var oldYear = y + addYear;
                if (oldYear > 0 && oldYear <= Utils.MaxYear)
                    cashflowSalary[y] = oldSalary[oldYear] * (1 - partYear);
                if (oldYear > 0 && oldYear < Utils.MaxYear)
                    cashflowSalary[y] = cashflowSalary[y] + oldSalary[oldYear + 1] * partYear;
            }

            var oldInf = salaryInflationType == SalaryType.RPI ? currentBasis.Data.Assumptions[AssumptionType.InflationRetailPriceIndex] : currentBasis.Data.Assumptions[AssumptionType.InflationConsumerPriceIndex];
            var newInf = salaryInflationType == SalaryType.RPI ? cashflowCurveBasis.RPI.Spot : cashflowCurveBasis.CPI.Spot;
            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                cashflowSalary[y] = cashflowSalary[y] * Math.Pow((1 + newInf[y]) / (1 + oldInf), y - 0.5);
            }
        }

        private void LoadTriggers(NewJPParameters parms)
        {
            nextTriggerIndex = 1;
            bool isFundingLevelTrigger = parms.FundingLevelTrigger;
            int triggerStepsCount = parms.TriggerSteps;
            double initialGrowth = parms.InitialGrowthRate;
            double targetGrowth = parms.TargetGrowthRate;
            double fundingLevel = parms.FundingLevel;
            int timeStartYear = parms.TriggerTimeStartYear;
            int timeEndYear = parms.TriggerTransitionPeriod;
            int totalTimeYears = timeStartYear + timeEndYear;
            double targetFundingLevel = 1.0;
            double multiplier = 0;

            triggers.Add(0, new NewJPTrigger(isFundingLevelTrigger ? NewJPTriggerType.FundingLevel : NewJPTriggerType.TimeBased, initialGrowth));

            if (triggerStepsCount == 1)
            {
                if (isFundingLevelTrigger)
                    triggers.Add(1, new NewJPTrigger(NewJPTriggerType.FundingLevel, targetGrowth, fundingLevel, 0));
                else
                    triggers.Add(1, new NewJPTrigger(NewJPTriggerType.TimeBased, targetGrowth, 0, timeStartYear));
            }
            else
            {
                for (int i = 1; i <= triggerStepsCount; i++)
                {
                    multiplier = ((double)i - 1) / (triggerStepsCount - 1);
                    var growth = initialGrowth + ((targetGrowth - initialGrowth) * ((double)i / triggerStepsCount));
                    if (isFundingLevelTrigger)
                    {
                        var funding = fundingLevel + ((targetFundingLevel - fundingLevel) * multiplier);
                        triggers.Add(i, new NewJPTrigger(NewJPTriggerType.FundingLevel, growth, funding, 0));
                    }
                    else
                    {
                        int timeInterval = (int)Math.Truncate(timeStartYear + ((totalTimeYears - timeStartYear) * multiplier) + 0.5);
                        triggers.Add(i, new NewJPTrigger(NewJPTriggerType.TimeBased, growth, 0, timeInterval));
                    }
                }
            }

        }

        private IDictionary<int, IBasisContext> GetTargetBases(NewJPParameters parms)
        {
            var bases = new Dictionary<int, IBasisContext>
                {
                    { 1, parms.FundingBasis }
                };
            if (parms.SelfSufficiencyBasis != null) bases.Add(2, parms.SelfSufficiencyBasis);
            if (parms.BuyoutBasis != null) bases.Add(3, parms.BuyoutBasis);

            return bases;
        }

        private NPV LoadKnownPV(IBasisContext ctx, DateTime at)
        {
            var adjCtx = ctx.AdjustData(pensionIncreases: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToPensionIncreasesDictionary());

            var lcf = new LiabilityCashflowStatefulService(adjCtx);

            lcf.AdjustTo(at, ctx.Assumptions, true);

            if (scheme.Assumptions.FROType == FROType.Bulk)
            {
                lcf.ApplyFRO(scheme.Assumptions.FROOptions);
                lcf.ApplyETV(scheme.Assumptions.ETVOptions);
                lcf.ApplyBenefitChanges(scheme.Assumptions.FutureBenefitOptions);
            }
            else if (scheme.Assumptions.FROType == FROType.Embedded)
            {
                eFroImpact = lcf.ApplyEmbeddedFRO(scheme.Assumptions.EFROOptions);

                var jpCalcService = new JourneyPlanSchemeManagedService(scheme, ctx.Results);

                if (eFroImpact != null)
                    jpCalcService.ApplyEmbeddedFROTimeZeroCETVAdjustment(eFroImpact);

                var preDiscRate = ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate];
                eFROProjLiabs = jpCalcService.ProjectedLiabilities(preDiscRate, eFroImpact);
            }

            lcf.ApplyPIE(scheme.Assumptions.PIEOptions, currentBasis.Results.PIEInit, ctx.Assumptions.PensionIncreases);

            var npv = lcf.GetPresentValue(at, ctx.Assumptions);

            return npv;
        }

        private NPV GetPresentValue(int year, CurvedBasis cb, bool applyCalibration)
        {
            var npv = new NPV();

            var yVal = 1 + year;

            var PartYear = new Dictionary<int, double>(Utils.MaxYear);

            double minDisc = 1000;
            double maxDisc = -1000;

            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                if (y < yVal)
                    PartYear.Add(y, 0);
                else if (y == yVal)
                    PartYear.Add(y, yVal - year);
                else
                    PartYear.Add(y, 1);

                minDisc = Math.Min(Math.Min(minDisc, cb.DiscPre.Forward[y]), Math.Min(cb.DiscPost.Forward[y], cb.DiscPen.Forward[y]));
                maxDisc = Math.Max(Math.Max(maxDisc, cb.DiscPre.Forward[y]), Math.Max(cb.DiscPost.Forward[y], cb.DiscPen.Forward[y]));
            }

            minDisc -= 0.00001;
            maxDisc += 0.00001;

            foreach (var cf in AdjCashflows)
            {
                var DCF = new Dictionary<int, double>(Utils.MaxYear);
                var DiscFac = new Dictionary<int, double>(Utils.MaxYear);
                var yBreach = 0;

                for (int y = 1; y <= Utils.MaxYear; y++)
                {
                    DCF.Add(y, 0);
                    DiscFac.Add(y, 1);

                    if (!cf.Cashflow.ContainsKey(y) || cf.Cashflow[y] == 0 || y < yVal || y > Utils.MaxYear)
                    {
                    }
                    else if (cf.MemberStatus == MemberStatus.Pensioner || cf.MinAge >= cf.RetirementAge)
                    {
                        DiscFac[y] = Math.Pow(1 + cb.DiscPen.Spot[yVal], year) / Math.Pow(1 + cb.DiscPen.Spot[y], y - PartYear[y] / 2);
                        DCF[y] = PartYear[y] * cf.Cashflow[y] * DiscFac[y];
                    }
                    else if (y == 1 || cf.BenefitType == BenefitType.LumpSum)
                    {
                        DiscFac[y] = Math.Pow(1 + cb.DiscPre.Spot[yVal], year) / Math.Pow(1 + cb.DiscPre.Spot[y], y - PartYear[y] / 2);
                        DCF[y] = PartYear[y] * cf.Cashflow[y] * DiscFac[y];
                    }
                    else
                    {
                        var oldpart = Math.Min((cf.Cashflow.ContainsKey(y - 1) ? cf.Cashflow[y - 1] : 0) * (1 + currentBasis.Assumptions.PensionIncreases[cf.PensionIncreaseReference]), cf.Cashflow[y]); //TODO reference to GetClientPensionIncreases() - refactor
                        var newpart = cf.Cashflow[y] - oldpart;

                        if (oldpart == 0)
                        {
                        }
                        else if (y == yVal)
                        {
                            oldpart = oldpart / Math.Pow(1 + cb.DiscPost.Forward[yVal], PartYear[y] / 2);
                        }
                        else
                        {
                            oldpart = oldpart * DiscFac[y - 1] / (Math.Pow(1 + cb.DiscPost.Forward[y - 1], PartYear[y - 1] / 2) * Math.Pow(1 + cb.DiscPost.Forward[y], PartYear[y] / 2));
                        }

                        if (newpart > 0)
                            newpart = newpart * Math.Pow(1 + cb.DiscPre.Spot[yVal], year) / Math.Pow(1 + cb.DiscPre.Spot[y], y - PartYear[y] / 2);

                        DiscFac[y] = (oldpart + newpart) / cf.Cashflow[y];
                        DCF[y] = PartYear[y] * (oldpart + newpart);
                    }

                    if (PartYear[y] > 0 && (DCF[y] == 0 ^ (!cf.Cashflow.ContainsKey(y) || cf.Cashflow[y] == 0)))
                        throw new Exception("Cashflow itself must either both be zero or both non-zero");

                    if (DiscFac[y] < 0.00000001 || DiscFac[y] > 2)
                        throw new Exception(string.Format("Unexpected DiscFac value: '{0}'", DiscFac[y].ToString()));

                    if (!CheckSpotRates || yBreach > 0)
                    {
                    }
                    else if (DCF[y] != 0)
                    {
                        var impliedDisc = Math.Exp(-Math.Log(DiscFac[y]) / y - 0.5) - 1;
                        if (impliedDisc < minDisc || impliedDisc > maxDisc)
                        {
                            yBreach = y;
                        }
                    }

                    if (!cf.IsBuyIn)
                        npv.Liabilities[cf.MemberStatus] += DCF[y];
                    else
                        npv.Buyin[cf.MemberStatus] += DCF[y];
                }
            }

            if (npv.Buyin[MemberStatus.ActivePast] != 0 || npv.Buyin[MemberStatus.ActiveFuture] != 0)
                throw new Exception("Insured actives found");

            //If required, multiply PV by calibration adjustments
            if (applyCalibration)
            {
                foreach (var val in Enum.GetValues(typeof(MemberStatus)))
                {
                    var memberStatus = (MemberStatus)val;
                    npv.Liabilities[memberStatus] = npv.Liabilities[memberStatus] * cb.LiabilityCalibration[memberStatus];
                    npv.Buyin[memberStatus] = npv.Buyin[memberStatus] * cb.BuyinCalibration[memberStatus];
                }
            }

            return npv;
        }

        private IDictionary<int, double> CalculateSalaryProgression(DateTime target, SalaryType salaryLinkedTo, IDictionary<int, double> cashflowBasisSpots, Basis b)
        {
            var salaryProgression = new Dictionary<int, double>(100);

            var oldSalary = b.SalaryProgression.First().SalaryRoll;

            var timeAdj = (target - b.EffectiveDate).TotalDays / Utils.DaysInAYear;

            var addYear = (int)Math.Floor(timeAdj);

            var partYear = timeAdj - addYear;

            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                salaryProgression.Add(y, 0);
                var oldYear = y + addYear;
                var oldSalaryThisYear = oldSalary.ContainsKey(oldYear) ? oldSalary[oldYear] : 0;
                var oldSalaryNextYear = oldSalary.ContainsKey(oldYear + 1) ? oldSalary[oldYear + 1] : 0;

                if (oldYear > 0 && oldYear <= Utils.MaxYear)
                {
                    salaryProgression[y] = oldSalaryThisYear * (1 - partYear);
                }
                if (oldYear > 0 && oldYear < Utils.MaxYear)
                {
                    salaryProgression[y] = salaryProgression[y] + oldSalaryNextYear * partYear;
                }
            }

            var oldInf = salaryLinkedTo == SalaryType.RPI ? b.Assumptions[AssumptionType.InflationRetailPriceIndex] : b.Assumptions[AssumptionType.InflationConsumerPriceIndex];
            var newInf = cashflowBasisSpots;

            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                salaryProgression[y] = salaryProgression[y] * Math.Pow((1 + newInf[y]).MathSafeDiv(1 + oldInf), y - 0.5);
            }

            return salaryProgression;
        }

        private CurvedBasis CalibrateToKnownPV(IBasisContext ctx, CurvedBasis cb, NPV knownLiab, bool isSelfSufficiencyBasis)
        {
            cb = cb ?? new CurvedBasis { GiltYield = new Curve(Utils.MaxYear) };
            cb.Name = ctx.Data.DisplayName;
            cb.Type = ctx.Data.Type;
            cb.IsFlat = true;

            cb.DiscPre = GetCurveFromFlatRate(ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate]);
            cb.DiscPost = GetCurveFromFlatRate(ctx.Assumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate]);
            cb.DiscPen = GetCurveFromFlatRate(ctx.Assumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate]);
            cb.RPI = GetCurveFromFlatRate(ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex]);
            cb.CPI = GetCurveFromFlatRate(ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex]);
            cb.Pinc = ctx.Assumptions.PensionIncreases.ToDictionary(pinc => pinc.Key, pinc => GetCurveFromFlatRate(pinc.Value));

            var currentPV = GetPresentValue(0, cb, false);
            var targetPV = isSelfSufficiencyBasis ? currentPV : knownLiab;

            SetCalibrationAdjustments(cb, targetPV, currentPV);

            return cb;
        }

        private void CalculateDefaultPremium(CurvedBasis cb, bool splitDiscount, NPV knownLiab)
        {
            var tol = 0.000001;
            var currentLiab = .0;
            var delta = .0;

            cb.GiltYield = giltYield;
            cb.RPI = rpi;
            cb.CPI = cpi;

            if (!splitDiscount || knownLiab.Liabilities[MemberStatus.Pensioner] == 0)
            {
                var pv = knownLiab;
                var targetLiab = pv.Liabilities[MemberStatus.ActivePast] + pv.Liabilities[MemberStatus.Deferred] + pv.Liabilities[MemberStatus.Pensioner];

                cb.PremiumPre = 0;
                var term = 20.0;
                var previousLiab = .0;
                for (var i = 1; i <= Utils.MaxYear; i++)
                {
                    cb.PremiumPost = cb.PremiumPre;
                    cb.DiscPre = GetGiltsPlus(cb.GiltYield, cb.PremiumPre);
                    cb.DiscPost = cb.DiscPre;
                    cb.DiscPen = cb.DiscPre;
                    pv = GetPresentValue(0, cb, true);
                    currentLiab = pv.Liabilities[MemberStatus.ActivePast] + pv.Liabilities[MemberStatus.Deferred] + pv.Liabilities[MemberStatus.Pensioner];
                    if (Math.Abs(currentLiab / targetLiab - 1) < tol)
                        break;

                    if (i > 1)
                        term = -(Math.Log(currentLiab) - Math.Log(previousLiab)) / Math.Log(1 + delta);

                    delta = Math.Pow(currentLiab / targetLiab, 1.0 / term) - 1;
                    previousLiab = currentLiab;
                    cb.PremiumPre = cb.PremiumPre + delta;
                }
            }
            else
            {
                var pv = knownLiab;
                var targetLiab = pv.Liabilities[MemberStatus.Pensioner];

                cb.DiscPre = cb.GiltYield;
                cb.DiscPost = cb.GiltYield;

                cb.PremiumPost = 0;
                var term = 12.0;
                var previousLiab = .0;

                for (var i = 1; i <= Utils.MaxYear; i++)
                {
                    cb.DiscPen = GetGiltsPlus(cb.GiltYield, cb.PremiumPost);
                    pv = GetPresentValue(0, cb, true);
                    currentLiab = pv.Liabilities[MemberStatus.Pensioner];
                    if (Math.Abs(currentLiab / targetLiab - 1) < tol)
                        break;

                    if (i > 1)
                        term = -(Math.Log(currentLiab) - Math.Log(previousLiab)) / Math.Log(1.0 + delta);

                    delta = Math.Pow(currentLiab / targetLiab, 1.0 / term) - 1;
                    previousLiab = currentLiab;
                    cb.PremiumPost = cb.PremiumPost + Math.Pow(currentLiab / targetLiab, 1 / term) - 1;
                }

                pv = knownLiab;
                targetLiab = pv.Liabilities[MemberStatus.ActivePast] + pv.Liabilities[MemberStatus.Deferred];

                cb.DiscPost = cb.DiscPen;
                cb.PremiumPre = cb.PremiumPost;
                term = 10.0;
                previousLiab = .0;

                for (var i = 1; i <= Utils.MaxYear; i++)
                {
                    cb.DiscPre = GetGiltsPlus(cb.GiltYield, cb.PremiumPre);
                    pv = GetPresentValue(0, cb, true);
                    currentLiab = pv.Liabilities[MemberStatus.ActivePast] + pv.Liabilities[MemberStatus.Deferred];
                    if (Math.Abs(currentLiab / targetLiab - 1) < tol)
                        break;

                    if (i > 1)
                        term = -(Math.Log(currentLiab) - Math.Log(previousLiab)) / Math.Log(1.0 + delta);

                    delta = Math.Pow(currentLiab / targetLiab, 1.0 / term) - 1;
                    previousLiab = currentLiab;
                    cb.PremiumPre = cb.PremiumPre + delta;
                }
            }

            //Store default premiums
            if (!DefaultPremiums.ContainsKey(cb.Type))
                DefaultPremiums.Add(cb.Type, new BeforeAfter<double>(cb.PremiumPre, cb.PremiumPost));

            var currentPV = GetPresentValue(0, cb, false);
            var targetPV = knownLiab;

            SetCalibrationAdjustments(cb, targetPV, currentPV);
        }

        private Curve GetGiltsPlus(Curve giltCurve, double premium)
        {
            var spots = new Dictionary<int, double>(Utils.MaxYear);

            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                spots.Add(y, giltCurve.Spot[y] + premium);
            }

            var forward = GetSpotToForward(spots);

            return new Curve(spots, forward);
        }

        private IDictionary<int, double> GetSpotToForward(IDictionary<int, double> spot)
        {
            var forward = new Dictionary<int, double>();

            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                forward.Add(y, Math.Pow(1 + spot[y], y));
            }

            for (int y = Utils.MaxYear; y >= 2; y--)
            {
                forward[y] = forward[y] / forward[y - 1] - 1;
            }

            forward[1] = forward[1] - 1;

            return forward;
        }

        /// <summary>
        /// Returns crossover point (index) when value in series 1 crosses (equals or greater than) value in series 2 at the same index
        /// For e.g. if series 1 = { 0 = 10, 1 = 20, 2 = 30, 3 = 40, 4 = 50 } and series 2 = { 0 = 40, 1 = 30, 2 = 20, 3 = 20, 4 = 10 }
        ///          then the value returned will be 2 because at this index/key, value of series 1 is >= value of series 2
        /// NOTE: Only need to return the first instance
        /// </summary>
        /// <param name="series1"></param>
        /// <param name="series2"></param>
        /// <returns></returns>
        private int GetCrossOverPoint(SortedList<int, double> series1, SortedList<int, double> series2)
        {
            if (series1 == null || series2 == null)
                return -1;

            int crossoverPoint = -1;

            foreach (KeyValuePair<int, double> kp in series1)
            {
                if (series2.ContainsKey(kp.Key) && kp.Value >= series2[kp.Key])
                    return kp.Key;
            }

            return crossoverPoint;
        }

        private Curve GetCurveFromFlatRate(double rate)
        {
            var spot = new Dictionary<int, double>(Utils.MaxYear);
            var forward = new Dictionary<int, double>(Utils.MaxYear);

            for (int y = 1; y <= Utils.MaxYear; y++)
            {
                spot.Add(y, rate);
                forward.Add(y, rate);
            }

            var curve = new Curve(spot, forward);

            return curve;
        }

        private void SetCalibrationAdjustments(CurvedBasis cb, NPV targetPV, NPV currentPV)
        {
            Action<
                IDictionary<MemberStatus, double>,
                IDictionary<MemberStatus, double>,
                IDictionary<MemberStatus, double>> calibrate = (calibration, target, current) =>
                {
                    foreach (int val in Enum.GetValues(typeof(MemberStatus)))
                    {
                        var status = (MemberStatus)val;
                        if (target[status] == 0)
                        {
                            calibration[status] = 0;
                        }
                        else if (current[status] == 0)
                        {
                            calibration[status] = 0;
                        }
                        else if (Math.Round(current[status] - target[status], 4) == 0)
                        {
                            calibration[status] = 1;
                        }
                        else
                        {
                            calibration[status] = target[status] / current[status];
                        }
                    }
                };

            calibrate(cb.LiabilityCalibration, targetPV.Liabilities, currentPV.Liabilities);
            calibrate(cb.BuyinCalibration, targetPV.Buyin, currentPV.Buyin);
        }
    }
}