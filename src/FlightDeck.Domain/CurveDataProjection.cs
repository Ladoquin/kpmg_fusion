﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain
{
    /// <summary>
    /// Curve data is essentially a 25 year projection by the BOE.
    /// </summary>
    public class CurveDataProjection
    {
        public int Id { get; private set; }
        public string IndexName { get; set; }
        public List<SpotRate> SpotRates { get; set; }

        public CurveDataProjection(int id, string indexName, List<SpotRate> spots)
        {
            Id = id;
            IndexName = indexName;
            SpotRates = spots;
        }
    }
}
