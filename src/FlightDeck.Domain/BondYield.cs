﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain
{
    public class BondYield
    {
        public FinancialIndex Index { get; set; }
        public string Label { get; set; }
        public double Value { get; set; }
    }
}
