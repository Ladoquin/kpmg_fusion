﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.JourneyPlan;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Liability Cashflow service
    /// Use this instance when stateful use of the cashflow adjustments is required. 
    /// For example, 
    /// When using from a Journey Plan calculation the cashflows will need to be adjusted for 
    /// any FRO changes, then the same cashflows will be adjusted again for ETV changes so state
    /// will need to be maintained.
    /// </summary>
    class LiabilityCashflowStatefulService : LiabilityCashflowServiceBase, ILiabilityCashflowStatefulService
    {
        private readonly DateTime at;
        private IBasisLiabilityAssumptions liabAssumptions;

        public LiabilityCashflowStatefulService(IBasisContext context)
            : base(context.Data, context.EvolutionData.LiabilityEvolution, RunMode.Normal)
        {
            this.at = context.AnalysisDate;
            this.liabAssumptions = context.Assumptions;
        }

        public LiabilityCashflowStatefulService(Basis basis, IDictionary<DateTime, Evolution.LiabilityEvolutionItem> liabilityEvolution,
                                                DateTime analysisDate, IBasisLiabilityAssumptions assumptions)
            : base (basis, liabilityEvolution, RunMode.Normal)
        {
            this.at = analysisDate;
            this.liabAssumptions = assumptions;
        }

        public void AdjustBase(Basis b)
        {
            this.b = b;
        }

        public void AdjustContext(IBasisLiabilityAssumptions assumptions)
        {
            liabAssumptions = assumptions;
        }

        public void SetPresentValue(DateTime date)
        {
            NPV = GetPresentValue(AdjCashflows, date, liabAssumptions);
        }

        public NPV GetPresentValue(DateTime date)
        {
            return GetPresentValue(AdjCashflows, date, liabAssumptions);
        }

        public NPV GetPresentValue(DateTime date, IBasisLiabilityAssumptions assumptions = null)
        {
            return GetPresentValue(AdjCashflows, date, assumptions ?? liabAssumptions);
        }

        public NPV GetBenefitOutgo(int yearNo, DateTime yearEnd)
        {
            return GetBenefitOutgo(yearNo, AdjCashflows, (yearEnd - b.EffectiveDate).TotalDays);
        }

        private bool IsFROApplicable(CashflowData cf)
        {
            return cf.MemberStatus == MemberStatus.Deferred && cf.MinAge >= 55 && cf.MaxAge <= 200 && !cf.IsBuyIn;
        }

        private bool IsETVApplicable(CashflowData cf)
        {
            return cf.MemberStatus == MemberStatus.Deferred && cf.MinAge >= 0 && cf.MaxAge < 55 && !cf.IsBuyIn;
        }

        private bool IsPIEApplicable(CashflowData cf)
        {
            return cf.BenefitType == BenefitType.Pension && cf.MemberStatus == MemberStatus.Pensioner && !cf.IsBuyIn;
        }

        private List<MemberStatus> _cashflowStatuses;
        private List<MemberStatus> CashflowStatuses
        {
            get
            {
                if (_cashflowStatuses == null)
                    _cashflowStatuses = Cashflows.Select(cf => cf.MemberStatus).Distinct().ToList();
                return _cashflowStatuses;
            }
        }

        public double ApplyFRO(FlexibleReturnOptions args)
        {
            if (AdjCashflows == null)
                throw new InvalidOperationException("Cannot apply FRO before AdjustTo has been called.");

            if (args == null || !CashflowStatuses.Contains(MemberStatus.Deferred))
                return 0;

            var liabsDischarged = dischargeLiabilities(args.TakeUpRate, IsFROApplicable);

            return liabsDischarged;
        }

        public JourneyPlanAfterResult.EFROImpact ApplyEmbeddedFRO(EmbeddedFlexibleReturnOptions args)
        {         
            if (AdjCashflows == null)
                throw new InvalidOperationException("Cannot apply FRO before AdjustTo has been called.");

            //NB: at time of writing this is specifically for embedded FRO at retirement.
            //For embedded TV options pre retirement more work is required, e.g. to discount TV back to an assumed leaving date.

            double PV;           //present value of some cashflows
            double[,] Split; 
            double OldPart;       //"old" element of every cashflow, i.e. carried forward from previous year (y-1)
            double NewPart;       //"new" element of every cashflow, i.e. new pensions commencing in year y
            double DiscRate;      //post retirement discount rate used to calculate liabs discharged
            double PincRate;      //rate of pension increase
            double Mort;          //mortality impact each year (where it can be derived)
            IDictionary<MemberStatus, double> changeInLiabsDischarged = new Dictionary<MemberStatus, double>();
            double totalLiabsDischarged = .0;

            if (!CashflowStatuses.Contains(MemberStatus.ActivePast) && !CashflowStatuses.Contains(MemberStatus.Deferred))
                return null;
            if (args.AnnualTakeUpRate <= 0) // no need to check selected froType as this method only gets called if froType has been selected as embeddedFro
                return null;
            if (b.Type == BasisType.Buyout)
                return null;

            var PVBefore = GetPresentValue(at);
            DiscRate = liabAssumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate];

            Dictionary<MemberStatus, IDictionary<int, double>> cetv = new Dictionary<MemberStatus, IDictionary<int, double>>();
            for (int i = 0; i < AdjCashflows.Count; i++)
            {
                var cf = AdjCashflows[i].Clone();

                //If Not (cf.Status = ACT Or cf.Status = DEF) Or cf.BuyIn Or cf.FirstUsedYear = 0 Then
                if (!(cf.MemberStatus == MemberStatus.ActivePast || cf.MemberStatus == MemberStatus.Deferred) || cf.IsBuyIn || cf.BeginYear == 0)
                {
                    //'ignore cf
                }
                //ElseIf cf.BenType = LumpSum Then
                else if (cf.BenefitType == BenefitType.LumpSum)
                {
                    //'Very simple - just add the lump sum
                    for (int yor = cf.BeginYear; yor <= cf.EndYear; yor++)
                    {
                        if (!cetv.ContainsKey(cf.MemberStatus))
                            cetv.Add(cf.MemberStatus, new Dictionary<int, double>(100));
                        if (!cetv[cf.MemberStatus].ContainsKey(yor))
                            cetv[cf.MemberStatus].Add(yor, 0.0);

                        cetv[cf.MemberStatus][yor] = cetv[cf.MemberStatus][yor] + cf.Cashflow[yor] * args.AnnualTakeUpRate * args.EmbeddedFroBasisChange;
                        cf.Cashflow[yor] = cf.Cashflow[yor] * (1 - args.AnnualTakeUpRate);
                    }

                    //'Save adjusted cashflows
                    AdjCashflows[i] = cf.Clone();
                }
                else
                {

                    //    'Split pension cashflows by year of retirement, i.e. populate Split()
                    //    '--------------------------------------------------------------------
                    Split = new double[cf.EndYear + 1, cf.EndYear + 1]; 
                    PincRate = liabAssumptions.PensionIncreases[cf.PensionIncreaseReference];

                    for (int y = cf.BeginYear; y <= cf.EndYear; y++)
                    {
                        //'Split each source cashflow into OldPart + NewPart
                        if (y == AdjCashflows[i].BeginYear)
                        {
                            OldPart = 0;
                            NewPart = AdjCashflows[i].Cashflow[y];
                        }
                        else
                        {
                            var x = AdjCashflows[i].Cashflow[y - 1] * (1 + PincRate);
                            var z = AdjCashflows[i].Cashflow[y];
                            OldPart = x <= z ? x : z;
                            NewPart = AdjCashflows[i].Cashflow[y] - OldPart;
                        }

                        //'Calculate mortality impact (if we can)
                        if (y == 1)
                        {
                            Mort = 1;
                        }
                        else if (OldPart > 0 && AdjCashflows[i].Cashflow[y - 1] > 0)
                        {
                            Mort = OldPart / (AdjCashflows[i].Cashflow[y - 1] * (1 + PincRate));
                        }
                        else
                        {
                            Mort = 1;
                        }

                        //'Insert the new component in Split() and roll the other components forward 1 year
                        Split[y, y] = NewPart; 

                        for (int yor = cf.BeginYear; yor <= y - 1; yor++)
                        {
                            Split[yor, y] = Split[yor, y - 1] * (1 + PincRate) * Mort;
                        }

                        //'Finally calculate residual cashflow
                        cf.Cashflow[y] = cf.Cashflow[y] * (1 - args.AnnualTakeUpRate);
                    }

                    //    'Save adjusted cashflows
                    AdjCashflows[i] = cf.Clone();

                    //    'Update CETVs paid
                    //    '-----------------
                    for (int yor = cf.BeginYear; yor <= cf.EndYear; yor++)
                    {
                        if (Split[yor, yor] != 0)
                        {
                            PV = 0;
                            for (int y = yor; y <= cf.EndYear; y++)
                            {
                                PV = PV + Split[yor, y] / Math.Pow((1 + DiscRate), (y - yor));
                            }
                            if (!cetv.ContainsKey(cf.MemberStatus))
                                cetv.Add(cf.MemberStatus, new Dictionary<int, double>(100));
                            if (!cetv[cf.MemberStatus].ContainsKey(yor))
                                cetv[cf.MemberStatus].Add(yor, 0.0);
                            cetv[cf.MemberStatus][yor] = cetv[cf.MemberStatus][yor] + PV * args.AnnualTakeUpRate * args.EmbeddedFroBasisChange;
                        }
                    }
                }
            }

            var PVAfter = GetPresentValue(AdjCashflows, at, liabAssumptions);

            var nonPenLiabsBefore = PVBefore.Liabilities[MemberStatus.ActivePast] + PVBefore.Liabilities[MemberStatus.Deferred];
            totalLiabsDischarged = nonPenLiabsBefore - (PVAfter.Liabilities[MemberStatus.ActivePast] + PVAfter.Liabilities[MemberStatus.Deferred]);

            changeInLiabsDischarged.Add(MemberStatus.ActivePast, PVBefore.Liabilities[MemberStatus.ActivePast] - PVAfter.Liabilities[MemberStatus.ActivePast]);
            changeInLiabsDischarged.Add(MemberStatus.Deferred, PVBefore.Liabilities[MemberStatus.Deferred] - PVAfter.Liabilities[MemberStatus.Deferred]);

            //add zero for non populated values.
            for (int k = 1; k <= Utils.MaxYear; k++ )
            {
                if(!cetv.ContainsKey(MemberStatus.ActivePast))
                    cetv.Add(MemberStatus.ActivePast, new Dictionary<int, double>());
                if (cetv[MemberStatus.ActivePast].ContainsKey(k) == false)
                    cetv[MemberStatus.ActivePast].Add(k, .0);

                if (!cetv.ContainsKey(MemberStatus.Deferred))
                    cetv.Add(MemberStatus.Deferred, new Dictionary<int, double>());
                if (cetv[MemberStatus.Deferred].ContainsKey(k) == false)
                    cetv[MemberStatus.Deferred].Add(k, .0);
            }

            return new JourneyPlanAfterResult.EFROImpact(cetv[MemberStatus.ActivePast], cetv[MemberStatus.Deferred], totalLiabsDischarged, changeInLiabsDischarged, nonPenLiabsBefore, at, b.EffectiveDate);
        }

        public double ApplyETV(EnhancedTransferValueOptions args)
        {
            if (AdjCashflows == null)
                throw new InvalidOperationException("Cannot apply ETV before AdjustTo has been called.");

            var liabsDischarged = dischargeLiabilities(args.TakeUpRate, IsETVApplicable);

            return liabsDischarged;
        }

        public PIECashflowImpact ApplyPIE(PieOptions args, PIEInitData init, IDictionary<int, double> clientPensionIncreases)
        {
            if (AdjCashflows == null)
                throw new InvalidOperationException("Cannot apply PIE before AdjustTo has been called.");

            var valueBefore = .0;
            var upliftFactor = .0;
            var valueNilIncs = .0;

            if (args.TakeUpRate != 0 && CashflowStatuses.Contains(MemberStatus.Pensioner))
            {
                var nilInc = b.UnderlyingPensionIncreaseIndices.FirstOrDefault(pinc => pinc.Value.IsNilIncrease);

                //Contradictary logic in Tracker - it throws the following exception when Nil incs aren't defined, 
                //yet the method to check if they're defined returns -1 when not, then only throws the error if the Nil inc value returned is 0.
                //if (nilInc.Value == null)
                //    throw new Exception("Nil incs are not defined in the pension increase definitions");

                var nextIncDate = new DateTime(at.Year, b.LastPensionIncreaseDate.Month, b.LastPensionIncreaseDate.Day);
                if (nextIncDate < at)
                    nextIncDate = nextIncDate.AddYears(1);
                var y1 = 0;
                for (int y = 1; y <= 100; y++)
                {
                    if (b.EffectiveDate.AddYears(y) >= nextIncDate)
                    {
                        y1 = y;
                        break;
                    }
                }

                Func<int, bool> IsNilInc = pincKey => b.UnderlyingPensionIncreaseIndices[pincKey].IsNilIncrease;

                var exchCfs = new List<CashflowData>(AdjCashflows.Count);

                for (int i = 0; i < AdjCashflows.Count; i++)
                {
                    var cf = AdjCashflows[i];

                    if (IsPIEApplicable(cf))
                    {
                        if (!IsNilInc(cf.PensionIncreaseReference))
                        {
                            var exchCf = cf.Clone();
                            for (int y = exchCf.BeginYear; y <= exchCf.EndYear; y++)
                            {
                                exchCf.Cashflow[y] = exchCf.Cashflow[y] * args.PortionEligible * args.TakeUpRate;
                                cf.Cashflow[y] -= exchCf.Cashflow[y];
                            }

                            exchCfs.Add(exchCf);
                        }
                    }
                }

                if (exchCfs.Count > 0)
                {
                valueBefore = GetPresentValue(exchCfs, at, liabAssumptions).Liabilities[MemberStatus.Pensioner];

                for (int j = 0; j < exchCfs.Count; j++)
                {
                    var esc = clientPensionIncreases[exchCfs[j].PensionIncreaseReference]; //TODO check that client pincs exist here
                    var escfac = 1 + esc * (1 - ((nextIncDate - at).TotalDays / 365.25));
                    for (int y = y1; y <= exchCfs[j].Cashflow.Keys.Max(); y++)
                    {
                        exchCfs[j].Cashflow[y] /= escfac;
                        escfac *= (1 + esc);
                    }
                    exchCfs[j].PensionIncreaseReference = nilInc.Key;
                }

                valueNilIncs = GetPresentValue(exchCfs, at, liabAssumptions).Liabilities[MemberStatus.Pensioner];

                var maxUplift = valueBefore.MathSafeDiv(valueNilIncs) - 1;

                var propMaxUplift = init == null
                    ? args.ActualUplift
                    : args.ActualUplift.MathSafeDiv(init.PenLiab == 0 ? 0 : init.ValueOfIncs.MathSafeDiv(init.PenLiab - init.ValueOfIncs));

                upliftFactor = 1 + propMaxUplift * maxUplift;
                
                for (int j = 0; j < exchCfs.Count; j++)
                {
                    for (int y = y1; y <= exchCfs[j].Cashflow.Keys.Max(); y++)
                    {
                        exchCfs[j].Cashflow[y] *= upliftFactor;
                    }
                }

                AdjCashflows.AddRange(exchCfs);
            }            
            }            

            return new PIECashflowImpact(valueBefore, upliftFactor, valueNilIncs);
        }

        public JourneyPlanAfterResult.BenefitChangesImpact ApplyBenefitChanges(FutureBenefitsParameters args)
        {
            if (!CashflowStatuses.Contains(MemberStatus.ActivePast) && !CashflowStatuses.Contains(MemberStatus.ActiveFuture))
                return new JourneyPlanAfterResult.BenefitChangesImpact(0, 0, 0, 0);

            NPV = GetPresentValue(AdjCashflows, at, liabAssumptions);

            var liabsBefore = NPV.Total;
            var serviceCostBefore = NPV.Liabilities[MemberStatus.ActiveFuture];

            if (args.AdjustmentType == BenefitAdjustmentType.BenefitReduction || args.AdjustmentType == BenefitAdjustmentType.DcImplementation)
            {
                ReduceFutureBenefits(args.AdjustmentType == BenefitAdjustmentType.BenefitReduction ? args.AdjustmentRate : 0);
            }

            if (args.AdjustmentType == BenefitAdjustmentType.SalaryLimit || args.AdjustmentType == BenefitAdjustmentType.CareRevaluation || args.AdjustmentType == BenefitAdjustmentType.DcImplementation)
            {
                ApplySalaryLimit(args.AdjustmentType, args.AdjustmentRate);
            }

            if (args.AdjustBenefits)
            {
                NPV = GetPresentValue(AdjCashflows, at, liabAssumptions);
            }

            var liabsAfter = NPV.Total;
            var serviceCostAfter = args.AdjustmentType == BenefitAdjustmentType.DcImplementation
                ? 0 // evaluated elsewhere
                : NPV.Liabilities[MemberStatus.ActiveFuture];

            return new JourneyPlanAfterResult.BenefitChangesImpact(liabsBefore, serviceCostBefore, liabsAfter, serviceCostAfter);
        }

        private double dischargeLiabilities(double takeupRate, Func<CashflowData, bool> isCashflowApplicable)
        {
            var liabsDischarged = .0;
            var lostCfs = new List<CashflowData>();

            if (takeupRate != 0)
            {
                for (int i = 0; i < AdjCashflows.Count; i++)
                {
                    var cf = AdjCashflows[i];

                    if (isCashflowApplicable(cf))
                    {
                        var lostCf = cf.Clone();
                        for (int y = lostCf.BeginYear; y <= lostCf.EndYear; y++)
                        {
                            lostCf.Cashflow[y] *= takeupRate;
                            cf.Cashflow[y] -= lostCf.Cashflow[y];
                        }

                        lostCfs.Add(lostCf);
                    }
                }
            }

            liabsDischarged = GetPresentValue(lostCfs, at, liabAssumptions).Liabilities[MemberStatus.Deferred];
            NPV.Liabilities[MemberStatus.Deferred] -= liabsDischarged;

            return liabsDischarged;
        }

        public double GetPreFRODeferredLiability()
        {
            var cf = AdjCashflows.Where(adj => IsFROApplicable(adj));

            var npv = GetPresentValue(cf, at, liabAssumptions);

            return npv.Liabilities[MemberStatus.Deferred];
        }

        public double GetPreETVDeferredLiability()
        {
            var cf = AdjCashflows.Where(adj => IsETVApplicable(adj));

            var npv = GetPresentValue(cf, at, liabAssumptions);

            return npv.Liabilities[MemberStatus.Deferred];
        }

        private void ReduceFutureBenefits(double futureBenefitProportion)
        {
            if (!Cashflows.Any(cf => cf.MemberStatus == MemberStatus.ActiveFuture))
                return;

            for (int i = 0; i < AdjCashflows.Count; i++)
            {
                var cf = AdjCashflows[i];

                if (cf.MemberStatus == MemberStatus.ActiveFuture)
                {
                    for (int y = cf.BeginYear; y <= cf.EndYear; y++)
                    {
                        cf.Cashflow[y] *= futureBenefitProportion;
                    }
                }
            }
        }

        private void ApplySalaryLimit(BenefitAdjustmentType adjType, double adj)
        {
            var statuses = Cashflows.Select(cf => cf.MemberStatus).Distinct().ToList();
            if (!statuses.Contains(MemberStatus.ActivePast) && !statuses.Contains(MemberStatus.ActiveFuture))
                return;

            var oldSalInc = liabAssumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease];
            var newSalInc = adjType == BenefitAdjustmentType.DcImplementation
                ? liabAssumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex]
                : adj;

            newSalInc = Math.Min(newSalInc, oldSalInc);

            for (int i = 0; i < AdjCashflows.Count; i++)
            {
                var cf = AdjCashflows[i];

                if (cf.MemberStatus == MemberStatus.ActivePast || cf.MemberStatus == MemberStatus.ActiveFuture)
                {
                    var cfPre = cf.Clone().Cashflow;
                    var newPart = .0;
                    var oldPart = .0;
                    var prevAdj = .0;

                    for (int y = cf.BeginYear; y <= cf.EndYear; y++)
                    {
                        var salAdj = Math.Pow((1 + newSalInc) / (1 + oldSalInc), y - .5);

                        if (cf.Cashflow[y] == 0)
                        {
                        }
                        else if (cf.BenefitType == BenefitType.LumpSum)
                        {
                            cf.Cashflow[y] *= salAdj;
                        }
                        else
                        {
                            if (y == cf.BeginYear)
                            {
                                newPart = cfPre[y];
                            }
                            else
                            {
                                oldPart = Math.Min(cfPre[y - 1] * (1 + liabAssumptions.PensionIncreases[cf.PensionIncreaseReference]), cfPre[y]);
                                newPart = cfPre[y] - oldPart;
                            }

                            oldPart *= prevAdj;
                            newPart *= salAdj;

                            prevAdj = (oldPart + newPart) / cf.Cashflow[y];

                            cf.Cashflow[y] = oldPart + newPart;
                        }
                    }
                }
            }
        }

        public void ConvertToLDI(ISchemeData schemeData, DateTime atDate, double discRate, double inflationRate)
        {
            if (!schemeData.LDICashflowBasis.OverrideAssumptions)
                return;

            var penIncCount = 6;

            GroupOver50sCashFlows();

            var liabAssumption = new Dictionary<AssumptionType, double>();
            var userRPI = inflationRate;
            var userCPI = inflationRate - schemeData.LDICashflowBasis.InflationGap;
            liabAssumption.Add(AssumptionType.PreRetirementDiscountRate, discRate);
            liabAssumption.Add(AssumptionType.PostRetirementDiscountRate, discRate);
            liabAssumption.Add(AssumptionType.PensionDiscountRate, discRate);
            liabAssumption.Add(AssumptionType.SalaryIncrease, inflationRate);
            liabAssumption.Add(AssumptionType.InflationRetailPriceIndex, inflationRate);
            liabAssumption.Add(AssumptionType.InflationConsumerPriceIndex, userCPI);

            var pincAssumptions = Basis.GetVariablePincAssumption(atDate, userRPI, userCPI, false, atDate);
            var basisLiabAssumptions = new BasisLiabilityAssumptions(liabAssumption, Basis.LifeExpectancy, pincAssumptions);

            AdjustTo(atDate, basisLiabAssumptions, true);

            // SaveLDIOverrides - replace initial assumptions on basis
            AdjustBase(Basis.Clone(null, null, null, null, atDate));
            Basis.AdjustLiabilityAssumptions(liabAssumption, schemeData.LDICashflowBasis);
            this.liabAssumptions = basisLiabAssumptions;

            // re-align black-scholes
            userRPI = Basis.UnderlyingAssumptionIndices[AssumptionType.InflationRetailPriceIndex].GetValue(atDate, Basis.EffectiveDate);
            userCPI = Basis.UnderlyingAssumptionIndices[AssumptionType.InflationConsumerPriceIndex].GetValue(atDate, Basis.EffectiveDate);
            var pincIndices = Basis.UnderlyingPensionIncreaseIndices;
            for (int i = 1; i <= penIncCount; i++)
            {
                var pinc = pincIndices[i];
                if (pincIndices[i].Category == AssumptionCategory.BlackScholes)
                    pinc.RealignBlackScholes(pinc.InflationType == InflationType.Rpi ? userRPI : userCPI);
            }

            Basis.AdjustPincAssumptions(Basis.GetVariablePincAssumption(Basis.EffectiveDate, userRPI, userCPI, false));

            // replace original cashflows
            ReplaceOriginalCashflows();
        }

        public void SetNPV(NPV npv)
        {
            NPV = npv;
        }
    }
}
