﻿namespace FlightDeck.Domain
{
    using System;

    public class GiltYieldService : IGiltYieldService
    {
        private readonly Basis b;

        public GiltYieldService(Basis b)
        {
            this.b = b;
        }

        public double GetGiltYield(DateTime at)
        {
            return b.UnderlyingGiltYieldIndex.GetValue(at) / 100;
        }
    }
}
