﻿namespace FlightDeck.Domain
{
    public enum AssumptionCategory
    {
        Fixed = 1,
        BlackScholes,
        Curved      //Not implemented in a spreadsheet

    }
}