﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    public class SchemeResults : ISchemeResults
    {
        public DateTime RefreshDate { get; set; }
        public BuyinCost BuyinCost { get; set; }
        public IEnumerable<LDI.LDIEvolutionItem> LDIEvolutionData { get; set; }
    }
}
