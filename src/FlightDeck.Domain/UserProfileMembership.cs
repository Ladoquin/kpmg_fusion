﻿namespace FlightDeck.Domain
{
    public class UserProfileMembership
    {
        public int UserId { get; set; }
        public int PasswordFailuresSinceLastSuccess { get; set; }
    }
}
