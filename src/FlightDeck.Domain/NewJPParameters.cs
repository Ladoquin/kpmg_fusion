﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    
    public class NewJPParameters
    {
        public IBasisContext CashflowBasis { get; set; }
        public DateTime AnalysisDate { get; set; }
        public double InvGrowthRateBefore { get; set; }
        public double SelfSufficiencyDiscountRate { get; set; }
        public IBasisContext FundingBasis { get; set; }
        public IBasisContext SelfSufficiencyBasis { get; set; }
        public IBasisContext BuyoutBasis { get; set; }
        public SalaryType SalaryInflationType;
        public NewJPExtraContributionOptions RecoveryPlan { get; set; }
        public CurveDataProjection CurveDataProjection { get; set; }
        public int TriggerSteps { get; set; }
        public double FundingLevel { get; set; }
        public bool FundingLevelTrigger { get; set; }
        public int TriggerTimeStartYear { get; set; }
        public int TriggerTransitionPeriod { get; set; }
        public double InitialGrowthRate { get; set; }
        public double TargetGrowthRate { get; set; }
        public bool UseCurves { get; set; }
        public bool IncludeBuyIns { get; set; }
    }
}
