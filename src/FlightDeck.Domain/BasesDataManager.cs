﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class BasesDataManager : IBasesDataManager
    {
        public BasisType BasisType { get; private set; }
        private readonly Dictionary<DateTime, Basis> bases;

        public BasesDataManager(IEnumerable<Basis> bases)
        {
            this.bases = bases.ToDictionary(b => b.EffectiveDate);

            BasisType = bases.First().Type;          
        }

        public Basis GetBasis(DateTime at)
        {
            if (at < bases.Keys.Min())
                return null;

            if (bases.ContainsKey(at))
                return bases[at];

            var effectiveDate = bases.Keys.Where(ed => ed < at).OrderBy(ed => ed).Last();

            return bases[effectiveDate];
        }

        public DateTime AnchorDate
        {
            get
            {
                return bases.Keys.Min();
            }
        }

        public IEnumerable<DateTime> EffectiveDates
        {
            get
            {
                return bases.Keys;
            }
        }
    }
}
