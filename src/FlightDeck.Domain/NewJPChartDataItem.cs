﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System;

    public class NewJPChartDataItem : IEquatable<NewJPChartDataItem>
    {
        public int Year { get; private set; }
        public double TechnicalProvision { get; private set; }
        public double SelfSufficiency { get; private set; }
        public double Buyout { get; private set; }
        public double Asset1 { get; private set; }
        public double Asset2 { get; private set; }


        public NewJPChartDataItem(
            int year,
            double technicalProvision,
            double accounting,
            double buyout,
            double asset1,
            double asset2)
        {
            Year = year;
            TechnicalProvision = technicalProvision;
            SelfSufficiency = accounting;
            Buyout = buyout;
            Asset1 = asset1;
            Asset2 = asset2;
        }

        public bool Equals(NewJPChartDataItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Year == other.Year
                && Utils.EqualityCheck(TechnicalProvision, other.TechnicalProvision)
                && Utils.EqualityCheck(SelfSufficiency, other.SelfSufficiency)
                && Utils.EqualityCheck(Buyout, other.Buyout)
                && Utils.EqualityCheck(Asset1, other.Asset1)
                && Utils.EqualityCheck(Asset2, other.Asset2);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((LiabilityEvolutionItem)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Year.GetHashCode();
                hashCode = (hashCode * 397) ^ TechnicalProvision.GetHashCode();
                hashCode = (hashCode * 397) ^ SelfSufficiency.GetHashCode();
                hashCode = (hashCode * 397) ^ Buyout.GetHashCode();
                hashCode = (hashCode * 397) ^ Asset1.GetHashCode();
                hashCode = (hashCode * 397) ^ Asset2.GetHashCode();
         
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Year: {0}, TechnicalProvision: {1}, Accounting: {2}, Buyout: {3}, Asset1: {4}, Asset2: {5}", Year, TechnicalProvision, SelfSufficiency, Buyout, Asset1, Asset2);
        }
    }
}