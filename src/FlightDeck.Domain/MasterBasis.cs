﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Domain
{
    public class MasterBasis
    {
        public int MasterBasisId { get; private set; }
        public SortedList<DateTime, Basis> Bases { get; private set; }
        public IEnumerable<DateTime> EffectiveDates { get; private set; }
        public BasisType Type { get; private set; }
        public string DisplayName { get; private set; }
        public DateTime AttributionStart { get; private set; }
        public DateTime AttributionEnd { get; private set; }
        public DateTime AnchorDate { get; private set; }

        public MasterBasis(IEnumerable<Basis> bases)
        {
            MasterBasisId = bases.First().MasterBasisId;

            Bases = new SortedList<DateTime, Basis>();
            foreach (var basis in bases)
            {
                Bases.Add(basis.EffectiveDate, basis);
            }

            EffectiveDates = Bases.Keys;
            AnchorDate = EffectiveDates.Min();
            Type = bases.First().Type;
            DisplayName = bases.First().DisplayName;
        }

        //TODO check if this is being implemented somewhere else post-refactor
        public DateTime GetIndexDate()
        {
            var date = DateTime.MaxValue;
            foreach (var b in Bases.Values)
            {
                var assumptionEndDate = b.UnderlyingAssumptionIndices.Values.Min(x => x.Index.EndDate);
                var pensionIncreaseEndDate = b.UnderlyingPensionIncreaseIndices.Values.Min(x => x.Index == null ? DateTime.MaxValue : x.Index.EndDate);
                var d = assumptionEndDate > pensionIncreaseEndDate ? pensionIncreaseEndDate : assumptionEndDate;
                if (date > d)
                    date = d;
            }
            return date;
        }
    }
}
