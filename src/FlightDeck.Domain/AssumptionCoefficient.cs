﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    class AssumptionCoefficient
    {
        public struct Component
        {
            public IDictionary<AssumptionType, double> Assumptions { get; private set; }
            public IDictionary<int, double> PensionIncreases { get; private set; }

            public Component(IDictionary<AssumptionType, double> assumptions, IDictionary<int, double> pensionIncreases)
                : this()
            {
                Assumptions = assumptions;
                PensionIncreases = pensionIncreases;
            }
        }

        public Component Base { get; private set; }
        public Component Variable { get; private set; }

        public AssumptionCoefficient(BasisLiabilityAssumptions assumptions)
        {
            Base = new Component(assumptions.LiabilityAssumptions, assumptions.PensionIncreases);
            Variable = new Component(assumptions.LiabilityAssumptions, assumptions.PensionIncreases); 
        }
        public AssumptionCoefficient(BasisLiabilityAssumptions atBase, BasisLiabilityAssumptions variable)
        {
            Base = new Component(atBase.LiabilityAssumptions, atBase.PensionIncreases);
            Variable = new Component(variable.LiabilityAssumptions, variable.PensionIncreases);
        }
    }
}
