﻿namespace FlightDeck.Domain
{
    public class PIEInitData
    {
        public double PenLiab { get; private set; }
        public double ValueOfIncs { get; private set; }

        public PIEInitData(double penLiab, double valueOfIncs)
        {
            PenLiab = penLiab;
            ValueOfIncs = valueOfIncs;
        }
    }
}
