﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    public interface IBasesDataManager
    {
        BasisType BasisType { get; }
        Basis GetBasis(DateTime at);
        DateTime AnchorDate { get; }
        IEnumerable<DateTime> EffectiveDates { get; }
    }
}
