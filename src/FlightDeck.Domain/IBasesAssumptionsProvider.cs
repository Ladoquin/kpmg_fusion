﻿namespace FlightDeck.Domain
{
    using System.Collections.Generic;
    
    public interface IBasesAssumptionsProvider
    {
        IDictionary<string, IBasisAssumptions> BasisAssumptionsCollection { get; }
    }
}
