﻿namespace FlightDeck.Domain
{
    using System;

    interface IIndexService
    {
        double GetGrowth(FinancialIndex index, DateTime start, DateTime end);
    }
}
