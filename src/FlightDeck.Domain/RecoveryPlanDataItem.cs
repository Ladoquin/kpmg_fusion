﻿namespace FlightDeck.Domain
{
    internal class RecoveryPlanDataItem
    {
        public double CurrentBuyin { get; private set; }
        public double NewBuyin { get; private set; }
        public double TotalAssets { get; private set; }

        public RecoveryPlanDataItem(double currentBuyin, double newBuyin, double totalAssets)
        {
            CurrentBuyin = currentBuyin;
            NewBuyin = newBuyin;
            TotalAssets = totalAssets;
        }
    }
}
