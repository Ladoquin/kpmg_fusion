﻿namespace FlightDeck.Domain
{
    using System;
    using System.Collections.Generic;

    public interface ISchemeContext
    {
        ISchemeResults Calculated { get; }
        ISchemeAssumptions Assumptions { get; }
        ISchemeData Data { get; }
        IBasesAssumptionsProvider BasesAssumptionsProvider { get; }
    }
}
