﻿namespace FlightDeck.Domain
{
    using System.Collections.Generic;
    using System.Linq;

    class MortalityAdjustmentService
    {
        private readonly IDictionary<int, double> mortalityAdjustments;

        public MortalityAdjustmentService(IDictionary<int, double> mortalityAdjustments)
        {
            this.mortalityAdjustments = mortalityAdjustments;
        }

        public double GetMortalityAdjustment(int age)
        {
            var lowerAdj = .0;
            var upperAdj = .0;
            var lowerAge = -1000;
            var upperAge = 1000;

            foreach(var thisAge in mortalityAdjustments.Keys.OrderBy(k => k))
            {
                var thisAdj = mortalityAdjustments[thisAge];

                if (thisAge <= age && thisAge > lowerAge)
                {
                    lowerAge = thisAge;
                    lowerAdj = thisAdj;
                }
                if (thisAge > age && thisAge < upperAge)
                {
                    upperAge = thisAge;
                    upperAdj = thisAdj;
                }
            }

            if (lowerAge == -1000)
                return upperAdj;
            else if (upperAge == 1000 || lowerAge == age)
                return lowerAdj;
            else
                return lowerAdj + (upperAdj - lowerAdj) * (age - lowerAge) / (upperAge - lowerAge);
        }
    }
}
