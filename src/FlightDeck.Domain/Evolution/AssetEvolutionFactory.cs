﻿namespace FlightDeck.Domain.Evolution
{
    public class AssetEvolutionFactory
    {
        public IAssetEvolutionService GetAssetEvolutionService(ISchemeData scheme)
        {
            if (!scheme.Funded)
                return new UnfundedAssetEvolutionService();

            return new AssetEvolutionService();
        }
    }
}
