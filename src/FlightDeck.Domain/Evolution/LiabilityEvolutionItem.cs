﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System;

    [Serializable]
    public class LiabilityEvolutionItem : IEquatable<LiabilityEvolutionItem>
    {
        public DateTime Date { get; private set; }
        public double Act { get; private set; }
        public double Def { get; private set; }
        public double Pen { get; private set; }
        public double Fut { get; private set; }
        public double Liabs { get; private set; }
        public double DefBuyIn { get; private set; }
        public double PenBuyIn { get; private set; }
        public double DiscPre { get; private set; }
        public double DiscPost { get; private set; }
        public double DiscPen { get; private set; }
        public double SalInc { get; private set; }
        public double RPI { get; private set; }
        public double CPI { get; private set; }
        public double Pinc1 { get; private set; }
        public double Pinc2 { get; private set; }
        public double Pinc3 { get; private set; }
        public double Pinc4 { get; private set; }
        public double Pinc5 { get; private set; }
        public double Pinc6 { get; private set; }
        public double ActDuration { get; private set; }
        public double DefDuration { get; private set; }
        public double PenDuration { get; private set; }
        public double FutDuration { get; private set; }

        public LiabilityEvolutionItem(
            DateTime date,
            double act,
            double def,
            double pen,
            double fut,
            double liabs,
            double defBuyIn,
            double penBuyIn,
            double discPre,
            double discPost,
            double discPen,
            double salInc,
            double rpi,
            double cpi,
            double pinc1,
            double pinc2,
            double pinc3,
            double pinc4,
            double pinc5,
            double pinc6,
            double actDuration,
            double defDuration,
            double penDuration,
            double futDuration)
        {
            Date = date;
            Act = act;
            Def = def;
            Pen = pen;
            Fut = fut;
            Liabs = liabs;
            DefBuyIn = defBuyIn;
            PenBuyIn = penBuyIn;
            DiscPre = discPre;
            DiscPost = discPost;
            DiscPen = discPen;
            SalInc = salInc;
            RPI = rpi;
            CPI = cpi;
            Pinc1 = pinc1;
            Pinc2 = pinc2;
            Pinc3 = pinc3;
            Pinc4 = pinc4;
            Pinc5 = pinc5;
            Pinc6 = pinc6;
            ActDuration = actDuration;
            DefDuration = defDuration;
            PenDuration = penDuration;
            FutDuration = futDuration;
        }

        public bool Equals(LiabilityEvolutionItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date.Date == other.Date.Date
                && Utils.EqualityCheck(Act, other.Act)
                && Utils.EqualityCheck(Def, other.Def)
                && Utils.EqualityCheck(Pen, other.Pen)
                && Utils.EqualityCheck(Fut, other.Fut)
                && Utils.EqualityCheck(Liabs, other.Liabs)
                && Utils.EqualityCheck(DefBuyIn, other.DefBuyIn)
                && Utils.EqualityCheck(PenBuyIn, other.PenBuyIn)
                && Utils.EqualityCheck(DiscPre, other.DiscPre)
                && Utils.EqualityCheck(DiscPost, other.DiscPost)
                && Utils.EqualityCheck(DiscPen, other.DiscPen)
                && Utils.EqualityCheck(SalInc, other.SalInc)
                && Utils.EqualityCheck(RPI, other.RPI)
                && Utils.EqualityCheck(CPI, other.CPI)
                && Utils.EqualityCheck(Pinc1, other.Pinc1)
                && Utils.EqualityCheck(Pinc2, other.Pinc2)
                && Utils.EqualityCheck(Pinc3, other.Pinc3)
                && Utils.EqualityCheck(Pinc4, other.Pinc4)
                && Utils.EqualityCheck(Pinc5, other.Pinc5)
                && Utils.EqualityCheck(Pinc6, other.Pinc6)
                && Utils.EqualityCheck(ActDuration, other.ActDuration)
                && Utils.EqualityCheck(DefDuration, other.DefDuration)
                && Utils.EqualityCheck(PenDuration, other.PenDuration)
                && Utils.EqualityCheck(FutDuration, other.FutDuration);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((LiabilityEvolutionItem)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ Act.GetHashCode();
                hashCode = (hashCode * 397) ^ Def.GetHashCode();
                hashCode = (hashCode * 397) ^ Pen.GetHashCode();
                hashCode = (hashCode * 397) ^ Fut.GetHashCode();
                hashCode = (hashCode * 397) ^ Liabs.GetHashCode();
                hashCode = (hashCode * 397) ^ DefBuyIn.GetHashCode();
                hashCode = (hashCode * 397) ^ PenBuyIn.GetHashCode();
                hashCode = (hashCode * 397) ^ DiscPre.GetHashCode();
                hashCode = (hashCode * 397) ^ DiscPost.GetHashCode();
                hashCode = (hashCode * 397) ^ DiscPen.GetHashCode();
                hashCode = (hashCode * 397) ^ SalInc.GetHashCode();
                hashCode = (hashCode * 397) ^ RPI.GetHashCode();
                hashCode = (hashCode * 397) ^ CPI.GetHashCode();
                hashCode = (hashCode * 397) ^ Pinc1.GetHashCode();
                hashCode = (hashCode * 397) ^ Pinc2.GetHashCode();
                hashCode = (hashCode * 397) ^ Pinc3.GetHashCode();
                hashCode = (hashCode * 397) ^ Pinc4.GetHashCode();
                hashCode = (hashCode * 397) ^ Pinc5.GetHashCode();
                hashCode = (hashCode * 397) ^ Pinc6.GetHashCode();
                hashCode = (hashCode * 397) ^ ActDuration.GetHashCode();
                hashCode = (hashCode * 397) ^ DefDuration.GetHashCode();
                hashCode = (hashCode * 397) ^ PenDuration.GetHashCode();
                hashCode = (hashCode * 397) ^ FutDuration.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Date: {0}, Act: {1}, Def: {2}, Pen: {3}, Fut: {4}, Liabs: {5}, DefBuyIn: {6}, PenBuyIn: {7}, DiscPre: {8}, DiscPost: {9}, DiscPen: {10}, SalInc: {11}, RPI: {12}, CPI: {13}, PenInc1: {14}, PenInc2: {15}, PenInc3: {16}, PenInc4: {17}, PenInc5: {18}, PenInc6: {19}, ActDuration: {20}, DefDuration: {21}, PenDuration: {22}, FutDuration: {23}", Date.ToString("dd/MM/yyyy"), Act, Def, Pen, Fut, Liabs, DefBuyIn, PenBuyIn, DiscPre, DiscPost, DiscPen, SalInc, RPI, CPI, Pinc1, Pinc2, Pinc3, Pinc4, Pinc5, Pinc6, ActDuration, DefDuration, PenDuration, FutDuration);
        }
    }
}