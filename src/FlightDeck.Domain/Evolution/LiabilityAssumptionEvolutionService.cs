﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    public class LiabilityAssumptionEvolutionService
    {
        private readonly IBasesDataManager basesDataManager;
        private readonly IAssumptionIndexEvolutionService assumptionIndexEvolutionService;

        public LiabilityAssumptionEvolutionService(IBasesDataManager basesDataManager)
            : this(basesDataManager, new AssumptionIndexEvolutionService())
        {
        }

        public LiabilityAssumptionEvolutionService(IBasesDataManager basesDataManager, IAssumptionIndexEvolutionService assumptionIndexEvolutionService)
        {
            this.basesDataManager = basesDataManager;
            this.assumptionIndexEvolutionService = assumptionIndexEvolutionService;
        }

        public IDictionary<DateTime, IDictionary<AssumptionType, double>> Calculate(DateTime start, DateTime end)
        {
            var assumptionTypeCount = Enum.GetValues(typeof(AssumptionType)).Length;

            var liabilityAssumptionEvolution = new Dictionary<DateTime, IDictionary<AssumptionType, double>>((int)(end - start).TotalDays);

            var d = start;
            do
            {
                var basis = basesDataManager.GetBasis(d);
                var vals = new Dictionary<AssumptionType, double>(assumptionTypeCount);

                foreach (var t in Enum.GetValues(typeof(AssumptionType)))
                {
                    var type = (AssumptionType)t;

                    var val = assumptionIndexEvolutionService.GetAssumptionIndexValue(type, d, basis);

                    vals.Add(type, val);
                }
                liabilityAssumptionEvolution.Add(d, vals);
                d = d.AddDays(1);
            }
            while (d <= end);

            return liabilityAssumptionEvolution;
        }        
    }
}
