﻿using FlightDeck.Domain.LDI;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;

namespace FlightDeck.Domain.Evolution
{
    [Serializable]
    public class BasisEvolutionData
    {
        public IDictionary<DateTime, LiabilityEvolutionItem> LiabilityEvolution { get; set; }
        public IDictionary<DateTime, AssetEvolutionItem> AssetEvolution { get; set; }
        public IDictionary<DateTime, double> LiabilityExperience { get; set; }
        public IDictionary<DateTime, double> BuyinExperience { get; set; }
        public IDictionary<DateTime, double> AssetExperience { get; set; }

        public Lazy<IDictionary<DateTime, ExpectedDeficitData>> ExpectedDefit { get; set; }

        public BasisEvolutionData(
            IDictionary<DateTime, LiabilityEvolutionItem> liabEvolution,
            IDictionary<DateTime, AssetEvolutionItem> assetEvolution,
            IDictionary<DateTime, double> liabExperience,
            IDictionary<DateTime, double> buyinExperience,
            IDictionary<DateTime, double> assetExperience)
        {
            LiabilityEvolution = liabEvolution;
            AssetEvolution = assetEvolution;
            LiabilityExperience = liabExperience;
            BuyinExperience = buyinExperience;
            AssetExperience = assetExperience;
        }

        public static BasisEvolutionData CreateEmpty()
        {
            return new BasisEvolutionData
            (
                new Dictionary<DateTime, LiabilityEvolutionItem>(),
                new Dictionary<DateTime, AssetEvolutionItem>(),
                new Dictionary<DateTime, double>(),
                new Dictionary<DateTime, double>(),
                new Dictionary<DateTime, double>()
            );
        }

        public bool IsEmpty()
        {
            return LiabilityEvolution.Keys.Count + AssetEvolution.Keys.Count + LiabilityExperience.Keys.Count + BuyinExperience.Keys.Count + AssetExperience.Keys.Count == 0;
        }
    }
}