﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AssumptionIndexEvolutionService : IAssumptionIndexEvolutionService
    {
        double multiplyer = 0.01;

        public double GetAssumptionIndexValue(AssumptionType type, DateTime at, Basis b)
        {
            var assumption = b.UnderlyingAssumptionIndices[(AssumptionType)type];
            var initialValue = assumption.InitialValue;
            var index = assumption.Index;

            var val = .0;
            if (assumption.Index == null || at == b.EffectiveDate)
            {
                val = initialValue;
            }
            else
            {
                switch (assumption.Category)
                {
                    case AssumptionCategory.Fixed:
                        val = initialValue + (index.GetValue(at) - index.GetValue(b.EffectiveDate)) * multiplyer;
                        break;
                    case AssumptionCategory.Curved:
                        break;
                    case AssumptionCategory.BlackScholes:
                        break;
                    default:
                        throw new Exception(string.Format("Unexpected assumption category: '{0}'", assumption.Category.ToString()));
                }
            }

            return val;
        }

        public IDictionary<AssumptionType, double> GetAllAssumptionIndexValues(DateTime at, Basis b)
        {
            var values = Utils.EnumToArray<AssumptionType>().ToDictionary(x => x, x => new AssumptionIndexEvolutionService().GetAssumptionIndexValue(x, at, b));

            return values;
        }
    }
}
