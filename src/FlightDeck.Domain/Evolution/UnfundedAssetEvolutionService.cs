﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.LDI;
    using System;
    using System.Collections.Generic;

    public class UnfundedAssetEvolutionService : IAssetEvolutionService
    {
        public IEnumerable<AssetEvolutionItem> Calculate(DateTime start, DateTime end, IPortfolioManager portfolioManager, IDictionary<DateTime, LDIEvolutionItem> ldiEvolutionData = null)
        {
            int i = 0;
            do
            {
                yield return new AssetEvolutionItem(start.AddDays(i), .0, .0, .0, .0, .0, .0, .0, .0, .0);
                i++;
            }
            while (start.AddDays(i) <= end);
        }

        public void SetLastKnownPosition(DateTime knownAt, AssetEvolutionItem val)
        {
        }
    }
}
