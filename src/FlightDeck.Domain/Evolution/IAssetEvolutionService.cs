﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.LDI;
    using System;
    using System.Collections.Generic;

    public interface IAssetEvolutionService
    {
        IEnumerable<AssetEvolutionItem> Calculate(DateTime start, DateTime end, IPortfolioManager portfolioManager, IDictionary<DateTime, LDIEvolutionItem> ldiEvolutionData = null);
        void SetLastKnownPosition(DateTime knownAt, AssetEvolutionItem val);
    }
}
