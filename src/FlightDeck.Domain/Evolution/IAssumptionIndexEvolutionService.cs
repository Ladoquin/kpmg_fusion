﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    
    public interface IAssumptionIndexEvolutionService
    {
        double GetAssumptionIndexValue(AssumptionType type, DateTime at, Basis b);
        IDictionary<AssumptionType, double> GetAllAssumptionIndexValues(DateTime at, Basis b);
    }
}
