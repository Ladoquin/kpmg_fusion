﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.JourneyPlan;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ExpectedDeficitService
    {
        private readonly ISchemeData scheme;

        public ExpectedDeficitService(ISchemeData scheme)
        {
            this.scheme = scheme;
        }

        public IDictionary<DateTime, ExpectedDeficitData> Calculate(DateTime start, DateTime end, Basis basisAtStart, IPortfolio portfolioAtStart, BasisEvolutionData evolutionData)
        {
            var dayCount = (int)(end - start).TotalDays;

            var evolution = new Dictionary<DateTime, ExpectedDeficitData>(dayCount);

            var defaultAssumptions = new BasisAssumptions
                (
                    evolutionData.LiabilityEvolution[start].ToAssumptionsDictionary(),
                    basisAtStart.LifeExpectancy,
                    evolutionData.LiabilityEvolution[start].ToPensionIncreasesDictionary(),
                    portfolioAtStart.AssetData.Assets.ToDictionary(x => x.Class.Type, x => x.DefaultReturn),
                    new RecoveryPlanAssumptions(0, 0, 0, 0, 0, false, 0, 0)
                );

            var ctx = new BasisContext(
                start,
                end,
                basisAtStart.EffectiveDate,
                basisAtStart,
                portfolioAtStart,
                null,
                evolutionData,
                defaultAssumptions);

            // Assets and liabs each year
            var yearlyAssetLiabs = new JourneyPlanExpectedDeficitService(scheme).Calculate(ctx, portfolioAtStart).ToList();

            var yearMax = 20;
            var years = new List<int>(yearMax + 1);
            for (var i = 0; i <= yearMax; i++)
                years.Add(i);

            var days = new List<DateTime>(dayCount);
            for (var i = 0; start.AddDays(i) <= end; i++)
                days.Add(start.AddDays(i));

            // Deficit at each year and daily change
            var yearlyDeficitAndDailyChange =
                years.ToDictionary(
                    x => start.AddYears(x),
                    x => new
                    {
                        Deficit = yearlyAssetLiabs[x].Value.SurplusDeficit,
                        DailyChange = (yearlyAssetLiabs[x + 1].Value.SurplusDeficit - yearlyAssetLiabs[x].Value.SurplusDeficit) / (start.AddYears(x + 1) - start.AddYears(x)).TotalDays
                    });

            var lastDates = days.ToDictionary(
                    x => x,
                    x => yearlyDeficitAndDailyChange.Keys.Where(d => d <= x).OrderBy(d => d).Last());

            // Daily evolution of deficit (linear interpolation)
            var dailyDeficitEvolution =
                days.ToDictionary(
                    x => x,
                    x => yearlyDeficitAndDailyChange[lastDates[x]].Deficit
                         + (x - lastDates[x]).TotalDays
                         * yearlyDeficitAndDailyChange[lastDates[x]].DailyChange);

            // Daily change in assets and liabs
            var dailyChange =
                yearlyAssetLiabs
                    .Where(x => x.Key <= yearMax)
                    .ToDictionary(
                        x => start.AddYears(x.Key),
                        x => new
                        {
                            Liabs = x.Value.Liabilities,
                            Assets = x.Value.Assets,
                            DCLiabs = (yearlyAssetLiabs[x.Key + 1].Value.Liabilities - x.Value.Liabilities) / (start.AddYears(x.Key + 1) - start.AddYears(x.Key)).TotalDays,
                            DCAssets = (yearlyAssetLiabs[x.Key + 1].Value.Assets - x.Value.Assets) / (start.AddYears(x.Key + 1) - start.AddYears(x.Key)).TotalDays
                        });

            // Daily evolution of assets and liabs (linear interpolation)
            foreach (var day in days)
            {
                var expectedLiabs = dailyChange[lastDates[day]].Liabs + dailyChange[lastDates[day]].DCLiabs * (day - lastDates[day]).TotalDays;
                var expectedAssets = dailyChange[lastDates[day]].Assets + dailyChange[lastDates[day]].DCAssets * (day - lastDates[day]).TotalDays;
                var fundingLevel = expectedAssets / expectedLiabs;

                // daily evolution plus deficit from previous calc
                evolution.Add(day,
                   new ExpectedDeficitData
                    (
                        day,
                        expectedAssets,
                        expectedLiabs,
                        dailyDeficitEvolution[day],
                        fundingLevel
                    ));
            }

            return evolution;
        }
    }
}
