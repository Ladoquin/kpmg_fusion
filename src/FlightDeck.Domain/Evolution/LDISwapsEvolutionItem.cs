﻿namespace FlightDeck.Domain.Evolution
{
    using System;

    public class LDISwapsEvolutionItem
    {
        public DateTime Date { get; set; }
        public double LDIInterestSwap { get; set; }
        public double LDIInflationSwap { get; set; }
    }
}
