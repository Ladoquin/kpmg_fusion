﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    static class LiabilityEvolutionItemExtensions
    {
        public static IDictionary<AssumptionType, double> ToAssumptionsDictionary(this LiabilityEvolutionItem liabilityEvolutionItem)
        {
            return new Dictionary<AssumptionType, double>
                {
                    { AssumptionType.PreRetirementDiscountRate, liabilityEvolutionItem.DiscPre },
                    { AssumptionType.PostRetirementDiscountRate, liabilityEvolutionItem.DiscPost },
                    { AssumptionType.PensionDiscountRate, liabilityEvolutionItem.DiscPen },
                    { AssumptionType.SalaryIncrease, liabilityEvolutionItem.SalInc },
                    { AssumptionType.InflationRetailPriceIndex, liabilityEvolutionItem.RPI },
                    { AssumptionType.InflationConsumerPriceIndex, liabilityEvolutionItem.CPI }
                };
        }

        public static IDictionary<int, double> ToPensionIncreasesDictionary(this LiabilityEvolutionItem liabilityEvolutionItem)
        {
            return new Dictionary<int, double>
                {
                    { 1, liabilityEvolutionItem.Pinc1 },
                    { 2, liabilityEvolutionItem.Pinc2 },
                    { 3, liabilityEvolutionItem.Pinc3 },
                    { 4, liabilityEvolutionItem.Pinc4 },
                    { 5, liabilityEvolutionItem.Pinc5 },
                    { 6, liabilityEvolutionItem.Pinc6 }
                };
        }

        /// <summary>
        /// Get the duration figures from the liability evolution to a dictionary. 
        /// </summary>
        /// <param name="liab"></param>
        /// <returns></returns>
        public static Dictionary<MemberStatus, double> ToDurationDictionary(this LiabilityEvolutionItem liabilityEvolutionItem)
        {
            return new Dictionary<MemberStatus, double>
                {
                    { MemberStatus.ActivePast, liabilityEvolutionItem.ActDuration },
                    { MemberStatus.Deferred, liabilityEvolutionItem.DefDuration },
                    { MemberStatus.Pensioner, liabilityEvolutionItem.PenDuration },
                    { MemberStatus.ActiveFuture, liabilityEvolutionItem.FutDuration }
                };
        }
    }
}
