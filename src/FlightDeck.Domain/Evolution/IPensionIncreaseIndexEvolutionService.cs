﻿namespace FlightDeck.Domain.Evolution
{
    using System;
    using System.Collections.Generic;
    
    public interface IPensionIncreaseIndexEvolutionService
    {
        IDictionary<int, double> GetPensionIncreaseIndexValue(DateTime at, Basis b);
    }
}
