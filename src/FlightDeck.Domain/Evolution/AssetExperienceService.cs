﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.Domain.Assets;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AssetExperienceService
    {
        public IDictionary<DateTime, double> CalculateStepChange(IPortfolioManager portfolioManager, IDictionary<DateTime, AssetEvolutionItem> evolution, IDictionary<DateTime, LDI.LDIEvolutionItem> LDISwapsEvolution)
        {
            var experience = new Dictionary<DateTime, double>();

            var idx = 0;
            var effectiveDates = portfolioManager.GetEffectiveDates().OrderBy(ed => ed)
                .ToDictionary(x => idx++, x => x);

            if (effectiveDates.Count > 1)
            {
                for (int i = 1; i < effectiveDates.Count; i++)
                {
                    var prevEffectiveDate = effectiveDates[i - 1];
                    var effectiveDate = effectiveDates[i];

                    // Need the assetVal that would exist if there wasn't a step change
                    // To work this out without running through the whole asset evolution we can use the already calculated evolution
                    // value on the day before the step change, then find out the evolution from that day to the next
                    var assetVal = evolution[effectiveDate.AddDays(-1)].AssetValue;

                    var portfolio = portfolioManager.GetPortfolio(prevEffectiveDate);

                    var growth = portfolio.GetGrowth(effectiveDate.AddDays(-1), effectiveDate);

                    var netIncome = portfolio.GetNetIncome(effectiveDate);

                    assetVal = assetVal * growth + netIncome;

                    if (portfolio.AssetData.LDIInForce && LDISwapsEvolution.ContainsKey(effectiveDate))
                    {
                        assetVal = assetVal + LDISwapsEvolution[effectiveDate].LDIInterestSwap + LDISwapsEvolution[effectiveDate].LDIInflationSwap;
                    }

                    var marketValue = portfolioManager.GetPortfolio(effectiveDate).MarketValue;

                    experience.Add(effectiveDate, marketValue - assetVal);
                }
            }

            return experience;
        }
    }
}
