﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    public class PensionIncreaseIndexEvolutionService : IPensionIncreaseIndexEvolutionService
    {        
        int penIncCount = 6;
        double multiplyer = 0.01;
        BlackScholesCalculator blackScholesCalculator;

        public PensionIncreaseIndexEvolutionService()
        {
            blackScholesCalculator = new BlackScholesCalculator();
        }

        public IDictionary<int, double> GetPensionIncreaseIndexValue(DateTime at, Basis b)
        {
            var effectiveDate = b.EffectiveDate;
            var vals = new Dictionary<int, double>(penIncCount);

            for (int pIncRef = 1; pIncRef <= penIncCount; pIncRef++)
            {
                var pinc = b.UnderlyingPensionIncreaseIndices[pIncRef];
                var initialValue = pinc.InitialValue;
                var index = pinc.Index;

                var val = .0;
                if (index == null)
                {
                    val = initialValue;
                }
                else
                {
                    switch (pinc.Category)
                    {
                        case AssumptionCategory.Fixed:
                            val = initialValue + (index.GetValue(at) - index.GetValue(effectiveDate)) * multiplyer;
                            break;
                        case AssumptionCategory.Curved:
                            break;
                        case AssumptionCategory.BlackScholes:
                            var notionalInflation = blackScholesCalculator.GetNotionalInflation(initialValue, pinc.InflationVolatility, pinc.Minimum, pinc.Maximum);
                            var currentInflation = notionalInflation + (index.GetValue(at) - index.GetValue(effectiveDate)) * multiplyer;
                            val = Utils.GetBlackScholesValue(currentInflation, pinc.InflationVolatility, 1, pinc.Minimum, pinc.Maximum);
                            break;
                        default:
                            throw new Exception(string.Format("Unexpected pension increase category: '{0}'", pinc.Category.ToString()));
                    }
                }

                vals.Add(pIncRef, val);
            }

            return vals;
        }
    }
}
