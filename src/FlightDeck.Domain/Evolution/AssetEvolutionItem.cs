﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System;

    [Serializable]
    public class AssetEvolutionItem : IEquatable<AssetEvolutionItem>
    {
        public DateTime Date { get; private set; }
        public double EmployeeContributions { get; private set; }
        public double EmployerContributions { get; private set; }
        public double TotalBenefits { get; private set; }
        public double NetBenefits { get; private set; }
        public double NetIncome { get; private set; }
        public double Growth { get; private set; }
        public double LDIInterestSwap { get; private set; }
        public double LDIInflationSwap { get; private set; }
        public double AssetValue { get; private set; }

        public AssetEvolutionItem(
            DateTime date,
            double employeeContributions,
            double employerContributions,
            double totalBenefits,
            double netBenefits,
            double netIncome,
            double growth,
            double ldiInterestSwap,
            double ldiInflationSwap,
            double assetValue)
        {
            Date = date;
            EmployeeContributions = employeeContributions;
            EmployerContributions = employerContributions;
            TotalBenefits = totalBenefits;
            NetBenefits = netBenefits;
            NetIncome = netIncome;
            Growth = growth;
            LDIInterestSwap = ldiInterestSwap;
            LDIInflationSwap = ldiInflationSwap;
            AssetValue = assetValue;
        }

        public bool Equals(AssetEvolutionItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date.Date == other.Date.Date
                && Utils.EqualityCheck(EmployeeContributions, other.EmployeeContributions)
                && Utils.EqualityCheck(EmployerContributions, other.EmployerContributions)
                && Utils.EqualityCheck(TotalBenefits, other.TotalBenefits)
                && Utils.EqualityCheck(NetBenefits, other.NetBenefits)
                && Utils.EqualityCheck(NetIncome, other.NetIncome)
                && Utils.EqualityCheck(Growth, other.Growth)
                && Utils.EqualityCheck(AssetValue, other.AssetValue)
                && Utils.EqualityCheck(LDIInterestSwap, other.LDIInterestSwap)
                && Utils.EqualityCheck(LDIInflationSwap, other.LDIInflationSwap);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((AssetEvolutionItem)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ EmployeeContributions.GetHashCode();
                hashCode = (hashCode * 397) ^ EmployerContributions.GetHashCode();
                hashCode = (hashCode * 397) ^ TotalBenefits.GetHashCode();
                hashCode = (hashCode * 397) ^ NetBenefits.GetHashCode();
                hashCode = (hashCode * 397) ^ NetIncome.GetHashCode();
                hashCode = (hashCode * 397) ^ Growth.GetHashCode();
                hashCode = (hashCode * 397) ^ LDIInterestSwap.GetHashCode();
                hashCode = (hashCode * 397) ^ LDIInflationSwap.GetHashCode();
                hashCode = (hashCode * 397) ^ AssetValue.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Date: {0}, EmployeeContributions: {1}, EmployerContributions: {2}, TotalBenefits: {3}, NetBenefits: {4}, NetIncome: {5}, Growth: {6}, LDIInterestSwap: {7}, LDIInflationSwap: {8}, AssetVaue: {9}", Date.ToString("dd/MM/yyyy"), EmployeeContributions, EmployerContributions, TotalBenefits, NetBenefits, NetIncome, Growth, LDIInterestSwap, LDIInflationSwap, AssetValue);
        }
    }
}
