﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.Domain.JourneyPlan;
    using FlightDeck.DomainShared;
    using System;
    using System.Linq;

    class AttributionCalculator
    {
        private readonly ISchemeContext scheme;
        private readonly BasisEvolutionData evolution;

        public AttributionCalculator(ISchemeContext scheme, BasisEvolutionData evolution)
        {
            this.scheme = scheme;
            this.evolution = evolution;
        }

        /// <summary>
        /// Attribution calcs from Tracker worksheet in Tracker.
        /// </summary>
        /// <remarks>
        /// This deliberately excludes AttrEROA. Other calcs are working out their own value for expected returns and 
        /// ignoring the one in the Attribution group. This means Attribution results can be based purely on Evolution results, 
        /// which is necessary when calculating historical results, ie, in the case of the USGAAP disclosure.
        /// /// </remarks>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public AttributionData Calculate(DateTime start, DateTime end)
        {
            var period = (end - start).TotalDays / Utils.DaysInAYear;

            var discPre = evolution.LiabilityEvolution[start].DiscPre;

            var assetEvolutionRange = evolution.AssetEvolution.Where(x => x.Key >= start && x.Key <= end).OrderBy(x => x.Key).ToList();

            var eeeContsReceived = assetEvolutionRange.Sum(x => x.Value.EmployeeContributions);
            var eerContsReceived = assetEvolutionRange.Sum(x => x.Value.EmployerContributions);
            var totBenefitOutgo = assetEvolutionRange.Sum(x => x.Value.TotalBenefits);
            var totNetBenefitOutgo = assetEvolutionRange.Sum(x => x.Value.NetBenefits);

            var totalContsReceived = eeeContsReceived + eerContsReceived;

            var recoveryPlanConts = scheme.Data.Funded ? new RecoveryPlanCalculator(scheme).GetPayment(scheme.Data.RecoveryPlanData, start, end) : 0;

            var serviceCost = evolution.LiabilityEvolution[start].Fut * period * Math.Pow(1 + discPre, period / 2);

            var assetsAtStart = evolution.AssetEvolution[start].AssetValue + evolution.LiabilityEvolution[start].PenBuyIn + evolution.LiabilityEvolution[start].DefBuyIn;

            var interestIncome = assetsAtStart * (Math.Pow(1 + discPre, period) - 1) + (totalContsReceived + recoveryPlanConts - totBenefitOutgo) * (Math.Pow(1 + discPre, (period / 2)) - 1);

            var adminExpenses = scheme.Data.Ias19ExpenseLoading * assetsAtStart * period;

            return new AttributionData(eeeContsReceived, eerContsReceived, totalContsReceived, interestIncome, recoveryPlanConts, adminExpenses, serviceCost, totBenefitOutgo, totNetBenefitOutgo);
        }
    }
}
