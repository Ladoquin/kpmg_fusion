﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class LiabilityExperienceService
    {
        public enum LiabilityExperienceType
        {
            Liab,
            Buyin
        }

        /// <summary>
        /// Calculate the liability step changes - these occur where new liability 'uploads' exists, ie, if there is more than one 
        /// set of liability data of the same basis type in the scheme.
        /// </summary>
        /// <remarks>
        /// The Tracker calculates step changes when it is performing the main liability evolution calculations. Fusion has
        /// separate the step change calculations for performance reasons.
        /// </remarks>
        /// <returns></returns>
        public IDictionary<LiabilityExperienceType, IDictionary<DateTime, double>> CalculateLiabilityStepChange(IBasesDataManager basesDataManager, IDictionary<DateTime, LiabilityEvolutionItem> liabilityEvolution)
        {
            var experience = new Dictionary<LiabilityExperienceType, IDictionary<DateTime, double>>();
            experience.Add(LiabilityExperienceType.Liab, new Dictionary<DateTime, double>());
            experience.Add(LiabilityExperienceType.Buyin, new Dictionary<DateTime, double>());

            var idx = 0;
            var effectiveDates = basesDataManager.EffectiveDates.OrderBy(ed => ed)
                .ToDictionary(x => idx++, x => x);

            if (effectiveDates.Count > 1)
            {
                var lcfByEffectiveDate =
                    effectiveDates.Values
                        .ToDictionary(
                            x => x,
                            x => new LiabilityCashflowStatelessService(
                                basesDataManager.GetBasis(x)
                                    .Adjust(pensionIncreases: liabilityEvolution[x].ToPensionIncreasesDictionary())));

                for (int i = 1; i < effectiveDates.Count; i++)
                {
                    var prevEffectiveDate = effectiveDates[i - 1];
                    var effectiveDate = effectiveDates[i];

                    // Need to know what the assumptions are pre step change, which are the assumptions that would have existed in the evolution
                    // calculations had there NOT been a step change (ie, rebasing of assumptions). 
                    // Because we've already calculated the assumption evolution with the step change accounted for, we need to go
                    // back to the original indices for the 'raw' values needed here.
                    var prevBasis = basesDataManager.GetBasis(prevEffectiveDate);    
                    var prevAssumptions = prevBasis.UnderlyingAssumptionIndices;
                    var prevPincs = prevBasis.UnderlyingPensionIncreaseIndices;
                    var prevTracked = new BasisLiabilityAssumptions(
                        new Dictionary<AssumptionType, double> {
                            { AssumptionType.PreRetirementDiscountRate, prevAssumptions[AssumptionType.PreRetirementDiscountRate].GetValue(effectiveDate, prevEffectiveDate) },
                            { AssumptionType.PostRetirementDiscountRate, prevAssumptions[AssumptionType.PostRetirementDiscountRate].GetValue(effectiveDate, prevEffectiveDate) },
                            { AssumptionType.PensionDiscountRate, prevAssumptions[AssumptionType.PensionDiscountRate].GetValue(effectiveDate, prevEffectiveDate) },
                            { AssumptionType.SalaryIncrease, prevAssumptions[AssumptionType.SalaryIncrease].GetValue(effectiveDate, prevEffectiveDate) },
                            { AssumptionType.InflationRetailPriceIndex, prevAssumptions[AssumptionType.InflationRetailPriceIndex].GetValue(effectiveDate, prevEffectiveDate) },
                            { AssumptionType.InflationConsumerPriceIndex, prevAssumptions[AssumptionType.InflationConsumerPriceIndex].GetValue(effectiveDate, prevEffectiveDate) }
                        },
                        null,
                        new Dictionary<int, double> {
                            { 1, prevPincs[1].GetValue(effectiveDate, prevEffectiveDate) },
                            { 2, prevPincs[2].GetValue(effectiveDate, prevEffectiveDate) },
                            { 3, prevPincs[3].GetValue(effectiveDate, prevEffectiveDate) },
                            { 4, prevPincs[4].GetValue(effectiveDate, prevEffectiveDate) },
                            { 5, prevPincs[5].GetValue(effectiveDate, prevEffectiveDate) },
                            { 6, prevPincs[6].GetValue(effectiveDate, prevEffectiveDate) }
                        });

                    var NPV = lcfByEffectiveDate[prevEffectiveDate].AdjustTo(effectiveDate, prevTracked);

                    var buyInPre = NPV.Buyin[MemberStatus.Deferred] + NPV.Buyin[MemberStatus.Pensioner];

                    var stepChange = -NPV.Total;

                    var assumptionsAtDate = new BasisLiabilityAssumptions(liabilityEvolution[effectiveDate]);

                    NPV = lcfByEffectiveDate[effectiveDate].AdjustTo(effectiveDate, assumptionsAtDate);

                    stepChange += NPV.Total;

                    var buyInPost = NPV.Buyin[MemberStatus.Deferred] + NPV.Buyin[MemberStatus.Pensioner];

                    experience[LiabilityExperienceType.Liab].Add(effectiveDate, stepChange);
                    experience[LiabilityExperienceType.Buyin].Add(effectiveDate, buyInPost - buyInPre);
                }
            }

            return experience;
        }
    }
}
