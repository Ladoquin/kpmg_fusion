﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.Domain.Assets;
    using LDI;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AssetEvolutionService : IAssetEvolutionService
    {
        KeyValuePair<DateTime, AssetEvolutionItem> lastKnownPosition;

        /// <summary>
        /// Calculate the value of assets tracked over time.
        /// </summary>
        /// <param name="start">When to track the assets from, typically the first anchor date.</param>
        /// <param name="end">When to track the assets till, typically the reresh date.</param>
        /// <param name="portfolioManager">Portfolio manager containing scheme's asset portolios.</param>
        /// <param name="LDISwapsEvolution">Calculated during Liability evolution, required for asset evolution.</param>
        /// <returns>Tracked assets.</returns>

        public IEnumerable<AssetEvolutionItem> Calculate(DateTime start, DateTime end, IPortfolioManager portfolioManager, IDictionary<DateTime, LDIEvolutionItem> ldiEvolutionData = null)
        {
            var assetTracker = new List<AssetEvolutionItem>((int)(end - start).TotalDays);

            var changeDate = 
                portfolioManager.GetEffectiveDates()
                    .Where(d => d > start)
                    .Union(new List<DateTime> { DateTime.MaxValue }) // to help loop below after last effective date
                        .OrderBy(d => d)
                            .GetEnumerator();

            changeDate.MoveNext();

            var valDate = start;
            
            var portfolio = portfolioManager.GetPortfolio(valDate);

            var assetVal = 0d;

            if (lastKnownPosition.Key.Date == valDate.AddDays(-1))
            {
                // we know the position yesterday so want the calculations to pick up from that day

                assetVal = lastKnownPosition.Value.AssetValue;

                valDate = valDate.AddDays(-1);
            }
            else
            {
                assetVal = portfolio.MarketValue;

                assetTracker.Add(new AssetEvolutionItem(start, 0, 0, 0, 0, 0, 1, 0, 0, assetVal));
            }

            //TODO can this be run in parallel?
            do
            {
                valDate = valDate.AddDays(1);

                var growth = portfolio.GetGrowth(valDate.AddDays(-1), valDate);

                var netIncome = portfolio.GetNetIncome(valDate);

                LDI.LDIEvolutionItem ldiItem = null;
                if (ldiEvolutionData != null)
                    ldiEvolutionData.TryGetValue(valDate, out ldiItem);

                var ldiInterestSwap = ldiItem != null
                                        ? ldiItem.LDIInterestSwap 
                                        : .0;
                var ldiInflationSwap = ldiItem != null
                                        ? ldiItem.LDIInflationSwap
                                        : .0;

                assetVal = assetVal * growth + netIncome + ldiInterestSwap + ldiInflationSwap;

                if (valDate == changeDate.Current)
                {
                    portfolio = portfolioManager.GetPortfolio(valDate);

                    assetVal = portfolio.MarketValue;

                    changeDate.MoveNext();
                }

                var employeeContributions = portfolio.GetEmployeeContributions(valDate);
                var employerContributions = portfolio.GetEmployerContributions(valDate);
                var totalBenefitOutgo = portfolio.GetBenefitOutgo(valDate, ProfitType.Gross);
                var netBenefitOutgo = portfolio.GetBenefitOutgo(valDate, ProfitType.Net);

                var assetTrackerItem = new AssetEvolutionItem(valDate, employeeContributions, employerContributions, totalBenefitOutgo, netBenefitOutgo, netIncome, growth, ldiInterestSwap, ldiInflationSwap, assetVal);

                assetTracker.Add(assetTrackerItem);
            }
            while (valDate < end);

            return assetTracker;
        }

        public void SetLastKnownPosition(DateTime knownAt, AssetEvolutionItem val)
        {
            lastKnownPosition = new KeyValuePair<DateTime, AssetEvolutionItem>(knownAt, val);
        }
    }
}
