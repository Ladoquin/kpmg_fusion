﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    public class PensionIncreaseEvolutionService
    {
        private readonly IBasesDataManager basesDataManager;
        private readonly IPensionIncreaseIndexEvolutionService pensionIncreaseIndexEvolutionService;

        public PensionIncreaseEvolutionService(IBasesDataManager basesDataManager)
            : this(basesDataManager, new PensionIncreaseIndexEvolutionService())
        {
        }
        public PensionIncreaseEvolutionService(IBasesDataManager basesDataManager, IPensionIncreaseIndexEvolutionService pensionIncreaseIndexEvolutionService)
        {
            this.basesDataManager = basesDataManager;
            this.pensionIncreaseIndexEvolutionService = pensionIncreaseIndexEvolutionService;
        }

        public IDictionary<DateTime, IDictionary<int, double>> Calculate(DateTime start, DateTime end)
        {
            var pensionIncreasesEvolution = new Dictionary<DateTime, IDictionary<int, double>>((int)(end - start).TotalDays);

            var d = start;
            do
            {
                var basis = basesDataManager.GetBasis(d); //TODO check performance

                var vals = pensionIncreaseIndexEvolutionService.GetPensionIncreaseIndexValue(d, basis);

                pensionIncreasesEvolution.Add(d, vals);
                d = d.AddDays(1);
            }
            while (d <= end);

            return pensionIncreasesEvolution;
        }
    }
}
