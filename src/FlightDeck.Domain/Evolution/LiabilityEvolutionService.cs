﻿namespace FlightDeck.Domain.Evolution
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class LiabilityEvolutionService
    {
        /// <summary>
        /// Calculate the evolution of liabilities.
        /// </summary>
        /// <remarks>
        /// The data returned corresponds to the liability amounts in the evolution results on the Tracker worksheet.
        /// It uses the assumption and pension increases evolution which must be calculated before using this method.
        /// Data for each day can be calculated independently but to allow the parallel processing
        /// the StepChange figures have been excluded. 
        /// These can be calculated separately using the results from this method.
        /// </remarks>
        /// <param name="start"></param>
        /// <param name="endDate"></param>
        /// <param name="basesDataManager"></param>
        /// <param name="assumptionEvolution"></param>
        /// <param name="lcf"></param>
        /// <returns></returns>
        public IEnumerable<LiabilityEvolutionItem> Calculate(DateTime start, DateTime endDate, IBasesDataManager basesDataManager, IDictionary<DateTime, IDictionary<AssumptionType, double>> assumptionEvolution, IDictionary<DateTime, IDictionary<int, double>> pensionIncreasesEvolution)
        {
            var liabilitiesEvolution = new List<LiabilityEvolutionItem>((int)(endDate - start).TotalDays);

            var effectiveDates = basesDataManager.EffectiveDates.OrderBy(d => d).ToList();

            var lcfByEffectiveDate =
                basesDataManager.EffectiveDates
                    .OrderBy(x => x)
                    .ToDictionary(
                        x => x,
                        x => new LiabilityCashflowStatelessService(
                            basesDataManager.GetBasis(x).Adjust(pensionIncreases: pensionIncreasesEvolution[x])));

            Parallel.For(0, (int)(endDate - start).TotalDays + 1, i =>
                {
                    var date = start.AddDays(i);
                    var ed = lcfByEffectiveDate.Keys.Last(d => d <= date);
                    var assumptionsAtDate = new BasisLiabilityAssumptions(assumptionEvolution[date], null, pensionIncreasesEvolution[date]);

                    var lcf = lcfByEffectiveDate[ed];

                    var NPV = lcf.AdjustTo(date, assumptionsAtDate);

                    var assumptions = assumptionEvolution[date];
                    var pincs = pensionIncreasesEvolution[date];

                    var l = new LiabilityEvolutionItem(date,
                        NPV.Liabilities[MemberStatus.ActivePast], NPV.Liabilities[MemberStatus.Deferred], NPV.Liabilities[MemberStatus.Pensioner], NPV.Liabilities[MemberStatus.ActiveFuture],
                        NPV.Total,
                        NPV.Buyin[MemberStatus.Deferred], NPV.Buyin[MemberStatus.Pensioner],
                        assumptions[AssumptionType.PreRetirementDiscountRate],
                        assumptions[AssumptionType.PostRetirementDiscountRate],
                        assumptions[AssumptionType.PensionDiscountRate],
                        assumptions[AssumptionType.SalaryIncrease],
                        assumptions[AssumptionType.InflationRetailPriceIndex],
                        assumptions[AssumptionType.InflationConsumerPriceIndex],
                        pincs[1],
                        pincs[2],
                        pincs[3],
                        pincs[4],
                        pincs[5],
                        pincs[6],
                        NPV.Duration[MemberStatus.ActivePast], NPV.Duration[MemberStatus.Deferred], NPV.Duration[MemberStatus.Pensioner], NPV.Duration[MemberStatus.ActiveFuture]);

                    lock (liabilitiesEvolution)
                    {
                        liabilitiesEvolution.Add(l);
                    }
                });

            return liabilitiesEvolution;
        }
    }
}
