﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Accounting;
    using FlightDeck.Domain.Analysis;
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.Domain.JourneyPlan;
    using FlightDeck.Domain.LDI;
    using FlightDeck.Domain.VaR;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class BasisAnalysisService
    {
        private readonly ISchemeContext scheme;

        public enum CalculationMode
        {
            CalculateEverything, // will calculate initialisation data too
            CalculateFromJourneyPlanBefore,
            CalculateFromRecoveryPlan,
            CalculateFromJourneyPlanAfter,
            CalculateFromVarWaterFall
        }

        public BasisAnalysisService(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public BasisAnalysisResults Calculate(IBasisContext ctx, CalculationMode mode = CalculationMode.CalculateEverything)
        {
            if (ctx.Results == null)
            {
                ctx.Results = new BasisAnalysisResults();
            }

            if (mode <= CalculationMode.CalculateEverything || ctx.Results.StickyResults == null)
            {
                ctx.Results = calculateAggregations(ctx);
                ctx.Results.StickyResults = new BasisAnalysisResultsStickyData();
            }

            /*
             * HACK!
             * This isn't great, due to limitations with the current tracker we need to run the JP calcs twice.
             * The problems are with cyclical references for BuyIn and Synthetic asset calculations. 
             * The calculations for both rely on values that are only populated after a JPAfter run. 
             * Easiest solutions for now is run the JP calcs twice, hence this call to calculatePreProjection.
             */
            calculatePreProjection(ctx);

            if (mode <= CalculationMode.CalculateFromJourneyPlanBefore || ctx.Results.JourneyPlanBefore == null)
            {
                ctx.Results = calculateUserLiabs(ctx);

                ctx.Results.PIEInit = new PIEInitCalculator().Calculate(ctx);

                var jpBefore = new JourneyPlanBeforeService(scheme).Calculate(ctx);

                ctx.Results.JourneyPlanBefore = jpBefore.YearlyTotals
                    .ToDictionary(
                        x => x.Key,
                        x => new BalanceData(x.Value.TotalAssets, x.Value.TotalLiability));

                ctx.Results.JPInvGrowth = jpBefore.YearlyTotals.Last().Value.Growth;

                ctx.Results.StickyResults.PreFRODeferredLiability = jpBefore.PreFRODeferredLiability;

                //ETV and FB have before components
                ctx.Results.ETV = new EtvData(jpBefore.PreETVDeferredLiability, 0, 0, 0, 0, 0, 0, 0, 0);
                ctx.Results.FutureBenefits =           
                    new FutureBenefitsData(0, 0, jpBefore.EmploymentRate, 0, 0, 0, 0, jpBefore.ServiceCostEstimate, 
                    jpBefore.ContributionRateEstimate, jpBefore.EstimatedBenefitReduction, 0, 0);
            }
            if (ctx.Results.RecoveryPlanPayments == null 
                || (mode <= CalculationMode.CalculateFromRecoveryPlan && scheme.Assumptions.RecoveryPlanOptions != RecoveryPlanType.NewFixed))
            {
                var year = ctx.AnalysisDate.Year;
                var rp = new RecoveryPlanCalculator(scheme).Calculate(ctx);
                ctx.Results.RecoveryPlanPaymentRequired = rp.PaymentRequired;
                ctx.Results.RecoveryPlanPayments = rp.Plan.ToDictionary(
                    x => year++,
                    x => new BeforeAfter<double>(x.Before, x.After));
            }
            if (mode <= CalculationMode.CalculateFromJourneyPlanAfter || ctx.Results.JourneyPlanAfter == null)
            {
                var jpAfter = new JourneyPlanAfterService(scheme).Calculate(ctx);

                ctx.Results.JourneyPlanAfter = jpAfter.YearlyTotals.ToDictionary(x => x.Key, x => new BalanceData(x.Value.TotalAssets, x.Value.TotalLiability));

                ctx.Results.JP2InvGrowth = jpAfter.YearlyTotals.Last().Value.Growth;
             
                if (scheme.Assumptions.FROType == FROType.Bulk)
                {
                    ctx.Results.FRO = calculateFRO(jpAfter.FRO, ctx.Results);
                    ctx.Results.ETV = calculateETV(jpAfter.ETV);
                    ctx.Results.FutureBenefits = calculateBenefitChanges(jpAfter.FutureBenefits, ctx);
                }
                else if (scheme.Assumptions.FROType == FROType.Embedded)
                {
                    ctx.Results.FRO = new FroData(0, 0, 0, ctx.Results.UserDefLiab, ctx.Results.StickyResults.PreFRODeferredLiability);
                    ctx.Results.EFRO = calculateEFRO(jpAfter.EFRO, ctx.Results, ctx.AnalysisDate, ctx.EffectiveDate);
                }

                ctx.Results.AdjustedCashflow = jpAfter.Cashflow;
                ctx.Results.Cashflow = calculateCashflow(jpAfter.Cashflow, false, ctx.AnalysisDate, ctx.Results.EFRO);
                ctx.Results.BuyinCashflow = calculateCashflow(jpAfter.Cashflow, true, ctx.AnalysisDate);

                ctx.Results.PIE = calculatePIE(jpAfter.PIE, ctx.Results);

                ctx.Results = calculateJourneyPlanAfterDependentResults(ctx);
            }
            if (mode <= CalculationMode.CalculateFromVarWaterFall || ctx.Results.VarWaterfall == null)
            {
                ctx.Results.GiltBasisLiabilityData = new LDI.GiltLiabilityCalculator(scheme).Calculate(ctx);
                ctx.Results.Hedging = new HedgeAnalysisCalculator(scheme, ctx).Calculate();
                ctx.Results.VaRInputs = new VaRInputsCalculator(scheme).Calculate(ctx);
                ctx.Results.VarWaterfall = new WaterfallCalculator(scheme, ctx).Calculate();
                ctx.Results.VarFunnel = new FunnelCalculator(scheme).Calculate(ctx);
            }

            return ctx.Results;
        }

        private void calculatePreProjection(IBasisContext context)
        {
            //Have to clone certain instances to prevent changes in here affecting the context passed in upon method return
            var results = new BasisAnalysisResults
                {
                    AssetsAtSED = context.Results.AssetsAtSED,
                    BuyInAtAnalysisDate = context.Results.BuyInAtAnalysisDate,
                    StickyResults = new BasisAnalysisResultsStickyData()
                    {
                        PreFRODeferredLiability = context.Results.StickyResults.PreFRODeferredLiability
                    }
                };
            var assetsPortfolio = scheme.Data.Funded
                ? (IPortfolio)new AssetPortfolio(context.Portfolio.AssetData, scheme.Data.SyntheticAssets, scheme.Data.ContributionRates, scheme.Data.RecoveryPlan, new BasesDataManager(new List<Basis> { context.Data }))
                : (IPortfolio)new EmptyPortfolio();
            var ctx = new BasisContext(context.StartDate, context.AnalysisDate, context.EffectiveDate, context.Data, assetsPortfolio, results, context.EvolutionData, context.Assumptions);

            ctx.Results = calculateUserLiabs(ctx);

            ctx.Results.PIEInit = new PIEInitCalculator().Calculate(ctx);

            var jpAfter = new JourneyPlanAfterService(scheme).Calculate(ctx);
            if (scheme.Assumptions.FROType == FROType.Bulk)
            {
                ctx.Results.FRO = calculateFRO(jpAfter.FRO, ctx.Results);
                ctx.Results.ETV = calculateETV(jpAfter.ETV);
                ctx.Results.FutureBenefits = calculateBenefitChanges(jpAfter.FutureBenefits, ctx);
            }
            else if (scheme.Assumptions.FROType == FROType.Embedded)
            {
                ctx.Results.FRO = new FroData(0, 0, 0, ctx.Results.UserDefLiab, ctx.Results.StickyResults.PreFRODeferredLiability);
                ctx.Results.EFRO = calculateEFRO(jpAfter.EFRO, ctx.Results, ctx.AnalysisDate, ctx.EffectiveDate);
            }
            ctx.Results.PIE = calculatePIE(jpAfter.PIE, ctx.Results);
            ctx.Results.UserActLiabAfter = ctx.Results.UserActLiab - ctx.Results.FutureBenefits.LiabilitiesChange - ctx.Results.EFRO.ChangeInActivesPastLiabilitiesDischarged;
            ctx.Results.UserDefLiabAfter = ctx.Results.UserDefLiab - ctx.Results.FRO.LiabilityDischarged - ctx.Results.ETV.LiabilityDischarged - ctx.Results.EFRO.ChangeInDeferredsLiabilitiesDischarged;
            ctx.Results.UserPenLiabAfter = ctx.Results.UserPenLiab - (ctx.Results.PIE.Liability.Before - ctx.Results.PIE.Liability.After);

            ctx.Results.Insurance = new BuyinCalculator(scheme).Calculate(ctx);
            var buyinCost = calculateBuyinCost(ctx);

            //Now save the results that should persist
            scheme.Calculated.BuyinCost =  buyinCost;
            context.Results = context.Results ?? new BasisAnalysisResults();
            context.Results.InvestmentStrategy = new InvestmentStrategyCalculator(scheme).Calculate(ctx);
        }

        private BasisAnalysisResults calculateAggregations(IBasisContext ctx)
        {
            ctx.Results.AssetsAtSED = ctx.EvolutionData.AssetEvolution[ctx.AnalysisDate].AssetValue + ctx.EvolutionData.LiabilityEvolution[ctx.AnalysisDate].DefBuyIn + ctx.EvolutionData.LiabilityEvolution[ctx.AnalysisDate].PenBuyIn;
            ctx.Results.BuyInAtAnalysisDate = ctx.EvolutionData.LiabilityEvolution[ctx.AnalysisDate].PenBuyIn + ctx.EvolutionData.LiabilityEvolution[ctx.AnalysisDate].DefBuyIn;

            return ctx.Results;
        }

        private BasisAnalysisResults calculateUserLiabs(IBasisContext ctx)
        {
            var userLiabs = new UserLiabsCalculator().Calculate(ctx);
            ctx.Results.UserActLiab = userLiabs.ActivePast;
            ctx.Results.UserDefLiab = userLiabs.Deferred;
            ctx.Results.UserPenLiab = userLiabs.Pensioners;
            ctx.Results.UserActDuration = userLiabs.ActDuration;
            ctx.Results.UserDefDuration = userLiabs.DefDuration;
            ctx.Results.UserPenDuration = userLiabs.PenDuration;
            ctx.Results.UserDefBuyIn = userLiabs.DeferredBuyin;
            ctx.Results.UserPenBuyIn = userLiabs.PensionerBuyin;

            return ctx.Results;
        }

        private IDictionary<int, IDictionary<MemberStatus, double>> calculateCashflow(IEnumerable<CashflowData> cashflowOutput, bool isBuyin, DateTime analysisDate, EFroData efro = null)
        {
            //aggregate (adjusted) cashflow output
            var adjustedCashflowOutput = new Dictionary<int, IDictionary<MemberStatus, double>>(JourneyPlanBaseService.MaxYear);
            for (int i = 0; i <= JourneyPlanBaseService.MaxYear; i++)
            {
                adjustedCashflowOutput.Add(analysisDate.AddYears(i).Year,
                    new Dictionary<MemberStatus, double>
                        {
                            { MemberStatus.ActivePast, cashflowOutput.Where(cf => cf.MemberStatus == MemberStatus.ActivePast && cf.IsBuyIn == isBuyin).Sum(cf => cf.Cashflow.ContainsKey(i + 1) ? cf.Cashflow[i + 1] : 0)},
                            { MemberStatus.Deferred, cashflowOutput.Where(cf => cf.MemberStatus == MemberStatus.Deferred && cf.IsBuyIn == isBuyin).Sum(cf => cf.Cashflow.ContainsKey(i + 1) ? cf.Cashflow[i + 1] : 0)},
                            { MemberStatus.Pensioner, cashflowOutput.Where(cf => cf.MemberStatus == MemberStatus.Pensioner && cf.IsBuyIn == isBuyin).Sum(cf => cf.Cashflow.ContainsKey(i + 1) ? cf.Cashflow[i + 1] : 0) },
                            { MemberStatus.ActiveFuture, cashflowOutput.Where(cf => cf.MemberStatus == MemberStatus.ActiveFuture && cf.IsBuyIn == isBuyin).Sum(cf => cf.Cashflow.ContainsKey(i + 1) ? cf.Cashflow[i + 1] : 0) },
                        });

                adjustedCashflowOutput[analysisDate.AddYears(i).Year][MemberStatus.ActivePast] += (efro == null || !efro.ActiveTransferValuesPayable.ContainsKey(i + 1) ? 0 : efro.ActiveTransferValuesPayable[i + 1]);
                adjustedCashflowOutput[analysisDate.AddYears(i).Year][MemberStatus.Deferred] += (efro == null || !efro.DeferredTransferValuesPayable.ContainsKey(i + 1) ? 0 : efro.DeferredTransferValuesPayable[i + 1]);
            }

            return adjustedCashflowOutput;
        }

        private FroData calculateFRO(JourneyPlanAfterResult.FROImpact fro, IBasisAnalysisResults results)
        {
            var cetvRatio = scheme.Assumptions.FROOptions.EquivalentTansferValue.MathSafeDiv(fro.PreFRODefLiab);

            return new FroData(cetvRatio, fro.AssetsDisinvested, fro.LiabilityDischarged, results.UserDefLiab, fro.PreFRODefLiab);
        }

        private EFroData calculateEFRO(JourneyPlanAfterResult.EFROImpact efro, IBasisAnalysisResults results, DateTime analysisDate, DateTime effectiveDate)
        {
            if (efro == null)
                return new EFroData(results.UserActLiab + results.UserDefLiab);

            var cetvPaid = scheme.Assumptions.EFROOptions.EmbeddedFroBasisChange * efro.TotalLiabilityDischarged;

            var changeInActLiabDischargedOutput = efro.ChangeInLiabsDischarged[MemberStatus.ActivePast] * (1 - scheme.Assumptions.EFROOptions.EmbeddedFroBasisChange);
            var changeInDefLiabDischargedOutput = efro.ChangeInLiabsDischarged[MemberStatus.Deferred] * (1 - scheme.Assumptions.EFROOptions.EmbeddedFroBasisChange);

            /*
             * The below transformation must happen after the Journey Plan calcs, as they require the pre-transformed values.
             * Then things like aggregated cashflow calcs use the transformed ones.
             */

            var cetv = new Dictionary<MemberStatus, Dictionary<int, double>>
                {
                    { MemberStatus.ActivePast, new Dictionary<int, double>(efro.ActiveTransferValuesPayable) },
                    { MemberStatus.Deferred, new Dictionary<int, double>(efro.DeferredTransferValuesPayable) }
                };

            //How many complete and part years are "in the past"?  (i.e. before mEffectiveDate)
            var p = (analysisDate - effectiveDate).TotalDays / Utils.DaysInAYear;
            var yAdd = (int)Math.Floor(p);
            p = p - yAdd;

            for (int y = 1; y <= Utils.MaxYear - yAdd; y++)
            {
                var r = 1 + y;

                cetv[MemberStatus.ActivePast][y] = cetv[MemberStatus.ActivePast][y + yAdd] * (1 - p);
                cetv[MemberStatus.Deferred][y] = cetv[MemberStatus.Deferred][y + yAdd] * (1 - p);
                if (y + yAdd < Utils.MaxYear)
                {
                    cetv[MemberStatus.ActivePast][y] += cetv[MemberStatus.ActivePast][y + yAdd + 1] * p;
                    cetv[MemberStatus.Deferred][y] += cetv[MemberStatus.Deferred][y + yAdd + 1] * p;
                }
            }

            return new EFroData(cetv[MemberStatus.ActivePast], cetv[MemberStatus.Deferred], cetvPaid, efro.TotalLiabilityDischarged,
                changeInActLiabDischargedOutput, changeInDefLiabDischargedOutput, efro.NonPensionersLiabilities);
        }

        private EtvData calculateETV(JourneyPlanAfterResult.ETVImpact etv)
        {
            var args = scheme.Assumptions.ETVOptions;

            var enhancedTVsPaid = etv.AssetsDisinvested * (1 + args.EnhancementToCetv);

            var cetvRatio = args.EquivalentTransferValue.MathSafeDiv(etv.PreETVDefLiab);

            var equivalentEtv = args.EquivalentTransferValue * (1 + args.EnhancementToCetv);

            var costToCompany = enhancedTVsPaid - etv.UnenhancedTVsPaid;

            var changeInPosition = etv.LiabilityDischarged - etv.UnenhancedTVsPaid;

            return new EtvData(etv.PreETVDefLiab, cetvRatio, equivalentEtv, etv.UnenhancedTVsPaid, enhancedTVsPaid, etv.LiabilityDischarged, costToCompany, changeInPosition, etv.AssetsDisinvested);
        }

        private PieData calculatePIE(PIECashflowImpact pie, IBasisAnalysisResults results)
        {
            var PIEInit = results.PIEInit;
            var args = scheme.Assumptions.PIEOptions;

            var valueAfter = pie.ValueNilIncs * pie.UpliftFactor;

            var PIEPenLiabEligible = PIEInit.PenLiab * args.PortionEligible;

            var valueOfFutureIncs = PIEInit.ValueOfIncs * args.PortionEligible;

            var PIEPenLiabNilIncs = PIEPenLiabEligible - valueOfFutureIncs;

            var PIEMaxUplift = PIEInit.PenLiab == 0 ? 0 : PIEInit.ValueOfIncs.MathSafeDiv(PIEInit.PenLiab - PIEInit.ValueOfIncs);

            var PIELiabAfter = results.UserPenLiab + args.TakeUpRate * (PIEPenLiabNilIncs * (1 + args.ActualUplift) - PIEPenLiabEligible);

            var changeInFundingPosition = results.UserPenLiab - PIELiabAfter;

            var PIEValueShared = args.ActualUplift.MathSafeDiv(PIEMaxUplift);

            return new PieData(new BeforeAfter<double>(results.UserPenLiab, PIELiabAfter), PIEInit.PenLiab, changeInFundingPosition, PIEPenLiabEligible, PIEPenLiabNilIncs, PIEInit.ValueOfIncs, 0, 0, PIEMaxUplift, PIEValueShared);
        }

        private FutureBenefitsData calculateBenefitChanges(JourneyPlanAfterResult.BenefitChangesImpact fb, IBasisContext ctx) //TODO does this handle when there is no future service cost?
        {
            var args = scheme.Assumptions.FutureBenefitOptions;

            var liabilitiesChange = fb.LiabilitiesBefore - fb.LiabilitiesAfter;

            var salaryRollAtAnalysisDate = new BasisQueries().GetSalaryRollAtDate(ctx.Data, ctx.AnalysisDate);

            var schemeConts = scheme.Data.ContributionRates.Where(r => r.Date <= ctx.AnalysisDate).OrderBy(r => r.Date).Last();

            var employeeRate = schemeConts.Employee + args.MemberContributionsIncrease;

            var annualCost = args.AdjustmentType == BenefitAdjustmentType.DcImplementation
                ? (args.AdjustmentRate + employeeRate) * salaryRollAtAnalysisDate
                : fb.ServiceCostAfter;

            var contRate = annualCost.MathSafeDiv(salaryRollAtAnalysisDate);

            var employerRate = Math.Max(0, contRate - employeeRate);

            var FBBestEstimateContRate = fb.BestEstimateServiceCost.MathSafeDiv(salaryRollAtAnalysisDate);

            var estimatedAdjustment = Math.Round((FBBestEstimateContRate - scheme.Data.ContractingOutContributionRate).MathSafeDiv(FBBestEstimateContRate), 4);

            var FBChangeInAnnualCost = annualCost - fb.ServiceCostBefore;

            var FBChangeInErRate = FBChangeInAnnualCost.MathSafeDiv(salaryRollAtAnalysisDate) - args.MemberContributionsIncrease;

            return new FutureBenefitsData(annualCost, contRate, employeeRate, employerRate, FBChangeInAnnualCost, FBChangeInErRate, liabilitiesChange, fb.BestEstimateServiceCost, FBBestEstimateContRate, estimatedAdjustment, 0, 0);
        }

        private BasisAnalysisResults calculateJourneyPlanAfterDependentResults(IBasisContext ctx)
        {
            ctx.Results.UserActLiabAfter = ctx.Results.UserActLiab - ctx.Results.FutureBenefits.LiabilitiesChange - ctx.Results.EFRO.ChangeInActivesPastLiabilitiesDischarged;
            ctx.Results.UserDefLiabAfter = ctx.Results.UserDefLiab - ctx.Results.FRO.LiabilityDischarged - ctx.Results.ETV.LiabilityDischarged - ctx.Results.EFRO.ChangeInDeferredsLiabilitiesDischarged;
            ctx.Results.UserPenLiabAfter = ctx.Results.UserPenLiab - (ctx.Results.PIE.Liability.Before - ctx.Results.PIE.Liability.After);

            //Note, the ordering is important here as some calculators require the results of previous calculators
            ctx.Results.Attribution = new AttributionCalculator(scheme, ctx.EvolutionData).Calculate(ctx.StartDate, ctx.AnalysisDate);
            ctx.Results.Insurance = new BuyinCalculator(scheme).Calculate(ctx);
            if (ctx.Data.Type == BasisType.Buyout)
            {
                scheme.Calculated.BuyinCost = calculateBuyinCost(ctx);
            }
            else
            {
                if (scheme.Calculated.BuyinCost == null)
                {
                    scheme.Calculated.BuyinCost = new BuyinCost(0, 0);
                }
                if (ctx.Results.StickyResults.Strains == null)
                {
                    ctx.Results.StickyResults.Strains = new Dictionary<int, IDictionary<StrainType, double>>();
                }
            }
            ctx.Results.InsurancePointChangeImpact = new InsuranceCalculator(scheme).GetBasisPointChangeImpact(ctx);
            ctx.Results.InvestmentStrategy = new InvestmentStrategyCalculator(scheme).Calculate(ctx);
            ctx.Results.WeightedReturns = new WeightedReturnsCalculator().Calculate(scheme, ctx);
            ctx.Results.FundingLevel = new FundingLevelCalculator(scheme).Calculate(ctx);
            ctx.Results.ABF = new AbfCalculator(scheme).Calculate(ctx);
            ctx.Results.ProfitLoss = new ProfitLossCalculator(scheme).Calculate(ctx); //TODO needed on non-accounting bases?             
            ctx.Results.SurplusAnalysis = new SurplusAnalysisCalculator(scheme).Calculate(ctx);
            if (ctx.Data.Type == BasisType.Accounting)
            {
                ctx.Results.BalanceSheet = new BalanceSheetCalculator().Calculate(ctx);

                if (scheme.Data.AccountingIAS19Visible)
                    ctx.Results.IAS19 = new IAS19ProfitLossCalculator(scheme).Calculate(ctx);
                if (scheme.Data.AccountingFRS17Visible)
                    ctx.Results.FRS17 = new IAS19ProfitLossCalculator(scheme).Calculate(ctx); //FRS17 is now FRS102 which is the same as IAS19 data
            }

            return ctx.Results;
        }

        private BuyinCost calculateBuyinCost(IBasisContext ctx)
        {
            var InsPenCost = ctx.Results.Insurance.PensionerLiabilityToBeInsured;
            var InsNonPenCost = ctx.Results.Insurance.NonPensionerLiabilityToBeInsured;

            return new BuyinCost(InsPenCost, InsNonPenCost);
        }
    }
}
