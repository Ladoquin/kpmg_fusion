﻿namespace FlightDeck.Domain
{
    public interface ILogDbInteractionScope
    {
        IRepository<T> GetRepository<T>() where T : class;
        void Dispose();
    }
}
