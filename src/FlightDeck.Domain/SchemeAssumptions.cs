﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public class SchemeAssumptions : ISchemeAssumptions
    {
        public FROType FROType { get; private set; }
        public FlexibleReturnOptions FROOptions { get; private set; }
        public EmbeddedFlexibleReturnOptions EFROOptions { get; private set; }
        public EnhancedTransferValueOptions ETVOptions { get; private set; }
        public PieOptions PIEOptions { get; private set; }
        public FutureBenefitsParameters FutureBenefitOptions { get; private set; }
        public IDictionary<AssetClassType, double> InvestmentStrategyAssetAllocation { get; private set; }
        public InvestmentStrategyOptions InvestmentStrategyOptions { get; private set; }
        public AbfOptions ABFOptions { get; private set; }
        public InsuranceParameters InsuranceOptions { get; private set; }
        public RecoveryPlanType RecoveryPlanOptions { get; private set; }
        public WaterfallOptions WaterfallOptions { get; set; }

        public SchemeAssumptions(
            FROType froType,
            FlexibleReturnOptions froOptions,
            EmbeddedFlexibleReturnOptions efroOptions, 
            EnhancedTransferValueOptions etvOptions,
            PieOptions pieOptions,
            FutureBenefitsParameters futureBenefitOptions,
            IDictionary<AssetClassType, double> investmentStrategyAssetAllocation,
            InvestmentStrategyOptions investmentStrategyOptions,
            AbfOptions abfOptions,
            InsuranceParameters insuranceOptions,
            RecoveryPlanType recoveryPlanOptions,
            WaterfallOptions waterfallOptions
            )
        {
            FROType = froType;
            FROOptions = froOptions;
            EFROOptions = efroOptions;
            ETVOptions = etvOptions;
            PIEOptions = pieOptions;
            FutureBenefitOptions = futureBenefitOptions;
            InvestmentStrategyAssetAllocation = investmentStrategyAssetAllocation;
            InvestmentStrategyOptions = investmentStrategyOptions;
            ABFOptions = abfOptions;
            InsuranceOptions = insuranceOptions;
            RecoveryPlanOptions = recoveryPlanOptions;
            WaterfallOptions = waterfallOptions;
        }
    }
}
