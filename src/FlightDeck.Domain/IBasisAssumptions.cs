﻿namespace FlightDeck.Domain
{
    /// <summary>
    /// Current state of basis options.
    /// </summary>
    public interface IBasisAssumptions : IBasisLiabilityAssumptions, IBasisAssetAssumptions
    {
    }
}
