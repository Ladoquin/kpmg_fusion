﻿using FlightDeck.Domain;

public interface IDbTransactionalScope : IDbInteractionScope
{
    void Commit();
}