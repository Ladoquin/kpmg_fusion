﻿namespace FlightDeck.Domain.Accounting
{
    using FlightDeck.DomainShared;
    using System;
    
    class IAS19ProfitLossCalculator
    {
        private readonly ISchemeContext scheme;

        public IAS19ProfitLossCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public IAS19Disclosure.IAS19PAndLForecastData Calculate(IBasisContext ctx)
        {
            var userDiscPre = ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate];
            var userDiscPen = ctx.Assumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate];

            var assetsAtStart = ctx.Results.InvestmentStrategy.TotalCoreAssets + ctx.Results.InvestmentStrategy.Buyin;

            var serviceCost = ctx.Results.ProfitLoss.ServiceCostIAS;

            var schemeExpenses = scheme.Data.Ias19ExpenseLoading * assetsAtStart;

            var interestCredit = -assetsAtStart * userDiscPre - ctx.Results.ProfitLoss.NetInvestments * (Math.Pow(1 + userDiscPre, 0.5) - 1);

            var interestCharge = (ctx.Results.UserActLiabAfter + ctx.Results.UserDefLiabAfter) * userDiscPre + ctx.Results.UserPenLiabAfter * userDiscPen - ctx.Results.ProfitLoss.BenefitsPaid * (Math.Pow(1 + userDiscPen, 0.5) - 1);

            var netInterest = interestCredit + interestCharge;

            var total = serviceCost + schemeExpenses + netInterest;

            return new IAS19Disclosure.IAS19PAndLForecastData(serviceCost, schemeExpenses, interestCredit, interestCharge, netInterest, total);
        }
    }
}
