﻿namespace FlightDeck.Domain.Accounting
{
    using FlightDeck.DomainShared;
    using System;
    
    class FRS17ProfitLossCalculator
    {
        private readonly ISchemeContext scheme;

        public FRS17ProfitLossCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public FRS17Disclosure.FRS17PAndLForecastData Calculate(IBasisContext ctx)
        {
            var r = ctx.Results;
            var userDiscPre = ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate];
            var userDiscPen = ctx.Assumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate];

            var serviceCost = r.ProfitLoss.ServiceCostFRS;

            var interestCost = (r.UserActLiabAfter + r.UserDefLiabAfter) * userDiscPre + r.UserPenLiabAfter * userDiscPen - r.ProfitLoss.BenefitsPaid * (Math.Pow(1 + userDiscPen, 0.5) - 1)
                        + (r.ProfitLoss.ServiceCostIAS - r.ProfitLoss.ServiceCostFRS);

            var assetsAtStart = r.InvestmentStrategy.TotalCoreAssets + r.InvestmentStrategy.Buyin;
            var userTotBuyin = r.UserDefBuyIn + r.UserPenBuyIn;
            var contsSubtotal = (r.ProfitLoss.EmployerContributions + r.ProfitLoss.DeficitContributions + r.ProfitLoss.MemberContributions + r.ProfitLoss.BenefitsPaidInsured) - r.ProfitLoss.BenefitsPaid;

            var expectedReturn = -(r.JP2InvGrowth * (assetsAtStart - userTotBuyin) + userDiscPre * userTotBuyin + (contsSubtotal * (Math.Pow(1 + r.JP2InvGrowth, 0.5) - 1))) + scheme.Data.Ias19ExpenseLoading * assetsAtStart;

            return new FRS17Disclosure.FRS17PAndLForecastData(serviceCost, interestCost, expectedReturn, serviceCost + interestCost + expectedReturn);
        }
    }
}
