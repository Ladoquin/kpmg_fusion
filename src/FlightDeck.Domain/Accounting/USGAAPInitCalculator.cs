﻿using FlightDeck.Domain.Assets;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Domain.Accounting
{
    public class USGAAPInitCalculator
    {
        private readonly ISchemeContext scheme;
        private readonly MasterBasis masterBasis;
        private readonly IBasesDataManager basesDataManager;
        private readonly IPortfolioManager portfolioManager;
        private readonly IBasisContext context;

        public USGAAPInitCalculator(ISchemeContext scheme, IBasisContext ctx, MasterBasis masterBasis, IPortfolioManager portfolioManager)
        {
            this.scheme = scheme;
            this.masterBasis = masterBasis;
            this.basesDataManager = new BasesDataManager(masterBasis.Bases.Values);
            this.portfolioManager = portfolioManager;
            this.context = ctx;
        }

        public USGAAPInitData Calculate(DateTime start, DateTime analysisDate)
        {            
            var initData = calculateUSGAAPTable(start, analysisDate);

            return initData;
        }

        private USGAAPInitData calculateUSGAAPTable(DateTime attributionStart, DateTime analysisDate)
        {
            var initData = new USGAAPInitData(new Dictionary<DateTime, AOCIData>(10));

            foreach (var effectiveDate in basesDataManager.EffectiveDates.Where(ed => ed <= analysisDate))
            {
                var b = basesDataManager.GetBasis(effectiveDate);

                initData.AOCITable.Add(effectiveDate, new AOCIData(b.ActuarialBalanceInAOCI, b.PriorServiceBalanceInAOCI, b.AmortisationOfPriorService, b.AmortisationPeriod));
            }

            var accountingPeriods = new MasterBasisQueries().GetAccountingPeriods(scheme.Calculated.RefreshDate, masterBasis, analysisDate).ToList();

            double? closingBalance = null;

            // fill any gaps for previous accounting periods that didn't have data supplied
            // the aoci table includes data for the current period because values from the current period are used in calculating the forecast
            // so the us gaap calculator might be run twice for the current period, once below in Historical mode, then again when downloading the disclosure (in Full mode).

            List<DateTime> effectiveDates = new List<DateTime>();
            effectiveDates.AddRange(accountingPeriods.Where(p => p > initData.AOCITable.Keys.Max()));
            if (!effectiveDates.Contains(context.StartDate))
                effectiveDates.Add(context.StartDate);

            var giltYeildAtStart = new GiltYieldService(basesDataManager.GetBasis(attributionStart)).GetGiltYield(attributionStart);

            foreach (var period in effectiveDates.OrderBy(d => d))
            {
                var start = initData.AOCITable.Where(k => k.Key <= period).OrderBy(k => k.Key).Last().Key;

                if (start == period && start != analysisDate)
                    continue;

                var b = basesDataManager.GetBasis(period);
                var pf = portfolioManager.GetPortfolio(period);

                var ctx = new BasisContext(
                    start, 
                    period,
                    b.EffectiveDate,
                    b,
                    pf,
                    context.Results,
                    context.EvolutionData,
                    context.Assumptions);

                initData.ExpectedReturns = calculateExpectedReturns(start, period);

                var assumptionsSource = context.AnalysisDate == period ? AssumptionsSourceType.Client : AssumptionsSourceType.Default;

                var usgaapCalc = new USGAAPCalculator(scheme, initData, context.EvolutionData, basesDataManager, portfolioManager).GetHistoric(start, period, giltYeildAtStart, ctx, assumptionsSource);

                var aociClosingBalance = usgaapCalc.ComprehensiveAccumulatedIncome.Total;
                var unrecognisedPSC = usgaapCalc.NetPeriodicPensionCost.AmortisationOfPriorService + initData.AOCITable[start].UnrecognisedPriorServiceCost;

                if (!initData.AOCITable.ContainsKey(period)) 
                    initData.AOCITable.Add(period, new AOCIData(aociClosingBalance, unrecognisedPSC, initData.AOCITable[start].AmortisationOfPriorServiceCost, initData.AOCITable[start].ActuarialGainAmortisation));

                closingBalance = aociClosingBalance;
            }

            return initData;
        }

        private IDictionary<USGAAPExpectedReturnsType, double> calculateExpectedReturns(DateTime start, DateTime analysisDate)
        {
            var expectedReturns = new Dictionary<USGAAPExpectedReturnsType, double>();

            expectedReturns.Add(USGAAPExpectedReturnsType.AnalysisEnd, new USGAAPExpectedReturnsAtDateCalculator(context.EvolutionData, basesDataManager, portfolioManager).GetExpectedReturn(analysisDate));
            expectedReturns.Add(USGAAPExpectedReturnsType.AnalysisStart, new USGAAPExpectedReturnsAtDateCalculator(context.EvolutionData, basesDataManager, portfolioManager).GetExpectedReturn(start));
            expectedReturns.Add(USGAAPExpectedReturnsType.UsersInputs, new USGAAPExpectedReturnsCustomCalculator(scheme, context).GetExpectedReturn());

            return expectedReturns;
        }
    }
}
