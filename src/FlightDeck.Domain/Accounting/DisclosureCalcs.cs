﻿namespace FlightDeck.Domain.Accounting
{
    using System;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using System.Linq;
    using FlightDeck.Domain.Evolution;    

    /// <remarks>
    /// Calculations and cell references valid as of Tracker v044c.
    /// </remarks>
    class DisclosureCalcs
    {
        private readonly ISchemeContext scheme;
        private readonly StartEnd<LiabilityEvolutionItem> liabEv;
        private readonly StartEnd<AssetEvolutionItem> assetEv;
        private readonly DateTime periodEnd;
        private readonly DateTime periodStart;
        private readonly IDisclosureContextResultsProvider contextResultsProvider;
        private readonly IExpectedRateOfReturnOnAssetsProvider ExpectedRateOfReturnsProvider;

        public AssumptionsSourceType AssumptionsSource { get; set; }

        public DisclosureCalcs(ISchemeContext scheme, DateTime start, DateTime end, BasisEvolutionData evolution, IDisclosureContextResultsProvider contextResultsProvider, IExpectedRateOfReturnOnAssetsProvider expectedRateOfReturnsProvider)
        {
            this.scheme = scheme;
            this.periodEnd = end;
            this.periodStart = start;
            this.liabEv = new StartEnd<LiabilityEvolutionItem>(evolution.LiabilityEvolution[periodStart], evolution.LiabilityEvolution[periodEnd]);
            this.assetEv = new StartEnd<AssetEvolutionItem>(evolution.AssetEvolution[periodStart], evolution.AssetEvolution[periodEnd]);
            this.contextResultsProvider = contextResultsProvider;
            this.ExpectedRateOfReturnsProvider = expectedRateOfReturnsProvider;            
        }

        public double AttrPeriod
        {
            get
            {
                return (periodEnd - periodStart).TotalDays / Utils.DaysInAYear;
            }
        }

        public double TrackerBC53
        {
            get
            {
                return D12; //I think this is right. It's coming from two different Excel cells but it looks like it's the same thing.
            }
        }

        public double C23
        {
            get
            {
                return AssumptionsSource == AssumptionsSourceType.Default
                    ? -liabEv.End.Liabs
                    : -(contextResultsProvider.UserLiab.ActivePast + contextResultsProvider.UserLiab.Deferred + contextResultsProvider.UserLiab.Pensioners);
            }
        }
        public double C24
        {
            get
            {
                return assetEv.End.AssetValue + liabEv.End.DefBuyIn + liabEv.End.PenBuyIn;
            }
        }

        public double C25
        {
            get
            {
                return C23 + C24;
            }
        }

        public double C31
        {
            get
            {
                return liabEv.Start.Liabs;
            }
        }

        public double C32
        {
            get
            {
                return contextResultsProvider.Attribution.AttrServiceCost - contextResultsProvider.Attribution.AttrEeeContsReceived;
            }
        }

        public double C33
        {
            get
            {
                return C31 * (Math.Pow(1 + D12, AttrPeriod) - 1) + (C35 + C36) * (Math.Pow(1 + D12, (AttrPeriod / 2)) - 1);
            }
        }

        public double C34
        {
            get
            {
                return C37 - (C31 + C32 + C33 + C35 + C36);
            }
        }

        public double C35
        {
            get
            {
                return contextResultsProvider.Attribution.AttrEeeContsReceived;
            }
        }

        public double C36
        {
            get
            {
                return -contextResultsProvider.Attribution.AttrTotBenefitOutgo;
            }
        }

        public double C37
        {
            get
            {
                return -C23;
            }
        }

        public double C42
        {
            get
            {
                return D24;
            }
        }

        public double C43
        {
            get
            {
                return contextResultsProvider.Attribution.AttrInterestIncome;
            }
        }

        public double C46
        {
            get
            {
                return contextResultsProvider.Attribution.AttrEerContsReceived + contextResultsProvider.Attribution.AttrRecoveryPlanConts;
            }
        }

        public double C47
        {
            get
            {
                return contextResultsProvider.Attribution.AttrEeeContsReceived;
            }
        }

        public double C48
        {
            get
            {
                return C36;
            }
        }

        public double C49
        {
            get
            {
                return C24;
            }
        }

        /// <summary>
        /// Tracked Pre Retirement Discount rate for the start of disclosure period
        /// </summary>
        public double D12
        {
            get
            {
                return liabEv.Start.DiscPre;
            }
        }

        public double D23
        {
            get
            {
                return -liabEv.Start.Liabs;
            }
        }

        public double D24
        {
            get
            {
                return assetEv.Start.AssetValue + liabEv.Start.DefBuyIn + liabEv.Start.PenBuyIn;
            }
        }

        public double D25
        {
            get
            {
                return D23 + D24;
            }
        }

        public double I33
        {
            get
            {
                return contextResultsProvider.Attribution.AttrServiceCost / (Math.Pow(1 + TrackerBC53, AttrPeriod / 2)) - contextResultsProvider.Attribution.AttrEeeContsReceived;
            }
        }

        public double I34
        {
            get
            {
                return C31 * (Math.Pow(1 + D12, AttrPeriod) - 1) + (C35 + C36) * (Math.Pow(1 + D12, AttrPeriod / 2) - 1) + contextResultsProvider.Attribution.AttrServiceCost * (Math.Pow(1 + TrackerBC53, AttrPeriod / 2) - 1);
            }
        }

        public double I43
        {
            get
            {
                return C42;
            }
        }

        public double I44
        {
            get
            {
                var j16 = ExpectedRateOfReturnsProvider.ExpectedRateOfReturnAtPeriodStart;
                return I43 * (Math.Pow(1 + j16, AttrPeriod) - 1) + (I46 + I47 + I48) * (Math.Pow(1 + j16, (AttrPeriod * 0.5)) - 1) - contextResultsProvider.Attribution.AttrSchemeAdminExpenses;
            }
        }

        public double I45
        {
            get
            {
                return I49 - (I43 + I44 + I46 + I47 + I48);
            }
        }

        public double I46
        {
            get
            {
                return C46;
            }
        }

        public double I47
        {
            get
            {
                return C47;
            }
        }

        public double I48
        {
            get
            {
                return C48;
            }
        }

        public double I49
        {
            get
            {
                return C49;
            }
        }

        public double J25
        {
            get
            {
                return D24;
            }
        }

        public double O56
        {
            get
            {
                return I44;
            }
        }
    }
}
