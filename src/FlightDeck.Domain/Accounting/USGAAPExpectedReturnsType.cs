﻿namespace FlightDeck.Domain.Accounting
{
    public enum USGAAPExpectedReturnsType : byte
    {
        AnalysisStart,
        AnalysisEnd,
        UsersInputs
    }
}
