﻿namespace FlightDeck.Domain.Accounting
{
    using FlightDeck.Domain.Analysis;
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// TODO rewrite this! This is now all over the place with different liability assumptions flying about left right and also centre. 
    /// </summary>
    public class USGAAPCalculator : IDisclosureContextResultsProvider, IExpectedRateOfReturnOnAssetsProvider
    {
        private readonly ISchemeContext scheme;
        private readonly IDictionary<DateTime, AOCIData> AOCITable;
        private readonly IDictionary<USGAAPExpectedReturnsType, double> expectedReturns;
        private readonly BasisEvolutionData evolution;
        private readonly IBasesDataManager basesDataManager;
        private readonly IPortfolioManager portfolioManager;
        private DateTime start;
        private DateTime end;
        private BasisAnalysisResults contextResults;
        private FRS17ProfitLossCalculator frs17PandLCalculator;
        private DisclosureCalcs calcs;

        private Lazy<UserLiabilityData> userLiab;
        private Lazy<AttributionData> attribution;
        private IDictionary<AssetClassType, double> clientAssetReturns;
        private double giltYieldAtStart;

        public UserLiabilityData UserLiab
        {
            get
            {
                return userLiab.Value;
            }
        }

        public AttributionData Attribution
        {
            get
            {
                return attribution.Value;
            }
        }

        public USGAAPCalculator(ISchemeContext scheme, USGAAPInitData initData, BasisEvolutionData evolution, IBasesDataManager basesDataManager, IPortfolioManager portfolioManager, IDictionary<AssetClassType, double> clientAssetReturns = null)
            : this(scheme, initData, evolution, basesDataManager, portfolioManager, null, clientAssetReturns)
        {
        }
        public USGAAPCalculator(ISchemeContext scheme, USGAAPInitData initData, BasisEvolutionData evolution, IBasesDataManager basesDataManager, IPortfolioManager portfolioManager, BasisAnalysisResults contextResults, IDictionary<AssetClassType, double> clientAssetReturns = null)
        {
            this.scheme = scheme;
            this.AOCITable = initData.AOCITable;
            this.expectedReturns = initData.ExpectedReturns;
            this.basesDataManager = basesDataManager;
            this.portfolioManager = portfolioManager;
            this.evolution = evolution;
            this.contextResults = contextResults;
            this.clientAssetReturns = clientAssetReturns;

            this.frs17PandLCalculator = new FRS17ProfitLossCalculator(scheme);            
        }

        public USGAAPDisclosure GetStartPositionSummary(DateTime start, DateTime end)
        {
            this.start = start;
            this.end = end;
            this.calcs = new DisclosureCalcs(scheme, start, end, evolution, this, this);

            var assumptions = getAssumptionsDataPrev(basesDataManager.GetBasis(start));
            var balanceSheet = new USGAAPDisclosure.USGAAPBalanceSheetAmountData(calcs.D23, calcs.D24, calcs.D25);

            return new USGAAPDisclosure(start, assumptions, null, balanceSheet, null, null, null, null, null, null, null, null);
        }

        private void initLazyCalculators(DateTime start, DateTime end, IBasisContext ctx = null)
        {
            if (ctx == null)
            {
                var b = basesDataManager.GetBasis(end);
                var pf = portfolioManager.GetPortfolio(end);
                var ev = evolution.LiabilityEvolution[end];
                ctx = new BasisContext(
                    start, end,
                    b.EffectiveDate,
                    b, pf,
                    null,
                    evolution,
                    new BasisAssumptions(
                        ev.ToAssumptionsDictionary(), b.LifeExpectancy, ev.ToPensionIncreasesDictionary(),
                        pf.AssetData.Assets.ToDictionary(x => x.Class.Type, x => x.DefaultReturn),
                        new RecoveryPlanAssumptions(0, 0, 0, 0, 0, false, 0, 0))
                        ); //TODO are the asset and recovery plan assumptions right here? 
            }
            else
            {
                clientAssetReturns = ctx.Assumptions.AssetAssumptions;
            }

            userLiab = new Lazy<UserLiabilityData>(() => new UserLiabsCalculator().Calculate(ctx));
            attribution = new Lazy<AttributionData>(() => new AttributionCalculator(scheme, evolution).Calculate(start, end));
        }

        public USGAAPDisclosure GetHistoric(DateTime start, DateTime end, double giltYieldAtStart, IBasisContext clientContext, AssumptionsSourceType assumptionsSource = AssumptionsSourceType.Default)
        {
            this.start = start;
            this.end = end;
            this.giltYieldAtStart = giltYieldAtStart;
            this.calcs = new DisclosureCalcs(scheme, start, end, evolution, this, this);
            this.calcs.AssumptionsSource = assumptionsSource;

            initLazyCalculators(start, end, clientContext);

            var netPeriodicPensionCost = getNetPeriodicPensionCostData();
            var comprehensiveAccumulatedIncome = getComprehensiveAccumulatedIncomeData();

            return new USGAAPDisclosure(end, null, null, null, null, null, null, null, netPeriodicPensionCost, null, comprehensiveAccumulatedIncome, null);
        }

        public USGAAPDisclosure Get(IBasisContext ctx, bool useClientAssumptions)
        {
            this.start = ctx.StartDate;
            this.end = ctx.AnalysisDate;
            this.calcs = new DisclosureCalcs(scheme, start, end, evolution, this, this);

            initLazyCalculators(start, end, ctx);

            var assumptionsSource = !useClientAssumptions ? AssumptionsSourceType.Default : AssumptionsSourceType.Client;

            calcs.AssumptionsSource = assumptionsSource;

            var positionAtStartDate = basesDataManager.GetBasis(start);

            var assumptions1 = getAssumptionsData(ctx.Assumptions);
            var assumptions2 = getAssumptionsDataPrev(positionAtStartDate);
            var balanceSheetAmounts = getBalanceSheetAmountData(assumptionsSource);
            var presentValue = getChangesInPresentValueData();
            var fairValue = getChangesInFairValueData();
            var abo = getAccumulatedBenefitObligationData(useClientAssumptions ? ctx.Assumptions : (IBasisLiabilityAssumptions)new BasisLiabilityAssumptions(evolution.LiabilityEvolution[end]));
            var futurePayments = getEstimatedPaymentData(ctx.Results.Cashflow);
            var netPeriodic = getNetPeriodicPensionCostData();
            var comprehensiveIncome = getComprehensiveIncomeData(presentValue, fairValue, netPeriodic);
            var comprehensiveAccumalatedIncome = getComprehensiveAccumulatedIncomeData();
            var periodicPensionCostForecast = getNetPeriodicPensionCostForecastData(ctx);

            return new USGAAPDisclosure(
                end,
                assumptions1,
                assumptions2,
                balanceSheetAmounts,
                presentValue,
                fairValue,
                abo,
                futurePayments,
                netPeriodic,
                comprehensiveIncome,
                comprehensiveAccumalatedIncome,
                periodicPensionCostForecast);
        }

        private double JPInvGrowthAtStart()
        {
            var eroa = portfolioManager.GetPortfolio(start).GetExpectedReturn(giltYieldAtStart, clientAssetReturns);

            return eroa;
        }

        private double USGAAPAmortActLossPeriod
        {
            get
            {
                return AOCITable[start].ActuarialGainAmortisation;
            }
        }
        private double O24
        {
            get
            {
                return expectedReturns[USGAAPExpectedReturnsType.AnalysisStart]; //P16
            }
        }
        private double O35
        {
            get
            {
                return calcs.C23;
            }
        }
        private double O36
        {
            get
            {
                return calcs.C24;
            }
        }
        private double O43
        {
            get
            {
                return calcs.C31;
            }
        }
        private double O44
        {
            get
            {
                return calcs.I33;
            }
        }
        private double O45
        {
            get
            {
                return calcs.I34;
            }
        }
        private double O46
        {
            get
            {
                return O50 - (O43 + O44 + O45 + O47 + O48 + O49);
            }
        }
        private double O47
        {
            get
            {
                return 0;
            }
        }
        private double O48
        {
            get
            {
                return calcs.C35;
            }
        }
        private double O49
        {
            get
            {
                return calcs.C36;
            }
        }
        private double O50
        {
            get
            {
                return calcs.C37;
            }
        }
        private double O55
        {
            get
            {
                return calcs.C42;
            }
        }
        private double O56
        {
            get
            {
                return O55 * (Math.Pow(1 + O24, calcs.AttrPeriod) - 1) + (O58 + O59 + O60) * (Math.Pow(1 + O24, calcs.AttrPeriod / 2) - 1) - Attribution.AttrSchemeAdminExpenses;
            }
        }
        private double O57
        {
            get
            {
                return O61 - (O55 + O56 + O58 + O59 + O60);
            }
        }
        private double O58
        {
            get
            {
                return calcs.C46;
            }
        }
        private double O59
        {
            get
            {
                return calcs.C47;
            }
        }
        private double O60
        {
            get
            {
                return calcs.C48;
            }
        }
        private double O61
        {
            get
            {
                return calcs.C49;
            }
        }
        private double O81
        {
            get
            {
                return -S78 * S81;
            }
        }
        private double O82
        {
            get
            {
                return Math.Abs(S82) < S79 
                    ? 0
                    : -Math.Sign(S82) * (Math.Abs(S82) - S79) / AOCITable[start].ActuarialGainAmortisation * S78;
            }
        }
        private double O88
        {
            get
            {
                return -(O57 - O46);
            }
        }
        private double O90
        {
            get
            {
                return O82;
            }
        }
        private double O91
        {
            get
            {
                return O81;
            }
        }
        private double O97
        {
            get
            {
                return S84;
            }
        }   
        private double O98
        {
            get
            {
                return O88 + O90;
            }
        }
        private double P35
        {
            get
            {
                return calcs.D23;
            }
        }
        private double P36
        {
            get
            {
                return calcs.D24;
            }
        }
        private double S78
        {
            get
            {
                return Math.Round(Math.Max(0, Math.Min(calcs.AttrPeriod, S83.MathSafeDiv(S81))), 2);
            }
        }
        private double S79
        {
            get
            {
                return .1 * Math.Max(-P35, P36);
            }
        }
        private double S81
        {
            get
            {
                return AOCITable[start].AmortisationOfPriorServiceCost;
            }
        }
        private double S82
        {
            get
            {
                return AOCITable[start].AOCI;
            }
        }
        private double S83
        {
            get
            {
                return AOCITable[start].UnrecognisedPriorServiceCost;
            }
        }
        private double S84
        {
            get
            {
                return S82 + S83;
            }
        }

        private DisclosureAssumptions getAssumptionsData(IBasisAssumptions clientAssumptions = null)
        {
            DisclosureAssumptions assumptions = null;

            var b = basesDataManager.GetBasis(end);
            var pf = portfolioManager.GetPortfolio(end);

            if (clientAssumptions == null)
            {
                var data = basesDataManager.GetBasis(end);

                var liabEv = evolution.LiabilityEvolution[end];

                var ctx = new BasisContext(
                    start,
                    end,
                    b.EffectiveDate,
                    b,
                    pf,
                    null,
                    evolution,
                    new BasisAssumptions(evolution.LiabilityEvolution[end].ToAssumptionsDictionary(), null, null, null, null));

                assumptions = new DisclosureAssumptions
                    {
                        DiscountRate = liabEv.DiscPre,
                        FutureSalaryGrowth = liabEv.SalInc,
                        RPIInflation = liabEv.RPI,
                        CpiInflation = liabEv.CPI,
                        ExpectedRateOfReturnOnAssets = expectedReturns[USGAAPExpectedReturnsType.AnalysisEnd],
                        MaleLifeExpectancy65 = data.LifeExpectancy.PensionerLifeExpectancy,
                        MaleLifeExpectancy65_45 = data.LifeExpectancy.DeferredLifeExpectancy
                    };
            }
            else
            {
                var ctx = new BasisContext(
                    start,
                    end,
                    b.EffectiveDate,
                    b,
                    pf,
                    contextResults,
                    evolution,
                    clientAssumptions);

                assumptions = new DisclosureAssumptions
                {
                    DiscountRate = clientAssumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate],
                    FutureSalaryGrowth = clientAssumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease],
                    RPIInflation = clientAssumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex],
                    CpiInflation = clientAssumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex],
                    ExpectedRateOfReturnOnAssets = expectedReturns[USGAAPExpectedReturnsType.UsersInputs],
                    MaleLifeExpectancy65 = clientAssumptions.LifeExpectancy.PensionerLifeExpectancy,
                    MaleLifeExpectancy65_45 = clientAssumptions.LifeExpectancy.DeferredLifeExpectancy
                };
            }

            return assumptions;
        }

        private DisclosureAssumptions getAssumptionsDataPrev(Basis positionAtStartDate)
        {
            var liabEv = evolution.LiabilityEvolution[start];

            return
                new DisclosureAssumptions(
                    liabEv.DiscPre,
                    expectedReturns[USGAAPExpectedReturnsType.AnalysisStart],
                    liabEv.SalInc,
                    liabEv.RPI,
                    liabEv.CPI,
                    positionAtStartDate.LifeExpectancy.PensionerLifeExpectancy,
                    positionAtStartDate.LifeExpectancy.DeferredLifeExpectancy);
        }

        private USGAAPDisclosure.USGAAPBalanceSheetAmountData getBalanceSheetAmountData(AssumptionsSourceType assumptionsSource)
        {
            return new USGAAPDisclosure.USGAAPBalanceSheetAmountData(calcs.C23, calcs.C24, calcs.C25);
        }

        private USGAAPDisclosure.ChangesInPresentValueData getChangesInPresentValueData()
        {
            var PVOpening = calcs.C31;
            var PVServiceCost = calcs.I33;
            var PVInterestCost = calcs.I34;
            var PVSchemeAmendments = 0;
            var PVConts = calcs.C35;
            var PVBenefitsPaid = calcs.C36;
            var PVClosing = calcs.C37;
            var PVActurialLoses = PVClosing - (PVOpening + PVServiceCost + PVInterestCost + PVSchemeAmendments + PVConts + PVBenefitsPaid);

            return new USGAAPDisclosure.ChangesInPresentValueData(PVOpening, PVServiceCost, PVInterestCost, PVActurialLoses, PVSchemeAmendments, PVConts, PVBenefitsPaid, PVClosing);
        }

        private USGAAPDisclosure.ChangesInFairValueData getChangesInFairValueData()
        {
            var FVOpening = calcs.C42;
            var FVEmployerConts = calcs.C46;
            var FVEmployeeConts = calcs.C47;
            var FVBenefitsPaid = calcs.C48;
            var FVClosing = calcs.C49;
            var FVExpectedReturn = calcs.O56;
            var FVCActurialGain = FVClosing - (FVOpening + FVExpectedReturn + FVEmployerConts + FVEmployeeConts + FVBenefitsPaid);

            return new USGAAPDisclosure.ChangesInFairValueData(FVOpening, FVExpectedReturn, FVCActurialGain, FVEmployerConts, FVEmployeeConts, FVBenefitsPaid, FVClosing);
        }

        private USGAAPDisclosure.AccumulatedBenefitObligationData getAccumulatedBenefitObligationData(IBasisLiabilityAssumptions clientAssumptions)
        {
            var originalSalaryIncrease = clientAssumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease];

            double salaryIncreaseAssumption = 0;

            switch (scheme.Data.ABOSalaryType.GetValueOrDefault())
            {
                case SalaryType.RPI:
                    salaryIncreaseAssumption = clientAssumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex];
                    break;
                case SalaryType.Zero:
                    salaryIncreaseAssumption = 0;
                    break;
                case SalaryType.CPI:
                default:
                    salaryIncreaseAssumption = clientAssumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex];
                    break;
            }

            clientAssumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease] = salaryIncreaseAssumption;

            var b = basesDataManager.GetBasis(end);

            var adjBasis = b.Adjust(pensionIncreases: evolution.LiabilityEvolution[b.EffectiveDate].ToPensionIncreasesDictionary());

            var ctx = new BasisContext(
                start, end, adjBasis.EffectiveDate, adjBasis, portfolioManager.GetPortfolio(end), null, this.evolution, null);

            var npv = new LiabilityCashflowStatefulService(ctx).AdjustTo(end, clientAssumptions, true);

            var abo = -npv.Total;

            clientAssumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease] = originalSalaryIncrease;

            return new USGAAPDisclosure.AccumulatedBenefitObligationData(abo);
        }

        private USGAAPDisclosure.EstimatedPaymentData getEstimatedPaymentData(IDictionary<int, IDictionary<MemberStatus, double>> cashflowData)
        {
            var futureBenefitPayments = new Dictionary<int, double>(6);
            Func<IDictionary<MemberStatus, double>, double, double> getEstimatedPayment = (cf, i) => (cf[MemberStatus.ActivePast] + cf[MemberStatus.Deferred] + cf[MemberStatus.Pensioner]) + (0.5 + i) * cf[MemberStatus.ActiveFuture];
            for (int i = 0; i < 10; i++)
            {
                var projYear = end.AddYears(i).Year;
                if (i < 5)
                {
                    futureBenefitPayments.Add(i + 1, getEstimatedPayment(cashflowData[projYear], i));
                }
                else
                {
                    if (!futureBenefitPayments.ContainsKey(6))
                    {
                        futureBenefitPayments.Add(6, 0);
                    }
                    futureBenefitPayments[6] += getEstimatedPayment(cashflowData[projYear], i);
                }
            }

            return new USGAAPDisclosure.EstimatedPaymentData(futureBenefitPayments[1], futureBenefitPayments[2], futureBenefitPayments[3], futureBenefitPayments[4], futureBenefitPayments[5], futureBenefitPayments[6]);
        }

        private USGAAPDisclosure.NetPeriodicPensionCostData getNetPeriodicPensionCostData()
        {
            var PCServiceCost = -O44;
            var PCInterestCost = -O45;
            var PCExpectedReturn = O56;
            var PCAmortisationPrior = -S78 * S81;
            var PCTotal = PCServiceCost + PCInterestCost + PCExpectedReturn + PCAmortisationPrior + O82;

            return new USGAAPDisclosure.NetPeriodicPensionCostData(PCServiceCost, PCInterestCost, PCExpectedReturn, PCAmortisationPrior, O82, PCTotal);
        }

        private USGAAPDisclosure.ComprehensiveIncomeData getComprehensiveIncomeData(
            USGAAPDisclosure.ChangesInPresentValueData changesInPresentValueData,
            USGAAPDisclosure.ChangesInFairValueData changesInFairValueData,
            USGAAPDisclosure.NetPeriodicPensionCostData netPeriodicPensionCostData)
        {
            var CIActurialLoss = -(changesInFairValueData.ActuarialLosses - changesInPresentValueData.ActuarialLosses);
            var CIPriorServiceCost = 0;
            var CIActurialGain = netPeriodicPensionCostData.AmortisationOfActuarialLosses;
            var CIPriorServiceCredit = netPeriodicPensionCostData.AmortisationOfPriorService;
            var CITotal = CIActurialLoss + CIPriorServiceCost + CIActurialGain + CIPriorServiceCredit;

            return new USGAAPDisclosure.ComprehensiveIncomeData(CIActurialLoss, CIPriorServiceCost, CIActurialGain, CIPriorServiceCredit, CITotal);
        }

        private USGAAPDisclosure.ComprehensiveAccumulatedIncomeData getComprehensiveAccumulatedIncomeData()
        {
            var O99 = O91;
            var CATotal = O97 + O98 + O99;

            return new USGAAPDisclosure.ComprehensiveAccumulatedIncomeData(O97, O98, O99, CATotal);
        }

        private USGAAPDisclosure.NetPeriodicPensionCostForecastData getNetPeriodicPensionCostForecastData(IBasisContext ctx)
        {
            var aociData = AOCITable[end];

            var frs17pandL = frs17PandLCalculator.Calculate(ctx);

            var T81 = aociData.AmortisationOfPriorServiceCost;
            var T82 = aociData.AOCI;
            var T83 = aociData.UnrecognisedPriorServiceCost;
            var T84 = T82 + T83;
            var T85 = aociData.ActuarialGainAmortisation;

            var T78 = Math.Round(Math.Max(0, Math.Min((end.AddDays(365.25) - end).TotalDays / 365.25, Utils.MathSafeDiv(T83, T81))), 2);
            var T79 = .1 * Math.Max(-O35, O36);

            var O108 = -T78 * T81;
            var O109 = Math.Abs(T82) < T79
                ? 0
                : -Math.Sign(T82) * (Math.Abs(T82) - T79) / aociData.ActuarialGainAmortisation;

            var total = -frs17pandL.CurrentServiceCost + -frs17pandL.InterestCost + -frs17pandL.ExpectedReturnOnSchemeAssets + O108 + O109;

            return new USGAAPDisclosure.NetPeriodicPensionCostForecastData(-frs17pandL.CurrentServiceCost, -frs17pandL.InterestCost, -frs17pandL.ExpectedReturnOnSchemeAssets, O108, O109, total);
        }

        public double ExpectedRateOfReturnAtPeriodStart
        {
            get { return expectedReturns[USGAAPExpectedReturnsType.AnalysisStart]; }
        }
    }
}