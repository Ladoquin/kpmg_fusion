﻿namespace FlightDeck.Domain.Accounting
{
    using FlightDeck.Domain.Analysis;
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class FRS17Calculator : IDisclosureContextResultsProvider, IExpectedRateOfReturnOnAssetsProvider
    {
        private readonly ISchemeContext scheme;
        private readonly IBasisContext ctx;
        private readonly IBasesDataManager basesDataManager;
        private readonly IPortfolioManager portfolioManager;
        private readonly DateTime periodEnd;
        private readonly DateTime periodStart;
        private readonly StartEnd<LiabilityEvolutionItem> liabEv;
        private readonly StartEnd<AssetEvolutionItem> assetEv;
        private readonly FRS17ProfitLossCalculator frs17PandLCalculator;
        private readonly DisclosureCalcs calcs;

        private Lazy<UserLiabilityData> userLiab;
        private Lazy<AttributionData> attribution;

        public FRS17Calculator(ISchemeContext scheme, IBasisContext ctx, IBasesDataManager basesDataManager, IPortfolioManager portfolioManager, DateTime periodStart, DateTime periodEnd)
        {
            this.scheme = scheme;
            this.ctx = ctx;
            this.basesDataManager = basesDataManager;
            this.portfolioManager = portfolioManager;
            this.periodEnd = periodEnd;
            this.periodStart = periodStart;
            this.liabEv = new StartEnd<LiabilityEvolutionItem>(ctx.EvolutionData.LiabilityEvolution[periodStart], ctx.EvolutionData.LiabilityEvolution[periodEnd]);
            this.assetEv = new StartEnd<AssetEvolutionItem>(ctx.EvolutionData.AssetEvolution[periodStart], ctx.EvolutionData.AssetEvolution[periodEnd]);

            userLiab = new Lazy<UserLiabilityData>(() => new UserLiabsCalculator().Calculate(ctx));
            attribution = new Lazy<AttributionData>(() => new AttributionCalculator(scheme, ctx.EvolutionData).Calculate(periodStart, periodEnd));

            this.frs17PandLCalculator = new FRS17ProfitLossCalculator(scheme);
            this.calcs = new DisclosureCalcs(scheme, periodStart, periodEnd, ctx.EvolutionData, this, this);
        }

        public UserLiabilityData UserLiab
        {
            get
            {
                return userLiab.Value;
            }
        }

        public AttributionData Attribution
        {
            get
            {
                return attribution.Value;
            }
        }

        public double JPInvGrowthAtStart()
        {
            var dataAtStart = basesDataManager.GetBasis(periodStart);

            var giltYield = new GiltYieldService(dataAtStart).GetGiltYield(periodStart);

            var eroa = giltYield + portfolioManager.GetPortfolio(ctx.StartDate).GetOutperformance(includeABF: false);

            return eroa;
        }

        public double ExpectedRateOfReturnAtPeriodStart
        {
            get
            {
                var jpInvGrowthAtStart = JPInvGrowthAtStart();

                var j16 = (jpInvGrowthAtStart * (calcs.J25 - (liabEv.Start.PenBuyIn + liabEv.Start.DefBuyIn)) + liabEv.Start.DiscPre * (liabEv.Start.PenBuyIn + liabEv.Start.DefBuyIn)).MathSafeDiv(calcs.J25);

                return j16;
            }
        }

        public FRS17Disclosure GetStartPositionSummary()
        {
            var assumptions = new DisclosureAssumptions
            {
                DiscountRate = liabEv.Start.DiscPre,
                FutureSalaryGrowth = liabEv.Start.SalInc,
                RPIInflation = liabEv.Start.RPI,
                CpiInflation = liabEv.Start.CPI,
                ExpectedRateOfReturnOnAssets = ExpectedRateOfReturnAtPeriodStart,
                MaleLifeExpectancy65 = basesDataManager.GetBasis(periodStart).LifeExpectancy.PensionerLifeExpectancy,
                MaleLifeExpectancy65_45 = basesDataManager.GetBasis(periodStart).LifeExpectancy.DeferredLifeExpectancy
            };

            var balanceSheetAmounts = new FRS17Disclosure.FRS17BalanceSheetAmountData(calcs.D23, calcs.D24, calcs.D25);

            return new FRS17Disclosure(periodStart, assumptions, balanceSheetAmounts, null, null, null, null);
        }

        public FRS17Disclosure Get(bool ignoreClientAssumptions = true)
        {
            var assumptionsSource = ignoreClientAssumptions ? AssumptionsSourceType.Default : AssumptionsSourceType.Client;

            calcs.AssumptionsSource = assumptionsSource;

            var assumptions = getAssumptionsData(assumptionsSource);
            var balanceSheetAmounts = getBalanceSheetAmountData(assumptionsSource);
            var presentValueSchemeLiabilities = getPresentValueSchemeLiabilityData(balanceSheetAmounts, ignoreClientAssumptions);
            var fairValueAssets = getFairValueSchemeAssetChangesData();
            var schemeSurplus = getSchemeSurplusChangesData(presentValueSchemeLiabilities);
            var pandl = getPAndLForecastData();

            return new FRS17Disclosure(
                periodEnd,
                assumptions,
                balanceSheetAmounts,
                presentValueSchemeLiabilities,
                fairValueAssets,
                schemeSurplus,
                pandl);
        }

        private DisclosureAssumptions getAssumptionsData(AssumptionsSourceType assumptionsSource)
        {
            var I25 = calcs.C24;
            var userTotBuyin = ctx.Results.UserDefBuyIn + ctx.Results.UserPenBuyIn;
            var discountRate = assumptionsSource == AssumptionsSourceType.Default
                ? liabEv.End.DiscPre
                : ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate];

            var expectedRateOfReturn = (ctx.Results.JP2InvGrowth * (I25 - userTotBuyin) + discountRate * userTotBuyin).MathSafeDiv(I25);

            var assumptions = new DisclosureAssumptions
                    {
                        DiscountRate = ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate],
                        FutureSalaryGrowth = ctx.Assumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease],
                        RPIInflation = ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex],
                        CpiInflation = ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex],
                        ExpectedRateOfReturnOnAssets = expectedRateOfReturn,
                        MaleLifeExpectancy65 = ctx.Assumptions.LifeExpectancy.PensionerLifeExpectancy,
                        MaleLifeExpectancy65_45 = ctx.Assumptions.LifeExpectancy.DeferredLifeExpectancy
                    };

            return assumptions;
        }

        private FRS17Disclosure.FRS17BalanceSheetAmountData getBalanceSheetAmountData(AssumptionsSourceType assumptionsSource)
        {
            return new FRS17Disclosure.FRS17BalanceSheetAmountData(calcs.C23, calcs.C24, calcs.C25);
        }

        private FRS17Disclosure.PresentValueSchemeLiabilityData getPresentValueSchemeLiabilityData(FRS17Disclosure.FRS17BalanceSheetAmountData balanceSheetAmountData, bool ignoreClientAssumptions)
        {
            var opening = calcs.C31;
            var serviceCost = calcs.I33;
            var interestExpense = calcs.I34;
            var actuarialLoses = calcs.C34;
            var conts = calcs.C35;
            var benefitsPaid = calcs.C36;
            var closing = calcs.C37;

            return new FRS17Disclosure.PresentValueSchemeLiabilityData(opening, serviceCost, interestExpense, actuarialLoses, conts, benefitsPaid, closing);
        }

        private FRS17Disclosure.FRS17FairValueSchemeAssetChangesData getFairValueSchemeAssetChangesData()
        {
            return new FRS17Disclosure.FRS17FairValueSchemeAssetChangesData(calcs.C42, calcs.I44, calcs.I45, calcs.I46, calcs.I47, calcs.I48, calcs.I49);
        }

        private FRS17Disclosure.FRS17SchemeSurplusChangesData getSchemeSurplusChangesData(FRS17Disclosure.PresentValueSchemeLiabilityData presentValueSchemeLiabilityData)
        {
            var opening = calcs.D25;
            var serviceCost = -presentValueSchemeLiabilityData.CurrentServiceCost;
            var interestCost = -presentValueSchemeLiabilityData.InterestExpense;
            var expectedReturn = calcs.I44;
            var pandlTotal = serviceCost + interestCost + expectedReturn;

            var acturialLossOnLiab = -presentValueSchemeLiabilityData.ActuarialLosses;
            var acturialLossOnAsset = calcs.I45;
            var strgl = acturialLossOnLiab + acturialLossOnAsset;

            var employerConts = calcs.I46;

            var total = opening + pandlTotal + strgl + employerConts;

            return new FRS17Disclosure.FRS17SchemeSurplusChangesData(opening, serviceCost, interestCost, expectedReturn, pandlTotal, acturialLossOnLiab, acturialLossOnAsset, strgl, employerConts, total);
        }

        private FRS17Disclosure.FRS17PAndLForecastData getPAndLForecastData()
        {
            var pl = frs17PandLCalculator.Calculate(ctx);

            var serviceCost = -pl.CurrentServiceCost;
            var interestCost = -pl.InterestCost;
            var expectedReturn = -pl.ExpectedReturnOnSchemeAssets;

            return new FRS17Disclosure.FRS17PAndLForecastData(serviceCost, interestCost, expectedReturn, serviceCost + interestCost + expectedReturn);
        }
    }
}
