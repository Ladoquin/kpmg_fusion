﻿namespace FlightDeck.Domain.Accounting
{
    using System;
    using System.Collections.Generic;

    public class USGAAPInitData
    {
        public IDictionary<DateTime, AOCIData> AOCITable { get; private set; }
        public IDictionary<USGAAPExpectedReturnsType, double> ExpectedReturns { get; set; }

        public USGAAPInitData(IDictionary<DateTime, AOCIData> aociTable)
        {
            AOCITable = aociTable;
        }
    }
}
