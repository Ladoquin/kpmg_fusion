﻿namespace FlightDeck.Domain.Accounting
{
    using FlightDeck.Domain.Analysis;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class IAS19Calculator : IDisclosureContextResultsProvider
    {
        private readonly ISchemeContext scheme;
        private readonly IBasisContext ctx;
        private readonly DateTime date;
        private readonly DateTime periodStart;
        private readonly StartEnd<LiabilityEvolutionItem> liabEv;
        private readonly StartEnd<AssetEvolutionItem> assetEv;
        private readonly DisclosureCalcs calcs;
        private readonly IAS19ProfitLossCalculator ias19PandLCalculator;

        private Lazy<UserLiabilityData> userLiab;
        private Lazy<AttributionData> attribution;

        public enum CalculationMode { Full, SummaryOnly }

        public IAS19Calculator(ISchemeContext scheme, IBasisContext ctx, DateTime date)
            : this(scheme, ctx, date, date)
        {
        }
        public IAS19Calculator(ISchemeContext scheme, IBasisContext ctx, DateTime date, DateTime periodStart)
        {
            this.scheme = scheme;
            this.ctx = ctx;
            this.date = date;
            this.periodStart = periodStart;
            this.liabEv = new StartEnd<LiabilityEvolutionItem>(ctx.EvolutionData.LiabilityEvolution[periodStart], ctx.EvolutionData.LiabilityEvolution[date]);
            this.assetEv = new StartEnd<AssetEvolutionItem>(ctx.EvolutionData.AssetEvolution[periodStart], ctx.EvolutionData.AssetEvolution[date]);

            userLiab = new Lazy<UserLiabilityData>(() => new UserLiabsCalculator().Calculate(ctx));
            attribution = new Lazy<AttributionData>(() => new AttributionCalculator(scheme, ctx.EvolutionData).Calculate(periodStart, date));

            this.ias19PandLCalculator = new IAS19ProfitLossCalculator(scheme);
            this.calcs = new DisclosureCalcs(scheme, periodStart, date, ctx.EvolutionData, this, null);
        }

        public UserLiabilityData UserLiab
        {
            get
            {
                return userLiab.Value;
            }
        }

        public AttributionData Attribution
        {
            get
            {
                return attribution.Value;
            }
        }

        public IAS19Disclosure Get(CalculationMode mode = CalculationMode.Full, bool ignoreClientAssumptions = true)
        {
            var assumptionsSource = ignoreClientAssumptions || mode == CalculationMode.SummaryOnly ? AssumptionsSourceType.Default : AssumptionsSourceType.Client;

            calcs.AssumptionsSource = assumptionsSource;

            var assumptions = getAssumptionsData(assumptionsSource);
            var balanceSheetAmounts = getBalanceSheetAmountData(assumptionsSource);
            var definedBenefitObligation = mode == CalculationMode.Full ? getDefinedBenefitObligationData(balanceSheetAmounts) : null;
            var fairValueAssets = mode == CalculationMode.Full ? getFairValueSchemeAssetChangesData(definedBenefitObligation, balanceSheetAmounts) : null;
            var schemeSurplus = mode == CalculationMode.Full ? getSchemeSurplusChangesData(definedBenefitObligation, fairValueAssets) : null;
            var pandl = mode == CalculationMode.Full ? getPAndLForecastData() : null;

            return new IAS19Disclosure(
                date,
                assumptions,
                balanceSheetAmounts,
                definedBenefitObligation,
                fairValueAssets,
                schemeSurplus,
                pandl);
        }

        private DisclosureAssumptions getAssumptionsData(AssumptionsSourceType assumptionsSource)
        {
            DisclosureAssumptions assumptions = null;

            if (assumptionsSource == AssumptionsSourceType.Default)
            {
                assumptions = new DisclosureAssumptions
                {
                    DiscountRate = liabEv.End.DiscPre,
                    FutureSalaryGrowth = liabEv.End.SalInc,
                    RPIInflation = liabEv.End.RPI,
                    CpiInflation = liabEv.End.CPI,
                    MaleLifeExpectancy65 = ctx.Data.LifeExpectancy.PensionerLifeExpectancy,
                    MaleLifeExpectancy65_45 = ctx.Data.LifeExpectancy.DeferredLifeExpectancy
                };
            }
            else
            {
                assumptions = new DisclosureAssumptions
                {
                    DiscountRate = ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate],
                    FutureSalaryGrowth = ctx.Assumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease],
                    RPIInflation = ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex],
                    CpiInflation = ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex],
                    MaleLifeExpectancy65 = ctx.Assumptions.LifeExpectancy.PensionerLifeExpectancy,
                    MaleLifeExpectancy65_45 = ctx.Assumptions.LifeExpectancy.DeferredLifeExpectancy
                };
            } 

            return assumptions;
        }

        private IAS19Disclosure.IAS19BalanceSheetAmountData getBalanceSheetAmountData(AssumptionsSourceType assumptionsSource)
        {
            return new IAS19Disclosure.IAS19BalanceSheetAmountData(calcs.C23, calcs.C24, calcs.C25);
        }

        private IAS19Disclosure.DefinedBenefitObligationData getDefinedBenefitObligationData(IAS19Disclosure.IAS19BalanceSheetAmountData balanceSheetAmountData)
        {
            var opening = calcs.C31;
            var serviceCost = calcs.C32;
            var interestExpense = calcs.C33;
            var actuarialLoses = calcs.C34;
            var conts = calcs.C35;
            var benefitsPaid = calcs.C36;
            var closing = -balanceSheetAmountData.DefinedBenefitObligation;            

            return new IAS19Disclosure.DefinedBenefitObligationData(opening, serviceCost, interestExpense, actuarialLoses, conts, benefitsPaid, closing);
        }

        private IAS19Disclosure.IAS19FairValueSchemeAssetChangesData getFairValueSchemeAssetChangesData(IAS19Disclosure.DefinedBenefitObligationData definedBenefitObligation, IAS19Disclosure.IAS19BalanceSheetAmountData balanceSheetAmounts)
        {
            var opening = calcs.D24;
            var interestIncome = ctx.Results.Attribution.AttrInterestIncome;
            var schemeAdmin = -ctx.Results.Attribution.AttrSchemeAdminExpenses;
            var contsEmployer = calcs.C46;
            var contsMembers = ctx.Results.Attribution.AttrEeeContsReceived;
            var benefitsPaid = definedBenefitObligation.BenefitsPaid;
            var closing = balanceSheetAmounts.FairValueOfSchemeAssets;
            var returnOnAssets = closing - (opening + interestIncome + schemeAdmin + contsEmployer + contsMembers + benefitsPaid);

            return new IAS19Disclosure.IAS19FairValueSchemeAssetChangesData(opening, interestIncome, returnOnAssets, schemeAdmin, contsEmployer, contsMembers, benefitsPaid, closing);
        }

        private IAS19Disclosure.IAS19SchemeSurplusChangesData getSchemeSurplusChangesData(IAS19Disclosure.DefinedBenefitObligationData definedBenefitObligation, IAS19Disclosure.IAS19FairValueSchemeAssetChangesData fairValueAssetData)
        {
            var opening = calcs.D25;
            var serviceCost = -definedBenefitObligation.CurrentServiceCost;
            var adminExpenses = fairValueAssetData.SchemeAdminExpenses;
            var netInterest = calcs.C43 - calcs.C33;
            var pandlTotal = serviceCost + adminExpenses + netInterest;

            var acturialLossOnLiab = -definedBenefitObligation.ActuarialLosses;
            var returnOnAssets = fairValueAssetData.ReturnOnSchemeAssets;
            var oci = acturialLossOnLiab + returnOnAssets;

            var employerConts = fairValueAssetData.ContributionsEmployer;

            var total = opening + pandlTotal + oci + employerConts;

            return new IAS19Disclosure.IAS19SchemeSurplusChangesData(opening, serviceCost, adminExpenses, netInterest, pandlTotal, acturialLossOnLiab, returnOnAssets, oci, employerConts, total);
        }

        private IAS19Disclosure.IAS19PAndLForecastData getPAndLForecastData()
        {
            var pl = ias19PandLCalculator.Calculate(ctx);

            var serviceCost = -pl.CurrentServiceCost;
            var adminExpenses = -pl.SchemeAdminExpenses;
            var netInterest = -(pl.InterestCredit + pl.InterestCharge);

            return new IAS19Disclosure.IAS19PAndLForecastData(serviceCost, adminExpenses, pl.InterestCredit, pl.InterestCharge, netInterest, serviceCost + adminExpenses + netInterest);
        }
    }
}
