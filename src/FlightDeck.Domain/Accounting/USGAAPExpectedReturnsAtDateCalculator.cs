﻿namespace FlightDeck.Domain.Accounting
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System;

    class USGAAPExpectedReturnsAtDateCalculator
    {
        private readonly BasisEvolutionData evolution;
        private readonly IBasesDataManager basesDataManager;
        private readonly IPortfolioManager portolioManager;

        /// <summary>
        /// Calculate the expected return at a particular date
        /// </summary>
        public USGAAPExpectedReturnsAtDateCalculator(BasisEvolutionData evolution, IBasesDataManager basesDataManager, IPortfolioManager portolioManager)
        {
            this.evolution = evolution;
            this.basesDataManager = basesDataManager;
            this.portolioManager = portolioManager;
        }

        public double GetExpectedReturn(DateTime at)
        {
            var benchmarkGiltYield = basesDataManager.GetBasis(at).UnderlyingGiltYieldIndex.GetValue(at) / 100;
            var outperformance = portolioManager.GetPortfolio(at).GetOutperformance(false);

            var penBuyin = evolution.LiabilityEvolution[at].PenBuyIn;
            var defBuyin = evolution.LiabilityEvolution[at].DefBuyIn;

            var coreAssets = evolution.AssetEvolution[at].AssetValue;
            var totalAssets = coreAssets + penBuyin + defBuyin;

            var coreAssetsReturn = benchmarkGiltYield + outperformance;
            var discPen = evolution.LiabilityEvolution[at].DiscPen;
            var discPre = evolution.LiabilityEvolution[at].DiscPre;

            var product =
                (coreAssets * coreAssetsReturn) +
                (penBuyin * discPen) +
                (defBuyin * discPre);

            var er = product.MathSafeDiv(totalAssets);

            return er;
        }
    }
}
