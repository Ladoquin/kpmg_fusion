﻿namespace FlightDeck.Domain.Accounting
{
    using FlightDeck.DomainShared;
    using System;
    
    public class AOCIData : IEquatable<AOCIData>
    {
        public double AOCI { get; private set; }
        public double UnrecognisedPriorServiceCost { get; private set; }
        public double AmortisationOfPriorServiceCost { get; private set; }
        public int ActuarialGainAmortisation { get; private set; }

        public AOCIData()
        {
        }

        public AOCIData(double aoci, double unrecognisedPrior, double amortisationOfPrior, int actuarialGain)
        {
            AOCI = aoci;
            UnrecognisedPriorServiceCost = unrecognisedPrior;
            AmortisationOfPriorServiceCost = amortisationOfPrior;
            ActuarialGainAmortisation = actuarialGain;
        }

        public bool Equals(AOCIData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

           return Math.Abs(AOCI - other.AOCI) < Utils.TestPrecision
                && Math.Abs(UnrecognisedPriorServiceCost - other.UnrecognisedPriorServiceCost) < Utils.TestPrecision
                && Math.Abs(AmortisationOfPriorServiceCost - other.AmortisationOfPriorServiceCost) < Utils.TestPrecision
                && Math.Abs(ActuarialGainAmortisation - other.ActuarialGainAmortisation) < Utils.TestPrecision;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((AOCIData)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = AOCI.GetHashCode();
                hashCode = (hashCode * 397) ^ UnrecognisedPriorServiceCost.GetHashCode();
                hashCode = (hashCode * 397) ^ AmortisationOfPriorServiceCost.GetHashCode();
                hashCode = (hashCode * 397) ^ ActuarialGainAmortisation.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("AOCI: {0}, UnrecognisedPriorServiceCost: {1}, AmortisationOfPriorServiceCost: {2}, ActuarialGainAmortisation: {3}", AOCI, UnrecognisedPriorServiceCost, AmortisationOfPriorServiceCost, ActuarialGainAmortisation);
        }
    }
}
