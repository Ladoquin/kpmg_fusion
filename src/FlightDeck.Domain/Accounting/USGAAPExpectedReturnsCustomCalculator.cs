﻿namespace FlightDeck.Domain.Accounting
{
    using FlightDeck.DomainShared;
    
    class USGAAPExpectedReturnsCustomCalculator
    {
        private readonly ISchemeContext scheme;
        private readonly IBasisContext ctx;

        public USGAAPExpectedReturnsCustomCalculator(ISchemeContext scheme, IBasisContext ctx)
        {
            this.scheme = scheme;
            this.ctx = ctx;
        }

        public double GetExpectedReturn()
        {
            var benchmarkGiltYield = ctx.Data.UnderlyingGiltYieldIndex.GetValue(ctx.AnalysisDate) / 100;
            var outperformance = ctx.Portfolio
                .SetStrategy(ctx, ctx.Assumptions.AssetAssumptions, scheme.Assumptions.InvestmentStrategyAssetAllocation)
                .GetOutperformance(false);

            var trackedPenBuyin = ctx.EvolutionData.LiabilityEvolution[ctx.AnalysisDate].PenBuyIn;
            var trackedDefBuyin = ctx.EvolutionData.LiabilityEvolution[ctx.AnalysisDate].DefBuyIn;

            var trackedCoreAssets = ctx.EvolutionData.AssetEvolution[ctx.AnalysisDate].AssetValue;
            var trackedTotalAssets = trackedCoreAssets + trackedPenBuyin + trackedDefBuyin;

            var userPenBuyin = ctx.Results.UserPenBuyIn;
            var userDefBuyin = ctx.Results.UserDefBuyIn;

            var totalAssets = trackedCoreAssets + userPenBuyin + userDefBuyin;

            var coreAssetsReturn = benchmarkGiltYield + outperformance;
            var discPen = ctx.Assumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate];
            var discPre = ctx.Assumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate];

            var product =
                (trackedCoreAssets * coreAssetsReturn) +
                (userPenBuyin * discPen) +
                (userDefBuyin * discPre);

            var er = product.MathSafeDiv(totalAssets);

            return er;
        }
    }
}
