﻿namespace FlightDeck.Domain.Accounting
{
    interface IExpectedRateOfReturnOnAssetsProvider
    {
        double ExpectedRateOfReturnAtPeriodStart { get; }
    }
}
