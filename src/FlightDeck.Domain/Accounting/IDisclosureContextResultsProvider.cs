﻿namespace FlightDeck.Domain.Accounting
{
    using FlightDeck.DomainShared;
    
    interface IDisclosureContextResultsProvider
    {
        UserLiabilityData UserLiab { get; }
        AttributionData Attribution { get; }
    }
}
