﻿namespace FlightDeck.Domain.Accounting
{
    internal class USGAAPTableDataItem
    {
        public double AOCI { get; private set; }
        public double UnrecognisedPriorServiceCost { get; private set; }
        public double AmortisationOfPriorServiceCost { get; private set; }
        public double AmortisationPeriod { get; private set; }

        public USGAAPTableDataItem(double aoci, double unrecognisedPriorServiceCost, double amortisationOfPriorServiceCost, double amortisationPeriod)
        {
            AOCI = aoci;
            UnrecognisedPriorServiceCost = unrecognisedPriorServiceCost;
            AmortisationOfPriorServiceCost = amortisationOfPriorServiceCost;
            AmortisationPeriod = amortisationPeriod;
        }
    }
}
