﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    class VarCalculator
    {
        private ISchemeContext scheme;
        private IBasisContext ctx;

        public VarCalculator(ISchemeContext scheme, IBasisContext ctx)
        {
            this.scheme = scheme;
            this.ctx = ctx;
        }

        private Tuple<double, double> getAssetLiabilityVals()
        {
            return Tuple.Create(ctx.Results.FundingLevel.Assets, ctx.Results.FundingLevel.Liabilities - ctx.Results.FundingLevel.Buyin);
        }

        public VarLensData GetVarWaterfall(int time = 3, double ci = .95)
        {
            var tailPoint = Math.Abs(ci - .95) < Utils.Precision ? 1.64485362695147 : Utils.NormSInv(ci);

            var al = getAssetLiabilityVals();
            var assetValue = al.Item1;
            var liabilities = al.Item2;

            var exposures = new Dictionary<AssetClassType, double>();
            foreach (var a in Utils.AllVarAssetClassTypes)
            {
                if (scheme.Assumptions.InvestmentStrategyAssetAllocation.ContainsKey(a))
                {
                    var mult = 1;
                    exposures.Add(a, scheme.Data.Volatilities[a] * mult * scheme.Assumptions.InvestmentStrategyAssetAllocation[a] * assetValue);
                }
                else if (Utils.LiabilityAssetClassTypes.Contains(a))
                    exposures.Add(a, scheme.Data.Volatilities[a] * (-1) * liabilities);
            }
            var totalRisk = CalculateRisk(exposures, time, tailPoint);
            var interestRisk = CalculateRisk(exposures, time, tailPoint, AssetClassType.LiabilityIntererst, AssetClassType.LiabilityIntererst);
            var longevityRisk = CalculateRisk(exposures, time, tailPoint, AssetClassType.LiabilityLongevity, AssetClassType.LiabilityLongevity);
            var totalLiabRisk = CalculateRisk(exposures, time, tailPoint, AssetClassType.LiabilityIntererst, AssetClassType.LiabilityLongevity);
            var liabHedgeCreditRisk = CalculateRisk(exposures, time, tailPoint, AssetClassType.LiabilityIntererst, AssetClassType.CorporateBonds);
            var liabHedgeRisk = CalculateRisk(exposures, time, tailPoint, AssetClassType.LiabilityIntererst, AssetClassType.IndexLinkedGilts);

            var hedgeRisk = liabHedgeCreditRisk - totalLiabRisk;
            var pureCreditRisk = CalculateRisk(exposures, time, tailPoint, AssetClassType.Abf, AssetClassType.CorporateBonds);
            var creditRisk = liabHedgeCreditRisk - liabHedgeRisk + pureCreditRisk;

            var equitytRisk = CalculateRisk(exposures, time, tailPoint, AssetClassType.Equity, AssetClassType.Equity);
            var otherGrowRisk = CalculateRisk(exposures, time, tailPoint, AssetClassType.Dgfs, AssetClassType.Property);

            var diversification = totalRisk - interestRisk - longevityRisk - hedgeRisk - creditRisk - equitytRisk - otherGrowRisk;

            var result = new VarLensData(interestRisk, 10000000, longevityRisk, hedgeRisk, creditRisk,
                equitytRisk, otherGrowRisk, diversification, totalRisk);

            return result;
        }

        public IList<VarFunnelData> GetVarFunnel()
        {
            const double tailPoint = 1.64485362695147;

            var al = getAssetLiabilityVals();
            var assets = al.Item1;
            var liabilities = al.Item2;

            var median3 = ctx.Results.JourneyPlanAfter.ContainsKey(3) ? ctx.Results.JourneyPlanAfter[3].SurplusDeficit : 0;
            var median6 = ctx.Results.JourneyPlanAfter.ContainsKey(6) ? ctx.Results.JourneyPlanAfter[6].SurplusDeficit : 0;
            var waterfall = GetVarWaterfall();

            var risk = waterfall.TotalRisk / (tailPoint * Math.Sqrt(3));

            var deficit = assets - liabilities;

            return new List<VarFunnelData>
            {
                new VarFunnelData(deficit, deficit, deficit, deficit, deficit),
                GetFunnel(3, median3, risk),
                GetFunnel(6, median6, risk),
            };
        }

        private VarFunnelData GetFunnel(int period, double median, double risk)
        {
            var seventyFive = risk * Math.Sqrt(period) * Utils.NormSInv(0.75);
            var ninetyFive = risk * Math.Sqrt(period) * Utils.NormSInv(0.95);
            
            return new VarFunnelData(ninetyFive + median, seventyFive + median, median, median - seventyFive, median - ninetyFive);
        }

        private double CalculateRisk(IDictionary<AssetClassType, double> exposures, int time, double tailPoint, AssetClassType from = AssetClassType.LiabilityIntererst, AssetClassType to = AssetClassType.Property)
        {
            int begin = 0, end = 0;
            for (var i = 0; i < Utils.AllVarAssetClassTypes.Count; i++)
            {
                if (Utils.AllVarAssetClassTypes[i] == from)
                    begin = i;
                if (Utils.AllVarAssetClassTypes[i] == to)
                    end = i;
            }
            var result = .0;
            for (var i = begin; i <= end; i++)
            {
                var r = Utils.AllVarAssetClassTypes[i];
                if (!exposures.ContainsKey(r))
                    continue;
                var rowExposure = exposures[r];
                for (var j = begin; j <= end; j++)
                {
                    var columnAsset = Utils.AllVarAssetClassTypes[j];
                    if (!exposures.ContainsKey(columnAsset))
                        continue;
                    result += scheme.Data.VarCorrelations[r][columnAsset] * rowExposure * exposures[columnAsset];
                }
            }
            result = Math.Pow(result * time, 0.5) * tailPoint;
            return result;
        }

    }
}