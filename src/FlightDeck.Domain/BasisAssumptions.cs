﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public class BasisAssumptions : IBasisAssumptions
    {
        public IDictionary<AssumptionType, double> LiabilityAssumptions { get; private set; }
        public LifeExpectancyAssumptions LifeExpectancy { get; private set; }
        public IDictionary<int, double> PensionIncreases { get; private set; }
        public IDictionary<AssetClassType, double> AssetAssumptions { get; private set; }
        public RecoveryPlanAssumptions RecoveryPlanOptions { get; private set; }

        public BasisAssumptions(
            IDictionary<AssumptionType, double> liabilityAssumptions,
            LifeExpectancyAssumptions lifeExpectancy,
            IDictionary<int, double> pensionIncreases,
            IDictionary<AssetClassType, double> assetAssumptions,
            RecoveryPlanAssumptions recoveryPlanOptions)
        {
            LiabilityAssumptions = liabilityAssumptions;
            LifeExpectancy = lifeExpectancy;
            PensionIncreases = pensionIncreases;
            AssetAssumptions = assetAssumptions;
            RecoveryPlanOptions = recoveryPlanOptions;
        }

        public BasisAssumptions Clone()
        {
            return new BasisAssumptions
                (
                new Dictionary<AssumptionType, double>(LiabilityAssumptions),
                new LifeExpectancyAssumptions(LifeExpectancy.PensionerLifeExpectancy, LifeExpectancy.DeferredLifeExpectancy),
                new Dictionary<int, double>(PensionIncreases),
                new Dictionary<AssetClassType, double>(AssetAssumptions),
                RecoveryPlanOptions.Clone()
                );
        }
    }
}
