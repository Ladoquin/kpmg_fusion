﻿namespace FlightDeck.Domain
{
    using System;
    
    public class StartEnd
    {
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        public StartEnd(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }
    }
}
