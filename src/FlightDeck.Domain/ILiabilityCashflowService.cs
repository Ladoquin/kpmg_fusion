﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    interface ILiabilityCashflowService
    {
        NPV NPV { get; }
        NPV AdjustTo(DateTime date, IBasisLiabilityAssumptions assumptionsAtDate);
        NPV AdjustTo(DateTime date, IBasisLiabilityAssumptions assumptionsAtDate, bool userBasis);
        NPV GetPresentValue(IEnumerable<CashflowData> cashflows, DateTime date, IBasisLiabilityAssumptions assumptions);        
        NPV GetBenefitOutgo(int yearNo, IEnumerable<CashflowData> cashflows);
        NPV PV01 { get; }
        NPV IE01 { get; }
        void CalculatePV01AndIE01(DateTime valuationDate, IBasisLiabilityAssumptions liabAssumptions, bool userBasis = true);
        double GetPV01(bool isBuyin);
        double GetIE01(bool isBuyin);
        int GetYearNumber(DateTime atDate);
        
        IEnumerable<CashflowData> GetCashflowOutput(DateTime at);
        void GroupOver50sCashFlows();
        double GetLiabDuration();
    }
}
