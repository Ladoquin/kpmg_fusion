﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightDeck.DomainShared;

namespace FlightDeck.Domain
{
    public static class IBasisContextExtensions
    {
        public static IBasisContext AdjustAssumptions(this IBasisContext ctx,
            IDictionary<AssumptionType, double> assumptions = null,
            IDictionary<int, double> pensionIncreases = null,
            LifeExpectancyAssumptions lifeExpectancy = null,
            IDictionary<AssetClassType, double> assetAssumptions = null)
        {
            return new BasisContext(
                ctx.StartDate,
                ctx.AnalysisDate,
                ctx.EffectiveDate,
                ctx.Data,
                ctx.Portfolio,
                ctx.Results,
                ctx.EvolutionData,
                new BasisAssumptions(
                    assumptions ?? ctx.Assumptions.LiabilityAssumptions,
                    lifeExpectancy ?? ctx.Assumptions.LifeExpectancy,
                    pensionIncreases ?? ctx.Assumptions.PensionIncreases,
                    assetAssumptions ?? ctx.Assumptions.AssetAssumptions,
                    ctx.Assumptions.RecoveryPlanOptions)); 
        }
    
        public static IBasisContext AdjustData(this IBasisContext ctx, 
            IDictionary<AssumptionType, double> assumptions = null,
            IDictionary<int, double> pensionIncreases = null)
        {
            return new BasisContext(
                ctx.StartDate,
                ctx.AnalysisDate,
                ctx.EffectiveDate,
                ctx.Data.Adjust(assumptions, pensionIncreases),
                ctx.Portfolio,
                ctx.Results,
                ctx.EvolutionData,
                ctx.Assumptions);                
        }
    }
}
