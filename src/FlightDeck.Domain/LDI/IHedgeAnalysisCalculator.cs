﻿namespace FlightDeck.Domain.LDI
{
    using FlightDeck.DomainShared;

    interface IHedgeAnalysisCalculator
    {
        HedgeData Calculate();
    }
}
