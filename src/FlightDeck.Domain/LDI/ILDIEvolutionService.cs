﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain.LDI
{
    interface ILDIEvolutionService
    {
        IEnumerable<LDIEvolutionItem> GetLiabilityHedgingData(DateTime refreshDate);
        IEnumerable<LDIEvolutionItem> GetLiabilityHedgingData(DateTime startDate, DateTime endDate);
    }
}
