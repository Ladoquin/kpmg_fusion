﻿namespace FlightDeck.Domain.LDI
{
    using System;

    [Serializable]
    public class LDIEvolutionItem
    {
        public System.DateTime Date { get; private set; }
        public double LDIInterestSwap { get; private set; }
        public double LDIInflationSwap { get; private set; }

        public LDIEvolutionItem(System.DateTime date, double ldiInterestSwap, double ldiInflationSwap)
        {
            this.Date = date;
            this.LDIInterestSwap = ldiInterestSwap;
            this.LDIInflationSwap = ldiInflationSwap;
        }
    }
}
