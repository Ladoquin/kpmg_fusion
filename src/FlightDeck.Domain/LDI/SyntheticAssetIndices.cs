﻿namespace FlightDeck.Domain.LDI
{
    public class SyntheticAssetIndices
    {
        public FinancialIndex EquityIncomeIndex { get; private set; }
        public FinancialIndex EquityOutgoIndex { get; private set; }
        public FinancialIndex CreditIncomeIndex { get; private set; }
        public FinancialIndex CreditOutgoIndex { get; private set; }

        public SyntheticAssetIndices(FinancialIndex equityIncomeIndex, FinancialIndex equityOutgoIndex, FinancialIndex creditIncomeIndex, FinancialIndex creditOutgoIndex)
        {
            EquityIncomeIndex = equityIncomeIndex;
            EquityOutgoIndex = equityOutgoIndex;
            CreditIncomeIndex = creditIncomeIndex;
            CreditOutgoIndex = creditOutgoIndex;
        }
    }
}
