﻿using FlightDeck.Domain.Evolution;
using FlightDeck.DomainShared;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Domain.LDI
{
    public class GiltLiabilityCalculator
    {
        private ISchemeContext scheme;

        private enum adjustmentType
        {
            user,
            gilt
        }

        public GiltLiabilityCalculator(ISchemeContext schemeContext)
        {
            this.scheme = schemeContext;
        }

        public GiltBasisLiabilityData Calculate(IBasisContext ctx)
        {
            if (scheme.Data.LDIBasesDataManager == null || scheme.Data.LDIBasesDataManager.AnchorDate > ctx.AnalysisDate)
                return new GiltBasisLiabilityData(0, 0, 0, 0, 0, 0, null);

            var pm = new Assets.PortfolioManager(scheme.Data, scheme.Data.LDIBasesDataManager);
            var pf = pm.GetPortfolio(ctx.AnalysisDate);

            var ldiProvider = scheme.Data.GetLDIBasisProvider(ctx.AnalysisDate);
            var ldiBasis = ldiProvider.LDIBasis.Clone();

            ldiBasis = ldiBasis.Adjust(
                pensionIncreases: new PensionIncreaseIndexEvolutionService().GetPensionIncreaseIndexValue(ldiBasis.EffectiveDate, ldiBasis)
                );

            var lcf = new LiabilityCashflowStatefulService(ldiBasis, ctx.EvolutionData.LiabilityEvolution, ctx.AnalysisDate, ctx.Assumptions);

            IBasisLiabilityAssumptions basisLiabilityAssumptions = null;
            IBasisLiabilityAssumptions PVandIEAssumptions = null;
            IDictionary<int, double> clientPensionIncreases = null;

            // if ldi overrides enabled then convertToLdi
            if (scheme.Data.LDICashflowBasis.OverrideAssumptions)
            {
                lcf.ConvertToLDI(scheme.Data, ctx.AnalysisDate, ldiProvider.DiscRate, ldiProvider.InflationRate);

                //todo: refactor this into ConvertToLDI (and also out of AssetHedgeCalculator)
                var underlyingPincs = lcf.Basis.UnderlyingPensionIncreaseIndices;
                var userPincs = 
                    underlyingPincs.Keys
                        .ToDictionary(x => x, 
                                        x => underlyingPincs[x]
                                                .GetValue(ctx.AnalysisDate,
                                                            (underlyingPincs[x].Category == AssumptionCategory.BlackScholes ? ctx.AnalysisDate : ctx.EffectiveDate)
                                                        )
                                      );
                lcf.AdjustBase(lcf.Basis.Clone(null, null, userPincs, null, ctx.AnalysisDate));

                basisLiabilityAssumptions = new BasisLiabilityAssumptions(lcf.Basis.Assumptions, lcf.Basis.LifeExpectancy, lcf.Basis.PensionIncreases);
                
                clientPensionIncreases = lcf.Basis.PensionIncreases;
                PVandIEAssumptions = basisLiabilityAssumptions;
            }
            else
            {
                if (ldiBasis.DisplayName == ctx.Data.DisplayName)
                {
                    basisLiabilityAssumptions = new BasisLiabilityAssumptions(ctx.EvolutionData.LiabilityEvolution[ctx.AnalysisDate], ctx.Assumptions.LifeExpectancy);
                    clientPensionIncreases = ctx.Assumptions.PensionIncreases;
                    PVandIEAssumptions = ctx.Assumptions;
                }
                else
                {
                    basisLiabilityAssumptions = scheme.BasesAssumptionsProvider.BasisAssumptionsCollection[scheme.Data.LDICashflowBasis.BasisName];
                    clientPensionIncreases = basisLiabilityAssumptions.PensionIncreases;
                    PVandIEAssumptions = basisLiabilityAssumptions;

                    lcf.AdjustContext(basisLiabilityAssumptions);
            }
            }

            lcf.AdjustTo(ctx.AnalysisDate, basisLiabilityAssumptions, false);

            ApplyAllSolutions(scheme.Assumptions, lcf, ctx, clientPensionIncreases);

            // calculate liabs and duration on gilt basis
            var totLiab = lcf.NPV.Total;
            var liabDuration = lcf.GetLiabDuration();

            lcf.CalculatePV01AndIE01(ctx.AnalysisDate, PVandIEAssumptions); 
            var giltLiabPV01 = lcf.GetPV01(false);
            var giltLiabIE01 = lcf.GetIE01(false);
            var buyinPV01 = lcf.GetPV01(true);
            var buyinIE01 = lcf.GetIE01(true);

            // hedge breakdown data 
            var giltConv = totLiab / ctx.EvolutionData.LiabilityEvolution[ctx.AnalysisDate].Liabs;
            var hedgeCalculator = new Domain.LDI.HedgeEvolutionCalculator(pf, giltLiabPV01, giltLiabIE01, buyinPV01, buyinIE01, giltConv);
            var hedgeData = hedgeCalculator.Calculate();
            return new GiltBasisLiabilityData(totLiab, liabDuration,
                                              giltLiabPV01, giltLiabIE01,
                                              buyinPV01, buyinIE01, 
                                              hedgeData);
        }

        private void ApplyAllSolutions(ISchemeAssumptions schemeAssumptions, LiabilityCashflowStatefulService lcf, IBasisContext ctx, IDictionary<int, double> clientPensionIncreases)// IBasisContext userCtx, IBasisContext ctx, IBasisContext giltCtx, IDictionary<int, double> adjustedPincs = null)
        {
            if (schemeAssumptions.FROType == FROType.Bulk)
            {
                lcf.ApplyFRO(schemeAssumptions.FROOptions);
                lcf.ApplyETV(schemeAssumptions.ETVOptions);
                lcf.ApplyBenefitChanges(schemeAssumptions.FutureBenefitOptions);
            }
            else
            {
                lcf.ApplyEmbeddedFRO(schemeAssumptions.EFROOptions);
            }

            //#TrackerBug pie init data is based on the currently active (non-ldi) basis
            var pieInitData = new Analysis.PIEInitCalculator().Calculate(ctx);
            // but pie calculation will use ldi related pension increases
            lcf.ApplyPIE(schemeAssumptions.PIEOptions, pieInitData, clientPensionIncreases);

            var NPV = lcf.GetPresentValue(ctx.AnalysisDate);

            lcf.SetNPV(NPV);
        }

        private void adjustBasis(adjustmentType type, LiabilityCashflowStatefulService lcf, IBasisContext giltCtx, IBasisContext userCtx)
        {
            if (type == adjustmentType.user)
            {
                lcf.AdjustContext(
                    new BasisLiabilityAssumptions(userCtx.Assumptions.LiabilityAssumptions, userCtx.Assumptions.LifeExpectancy, userCtx.Assumptions.PensionIncreases));
            }
            else if (type == adjustmentType.gilt)
            {
                if (scheme.Data.LDICashflowBasis.OverrideAssumptions)
                {
                    lcf.AdjustContext(
                        new BasisLiabilityAssumptions(giltCtx.Assumptions.LiabilityAssumptions, giltCtx.Assumptions.LifeExpectancy, giltCtx.Assumptions.PensionIncreases));
                } 
            }
        }
    }
}
