﻿namespace FlightDeck.Domain.LDI
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    
    class LDIBasisProvider : ILDIBasisProvider
    {
        private readonly ISchemeData scheme;
        private readonly DateTime at;
        private readonly LDICashflowBasis LDICashflowBasis;

        double multiplyer = .01;
        double? discRate = null;
        double? inflationRate = null;

        public LDIBasisProvider(ISchemeData scheme, DateTime at)
        {
            this.scheme = scheme;
            this.LDICashflowBasis = scheme.LDICashflowBasis;
            this.at = at;
        }

        public double DiscRate
        {
            get
            {
                if (!discRate.HasValue)
                {
                    if (LDICashflowBasis.OverrideAssumptions)
                    {
                        discRate = (LDICashflowBasis.DiscountRateIndex.GetValue(at) * multiplyer) + LDICashflowBasis.DiscountRatePremium;
                    }
                    else
                    {
                        discRate = .0;
                    }
                }
                return discRate.Value;
            }
        }

        public double InflationRate
        {
            get
            {
                if (!inflationRate.HasValue)
                {
                    if (LDICashflowBasis.OverrideAssumptions)
                    {
                        inflationRate = LDICashflowBasis.InflationIndex.GetValue(at) * multiplyer;
                    }
                    else
                    {
                        inflationRate = .0;
                    }
                }
                return inflationRate.Value;
            }
        }

        public Basis LDIBasis
        {
            get
            {
                var ldiBasis = scheme.LDIBasesDataManager.GetBasis(at).Clone();

                // prepare pension increases (based on ldi settings)
                var userRPI = ldiBasis.Assumptions[AssumptionType.InflationRetailPriceIndex];
                var userCPI = ldiBasis.Assumptions[AssumptionType.InflationConsumerPriceIndex];
                if (LDICashflowBasis.OverrideAssumptions)
                {
                    userRPI = InflationRate;
                    userCPI = InflationRate - LDICashflowBasis.InflationGap;
                }

                var pincs = ldiBasis.GetVariablePincAssumption(ldiBasis.EffectiveDate, userRPI, userCPI, false);

                ldiBasis = ldiBasis.Adjust(pensionIncreases: pincs);

                return ldiBasis;
            }
        }
    }
}
