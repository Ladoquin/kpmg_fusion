﻿namespace FlightDeck.Domain.LDI
{
    public class LDICashflowBasis
    {
        public string BasisName { get; private set; }
        public bool OverrideAssumptions { get; private set; }
        public FinancialIndex DiscountRateIndex { get; private set; }
        public double DiscountRatePremium { get; private set; }
        public FinancialIndex InflationIndex { get; private set; }
        public double InflationGap { get; private set; }

        public LDICashflowBasis(string basisName, bool overrideAssumptions, FinancialIndex discountRateIndex, double discountRatePremium, FinancialIndex inflationIndex, double inflationGap)
        {
            BasisName = basisName;
            OverrideAssumptions = overrideAssumptions;
            DiscountRateIndex = discountRateIndex;
            DiscountRatePremium = discountRatePremium;
            InflationIndex = inflationIndex;
            InflationGap = inflationGap;
        }
    }
}
