﻿namespace FlightDeck.Domain.LDI
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public abstract class HedgeCalculatorBase
    {
        protected double InputInterestHedgeProp;
        protected double InputInflationHedgeProp;
        protected double GiltLiabPV01;
        protected double GiltLiabIE01;
        protected double BuyinPV01;
        protected double BuyinIE01;
        protected double GiltDuration;
        protected double CorpDuration;
        protected double GiltConversion;
        protected double CorpAssets;
        protected double GiltAssets;
        protected double ILGAssets;
        protected double CorpAssetPV01;
        protected double GiltAssetPV01;
        protected double ILGAssetPV01;
        protected double ILGAssetIE01;
        protected double BuyInInterestHedgeProp;
        protected double BuyInInflationHedgeProp;
        protected double GiltInterestHedgeProp;
        protected double GiltInflationHedgeProp;
        protected double TotAssetsPV01;
        protected double TotAssetsIE01;
        protected double InterestSwapPV01;
        protected double InflationSwapPV01;
        protected bool LDIInForce;

        protected abstract void init();

        public HedgeData Calculate()
        {
            GiltInterestHedgeProp = 0;
            GiltInflationHedgeProp = 0;
            BuyInInterestHedgeProp = 0;
            BuyInInflationHedgeProp = 0;

            init();

            calculatePhysicalAssetHedge();
            calculateBuyInHedge();
            calculateSwapHedge();

            var hedge = new HedgeData
                (
                    GiltInterestHedgeProp,
                    GiltInflationHedgeProp,
                    GiltInterestHedgeProp * GiltConversion,
                    GiltInflationHedgeProp * GiltConversion,
                    BuyInInterestHedgeProp,
                    BuyInInflationHedgeProp,
                    BuyInInterestHedgeProp
                )
                {
                    Breakdown = calculateBreakdown()
                };

            return hedge;
        }

        private void calculatePhysicalAssetHedge()
        {
            CorpAssetPV01 = CorpAssets * CorpDuration * 0.0001;
            GiltAssetPV01 = GiltAssets * GiltDuration * 0.0001;
            ILGAssetPV01 = ILGAssets * GiltDuration * 0.0001;

            ILGAssetIE01 = ILGAssetPV01;
        }

        private void calculateBuyInHedge()
        {
            BuyInInterestHedgeProp = BuyinPV01.MathSafeDiv(GiltLiabPV01);
            BuyInInflationHedgeProp = BuyinIE01.MathSafeDiv(GiltLiabIE01);
        }

        private void calculateSwapHedge()
        {
            if (!LDIInForce)
            {
                GiltInterestHedgeProp = InputInterestHedgeProp;
                GiltInflationHedgeProp = InputInflationHedgeProp;

                TotAssetsPV01 = CorpAssetPV01 + GiltAssetPV01 + ILGAssetPV01 + BuyinPV01 + GiltInterestHedgeProp * GiltLiabPV01;
                TotAssetsIE01 = ILGAssetIE01 + BuyinIE01 + GiltInflationHedgeProp * GiltLiabIE01;
            }
            else
            {
                TotAssetsPV01 = GiltLiabPV01 * InputInterestHedgeProp;
                TotAssetsIE01 = GiltLiabIE01 * InputInflationHedgeProp;

                InterestSwapPV01 = TotAssetsPV01 - (CorpAssetPV01 + GiltAssetPV01 + ILGAssetPV01 + BuyinPV01);
                InflationSwapPV01 = TotAssetsIE01 - (ILGAssetIE01 + BuyinIE01);

                GiltInterestHedgeProp = InterestSwapPV01.MathSafeDiv(GiltLiabPV01);
                GiltInflationHedgeProp = InflationSwapPV01.MathSafeDiv(GiltLiabIE01);
            }
        }

        private HedgeBreakdownData calculateBreakdown()
        {
            var GiltPhysicalAssetInterestHedge = (CorpAssetPV01 + GiltAssetPV01 + ILGAssetPV01).MathSafeDiv(GiltLiabPV01);
            var GiltPhysicalAssetInflationHedge = ILGAssetIE01.MathSafeDiv(GiltLiabIE01);

            //Current basis / physical assets
            var CurrPhysicalAssetInterestHedge = GiltPhysicalAssetInterestHedge * GiltConversion;
            var CurrPhysicalAssetInflationHedge = GiltPhysicalAssetInflationHedge * GiltConversion;

            //Both bases / buy-in
            var BuyInInterestHedge = BuyInInterestHedgeProp;
            var BuyInInflationHedge = BuyInInflationHedgeProp;

            //Cashflow basis / LDI overlay
            var GiltLDIOverlayInterestHedge = LDIInForce ? GiltInterestHedgeProp : .0;
            var GiltLDIOverlayInflationHedge = LDIInForce ? GiltInflationHedgeProp : .0;

            //Current basis / LDI overlay
            var CurrLDIOverlayInterestHedge = GiltLDIOverlayInterestHedge * GiltConversion;
            var CurrLDIOverlayInflationHedge = GiltLDIOverlayInflationHedge * GiltConversion;

            var CurrInterestTotal = CurrPhysicalAssetInterestHedge + BuyInInterestHedge + CurrLDIOverlayInterestHedge;
            var CurrInflationTotal = CurrPhysicalAssetInflationHedge + BuyInInflationHedge + CurrLDIOverlayInflationHedge;
            var GiltInterestTotal = GiltPhysicalAssetInterestHedge + BuyInInterestHedge + GiltLDIOverlayInterestHedge;
            var GiltInflationTotal = GiltPhysicalAssetInflationHedge + BuyInInflationHedge + GiltLDIOverlayInflationHedge;

            return new HedgeBreakdownData
                {
                    CurrentBasis = new Dictionary<HedgeType, HedgeBreakdownItem>
                        {
                            { HedgeType.Interest, new HedgeBreakdownItem(CurrPhysicalAssetInterestHedge, BuyInInterestHedge, CurrLDIOverlayInterestHedge, CurrInterestTotal) },
                            { HedgeType.Inflation, new HedgeBreakdownItem(CurrPhysicalAssetInflationHedge, BuyInInflationHedge, CurrLDIOverlayInflationHedge, CurrInflationTotal) }
                        },
                    Cashflows = new Dictionary<HedgeType, HedgeBreakdownItem>
                        {
                            { HedgeType.Interest, new HedgeBreakdownItem(GiltPhysicalAssetInterestHedge, BuyInInterestHedge, GiltLDIOverlayInterestHedge, GiltInterestTotal) },
                            { HedgeType.Inflation, new HedgeBreakdownItem(GiltPhysicalAssetInflationHedge, BuyInInflationHedge, GiltLDIOverlayInflationHedge, GiltInflationTotal) }
                        }
                };
        }
    }
}
