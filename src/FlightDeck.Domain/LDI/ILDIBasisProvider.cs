﻿namespace FlightDeck.Domain.LDI
{
    using System;

    public interface ILDIBasisProvider
    {
        double DiscRate { get; }
        double InflationRate { get; }
        Basis LDIBasis { get; }
    }
}
