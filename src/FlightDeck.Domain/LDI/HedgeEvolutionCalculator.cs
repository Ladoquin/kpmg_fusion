﻿namespace FlightDeck.Domain.LDI
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public class HedgeEvolutionCalculator : HedgeCalculatorBase
    {
        readonly IPortfolio pf;
        readonly double giltLiabPV01;
        readonly double giltLiabIE01;
        readonly double buyinPV01;
        readonly double buyinIE01;
        readonly double giltConversion;

        public HedgeEvolutionCalculator(IPortfolio pf, double giltLiabPV01, double giltLiabIE01, double buyinPV01, double buyinIE01, double giltConversion)
        {
            this.pf = pf;
            this.giltLiabPV01 = giltLiabPV01;
            this.giltLiabIE01 = giltLiabIE01;
            this.buyinPV01 = buyinPV01;
            this.buyinIE01 = buyinIE01;
            this.giltConversion = giltConversion;
        }
        
        protected override void init()
        {
            LDIInForce = pf.AssetData.LDIInForce;

            InputInterestHedgeProp = pf.AssetData.InterestHedge;
            InputInflationHedgeProp = pf.AssetData.InflationHedge;
            GiltConversion = giltConversion;

            GiltLiabPV01 = giltLiabPV01;
            GiltLiabIE01 = giltLiabIE01;
            BuyinPV01 = buyinPV01;
            BuyinIE01 = buyinIE01;

            CorpAssets = pf.GetAssetValue(AssetClassType.CorporateBonds);
            GiltAssets = pf.GetAssetValue(AssetClassType.FixedInterestGilts);
            ILGAssets = pf.GetAssetValue(AssetClassType.IndexLinkedGilts);

            CorpDuration = pf.AssetData.CorpDuration;
            GiltDuration = pf.AssetData.GiltDuration;
        }
    }
}
