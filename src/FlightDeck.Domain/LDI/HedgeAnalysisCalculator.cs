﻿namespace FlightDeck.Domain.LDI
{
    using FlightDeck.DomainShared;

    class HedgeAnalysisCalculator : HedgeCalculatorBase, IHedgeAnalysisCalculator
    {
        private ISchemeContext scheme;
        private IBasisContext ctx;

        public HedgeAnalysisCalculator(ISchemeContext scheme, IBasisContext ctx)
        {
            this.scheme = scheme;
            this.ctx = ctx;
        }

        protected override void init()
        {
            LDIInForce = scheme.Assumptions.InvestmentStrategyOptions.LDIInForce;

            InputInterestHedgeProp = .0;
            InputInflationHedgeProp = .0;

            if (LDIInForce)
            {
                InputInterestHedgeProp = scheme.Assumptions.InvestmentStrategyOptions.InterestHedge;
                InputInflationHedgeProp = scheme.Assumptions.InvestmentStrategyOptions.InflationHedge;
            }

            var giltLiability = ctx.Results.GiltBasisLiabilityData;
            GiltLiabPV01 = giltLiability.LiabPV01;
            GiltLiabIE01 = giltLiability.LiabIE01;
            BuyinPV01 = giltLiability.BuyInPV01;
            BuyinIE01 = giltLiability.BuyInIE01;

            GiltDuration = scheme.Data.Funded ? ctx.Portfolio.AssetData.GiltDuration : scheme.Data.GiltDurationDefault;
            CorpDuration = scheme.Data.Funded ? ctx.Portfolio.AssetData.CorpDuration : scheme.Data.CorpDurationDefault;
            GiltConversion = giltLiability.TotalLiab.MathSafeDiv(ctx.Results.FundingLevel.Liabilities);

            var coreAssets = ctx.Results.InvestmentStrategy.TotalCoreAssets;

            CorpAssets = coreAssets * ctx.Results.InvestmentStrategy.ABFAdjustment * scheme.Assumptions.InvestmentStrategyAssetAllocation[AssetClassType.CorporateBonds];
            GiltAssets = coreAssets * ctx.Results.InvestmentStrategy.ABFAdjustment * scheme.Assumptions.InvestmentStrategyAssetAllocation[AssetClassType.FixedInterestGilts];
            ILGAssets = coreAssets * ctx.Results.InvestmentStrategy.ABFAdjustment * scheme.Assumptions.InvestmentStrategyAssetAllocation[AssetClassType.IndexLinkedGilts];
        }
    }
}
