﻿using FlightDeck.Domain.Assets;
using FlightDeck.Domain.Evolution;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Domain.LDI
{
    public class LDIEvolutionService : ILDIEvolutionService
    {
        private IBasesDataManager ldiBasesDataManager;
        private ISchemeData schemeData;
        private IDictionary<DateTime, HedgeData> hedgeTable;

        public LDIEvolutionService(IBasesDataManager ldiBasesDataManager, ISchemeData schemeData)
        {
            this.ldiBasesDataManager = ldiBasesDataManager;
            this.schemeData = schemeData;
            this.hedgeTable = new Dictionary<DateTime, HedgeData>();
        }

        public IEnumerable<LDIEvolutionItem> GetLiabilityHedgingData(DateTime refreshDate)
        {
            return Calculate(refreshDate);
        }

        public IEnumerable<LDIEvolutionItem> GetLiabilityHedgingData(DateTime startDate, DateTime endDate)
        {
            return Calculate(endDate, startDate);
        }

        private class IndexParams
        {
            public double InitialValue;
            public FinancialIndex Index;
            public AssumptionCategory Category;

            public IndexParams(double initialValue, FinancialIndex index, AssumptionCategory category)
            {
                InitialValue = initialValue;
                Index = index;
                Category = category;
            }
        }

        private IEnumerable<LDIEvolutionItem> Calculate(DateTime endDate, DateTime? startFrom = null)
        {            
            var anchor = ldiBasesDataManager.AnchorDate;
            var start = anchor;
            if (startFrom.HasValue)
            {
                start = startFrom.Value;
                //starting point isn't the start of scheme so need to update the anchor to start from the right effective date
                anchor = ldiBasesDataManager.EffectiveDates.Where(ed => ed <= start).OrderByDescending(ed => ed).First();                
            }            
            var ldiOverrideAssumptions = schemeData.LDICashflowBasis.OverrideAssumptions;
            var ldiInForce = false;
            IPortfolio portfolio = null;
            var ldiEvolution = new List<LDIEvolutionItem>((int)(endDate - start).TotalDays);

            var effectiveDates = ldiBasesDataManager.EffectiveDates.OrderBy(d => d).ToList();

            Func<DateTime, HedgeData> getHedgeItem = dt =>
            {
                return hedgeTable.Where(x => x.Key <= dt).OrderByDescending(o => o.Key).First().Value;
            };

            Func<IDictionary<AssumptionType, FinancialAssumption>, DateTime, DateTime, Dictionary<AssumptionType, double>> getLiabAssumption =
                (indices, d, effDate) =>
                {
                    return indices.ToDictionary(x => x.Key, y => y.Value.GetValue(d, effDate));
                };

            Basis basis = null;
            var valDate = start;
            IBasisLiabilityAssumptions basisLiabAssumptions = null;
            LiabilityCashflowStatefulService lcf = null;
            var multiplyer = 0.01;

            Action<DateTime> loadBasis = at =>
            {
                basis = ldiBasesDataManager.GetBasis(at).Clone();
                var pincsAtDate = new PensionIncreaseIndexEvolutionService().GetPensionIncreaseIndexValue(at, basis);
                basis = basis.Adjust(pensionIncreases: pincsAtDate);
                basisLiabAssumptions = new BasisLiabilityAssumptions(basis.Assumptions, basis.LifeExpectancy, pincsAtDate);
                lcf = new LiabilityCashflowStatefulService(basis, null, at, basisLiabAssumptions);
            };

            Action<DateTime> convertToLDI = at =>
            {
                var inflationRate = schemeData.LDICashflowBasis.InflationIndex.GetValue(at) * multiplyer;
                var discRate = (schemeData.LDICashflowBasis.DiscountRateIndex.GetValue(at) * multiplyer) + schemeData.LDICashflowBasis.DiscountRatePremium;
                lcf.ConvertToLDI(schemeData, at, discRate, inflationRate);
            };

            loadBasis(anchor);

            if (ldiOverrideAssumptions)
            {
                convertToLDI(anchor);
            }
            
            NPV lastNPV = null;
            ldiEvolution.Add(new LDIEvolutionItem(valDate, .0, .0));
            do
            {
                var previousDate = valDate.AddDays(-1);
                if (valDate > anchor)
                {
                    var assumptionEvolutionService = new AssumptionIndexEvolutionService();

                    // 1) rerun using previous day's assumptions
                    var liabilityAssumptionsAtPrevDate = Utils.EnumToArray<AssumptionType>().ToDictionary(x => x, x => assumptionEvolutionService.GetAssumptionIndexValue(x, previousDate, lcf.Basis));
                    var pensionIncreasesAtPrevDate = new PensionIncreasesTrackerService().calculatePensionIncreases(previousDate, anchor, lcf.Basis.UnderlyingPensionIncreaseIndices, lcf.Basis.UnderlyingAssumptionIndices, liabilityAssumptionsAtPrevDate, null, false);
                    var assumptionsAtPrevDate = new BasisLiabilityAssumptions(liabilityAssumptionsAtPrevDate, lcf.Basis.LifeExpectancy, pensionIncreasesAtPrevDate);

                    var NPV = lcf.AdjustTo(previousDate, assumptionsAtPrevDate, true);

                    // 2) rerun changing only the discount rates (i.e. leave pincs and inflation alone)
                    var partialLiabAssumptionsAtValDate = new Dictionary<AssumptionType, double>
                        {
                            { AssumptionType.PreRetirementDiscountRate, assumptionEvolutionService.GetAssumptionIndexValue(AssumptionType.PreRetirementDiscountRate, valDate, lcf.Basis) },
                            { AssumptionType.PostRetirementDiscountRate, assumptionEvolutionService.GetAssumptionIndexValue(AssumptionType.PostRetirementDiscountRate, valDate, lcf.Basis) },
                            { AssumptionType.PensionDiscountRate, assumptionEvolutionService.GetAssumptionIndexValue(AssumptionType.PensionDiscountRate, valDate, lcf.Basis) },
                            { AssumptionType.SalaryIncrease, liabilityAssumptionsAtPrevDate[AssumptionType.SalaryIncrease] },
                            { AssumptionType.InflationRetailPriceIndex, liabilityAssumptionsAtPrevDate[AssumptionType.InflationRetailPriceIndex] },
                            { AssumptionType.InflationConsumerPriceIndex, liabilityAssumptionsAtPrevDate[AssumptionType.InflationConsumerPriceIndex] }
                        };
                    var partialAssumptionsAtValDate = new BasisLiabilityAssumptions(partialLiabAssumptionsAtValDate, lcf.Basis.LifeExpectancy, pensionIncreasesAtPrevDate);

                    var NPV2 = lcf.AdjustTo(valDate, partialAssumptionsAtValDate, true);

                    // 3) rerun changing inflation assumptions too (i.e. using b on ValDate)
                    var liabilityAssumptionsAtValDate = Utils.EnumToArray<AssumptionType>().ToDictionary(x => x, x => assumptionEvolutionService.GetAssumptionIndexValue(x, valDate, lcf.Basis));
                    var pensionIncreasesAtValDate = new PensionIncreasesTrackerService().calculatePensionIncreases(valDate, anchor, lcf.Basis.UnderlyingPensionIncreaseIndices, lcf.Basis.UnderlyingAssumptionIndices, liabilityAssumptionsAtValDate, null, false);
                    var assumptionsAtValDate = new BasisLiabilityAssumptions(liabilityAssumptionsAtValDate, lcf.Basis.LifeExpectancy, pensionIncreasesAtValDate); 

                    var NPV3 = lcf.AdjustTo(valDate, assumptionsAtValDate, false);

                    // 4) update asset mix if there was an update yesterday
                    if (schemeData.SchemeAssets.Any(x => x.EffectiveDate == previousDate) || portfolio == null)
                    {
                        var effDate = previousDate;
                        var assets = schemeData.SchemeAssets.Where(x => x.EffectiveDate == effDate).FirstOrDefault();

                        if (assets == null)
                        {
                            // Might be running these calcs from a data other than the anchor date, which means we won't necessarily 
                            // have crossed an effective date boundary and therefore won't have set a portfolio or hedgeTable item.
                            // Hence portfolio == null check
                            // So set these now using the most recent effective date for this date
                            effDate = schemeData.SchemeAssets.Select(x => x.EffectiveDate).Where(ed => ed <= effDate).OrderByDescending(ed => ed).First();
                            assets = schemeData.SchemeAssets.Where(x => x.EffectiveDate == effDate).First();
                        }

                        ldiInForce = assets.LDIInForce;
                        var pm = new PortfolioManager(schemeData, ldiBasesDataManager);
                        var pf = pm.GetPortfolio(effDate);
                        portfolio = pf;

                        var userAssumptions = new AssumptionIndexEvolutionService().GetAllAssumptionIndexValues(effDate, lcf.Basis);
                        var pincsAtPrevDate = new PensionIncreasesTrackerService().calculatePensionIncreases(effDate, lcf.Basis.EffectiveDate, lcf.Basis.UnderlyingPensionIncreaseIndices, lcf.Basis.UnderlyingAssumptionIndices, userAssumptions, null, false);

                        assumptionsAtPrevDate = new BasisLiabilityAssumptions(userAssumptions, assumptionsAtPrevDate.LifeExpectancy, pincsAtPrevDate);
                        lcf.CalculatePV01AndIE01(effDate, assumptionsAtPrevDate, false);
                        var giltLiabPV01 = lcf.GetPV01(false);
                        var giltLiabIE01 = lcf.GetIE01(false);
                        var buyinPV01 = lcf.GetPV01(true);
                        var buyinIE01 = lcf.GetIE01(true);

                        var hedgeCalculator = new HedgeEvolutionCalculator(pf, giltLiabPV01, giltLiabIE01, buyinPV01, buyinIE01, 1);
                        var hedgeItem = hedgeCalculator.Calculate();
                        hedgeTable.Add(effDate, hedgeItem);                        
                    }

                    var benefits = lcf.GetBenefitOutgo(lcf.GetYearNumber(valDate));
                    var ben = (benefits.Liabilities[MemberStatus.ActivePast] + benefits.Liabilities[MemberStatus.Deferred] + benefits.Liabilities[MemberStatus.Pensioner]) / 365.25;
                    var cashReturn = NPV.Total * portfolio.GetLDICashIndexGrowth(previousDate, valDate);

                    var hcItem = getHedgeItem(valDate);
                    var ldiInterestSwap = ldiInForce ? hcItem.GiltLDIOverlayInterestHedge * (NPV2.Total - NPV.Total + ben - cashReturn) : .0;
                    var ldiInflationSwap = ldiInForce ? hcItem.GiltLDIOverlayInflationHedge * (NPV3.Total - NPV2.Total) : .0;

                    ldiEvolution.Add(new LDIEvolutionItem(valDate, ldiInterestSwap, ldiInflationSwap));

                    // Important: set basisLiab assumptions to current date's assumptions
                    basisLiabAssumptions = assumptionsAtValDate;
                }

                basisLiabAssumptions = new BasisLiabilityAssumptions
                    (
                        basisLiabAssumptions.LiabilityAssumptions,
                        basisLiabAssumptions.LifeExpectancy,
                        lcf.Basis.UnderlyingPensionIncreaseIndices.ToDictionary(x => x.Key, x => x.Value.GetValue(valDate, anchor))
                    );
                lastNPV = lcf.AdjustTo(valDate, basisLiabAssumptions, false);

                // check if we need to change basis
                if (effectiveDates.Any(x => x == valDate) && valDate > anchor)
                {
                    anchor = valDate;

                    loadBasis(anchor);

                    if (ldiOverrideAssumptions)
                    {
                        convertToLDI(anchor);
                    }

                    lcf.AdjustTo(valDate, basisLiabAssumptions);
                }

                valDate = valDate.AddDays(1);
            }
            while (valDate <= endDate);

            return ldiEvolution;
        }

    }
}
