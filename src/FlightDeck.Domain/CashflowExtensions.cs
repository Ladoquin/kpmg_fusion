﻿namespace FlightDeck.Domain
{
    using System.Collections.Generic;
    using System.Linq;
    
    /// <summary>
    /// Class written primarily to help debugging, don't know if it will come in useful elsewhere. 
    /// </summary>
    static class CashflowExtensions
    {
        public static double SumUp(this List<CashflowData> cashflow)
        {
            return cashflow.Sum(x => x.Cashflow.Sum(y => y.Value));
        }

        public static Dictionary<MemberStatus, double> SumUpParts(this List<CashflowData> cashflow)
        {
            return cashflow
                .GroupBy(x => x.MemberStatus)
                .ToDictionary(x => x.Key, x => x.Sum(y => y.Cashflow.Sum(z => z.Value)));
        }
    }
}
