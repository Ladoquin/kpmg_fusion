﻿namespace FlightDeck.Domain
{
    using System;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using FlightDeck.Domain;

    public interface ISchemeResults
    {
        DateTime RefreshDate { get; }
        BuyinCost BuyinCost { get; set; }
        IEnumerable<LDI.LDIEvolutionItem> LDIEvolutionData { get; }
    }
}
