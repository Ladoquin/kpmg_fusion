﻿using FlightDeck.DomainShared;

namespace FlightDeck.Domain
{
    public class FinancialAssumption: AssumptionBase
    {
        public AssumptionType Type { get; private set; }
        public string Name { get; private set; }

        public FinancialAssumption(int id, int basisId, double initialValue, FinancialIndex index,
            AssumptionType assumptionType, string name, bool isVisible = true) :
                base(id, basisId, initialValue, index, isVisible)
        {
            Name = name;
            Type = assumptionType;
        }

        public FinancialAssumption Clone()
        {
            return new FinancialAssumption(Id, BasisId, InitialValue, Index, Type, Name, IsVisible);
        }
    }

    public class SimpleAssumption
    {
        public SimpleAssumptionType Type { get; private set; }
        public string Name { get; private set; }
        public bool IsVisible { get; private set; }
        public double Value { get; private set; }

        public SimpleAssumption(SimpleAssumptionType type, double value, string name, bool isVisible)
        {
            IsVisible = isVisible;
            Name = name;
            Value = value;
            Type = type;
        }

        public SimpleAssumption Clone()
        {
            return new SimpleAssumption(Type, Value, Name, IsVisible);
        }
    }
}
