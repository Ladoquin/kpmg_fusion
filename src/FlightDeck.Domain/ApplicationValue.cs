﻿namespace FlightDeck.Domain
{
    public class ApplicationValue
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
