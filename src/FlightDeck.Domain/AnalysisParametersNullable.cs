﻿namespace FlightDeck.Domain
{
    public class AnalysisParametersNullable
    {
        public double? FROMinTVAmount { get; private set; }
        public double? FROMaxTVAmount { get; private set; }
        public double? ETVMinTVAmount { get; private set; }
        public double? ETVMaxTVAmount { get; private set; }

        public AnalysisParametersNullable()
        {
        }
        public AnalysisParametersNullable(double? froMin, double? froMax, double? etvMin, double? etvMax)
        {
            FROMinTVAmount = froMin;
            FROMaxTVAmount = froMax;
            ETVMinTVAmount = etvMin;
            ETVMaxTVAmount = etvMax;
        }
    }
}
