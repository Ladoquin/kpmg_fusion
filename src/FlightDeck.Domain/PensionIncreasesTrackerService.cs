﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain
{
    public class PensionIncreasesTrackerService
    {
        public Dictionary<int, double> calculatePensionIncreases(Basis currentBasis, DateTime at, BeforeAfter<AssumptionData> clientLiabilityAssumptions, BeforeAfter<IDictionary<int, double>> clientPensionIncreases)
        {
            return calculatePensionIncreases
                (
                at,
                currentBasis.EffectiveDate,
                currentBasis.UnderlyingPensionIncreaseIndices,
                currentBasis.UnderlyingAssumptionIndices,
                clientLiabilityAssumptions.After.ToDictionary(),
                clientPensionIncreases.After,
                clientPensionIncreases.IsDirty
                );
        }

        public Dictionary<int, double> calculatePensionIncreases(DateTime at, DateTime effectiveDate, IDictionary<int, PensionIncreaseAssumption> pincDefs, IDictionary<AssumptionType, FinancialAssumption> assumptions, IDictionary<AssumptionType, double> clientAssumptions, IDictionary<int, double> clientPincs, bool pincsUnlocked)
        {
            var pincs = pincDefs.Keys.ToDictionary(x => x, x => .0);

            clientPincs = clientPincs ?? new Dictionary<int, double>();

            foreach (var p in pincDefs.Values)
            {
                if (p.IsFixed)
                {
                    pincs[p.ReferenceNumber] = p.FixedIncrease;
                }
                else if (pincsUnlocked && clientPincs.ContainsKey(p.ReferenceNumber))
                {
                    pincs[p.ReferenceNumber] = clientPincs[p.ReferenceNumber];
                }
                else
                {
                    if (p.InflationType != InflationType.None)
                    {
                        double inflationAdjustment;
                        if (p.InflationType == InflationType.Cpi)
                            inflationAdjustment = clientAssumptions[AssumptionType.InflationConsumerPriceIndex] -
                              assumptions[AssumptionType.InflationConsumerPriceIndex].GetValue(at, effectiveDate);
                        else
                            inflationAdjustment = clientAssumptions[AssumptionType.InflationRetailPriceIndex] -
                              assumptions[AssumptionType.InflationRetailPriceIndex].GetValue(at, effectiveDate);
                        pincs[p.ReferenceNumber] = p.GetValue(at, effectiveDate, inflationAdjustment);
                    }
                    else
                        pincs[p.ReferenceNumber] = p.GetValue(at, effectiveDate);
                }
            }
            return pincs;
        }

        public Dictionary<int, double> CalculateVariablePensionIncreases(DateTime at, DateTime effectiveDate, IDictionary<int, PensionIncreaseAssumption> pincDefs, IDictionary<int, double> currentVals, double userRPI, double userCPI)
        {
            var pincs = pincDefs.Keys.ToDictionary(x => x, x => currentVals[x]);

            foreach (var p in pincDefs.Values)
            {
                var Inf = p.InflationType == InflationType.Rpi ? userRPI : userCPI;
                if (p.Category == AssumptionCategory.BlackScholes)
                {
                    pincs[p.ReferenceNumber] = p.GetValue(at, effectiveDate, null, Inf);
                }
                else if (p.Category == AssumptionCategory.Fixed && p.InflationType != InflationType.None)
                {
                    pincs[p.ReferenceNumber] = Inf;
                }
            }
            return pincs;
        }
    }
}
