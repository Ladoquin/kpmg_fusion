﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.LDI;
    using FlightDeck.DomainShared;
    using log4net;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    public class PensionScheme : PensionSchemeBase, ISchemeData
    {
        private struct SavedValuesBag
        {
            public double Over55LiabilityOnDefaultBasis { get; set; }
            public double Under55LiabilityOnDefaultBasis { get; set; }
        }

        protected IDictionary<MasterBasisKey, MasterBasis> Bases;
        private MarketInfoProvider marketInfoProvider;
        private AssetsGrowthProvider assetsGrowthProvider;

        // ISchemeOptions implementation

        public virtual DateTime RefreshDate { get; protected set; } // virtual to allow mocking
        public IEnumerable<DateTime> AssetEffectiveDates { get; protected set; }
        public IDictionary<DateTime, RecoveryPayment> RecoveryPlan { get; private set; }
        public IEnumerable<RecoveryPayment> RecoveryPlanData { get { return RecoveryPlanRaw; } }
        public int GiltDurationDefault { get { return ApplicationParameters.GiltDurationDefault; } }
        public int CorpDurationDefault { get { return ApplicationParameters.CorpDurationDefault; } }        
        internal RecoveryPlanAssumptions FixedAssumptions;
        
        private IBasesDataManager _ldiBasesDataManager;
        public IBasesDataManager LDIBasesDataManager
        {
            get
            {
                if (_ldiBasesDataManager == null && LDICashflowBasis != null && LDICashflowBasis.BasisName != null)
                {
                    var ldiBases = UnderlyingBases.Where(x => x.DisplayName == LDICashflowBasis.BasisName);
                    if (ldiBases != null && ldiBases.Any())
                        _ldiBasesDataManager = new BasesDataManager(ldiBases);
                }
                return _ldiBasesDataManager;
            }
        }

        public PensionScheme(int id, string name, string reference, IEnumerable<Basis> bases, IEnumerable<SchemeAsset> assets,
            IList<Contribution> contributionRates, IEnumerable<RecoveryPayment> recoveryPlan, SchemeDataSource dataSource,
            IDictionary<AssetClassType, double> volatilities, IDictionary<AssetClassType, double> defaultVolatilityAssumptions, IDictionary<AssetClassType, IDictionary<AssetClassType, double>> varCorrelations,
            IDictionary<NamedPropertyGroupType, NamedPropertyGroup> namedProperties, MarketInfoProvider infoProvider, AssetsGrowthProvider assetsGrowthProvider,
            double returnOnAssets, bool isGrowthFixed, double ias19ExpenseLoading,
            AccountingStandardType accountingDefaultStandard, bool accountingIAS19Visible, bool accountingFRS17Visible, bool accountingUSGAAPVisible,
            SalaryType? aboSalaryType, PensionSchemeApplicationParameters applicationParameters, AnalysisParametersNullable analysisParameters,
            double contractingOutContributionRate, LDICashflowBasis ldiCashflowBasis, SyntheticAssetIndices syntheticAssets, bool funded,
            string defaultBasis)
            : base(
                id, name, reference, bases, assets, contributionRates, recoveryPlan, dataSource, volatilities, defaultVolatilityAssumptions, varCorrelations, namedProperties, returnOnAssets, isGrowthFixed,
                ias19ExpenseLoading, accountingDefaultStandard, accountingIAS19Visible, accountingFRS17Visible, accountingUSGAAPVisible, aboSalaryType,
                applicationParameters, analysisParameters, contractingOutContributionRate, ldiCashflowBasis, syntheticAssets, funded, defaultBasis)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            log4net.LogManager.GetLogger("scheme-load").InfoFormat("Loading scheme '{0}'", name);

            SetAssetEffectiveDates();

            RecoveryPlan = GetOldRecoveryPlan(RecoveryPlanRaw);

            SetRefreshDate(bases);

            SetBases(bases);

            this.marketInfoProvider = infoProvider;
            this.assetsGrowthProvider = assetsGrowthProvider;            
            
            stopwatch.Stop();

            LogManager.GetLogger("performance").InfoFormat("PensionScheme..ctor(): {0} milliseconds", stopwatch.ElapsedMilliseconds.ToString());
        }

        protected virtual void SetAssetEffectiveDates()
        {
            AssetEffectiveDates = SchemeAssets.Select(asset => asset.EffectiveDate).Distinct();
        }

        protected virtual void SetRefreshDate(IEnumerable<Basis> bases)
        {
            // Refresh date is the most recent date that all indices (used by the scheme) exist up to

            var assumptionEndDate = bases.Min(b => b.UnderlyingAssumptionIndices.Min(assumption => assumption.Value.Index.EndDate));
            var pensionIncreaseEndDate = bases.Min(b => b.UnderlyingPensionIncreaseIndices.Min(pinc => pinc.Value.Index == null ? DateTime.MaxValue : pinc.Value.Index.EndDate));

            RefreshDate = assumptionEndDate <= pensionIncreaseEndDate ? assumptionEndDate : pensionIncreaseEndDate;
        }

        public IEnumerable<MasterBasisKey> GetMasterbasisKeys()
        {
            return Bases.Keys;
        }

        public MasterBasis GetBasis(int masterBasisId)
        {
            return Bases[Bases.Keys.Single(x => x.Id == masterBasisId)];
        }

        protected virtual void SetBases(IEnumerable<Basis> bases)
        {
            Bases = new Dictionary<MasterBasisKey, MasterBasis>();
            // can't group on master basis Id because at time of import they'll all equal 0
            // problem is that kpmg are using display name as the key, and fusion is using a newly created int (ie, master basis Id)
            var masterBases = bases.GroupBy(x => new MasterBasisKey(x.MasterBasisId, x.Type, x.DisplayName, x.PensionSchemeId));

            foreach (var b in masterBases)
            {
                Bases.Add(b.Key, new MasterBasis(b));
            }
        }

        private Dictionary<DateTime, RecoveryPayment> GetOldRecoveryPlan(IEnumerable<RecoveryPayment> recoveryPlan)
        {
            var result = new Dictionary<DateTime, RecoveryPayment>();

            foreach (var recoveryPayment in recoveryPlan)
            {
                var contribution = recoveryPayment.Amount * recoveryPayment.Gap / 12;
                var i = 1;
                var nextDate = recoveryPayment.Date;
                while (nextDate <= recoveryPayment.EndDate)
                {
                    result.Add(nextDate, new RecoveryPayment(Id, nextDate, nextDate, contribution, 1));
                    nextDate = recoveryPayment.Date.AddMonths(i * recoveryPayment.Gap);// nextDate.AddMonths(recoveryPayment.Gap);
                    i++;
                }
            }
            return result;
        }

        public IDictionary<AssetClassType, AssetClass> GetAssetDefinitions()
        {
            var allAssets = SchemeAssets.SelectMany(x => x.Assets);
            return allAssets
                .Select(x => x.Class.Type)
                .Distinct()
                .ToDictionary(
                    x => x,
                    x => allAssets.First(asset => asset.Class.Type == x).Class
                );
        }

        public SchemeAsset GetSchemeAsset(DateTime at)
        {
            if (Funded)
            {
                if (SchemeAssets != null && SchemeAssets.Any(a => a.EffectiveDate <= at))
                {
                    return SchemeAssets.Where(a => a.EffectiveDate <= at).OrderBy(a => a.EffectiveDate).Last();
                }
            }

            return new EmptySchemeAsset();
        }

        public MarketInfo GetMarketInfo(DateTime from, DateTime till)
        {
            IList<KeyValuePair<string, string>> marketDataItems = new List<KeyValuePair<string, string>>();

            if (NamedProperties.ContainsKey(NamedPropertyGroupType.MarketData) && NamedProperties[NamedPropertyGroupType.MarketData].Properties != null)
                marketDataItems = NamedProperties[NamedPropertyGroupType.MarketData].Properties;

            var marketInfo = marketInfoProvider.GetInfo(from, till, marketDataItems);

            return marketInfo;
        }

        public IEnumerable<AssetFundGrowth> GetGrowthOverPeriod(DateTime from, DateTime till)
        {
            if (Funded) //TODO break pension scheme loading in to a factory class and use a different concrete instance for unfunded schemes - Fusion 2!
            {
                var growthOverPeriod = new GrowthOverPeriodCalculator().Calculate(SchemeAssets, SyntheticAssets, from, till);

                return growthOverPeriod;
            }

            return new List<AssetFundGrowth>();
        }

        public ILDIBasisProvider GetLDIBasisProvider(DateTime at)
        {
            return new LDIBasisProvider(this, at);
        }
    }
}
