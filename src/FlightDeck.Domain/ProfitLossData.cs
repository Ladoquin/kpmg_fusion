﻿namespace FlightDeck.Domain
{
    public class ProfitLossData
    {
        public double SalaryRoll { get; private set; }
        public double AccrualCost { get; private set; }
        public double TotalRate { get; private set; }
        public double EmployerCost { get; private set; }
        public double ServiceCostFRS { get; private set; }
        public double ServiceCostIAS { get; private set; }
        public double BenefitsPaid { get; private set; }
        public double EmployerContributions { get; private set; }
        public double DeficitContributions { get; private set; }
        public double MemberContributions { get; private set; }
        public double BenefitsPaidInsured { get; private set; }
        public double NetInvestments { get; private set; }

        public ProfitLossData(
            double salaryRoll,
            double accrualCost,
            double totalRate,
            double employerCost,
            double serviceCostFRS,
            double serviceCostIAS,
            double benefitsPaid,
            double employerConts,
            double deficitConts,
            double memberConts,
            double benefitsPaidInsured,
            double netInvesments)
        {
            SalaryRoll = salaryRoll;
            AccrualCost = accrualCost;
            TotalRate = totalRate;
            EmployerCost = employerCost;
            ServiceCostFRS = serviceCostFRS;
            ServiceCostIAS = serviceCostIAS;
            BenefitsPaid = benefitsPaid;
            EmployerContributions = employerConts;
            DeficitContributions = deficitConts;
            MemberContributions = memberConts;
            BenefitsPaidInsured = benefitsPaidInsured;
            NetInvestments = netInvesments;
        }
    }
}