﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    
    public interface ILogRepository : IRepository<Log>
    {
        IEnumerable<Log> GetAll(DateTime date, string level = null);
    }
}
