﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public interface IBasisLiabilityAssumptions
    {
        IDictionary<AssumptionType, double> LiabilityAssumptions { get; }
        LifeExpectancyAssumptions LifeExpectancy { get; }
        IDictionary<int, double> PensionIncreases { get; }
    }
}
