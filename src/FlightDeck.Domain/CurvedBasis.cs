﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    class CurvedBasis
    {
        public string Name { get; set; }
        public BasisType Type { get; set; }
        public bool IsFlat { get; set; }
        public Curve GiltYield { get; set; }
        public double PremiumPre { get; set; }
        public double PremiumPost { get; set; }
        public double DefaultPremiumPre { get; set; }
        public double DefaultPremiumPost { get; set; }
        public Curve DiscPre { get; set; }
        public Curve DiscPost { get; set; }
        public Curve DiscPen { get; set; }
        public Curve RPI { get; set; }
        public Curve CPI { get; set; }
        public IDictionary<int, Curve> Pinc { get; set; }
        public IDictionary<MemberStatus, double> LiabilityCalibration { get; set; }
        public IDictionary<MemberStatus, double> BuyinCalibration { get; set; }

        public CurvedBasis()
        {
            Pinc = new Dictionary<int, Curve>();
            LiabilityCalibration = new Dictionary<MemberStatus, double>();
            BuyinCalibration = new Dictionary<MemberStatus, double>();
        }
    }
}
