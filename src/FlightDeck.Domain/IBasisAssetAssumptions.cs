﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public interface IBasisAssetAssumptions
    {
        IDictionary<AssetClassType, double> AssetAssumptions { get; }
        RecoveryPlanAssumptions RecoveryPlanOptions { get; }
    }
}
