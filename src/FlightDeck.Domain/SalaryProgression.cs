﻿using System.Collections.Generic;

namespace FlightDeck.Domain
{
    public class SalaryProgression
    {
        public int Id { get; private set; }
        public int BasisId { get; private set; }
        public int MinAge { get; private set; }
        public int MaxAge { get; private set; }
        public int RetirementAge { get; private set; }
        public IDictionary<int, double> SalaryRoll { get; private set; }

        public SalaryProgression(int id, int basisId, int minAge, int maxAge, int retirementAge, IDictionary<int, double> salaryRoll)
        {
            SalaryRoll = salaryRoll;
            RetirementAge = retirementAge;
            MaxAge = maxAge;
            MinAge = minAge;
            BasisId = basisId;
            Id = id;
        }

        public SalaryProgression Clone()
        {
            return new SalaryProgression(
                    Id, BasisId, MinAge, MaxAge, RetirementAge, new Dictionary<int, double>(SalaryRoll)
                );
        }
    }
}