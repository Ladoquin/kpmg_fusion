﻿namespace FlightDeck.Domain.JourneyPlan
{
    using FlightDeck.DomainShared;
    using System;
    using System.Linq;
    using System.Collections.Generic;

    class JourneyPlanAfterResult
    {
        public class FROImpact
        {
            public double PreFRODefLiab { get; private set; }
            public double LiabilityDischarged { get; private set; }
            public double AssetsDisinvested { get; private set; }

            public FROImpact(double preFRODefLiab, double liabilityDischarged, double assetsDisinvested)
            {
                PreFRODefLiab = preFRODefLiab;
                LiabilityDischarged = liabilityDischarged;
                AssetsDisinvested = assetsDisinvested;
            }
        }

        public class EFROImpact
        {
            public IDictionary<MemberStatus, double> ChangeInLiabsDischarged { get; private set; }
            public double TotalLiabilityDischarged { get; private set; }
            public double NonPensionersLiabilities { get; private set; }
            public IDictionary<int, double> ActiveTransferValuesPayable { get; private set; }
            public IDictionary<int, double> DeferredTransferValuesPayable { get; private set; }
            public IDictionary<int, double> AdjustedActiveTransferValuesPayable { get; set; }
            public IDictionary<int, double> AdjustedDeferredTransferValuesPayable { get; set; }
            public DateTime AnalysisDate { get; private set; }
            public DateTime EffectiveDate { get; private set; }

            public EFROImpact(IDictionary<int, double> activeTransferValuesPayable, IDictionary<int, double> deferredTransferValuesPayable,
                            double totalLiabsDischarged, IDictionary<MemberStatus, double> changeInLiabsDischarged, double nonPensionersLiabilities,
                            DateTime analysisDate, DateTime effectiveDate)
            {
                AnalysisDate = analysisDate;
                EffectiveDate = effectiveDate;
                ActiveTransferValuesPayable = activeTransferValuesPayable;
                DeferredTransferValuesPayable = deferredTransferValuesPayable;
                ChangeInLiabsDischarged = changeInLiabsDischarged;
                TotalLiabilityDischarged = totalLiabsDischarged;
                NonPensionersLiabilities = nonPensionersLiabilities;
            }            
        }

        public class ETVImpact
        {
            public double PreETVDefLiab { get; private set; }
            public double LiabilityDischarged { get; private set; }
            public double AssetsDisinvested { get; private set; }
            public double UnenhancedTVsPaid { get; private set; }

            public ETVImpact(double preETVDefLiab, double liabilityDischarged, double assetsDisinvested, double unenhancedTVsPaid)
            {
                PreETVDefLiab = preETVDefLiab;
                LiabilityDischarged = liabilityDischarged;
                AssetsDisinvested = assetsDisinvested;
                UnenhancedTVsPaid = unenhancedTVsPaid;
            }
        }

        public class BenefitChangesImpact
        {
            public double LiabilitiesBefore { get; private set; }
            public double ServiceCostBefore { get; private set; }
            public double LiabilitiesAfter { get; private set; }
            public double ServiceCostAfter { get; private set; }
            public double BestEstimateServiceCost { get; set; }

            public BenefitChangesImpact(double liabilitiesBefore, double serviceCostBefore, double liabilitiesAfter, double serviceCostAfter)
            {
                LiabilitiesBefore = liabilitiesBefore;
                ServiceCostBefore = serviceCostBefore;
                LiabilitiesAfter = liabilitiesAfter;
                ServiceCostAfter = serviceCostAfter;
            }
        }

        public FROImpact FRO { get; set; }
        public EFROImpact EFRO { get; set; }
        public ETVImpact ETV { get; set; }
        public PIECashflowImpact PIE { get; set; }
        public BenefitChangesImpact FutureBenefits { get; set; }
        public NPV NPV { get; set; }
        public double InvGrowth { get; set; }
        public IDictionary<int, JourneyPlanItem> YearlyTotals { get; set; }
        public IEnumerable<CashflowData> Cashflow { get; set; }
        public double LiabDuration { get; set; }
    }
}
