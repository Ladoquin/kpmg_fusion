﻿namespace FlightDeck.Domain.JourneyPlan
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class JourneyPlanAfterService : JourneyPlanBaseService
    {
        private readonly ISchemeContext scheme;

        private DateTime at;
        private IBasisContext ctx;
        private IGiltYieldService giltYield;
        private JourneyPlanSchemeManagedService calcService;
        private ILiabilityCashflowStatefulService lcf;
        private JourneyPlanAfterResult.FROImpact froImpact;
        private JourneyPlanAfterResult.EFROImpact efroImpact;
        private JourneyPlanAfterResult.ETVImpact etvImpact;
        private PIECashflowImpact pieImpact;
        private JourneyPlanAfterResult.BenefitChangesImpact futureBenefitsImpact;
        private double jp2InvGrowth;
        private IDictionary<int, JourneyPlanItem> yearlyTotals;
        private IEnumerable<CashflowData> cashflowOutput;

        protected override bool CeaseAccrual
        {
            get { return true; }
        }

        protected override DateTime? CeaseAccrualDate
        {
            get { return at; }
        }

        protected override double InsDefProp
        {
            get
            {
                return scheme.Assumptions.InsuranceOptions.NonpensionerLiabilityPercInsured;
            }
        }

        protected override double InsPenProp
        {
            get
            {
                return scheme.Assumptions.InsuranceOptions.PensionerLiabilityPercInsured;
            }
        }

        protected override double GetInitialAssetVal()
        {
            var assets = ctx.EvolutionData.AssetEvolution[at].AssetValue + ctx.EvolutionData.LiabilityEvolution[at].DefBuyIn + ctx.EvolutionData.LiabilityEvolution[at].PenBuyIn;
            var currBuyin = GetInitialCurrentBuyinVal();

            assets = assets - currBuyin;

            return assets;
        }

        protected override double GetInitialCurrentBuyinVal()
        {
            return ctx.EvolutionData.LiabilityEvolution[at].DefBuyIn + ctx.EvolutionData.LiabilityEvolution[at].PenBuyIn;
        }

        protected override void ApplyUserAllocation(IPortfolio pf, double marketValue)
        {
            pf.SetMarketValue(marketValue, at);
            pf.SetStrategy(
                ctx,
                ctx.Assumptions.AssetAssumptions, 
                scheme.Assumptions.InvestmentStrategyAssetAllocation, 
                scheme.Assumptions.InvestmentStrategyOptions.SyntheticEquity, 
                scheme.Assumptions.InvestmentStrategyOptions.SyntheticCredit);
        }

        protected override double ApplyNewABF(IPortfolio pf, double marketValue)
        {
            return calcService.ApplyNewABF(at, ctx.Data, pf, marketValue);
        }

        protected override ExpectedPortfolioGrowth ApplyExpectedPortfolioGrowth(IPortfolio pf)
        {
            var giltYieldVal = giltYield.GetGiltYield(at);
            jp2InvGrowth = giltYieldVal + pf.GetOutperformance(ctx.Data.Type != BasisType.Accounting, ctx.Assumptions.AssetAssumptions);

            var expectedPortfolioGrowth = new ExpectedPortfolioGrowth { Growth = jp2InvGrowth };

            var opts = ctx.Assumptions.RecoveryPlanOptions;

            if (scheme.Assumptions.RecoveryPlanOptions != RecoveryPlanType.Current)
            {
                expectedPortfolioGrowth.LumpSum = opts.LumpSumPayment;
                expectedPortfolioGrowth.Term = opts.Term;
                expectedPortfolioGrowth.Incs = opts.Increases;
                expectedPortfolioGrowth.Amount = ctx.Results.RecoveryPlanPaymentRequired;
                expectedPortfolioGrowth.StartMonth = opts.StartMonth;
                expectedPortfolioGrowth.Start = new DateTime(at.Year, at.Month, at.Day).AddMonths(opts.StartMonth);
                expectedPortfolioGrowth.StartYear = 1 + (int)Math.Floor((double)opts.StartMonth / 12);
                expectedPortfolioGrowth.EndYear = expectedPortfolioGrowth.StartYear + opts.Term;
            }

            return expectedPortfolioGrowth;
        }

        protected override double AdjustLiabilityToDate(IPortfolio pf, double marketValue)
        {
            lcf = new LiabilityCashflowStatefulService(ctx);

            lcf.AdjustTo(at, ctx.Assumptions, true);

            pf.SetMarketValue(marketValue, at);

            //Website only allows EITHER (FRO, ETV, fut bens) OR (EFRO), not both
            if (scheme.Assumptions.FROType == FROType.Bulk)
            {
                froImpact = calcService.ApplyFRO(lcf, scheme.Assumptions.FROOptions);
                etvImpact = calcService.ApplyETV(lcf, scheme.Assumptions.ETVOptions);
                futureBenefitsImpact = calcService.ApplyBenefitChanges(lcf, scheme.Assumptions.FutureBenefitOptions);

                var assetsToDisinvest = froImpact.AssetsDisinvested + etvImpact.AssetsDisinvested; //todo: efro impact?

                pf.Disinvest(assetsToDisinvest);
            }
            else if (scheme.Assumptions.FROType == FROType.Embedded)
            {
                //Embedded FRO option at retirement (i.e. after the Analysis Date, business as usual)
                efroImpact = calcService.ApplyEmbeddedFRO(lcf, scheme.Assumptions.EFROOptions);

                calcService.ApplyEmbeddedFROTimeZeroCETVAdjustment(efroImpact);
            }

            pieImpact = calcService.ApplyPIE(lcf, scheme.Assumptions.PIEOptions, ctx.Assumptions);

            cashflowOutput = lcf.GetCashflowOutput(at).ToList();

            lcf.SetPresentValue(at);

            return pf.MarketValue;
        }

        protected override void UpdateBestEstimateServiceCost()
        {
            if (scheme.Assumptions.FROType == FROType.Embedded)
                return;

            futureBenefitsImpact.BestEstimateServiceCost = this.calcService.GetBestEstimateServiceCost(ctx, jp2InvGrowth, at);          
        }

        protected override double GetTotalRecoveryPlanContribution(IPortfolio pf, int projYear, DateTime yearStart, DateTime yearEnd, ref ExpectedPortfolioGrowth expectedPortfolioGrowth)
        {
            var oldRpcont = .0;
            var newRpcont = .0;

            if (scheme.Assumptions.RecoveryPlanOptions == RecoveryPlanType.Current)
            {
                oldRpcont = pf.GetTotalRecoveryPlanPayment(yearStart.AddDays(1), yearEnd);
            }
            else
            {
                var rp = calcService.GetTotalRecoveryPlanContribution(pf, projYear, yearStart, yearEnd, ref expectedPortfolioGrowth);

                oldRpcont = rp.Item1;
                newRpcont = rp.Item2;
            }

            return oldRpcont + newRpcont;
        }

        protected override NPV GetPresentValue(DateTime projDate)
        {
            return lcf.GetPresentValue(projDate);
        }

        protected override NPV GetBenefitOutgo(int projYear, DateTime projDate)
        {
            return lcf.GetBenefitOutgo(0, projDate);
        }

        protected override double AdjustNetBenefitOutgoForInsurance(double netBenefitOutgo, NPV ben, int projYear)
        {
            double efroActivePlusDeferreds = .0;
            if (efroImpact != null)
                efroActivePlusDeferreds = efroImpact.AdjustedActiveTransferValuesPayable[projYear] + efroImpact.AdjustedDeferredTransferValuesPayable[projYear];

            netBenefitOutgo = netBenefitOutgo + efroActivePlusDeferreds
                - InsPenProp * (ben.Liabilities[MemberStatus.Pensioner] - ben.Buyin[MemberStatus.Pensioner]) - InsDefProp * (ben.Liabilities[MemberStatus.Deferred] - ben.Buyin[MemberStatus.Deferred]);

            return netBenefitOutgo;
        }

        protected override double AdjustTotalBenefitOutgoForInsurance(double totBenefitOutgo, int projYear)
        {
            double efroActivePlusDeferreds = .0;
            if (efroImpact != null)
                efroActivePlusDeferreds = efroImpact.AdjustedActiveTransferValuesPayable[projYear] + efroImpact.AdjustedDeferredTransferValuesPayable[projYear];

            totBenefitOutgo = totBenefitOutgo + efroActivePlusDeferreds;

            return totBenefitOutgo;
        }

        protected override double AdjustTotalLiabilities(int projYear, double totalLiabs)
        {
            totalLiabs = totalLiabs + calcService.GetPresentValueOfEmbeddedFROTransferValues(projYear, ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate], efroImpact); //vba => PresentValueOfTVs

            return totalLiabs;
        }

        protected override double AdjustAssetsForLumpSum(double assets, ExpectedPortfolioGrowth expectedPortfolioGrowth, double growth)
        {
            if (scheme.Assumptions.RecoveryPlanOptions != RecoveryPlanType.Current)
            {
                assets = assets + expectedPortfolioGrowth.LumpSum * (1 + growth * expectedPortfolioGrowth.PartYear);
            }
            return assets;
        }

        protected override void SaveAnnualJourneyPlan(int projYear, NPV pv, double totLiabs, double employerConts, double employeeConts, double totRPCont, double netBenefitOutgo, double netIncome, double growth, double assets, double currBuyIn, double newBuyIn, double totAssets)
        {
            if (yearlyTotals == null)
                yearlyTotals = new Dictionary<int, JourneyPlanItem>(50);

            yearlyTotals.Add(projYear, new JourneyPlanItem
                (
                    projYear,
                    pv.Liabilities[MemberStatus.ActivePast],
                    pv.Liabilities[MemberStatus.Deferred],
                    pv.Liabilities[MemberStatus.Pensioner],
                    pv.Liabilities[MemberStatus.ActiveFuture],
                    pv.Buyin[MemberStatus.Deferred],
                    pv.Buyin[MemberStatus.Pensioner],
                    totLiabs,
                    employeeConts,
                    employerConts,
                    totRPCont,
                    netBenefitOutgo,
                    netIncome,
                    growth,
                    assets,
                    currBuyIn,
                    newBuyIn,
                    totAssets
                ));
        }

        public JourneyPlanAfterService(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public JourneyPlanAfterResult Calculate(IBasisContext ctx)
        {
            ctx.Data = ctx.Data.Adjust(
                assumptions: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToAssumptionsDictionary(),
                pensionIncreases: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToPensionIncreasesDictionary());

            this.ctx = ctx;
            this.at = ctx.AnalysisDate;
            this.giltYield = new GiltYieldService(ctx.Data);
            this.calcService = new JourneyPlanSchemeManagedService(scheme, ctx.Results);

            base.Calculate(at, true, scheme.Data, ctx.Data, ctx.EvolutionData, ctx.Portfolio);

            return new JourneyPlanAfterResult()
            {
                FRO = froImpact,
                EFRO = efroImpact,
                ETV = etvImpact,
                PIE = pieImpact,
                FutureBenefits = futureBenefitsImpact,
                InvGrowth = jp2InvGrowth,
                YearlyTotals = yearlyTotals,
                Cashflow = cashflowOutput,
                LiabDuration = lcf != null ? lcf.GetLiabDuration() : .0
            };
        }
    }
}
