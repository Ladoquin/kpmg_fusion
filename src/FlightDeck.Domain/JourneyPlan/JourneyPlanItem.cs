﻿namespace FlightDeck.Domain.JourneyPlan
{
    class JourneyPlanItem
    {
        public int Year { get; private set; }
        //liabs
        public double Act { get; private set; }
        public double Def { get; private set; }
        public double Pen { get; private set; }
        public double Fut { get; private set; }
        public double DefBuyin { get; private set; }
        public double PenBuyin { get; private set; }
        public double TotalLiability { get; private set; }
        //assets
        public double EEConts { get; private set; }
        public double ERConts { get; private set; }
        public double RP { get; private set; }
        public double BenefitOutgo { get; private set; }
        public double NetIncome { get; private set; }
        public double Growth { get; private set; }
        public double AssetsAtEnd { get; private set; }
        public double CurrentBuyin { get; private set; }
        public double NewBuyin { get; private set; }
        public double TotalAssets { get; private set; }
        
        public JourneyPlanItem(
            int year, 
            double act, double def, double pen, double fut, double defBuyin, double penBuyin, double totalLiability,
            double eeConts, double erConts, double rp, double benefitOutgo, double netIncome, double growth, double assetsAtEnd, double currentBuyin, double newBuyin, double totalAssets)
        {
            Year = year;
            Act = act;
            Def = def;
            Pen = pen;
            Fut = fut;
            DefBuyin = defBuyin;
            PenBuyin = penBuyin;
            TotalLiability = totalLiability;
            EEConts = eeConts;
            ERConts = erConts;
            RP = rp;
            BenefitOutgo = benefitOutgo;
            NetIncome = netIncome;
            Growth = growth;
            AssetsAtEnd = assetsAtEnd;
            CurrentBuyin = currentBuyin;
            NewBuyin = newBuyin;
            TotalAssets = totalAssets;
        }
    }
}
