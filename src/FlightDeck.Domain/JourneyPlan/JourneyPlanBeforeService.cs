﻿namespace FlightDeck.Domain.JourneyPlan
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;    
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class JourneyPlanBeforeService : JourneyPlanBaseService
    {
        private readonly ISchemeContext scheme;
        private DateTime at;
        private IBasisContext ctx;
        private IGiltYieldService giltYield;
        private JourneyPlanSchemeManagedService calcService;

        private ILiabilityCashflowStatefulService lcf;
        private double jpInvGrowth;
        private Dictionary<int, JourneyPlanItem> yearlyTotals;

        protected override bool CeaseAccrual
        {
            get { return true; }
        }

        protected override DateTime? CeaseAccrualDate
        {
            get { return at; }
        }

        public JourneyPlanBeforeService(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public JourneyPlanBeforeResult Calculate(IBasisContext ctx)
        {
            ctx.Data = ctx.Data.Adjust(
                assumptions: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToAssumptionsDictionary(),
                pensionIncreases: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToPensionIncreasesDictionary());

            this.ctx = ctx;
            this.at = ctx.AnalysisDate;
            this.giltYield = new GiltYieldService(ctx.Data);
            this.calcService = new JourneyPlanSchemeManagedService(scheme, ctx.Results);

            base.Calculate(at, true, scheme.Data, ctx.Data, ctx.EvolutionData, ctx.Portfolio);

            //todo: lots calculated here thats also done in BasisAnalysisService.calculateBenefitChanges ... factor out to one place!
            var employeeRate = scheme.Data.ContributionRates.Where(r => r.Date <= ctx.AnalysisDate).OrderBy(r => r.Date).Last().Employee;
            var serviceCostEstimate = this.calcService.GetBestEstimateServiceCost(ctx, jpInvGrowth, at);
            var salaryRollAtAnalysisDate = new BasisQueries().GetSalaryRollAtDate(ctx.Data, at);
            var FBBestEstimateContRate = serviceCostEstimate.MathSafeDiv(salaryRollAtAnalysisDate);
            var estimatedAdjustment = (FBBestEstimateContRate - scheme.Data.ContractingOutContributionRate).MathSafeDiv(FBBestEstimateContRate);

            return new JourneyPlanBeforeResult
            {
                YearlyTotals = yearlyTotals,
                PreFRODeferredLiability = lcf.GetPreFRODeferredLiability(),
                PreETVDeferredLiability = lcf.GetPreETVDeferredLiability(),
                EmploymentRate = employeeRate,
                ServiceCostEstimate = serviceCostEstimate,
                ContributionRateEstimate = FBBestEstimateContRate,
                EstimatedBenefitReduction = estimatedAdjustment,
                InvGrowth = jpInvGrowth
            };
        }

        protected override double GetInitialAssetVal()
        {
            var assets = ctx.EvolutionData.AssetEvolution[at].AssetValue + ctx.EvolutionData.LiabilityEvolution[at].DefBuyIn + ctx.EvolutionData.LiabilityEvolution[at].PenBuyIn;
            var currBuyin = GetInitialCurrentBuyinVal();

            assets = assets - currBuyin;

            return assets;
        }

        protected override double GetInitialCurrentBuyinVal()
        {
            return ctx.EvolutionData.LiabilityEvolution[at].DefBuyIn + ctx.EvolutionData.LiabilityEvolution[at].PenBuyIn;
        }

        protected override void ApplyUserAllocation(IPortfolio pf, double marketValue)
        {
            pf.SetStrategy(ctx, expectedReturns: ctx.Assumptions.AssetAssumptions);
        }

        protected override ExpectedPortfolioGrowth ApplyExpectedPortfolioGrowth(IPortfolio pf)
        {
            var giltYieldVal = giltYield.GetGiltYield(at);
            jpInvGrowth = giltYieldVal + pf.GetOutperformance(ctx.Data.Type != BasisType.Accounting, ctx.Assumptions.AssetAssumptions);

            var expectedPortfolioGrowth = new ExpectedPortfolioGrowth { Growth = jpInvGrowth };

            return expectedPortfolioGrowth;
        }

        protected override double AdjustLiabilityToDate(IPortfolio pf, double marketValue)
        {
            lcf = new LiabilityCashflowStatefulService(ctx);

            lcf.AdjustTo(at, ctx.Assumptions, true);

            pf.SetMarketValue(marketValue, at);

            //Could do cashflow outputs here, but will allow JourneyPlanAfter to take care of it
            //as the first time through there won't be any assumption changes and JourneyPlanAfter's cashflow output
            //will match what would be done here. 
            //That can then be cached, and JourneyPlanAfter will then produce the 'after' figures on subsequent runs.

            return marketValue;
        }

        protected override double GetTotalRecoveryPlanContribution(IPortfolio pf, int projYear, DateTime yearStart, DateTime yearEnd, ref ExpectedPortfolioGrowth expectedPortfolioGrowth)
        {
            var oldRpcont = .0;
            var newRpcont = .0;

            oldRpcont = pf.GetTotalRecoveryPlanPayment(yearStart.AddDays(1), yearEnd);

            return oldRpcont + newRpcont;
        }

        protected override NPV GetPresentValue(DateTime projDate)
        {
            return lcf.GetPresentValue(projDate);
        }

        protected override NPV GetBenefitOutgo(int projYear, DateTime projDate)
        {
            return lcf.GetBenefitOutgo(projYear, projDate);
        }

        protected override double AdjustNetBenefitOutgoForInsurance(double netBenefitOutgo, NPV ben, int projYear)
        {
            netBenefitOutgo = netBenefitOutgo - InsPenProp * (ben.Liabilities[MemberStatus.Pensioner] - ben.Buyin[MemberStatus.Pensioner]) - InsDefProp * (ben.Liabilities[MemberStatus.Deferred] - ben.Buyin[MemberStatus.Deferred]);

            return netBenefitOutgo;
        }

        protected override void SaveAnnualJourneyPlan(int projYear, NPV pv, double totLiabs, double employerConts, double employeeConts, double totRPCont, double netBenefitOutgo, double netIncome, double growth, double assets, double currBuyIn, double newBuyIn, double totAssets)
        {
            if (yearlyTotals == null)
                yearlyTotals = new Dictionary<int, JourneyPlanItem>(50);

            yearlyTotals.Add(projYear, new JourneyPlanItem
                (
                    projYear,
                    pv.Liabilities[MemberStatus.ActivePast],
                    pv.Liabilities[MemberStatus.Deferred],
                    pv.Liabilities[MemberStatus.Pensioner],
                    pv.Liabilities[MemberStatus.ActiveFuture],
                    pv.Buyin[MemberStatus.Deferred],
                    pv.Buyin[MemberStatus.Pensioner],
                    totLiabs,
                    employeeConts,
                    employerConts,
                    totRPCont,
                    netBenefitOutgo,
                    netIncome, 
                    growth,
                    assets,
                    currBuyIn, 
                    newBuyIn, 
                    totAssets
                ));
        }
    }
}
