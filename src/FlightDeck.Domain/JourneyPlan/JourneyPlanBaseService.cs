﻿namespace FlightDeck.Domain.JourneyPlan
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    abstract class JourneyPlanBaseService
    {
        private struct ProjectConts
        {
            public double ErConts { get; private set; }
            public double EeConts { get; private set; }

            public ProjectConts(double erConts, double eeConts)
                : this()
            {
                ErConts = erConts;
                EeConts = eeConts;
            }
        }

        /// <summary>
        /// Shared Contribution class doesn't have End date, so creating a private struct here for ProjectConts calculation.
        /// </summary>
        private class ContRate
        {
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
            public double Salary { get; set; }
            public double EeRate { get; set; }
            public double ErRate { get; set; }

            public ContRate(DateTime start, DateTime end, double salary, double eeRate, double erRate)
            {
                Start = start;
                End = end;
                Salary = salary;
                EeRate = eeRate;
                ErRate = erRate;
            }
        }

        public const int MaxYear = 50;

        protected abstract double GetInitialAssetVal();
        protected abstract double GetInitialCurrentBuyinVal();
        protected abstract ExpectedPortfolioGrowth ApplyExpectedPortfolioGrowth(IPortfolio pf);
        protected abstract double GetTotalRecoveryPlanContribution(IPortfolio pf, int projYear, DateTime yearStart, DateTime yearEnd, ref ExpectedPortfolioGrowth expectedPortfolioGrowth);
        protected abstract NPV GetPresentValue(DateTime projDate);
        protected abstract NPV GetBenefitOutgo(int projYear, DateTime projDate);

        protected virtual double InsPenProp { get { return .0; } }
        protected virtual double InsDefProp { get { return .0; } }
        protected virtual bool CeaseAccrual { get { return false; } }
        protected virtual DateTime? CeaseAccrualDate { get { return null; } }
        protected virtual double ApplyNewABF(IPortfolio pf, double marketValue) { return marketValue; }
        protected virtual double AdjustLiabilityToDate(IPortfolio pf, double marketValue) { return marketValue; }
        protected virtual void ApplyUserAllocation(IPortfolio pf, double marketValue) { }
        protected virtual void UpdateBestEstimateServiceCost() { }
        protected virtual NPV AdjustPV(int projYear, NPV pv) { return pv; }
        protected virtual double AdjustTotalLiabilities(int projYear, double totalLiabs) { return totalLiabs; }
        protected virtual void CreateNewRecoveryPlan(int projYear, double[] totalAssets, double[] totalLiability, double growth, ExpectedPortfolioGrowth expectedPortfolioGrowth) { }
        protected virtual double AdjustNetBenefitOutgoForInsurance(double netBenefitOutgo, NPV ben, int projYear) { return netBenefitOutgo; }
        protected virtual double AdjustTotalBenefitOutgoForInsurance(double totBenefitOutgo, int projYear) { return totBenefitOutgo; }
        protected virtual double AdjustGrowthFromLiabilityChange(double growth, int projYear, double[] totalLiab, double totBenefitOutgo, ExpectedPortfolioGrowth expectedPortfolioGrowth) { return growth; }
        protected virtual double AdjustAssetsForLumpSum(double assets, ExpectedPortfolioGrowth expectedPortfolioGrowth, double growth) { return assets; }
        protected virtual void SaveAnnualJourneyPlan(
            int projYear, 
            NPV pv, double totLiabs,
            double employerConts, double employeeConts, double totRPCont, double netBenefitOutgo, double netIncome, double growth,
            double assets, double currBuyIn, double newBuyIn, double totAssets)
        {
        }

        protected void Calculate(DateTime date, bool userBasis, ISchemeData scheme, Basis b, BasisEvolutionData evolution, IPortfolio pf)
        {
            var liabilityEffectiveDate = b.EffectiveDate;
            var liabilityEvolution = evolution.LiabilityEvolution;

            var assets = new double[MaxYear + 1];
            var currBuyIn = new double[MaxYear + 1];

            assets[0] = GetInitialAssetVal();
            currBuyIn[0] = GetInitialCurrentBuyinVal();

            ApplyUserAllocation(pf, assets[0]);

            assets[0] = ApplyNewABF(pf, assets[0]);

            var expectedPortfolioGrowth = ApplyExpectedPortfolioGrowth(pf);

            var growth = expectedPortfolioGrowth.Growth;

            var projectContsAssumptions = new AssumptionCoefficient(new BasisLiabilityAssumptions(liabilityEvolution[liabilityEffectiveDate]));

            var projectConts = GetProjectConts(date, b, scheme.ContributionRates, projectContsAssumptions, userBasis);

            assets[0] = AdjustLiabilityToDate(pf, assets[0]);

            UpdateBestEstimateServiceCost();

            var totalLiab = new double[MaxYear + 1];            
            var newBuyIn = new double[MaxYear + 1];
            var totAssets = new double[MaxYear + 1];
            var netBenefitOutgo = .0;
            var totRPCont = .0;
            var netIncome = .0;
            var yearEnd = date;
            for (var projYear = 0; projYear <= MaxYear; projYear++)
            {

                // The default behavior of the VBA DateSerial fuction when presented with 29th of feb for not a leap year is to return 1st March
                var dayOfYear = date.Day;
                var month = date.Month;
                if (!DateTime.IsLeapYear(date.Year + projYear) && date.Month == 2 && date.Day == 29)
                {
                    dayOfYear = 1;
                    month = 3;
                }

                var projDate = new DateTime(date.Year + projYear, month, dayOfYear);

                var yearStart = yearEnd;
                yearEnd = projDate;

                var pv = GetPresentValue(projDate);

                pv = AdjustPV(projYear, pv);                

                totalLiab[projYear] = pv.Liabilities[MemberStatus.ActivePast] + pv.Liabilities[MemberStatus.Deferred] + pv.Liabilities[MemberStatus.Pensioner];

                totalLiab[projYear] = AdjustTotalLiabilities(projYear, totalLiab[projYear]);

                currBuyIn[projYear] = pv.Buyin[MemberStatus.Deferred] + pv.Buyin[MemberStatus.Pensioner];

                if (projYear > 0)
                {
                    totRPCont = GetTotalRecoveryPlanContribution(pf, projYear, yearStart, yearEnd, ref expectedPortfolioGrowth);

                    var ben = GetBenefitOutgo(projYear, projDate);

                    var totBenefitOutgo = ben.Liabilities[MemberStatus.ActivePast] + ben.Liabilities[MemberStatus.Deferred] + ben.Liabilities[MemberStatus.Pensioner];

                    if (!CeaseAccrual)
                    {
                        totBenefitOutgo += ben.Liabilities[MemberStatus.ActiveFuture] * projYear;
                    }

                    netBenefitOutgo = totBenefitOutgo - (ben.Buyin[MemberStatus.Deferred] + ben.Buyin[MemberStatus.Pensioner]);

                    totBenefitOutgo = AdjustTotalBenefitOutgoForInsurance(totBenefitOutgo, projYear);
                    netBenefitOutgo = AdjustNetBenefitOutgoForInsurance(netBenefitOutgo, ben, projYear);

                    netIncome =
                        (CeaseAccrual ? 0 : projectConts[projYear].EeConts + projectConts[projYear].ErConts)
                        + totRPCont
                        - netBenefitOutgo;

                    growth = AdjustGrowthFromLiabilityChange(growth, projYear, totalLiab, totBenefitOutgo, expectedPortfolioGrowth);

                    assets[projYear] = assets[projYear - 1] * (1 + growth) + netIncome * (1 + growth / 2);

                    if (projYear == expectedPortfolioGrowth.StartYear)
                    {
                        assets[projYear] = AdjustAssetsForLumpSum(assets[projYear], expectedPortfolioGrowth, growth);
                    }
                }

                var insPenValue = InsPenProp * (pv.Liabilities[MemberStatus.Pensioner] - pv.Buyin[MemberStatus.Pensioner]);
                var insDefValue = InsDefProp * (pv.Liabilities[MemberStatus.ActivePast] + pv.Liabilities[MemberStatus.Deferred] - pv.Buyin[MemberStatus.Deferred]);
                newBuyIn[projYear] = insPenValue + insDefValue;

                totAssets[projYear] = assets[projYear] + currBuyIn[projYear] + newBuyIn[projYear];

                CreateNewRecoveryPlan(projYear, totAssets, totalLiab, growth, expectedPortfolioGrowth);

                var employerConts = CeaseAccrual || projYear == 0 ? 0 : projectConts[projYear].ErConts;
                var employeeConts = CeaseAccrual || projYear == 0 ? 0 : projectConts[projYear].EeConts;

                if (!scheme.Funded)
                {
                    netIncome = .0;
                    assets[projYear] = .0;
                    totAssets[projYear] = .0;
                    growth = .0;
                }

                SaveAnnualJourneyPlan(
                    projYear, 
                    pv, totalLiab[projYear],
                    employerConts, employeeConts, totRPCont, netBenefitOutgo, netIncome, growth,
                    assets[projYear], currBuyIn[projYear], newBuyIn[projYear], totAssets[projYear]
                    );
            }
        }

        private IDictionary<int, ProjectConts> GetProjectConts(DateTime date, Basis b, IList<Contribution> contributionRates, AssumptionCoefficient assumptions, bool userBasis)
        {
            // Assumes there is only one salary progression, which is ok for now, but multiple are possible

            var projectConts = new Dictionary<int, ProjectConts>(50);

            var salaries = new Dictionary<int, double>(100);

            var yMax = 100; // can this be dynamic? In tracker it's dynamically pulled from the salary progression max row, but I think this is always 100.

            for (var y = 1; y <= yMax; y++)
            {
                var salary = b.SalaryProgression.Count > 0 && b.SalaryProgression[0].SalaryRoll.ContainsKey(y) 
                    ? b.SalaryProgression[0].SalaryRoll[y] 
                    : 0;

                salary = salary * Math.Pow((1 + assumptions.Variable.Assumptions[AssumptionType.SalaryIncrease]) / (1 + assumptions.Base.Assumptions[AssumptionType.SalaryIncrease]), y - 0.5);

                salaries.Add(y, salary);
            }

            var i = 0;
            var data = new Dictionary<int, ContRate>(); // using a dictionary to mimic a 1 based index
            var splitDate = date;
            var splitNo = 0;

            for (var y = 1; y <= yMax; y++)
            {
                i++;
                data.Add(i, new ContRate
                    (
                        date.AddYears(y - 1),
                        date.AddYears(y).AddDays(-1),
                        salaries[y],
                        -1,
                        -1
                    ));

                if (data[i].Start < splitDate && splitDate <= data[i].End)
                {
                    data.Add(i + 1, data[i]);
                    data[i].Salary = data[i].Salary * ((splitDate - data[i].Start).TotalDays / (data[i].End.AddDays(1) - data[i].Start).TotalDays);
                    data[i + 1].Salary = data[i + 1].Salary - data[i].Salary;

                    data[i].End = splitDate.AddDays(-1);
                    data[i + 1].Start = splitDate;

                    i++;
                    splitNo++;
                    splitDate = date.AddYears(splitNo);
                }
            }
            var used = i;

            foreach (var contRate in contributionRates)
            {
                var cn = new ContRate(contRate.Date, DateTime.MinValue, -1, contRate.Employee, contRate.Employer);

                for (i = 1; i <= used; i++)
                {
                    if (cn.Start == data[i].Start)
                    {
                        data[i].EeRate = cn.EeRate;
                        data[i].ErRate = cn.ErRate;
                        break;
                    }
                    else if (cn.Start > data[i].Start && cn.Start < data[i].End)
                    {
                        cn.Salary = data[i].Salary;
                        data[i].Salary = data[i].Salary * ((cn.Start - data[i].Start).TotalDays / ((data[i].End.AddDays(1) - data[i].Start).TotalDays));
                        cn.Salary = cn.Salary - data[i].Salary;

                        cn.End = data[i].End;
                        data[i].End = cn.Start.AddDays(-1);

                        for (int j = used; j <= i + 1; j--)
                        {
                            data[j + 1] = data[j];
                        }

                        data[i + 1] = cn;
                        used++;
                        break;
                    }
                }
            }

            if (data[1].EeRate < 0)
            {
                if (contributionRates[0].Date <= data[1].Start)
                {
                    data[1].EeRate = contributionRates[0].Employee;
                    data[1].ErRate = contributionRates[0].Employer;
                }
                else
                {
                    data[1].EeRate = 0;
                    data[1].ErRate = 0;
                }
            }

            for (i = 2; i <= used; i++)
            {
                if (data.ContainsKey(i))
                {
                    if (data[i].EeRate < 0) data[i].EeRate = data[i - 1].EeRate;
                    if (data[i].ErRate < 0) data[i].ErRate = data[i - 1].ErRate;
                }
                else
                {
                    data.Add(i, new ContRate(DateTime.MinValue, DateTime.MinValue, 0, 0, 0));
                }
            }

            var yearEnd = date.AddDays(-1);
            for (var y = 1; y <= yMax; y++)
            {
                var yearStart = yearEnd.AddDays(1);

                var dayOfYear = date.Day;
                var month = date.Month;
                if (!DateTime.IsLeapYear(date.Year + y) && date.Month == 2 && date.Day == 29)
                {
                    dayOfYear = 1;
                    month = 3;
                }

                yearEnd = new DateTime(date.Year + y, month, dayOfYear).AddDays(-1);

                var eeconts = .0;
                var erconts = .0;

                for (i = 1; i <= used; i++)
                {
                    if (yearStart <= data[i].Start && data[i].End <= yearEnd)
                    {
                        eeconts += data[i].Salary * data[i].EeRate;
                        erconts += data[i].Salary * data[i].ErRate;
                    }
                    else if (data[i].Start > yearEnd)
                    {
                        break;
                    }
                }

                projectConts.Add(y, new ProjectConts(erconts, eeconts));
            }

            return projectConts;
        }

    }
}
