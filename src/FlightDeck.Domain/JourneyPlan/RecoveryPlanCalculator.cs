﻿using FlightDeck.Domain.Evolution;
using FlightDeck.Domain.JourneyPlan;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain.JourneyPlan
{
    class RecoveryPlanCalculator
    {
        public class RecoveryPlanCalculatorOutput
        {
            public double PaymentRequired { get; private set; }
            public IList<BeforeAfter<double>> Plan { get; private set; }

            public RecoveryPlanCalculatorOutput(double paymentRequired, IList<BeforeAfter<double>> plan)
            {
                PaymentRequired = paymentRequired;
                Plan = plan;
            }
        }

        private readonly ISchemeContext scheme;

        public RecoveryPlanCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public RecoveryPlanCalculatorOutput Calculate(IBasisContext ctx)
        {
            var opts = ctx.Assumptions.RecoveryPlanOptions;

            var startDateIs29thFeb = DateTime.IsLeapYear(ctx.AnalysisDate.Year) && ctx.AnalysisDate.Month == 2 && ctx.AnalysisDate.Day == 29;

            var startDate = ctx.AnalysisDate.AddMonths(opts.StartMonth);

            var paymentRequired = GetPaymentRequired(ctx);

            var oldPlan = scheme.Data.RecoveryPlanData;

            var newPlan = new List<RecoveryPayment>();
            var prevAmount = .0;
            var periodStart = ctx.AnalysisDate.AddMonths(opts.StartMonth);
            for (var i = 0; i < opts.Term; i++)
            {
                var amount = paymentRequired;
                if (i > 0)
                {
                    periodStart = periodStart.AddYears(1);

                    if (startDateIs29thFeb && DateTime.IsLeapYear(periodStart.Year))
                        periodStart = periodStart.AddDays(1);

                    var term = i + 1;
                    amount = term <= opts.Term
                        ? prevAmount * (1 + opts.Increases)
                        : 0;
                }
                prevAmount = amount;
                newPlan.Add(new RecoveryPayment
                    (
                        0,
                        periodStart,
                        periodStart.AddYears(1).AddDays(-1),
                        amount,
                        1
                    ));
            }

            var payments = new List<BeforeAfter<double>>(20);
            for (var year = 1; year <= Utils.MaxRecoveryPlanYear; year++)
            {
                var s = ctx.AnalysisDate.AddYears(year - 1);
                var e = s.AddYears(1).AddDays(-1);                
                var op = GetPayment(oldPlan, s, new DateTime(Math.Min(startDate.AddDays(-1).Ticks, e.Ticks)));
                var np = GetPayment(newPlan, s, e);
                var l = year == opts.StartYear ? opts.LumpSumPayment : 0;

                var oldPlanPayment = GetPayment(oldPlan, s, e);

                payments.Add(new BeforeAfter<double>(oldPlanPayment, op + np + l));
            }

            while (payments.Count > 0 && payments.Last().Before == 0 && payments.Last().After == 0)
            {
                payments.RemoveAt(payments.Count - 1);
            };

            return new RecoveryPlanCalculatorOutput(paymentRequired, payments);
        }

        private double GetPaymentRequired(IBasisContext ctx)
        {
            ctx.Data = ctx.Data.Adjust(
                assumptions: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToAssumptionsDictionary(),
                pensionIncreases: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToPensionIncreasesDictionary());

            var jp = new JourneyPlanRecoveryPlanService(scheme);

            var paymentRequired = jp.Calculate(ctx, ctx.Portfolio);

            return paymentRequired;
        }

        public double GetPayment(IEnumerable<RecoveryPayment> periods, DateTime startDate, DateTime endDate)
        {
            var result = .0;

            foreach (var rp in periods)
            {
                if (startDate <= rp.EndDate && endDate >= rp.Date && rp.Amount > 0)
                {
                    var rpinstalment = rp.Amount * rp.Gap / 12;
                    var m = 0;
                    var d = rp.Date;
                    do
                    {
                        if (d >= startDate)
                        {
                            result += rpinstalment;
                        }
                        m += rp.Gap;
                        d = rp.Date.AddMonths(m);
                    }
                    while (d <= rp.EndDate && d <= endDate);
                }
            }

            return result;
        }
    }
}
