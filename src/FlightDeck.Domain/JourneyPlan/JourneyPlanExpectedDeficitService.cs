﻿namespace FlightDeck.Domain.JourneyPlan
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    class JourneyPlanExpectedDeficitService : JourneyPlanBaseService
    {
        private readonly ISchemeData scheme;
        private DateTime at;
        private IBasisContext ctx;
        private IGiltYieldService giltYield;

        private double initialMarketValue;
        private ILiabilityCashflowService lcf;
        private Dictionary<int, BalanceData> expectedDeficit;

        public JourneyPlanExpectedDeficitService(ISchemeData scheme)
        {
            this.scheme = scheme;
        }

        protected override double GetInitialAssetVal()
        {
            return initialMarketValue;
        }

        protected override double GetInitialCurrentBuyinVal()
        {
            return ctx.EvolutionData.LiabilityEvolution[at].DefBuyIn + ctx.EvolutionData.LiabilityEvolution[at].PenBuyIn;
        }

        protected override ExpectedPortfolioGrowth ApplyExpectedPortfolioGrowth(IPortfolio pf)
        {
            var giltYieldVal = giltYield.GetGiltYield(at);
            var growth = giltYieldVal + pf.GetOutperformance(ctx.Data.Type != BasisType.Accounting);

            return new ExpectedPortfolioGrowth
            {
                Growth = growth
            };
        }

        protected override double GetTotalRecoveryPlanContribution(IPortfolio pf, int projYear, DateTime yearStart, DateTime yearEnd, ref ExpectedPortfolioGrowth expectedPortfolioGrowth)
        {
            return pf.GetTotalRecoveryPlanPayment(yearStart.AddDays(1), yearEnd);
        }

        protected override NPV AdjustPV(int projYear, NPV pv)
        {
            pv.Liabilities[MemberStatus.ActivePast] += pv.Liabilities[MemberStatus.ActiveFuture] * projYear;

            return pv;
        }

        protected override NPV GetPresentValue(DateTime projDate)
        {
            return lcf.GetPresentValue(ctx.Data.Cashflows, projDate, ctx.Assumptions);
        }

        protected override NPV GetBenefitOutgo(int projYear, DateTime projDate)
        {
            return lcf.GetBenefitOutgo(projYear, ctx.Data.Cashflows);
        }

        protected override void SaveAnnualJourneyPlan(int projYear, NPV pv, double totLiabs, double employerConts, double employeeConts, double totRPCont, double netBenefitOutgo, double netIncome, double growth, double assets, double currBuyIn, double newBuyIn, double totAssets)
        {
            if (expectedDeficit == null)
                expectedDeficit = new Dictionary<int, BalanceData>(50);

            expectedDeficit.Add(projYear, new BalanceData(totAssets, totLiabs));
        }

        public IDictionary<int, BalanceData> Calculate(IBasisContext ctx, IPortfolio pf)
        {
            ctx = ctx
                .AdjustData(
                    pensionIncreases: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToPensionIncreasesDictionary())
                .AdjustAssumptions(
                    pensionIncreases: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToPensionIncreasesDictionary());

            this.ctx = ctx;
            this.at = ctx.StartDate;
            this.giltYield = new GiltYieldService(ctx.Data);

            initialMarketValue = pf.MarketValue;
            lcf = new LiabilityCashflowStatelessService(ctx.Data);

            base.Calculate(at, false, scheme, ctx.Data, ctx.EvolutionData, pf);

            return expectedDeficit;
        }
    }
}
