﻿using FlightDeck.Domain.Assets;
using FlightDeck.Domain.Evolution;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain.JourneyPlan
{
    /// <summary>
    /// Provide shared calculations between JourneyPlan service classes that deal with 
    /// calculating the Journey Plan while taking scheme managed options in to account. 
    /// For example, JourneyPlanAfterService and JourneyPlanRecoveryPlan both apply FRO/ETV/PIE
    /// to cashflows when evaluating their JourneyPlan result. 
    /// </summary>
    /// <remarks>
    /// Note, using this class in isolation doesn't make much sense, it should be used in the context of 
    /// a JourneyPlan calculation. 
    /// </remarks>
    class JourneyPlanSchemeManagedService
    {
        private readonly ISchemeContext scheme;
        private readonly IBasisAnalysisResults results;

        public JourneyPlanSchemeManagedService(ISchemeContext scheme, IBasisAnalysisResults results)
        {
            this.scheme = scheme;
            this.results = results;
        }

        public double ApplyNewABF(DateTime at, Basis b, IPortfolio pf, double marketValue)
        {
            if (scheme.Assumptions.ABFOptions.Value > 0 && b.Type != BasisType.Accounting)
            {
                pf.SetMarketValue(marketValue, at);
                pf.SetNewABF(scheme.Assumptions.ABFOptions.Value);
                marketValue = pf.MarketValue;
            }
            marketValue = marketValue + scheme.Assumptions.InvestmentStrategyOptions.CashInjection - (scheme.Calculated.BuyinCost.CostOfPensionerBuyin + scheme.Calculated.BuyinCost.CostOfNonpensionerBuyin);
            return marketValue;
        }

        public JourneyPlanAfterResult.FROImpact ApplyFRO(ILiabilityCashflowStatefulService lcf, FlexibleReturnOptions froArgs)
        {
            var preFRODefLiab = lcf.GetPreFRODeferredLiability();

            var liabilityDischarged = lcf.ApplyFRO(froArgs);

            var cetvRatio = froArgs.EquivalentTansferValue.MathSafeDiv(preFRODefLiab);

            var assetsDisinvested = liabilityDischarged * cetvRatio;

            return new JourneyPlanAfterResult.FROImpact(preFRODefLiab, liabilityDischarged, assetsDisinvested);
        }

        public JourneyPlanAfterResult.EFROImpact ApplyEmbeddedFRO(ILiabilityCashflowStatefulService lcf, EmbeddedFlexibleReturnOptions efroArgs)
        {
            var efroResult = lcf.ApplyEmbeddedFRO(efroArgs);

            return efroResult;
        }

        public JourneyPlanAfterResult.ETVImpact ApplyETV(ILiabilityCashflowStatefulService lcf, EnhancedTransferValueOptions etvArgs)
        {
            var preETVDefLiab = lcf.GetPreETVDeferredLiability();

            var liabilityDischarged = lcf.ApplyETV(etvArgs);

            var cetvRatio = etvArgs.EquivalentTransferValue.MathSafeDiv(preETVDefLiab);

            var unenhancedTVsPaid = liabilityDischarged * cetvRatio;

            var assetsDisinvested = etvArgs.EquivalentTransferValue * etvArgs.TakeUpRate;

            return new JourneyPlanAfterResult.ETVImpact(preETVDefLiab, liabilityDischarged, assetsDisinvested, unenhancedTVsPaid);
        }

        public PIECashflowImpact ApplyPIE(ILiabilityCashflowStatefulService lcf, PieOptions pieArgs, IBasisLiabilityAssumptions clientAssumptions)
        {
            var pieResult = lcf.ApplyPIE(pieArgs, results.PIEInit, clientAssumptions.PensionIncreases);

            return pieResult;
        }

        public JourneyPlanAfterResult.BenefitChangesImpact ApplyBenefitChanges(ILiabilityCashflowStatefulService lcf, FutureBenefitsParameters fbArgs)
        {
            var benefitChangeResult = lcf.ApplyBenefitChanges(fbArgs);

            return benefitChangeResult;
        }

        public Tuple<double, double> GetTotalRecoveryPlanContribution(IPortfolio pf, int projYear, DateTime yearStart, DateTime yearEnd, ref ExpectedPortfolioGrowth expectedPortfolioGrowth)
        {
            var rp = expectedPortfolioGrowth;
            var oldRpCont = pf.GetTotalRecoveryPlanPayment(yearStart.AddDays(1), yearEnd.Ticks < rp.Start.AddDays(-1).Ticks ? yearEnd : rp.Start.AddDays(-1));
            var newRpCont = .0;

            if (projYear == rp.StartYear)
            {
                rp.PartYear = 1.0 - (rp.StartMonth % 12) / 12.0;
                if (rp.PartYear == 0)
                {
                    rp.PartYear = 1;
                }
                newRpCont = rp.Amount * rp.PartYear;
                rp.Amount = rp.Amount * (1 + rp.Incs * rp.PartYear);
            }
            else if (rp.StartYear < projYear && projYear < rp.EndYear)
            {
                newRpCont = rp.Amount;
                rp.Amount = rp.Amount * (1 + rp.Incs);
            }
            else if (projYear == rp.EndYear)
            {
                newRpCont = rp.Amount * (1 - rp.PartYear) / (1 + rp.Incs * rp.PartYear);
            }

            expectedPortfolioGrowth = rp;
            
            return Tuple.Create(oldRpCont, newRpCont);
        }

        public double GetPresentValueOfEmbeddedFROTransferValues(int startYear, double discountRate, JourneyPlanAfterResult.EFROImpact efro, MemberStatus? reqStatus = null)
        {
            double pv = .0;

            if (efro == null)
                return pv;

            var useActiveTVPayable = reqStatus == null || reqStatus.Value == MemberStatus.ActivePast;
            var useDeferredTVPayable = reqStatus == null || reqStatus.Value == MemberStatus.Deferred;

            for (int y = startYear + 1; y < Utils.MaxYear; y++)
            {
                pv = pv + ((
                            (useActiveTVPayable ? efro.AdjustedActiveTransferValuesPayable[y] : 0)
                          + (useDeferredTVPayable ? efro.AdjustedDeferredTransferValuesPayable[y] : 0) 
                          )
                          / Math.Pow((1 + discountRate), (y - 0.5 - startYear))
                    );
            }

            return pv;
        }

        public Dictionary<int, Dictionary<MemberStatus, double>> ProjectedLiabilities(double discRate, JourneyPlanAfterResult.EFROImpact efroImpact)
        {
            // vba => ProjLiabArray
            var projLiabArray = new Dictionary<int, Dictionary<MemberStatus, double>>();

            for(var t = 0; t <= Utils.MaxYear; t++)
            {
                var activeTV = GetPresentValueOfEmbeddedFROTransferValues(t, discRate, efroImpact, MemberStatus.ActivePast);
                var defTV = GetPresentValueOfEmbeddedFROTransferValues(t, discRate, efroImpact, MemberStatus.Deferred);
                projLiabArray.Add(t, new Dictionary<MemberStatus, double>() { 
                                        { MemberStatus.ActivePast, activeTV }, 
                                        { MemberStatus.Deferred, defTV }
                                    }
                                 );
            }

            return projLiabArray;
        }

        public double GetBestEstimateServiceCost(IBasisContext ctx, double jp2InvGrowth, DateTime at)
        {
            var lcf = new LiabilityCashflowStatefulService(ctx);

            var growthAssumptions = new BasisLiabilityAssumptions(
                new Dictionary<AssumptionType, double>
                                        {
                                            { AssumptionType.PreRetirementDiscountRate, jp2InvGrowth },
                                            { AssumptionType.PostRetirementDiscountRate, jp2InvGrowth },
                                            { AssumptionType.PensionDiscountRate, jp2InvGrowth },
                                            { AssumptionType.SalaryIncrease, ctx.Assumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease] },
                                            { AssumptionType.InflationRetailPriceIndex, ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex] },
                                            { AssumptionType.InflationConsumerPriceIndex, ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex] },
                                        },
                ctx.Data.LifeExpectancy,
                ctx.EvolutionData.LiabilityEvolution[at].ToPensionIncreasesDictionary());

            var adjustedCtx = ctx.AdjustAssumptions(growthAssumptions.LiabilityAssumptions);

            lcf.AdjustTo(at, adjustedCtx.Assumptions, true);

            var npv = lcf.GetPresentValue(at, adjustedCtx.Assumptions);

            var serviceCostEstimate = npv.Liabilities[MemberStatus.ActiveFuture];

            return serviceCostEstimate;
        }

        public void ApplyEmbeddedFROTimeZeroCETVAdjustment(JourneyPlanAfterResult.EFROImpact efroImpact)
        {
            if (efroImpact == null)
                return;

            efroImpact.AdjustedActiveTransferValuesPayable = getEmbeddedFROCETVArray(efroImpact.ActiveTransferValuesPayable, efroImpact.AnalysisDate, efroImpact.EffectiveDate);
            efroImpact.AdjustedDeferredTransferValuesPayable = getEmbeddedFROCETVArray(efroImpact.DeferredTransferValuesPayable, efroImpact.AnalysisDate, efroImpact.EffectiveDate);
        }

        private IDictionary<int, double> getEmbeddedFROCETVArray(IDictionary<int, double> transferValuesPayable, DateTime analysisDate, DateTime effectiveDate)
        {
            var startTime = (analysisDate - effectiveDate).TotalDays / 365.25;
            if (transferValuesPayable == null || startTime < 0 || startTime > transferValuesPayable.Keys.Max())
                return null;

            if (analysisDate == effectiveDate)
                return transferValuesPayable;

            var TVs = new Dictionary<int, double>();
            var startYear = 1 + (int)Math.Floor(startTime);
            var partYear = startYear - startTime;

            int i = 1;
            for (int y = startYear; y <= Utils.MaxYear; y++)
            {
                TVs[i] = partYear * transferValuesPayable[y];
                if (y < Utils.MaxYear)
                    TVs[i] = TVs[i] + ((1 - partYear) * transferValuesPayable[y + 1]);
                i++;
            }

            // fill TVs upto max year
            for (int y = TVs.Keys.Max(); y <= Utils.MaxYear; y++)
                TVs[y] = 0;

            return TVs;
        }
    }
}
