﻿namespace FlightDeck.Domain.JourneyPlan
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    class JourneyPlanRecoveryPlanService : JourneyPlanBaseService
    {
        private readonly ISchemeContext scheme;
        private DateTime at;
        private IBasisContext ctx;
        private JourneyPlanSchemeManagedService calcService;

        private ILiabilityCashflowStatefulService lcf;
        private double newRPCont;
        private double target;
        private double growthPeriod;
        private double value;

        private JourneyPlanAfterResult.FROImpact froImpact;
        private JourneyPlanAfterResult.EFROImpact efroImpact;
        private JourneyPlanAfterResult.ETVImpact etvImpact;

        protected override bool CeaseAccrual
        {
            get { return true; }
        }

        protected override DateTime? CeaseAccrualDate
        {
            get { return at; }
        }

        protected override double GetInitialAssetVal()
        {
            var assets = ctx.EvolutionData.AssetEvolution[at].AssetValue + ctx.EvolutionData.LiabilityEvolution[at].DefBuyIn + ctx.EvolutionData.LiabilityEvolution[at].PenBuyIn;
            var currBuyin = GetInitialCurrentBuyinVal();

            assets = assets - currBuyin;

            return assets;
        }

        protected override double GetInitialCurrentBuyinVal()
        {
            return ctx.EvolutionData.LiabilityEvolution[at].DefBuyIn + ctx.EvolutionData.LiabilityEvolution[at].PenBuyIn;
        }

        protected override double InsDefProp
        {
            get
            {
                return scheme.Assumptions.InsuranceOptions.NonpensionerLiabilityPercInsured;
            }
        }

        protected override double InsPenProp
        {
            get
            {
                return scheme.Assumptions.InsuranceOptions.PensionerLiabilityPercInsured;
            }
        }

        protected override double ApplyNewABF(IPortfolio pf, double marketValue)
        {
            return calcService.ApplyNewABF(at, ctx.Data, pf, marketValue);
        }

        protected override ExpectedPortfolioGrowth ApplyExpectedPortfolioGrowth(IPortfolio pf)
        {
            var opts = ctx.Assumptions.RecoveryPlanOptions;

            var startYear = 1 + (int)Math.Floor((double)opts.StartMonth / 12);

            var expectedPortfolioGrowth = new ExpectedPortfolioGrowth
                {
                    StartMonth = opts.StartMonth,
                    LumpSum = opts.LumpSumPayment,
                    Term = opts.Term,
                    Incs = opts.Increases,
                    Amount = 1,
                    Start = new DateTime(at.Year, at.Month, at.Day).AddMonths(opts.StartMonth),
                    StartYear = startYear,
                    EndYear = startYear + opts.Term
                };

            if (opts.IsGrowthFixed)
            {
                expectedPortfolioGrowth.Growth = opts.InvestmentGrowth; 
            }
            else
            {
                expectedPortfolioGrowth.DiscountAdd = opts.DiscountIncrement;
                expectedPortfolioGrowth.Growth = 0;
            }

            return expectedPortfolioGrowth;
        }

        protected override double AdjustLiabilityToDate(IPortfolio pf, double marketValue)
        {
            lcf = new LiabilityCashflowStatefulService(ctx);

            lcf.AdjustTo(at, ctx.Assumptions, true);

            pf.SetMarketValue(marketValue, at);

            if (scheme.Assumptions.FROType == FROType.Bulk)
            {
                froImpact = calcService.ApplyFRO(lcf, scheme.Assumptions.FROOptions);
                etvImpact = calcService.ApplyETV(lcf, scheme.Assumptions.ETVOptions);
                calcService.ApplyBenefitChanges(lcf, scheme.Assumptions.FutureBenefitOptions);

                var assetsToDisinvest = froImpact.AssetsDisinvested + etvImpact.AssetsDisinvested;

                pf.Disinvest(assetsToDisinvest);
            }
            else if (scheme.Assumptions.FROType == FROType.Embedded)
            {
                //Embedded FRO option at retirement (i.e. after the Analysis Date, business as usual)
                efroImpact = calcService.ApplyEmbeddedFRO(lcf, scheme.Assumptions.EFROOptions);

                calcService.ApplyEmbeddedFROTimeZeroCETVAdjustment(efroImpact);
            }

            calcService.ApplyPIE(lcf, scheme.Assumptions.PIEOptions, ctx.Assumptions);

            return pf.MarketValue;
        }

        protected override double GetTotalRecoveryPlanContribution(IPortfolio pf, int projYear, DateTime yearStart, DateTime yearEnd, ref ExpectedPortfolioGrowth expectedPortfolioGrowth)
        {
            var rp = calcService.GetTotalRecoveryPlanContribution(pf, projYear, yearStart, yearEnd, ref expectedPortfolioGrowth);

            newRPCont = rp.Item2;

            return rp.Item1;          
        }

        protected override NPV GetPresentValue(DateTime projDate)
        {
            return lcf.GetPresentValue(projDate);
        }

        protected override NPV GetBenefitOutgo(int projYear, DateTime projDate)
        {
            return lcf.GetBenefitOutgo(projYear, projDate);
        }

        protected override double AdjustNetBenefitOutgoForInsurance(double netBenefitOutgo, NPV ben, int projYear)
        {
            double efroActivePlusDeferreds = .0;
            if (efroImpact != null)
                efroActivePlusDeferreds = efroImpact.AdjustedActiveTransferValuesPayable[projYear] + efroImpact.AdjustedDeferredTransferValuesPayable[projYear];

            netBenefitOutgo = netBenefitOutgo + efroActivePlusDeferreds
                - InsPenProp * (ben.Liabilities[MemberStatus.Pensioner] - ben.Buyin[MemberStatus.Pensioner]) - InsDefProp * (ben.Liabilities[MemberStatus.Deferred] - ben.Buyin[MemberStatus.Deferred]);

            return netBenefitOutgo;
        }

        protected override double AdjustTotalBenefitOutgoForInsurance(double totBenefitOutgo, int projYear)
        {
            double efroActivePlusDeferreds = .0;
            if (efroImpact != null)
                efroActivePlusDeferreds = efroImpact.AdjustedActiveTransferValuesPayable[projYear] + efroImpact.AdjustedDeferredTransferValuesPayable[projYear];

            totBenefitOutgo = totBenefitOutgo + efroActivePlusDeferreds;

            return totBenefitOutgo;
        }

        protected override double AdjustTotalLiabilities(int projYear, double totalLiabs)
        {
            totalLiabs = totalLiabs + calcService.GetPresentValueOfEmbeddedFROTransferValues(projYear, ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate], efroImpact); //vba => PresentValueOfTVs

            return totalLiabs;
        }

        protected override double AdjustGrowthFromLiabilityChange(double growth, int projYear, double[] totalLiab, double totBenefitOutgo, ExpectedPortfolioGrowth expectedPortfolioGrowth)
        {
            if (!ctx.Assumptions.RecoveryPlanOptions.IsGrowthFixed)
            {
                growth = (totalLiab[projYear] - totalLiab[projYear - 1] + totBenefitOutgo) / (totalLiab[projYear - 1] - totBenefitOutgo / 2);
                growth += expectedPortfolioGrowth.DiscountAdd;
            }
            return growth;
        }

        protected override double AdjustAssetsForLumpSum(double assets, ExpectedPortfolioGrowth expectedPortfolioGrowth, double growth)
        {
            assets = assets + expectedPortfolioGrowth.LumpSum * (1 + growth * expectedPortfolioGrowth.PartYear);

            return assets;
        }

        protected override void CreateNewRecoveryPlan(int projYear, double[] totalAssets, double[] totalLiability, double growth, ExpectedPortfolioGrowth expectedPortfolioGrowth)
        {
            var rp = expectedPortfolioGrowth;

            if (rp.StartYear <= projYear && projYear <= rp.EndYear)
            {
                if (projYear == rp.StartYear)
                {
                    growthPeriod = rp.PartYear;
                }
                else if (projYear == rp.EndYear)
                {
                    growthPeriod = 1 - rp.PartYear;
                    target = (totalLiability[projYear] - totalAssets[projYear]) * (1 - rp.PartYear) + (totalLiability[projYear - 1] - totalAssets[projYear - 1]) * rp.PartYear;
                }
                else
                {
                    growthPeriod = 1;
                }
                value = value * (1 + growth * growthPeriod) + newRPCont * (1 + growth * growthPeriod / 2);
            }
        }

        public JourneyPlanRecoveryPlanService(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public double Calculate(IBasisContext ctx, IPortfolio pf)
        {
            this.ctx = ctx;
            this.at = ctx.AnalysisDate;
            this.calcService = new JourneyPlanSchemeManagedService(scheme, ctx.Results);

            base.Calculate(at, true, scheme.Data, ctx.Data, ctx.EvolutionData, pf);

            var amount = Math.Max(0, target / value);

            return amount;
        }
    }
}
