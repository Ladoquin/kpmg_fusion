﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain.JourneyPlan
{
    class JourneyPlanBeforeResult
    {
        public IDictionary<int, JourneyPlanItem> YearlyTotals { get; set; }
        public double PreFRODeferredLiability { get; set; }
        public double PreETVDeferredLiability { get; set; }
        public double EmploymentRate { get; set; }
        public double ServiceCostEstimate { get; set; }
        public double ContributionRateEstimate { get; set; }
        public double EstimatedBenefitReduction { get; set; }
        public double InvGrowth { get; set; }
    }
}
