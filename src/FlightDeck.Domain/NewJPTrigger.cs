﻿namespace FlightDeck.Domain
{
    public class NewJPTrigger
    {
        public double FundingLevel { get; set; }
        public double Growth { get; set; }
        public NewJPTriggerType TriggerType { get; set; }
        public int TimeInterval { get; set; }

        public NewJPTrigger(NewJPTriggerType triggerType, double growth, double fundingLevel = 0, int timeInterval = 0)
        {
            this.TriggerType = triggerType;
            this.FundingLevel = fundingLevel;
            this.TimeInterval = timeInterval;
            this.Growth = growth;
        }
    }

    public enum NewJPTriggerType
    {
        FundingLevel = 1,
        TimeBased = 2
    }
}
