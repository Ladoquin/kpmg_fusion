using System.Collections.Generic;

namespace FlightDeck.Domain
{
    public class CacheDependencies
    {
        public List<string> Indices { get; set; }
        public List<int> MasterBasisIds { get; set; }
    }
}
