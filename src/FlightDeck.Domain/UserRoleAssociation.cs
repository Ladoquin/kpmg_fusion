﻿namespace FlightDeck.Domain
{
    public class UserRoleAssociation
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
