﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain
{
    class BlackScholesCalculator
    {
        public double GetNotionalInflation(double initialValue, double inflationVolatility, double min, double max)
        {
            double currentInflation = .0, previousInflation = .0;
            double currentAssumption = .0, previousAssumption = .0;

            const int maxCount = 100;
            for (var i = 1; i <= maxCount; i++)
            {
                double nextInflation;
                if (i == 1)
                    nextInflation = initialValue;
                else if (i == 2)
                    nextInflation = currentInflation + (initialValue - currentAssumption);
                else
                    nextInflation = previousInflation +
                                    (currentInflation - previousInflation) * (initialValue - previousAssumption) /
                                    (currentAssumption - previousAssumption);
                
                previousInflation = currentInflation;
                previousAssumption = currentAssumption;
                currentInflation = nextInflation;
                currentAssumption = Utils.GetBlackScholesValue(currentInflation, inflationVolatility, 1, min, max);
                if (Math.Abs(currentAssumption - initialValue) < 0.00001)
                {
                    if (currentInflation < -0.05 || currentInflation > 0.1)
                        throw new ApplicationException("Notional inflation value is outside range");
                    return currentInflation;
                }
            }
            throw new ApplicationException(string.Format("Notional inflation can not be calculated in {0} steps", maxCount));
        }
    }
}
