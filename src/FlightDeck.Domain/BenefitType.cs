﻿namespace FlightDeck.Domain
{
    public enum BenefitType
    {
        Pension = 1,
        LumpSum = 2
    }
}