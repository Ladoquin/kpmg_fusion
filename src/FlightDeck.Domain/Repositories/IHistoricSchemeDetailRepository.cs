﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain.Repositories
{
    public interface IHistoricSchemeDetailRepository : IRepository<HistoricSchemeDetail>
    {
        IEnumerable<HistoricSchemeDetail> GetAll(int schemeId);
    }
}
