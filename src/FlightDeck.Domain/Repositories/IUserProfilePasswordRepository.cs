﻿namespace FlightDeck.Domain.Repositories
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public interface IUserProfilePasswordRepository : IRepository<PasswordData>
    {
        byte[] GetSalt(string username);
        IEnumerable<byte[]> GetHistory(string username);
        void Insert(string username, byte[] encryptedPassword, byte[] salt);
        void TrimHistory(string username, int maxCount);
    }
}
