﻿namespace FlightDeck.Domain.Repositories
{
    using FlightDeck.DomainShared;

    public interface IBasisCacheRepository
    {
        void DeleteAll();
        void DeleteAllByScheme(int schemeId);
        void DeleteAllByScheme(string schemeName);
        BasisCache GetByMasterBasisId(int masterbasisId);
    }
}
