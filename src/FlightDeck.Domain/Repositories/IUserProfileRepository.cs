﻿namespace FlightDeck.Domain.Repositories
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public interface IUserProfileRepository : IRepository<UserProfile>
    {
        UserProfile GetByUsername(string username);
        UserProfile GetByEmail(string email);
        IEnumerable<UserProfile> GetMany(IEnumerable<int> userIds);
        IEnumerable<UserProfileSearchKeys> GetSearchKeys(IEnumerable<int> excludeUserIds);
        void Delete(string username);
        void Delete(IEnumerable<int> userIds);
        void PurgeRestrictedSchemeAccess(int schemeId, IEnumerable<int> roleIds);
        void PurgeUnrestrictedSchemeAccess(int schemeId, IEnumerable<int> roleIds);
        void AddFavouriteScheme(int userId, int schemeDetailId);
        void RemoveFavouriteScheme(int userId, int schemeDetailId);
    }
}
