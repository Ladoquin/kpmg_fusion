﻿namespace FlightDeck.Domain.Repositories
{
    public interface IApplicationRepository : IRepository<ApplicationValue>
    {
        ApplicationValue GetByKey(string key);

        string GetTrackerVersionSupported();
    }
}
