﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;

namespace FlightDeck.Domain.Repositories
{
    public interface ICurveDataRepository : IRepository<CurveDataProjection>
    {
        CurveDataProjection GetByName(string name);
        CurveDataProjection GetProjectionByDate(DateTime date);
    }
}