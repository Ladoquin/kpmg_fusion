﻿namespace FlightDeck.Domain.Repositories
{
    public interface IUserProfileMembershipRepository : IRepository<UserProfileMembership>
    {
        void Unlock(int id);
    }
}
