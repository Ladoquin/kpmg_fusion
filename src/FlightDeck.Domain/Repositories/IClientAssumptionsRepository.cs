﻿namespace FlightDeck.Domain.Repositories
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public interface IClientAssumptionsRepository : IRepository<ClientAssumption>
    {
        IEnumerable<ClientAssumption> GetAll(int schemeDetailId);
        void Delete(int id);
        void DeleteAll(int schemeDetailId);
    }
}
