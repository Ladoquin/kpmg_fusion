﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain.Repositories
{
    public interface IIndexImportDetailRepository : IRepository<IndexImportDetail>
    {
        IndexImportDetail GetLatest();
    }

    public interface IIndexImportIndicesRepository: IRepository<IndexImportIndices>
    {
        void DeleteByImportId(int IndexImportId);
    }
}
