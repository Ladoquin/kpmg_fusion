﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain.Repositories
{
    public interface ICurveImportDetailRepository : IRepository<CurveImportDetail>
    {
        CurveImportDetail GetLatest();
    }
}
