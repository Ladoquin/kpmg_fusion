﻿namespace FlightDeck.Domain.Repositories
{
    using FlightDeck.DomainShared;
    
    public interface IParametersImportHistoryRepository : IRepository<ParametersImportHistory>
    {
    }
}
