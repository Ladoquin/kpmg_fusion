﻿using FlightDeck.DomainShared;
using System.Collections.Generic;

namespace FlightDeck.Domain.Repositories
{
    public interface ISchemeDetailRepository : IRepository<SchemeDetail>
    {
        SchemeDetail GetByName(string schemeName);
        IEnumerable<SchemeDetail> GetByNames(IEnumerable<string> schemeNames);
        IEnumerable<int> GetAllUnrestricted();
        IEnumerable<SchemeDetail> GetByCreator(string username);
        void Delete(SchemeDetail scheme);
        void DeleteDocument(SchemeDocument doc);
    }
}
