﻿using FlightDeck.Domain.Assets;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;

namespace FlightDeck.Domain.Repositories
{
    public interface IFinancialIndexRepository : IRepository<FinancialIndex>
    {
        FinancialIndex GetByName(string name);
        FinancialIndex GetByNameComplete(string name);
        FinancialIndex GiltsIndex { get; }
        IEnumerable<dynamic> GetIndicesByImportId(int importDetailId);
        int FillEndGap(FinancialIndex item, DateTime EndDate);
    }

    public interface I : IRepository<FinancialIndex>
    {
        FinancialIndex GetByName(string name);
        FinancialIndex GiltsIndex { get; }
    }

    public interface ILiabilityCashFlowRepository : IRepository<CashflowData>
    {
        IEnumerable<CashflowData> GetByBasisId(int basisId);
    }

    public interface IFinancialAssumptionRepository : IRepository<FinancialAssumption>
    {
        IEnumerable<FinancialAssumption> GetByBasisId(int basisId, IFinancialIndexRepository indexRepository);
    }

    public interface IPensionIncreaseAssumptionRepository : IRepository<PensionIncreaseAssumption>
    {
        IEnumerable<PensionIncreaseAssumption> GetByBasisId(int basisId, IFinancialIndexRepository indexRepository);
    }

    public interface ISchemeAssetRepository : IRepository<SchemeAsset>
    {
        IEnumerable<SchemeAsset> GetByPensionSchemeId(int pensionSchemeId, IAssetRepository assetRepository, IAssetClassRepository assetClassRepository, IFinancialIndexRepository indexRepository, IAssetFundRepository assetFundRepository);
    }

    public interface IAssetRepository : IRepository<Asset>
    {
        IEnumerable<Asset> GetBySchemeAssetId(int schemeAssetId, IAssetClassRepository assetClassRepository, IFinancialIndexRepository indexRepository, IAssetFundRepository assetFundRepository);
    }

    public interface IAssetFundRepository : IRepository<AssetFund>
    {
        IEnumerable<AssetFund> GetByAssetId(int assetId, IFinancialIndexRepository indexRepository, AssetClass assetClass);
    }

    public interface IAssetClassRepository : IRepository<AssetClass>
    {
        IEnumerable<Tuple<AssetClassType, double>> GetVolatilitiesByScheme(int schemeId);
        IEnumerable<Tuple<AssetClassType, double>> GetVolatilitiesByName(string name);
        IEnumerable<Tuple<AssetClassType, IEnumerable<Tuple<AssetClassType, double>>>> GetVarCorrelationsByScheme(int schemeId);
        IEnumerable<Tuple<AssetClassType, IEnumerable<Tuple<AssetClassType, double>>>> GetVarCorrelationsByName(string name);
        void UpdateVolatilities(string name, IDictionary<AssetClassType, double> volatilities);
        void UpdateVarCorrelations(string name, IDictionary<AssetClassType, IDictionary<AssetClassType, double>> correlations);
    }

    public interface ISalaryProgressionRepository : IRepository<SalaryProgression>
    {
        IEnumerable<SalaryProgression> GetByBasisId(int basisId);
    }

    public interface IPensionSchemeRepository : IRepository<PensionScheme>
    {
        PensionScheme GetById(int companyId, IFinancialIndexRepository indexRepository, IAssetClassRepository assetClassRepository);
        int Insert(PensionSchemeBase item);
        CacheDependencies GetCacheDependencies(int companyId);
    }
}