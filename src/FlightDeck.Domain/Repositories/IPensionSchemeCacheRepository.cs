﻿namespace FlightDeck.Domain.Repositories
{
    using DomainShared;
    using System.Collections.Generic;
    public interface IPensionSchemeCacheRepository
    {
        void DeleteAll();
        void DeleteAllByScheme(int schemeId);
        void DeleteAllByScheme(string schemeName);
        PensionSchemeCache GetBySchemeId(int pensionSchemeId);
        IEnumerable<int> GetActiveSchemesWithCache();
    }
}
