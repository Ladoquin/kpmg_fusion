﻿using System;
using System.Collections.Generic;

namespace FlightDeck.Domain
{
    public interface ICustomIndexService
    {
        Dictionary<DateTime, double> CreateCustomIndex(bool isTotalReturn, Dictionary<FinancialIndex, double> components, double rounding);
    }
}
