﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Domain
{
    public class SpotRate
    {
        public int Maturity { get; set; }
        public double Rate { get; set; }
        public DateTime Date { get; set; }

        public SpotRate(int maturity, double rate, DateTime date)
        {
            Maturity = maturity;
            Rate = rate;
            Date = date;
        }
    }
}
