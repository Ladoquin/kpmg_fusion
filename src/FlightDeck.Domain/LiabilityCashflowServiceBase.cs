﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using log4net;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    abstract class LiabilityCashflowServiceBase : ILiabilityCashflowService
    {
        protected enum RunMode
        {
            Normal,
            Stateless
        }

        protected readonly RunMode runMode;
        protected List<CashflowData> Cashflows;
        protected Basis b;
        protected readonly IDictionary<DateTime, LiabilityEvolutionItem> liabilityEvolution;

        public Basis Basis
        {
            get { return b; } 
        }

        protected List<CashflowData> AdjCashflows;
        public NPV PV01 { get; protected set; }
        public NPV IE01 { get; protected set; }

        public NPV NPV { get; protected set; }

        /// <summary>
        /// LiabilityCashflowService
        /// </summary>
        /// <param name="basisData">Basis data on effective date</param>
        /// <param name="liabilityEvolution">Temporary fix in the Tracker requires the duration from the Liability Evolution when adjusting mortality rates.</param>
        /// <param name="mode">
        /// Run mode can be set to Stateless when Adjusted Cashflows do not need to be retained, i.e., when calculating Evolution of Liabilities.
        /// Be aware, if initialised as Stateless, the caller should not rely on any public members like NPV (as they have not been protected against multi-threading).
        /// </param>
        protected LiabilityCashflowServiceBase(Basis basis, IDictionary<DateTime, LiabilityEvolutionItem> liabilityEvolution, RunMode mode)
        {
            this.b = basis;
            this.liabilityEvolution = liabilityEvolution;
            this.Cashflows = new List<CashflowData>(b.Cashflows);
            this.NPV = new Domain.NPV();

            runMode = mode;
        }

        public NPV AdjustTo(DateTime date, IBasisLiabilityAssumptions assumptionsAtDate)
        {
            return AdjustTo(date, assumptionsAtDate, false);
        }

        /// <summary>
        /// Implements the AdjustTo method in the Tracker.
        /// It creates an adjusted version of the Cashflows and calculates their NPV. 
        /// </summary>
        public NPV AdjustTo(DateTime date, IBasisLiabilityAssumptions assumptionsAtDate, bool userBasis)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var id = date.ToString("dd/MM/yy");

            var perfLog = LogManager.GetLogger("performance");

            perfLog.DebugFormat("LiabilityCashflowService.AdjustTo({0}): [1] Prepare context - {1} milliseconds", id, stopwatch.ElapsedMilliseconds.ToString());

            var adjCashflows = new List<CashflowData>(Cashflows.Count);

            var variable = assumptionsAtDate;
            var atBase = new BasisLiabilityAssumptions(b.Assumptions, b.LifeExpectancy, b.PensionIncreases);

            var mortImpact = .0;
            var adjEffectiveDate = date;
            var valTime = (date - b.EffectiveDate).TotalDays / 365.25;

            var maxEndYear = Cashflows.Max(cf => cf.EndYear);

            Func<Dictionary<int, double>> createCashflowDictionary = () =>
            {
                var dic = new Dictionary<int, double>(100);
                for (int i = 1; i <= maxEndYear; i++)
                    dic.Add(i, 0);
                return dic;
            };

            var oldCf1 = createCashflowDictionary();
            var oldCf2 = createCashflowDictionary();
            var newCf1 = createCashflowDictionary();
            var newCf2 = createCashflowDictionary();
            var totCf2 = createCashflowDictionary();

            var futCount = 0;

            var adjSal = (1 + variable.LiabilityAssumptions[AssumptionType.SalaryIncrease]) / (1 + atBase.LiabilityAssumptions[AssumptionType.SalaryIncrease]);
            var adjCPI = (1 + variable.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex]) / (1 + atBase.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex]);
            var adjRPI = (1 + variable.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex]) / (1 + atBase.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex]);

            var adjPinc =
                variable.PensionIncreases.ToDictionary(
                    x => x.Key,
                    x => (1 + x.Value) / (1 + atBase.PensionIncreases[x.Key]));

#if DEBUG
            if ((System.Configuration.ConfigurationManager.AppSettings["DebugLiabilityCashflowService"] ?? "").Equals("true", StringComparison.CurrentCultureIgnoreCase))
                System.Diagnostics.Debug.WriteLine(string.Format("AdjustTo: adjSal={0}, adjCPI={1}, adjRPI={2}, adjPinc[1]={3}, adjPinc[2]={4}, adjPinc[3]={5}, adjPinc[4]={6}, adjPinc[7]={3}, adjPinc[6]={8}", adjSal, adjCPI, adjRPI, adjPinc[1], adjPinc[2], adjPinc[3], adjPinc[4], adjPinc[5], adjPinc[6]));
#endif

            var isCpiRev = false;
            var isRpiRev = false;
            var pincEffDate = .0;
            var pincValDate = .0;

            perfLog.DebugFormat("LiabilityCashflowService.AdjustTo({0}): [2] Begin cashflow loop - {1} milliseconds", id, stopwatch.ElapsedMilliseconds.ToString());

            for (int i = 0; i < Cashflows.Count(); i++)
            {
                var cf = Cashflows[i].Clone();

                if (cf.MemberStatus == MemberStatus.ActiveFuture)
                    futCount++;

                if (cf.MemberStatus == MemberStatus.Deferred)
                {
                    isCpiRev = cf.RevaluationType == RevaluationType.None || cf.RevaluationType == RevaluationType.Cpi;
                    isRpiRev = cf.RevaluationType == RevaluationType.Rpi;
                }

                if (date >= b.EffectiveDate && cf.BeginYear > 0)
                {
                    if (cf.BenefitType == BenefitType.Pension && cf.PensionIncreaseReference != 0)
                    {
                        pincEffDate = atBase.PensionIncreases[cf.PensionIncreaseReference];
                        pincValDate = variable.PensionIncreases[cf.PensionIncreaseReference];
                    }

                    for (int y = cf.BeginYear; y <= cf.EndYear; y++)
                    {
                        if (cf.BenefitType == BenefitType.LumpSum)
                        {
                            if (date == b.EffectiveDate && !userBasis)
                            {
                            }
                            else if (cf.MemberStatus == MemberStatus.Deferred && isCpiRev)
                            {
                                cf.Cashflow[y] = cf.Cashflow[y] * Math.Pow(adjCPI, y - 0.5);
                            }
                            else if (cf.MemberStatus == MemberStatus.Deferred && isRpiRev)
                            {
                                cf.Cashflow[y] = cf.Cashflow[y] * Math.Pow(adjRPI, y - 0.5);
                            }
                            else if (cf.MemberStatus == MemberStatus.Deferred)
                            {
                            }
                            else
                            {
                                cf.Cashflow[y] = cf.Cashflow[y] * Math.Pow(adjSal, y - 0.5);
                            }
                        }
                        else if (cf.MemberStatus == MemberStatus.Pensioner || cf.MinAge >= cf.RetirementAge)
                        {
                            oldCf1[y] = cf.Cashflow[y];
                            if (date == b.EffectiveDate && !userBasis)
                            {
                                oldCf2[y] = oldCf1[y];
                            }
                            else if (y > 1 && cf.MemberStatus == MemberStatus.Pensioner)
                            {
                                mortImpact = oldCf1[y] / (oldCf1[y - 1] * (1 + pincEffDate));
                                oldCf2[y] = oldCf2[y - 1] * (1 + pincValDate) * mortImpact;
                            }
                            else
                            {
                                mortImpact = 1;
                                oldCf2[y] = oldCf1[y] * Math.Pow((1 + pincValDate) / (1 + pincEffDate), 0.5);
                            }

                            cf.Cashflow[y] = oldCf2[y];
                        }
                        else
                        {
                            if (y == cf.BeginYear)
                            {
                                newCf1[y] = cf.Cashflow[y];
                                oldCf1[y] = 0;
                            }
                            else
                            {
                                oldCf1[y] = Math.Min(Cashflows[i].Cashflow[y], Cashflows[i].Cashflow[y - 1] * (1 + pincEffDate));
                                newCf1[y] = Cashflows[i].Cashflow[y] - oldCf1[y];
                            }

                            if (y > cf.BeginYear && oldCf1[y] > 0 && newCf1[y] == 0)
                            {
                                mortImpact = Cashflows[i].Cashflow[y] / (Cashflows[i].Cashflow[y - 1] * (1 + pincEffDate));
                            }
                            else
                            {
                                mortImpact = 1;
                            }

                            if (oldCf1[y] == 0)
                                oldCf2[y] = 0;
                            else
                                oldCf2[y] = totCf2[y - 1] * (1 + pincValDate) * mortImpact;

                            if (newCf1[y] == 0)
                                newCf2[y] = 0;
                            else if (cf.MemberStatus == MemberStatus.Deferred && isCpiRev)
                                newCf2[y] = newCf1[y] * Math.Pow(adjCPI, y - 0.5);
                            else if (cf.MemberStatus == MemberStatus.Deferred && isRpiRev)
                                newCf2[y] = newCf1[y] * Math.Pow(adjRPI, y - 0.5);
                            else if (cf.MemberStatus == MemberStatus.Deferred)
                                newCf2[y] = newCf1[y];
                            else if (cf.MemberStatus == MemberStatus.ActivePast || cf.MemberStatus == MemberStatus.ActiveFuture)
                                newCf2[y] = newCf1[y] * Math.Pow(adjSal, y - 0.5);

                            totCf2[y] = oldCf2[y] + newCf2[y];

                            cf.Cashflow[y] = totCf2[y];
                        }
                    }
                }

                //Update age limits and change over NRAs to pensioners
                cf.MinAge = cf.MinAge + valTime;
                cf.MaxAge = cf.MaxAge + valTime;

                if (cf.MemberStatus != MemberStatus.Pensioner && cf.MinAge >= cf.RetirementAge)
                {
                    cf.MemberStatus = MemberStatus.Pensioner;
                }

                adjCashflows.Add(cf);
            }

            perfLog.DebugFormat("LiabilityCashflowService.AdjustTo({0}): [3] Cashflow loop complete - {1} milliseconds", id, stopwatch.ElapsedMilliseconds.ToString());

            var accrualYears = (date - b.EffectiveDate).TotalDays / 365.25;

            var cashflowsToAdd = new List<CashflowData>();

            perfLog.DebugFormat("LiabilityCashflowService.AdjustTo({0}): [4] Begin active future loop - {1} milliseconds", id, stopwatch.ElapsedMilliseconds.ToString());

            if (Cashflows.Any(cf => cf.MemberStatus == MemberStatus.ActiveFuture)
                && accrualYears > 0)
            {
                foreach (var cf in adjCashflows)
                {
                    if (cf.MemberStatus == MemberStatus.ActiveFuture && cf.BeginYear > 0)
                    {
                        var newCf = cf.Clone();
                        newCf.MemberStatus = MemberStatus.ActivePast;

                        for (int y = newCf.BeginYear; y <= newCf.EndYear; y++)
                        {
                            newCf.Cashflow[y] = accrualYears * newCf.Cashflow[y];
                        }

                        if (!newCf.Cashflow.All(x => x.Value == 0))
                        {
                            cashflowsToAdd.Add(newCf);
                        }
                    }
                }

                adjCashflows.AddRange(cashflowsToAdd);
            }

            perfLog.DebugFormat("LiabilityCashflowService.AdjustTo({0}): [5] Get present value - {1} milliseconds", id, stopwatch.ElapsedMilliseconds.ToString());

            var tempNPV = GetPresentValue(adjCashflows, date, variable);

            perfLog.DebugFormat("LiabilityCashflowService.AdjustTo({0}): [6] Adjust mortality - {1} milliseconds", id, stopwatch.ElapsedMilliseconds.ToString());

            if (userBasis && variable.LifeExpectancy != null && !b.LifeExpectancy.Equals(variable.LifeExpectancy))
            {
                AdjustMortality(ref adjCashflows, variable.LifeExpectancy, b.MortalityAdjustments, tempNPV.Duration, date, b.EffectiveDate);
                tempNPV = GetPresentValue(adjCashflows, date, variable);

                perfLog.DebugFormat("LiabilityCashflowService.AdjustTo({0}): [7.b] Adjust mortality complete - {1} milliseconds", id, stopwatch.ElapsedMilliseconds.ToString());
            }

            tempNPV.Total =
                tempNPV.Liabilities.Any(x => x.Key != MemberStatus.ActiveFuture)
                ? tempNPV.Liabilities.Where(x => x.Key != MemberStatus.ActiveFuture).Sum(x => x.Value)
                : 0;

            if (runMode == RunMode.Normal)
            {
                AdjCashflows = adjCashflows;
                NPV = tempNPV;
            }

            stopwatch.Stop();

            perfLog.DebugFormat("LiabilityCashflowService.AdjustTo(): {0} milliseconds", stopwatch.ElapsedMilliseconds.ToString());

            return tempNPV;
        }

        /// <summary>
        /// Calculate Present Value.
        /// </summary>
        /// <remarks>
        /// This currently calculates the Duration in the process, meaning this is doing two things, but 
        /// this is how Tracker currently works out Duration.
        /// </remarks>
        public NPV GetPresentValue(IEnumerable<CashflowData> cashflows, DateTime date, IBasisLiabilityAssumptions assumptions)
        {
            var val = new NPV();
            var valTime = (date - b.EffectiveDate).TotalDays / 365.25;
            var yAdd = (int)valTime;

            var discPre = 1.0 + assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate];
            var discPost = 1.0 + assumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate];
            var discPen = 1.0 + assumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate];
            var penIncCount = 6;
            var esc = new double[penIncCount + 1]; //will have an leading [0] item which isn't required, but easier to leave then messing about with adjusting indices
            for (int pincRef = 1; pincRef <= penIncCount; pincRef++)
            {
                esc[pincRef] = assumptions.PensionIncreases.ContainsKey(pincRef) ? 1.0 + assumptions.PensionIncreases[pincRef] : .0;
            }

#if DEBUG
            if ((System.Configuration.ConfigurationManager.AppSettings["DebugLiabilityCashflowService"] ?? "").Equals("true", StringComparison.CurrentCultureIgnoreCase))
                System.Diagnostics.Debug.WriteLine(string.Format("GetPresentValue: discPre={0}, discPost={1}, discPen={2}, esc[1]={3}, esc[2]={4}, esc[3]={5}, esc[4]={6}, esc[5]={7}, esc[6]={8}", discPre, discPost, discPen, esc[1], esc[2], esc[3], esc[4], esc[5], esc[6]));
#endif

            double dcf;
            double partYear;
            double discTerm;
            double oldPart;
            double newPart;
            double discFac;
            double discFacPrev;
            int yMax = 100;

            var cfs = cashflows.ToList();

            for (int i = 0; i < cfs.Count; i++)
            {
                var cf = cfs[i];
                discFac = 1.0;
                discFacPrev = .0;
                partYear = .0;
                discTerm = .0;
                oldPart = .0;
                newPart = .0;

                for (int y = cf.BeginYear; y <= cf.EndYear; y++)
                {
                    partYear = .0;
                    dcf = .0;
                    discFacPrev = discFac;
                    discFac = 1.0;
                    var prevCf = y == cf.BeginYear ? .0 : cf.Cashflow[y - 1];

                    if (y <= yAdd)
                    {
                        partYear = .0;
                        discTerm = .0;
                    }
                    else if (y == yAdd + 1)
                    {
                        partYear = 1.0 - (valTime - yAdd);
                        discTerm = y - valTime - partYear / 2.0;
                    }
                    else
                    {
                        partYear = 1.0;
                        discTerm = y - valTime - 0.5;
                    }

                    if (cf.Cashflow[y] == .0 || y <= yAdd || y > yMax)
                    {
                    }
                    else if (cf.MemberStatus == MemberStatus.Pensioner || cf.MinAge >= cf.RetirementAge)
                    {
                        discFac = Math.Pow(discPen, -discTerm);
                        dcf = partYear * cf.Cashflow[y] * discFac;
                    }
                    else if (y == 1 || cf.BenefitType == BenefitType.LumpSum)
                    {
                        discFac = Math.Pow(discPre, -discTerm);
                        dcf = partYear * cf.Cashflow[y] * discFac;
                    }
                    else
                    {
                        oldPart = Math.Min(prevCf * esc[cf.PensionIncreaseReference], cf.Cashflow[y]);
                        newPart = cf.Cashflow[y] - oldPart;

                        if (oldPart > .0)
                        {
                            if (y == yAdd + 1)
                            {
                                oldPart = oldPart / Math.Pow(discPost, discTerm);
                            }
                            else
                            {
                                oldPart = oldPart * discFacPrev / discPost;
                            }
                        }
                        if (newPart > .0)
                        {
                            newPart = newPart / Math.Pow(discPre, discTerm);
                        }

                        discFac = (oldPart + newPart) / cf.Cashflow[y];
                        dcf = partYear * (oldPart + newPart);
                    }

                    if (cf.IsBuyIn)
                    {
                        val.Buyin[cf.MemberStatus] += dcf;
                    }
                    else
                    {
                        val.Liabilities[cf.MemberStatus] += dcf;
                        val.Duration[cf.MemberStatus] += dcf * (y - 0.5);
                    }
                }
            };

            foreach (var status in val.Duration.Keys.ToList())
                val.Duration[status] /= (val.Liabilities[status] > 0) ? val.Liabilities[status] : 1;

            val.Total = val.Liabilities[MemberStatus.ActivePast] + val.Liabilities[MemberStatus.Deferred] + val.Liabilities[MemberStatus.Pensioner];

            return val;
        }

        public NPV GetBenefitOutgo(int yearNo)
        {
            return GetBenefitOutgo(yearNo, Cashflows, null);
        }

        public NPV GetBenefitOutgo(int yearNo, IEnumerable<CashflowData> cashflows)
        {
            return GetBenefitOutgo(yearNo, cashflows, null);
        }

        public NPV GetBenefitOutgo(int yearNo, IEnumerable<CashflowData> cashflows, double? YearEndToEffetiveDateDaySpan = null)
        {
            var totalOutgo = new NPV();

            if (YearEndToEffetiveDateDaySpan.HasValue) // Using this as equivalent of UseAdjCashflows in Tracker
            {
                var endTime = YearEndToEffetiveDateDaySpan.Value / 365.25;
                var startTime = endTime - 1;
                var y1 = Math.Max(1, 1 + (int)Math.Floor(startTime));
                var p1 = y1 - startTime;
                foreach (var cf in AdjCashflows)
                {
                    var cfy1 = cf.Cashflow.ContainsKey(y1) ? cf.Cashflow[y1] : 0;
                    var cfy_plus1 = cf.Cashflow.ContainsKey(y1 + 1) ? cf.Cashflow[y1 + 1] : 0;
                    var outgo = p1 * cfy1;
                    if (y1 < cf.Cashflow.Keys.Max() && p1 < 1)
                    {
                        outgo = outgo + (1 - p1) * cfy_plus1;
                    }
                    if (!cf.IsBuyIn)
                    {
                        totalOutgo.Liabilities[cf.MemberStatus] += outgo;
                    }
                    else
                    {
                        totalOutgo.Buyin[cf.MemberStatus] += outgo;
                    }
                }
            }
            else
            {
                foreach (var cf in cashflows)
                {
                    if (cf.BeginYear <= yearNo && yearNo <= cf.EndYear)
                    {
                        if (cf.IsBuyIn)
                        {
                            totalOutgo.Buyin[cf.MemberStatus] += cf.Cashflow[yearNo];
                        }
                        else
                        {
                            totalOutgo.Liabilities[cf.MemberStatus] += cf.Cashflow[yearNo];
                        }
                    }
                }
            }

            return totalOutgo;
        }

        public IEnumerable<CashflowData> GetCashflowOutput(DateTime at)
        {
            if (at < b.EffectiveDate)
                return null;

            var cashflowOutput = new List<CashflowData>(AdjCashflows.Count);

            var yMax = 100;
            var valTime = (at - b.EffectiveDate).TotalDays / Utils.DaysInAYear;
            var addYears = (int)Math.Floor(valTime);
            var partYear = valTime - addYears;

            foreach (var adjCf in AdjCashflows)
            {
                var outputCf = new Dictionary<int, double>(100);
                for (var y = 1; y <= yMax - addYears; y++)
                {
                    var v = (1 - partYear) * (adjCf.Cashflow.ContainsKey(y + addYears) ? adjCf.Cashflow[y + addYears] : 0);
                    if (y < yMax - addYears)
                        v = v + partYear * (adjCf.Cashflow.ContainsKey(y + addYears + 1) ? adjCf.Cashflow[y + addYears + 1] : 0);
                    outputCf.Add(y, v);
                }

                var c = adjCf.Clone(outputCf, true);

                c.MinAge = Math.Round(c.MinAge);
                c.MaxAge = Math.Round(c.MaxAge);

                cashflowOutput.Add(c);
            }

            return cashflowOutput;
        }

        private void AdjustMortality(ref List<CashflowData> cashflows, LifeExpectancyAssumptions lifeExpectancyAssumptions, IDictionary<int, double> mortalityAdjustments, IDictionary<MemberStatus, double> calculatedDuration, DateTime date, DateTime effectiveDate)
        {
            var mortalityAdjuster = new MortalityAdjustmentService(mortalityAdjustments);

            var duration = new Dictionary<MemberStatus, double>
                {
                    { MemberStatus.ActivePast, calculatedDuration[MemberStatus.ActivePast] != 0 ? calculatedDuration[MemberStatus.ActivePast] : 20 },
                    { MemberStatus.Deferred, calculatedDuration[MemberStatus.Deferred] != 0 ? calculatedDuration[MemberStatus.Deferred] : 20 },
                    { MemberStatus.Pensioner, calculatedDuration[MemberStatus.Pensioner] != 0 ? calculatedDuration[MemberStatus.Pensioner] : 12 },
                    { MemberStatus.ActiveFuture, calculatedDuration[MemberStatus.ActiveFuture] != 0 ? calculatedDuration[MemberStatus.ActiveFuture] : 20 }
                };

            var deltas = new Dictionary<bool, double>
                {
                    { true, lifeExpectancyAssumptions.PensionerLifeExpectancy - b.LifeExpectancy.PensionerLifeExpectancy },
                    { false, lifeExpectancyAssumptions.DeferredLifeExpectancy - b.LifeExpectancy.DeferredLifeExpectancy }
                };
            
            foreach (var cashflow in cashflows)
            {
                var delta = deltas[cashflow.MemberStatus == MemberStatus.Pensioner];
                var liabAdj = 1 + mortalityAdjuster.GetMortalityAdjustment(Convert.ToInt32((cashflow.MinAge + cashflow.MaxAge) / 2)) * delta;

                for (int y = cashflow.BeginYear; y <= cashflow.EndYear; y++)
                {
                    cashflow.Cashflow[y] = cashflow.Cashflow[y] * Math.Pow(liabAdj, (y - 0.5) / duration[cashflow.MemberStatus]);
                }
            }
        }

        public void ReplaceOriginalCashflows()
        {
            if (AdjCashflows.Any())
                Cashflows = AdjCashflows;
        }

        public void CalculatePV01AndIE01(DateTime valDate, IBasisLiabilityAssumptions assumptions, bool userBasis = true)
        {
            //Copy the required assumptions to the user's basis
            if (!userBasis)
            {
                var underlyingPincs = b.UnderlyingPensionIncreaseIndices;
                var userPincs = underlyingPincs.Keys.ToDictionary(x => x, x => underlyingPincs[x].GetValue(valDate, valDate));
                this.b = Basis.Adjust(null, userPincs);
            }

            var liabAssumptions = assumptions.Clone();
            var pincAssumptions = new PensionIncreasesTrackerService().CalculateVariablePensionIncreases(valDate, valDate, b.UnderlyingPensionIncreaseIndices, liabAssumptions.PensionIncreases, liabAssumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex], liabAssumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex]);
            liabAssumptions = new BasisLiabilityAssumptions(liabAssumptions.LiabilityAssumptions, liabAssumptions.LifeExpectancy, pincAssumptions);

            AdjustTo(valDate, liabAssumptions, true);
            var pv0 = GetPresentValue(AdjCashflows, valDate, liabAssumptions);

            liabAssumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] = liabAssumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] - 0.0001;
            liabAssumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate] = liabAssumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate] - 0.0001;
            liabAssumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate] = liabAssumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate] - 0.0001;

            PV01 = GetPresentValue(AdjCashflows, valDate, liabAssumptions);

            liabAssumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] = liabAssumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] + 0.0001;
            liabAssumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate] = liabAssumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate] + 0.0001;
            liabAssumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate] = liabAssumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate] + 0.0001;
            liabAssumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease] = liabAssumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease] + 0.0001;
            liabAssumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex] = liabAssumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex] + 0.0001;
            liabAssumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex] = liabAssumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex] + 0.0001;
            // because we have changed InflationRPI and InflationCPI, we need to update variable PINCS as well
            pincAssumptions = new PensionIncreasesTrackerService().CalculateVariablePensionIncreases(valDate, valDate, b.UnderlyingPensionIncreaseIndices, liabAssumptions.PensionIncreases, liabAssumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex], liabAssumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex]);

            liabAssumptions = new BasisLiabilityAssumptions(liabAssumptions.LiabilityAssumptions, liabAssumptions.LifeExpectancy, pincAssumptions);

            AdjustTo(valDate, liabAssumptions, true);

            IE01 = GetPresentValue(AdjCashflows, valDate, liabAssumptions);

            PV01.Liabilities[MemberStatus.ActivePast] -= pv0.Liabilities[MemberStatus.ActivePast];
            PV01.Liabilities[MemberStatus.Deferred] -= pv0.Liabilities[MemberStatus.Deferred];
            PV01.Liabilities[MemberStatus.Pensioner] -= pv0.Liabilities[MemberStatus.Pensioner];
            PV01.Liabilities[MemberStatus.ActiveFuture] -= pv0.Liabilities[MemberStatus.ActiveFuture];

            PV01.Buyin[MemberStatus.ActivePast] -= pv0.Buyin[MemberStatus.ActivePast];
            PV01.Buyin[MemberStatus.Deferred] -= pv0.Buyin[MemberStatus.Deferred];
            PV01.Buyin[MemberStatus.Pensioner] -= pv0.Buyin[MemberStatus.Pensioner];
            PV01.Buyin[MemberStatus.ActiveFuture] -= pv0.Buyin[MemberStatus.ActiveFuture];

            IE01.Liabilities[MemberStatus.ActivePast] -= pv0.Liabilities[MemberStatus.ActivePast];
            IE01.Liabilities[MemberStatus.Deferred] -= pv0.Liabilities[MemberStatus.Deferred];
            IE01.Liabilities[MemberStatus.Pensioner] -= pv0.Liabilities[MemberStatus.Pensioner];
            IE01.Liabilities[MemberStatus.ActiveFuture] -= pv0.Liabilities[MemberStatus.ActiveFuture];

            IE01.Buyin[MemberStatus.ActivePast] -= pv0.Buyin[MemberStatus.ActivePast];
            IE01.Buyin[MemberStatus.Deferred] -= pv0.Buyin[MemberStatus.Deferred];
            IE01.Buyin[MemberStatus.Pensioner] -= pv0.Buyin[MemberStatus.Pensioner];
            IE01.Buyin[MemberStatus.ActiveFuture] -= pv0.Buyin[MemberStatus.ActiveFuture];
        }

        public double GetPV01(bool isBuyin)
        {
            if (isBuyin)
                return PV01.Buyin[MemberStatus.ActivePast] + PV01.Buyin[MemberStatus.Deferred] + PV01.Buyin[MemberStatus.Pensioner];
            else
                return PV01.Liabilities[MemberStatus.ActivePast] + PV01.Liabilities[MemberStatus.Deferred] + PV01.Liabilities[MemberStatus.Pensioner];
        }

        public double GetIE01(bool isBuyin)
        {
            if (isBuyin)
                return IE01.Buyin[MemberStatus.ActivePast] + IE01.Buyin[MemberStatus.Deferred] + IE01.Buyin[MemberStatus.Pensioner];
            else
                return IE01.Liabilities[MemberStatus.ActivePast] + IE01.Liabilities[MemberStatus.Deferred] + IE01.Liabilities[MemberStatus.Pensioner];
        }

        public int GetYearNumber(DateTime atDate)
        {
            int i = 0;
            for (i = 1; i <= Utils.MaxYear; i++)
            {
                if (b.EffectiveDate.AddYears(i) > atDate)
                    break;
            }
            return i;
        }

        public void GroupOver50sCashFlows()
        {
            List<CashflowData> groupedCashflows = new List<CashflowData>();

            Func<CashflowData, CashflowData> matchingGroupedData = cashflow =>
            {
                return groupedCashflows.FirstOrDefault(x =>
                        x.MemberStatus == cashflow.MemberStatus &&
                        x.MinAge == cashflow.MinAge &&
                        x.MaxAge == cashflow.MaxAge &&
                        x.RetirementAge == cashflow.RetirementAge &&
                        x.BenefitType == cashflow.BenefitType &&
                        x.PensionIncreaseReference == cashflow.PensionIncreaseReference &&
                        x.RevaluationType == cashflow.RevaluationType &&
                        x.IsBuyIn == cashflow.IsBuyIn
                    );
            };

            foreach (var cf in Cashflows.Where(cf => cf.RevaluationType == RevaluationType.None))
            {
                cf.RevaluationType = RevaluationType.Cpi;
            }

            var bandMin = 0;
            var bandMax = 0;
            foreach (var cf in Cashflows)
            {
                if (cf.BeginYear > 0)
                {
                    bandMin = 5 * (int)Math.Floor((double)(cf.MinAge / 5));
                    bandMax = bandMin + 5;
                    if (cf.MaxAge <= bandMax)
                    {
                        cf.MinAge = bandMin;
                        cf.MaxAge = bandMax;
                    }

                    var groupedCF = matchingGroupedData(cf);
                    if (groupedCF != null)
                    {
                        for (int y = cf.BeginYear; y <= cf.EndYear; y++)
                        {
                            if (groupedCF.Cashflow.ContainsKey(y))
                                groupedCF.Cashflow[y] += cf.Cashflow[y];
                            else
                            {
                                groupedCF.Cashflow.Add(y, cf.Cashflow[y]);
                                groupedCF.CalibrateBeginAndEndYear();
                            }
                        }
                    }
                    else
                        groupedCashflows.Add(cf);
                }
            }
            Cashflows = groupedCashflows;
        }

        public double GetLiabDuration()
        {
            var num = .0;
            var den = .0;

            if (NPV != null)
            {
                num += NPV.Duration[MemberStatus.ActivePast] * NPV.Liabilities[MemberStatus.ActivePast];
                num += NPV.Duration[MemberStatus.Deferred] * NPV.Liabilities[MemberStatus.Deferred];
                num += NPV.Duration[MemberStatus.Pensioner] * NPV.Liabilities[MemberStatus.Pensioner];
                den = NPV.Liabilities[MemberStatus.ActivePast] + NPV.Liabilities[MemberStatus.Deferred] + NPV.Liabilities[MemberStatus.Pensioner];
            }

            if (num != .0)
                return num / den;
            else
                return .0;
        }

    }
}
