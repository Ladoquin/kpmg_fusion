﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.LDI;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public class PensionSchemeBase
    {
        public int Id { get; protected set; }
        public string Name { get; protected set; }
        public string Reference { get; protected set; }
        public IEnumerable<Basis> UnderlyingBases { get; protected set; }
        public IEnumerable<SchemeAsset> SchemeAssets { get; protected set; }
        public IList<Contribution> ContributionRates { get; protected set; }
        public IEnumerable<RecoveryPayment> RecoveryPlanRaw { get; protected set; }
        public SchemeDataSource DataSource { get; protected set; }
        public IDictionary<AssetClassType, double> Volatilities { get; protected set; }
        public IDictionary<AssetClassType, double> DefaultVolatilityAssumptions { get; protected set; }        
        public IDictionary<AssetClassType, IDictionary<AssetClassType, double>> VarCorrelations { get; protected set; }
        public IDictionary<NamedPropertyGroupType, NamedPropertyGroup> NamedProperties { get; protected set; }   
        public double ReturnOnAssets { get; protected set; }
        public bool IsGrowthFixed { get; protected set; }
        public double Ias19ExpenseLoading { get; protected set; }
        public AccountingStandardType AccountingDefaultStandard { get; protected set; }
        public bool AccountingIAS19Visible { get; protected set; } //TODO should these be refactored in to AnalysisParameters? 
        public bool AccountingFRS17Visible { get; protected set; }
        public bool AccountingUSGAAPVisible { get; protected set; }
        public SalaryType? ABOSalaryType { get; protected set; }
        public PensionSchemeApplicationParameters ApplicationParameters { get; protected set; }
        public AnalysisParametersNullable AnalysisParameters { get; protected set; }
        public double ContractingOutContributionRate { get; protected set; }
        public LDICashflowBasis LDICashflowBasis { get; protected set; }
        public SyntheticAssetIndices SyntheticAssets { get; protected set; }
        public bool Funded { get; protected set; }
        public readonly string DefaultBasis;

        public PensionSchemeBase(int id, string name, string reference, IEnumerable<Basis> bases, IEnumerable<SchemeAsset> assets,
            IList<Contribution> contributionRates, IEnumerable<RecoveryPayment> recoveryPlan, SchemeDataSource dataSource,
            IDictionary<AssetClassType, double> volatilities, IDictionary<AssetClassType, double> defaultVolatilityAssumptions, IDictionary<AssetClassType, IDictionary<AssetClassType, double>> varCorrelations,
            IDictionary<NamedPropertyGroupType, NamedPropertyGroup> namedProperties,
            double returnOnAssets, bool isGrowthFixed, double ias19ExpenseLoading,
            AccountingStandardType accountingDefaultStandard, bool accountingIAS19Visible, bool accountingFRS17Visible, bool accountingUSGAAPVisible,
            SalaryType? aboSalaryType, PensionSchemeApplicationParameters applicationParameters, AnalysisParametersNullable analysisParameters,
            double contractingOutContributionRate, LDICashflowBasis ldiCashflowBasis, SyntheticAssetIndices syntheticAssets, bool funded,
            string defaultBasis)
        {
            UnderlyingBases = bases;
            AccountingDefaultStandard = accountingDefaultStandard;
            AccountingIAS19Visible = accountingIAS19Visible;
            AccountingFRS17Visible = accountingFRS17Visible;
            AccountingUSGAAPVisible = accountingUSGAAPVisible;
            ABOSalaryType = aboSalaryType;
            Ias19ExpenseLoading = ias19ExpenseLoading;
            IsGrowthFixed = isGrowthFixed;
            ReturnOnAssets = returnOnAssets;
            NamedProperties = namedProperties;
            Volatilities = volatilities;
            VarCorrelations = varCorrelations;
            DefaultVolatilityAssumptions = defaultVolatilityAssumptions;
            ApplicationParameters = applicationParameters;
            AnalysisParameters = analysisParameters;
            Reference = reference;
            Name = name;
            DataSource = dataSource;
            Id = id;
            DefaultBasis = defaultBasis;
            SchemeAssets = assets;
            ContributionRates = contributionRates;
            RecoveryPlanRaw = recoveryPlan;
            ContractingOutContributionRate = contractingOutContributionRate;
            LDICashflowBasis = ldiCashflowBasis;
            SyntheticAssets = syntheticAssets;
            Funded = funded;
        }
    }
}
