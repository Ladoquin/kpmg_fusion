﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.JourneyPlan;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    interface ILiabilityCashflowStatefulService : ILiabilityCashflowService
    {
        NPV GetPresentValue(DateTime date);
        NPV GetPresentValue(DateTime date, IBasisLiabilityAssumptions assumptions);
        NPV GetBenefitOutgo(int yearNo, DateTime yearEnd);
        double GetPreFRODeferredLiability();
        double GetPreETVDeferredLiability();
        double ApplyFRO(FlexibleReturnOptions args);
        JourneyPlanAfterResult.EFROImpact ApplyEmbeddedFRO(EmbeddedFlexibleReturnOptions args);
        double ApplyETV(EnhancedTransferValueOptions args);
        PIECashflowImpact ApplyPIE(PieOptions args, PIEInitData init, IDictionary<int, double> clientPensionIncreases);
        JourneyPlanAfterResult.BenefitChangesImpact ApplyBenefitChanges(FutureBenefitsParameters args);
        void ConvertToLDI(ISchemeData schemeData, DateTime atDate, double discRate, double inflationRate);
        void SetPresentValue(DateTime date);
    }
}
