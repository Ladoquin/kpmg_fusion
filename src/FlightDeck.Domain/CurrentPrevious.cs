﻿namespace FlightDeck.Domain
{
    public struct CurrentPrevious<T> where T:struct
    {
        public T Current { get; set; }
        public T Previous { get; set; }

        public CurrentPrevious(T current, T previous) : this()
        {
            Current = current;
            Previous = previous;
        }

        public void SetPrevious()
        {
            Previous = Current;
        }
    }
}