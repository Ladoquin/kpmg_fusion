﻿namespace FlightDeck.Domain
{
    public enum AssumptionChangeType
    {
        LiabilityAssumption,
        AssetAssumption,
        PensionIncreasesAssumptions,
        RecoveryPlan,
        InvestmentStrategyAllocations,
        InvestmentStrategyOptions,
        FROType,
        FRO,
        EFRO,
        ETV,
        PIE,
        FutureBenefits,
        InsuredLiabilityPercentage,
        ABF,
        Waterfall
    }
}
