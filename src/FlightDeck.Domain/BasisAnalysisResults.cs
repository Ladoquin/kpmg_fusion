﻿using FlightDeck.Domain.VaR;
using FlightDeck.DomainShared;
using FlightDeck.DomainShared.VaR;
using System;
using System.Collections.Generic;

namespace FlightDeck.Domain
{
    public class BasisAnalysisResultsStickyData
    {        
        public double PreFRODeferredLiability { get; set; }
        /// <summary>
        /// Currently only calculated once when first hit a buyout basis and ignores any analysis changes
        /// </summary>
        public IDictionary<int, IDictionary<StrainType, double>> Strains { get; set; }
    }

    public class BasisAnalysisResults : IBasisAnalysisResults
    {
        public BasisAnalysisResultsStickyData StickyResults { get; set; }

        public double UserActLiab { get; set; }
        public double UserDefLiab { get; set; }
        public double UserPenLiab { get; set; }
        public double UserServiceCost { get; set; }
        public double UserActDuration { get; set; }
        public double UserDefDuration { get; set; }
        public double UserPenDuration { get; set; }
        public double UserDefBuyIn { get; set; }
        public double UserPenBuyIn { get; set; }

        public double UserActLiabAfter { get; set; }
        public double UserDefLiabAfter { get; set; }
        public double UserPenLiabAfter { get; set; }

        public double AssetsAtSED { get; set; }
        public double BuyInAtAnalysisDate { get; set; }

        public double JPInvGrowth { get; set; }
        public double JP2InvGrowth { get; set; }

        public IDictionary<int, BalanceData> JourneyPlanBefore { get; set; }
        public IDictionary<int, BalanceData> JourneyPlanAfter { get; set; }
        public IEnumerable<CashflowData> AdjustedCashflow { get; set; }
        public IDictionary<int, IDictionary<MemberStatus, double>> Cashflow { get; set; }
        public IDictionary<int, IDictionary<MemberStatus, double>> BuyinCashflow { get; set; }
        public IDictionary<int, BeforeAfter<double>> RecoveryPlanPayments { get; set; }
        public double RecoveryPlanPaymentRequired { get; set; }        
        public InvestmentStrategyData InvestmentStrategy { get; set; }
        public HedgeData Hedging { get; set; }
        
        private FroData _fro = null;
        public FroData FRO {
            get
            {
                if (_fro == null)
                    _fro = new FroData(0, 0, 0, 0, 0); //no fro data? just return an empty object. //todo: FROData.Empty()? static method?

                return _fro;
            }
            set
            {
                _fro = value;
            }
        }

        private EFroData _eFro = null;
        public EFroData EFRO
        {
            get
            {
                if (_eFro == null)
                    _eFro = new EFroData(new Dictionary<int, double>(), new Dictionary<int, double>(), 0, 0, 0, 0, 0); //no efro data? just return an empty object. //todo: EFROData.Empty()? static method?

                return _eFro;
            }
            set
            {
                _eFro = value;
            }
        }

        private EtvData _etv = null;
        public EtvData ETV {
            get
            {
                if (_etv == null)
                    _etv = new EtvData(0, 0, 0, 0, 0, 0, 0, 0, 0);

                return _etv;
            }
            set
            {
                _etv = value;
            }
        }
        public PieData PIE { get; set; }

        private FutureBenefitsData _futureBenefits = null;
        public FutureBenefitsData FutureBenefits {
            get
            {
                if (_futureBenefits == null)
                    _futureBenefits = new FutureBenefitsData(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

                return _futureBenefits;
            }
            set
            {
                _futureBenefits = value;
            }
        }
        public PIEInitData PIEInit { get; set; }
        public AbfData ABF { get; set; }
        public BuyinData Insurance { get; set; }
        public double InsurancePointChangeImpact { get; set; }
        public FundingData FundingLevel { get; set; }
        public BalanceData BalanceSheet { get; set; }
        public ProfitLossData ProfitLoss { get; set; }
        public AttributionData Attribution { get; set; }
        public SurplusAnalysisData SurplusAnalysis { get; set; }
        public WaterfallData VarWaterfall { get; set; }
        public IList<VarFunnelData> VarFunnel { get; set; }
        public WeightedReturnParameters WeightedReturns { get; set; }
        public IAS19Disclosure.IAS19PAndLForecastData IAS19 { get; set; }
        public IAS19Disclosure.IAS19PAndLForecastData FRS17 { get; set; }
        public GiltBasisLiabilityData GiltBasisLiabilityData { get; set; }        
        public VaRInputs VaRInputs { get; set; }
    }
}
