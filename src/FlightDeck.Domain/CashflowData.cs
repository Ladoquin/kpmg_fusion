﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class CashflowData : IEquatable<CashflowData>
    {
        public int Id { get; private set; }
        public int BasisId { get; set; }
        public MemberStatus MemberStatus { get; set; }
        public double MinAge { get; set; }
        public double MaxAge { get; set; }
        public RevaluationType RevaluationType { get; set; }
        public int RetirementAge { get; private set; }
        public BenefitType BenefitType { get; private set; }
        public Dictionary<int, double> Cashflow { get; protected set; }
        public int BeginYear { get; private set; }
        public int EndYear { get; private set; }
        public bool IsBuyIn { get; private set; }
        public int PensionIncreaseReference { get; set; }

        public CashflowData(
            int id,
            int basisId,
            MemberStatus memberStatus,
            double minAge,
            double maxAge,
            RevaluationType revaluationType,
            int retirementAge,
            BenefitType benefitType,
            Dictionary<int, double> cashflow,
            int beginYear,
            int endYear,
            bool isBuyin, 
            int? pensionIncreaseReference)
        {
            Id = id;
            BasisId = basisId;
            MemberStatus = memberStatus;
            MinAge = minAge;
            MaxAge = maxAge;
            RevaluationType = revaluationType;
            RetirementAge = retirementAge;
            BenefitType = benefitType;
            Cashflow = cashflow;
            BeginYear = beginYear;
            EndYear = endYear;
            IsBuyIn = isBuyin;
            PensionIncreaseReference = pensionIncreaseReference.GetValueOrDefault(0);
        }

        public CashflowData Clone()
        {
            return new CashflowData(Id, BasisId, MemberStatus, MinAge, MaxAge, RevaluationType, RetirementAge, BenefitType, new Dictionary<int, double>(Cashflow), BeginYear, EndYear, IsBuyIn, PensionIncreaseReference);
        }
        public CashflowData Clone(IDictionary<int, double> cashflow, bool recalibrateBeginAndEndYear = false)
        {
            var beginYear = BeginYear;
            var endYear = EndYear;
            if (recalibrateBeginAndEndYear)
            {
                beginYear = cashflow.Any(cf => cf.Value != 0) ? cashflow.First(cf => cf.Value != 0).Key : 0;
                endYear = beginYear == 0 ? 0 : cashflow.Last(cf => cf.Value != 0).Key;
            }
            return new CashflowData(Id, BasisId, MemberStatus, MinAge, MaxAge, RevaluationType, RetirementAge, BenefitType, new Dictionary<int, double>(cashflow), beginYear, endYear, IsBuyIn, PensionIncreaseReference);
        }

        public void CalibrateBeginAndEndYear()
        {
            BeginYear = Cashflow.Any(cf => cf.Value != 0) ? Cashflow.Where(cf => cf.Value != 0).OrderBy(x => x.Key).First().Key : 0;
            EndYear = BeginYear == 0 ? 0 : Cashflow.Where(cf => cf.Value != 0).OrderByDescending(x => x.Key).First().Key;
            if (Cashflow.Count() <= (EndYear - BeginYear))
            {
                // there must be 0 value for one of the cashflows, so need to add
                for(int i = BeginYear; i <= EndYear; i++)
                {
                    if (!Cashflow.ContainsKey(i))
                        Cashflow.Add(i, .0);
                }
            }
        }

        public bool Equals(CashflowData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            var equal =
                //Id == other.Id
                BasisId == other.BasisId
                && MemberStatus == other.MemberStatus
                && Math.Abs(MinAge - other.MinAge) < Utils.TestPrecision
                && Math.Abs(MaxAge - other.MaxAge) < Utils.TestPrecision
                && RevaluationType == other.RevaluationType
                && RetirementAge == other.RetirementAge
                && BenefitType == other.BenefitType
                && IsBuyIn == other.IsBuyIn
                && PensionIncreaseReference == other.PensionIncreaseReference;

            if (equal)
            {
                //begin year may indicate the first year used, or the first non-zero year used, so have to check both cases
                if (BeginYear != other.BeginYear)
                {
                    var lowerBeginYearGroup = BeginYear < other.BeginYear ? this : other;
                    var higherBeginYearGroup = BeginYear > other.BeginYear ? this : other;
                    for (var y = lowerBeginYearGroup.BeginYear; y <= higherBeginYearGroup.BeginYear - 1; y++)
                    {
                        if (lowerBeginYearGroup.Cashflow.ContainsKey(y) && lowerBeginYearGroup.Cashflow[y] != 0)
                            equal = false;
                    }
                }
            }

            if (equal)
            {
                //end year may indicate the last year used, or the last non-zero year used, so have to check both cases
                if (EndYear != other.EndYear)
                {
                    var higherEndYearGroup = EndYear > other.EndYear ? this : other;
                    var lowerEndYearGroup = EndYear < other.EndYear ? this : other;
                    for (var y = lowerEndYearGroup.EndYear + 1; y <= higherEndYearGroup.EndYear; y++)
                    {
                        if (higherEndYearGroup.Cashflow.ContainsKey(y) && higherEndYearGroup.Cashflow[y] != 0)
                            equal = false;
                    }
                }
            }

            if (equal)
            {
                for (int i = Math.Min(Cashflow.Keys.Min(), other.Cashflow.Keys.Min()); i < Math.Max(Cashflow.Keys.Max(), other.Cashflow.Keys.Max()); i++)
                {
                    if (!Cashflow.ContainsKey(i) || !other.Cashflow.ContainsKey(i) || Math.Abs(Cashflow[i] - other.Cashflow[i]) > Utils.TestPrecision)
                        equal = false;
                }
            }

            return equal;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CashflowData)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Id.GetHashCode();
                hashCode = (hashCode * 397) ^ BasisId.GetHashCode();
                hashCode = (hashCode * 397) ^ MemberStatus.GetHashCode();
                hashCode = (hashCode * 397) ^ MinAge.GetHashCode();
                hashCode = (hashCode * 397) ^ MaxAge.GetHashCode();
                hashCode = (hashCode * 397) ^ RevaluationType.GetHashCode();
                hashCode = (hashCode * 397) ^ RetirementAge.GetHashCode();
                hashCode = (hashCode * 397) ^ BenefitType.GetHashCode();
                hashCode = (hashCode * 397) ^ BeginYear.GetHashCode();
                hashCode = (hashCode * 397) ^ EndYear.GetHashCode();
                hashCode = (hashCode * 397) ^ IsBuyIn.GetHashCode();
                hashCode = (hashCode * 397) ^ PensionIncreaseReference.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Id: {0}, BasisId: {1}, MemberStatus: {2}, MinAge: {3}, MaxAge: {4}, RevaluationType: {5}, RetirementAge: {6}, BenefitType: {7}, BeginYear: {8}, EndYear: {9}, IsBuyIn: {10}, PensionIncreaseReference: {11}", Id, BasisId, MemberStatus, MinAge, MaxAge, RevaluationType, RetirementAge, BenefitType, BeginYear, EndYear, IsBuyIn, PensionIncreaseReference);
        }
    }
}
