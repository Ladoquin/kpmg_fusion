﻿namespace FlightDeck.Domain
{
    using System;
    
    public interface IGiltYieldService
    {
        double GetGiltYield(DateTime at);
    }
}
