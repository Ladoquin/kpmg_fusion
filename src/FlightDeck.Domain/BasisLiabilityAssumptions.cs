﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    class BasisLiabilityAssumptions : IBasisLiabilityAssumptions
    {
        public IDictionary<AssumptionType, double> LiabilityAssumptions { get; private set; }
        public LifeExpectancyAssumptions LifeExpectancy { get; private set; }
        public IDictionary<int, double> PensionIncreases { get; private set; }

        public BasisLiabilityAssumptions(IDictionary<AssumptionType, double> assumptions, LifeExpectancyAssumptions lifeExpectancy, IDictionary<int, double> pensionIncreases)
        {
            LiabilityAssumptions = assumptions;
            LifeExpectancy = lifeExpectancy;
            PensionIncreases = pensionIncreases;
        }

        public BasisLiabilityAssumptions(LiabilityEvolutionItem liabilityEvolutionItem)
        {
            LiabilityAssumptions = liabilityEvolutionItem.ToAssumptionsDictionary();
            PensionIncreases = liabilityEvolutionItem.ToPensionIncreasesDictionary();
        }

        public BasisLiabilityAssumptions(LiabilityEvolutionItem liabilityEvolutionItem, LifeExpectancyAssumptions lifeExpectancy)
            : this(liabilityEvolutionItem)
        {
            LifeExpectancy = lifeExpectancy;
        }
    }
}
