﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal static class BasisExtensions
    {
        /// <summary>
        /// Create an adjusted copy of the basis. Note, the current basis is unchanged.
        /// </summary>
        internal static Basis Adjust(this Basis b, IDictionary<AssumptionType, double> assumptions = null, IDictionary<int, double> pensionIncreases = null)
        {
            return new Basis
                (
                    b.Id,
                    b.PensionSchemeId,
                    b.MasterBasisId,
                    b.DisplayName,
                    b.Type,
                    b.EffectiveDate,
                    b.LastPensionIncreaseDate,
                    b.LinkedToCorpBonds,
                    b.SalaryProgression,
                    b.Cashflows,
                    assumptions ?? b.Assumptions,
                    pensionIncreases ?? b.PensionIncreases,
                    b.MortalityAdjustments,
                    b.BondYield,
                    b.ActuarialBalanceInAOCI, b.PriorServiceBalanceInAOCI, b.AmortisationOfPriorService, b.AmortisationPeriod,
                    b.SimpleAssumptions,
                    b.RefGiltYieldId,
                    b.UnderlyingAssumptionIndices,
                    b.UnderlyingPensionIncreaseIndices,
                    b.UnderlyingGiltYieldIndex
                );
        }

        internal static Basis Clone(this Basis b,
                IEnumerable<CashflowData> cashflows = null,
                IDictionary<AssumptionType, double> assumptions = null, 
                IDictionary<int, double> pensionIncreases = null,
                FinancialIndex giltYieldIndex = null,
                DateTime? effectiveDate = null)
        {
            return new Basis(
                    b.Id,
                    b.PensionSchemeId,
                    b.MasterBasisId,
                    b.DisplayName,
                    b.Type,
                    effectiveDate.HasValue ? effectiveDate.Value : b.EffectiveDate,
                    b.LastPensionIncreaseDate,
                    b.LinkedToCorpBonds,
                    b.SalaryProgression.Select(x => x.Clone()).ToList(),
                    cashflows ?? b.Cashflows.Select(x => x.Clone()).ToList(), 
                    assumptions ?? new Dictionary<AssumptionType, double>(b.Assumptions), 
                    pensionIncreases ?? new Dictionary<int, double>(b.PensionIncreases), 
                    new Dictionary<int, double>(b.MortalityAdjustments), 
                    b.BondYield,
                    b.ActuarialBalanceInAOCI, b.PriorServiceBalanceInAOCI, b.AmortisationOfPriorService, b.AmortisationPeriod,
                    b.SimpleAssumptions.ToDictionary(x => x.Key, x => x.Value.Clone()), 
                    b.RefGiltYieldId,
                    b.UnderlyingAssumptionIndices.ToDictionary(x => x.Key, x => x.Value.Clone()),
                    b.UnderlyingPensionIncreaseIndices.ToDictionary(x => x.Key, x => x.Value.Clone()),
                    giltYieldIndex ?? b.UnderlyingGiltYieldIndex
                );
        }

        /// <summary>
        /// Create an adjusted copy of the basis. Note, the current basis is unchanged.
        /// </summary>
        internal static Basis Adjust(this Basis b,
            double? preRetirementDiscountRate = null,
            double? postRetirementDiscountRate = null,
            double? pensionDiscountRate = null,
            double? salaryIncrease = null,
            double? rpi = null,
            double? cpi = null)
        {
            var assumptions = new Dictionary<AssumptionType, double>(b.Assumptions);

            if (preRetirementDiscountRate.HasValue)
                assumptions[AssumptionType.PreRetirementDiscountRate] = preRetirementDiscountRate.Value;
            if (postRetirementDiscountRate.HasValue)
                assumptions[AssumptionType.PostRetirementDiscountRate] = postRetirementDiscountRate.Value;
            if (pensionDiscountRate.HasValue)
                assumptions[AssumptionType.PensionDiscountRate] = pensionDiscountRate.Value;
            if (salaryIncrease.HasValue)
                assumptions[AssumptionType.SalaryIncrease] = salaryIncrease.Value;
            if (rpi.HasValue)
                assumptions[AssumptionType.InflationRetailPriceIndex] = rpi.Value;
            if (cpi.HasValue)
                assumptions[AssumptionType.InflationConsumerPriceIndex] = cpi.Value;

            return b.Adjust(assumptions: assumptions);
        }

        internal static void AdjustPincAssumptions(this Basis b, IDictionary<int, double> pensionIncreaseAssumptions)
        {
            var penIncCount = 6;
            for (int i = 1; i <= penIncCount; i++)
                b.PensionIncreases[i] = pensionIncreaseAssumptions[i];
        }

        internal static void AdjustLiabilityAssumptions(this Basis b, IDictionary<AssumptionType, double> liabAssumptions, LDI.LDICashflowBasis ldiCashflowBasis)
        {
            if (liabAssumptions != null && ldiCashflowBasis != null)
            {

                b.UnderlyingAssumptionIndices[AssumptionType.PreRetirementDiscountRate].OverrideIndex(ldiCashflowBasis.DiscountRateIndex, liabAssumptions[AssumptionType.PreRetirementDiscountRate]);
                b.UnderlyingAssumptionIndices[AssumptionType.PostRetirementDiscountRate].OverrideIndex(ldiCashflowBasis.DiscountRateIndex, liabAssumptions[AssumptionType.PostRetirementDiscountRate]);
                b.UnderlyingAssumptionIndices[AssumptionType.PensionDiscountRate].OverrideIndex(ldiCashflowBasis.DiscountRateIndex, liabAssumptions[AssumptionType.PensionDiscountRate]);
                b.UnderlyingAssumptionIndices[AssumptionType.SalaryIncrease].OverrideIndex(ldiCashflowBasis.InflationIndex, liabAssumptions[AssumptionType.SalaryIncrease]);
                b.UnderlyingAssumptionIndices[AssumptionType.InflationRetailPriceIndex].OverrideIndex(ldiCashflowBasis.InflationIndex, liabAssumptions[AssumptionType.InflationRetailPriceIndex]);
                b.UnderlyingAssumptionIndices[AssumptionType.InflationConsumerPriceIndex].OverrideIndex(ldiCashflowBasis.InflationIndex, liabAssumptions[AssumptionType.InflationConsumerPriceIndex]);

                foreach (var t in Enum.GetValues(typeof(AssumptionType)))
                    b.Assumptions[(AssumptionType)t] = liabAssumptions[(AssumptionType)t];
            }
        }

        internal static IDictionary<int, double> GetVariablePincAssumption(this Basis b, DateTime atDate, double userRPI, double userCPI, bool applyInflationAdjustment, DateTime? effectiveDate = null)
        {
            var pincAssumptions = new Dictionary<int, double>();
            var penIncCount = 6;
            var pincIndices = b.UnderlyingPensionIncreaseIndices;

            var effDate = effectiveDate.GetValueOrDefault(b.EffectiveDate);

            for (int i = 1; i <= penIncCount; i++)
            {
                var pinc = pincIndices[i];
                var val = .0;
                var infChange = .0;
                if (applyInflationAdjustment)
                {
                    var infRPI = b.UnderlyingAssumptionIndices[AssumptionType.InflationRetailPriceIndex].GetValue(atDate, effDate);
                    var infCPI = b.UnderlyingAssumptionIndices[AssumptionType.InflationConsumerPriceIndex].GetValue(atDate, effDate);
                    if (pinc.InflationType == InflationType.Rpi)
                        infChange = userRPI - infRPI;
                    if (pinc.InflationType == InflationType.Cpi)
                        infChange = userCPI - infCPI;
                }

                var infRate = pinc.InflationType == InflationType.Rpi ? userRPI : userCPI;
                if (pinc.Category == AssumptionCategory.BlackScholes)
                {
                    val = pinc.GetValue(atDate, effDate,
                            notionalInflationOverride: infRate);
                }
                else if (pinc.Category == AssumptionCategory.Fixed && pinc.InflationType != InflationType.None)
                    val = infRate;
                else
                    val = pinc.GetValue(atDate, effDate, infChange);

                pincAssumptions.Add(i, val);
            }

            return pincAssumptions;
        }

        internal static IBasisLiabilityAssumptions GetAssumptionsAtDate(this Basis b, DateTime at, DateTime? effectiveDate = null)
        {
            var liabAssumptionsAtDate = new Dictionary<AssumptionType, double>();
            foreach (var t in Enum.GetValues(typeof(AssumptionType)))
            {
                var val = b.UnderlyingAssumptionIndices[(AssumptionType)t].GetValue(at, effectiveDate.GetValueOrDefault(b.EffectiveDate));
                liabAssumptionsAtDate.Add((AssumptionType)t, val);
            }

            var userRPI = liabAssumptionsAtDate[AssumptionType.InflationRetailPriceIndex];
            var userCPI = liabAssumptionsAtDate[AssumptionType.InflationConsumerPriceIndex];
            var pincs = b.GetVariablePincAssumption(at, userRPI, userCPI, false);

            return new BasisLiabilityAssumptions(liabAssumptionsAtDate, new LifeExpectancyAssumptions(b.LifeExpectancy.PensionerLifeExpectancy, b.LifeExpectancy.DeferredLifeExpectancy), pincs);
        }
    }
}
