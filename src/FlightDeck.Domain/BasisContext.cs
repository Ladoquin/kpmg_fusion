﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using System;
    
    public class BasisContext : IBasisContext
    {
        public DateTime StartDate { get; private set; }
        public DateTime AnalysisDate { get; private set; }
        public DateTime EffectiveDate { get; private set; }
        public Basis Data { get; set; }
        public IPortfolio Portfolio { get; private set; }
        public BasisEvolutionData EvolutionData { get; private set; }
        public BasisAnalysisResults Results { get; set; }
        public IBasisAssumptions Assumptions { get; private set; }

        public BasisContext(DateTime startDate, DateTime analysisDate, DateTime effectiveDate, Basis data, IPortfolio portfolio, BasisAnalysisResults results, BasisEvolutionData evolution, IBasisAssumptions assumptions)
        {
            StartDate = startDate;
            AnalysisDate = analysisDate;
            EffectiveDate = effectiveDate;
            Data = data;
            Portfolio = portfolio;
            EvolutionData = evolution;
            Results = results;
            Assumptions = assumptions;
        }
    }
}
