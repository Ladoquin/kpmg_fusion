﻿namespace FlightDeck.Domain
{
    struct StartEnd<T>
    {
        public T Start { get; private set; }
        public T End { get; private set; }

        public StartEnd(T start, T end)
            : this()
        {
            Start = start;
            End = end;
        }
    }
}
