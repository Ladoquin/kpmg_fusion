﻿using System;

namespace FlightDeck.Domain
{
    public class RecoveryPayment
    {
        public int PensionSchemeId { get; private set; }
        public DateTime Date { get; private set; }
        public DateTime EndDate { get; private set; }
        public double Amount { get; private set; }
        public int Gap { get; private set; }

        public RecoveryPayment(int pensionSchemeId, DateTime date, DateTime endDate, double amount, int gap)
        {
            EndDate = endDate;
            Gap = gap;
            Amount = amount;
            Date = date;
            PensionSchemeId = pensionSchemeId;
        }
    }
}