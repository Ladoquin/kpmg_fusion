﻿namespace FlightDeck.Domain
{
    using System;

    public class BondYieldService : IGiltYieldService
    {
        private readonly Basis b;

        public BondYieldService(Basis b)
        {
            this.b = b;
        }

        public double GetGiltYield(DateTime at)
        {
            return b.BondYield.Index.GetValue(at) / 100;
        }
    }
}
