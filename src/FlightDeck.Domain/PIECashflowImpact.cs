﻿namespace FlightDeck.Domain
{
    class PIECashflowImpact
    {
        public double ValueBefore { get; private set; }
        public double UpliftFactor { get; private set; }
        public double ValueNilIncs { get; private set; }

        public PIECashflowImpact(double valueBefore, double upliftFactor, double valueNilIncs)
        {
            ValueBefore = valueBefore;
            UpliftFactor = upliftFactor;
            ValueNilIncs = valueNilIncs;
        }
    }
}
