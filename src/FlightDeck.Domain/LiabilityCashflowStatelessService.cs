﻿namespace FlightDeck.Domain
{
    using System.Collections.Generic;

    /// <summary>
    /// Liability Cashflow Service
    /// Use this stateless instance client changes are being ignored.
    /// For example,
    /// When using in order to work out Liabilty Evolution, FRO/ETV changes are not taken in to 
    /// consideration and no state is required to evalution the liability on a particular date.
    /// </summary>
    /// <remarks>
    /// No need to pass liability evolution data to the base because this is only required when mortality adjustments have been made, 
    /// and if you're doing that then you should be using LiabilityCashflowStatefulService instead.
    /// </remarks>
    class LiabilityCashflowStatelessService : LiabilityCashflowServiceBase
    {
        public LiabilityCashflowStatelessService(Basis basis)
            : base(basis, null, RunMode.Stateless)
        {
        }
        public LiabilityCashflowStatelessService(Basis basis, bool adjustCashflows)
            : base(basis, null, adjustCashflows ? RunMode.Normal : RunMode.Stateless)
        {
        }
    }
}
