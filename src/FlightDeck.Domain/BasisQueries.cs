﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightDeck.DomainShared;

namespace FlightDeck.Domain
{
    class BasisQueries
    {
        public double GetSalaryRollAtDate(Basis basis, DateTime date)
        {
            for (int y = 1; y <= 100; y++)
            {
                if (basis.EffectiveDate.AddYears(y) > date)
                    return basis.SalaryProgression.Count > 0 && basis.SalaryProgression[0].SalaryRoll.ContainsKey(y)
                        ? basis.SalaryProgression[0].SalaryRoll[y]
                        : 0;
            }

            return 0;
        }
    }
}
