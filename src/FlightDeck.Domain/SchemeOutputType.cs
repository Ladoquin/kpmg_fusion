﻿namespace FlightDeck.Domain
{
    using System;

    [Flags]
    public enum SchemeOutputType
    {
        None = 0,
        JourneyPlanAfter = 1,
        BuyinCost = 2,
        BuyoutStrains = 4
    }
}
