﻿namespace FlightDeck.Domain
{
    public class PensionSchemeApplicationParameters
    {
        public double InsuranceBasisPointImpact { get; private set; }
        public double FROMinTVPercentage { get; private set; }
        public double FROMaxTVPercentage { get; private set; }
        public double ETVMinTVPercentage { get; private set; }
        public double ETVMaxTVPercentage { get; private set; }
        public int GiltDurationDefault { get; private set; }
        public int CorpDurationDefault { get; private set; }
        
        public PensionSchemeApplicationParameters(
            double insuranceBasisPointImpact,
            double froMinTVPercentage,
            double froMaxTVPercentage,
            double etvMinTVPercentage,
            double etvMaxTVPercentage,
            int giltDurationDefault,
            int corpDurationDefault)
        {
            InsuranceBasisPointImpact = insuranceBasisPointImpact;
            FROMinTVPercentage = froMinTVPercentage;
            FROMaxTVPercentage = froMaxTVPercentage;
            ETVMinTVPercentage = etvMinTVPercentage;
            ETVMaxTVPercentage = etvMaxTVPercentage;
            GiltDurationDefault = giltDurationDefault;
            CorpDurationDefault = corpDurationDefault;
        }
    }
}
