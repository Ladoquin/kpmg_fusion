﻿namespace FlightDeck.Domain
{
    public delegate void InputChangeHandler(object sender, AssumptionChangeType e);
}
