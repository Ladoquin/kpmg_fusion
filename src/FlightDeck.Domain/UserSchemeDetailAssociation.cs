﻿using FlightDeck.DomainShared;

namespace FlightDeck.Domain
{
    public class UserSchemeDetailAssociation
    {
        public int UserId { get; set; }
        public SchemeDetail SchemeDetail { get; set; }
    }
}
