﻿using System;

namespace FlightDeck.Domain
{
    public abstract class AssumptionBase
    {
        protected const double multiplyer = 0.01;
        public int Id { get; private set; }
        public int BasisId { get; private set; }
        public bool IsVisible { get; private set; }
        public double InitialValue { get; private set; }
        public AssumptionCategory Category { get; private set; }
        public FinancialIndex Index { get; private set; }

        public AssumptionBase(int id, int basisId, double initialValue, FinancialIndex index, bool isVisible = true,
            AssumptionCategory category = AssumptionCategory.Fixed)
        {
            Id = id;
            BasisId = basisId;
            Index = index;
            InitialValue = initialValue;
            Category = category;
            IsVisible = isVisible;
        }

        public virtual double GetValue(DateTime date, DateTime effectiveDate, double? adjustment = null)
        {
            var result =  Index != null ? 
                InitialValue + (Index.GetValue(date) - Index.GetValue(effectiveDate))*multiplyer : // TODO: why?
                InitialValue;
            if (adjustment.HasValue)
                result += adjustment.Value;
            return result;
        }

        public void OverrideIndex(FinancialIndex index, double initialValue)
        {
            Index = index;
            InitialValue = initialValue;
        }
    }
}