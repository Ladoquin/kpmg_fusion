﻿namespace FlightDeck.Domain
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    /// <summary>
    /// Current state of scheme options.
    /// These are essentially the Client Before-After options flattened
    /// </summary>
    public interface ISchemeAssumptions
    {
        FROType FROType { get; }
        FlexibleReturnOptions FROOptions { get; }
        EmbeddedFlexibleReturnOptions EFROOptions { get; }
        EnhancedTransferValueOptions ETVOptions { get; }
        PieOptions PIEOptions { get; }
        FutureBenefitsParameters FutureBenefitOptions { get; }
        IDictionary<AssetClassType, double> InvestmentStrategyAssetAllocation { get; }
        InvestmentStrategyOptions InvestmentStrategyOptions { get; }        
        AbfOptions ABFOptions { get; }
        InsuranceParameters InsuranceOptions { get; }
        RecoveryPlanType RecoveryPlanOptions { get; }
        WaterfallOptions WaterfallOptions { get; set; }
    }
}
