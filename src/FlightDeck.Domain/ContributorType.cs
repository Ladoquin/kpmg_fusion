﻿namespace FlightDeck.Domain
{
    enum ContributorType
    {
        Employer,
        Employee
    }
}
