﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.DomainShared;

    class BuyinCalculator
    {
        private readonly ISchemeContext scheme;

        public BuyinCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public BuyinData Calculate(IBasisContext ctx)
        {
            var InsPenLiab = 
                ctx.Results.UserPenLiab + 
                ctx.Results.PIE.Liability.After - 
                ctx.Results.PIE.Liability.Before;
            var InsNonPenLiab = 
                ctx.Results.UserActLiab +
                ctx.Results.UserDefLiab -
                ctx.Results.FRO.LiabilityDischarged - 
                ctx.Results.ETV.LiabilityDischarged;

            var uninsuredPenLiab = InsPenLiab - ctx.Results.UserPenBuyIn;
            var uninsuredNonPenLiab = InsNonPenLiab - ctx.Results.UserDefBuyIn;

            var InsPenAsset = uninsuredPenLiab * scheme.Assumptions.InsuranceOptions.PensionerLiabilityPercInsured;
            var InsNonPenAsset = uninsuredNonPenLiab * scheme.Assumptions.InsuranceOptions.NonpensionerLiabilityPercInsured;

            return new BuyinData(uninsuredPenLiab, uninsuredNonPenLiab, InsPenAsset, InsNonPenAsset);
        }
    }
}
