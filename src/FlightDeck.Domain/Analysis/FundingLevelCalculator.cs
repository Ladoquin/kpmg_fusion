﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.DomainShared;
    using System;

    class FundingLevelCalculator
    {
        private readonly ISchemeContext scheme;

        public FundingLevelCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public FundingData Calculate(IBasisContext ctx)
        {
            //liabs
            var actives = ctx.Results.UserActLiab - ctx.Results.FutureBenefits.LiabilitiesChange - ctx.Results.EFRO.ChangeInActivesPastLiabilitiesDischarged;
            var deferreds = ctx.Results.UserDefLiab - ctx.Results.FRO.LiabilityDischarged - ctx.Results.ETV.LiabilityDischarged - ctx.Results.EFRO.ChangeInDeferredsLiabilitiesDischarged;
            var pensioners = ctx.Results.UserPenLiab - (ctx.Results.PIE.Liability.Before - ctx.Results.PIE.Liability.After);

            //assets
            var abf = ctx.Data.Type == BasisType.Accounting ? 0 : scheme.Assumptions.ABFOptions.Value;
            var assets =
                ctx.Results.AssetsAtSED -
                ctx.Results.FRO.CetvPaid -
                ctx.Results.ETV.CetvPaid -
                ctx.Results.BuyInAtAnalysisDate -
                scheme.Calculated.BuyinCost.Total +
                abf +
                scheme.Assumptions.InvestmentStrategyOptions.CashInjection;
            var buyin =
                (ctx.Results.UserDefBuyIn + ctx.Results.UserPenBuyIn) +
                (ctx.Results.Insurance.PensionerLiabilityToBeInsured + ctx.Results.Insurance.NonPensionerLiabilityToBeInsured);

            var liabsTotal = actives + deferreds + pensioners;
            var assetsTotal = assets + buyin;
            var deficitTotal = assetsTotal - liabsTotal;

            return new FundingData(actives, deferreds, pensioners, assets, buyin, deficitTotal);
        }
    }
}
