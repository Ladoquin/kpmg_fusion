﻿using System;
using System.Linq;
using FlightDeck.DomainShared;

namespace FlightDeck.Domain.Analysis
{
    class AbfCalculator
    {
        private ISchemeContext scheme;

        public AbfCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public AbfData Calculate(IBasisContext ctx)
        {
            const int years = 4;

            var deficit = ctx.Results.FundingLevel.Liabilities - ((ctx.Results.FundingLevel.Assets + ctx.Results.FundingLevel.Buyin) - scheme.Assumptions.ABFOptions.Value);
            var discountRate =
                (
                ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] *
                ctx.Results.UserActLiab +
                ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] *
                ctx.Results.UserDefLiab +
                ctx.Assumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate] *
                ctx.Results.UserPenLiab
                )
                /
                (
                ctx.Results.UserActLiab + ctx.Results.UserDefLiab + ctx.Results.UserPenLiab
                );

            var rpLength = 10;
            var rpConts = Math.Max(0, deficit.MathSafeDiv((1 - Math.Pow(1 + discountRate, -rpLength)).MathSafeDiv(Math.Log(1 + discountRate))));

            var corpTax = 0.20;
            var rpNetCash = rpConts * years * (1 - corpTax);

            var proportion = scheme.Assumptions.ABFOptions.Value.MathSafeDiv(deficit);
            var value = scheme.Assumptions.ABFOptions.Value;

            var abfLength = 20;
            var abfConts = value.MathSafeDiv((1 - Math.Pow(1 + discountRate, -abfLength)).MathSafeDiv(discountRate));

            var totInterest = .0;
            var balance = value;
            for (int y = 1; y <= years; y++)
            {
                var interest = balance * discountRate;
                totInterest += interest;
                balance = balance + interest - abfConts;
            }

            var abfNetCash = Math.Max(0, years * abfConts - value * corpTax - totInterest * corpTax
                    + years * Math.Max(0, (deficit - value).MathSafeDiv(deficit) * rpConts * (1 - corpTax)));
            var cashSaving = rpNetCash - abfNetCash;

            // abf value is slightly different in that it's both a client settable value and a result required in calcs
            // but requires the basis type logic around it as below (because it's settable at the scheme level)
            var basisValue = ctx.Data.Type == BasisType.Accounting ? 0 : value;

            return new AbfData(deficit, rpConts, rpNetCash, proportion, abfConts, abfNetCash, cashSaving, basisValue);
        }
    }
}