﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    
    class UserLiabsCalculator
    {
        public UserLiabilityData Calculate(IBasisContext ctx)
        {
            ctx = ctx.AdjustData(
                pensionIncreases: ctx.EvolutionData.LiabilityEvolution[ctx.EffectiveDate].ToPensionIncreasesDictionary());

            var npv = new LiabilityCashflowStatefulService(ctx).AdjustTo(
                ctx.AnalysisDate,
                ctx.Assumptions,
                true);

            return new UserLiabilityData
                (
                npv.Liabilities[MemberStatus.ActivePast],
                npv.Liabilities[MemberStatus.Deferred],
                npv.Liabilities[MemberStatus.Pensioner],
                npv.Liabilities[MemberStatus.ActiveFuture],
                npv.Duration[MemberStatus.ActivePast],
                npv.Duration[MemberStatus.Deferred],
                npv.Duration[MemberStatus.Pensioner],
                npv.Buyin[MemberStatus.Deferred],
                npv.Buyin[MemberStatus.Pensioner]
                );
        }
    }
}
