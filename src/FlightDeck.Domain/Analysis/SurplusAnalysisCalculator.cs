﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.DomainShared;
    using System;
    using System.Linq;

    class SurplusAnalysisCalculator
    {
        private readonly ISchemeContext scheme;

        public SurplusAnalysisCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public SurplusAnalysisData Calculate(IBasisContext ctx)
        {
            var start = ctx.StartDate;
            var end = ctx.AnalysisDate;

            var period = (end - start).TotalDays / Utils.DaysInAYear;

            var assetsAtStart = ctx.EvolutionData.AssetEvolution[start].AssetValue + ctx.EvolutionData.LiabilityEvolution[start].PenBuyIn + ctx.EvolutionData.LiabilityEvolution[start].DefBuyIn;

            var liabsAtStart = ctx.EvolutionData.LiabilityEvolution[start].Act + ctx.EvolutionData.LiabilityEvolution[start].Def + ctx.EvolutionData.LiabilityEvolution[start].Pen;

            var assetsAtEnd = ctx.EvolutionData.AssetEvolution[end].AssetValue + ctx.EvolutionData.LiabilityEvolution[end].PenBuyIn + ctx.EvolutionData.LiabilityEvolution[end].DefBuyIn;

            var liabsAtEnd = ctx.EvolutionData.LiabilityEvolution[end].Act + ctx.EvolutionData.LiabilityEvolution[end].Def + ctx.EvolutionData.LiabilityEvolution[end].Pen;

            var initialDeficit = assetsAtStart - liabsAtStart;

            var interestOnLiabs =
                -(ctx.EvolutionData.LiabilityEvolution[start].Act + ctx.EvolutionData.LiabilityEvolution[start].Def) *
                (Math.Pow(1 + ctx.EvolutionData.LiabilityEvolution[start].DiscPre, period) - 1) -
                ctx.Results.Attribution.AttrEeeContsReceived *
                (Math.Pow(1 + ctx.EvolutionData.LiabilityEvolution[start].DiscPre, period / 2) - 1) -
                ctx.EvolutionData.LiabilityEvolution[start].Pen *
                (Math.Pow(1 + ctx.EvolutionData.LiabilityEvolution[start].DiscPen, period) - 1) +
                ctx.Results.Attribution.AttrTotBenefitOutgo *
                (Math.Pow(1 + ctx.EvolutionData.LiabilityEvolution[start].DiscPen, period / 2) - 1);
            
            var liabilityStepChangeTotal = ctx.EvolutionData.LiabilityExperience.Where(x => x.Key > start && x.Key <= end).Sum(x => x.Value);
            var assetStepChangeTotal = ctx.EvolutionData.AssetExperience.Where(x => x.Key > start && x.Key <= end).Sum(x => x.Value);
            var buyinStepChangeTotal = ctx.EvolutionData.BuyinExperience.Where(x => x.Key > start && x.Key <= end).Sum(x => x.Value);

            var liabilityExperience = -liabilityStepChangeTotal;

            var contributions = ctx.Results.Attribution.AttrRecoveryPlanConts;

            var assetExperience = assetStepChangeTotal + buyinStepChangeTotal;

            var assetReturn = assetsAtEnd - (assetsAtStart + (ctx.Results.Attribution.AttrEeeContsReceived + ctx.Results.Attribution.AttrEerContsReceived) + ctx.Results.Attribution.AttrRecoveryPlanConts - ctx.Results.Attribution.AttrTotBenefitOutgo + assetExperience);

            var endDeficit = assetsAtEnd - liabsAtEnd;

            var marketChange = endDeficit - (initialDeficit + liabilityExperience + interestOnLiabs + assetExperience + contributions + assetReturn);

            return new SurplusAnalysisData(initialDeficit, interestOnLiabs, marketChange, liabilityExperience, contributions, assetReturn, assetExperience, endDeficit);
        }
    }
}
