﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;

    class InvestmentStrategyCalculator
    {
        private readonly ISchemeContext scheme;

        public InvestmentStrategyCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public InvestmentStrategyData Calculate(IBasisContext ctx)
        {
            var newAbf = scheme.Assumptions.ABFOptions.Value;
            var abfProportion = scheme.Assumptions.InvestmentStrategyAssetAllocation.ContainsKey(AssetClassType.Abf) ? scheme.Assumptions.InvestmentStrategyAssetAllocation[AssetClassType.Abf] : 0;

            if (ctx.Data.Type == BasisType.Accounting)
            {
                newAbf = 0;
                abfProportion = 0;
            }

            var abfAdjustment = (1.0).MathSafeDiv(1.0 - abfProportion);

            // breakdown            
            var existingABF = (ctx.Results.AssetsAtSED - ctx.Results.BuyInAtAnalysisDate) * abfProportion;
            var coreAssets = ctx.Results.AssetsAtSED - ctx.Results.BuyInAtAnalysisDate - existingABF;

            // revised core assets
            var subTotalCoreAssets =
                coreAssets +
                scheme.Assumptions.InvestmentStrategyOptions.CashInjection +
                -scheme.Calculated.BuyinCost.Total +
                -ctx.Results.FRO.CetvPaid +
                -ctx.Results.ETV.CetvPaid;

            // revised other assets
            var buyinTotal =
                (ctx.Results.UserPenBuyIn + ctx.Results.UserDefBuyIn) +
                (ctx.Results.Insurance.PensionerLiabilityToBeInsured + ctx.Results.Insurance.NonPensionerLiabilityToBeInsured);
            var abfTotal =
                existingABF +
                newAbf;
            var subTotalOtherAssets =
                abfTotal +
                buyinTotal;

            var grandTotalAssets = subTotalCoreAssets + subTotalOtherAssets;

            var coreMarketValue = .0;
            foreach (var allocation in scheme.Assumptions.InvestmentStrategyAssetAllocation)
            {
                coreMarketValue += subTotalCoreAssets * allocation.Value;
            }

            var grandTotal = coreMarketValue + subTotalOtherAssets;

            return new InvestmentStrategyData(coreMarketValue, buyinTotal, abfTotal, abfAdjustment);
        }
    }
}
