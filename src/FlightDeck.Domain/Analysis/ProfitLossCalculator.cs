﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.DomainShared;
    using System;
    using System.Linq;
    
    class ProfitLossCalculator
    {
        private readonly ISchemeContext scheme;

        public ProfitLossCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public ProfitLossData Calculate(IBasisContext ctx)
        {
            var salaryRoll = .0;
            for (int y = 1; y <= 100; y++)
            {
                salaryRoll = ctx.Data.SalaryProgression.Count > 0 && ctx.Data.SalaryProgression[0].SalaryRoll.ContainsKey(y)
                    ? ctx.Data.SalaryProgression[0].SalaryRoll[y]
                    : 0;
                if (ctx.Data.EffectiveDate.AddYears(y) > ctx.AnalysisDate)
                    break;
            }

            var accrualCost = ctx.Results.FutureBenefits.AnnualCost;

            var totalRate = accrualCost.MathSafeDiv(salaryRoll);

            var contRate = scheme.Data.ContributionRates.Where(x => x.Date <= ctx.Data.EffectiveDate).OrderBy(x => x.Date).FirstOrDefault();
            var contRateEmployee = contRate != null ? contRate.Employee : 0;
            var contRateEmployer = contRate != null ? contRate.Employer : 0;

            var employerCost = Math.Round(totalRate - contRateEmployee, 3);

            var serviceCostFRS = employerCost * salaryRoll;

            var memberConts = contRateEmployee * salaryRoll;

            var serviceCostIAS = totalRate * salaryRoll * (1 + ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] / 2) - memberConts;

            var benefitsPaid = ctx.Results.Cashflow[ctx.AnalysisDate.Year].Where(x => x.Key != MemberStatus.ActiveFuture).Sum(x => x.Value);

            var employerConts = contRateEmployer * salaryRoll;

            var rpp = ctx.Results.RecoveryPlanPayments.Values;
            var deficitConts = scheme.Assumptions.RecoveryPlanOptions == RecoveryPlanType.Current
                ? (rpp.Any() ? rpp.First().Before : 0)
                : (rpp.Any() ? rpp.First().After : 0);

            var benefitsPaidInsured = ctx.Results.BuyinCashflow[ctx.AnalysisDate.Year][MemberStatus.Pensioner];

            var netInvestments = (employerConts + deficitConts + memberConts + benefitsPaidInsured) - benefitsPaid;

            return new ProfitLossData(salaryRoll, accrualCost, totalRate, employerCost, serviceCostFRS, serviceCostIAS, benefitsPaid, employerConts, deficitConts, memberConts, benefitsPaidInsured, netInvestments);
        }
    }
}
