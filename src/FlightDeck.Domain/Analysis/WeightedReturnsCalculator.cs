﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.DomainShared;
    using System;
    using System.Linq;
    using System.Collections.Generic;

    class WeightedReturnsCalculator
    {
        public WeightedReturnParameters Calculate(ISchemeContext scheme, IBasisContext ctx)
        {
            var a = ctx.Results.UserActLiab;
            var d = ctx.Results.UserDefLiab - ctx.Results.FRO.LiabilityDischarged - ctx.Results.ETV.LiabilityDischarged;// -ctx.Results.EFRO.ChangeInDeferredsLiabilitiesDischarged;
            var p = ctx.Results.UserPenLiab - ctx.Results.PIE.LiabilityChange;

            var weightedDiscountRate =
                (a * ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] +
                 d * ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] +
                 p * ctx.Assumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate])
                / (a + d + p);

            var yield = ctx.Data.UnderlyingGiltYieldIndex.GetValue(ctx.AnalysisDate) / 100;

            var maxReturn = GetMaxReturn(scheme, ctx);

            return new WeightedReturnParameters(ctx.Results.JP2InvGrowth, ctx.Results.JP2InvGrowth - yield, weightedDiscountRate, weightedDiscountRate - yield, ctx.Results.JPInvGrowth - yield, maxReturn);
        }

        public double GetMaxReturn(ISchemeContext scheme, IBasisContext ctx)
        {
            //var nonGrowthAssetGrowth = scheme.Assumptions.InvestmentStrategyAssetAllocation[]
            var investmentStrategy = scheme.Assumptions.InvestmentStrategyAssetAllocation;
            var coreAssets = ctx.Results.InvestmentStrategy.TotalCoreAssets;
            var abfAdjust = ctx.Results.InvestmentStrategy.ABFAdjustment;

            var growthAssetValue = investmentStrategy.Where(
                                x => Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(x.Key).Category == AssetCategory.Growth
                              ).Sum(x => x.Value) * coreAssets * abfAdjust;
            var nonGrowthAssetValue = coreAssets + ctx.Results.InvestmentStrategy.ABF - growthAssetValue;

            var penBuyin = ctx.Results.UserPenBuyIn + ctx.Results.Insurance.PensionerLiabilityToBeInsured;
            var nonPenBuyin = ctx.Results.UserDefBuyIn + ctx.Results.Insurance.NonPensionerLiabilityToBeInsured;

            var totalAssetValue = nonGrowthAssetValue + growthAssetValue + penBuyin + nonPenBuyin;

            var jpGiltYield = new GiltYieldService(ctx.Data).GetGiltYield(ctx.AnalysisDate);

            var marketGrowth = ((nonGrowthAssetValue * jpGiltYield)
                               + (growthAssetValue * (jpGiltYield + .04))
                               + (penBuyin * ctx.Assumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate])
                               + (nonPenBuyin * ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate])
                               )
                               .MathSafeDiv(totalAssetValue);
            return marketGrowth;
        }
    }
}
