﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public class InsuranceCalculator
    {
        private readonly ISchemeContext scheme;

        public InsuranceCalculator(ISchemeContext scheme)
        {
            this.scheme = scheme;
        }

        public double GetBasisPointChangeImpact(IBasisContext ctx)
        {
            if (ctx.Data.Type != BasisType.Buyout)
                return 0;

            var pointIncrease = scheme.Data.ApplicationParameters.InsuranceBasisPointImpact;

            var DefProp = scheme.Assumptions.InsuranceOptions.NonpensionerLiabilityPercInsured;
            var PenProp = scheme.Assumptions.InsuranceOptions.PensionerLiabilityPercInsured;

            var lcf = new LiabilityCashflowStatefulService(ctx).AdjustTo(ctx.AnalysisDate, ctx.Assumptions, true);

            var NpvPre = DefProp * lcf.Liabilities[MemberStatus.ActivePast] + DefProp * (lcf.Liabilities[MemberStatus.Deferred] - lcf.Buyin[MemberStatus.Deferred]) + PenProp * (lcf.Liabilities[MemberStatus.Pensioner] - lcf.Buyin[MemberStatus.Pensioner]);

            var adjustedAssumptions =
                ctx.AdjustAssumptions(
                    assumptions:
                        new Dictionary<AssumptionType, double>
                            {
                                { AssumptionType.PreRetirementDiscountRate, ctx.Assumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate] + pointIncrease },
                                { AssumptionType.PostRetirementDiscountRate, ctx.Assumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate] + pointIncrease },
                                { AssumptionType.PensionDiscountRate, ctx.Assumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate] + pointIncrease },
                                { AssumptionType.SalaryIncrease, ctx.Assumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease] },
                                { AssumptionType.InflationRetailPriceIndex, ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex] },
                                { AssumptionType.InflationConsumerPriceIndex, ctx.Assumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex] }
                            }
                    );


            lcf = new LiabilityCashflowStatefulService(ctx).AdjustTo(ctx.AnalysisDate, adjustedAssumptions.Assumptions, true);

            var NpvPost = DefProp * lcf.Liabilities[MemberStatus.ActivePast] + DefProp * (lcf.Liabilities[MemberStatus.Deferred] - lcf.Buyin[MemberStatus.Deferred]) + PenProp * (lcf.Liabilities[MemberStatus.Pensioner] - lcf.Buyin[MemberStatus.Pensioner]);

            var impact = NpvPost - NpvPre;

            return impact;
        }
    }
}
