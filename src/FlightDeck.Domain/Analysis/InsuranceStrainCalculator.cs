﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class InsuranceStrainCalculator
    {
        private readonly ISchemeContext scheme;
        private DateTime attributionEnd;

        public InsuranceStrainCalculator(ISchemeContext scheme, DateTime attributionEnd)
        {
            this.scheme = scheme;
            this.attributionEnd = attributionEnd;
        }

        /// <summary>
        /// TODO this takes in EvolutionData as a parameter, but it only needs the Liability evolution. 
        /// This is currently because CachedPensionScheme calls this, because CachedPensionScheme has access to all saved Evolution data. 
        /// PensionScheme would not have this. But don't want to waste time converting EvolutionData back to Liability evolution. 
        /// Could pass in a translation Func<> ? 
        /// Or CachedBasis has the underlying data you need.
        /// </summary>
        /// <param name="basesEvolution"></param>
        /// <returns></returns>
        public IDictionary<int, IDictionary<StrainType, double>> Calculate(IDictionary<MasterBasisKey, IDictionary<DateTime, EvolutionData>> basesEvolution)
        {
            if (!basesEvolution.Any(x => x.Key.BasisType == BasisType.Buyout))
                return null;

            var matrix = new Dictionary<int, IDictionary<StrainType, double>>(10);
            
            var buyoutTracker = 
                basesEvolution[basesEvolution.Keys.First(x => x.BasisType == BasisType.Buyout)]
                    .ToDictionary(
                        x => x.Key,
                        x => Tuple.Create(x.Value.Active, x.Value.Deferred, x.Value.Pensioner, x.Value.DeferredBuyIn + x.Value.PensionerBuyIn));

            var insPenPropotion = scheme.Assumptions.InsuranceOptions.PensionerLiabilityPercInsured;
            var insDefPropotion = scheme.Assumptions.InsuranceOptions.NonpensionerLiabilityPercInsured;

            var otherKeys = basesEvolution.Keys.Where(key => key.BasisType != BasisType.Buyout);
            foreach (var otherKey in otherKeys)
            {
                var other = basesEvolution[otherKey];

                var tracker =
                    other.ToDictionary(
                        x => x.Key,
                        x => Tuple.Create(x.Value.Active, x.Value.Deferred, x.Value.Pensioner, x.Value.DeferredBuyIn + x.Value.PensionerBuyIn));

                var startDate = tracker.Keys.Min() > buyoutTracker.Keys.Min() ? tracker.Keys.Min() : buyoutTracker.Keys.Min();

                var differences =
                    buyoutTracker
                    .Where(x => x.Key >= startDate && x.Key <= attributionEnd)
                        .Select(x => new
                        {
                            date = x.Key,
                            value = insDefPropotion * (x.Value.Item1 - tracker[x.Key].Item1) +
                                    insDefPropotion * (x.Value.Item2 - tracker[x.Key].Item2) +
                                    insPenPropotion * ((x.Value.Item3 - x.Value.Item4) - (tracker[x.Key].Item3 - tracker[x.Key].Item4))
                        })
                        .OrderBy(x => x.date);


                var boundary = Math.Pow(10, 100);

                var minstrain = Math.Min(boundary, differences.Any() ? differences.Min(x => x.value) : 0);
                var maxstrain = Math.Max(-boundary, differences.Any() ? differences.Max(x => x.value) : 0);

                var matrixItem = new Dictionary<StrainType, double>
                        {
                            { StrainType.Current, differences.Any() ? differences.Last().value : .0 },
                            { StrainType.Max, maxstrain },
                            { StrainType.Min, minstrain }
                        };

                matrix.Add(otherKey.Id, matrixItem);
            }

            return matrix;
        }
    }
}
