﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.Domain.Evolution;
    using FlightDeck.DomainShared;
    
    class PIEInitCalculator
    {
        public PIEInitData Calculate(IBasisContext ctx)
        {
            var adjustedCtx = ctx.AdjustData(
                pensionIncreases: ctx.EvolutionData.LiabilityEvolution[ctx.Data.EffectiveDate].ToPensionIncreasesDictionary());

            var lcf = (ILiabilityCashflowStatefulService)new LiabilityCashflowStatefulService(adjustedCtx);

            lcf.AdjustTo(ctx.AnalysisDate, adjustedCtx.Assumptions, true);

            var pie = lcf.ApplyPIE(new PieOptions(1, 1, 1), null, adjustedCtx.Assumptions.PensionIncreases);

            var valueAfter = pie.ValueNilIncs * pie.UpliftFactor;

            return new PIEInitData(pie.ValueBefore, valueAfter - pie.ValueNilIncs);
        }
    }
}
