﻿namespace FlightDeck.Domain.Analysis
{
    using FlightDeck.DomainShared;
    
    class BalanceSheetCalculator
    {
        public BalanceData Calculate(IBasisContext ctx)
        {
            var assets = ctx.Results.InvestmentStrategy.TotalCoreAssets + ctx.Results.InvestmentStrategy.Buyin;

            var liabilities =
                ctx.Results.UserActLiab +
                ctx.Results.UserDefLiab -
                ctx.Results.FRO.LiabilityDischarged -
                ctx.Results.ETV.LiabilityDischarged -
                ctx.Results.EFRO.ChangeInDeferredsLiabilitiesDischarged -
                ctx.Results.EFRO.ChangeInActivesPastLiabilitiesDischarged +
                ctx.Results.UserPenLiab -
                (ctx.Results.PIE.Liability.Before - ctx.Results.PIE.Liability.After) -
                ctx.Results.FutureBenefits.LiabilitiesChange;

            return new BalanceData(assets, liabilities);
        }
    }
}
