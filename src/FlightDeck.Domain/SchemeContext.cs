﻿namespace FlightDeck.Domain
{
    using System.Collections.Generic;
    
    public class SchemeContext : ISchemeContext
    {
        public ISchemeData Data { get; private set; }
        public ISchemeAssumptions Assumptions { get; private set; }
        public ISchemeResults Calculated { get; private set; }
        public IBasesAssumptionsProvider BasesAssumptionsProvider { get; private set; }

        public SchemeContext(ISchemeData data, ISchemeAssumptions assumptions, ISchemeResults calculated, IBasesAssumptionsProvider basesAssumptionsProvider)
        {
            Data = data;
            Assumptions = assumptions;
            Calculated = calculated;
            BasesAssumptionsProvider = basesAssumptionsProvider;
        }
    }
}
