﻿using FlightDeck.Domain.Assets;
using FlightDeck.Domain.Evolution;
using System;

namespace FlightDeck.Domain
{
    /// <summary>
    /// The full state of a basis relevant to most journey plan calculations.
    /// </summary>
    public interface IBasisContext
    {
        /// <summary>
        /// The analysis start date this context applies at;
        /// </summary>
        DateTime StartDate { get; }
        /// <summary>
        /// The analysis date this context applies at - ie, the analysis period end date
        /// </summary>
        DateTime AnalysisDate { get; }
        /// <summary>
        /// The date of the nearest past known Basis data to the Analysis date (ie, date of the relevant Liabs_ worksheet in Tracker)
        /// </summary>
        DateTime EffectiveDate { get; }
        /// <summary>
        /// The nearest past known Basis data to the Analysis date (ie, the data contained in the relevant Liabs_ worksheet in Tracker)
        /// </summary>
        Basis Data { get; set;  }
        /// <summary>
        /// The evolution of the basis from anchor date to refresh date, this doesn't change once calculated.
        /// </summary>
        BasisEvolutionData EvolutionData { get; }
        /// <summary>
        /// Asset portfolio relevant to the Analysis date.
        /// </summary>
        IPortfolio Portfolio { get; }
        /// <summary>
        /// Most calculations rely on the results of previous calculations - this holds them all.
        /// </summary>
        BasisAnalysisResults Results { get; set; }
        /// <summary>
        /// The basis assumptions in place
        /// </summary>
        IBasisAssumptions Assumptions { get; }
    }
}
