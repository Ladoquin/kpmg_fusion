﻿using FlightDeck.DomainShared;
using System;

namespace FlightDeck.Domain
{
    public class LifeExpectancyAssumptions : IEquatable<LifeExpectancyAssumptions>
    {
        public double PensionerLifeExpectancy { get; private set; }
        public double DeferredLifeExpectancy { get; private set; }

        public LifeExpectancyAssumptions(double pensionerLifeExpectancy, double deferredLifeExpectancy)
        {
            PensionerLifeExpectancy = pensionerLifeExpectancy;
            DeferredLifeExpectancy = deferredLifeExpectancy;
        }

        public bool Equals(LifeExpectancyAssumptions other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(PensionerLifeExpectancy, other.PensionerLifeExpectancy)
                && Utils.EqualityCheck(DeferredLifeExpectancy, other.DeferredLifeExpectancy);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((LifeExpectancyAssumptions)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = PensionerLifeExpectancy.GetHashCode();
                hashCode = (hashCode * 397) ^ DeferredLifeExpectancy.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("PensionerLifeExpectancy: {0}, DeferredLifeExpectancy: {1}", PensionerLifeExpectancy, DeferredLifeExpectancy);
        }
    }
}
