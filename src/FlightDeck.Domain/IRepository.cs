﻿using System.Collections.Generic;
using TKey = System.Int32;

namespace FlightDeck.Domain
{
    public interface IRepository<T>
    {
        T GetById(TKey id);
        IEnumerable<T> GetAll();
        TKey Insert(T item);
        bool Update(T item);
        void InsertOrUpdate(T item);
        void ClearCache();
    }
}