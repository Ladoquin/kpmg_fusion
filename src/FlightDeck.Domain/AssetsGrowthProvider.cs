﻿namespace FlightDeck.Domain
{
    using FlightDeck.Domain.Repositories;
    using log4net;
    using System;
    using System.Collections.Generic;

    public class AssetsGrowthProvider
    {
        private readonly IFinancialIndexRepository indexRepository;

        public AssetsGrowthProvider(IFinancialIndexRepository indexRepository)
        {
            this.indexRepository = indexRepository;
        }

        public IEnumerable<KeyValuePair<string, double>> GetInfo(DateTime from, DateTime till, IList<KeyValuePair<string, string>> assetsGrowthIndexItems)
        {
            var growth = new List<KeyValuePair<string, double>>(10);

            foreach (var item in assetsGrowthIndexItems)
            {
                var index = indexRepository.GetByName(item.Value);

                if (index != null)
                {
                    if (index.ContainsValue(till.AddDays(-1)) && index.ContainsValue(from))
                    {
                        growth.Add(new KeyValuePair<string, double>(item.Key, (index.GetValue(till.AddDays(-1)) / index.GetValue(from) - 1)));
                    }
                    else
                    {
                        LogManager.GetLogger(this.GetType()).WarnFormat("Index values for '{0}' not found on {1} and/or {2}", item.Value, till.AddDays(-1).ToString("dd/MM/yyyy"), from.ToString("dd/MM/yyyy"));
                    }
                }
                else
                {
                    LogManager.GetLogger(this.GetType()).WarnFormat("Index '{0}' not found", item.Value);
                }
            }

            return growth;
        }
    }
}
