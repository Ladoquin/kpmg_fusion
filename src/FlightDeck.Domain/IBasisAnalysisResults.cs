﻿namespace FlightDeck.Domain
{
    using System;
    using System.Collections.Generic;
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;

    public interface IBasisAnalysisResults
    {
        BasisAnalysisResultsStickyData StickyResults { get; }

        double AssetsAtSED { get; }
        AttributionData Attribution { get; }
        double BuyInAtAnalysisDate { get; }
        IDictionary<int, IDictionary<MemberStatus, double>> BuyinCashflow { get; }
        IDictionary<int, IDictionary<MemberStatus, double>> Cashflow { get; }
        EtvData ETV { get; }
        FroData FRO { get; }
        FundingData FundingLevel { get; }
        FutureBenefitsData FutureBenefits { get; }
        BuyinData Insurance { get; }
        InvestmentStrategyData InvestmentStrategy { get; }
        HedgeData Hedging { get; }
        IDictionary<int, BalanceData> JourneyPlanAfter { get; }
        IDictionary<int, BalanceData> JourneyPlanBefore { get; }
        double JP2InvGrowth { get; }
        double JPInvGrowth { get; }
        PIEInitData PIEInit { get; }
        PieData PIE { get; }
        ProfitLossData ProfitLoss { get; }
        double RecoveryPlanPaymentRequired { get; }
        IDictionary<int, BeforeAfter<double>> RecoveryPlanPayments { get; }
        double UserActDuration { get; }
        double UserActLiab { get; }
        double UserDefBuyIn { get; }
        double UserDefDuration { get; }
        double UserDefLiab { get; }
        double UserPenBuyIn { get; }
        double UserPenDuration { get; }
        double UserPenLiab { get; }
        double UserServiceCost { get; }
        double UserActLiabAfter { get; }
        double UserDefLiabAfter { get; }
        double UserPenLiabAfter { get; }
        IAS19Disclosure.IAS19PAndLForecastData IAS19 { get; }
        IAS19Disclosure.IAS19PAndLForecastData FRS17 { get; }
        GiltBasisLiabilityData GiltBasisLiabilityData { get; }
        VaRInputs VaRInputs { get; }
    }
}
