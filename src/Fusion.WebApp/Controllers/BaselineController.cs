﻿namespace Fusion.WebApp.Controllers
{
    using FlightDeck.DomainShared;
    using Fusion.WebApp.Models.Attributes;
    using FlightDeck.ServiceInterfaces;
    using Fusion.WebApp.Filters;
    using Fusion.WebApp.Models;
    using Fusion.WebApp.Services;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    [Authorize]
    [SchemeAdmit]
    public class BaselineController : FusionBaseController
    {
        private IViewService _viewService;
        private IViewService viewService
        {
            get
            {
                if (_viewService == null)
                    _viewService = ServiceFactory.GetService<IViewService>(this);
                return _viewService;
            }
        }

        public ActionResult Index()
        {
            return View("Baseline", new SchemeViewModel(Scheme));
        }

        [HttpGet]
        public ActionResult Get()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            EvolutionData currentTrackerItem = null;
            var bases = new List<Hashtable>(3);

            foreach (var basis in scheme.GetAllBases())
            {
                var hash = new Hashtable();

                hash.Add("id", basis.MasterBasisId);
                hash.Add("type", basis.BasisType.ToString());
                hash.Add("name", basis.Name);
                hash.Add("balance", basis.ExpectedDeficit.First().Value.Deficit);
                hash.Add("assumptions", basis.Evolution.First().Value.ToAssumptionData(basis.GetClientLiabilityAssumptions().Before.PensionDiscountRate, basis.GetClientLiabilityAssumptions().Before.DeferredLifeExpectancy));
                hash.Add("active", basis.MasterBasisId == scheme.CurrentBasis.MasterBasisId);
                hash.Add("anchorDate", JsonHelper.ConvertDateToJSON(basis.AnchorDate));

                bases.Add(hash);

                if ((bool)hash["active"])
                {
                    currentTrackerItem = basis.Evolution.Values.First();
                }
            }

            var totalCosts = scheme.CurrentBasis.TotalCostsAtAnchor ?? new TotalCosts(0, 0, 0, 0, 0);
            var liabilitySplit = new List<BreakdownItem>
                {
                    { new BreakdownItem("Active", totalCosts.Active) },
                    { new BreakdownItem("Deferred", totalCosts.Deferred) },
                    { new BreakdownItem("Pensioner", totalCosts.Pension) }
                };

            var assetTotal = scheme.CurrentBasis.Evolution.First().Value.TotalAssetsPlusBuyin;
            var assetInfoAtAnchor = viewService.GetSchemeAssetDataAtAnchor(scheme);

            var pensionIncreases = viewService.GetPensionIncreasesAtAnchor(scheme);

            var b = scheme.CurrentBasis;
            var builtInHedge = scheme.Funded ? scheme.CurrentBasis.GetHedgingDataAtAnchorDate() : null;

            var json = new
            {
                bases = bases.Select(x => new
                {
                    id = x["id"],
                    type = x["type"],
                    name = x["name"],
                    balance = x["balance"],
                    assumptions = x["assumptions"],
                    active = x["active"],
                    anchorDate = x["anchorDate"]
                }),
                current = new
                {
                    basis = new { type = scheme.CurrentBasis.BasisType.ToString(), name = scheme.CurrentBasis.Name },
                    anchorDate = JsonHelper.ConvertDateToJSON(currentTrackerItem.Date),
                    balance = scheme.CurrentBasis.ExpectedDeficit[currentTrackerItem.Date].Deficit,
                    liabilityTotal = liabilitySplit.Sum(i => i.Value),
                    assetTotal = assetTotal,
                    liabilitiesBreakdown = liabilitySplit,
                    assetsBreakdown = assetInfoAtAnchor.Funds,
                    syntheticAssetInfo = assetInfoAtAnchor.SyntheticAssetInfo,
                    assumptions = viewService.GetLiabilityAssumptions(scheme, currentTrackerItem.Date, new LabelService(this).GetDefaultAssumptionLabels(scheme.CurrentBasis)),
                    pensionIncreases = pensionIncreases,
                    namedPropertyGroup = scheme.CurrentBasis.BasisType == BasisType.Accounting ? scheme.NamedProperties[NamedPropertyGroupType.BaselineInfo] : scheme.NamedProperties[NamedPropertyGroupType.ScheduleOfConts]
                },
                ldiData = builtInHedge != null
                    ? new
                    {
                        interestRate = builtInHedge.CurrentBasis[HedgeType.Interest].Total,
                        inflation = builtInHedge.CurrentBasis[HedgeType.Inflation].Total,
                        cashflowInterestRate = builtInHedge.Cashflows[HedgeType.Interest].Total,
                        cashflowInflation = builtInHedge.Cashflows[HedgeType.Inflation].Total
                    }
                    : null
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }
    }
}
