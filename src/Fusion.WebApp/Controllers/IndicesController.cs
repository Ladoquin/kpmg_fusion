﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.WebApp.Filters;
using Fusion.WebApp.Models;
using Fusion.WebApp.Models.Attributes;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Fusion.WebApp.Controllers
{
    [Authorize]
    public class IndicesController : FusionBaseController
    {
        #region services

        private IIndexImportDetailService _indexService;
        public IIndexImportDetailService IndexService 
        { 
            private get 
            { 
                if (_indexService == null) 
                    _indexService = ServiceFactory.GetService<IIndexImportDetailService>(this); 
                return _indexService; 
            }
            set { _indexService = value; }
        }

        private IXmlSchemaValidator _xmlSchemaValidator;
        public IXmlSchemaValidator XmlSchemaValidator
        {
            private get
            {
                if (_xmlSchemaValidator == null)
                    _xmlSchemaValidator = ServiceFactory.GetService<IXmlSchemaValidator>(this);
                return _xmlSchemaValidator;
            }
            set { _xmlSchemaValidator = value; }
        }

        private IIndicesService _indicesService;
        public IIndicesService IndicesService
        {
            private get
            {
                if (_indicesService == null)
                    _indicesService = ServiceFactory.GetService<IIndicesService>(this);
                return _indicesService;
            }
            set { _indicesService = value; }
        }

        private IPreCalculationService _preCalculationService;
        public IPreCalculationService PreCalculationService
        {
            private get
            {
                if (_preCalculationService == null)
                    _preCalculationService = ServiceFactory.GetService<IPreCalculationService>(this);
                return _preCalculationService;
            }
            set { _preCalculationService = value; }
        }

        #endregion

        //
        // GET: /Indicies/Manage
        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [UserAdmit]
        public ActionResult Manage()
        {
            var lastestImportDetail = IndexService.GetLatest();
            return View("Manage", new IndexImportViewModel() { ImportDetail = lastestImportDetail });
        }

        [HttpGet]
        public ActionResult Save()
        {
            return RedirectToAction("Manage");
        }

        [HttpPost]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [ValidateAntiForgeryToken]
        public ActionResult Save(IndexImportViewModel model)
        {
            var logger = LogManager.GetLogger("indices-import");
            var stopwatch = new Stopwatch();

            model.Submitted = true;

            var allowInvalidSubmission = User.IsInRole(RoleType.Admin.ToString());

            // set application state variable
            ControllerContext.HttpContext.Application["IndicesUploadInProgress"] = "true";

            if (ModelState.IsValid)
            {
                logger.InfoFormat("Uploading stats file '{0}'", model.ImportFile.FileName);
                stopwatch.Start();

                var stats = new BinaryReader(model.ImportFile.InputStream).ReadBytes((int)model.ImportFile.InputStream.Length);

                stopwatch.Stop();
                logger.InfoFormat("Uploaded stats file, time taken: {0} seconds", stopwatch.Elapsed.TotalSeconds.ToString());

                using (var xsd = Assembly.GetExecutingAssembly().GetManifestResourceStream("Fusion.WebApp.Resources.stats.xsd"))
                {
                    logger.InfoFormat("Validate xml");

                    var xmlok = XmlSchemaValidator.Validate(new MemoryStream(stats), xsd);

                    //todo: validate here that only core indices make up any custom indices.

                    if (xmlok)
                    {
                        logger.InfoFormat("Validation ok");

                        model.ImportDetail = save(model, stats);
                    }
                    else
                    {
                        logger.InfoFormat("Validation failed");

                        ModelState.AddModelError("ImportFile", "Xml not valid");

                        model.ValidationMessages = XmlSchemaValidator.Messages;

                        XmlSchemaValidator.Messages.ForEach(m => logger.InfoFormat("Import file '{0}' validation message: {1}", model.ImportFile.FileName, m));

                        if (allowInvalidSubmission)
                        {
                            TempData["stats"] = stats;
                            TempData["model"] = model;
                        }
                    }
                }
            }

            if (ModelState.IsValid)
            {
                model.Result = ResultEnum.Success;
            }
            else
            {
                var lastestImportDetail = IndexService.GetLatest();
                model.Result = ResultEnum.Failure;
                model.ImportDetail = lastestImportDetail;
                model.AllowInvalidSubmission = allowInvalidSubmission;
            }

            // reset application state variable
            ControllerContext.HttpContext.Application["IndicesUploadInProgress"] = "false";

            return View("Manage", model);
        }

        /// <summary>
        /// Ignore this ForceSave overload, it is only used for handling a GET request to the POST url, ie, in the case of a forms authentication timed-out redirect.
        /// </summary>
        [HttpGet]
        public ActionResult ForceSave()
        {
            return RedirectToAction("Manage");
        }

        /// <summary>
        /// Use this ForceSave overload to attempt the save of the stats upload.
        /// </summary>
        /// <param name="form">Ignore, pass null if necessary, this argument is only being used to distinguish this method overload from the HttpGet one.</param>
        [HttpPost]
        [FusionAuthorize(RoleType.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult ForceSave(object form)
        {
            if (TempData.ContainsKey("stats"))
            {
                var stats = TempData["stats"] as byte[];
                var model = TempData["model"] as IndexImportViewModel;

                model.ImportDetail = save(model, stats);
                model.Result = ModelState.IsValid ? ResultEnum.Success : model.Result = ResultEnum.Failure;

                return View("Manage", model);
            }

            return RedirectToAction("Manage");
        }

        private IndexImportDetail save(IndexImportViewModel model, byte[] stats)
        {
            var ok = false;
            var importDetail = new IndexImportDetail();
            var xdoc = new XDocument();
            var logger = LogManager.GetLogger("indices-import");
            var stopwatch = new Stopwatch();

            model.ValidationMessages = new List<string>();

            try
            {
                logger.Info("Load stats stream");
                var xml = new MemoryStream(stats);

                logger.Info("Load xml doc");
                xdoc = XDocument.Load(xml);

                logger.Info("Importing stats file to staging table");
                stopwatch.Start();

                IndexService.ImportIndices(xdoc);

                stopwatch.Stop();
                logger.InfoFormat("Imported stats file '{0}', time taken: {1} seconds", model.ImportFile.FileName, stopwatch.Elapsed.TotalSeconds.ToString());

                try
                {
                    logger.Info("Clear precalculated calc results down to earliest index change date");
                    stopwatch.Reset();
                    stopwatch.Start();
                    PreCalculationService.ClearExistingToEarliestUpdate(IndexService.IndexEarliestUpdateDate);

                    stopwatch.Stop();
                    logger.InfoFormat("Cache cleanup time taken: {0} seconds", stopwatch.Elapsed.TotalSeconds.ToString());
                }
                catch (Exception e)
                {
                    //we don't want cache clean up problems to stop the stats upload
                    logger.Error("Error on cache clean up. Fix this to re-enable the performance benefits of using cached calculation results. " + e.ToString());

                    //we have to clear all the caches as its possible that historic indices changes won't be used in new calcs
                    logger.Info("Clearing all result caches");
                    PreCalculationService.ClearExisting();
                }

                ok = true;
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ImportFile", "Failures during import");
                logger.Error(string.Format("Import stats failed, file '{0}'", model.ImportFile.FileName), ex);
            }

            if (ok)
            {
                try
                {
                    logger.Info("Update import details");
                    importDetail = IndexService.Save(xdoc, User);

                    if (AppSettings.IndexFillEndGap)
                    {
                        logger.Info("Filling index end gaps");
                        IndexService.FillEndGap(importDetail.IndexImportDetailId);
                    }

                    logger.Info("Refreshing current scheme");
                    RefreshScheme();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("ImportFile", "Failures saving audit trail");
                    logger.Error("Import stats audit trail failed to save", ex);
                }
            }
            else
            {
                importDetail = IndexService.GetLatest();
            }

            logger.Info("Index import complete");
            return importDetail;
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        public ActionResult ViewAll()
        {
            var indexSummaries = IndicesService.GetAllSummaries();

            return View(indexSummaries);
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        public ActionResult ViewAllIndexValues(int id)
        {
            var values = IndicesService.GetIndexValues(id);

            var html = new StringBuilder(5000);
            html.Append("<table>");
            foreach (var date in values.Keys)
            {
                html.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", ((DateTime)date).ToString("yyyy-MM-dd"), values[date]);
            }
            html.Append("</table>");

            return Content(html.ToString());
        }
    }
}
