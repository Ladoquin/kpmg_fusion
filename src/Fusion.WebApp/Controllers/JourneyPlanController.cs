﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using FlightDeck.ServiceInterfaces.Reports;
using Fusion.WebApp.Filters;
using Fusion.WebApp.Models;
using Fusion.WebApp.Models.Attributes;
using Fusion.WebApp.RequestObjects;
using Fusion.WebApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace Fusion.WebApp.Controllers
{
    [FusionAuthorize(RoleType.Admin, RoleType.Client, RoleType.Consultant, RoleType.Installer)]
    [SchemeAdmit]
    public class JourneyPlanController : FusionBaseController
    {
        const int DefaultYearsToView = 40;

        public ActionResult Index()
        {
            if (!Scheme.PensionScheme.Funded)
            {
                return RedirectToRoute("Home");
            }

            if (Scheme.PensionScheme.GetClientJourneyPlanOptions().After == null) //JP failed to load
            {
                return RedirectToAction("Index", "Error");
            }

            return View("JourneyPlan", new SchemeViewModel(Scheme));
        }

        public ActionResult IncludeBuyInsControl()
        {
            return PartialView("_IncludeBuyInsControl");
        }

        public ActionResult RecoveryPlanControls()
        {
            return PartialView("_RecoveryPlanControls");
        }

        public ActionResult BasisControls()
        {
            return PartialView("_BasisControls", new SchemeViewModel(Scheme));
        }

        public ActionResult TriggerControls()
        {
            return PartialView("_TriggerControls", new SchemeViewModel(Scheme));
        }

        public ActionResult AssetGrowthControls()
        {
            return PartialView("_AssetGrowthControls", new SchemeViewModel(Scheme));
        }

        public ActionResult SelfSufficiencyControls()
        {
            return PartialView("_SelfSufficiencyControls", new SchemeViewModel(Scheme));
        }

        public ActionResult Assumptions()
        {
            return PartialView("_Assumptions");
        }

        public JsonResult SetJourneyPlanData(JourneyPlanRequest journeyPlanRequest) //can't use raw JourneyPlanOptions without Newtonsoft as default serialiser...
        {
            var buyOutBasis = Scheme.PensionScheme.GetAllBases().Where(b => b.BasisType == BasisType.Buyout).FirstOrDefault();

            var journeyPlanOptions = new JourneyPlanOptions
                 {
                     BuyoutBasisKey = buyOutBasis == null ? -1 : buyOutBasis.MasterBasisId,
                     RecoveryPlan = new NewJPExtraContributionOptions(journeyPlanRequest.StartYear, journeyPlanRequest.AnnualContributions,
                     journeyPlanRequest.Term, 0, journeyPlanRequest.AddCurrentRecoveryPlan, journeyPlanRequest.AddExtraContributions),
                     SalaryInflationType = SalaryType.CPI,
                     SelfSufficiencyBasisKey = Scheme.PensionScheme.CurrentBasis.MasterBasisId,
                     FundingBasisKey = journeyPlanRequest.TechnicalProvisionsBasisId,
                     TriggerSteps = journeyPlanRequest.NumberOfSteps,
                     FundingLevel = journeyPlanRequest.FundingLevel,
                     CurrentReturn = journeyPlanRequest.CurrentReturn,
                     InitialReturn = journeyPlanRequest.InitialReturn,
                     FinalReturn = journeyPlanRequest.FinalReturn,
                     FundingLevelTrigger = journeyPlanRequest.FundingLevelTrigger,
                     TriggerTimeStartYear = journeyPlanRequest.TriggerTimeStartYear,
                     TriggerTransitionPeriod = journeyPlanRequest.TriggerTimeEndYear,
                     SelfSufficiencyDiscountRate = journeyPlanRequest.DiscountRate,
                     UseCurves = false,
                     IncludeBuyIns = journeyPlanRequest.IncludeBuyIns
                 };

            SetClientState(journeyPlanRequest);
            
            Scheme.PensionScheme.SetClientJourneyPlanOptions(journeyPlanOptions);

            var jp = Scheme.PensionScheme.GetNewJP();

            return buildJourneyPlanResponse(jp);
        }

        public void SetClientState(JourneyPlanRequest journeyPlanRequest)
        {
            //todo: use session manager?
            Session["JourneyPlanDisplayOptions"] = new
            {
                ShowCurrentAssets = journeyPlanRequest.ShowCurrentAssets,
                ShowNewAssets = journeyPlanRequest.ShowNewAssets,
                ShowFunding = journeyPlanRequest.ShowFunding,
                ShowSelfSufficiency = journeyPlanRequest.ShowSelfSufficiency,
                ShowBuyOut = journeyPlanRequest.ShowBuyOut,
                YearsToView = journeyPlanRequest.YearsToView
            };
        }

        public JsonResult NewJourneyPlanData()
        {
            return buildJourneyPlanResponse(Scheme.PensionScheme.GetNewJP());
        }

        public JsonResult ResetJourneyPlanData()
        {
            Scheme.PensionScheme.ResetClientJourneyPlanOptions();
            new SessionManagerService(this).ResetJourneyPlanDisplayOptions();
            return buildJourneyPlanResponse(Scheme.PensionScheme.GetNewJP());
        }

        public JsonResult GetExistingAssumptions()
        {
            if (Scheme.PensionScheme.GetClientJourneyPlanOptions().After == null)
                return errorResponse();

            var clientRpAssumptions = Scheme.PensionScheme.CurrentBasis.GetClientRecoveryAssumptions().After;
            var type = Scheme.PensionScheme.GetClientRecoveryPlanOptions().After == RecoveryPlanType.Current ? "current" : "new";
            var currentRP = type == "current" ? Scheme.PensionScheme.CurrentBasis.GetRecoveryPlan().Before : Scheme.PensionScheme.CurrentBasis.GetRecoveryPlan().After;
            var yearOneConts = (currentRP != null && currentRP.Count() > 0) ? currentRP[0] : 0;
            var lastPaymentDate = Scheme.PensionScheme.ContributionsEndDate;
            var rpAssumptions = new {
                                     type,
                                     length = clientRpAssumptions.Term,
                                     increase = clientRpAssumptions.Increases,
                                     lumpSumAmount = clientRpAssumptions.LumpSumPayment,
                                     commencement = clientRpAssumptions.StartMonth,
                                     firstYearCont = yearOneConts,
                                     lastPaymentDate = lastPaymentDate == null ? null : (double?)(lastPaymentDate.Value - JsonHelper.UnixEpoch).TotalMilliseconds
                                 };

            // on UI, technical prov basis is called Funding basis (second dropdown on new JP page), so get selected tech prov basis from client options
            var techProvBasisId = Scheme.PensionScheme.GetClientJourneyPlanOptions().After.FundingBasisKey;
            var fundingBasis = Scheme.PensionScheme.GetBasis(techProvBasisId);

            var buyoutBasisId = Scheme.PensionScheme.GetBasesIdentifiers().Where(x => x.Type == BasisType.Buyout).FirstOrDefault().Id;
            var buyoutBasis = Scheme.PensionScheme.GetBasis(buyoutBasisId);

            var giltYieldLabel = Scheme.PensionScheme.NamedProperties.ContainsKey(NamedPropertyGroupType.RefGiltYeild)
                        ? Scheme.PensionScheme.NamedProperties[NamedPropertyGroupType.RefGiltYeild].Properties.First(x => x.Key == NamedRefGiltYieldPropertyTypes.DisplayName.ToString()).Value
                        : null;

            Func<IBasis, AssumptionData> getLiabAssumptions = basis => 
            {
                if (basis == null)
                    return null;

                var assumptions = basis.GetClientLiabilityAssumptions();
                if (assumptions != null)
                    return assumptions.After;
                else
                {
                    if (!basis.Evolution.ContainsKey(Scheme.PensionScheme.CurrentBasis.AttributionEnd))
                        return null;
                    var evolutionData = basis.Evolution[Scheme.PensionScheme.CurrentBasis.AttributionEnd];
                    AssumptionData liabAssumptions = new AssumptionData(evolutionData.PreRetirementDiscountRate, evolutionData.PostRetirementDiscountRate,
                                                        evolutionData.PensionerDiscountRate, 0, 0, 0, 0, 0);
                    return liabAssumptions;
                }
            };

            var fundingBasisAssumptions = fundingBasis != null ? getLiabAssumptions(fundingBasis) : null;
            var buyoutBasisAssumptions = buyoutBasis != null ? getLiabAssumptions(buyoutBasis) : null;
            var fundingGiltYield = fundingBasis != null ? fundingBasis.GetGiltYield() : 0;
            var buyoutGiltYield = buyoutBasis != null ? buyoutBasis.GetGiltYield() : 0;

            return Json(new
                {
                    fundingBasisAssumptions = fundingBasisAssumptions != null ? new {
                                                preDiscountRate = fundingBasisAssumptions.PreRetirementDiscountRate,
                                                postDiscountRate = fundingBasisAssumptions.PostRetirementDiscountRate,
                                                pensionerDiscountRate = fundingBasisAssumptions.PensionDiscountRate,
                                                giltYield = fundingGiltYield
                                              }
                                              : null,
                    buyoutBasisAssumptions = buyoutBasisAssumptions != null ? new {
                                                preDiscountRate = buyoutBasisAssumptions.PreRetirementDiscountRate,
                                                postDiscountRate = buyoutBasisAssumptions.PostRetirementDiscountRate,
                                                pensionerDiscountRate = buyoutBasisAssumptions.PensionDiscountRate,
                                                giltYield = buyoutGiltYield
                                             }
                                             : null,
                    giltYieldLabel = giltYieldLabel,
                    recoveryPlanAssumptions = rpAssumptions
                },
                JsonRequestBehavior.AllowGet);
        }

        private JsonResult errorResponse()
        {
            return Json(
                new
                {
                    jpLoadError = true //todo: expand with more error details eg. 'missing tp basis at analysis date'
                }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult buildJourneyPlanResponse(NewJPData data)
        {
            if (Scheme.PensionScheme.GetClientJourneyPlanOptions().Before == null)
                return errorResponse();

            SchemeViewModel sch = new SchemeViewModel(Scheme);

            dynamic displayOptions = Session["JourneyPlanDisplayOptions"];

            var yearsToView = displayOptions == null ? DefaultYearsToView : (int)displayOptions.YearsToView;

            var scheme = Scheme.PensionScheme;
            var options = scheme.GetClientJourneyPlanOptions().After;
            Func<IDictionary<int, double>, long[]> toDataSeries = d => d.Values.Select(x => (long)Math.Ceiling(x)).Take(yearsToView + 1).ToArray();
            var series = new List<seriesData>(5);
            if (data.Assets1 != null)
                series.Add(new seriesData { displayName = "Current assets", colour = "#E27320", dataPoints = toDataSeries(data.Assets1), isHidden = true });
            if (data.Assets2 != null)
                series.Add(new seriesData { displayName = "New assets", colour = "#E27320", dashedLine = true, dataPoints = toDataSeries(data.Assets2) });              
            if (data.Liab1 != null)
                series.Add(new seriesData { displayName = "Funding", colour = "#0f7bb9", dataPoints = toDataSeries(data.Liab1) });
            if (data.Liab2 != null)
                series.Add(new seriesData { displayName = "Self sufficiency", colour = "#7EB91E", dataPoints = toDataSeries(data.Liab2) });
            if (data.Liab3 != null)
                series.Add(new seriesData { displayName = "Buy-out", colour = "#8A208A", dataPoints = toDataSeries(data.Liab3), isHidden = true, isDisabled = true });

            var lumpSumMax = Scheme.PensionScheme.CurrentBasis.GetClientRecoveryAssumptions().Before.MaxLumpSumPayment; 

            return Json(
                new
                {
                    xAxisCategories = data.Liab1.Keys,
                    series,
                    crossOverSeriesItem = 3,    // assets in our example

                    newJourneyPlan = new
                    {
                        includeBuyIns = options.IncludeBuyIns,
                        technicalProvisionsBasisId = options.FundingBasisKey,
                        effectiveDate = scheme.CurrentBasis.AttributionEnd,
                        selfSufficiencyData = new
                        {
                            discountRate = new { val = options.SelfSufficiencyDiscountRate, max = 0.015, original = scheme.GetClientJourneyPlanOptions().Before.SelfSufficiencyDiscountRate }
                        },
                        assetGrowthData = new
                        {
                            currentReturn = new { val = options.CurrentReturn, min = 0, max = 0.05, original = scheme.GetClientJourneyPlanOptions().Before.CurrentReturn }
                        },
                        triggerData = new
                        {
                            fundingLevelTrigger = options.FundingLevelTrigger,
                            initialReturnAssets = new { val = options.InitialReturn, min = 0, max = 0.05, original = scheme.GetClientJourneyPlanOptions().Before.InitialReturn },
                            finalReturnAssets = new { val = options.FinalReturn, min = 0, max = 0.05, original = scheme.GetClientJourneyPlanOptions().Before.FinalReturn },
                            fundingLevel = new { val = options.FundingLevel, min = 0.8, max = 1.2, original = scheme.GetClientJourneyPlanOptions().Before.FundingLevel },
                            numberOfSteps = new { val = options.TriggerSteps, min = 0, max = 10, original = scheme.GetClientJourneyPlanOptions().Before.TriggerSteps },
                            triggerTimeStartYear = new { val = options.TriggerTimeStartYear, min = 1, max = Utils.MaxJourneyPlanYear, original = scheme.GetClientJourneyPlanOptions().Before.TriggerTimeStartYear },
                            triggerTimeEndYear = new { val = options.TriggerTransitionPeriod, min = 1, max = Utils.MaxJourneyPlanYear, original = scheme.GetClientJourneyPlanOptions().Before.TriggerTransitionPeriod },
                            triggerYears = data.TriggerYears
                        },
                        recoveryPlanData = new
                        {
                            startYear = new { val = options.RecoveryPlan.StartYear, max = 20, original = scheme.GetClientJourneyPlanOptions().Before.RecoveryPlan.StartYear },
                            annualCont = new { val = options.RecoveryPlan.AnnualContributions, max = lumpSumMax, original = scheme.GetClientJourneyPlanOptions().Before.RecoveryPlan.AnnualContributions },
                            term = new { val = options.RecoveryPlan.Term, max = 20, original = scheme.GetClientJourneyPlanOptions().Before.RecoveryPlan.Term },
                            annualInc = new { val = options.RecoveryPlan.AnnualIncrements, max = 1, original = scheme.GetClientJourneyPlanOptions().Before.RecoveryPlan.AnnualIncrements },
                            addExtraContributions = options.RecoveryPlan.AddExtraContributions,
                            addCurrentRecoveryPlan = options.RecoveryPlan.AddCurrentRecoveryPlan
                        },
                        crossOverPoints = new
                        {
                            tpca = getCrossOverPoint(data.CurrentAssetsCrossOverPoints, BasisType.TechnicalProvision),
                            ssca = getCrossOverPoint(data.CurrentAssetsCrossOverPoints, BasisType.SelfSufficiency),
                            boca = getCrossOverPoint(data.CurrentAssetsCrossOverPoints, BasisType.Buyout),
                            tpna = getCrossOverPoint(data.NewAssetsCrossOverPoints, BasisType.TechnicalProvision),
                            ssna = getCrossOverPoint(data.NewAssetsCrossOverPoints, BasisType.SelfSufficiency),
                            bona = getCrossOverPoint(data.NewAssetsCrossOverPoints, BasisType.Buyout)
                        }
                    },
                    dates = new
                        {
                            val = getDateJSON(scheme.CurrentBasis.AttributionEnd),
                            min = getDateJSON(scheme.CurrentBasis.AnchorDate),
                            max = getDateJSON(scheme.RefreshDate)
                        },
                    displayOptions = new
                    {
                        showCurrentAssets = displayOptions == null ? true : displayOptions.ShowCurrentAssets,
                        showNewAssets = displayOptions == null ? false : displayOptions.ShowNewAssets,
                        showFunding = displayOptions == null ? true : displayOptions.ShowFunding,
                        showSelfSufficiency = displayOptions == null ? false : displayOptions.ShowSelfSufficiency,
                        showBuyOut = displayOptions == null ? false : displayOptions.ShowBuyOut,
                        yearsToView = yearsToView
                    }
                },
                JsonRequestBehavior.AllowGet);
        }

        private int getCrossOverPoint(IDictionary<BasisType, int> crossOverCollection, BasisType basisType)
        {
            if (crossOverCollection == null || !crossOverCollection.ContainsKey(basisType))
                return -1;

            return crossOverCollection[basisType];
        }

        private double getDateJSON(DateTime date)
        {
            DateTime unixEpoch = new DateTime(1970, 1, 1);
            return (date - unixEpoch).TotalMilliseconds;
        }

        class seriesData
        {
            public string displayName;
            public long[] dataPoints;
            public bool dashedLine;
            public string colour;
            public bool isHidden;
            public bool isDisabled;
        }
    }
}
