using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.WebApp.Filters;
using Fusion.WebApp.Models;
using Fusion.WebApp.Models.Attributes;
using Fusion.WebApp.Services;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web.Caching;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace Fusion.WebApp.Controllers
{
    [FusionAuthorize(RoleType.Admin, RoleType.Client, RoleType.Consultant, RoleType.Installer)]
    [AjaxAuthorize]
    public class JsonController : FusionBaseController
    {
        private IViewService _viewService;
        private IViewService viewService
        {
            get
            {
                if (_viewService == null)
                    _viewService = ServiceFactory.GetService<IViewService>(this);
                return _viewService;
            }
        }

        private IClientAssumptionService _clientAssumptionService;
        private IClientAssumptionService clientAssumptionService
        {
            get
            {
                if (_clientAssumptionService == null)
                    _clientAssumptionService = ServiceFactory.GetService<IClientAssumptionService>(this);
                return _clientAssumptionService;
            }
        }

        private IUserProfileService _userProfileService;
        private IUserProfileService userProfileService
        {
            get
            {
                if (_userProfileService == null)
                    _userProfileService = ServiceFactory.GetService<IUserProfileService>(this);
                return _userProfileService;
            }
        }

        private PanelState getPanelState()
        {
            var scheme = Scheme.PensionScheme;
            var hasBuyoutScheme = scheme.HasBasisOfType(BasisType.Buyout);
            var insuranceIsDirty = false;
            if (hasBuyoutScheme)
                insuranceIsDirty = scheme.GetClientInsuranceParameters().IsDirty;

            return new PanelState
                {
                    liabilityAssumptions = scheme.CurrentBasis.GetClientLiabilityAssumptions().IsDirty,
                    pensionIncreaseAssumptions = scheme.CurrentBasis.GetClientPensionIncreases().IsDirty,
                    assetAssumptions = scheme.CurrentBasis.GetClientAssetAssumptions().IsDirty,
                    recoveryPlan = scheme.CurrentBasis.GetClientRecoveryAssumptions().IsDirty,
                    investmentStrategy = scheme.GetClientInvestmentStrategyAllocations().IsDirty,
                    investmentStrategyOptions = scheme.GetClientInvestmentStrategyOptions().IsDirty,
                    FRO = scheme.GetClientFroType().IsDirty || scheme.GetClientFroOptions().IsDirty || scheme.GetClientEFroOptions().IsDirty,
                    ETV = scheme.GetClientEtvOptions().IsDirty,
                    PIE = scheme.GetClientPieOptions().IsDirty,
                    futureBenefits = scheme.GetClientFutureBenefitsParameters().IsDirty,
                    ABF = scheme.GetClientAbfOptions().IsDirty,
                    insurance = insuranceIsDirty
                };
        }

        private enum RequestType
        {
            VarFunnel,
            VarWaterfall,
            CashLens,
            AccountingData,
            JourneyPlan,
            Cashflow,
        }

        private bool isDataDirty(RequestType type)
        {
            var panelState = getPanelState();

            //TODO add more rules? only rule implemented so far is that if you change investment strategy then the Cash Lens doesn't change
            var isdirty =
                panelState.liabilityAssumptions ||
                panelState.pensionIncreaseAssumptions ||
                panelState.assetAssumptions ||
                panelState.FRO ||
                panelState.recoveryPlan ||
                panelState.ETV ||
                panelState.PIE ||
                panelState.insurance ||
                panelState.futureBenefits ||
                panelState.ABF ||
                panelState.investmentStrategyOptions;

            if (type != RequestType.CashLens)
            {
                isdirty = isdirty || panelState.investmentStrategy;
            }

            return isdirty;
        }

        public ActionResult PanelState()
        {
            if (Scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            return Json(getPanelState(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAnalysisAssumptions(string key)
        {
            var id = clientAssumptionService.SaveCurrentOptions(key, Scheme, User);

            var json = new { id };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult LoadAnalysisAssumptions(int id)
        {
            var scheme = Scheme;

            clientAssumptionService.RestoreOptionsToScheme(id, scheme);

            // don't currently have any backend validation on inputs, so take care of old and possibly now outdated scenario values here

            var efro = scheme.PensionScheme.GetClientEFroOptions().After;
            if (efro.AnnualTakeUpRate > 0 && efro.EmbeddedFroBasisChange < 0.5)
                scheme.PensionScheme.SetClientEFroOptions(new EmbeddedFlexibleReturnOptions(efro.AnnualTakeUpRate, 0.5));

            var json = getBasisForAnalysisJSON(scheme.PensionScheme);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteAnalysisAssumptions(int id)
        {
            clientAssumptionService.DeleteSavedOptions(id, Scheme);

            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Liabilities()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = liabilities();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object liabilities()
        {
            var scheme = Scheme.PensionScheme;

            var labels = new LabelService(this).GetDefaultAssumptionLabels(scheme.CurrentBasis);
            var visibleAssumptions = viewService.GetLiabilityAssumptionsCustom(scheme, labels);

            var pensionIncreases = viewService.GetPensionIncreasesCustom(scheme);

            return new
            {
                date = JsonHelper.ConvertDateToJSON(scheme.CurrentBasis.AttributionEnd),
                basis = JsonHelper.ConvertBasisToJSON(scheme.CurrentBasis),
                basisBondYield = JsonHelper.ConvertBondYieldToJson(scheme.CurrentBasis),
                liabilitiesBreakdown = viewService.GetLiabilitiesCustomSplit(scheme),
                assumptions = visibleAssumptions,
                pensionIncreases = pensionIncreases.Keys.Select(x => new
                    {
                        x.Key,
                        x.Label,
                        Value = new
                            {
                                pensionIncreases[x].Before,
                                pensionIncreases[x].After
                            },
                        x.IsFixed,
                        x.IsVisible
                    }),
                pensionIncreasesActive = scheme.CurrentBasis.GetClientPensionIncreases().IsDirty,
                hiddenAssumptions = getHiddenAssumptions(visibleAssumptions)
            };
        }

        private IEnumerable<KeyLabelValue<string, double>> getHiddenAssumptions(IEnumerable<KeyLabelValue<string, BeforeAfter<double>>> assumptions)
        {
            // check all available liability assumptions (before)
            // if any of these doesn't exists in visible assumptions, add missing ones 
            List<string> visibleAssumptions = assumptions.Select(x => x.Key.ToLower()).ToList();
            List<string> allAssumptions = new List<string>() { "PreRetirementDiscountRate", "", "", "", "", "" };
            List<KeyLabelValue<string, double>> hiddenAssumptions = new List<KeyLabelValue<string, double>>();

            var beforeAssumptions = Scheme.PensionScheme.CurrentBasis.GetClientLiabilityAssumptions().Before;
            if (!visibleAssumptions.Contains("preretirementdiscountrate"))
                hiddenAssumptions.Add(new KeyLabelValue<string, double>("PreRetirementDiscountRate", "PreRetirementDiscountRate", beforeAssumptions.PreRetirementDiscountRate));

            if (!visibleAssumptions.Contains("postretirementdiscountrate"))
                hiddenAssumptions.Add(new KeyLabelValue<string, double>("PostRetirementDiscountRate", "PostRetirementDiscountRate", beforeAssumptions.PostRetirementDiscountRate));

            if (!visibleAssumptions.Contains("pensiondiscountrate"))
                hiddenAssumptions.Add(new KeyLabelValue<string, double>("PensionDiscountRate", "PensionDiscountRate", beforeAssumptions.PensionDiscountRate));

            if (!visibleAssumptions.Contains("salaryincrease"))
                hiddenAssumptions.Add(new KeyLabelValue<string, double>("SalaryIncrease", "SalaryIncrease", beforeAssumptions.SalaryIncrease));

            if (!visibleAssumptions.Contains("inflationretailpriceindex"))
                hiddenAssumptions.Add(new KeyLabelValue<string, double>("InflationRetailPriceIndex", "InflationRetailPriceIndex", beforeAssumptions.InflationRetailPriceIndex));

            if (!visibleAssumptions.Contains("inflationconsumerpriceindex"))
                hiddenAssumptions.Add(new KeyLabelValue<string, double>("InflationConsumerPriceIndex", "InflationConsumerPriceIndex", beforeAssumptions.InflationConsumerPriceIndex));

            return hiddenAssumptions;
        }

        [HttpGet]
        public ActionResult Assets()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = assets();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object assets()
        {
            var scheme = Scheme.PensionScheme;

            var assetsBreakdown = viewService.GetAssetsBreakdown(scheme, AssumptionsSourceType.Client);

            var expReturns = scheme.CurrentBasis.GetClientAssetAssumptions();
            var assetReturns = scheme.CurrentBasis.GetClientAssetAssumptions().Before
                .Select(x => new AssetReturnItem((int)x.Key, x.Key.DisplayName(),
                    new BeforeAfter<double>(expReturns.Before[x.Key], expReturns.After[x.Key]))
                    {
                        UserMin = scheme.AssetDefinitions.ContainsKey(x.Key) ? scheme.AssetDefinitions[x.Key].ReturnSpec.Min : AssetDefaults.DefaultAssetReturnSpec.Min,
                        UserMax = scheme.AssetDefinitions.ContainsKey(x.Key) ? scheme.AssetDefinitions[x.Key].ReturnSpec.Max : AssetDefaults.DefaultAssetReturnSpec.Max,
                        UserIncrement = scheme.AssetDefinitions.ContainsKey(x.Key) ? scheme.AssetDefinitions[x.Key].ReturnSpec.Increment : AssetDefaults.DefaultAssetReturnSpec.Increment,
                    });
            var assetsInfo = viewService.GetSchemeAssetData(scheme, AssumptionsSourceType.Client);

            var ldiData = scheme.Funded ? scheme.CurrentBasis.GetHedgingBreakdown().After : null;

            return new
                {
                    date = JsonHelper.ConvertDateToJSON(scheme.CurrentBasis.AttributionEnd),
                    assetsBreakdown = assetsInfo.Funds,
                    syntheticAssetInfo = assetsInfo.SyntheticAssetInfo,
                    assetReturns = assetReturns,
                    weightedAssetReturn = viewService.GetWeightedAssetReturn(scheme),
                    ldiData = ldiData == null ? null : new
                    {
                        interestRate = ldiData.CurrentBasis[HedgeType.Interest].Total,
                        inflation = ldiData.CurrentBasis[HedgeType.Inflation].Total,
                        cashflowInterestRate = ldiData.Cashflows[HedgeType.Interest].Total,
                        cashflowInflation = ldiData.Cashflows[HedgeType.Inflation].Total
                    }
                };
        }

        [HttpGet]
        public ActionResult Fro()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = fro();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object fro()
        {
            var scheme = Scheme.PensionScheme;

            var froType = scheme.GetClientFroType().After;

            // :) :)
            var aFro = scheme.CurrentBasis.GetFroData().After;
            var aEFro = scheme.CurrentBasis.GetEFroData().After;
            var nonPenLiab = aEFro.NonPensionersLiabilities;

            if (nonPenLiab <= 0)
            {
                var liabs = scheme.CurrentBasis.GetLiabilityData(AttributionEndType.RefreshDate);
                nonPenLiab = liabs.ActiveFuture + liabs.ActivePast + liabs.Deferred;
            }

            var eFroData = new
            {
                setup = new
                {
                    scheme.GetClientEFroOptions().Before.EmbeddedFroBasisChange,
                    scheme.GetClientEFroOptions().Before.AnnualTakeUpRate,
                    scheme.AnalysisParameters.FROMinTransferValue,
                    scheme.AnalysisParameters.FROMaxTransferValue
                },
                aEFro.CetvPaid,
                aEFro.Improvement,
                aEFro.LiabilityDischarged,
                NonPensionersLiabilities = nonPenLiab,
                scheme.GetClientEFroOptions().After.EmbeddedFroBasisChange,
                scheme.GetClientEFroOptions().After.AnnualTakeUpRate,
            };

            return new
            {
                froType,
                setup = new
                {
                    scheme.GetClientFroOptions().Before.EquivalentTansferValue,
                    scheme.GetClientFroOptions().Before.TakeUpRate,
                    scheme.AnalysisParameters.FROMinTransferValue,
                    scheme.AnalysisParameters.FROMaxTransferValue
                },
                aFro.CetvPaid,
                aFro.SizeOfTV,
                aFro.Improvement,
                aFro.LiabilityDischarged,
                aFro.Over55,
                aFro.Under55,
                scheme.GetClientFroOptions().After.EquivalentTansferValue,
                scheme.GetClientFroOptions().After.TakeUpRate,
                eFroData,
                isBuyoutBasis = scheme.CurrentBasis.BasisType == BasisType.Buyout
            };
        }

        public ActionResult SetFro(string froData)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var froDataMap = JsonConvert.DeserializeObject<Dictionary<string, double>>(froData);
            scheme.SetClientFroType(FROType.Bulk);
            scheme.SetClientFroOptions(new FlexibleReturnOptions(froDataMap["equivalentTansferValue"], froDataMap["takeUpRate"]));
            return Fro();
        }

        public ActionResult SetEFro(string eFroData)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var eFroDataMap = JsonConvert.DeserializeObject<Dictionary<string, double>>(eFroData);

            scheme.SetClientFroType(FROType.Embedded);
            scheme.SetClientEFroOptions(new EmbeddedFlexibleReturnOptions(eFroDataMap["takeUpRate"], eFroDataMap["equivalentTansferValue"]));
            return Fro();
        }

        public ActionResult ResetFro()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);
            scheme.ResetClientFroType();
            scheme.ResetClientFroOptions();
            return Fro();
        }

        public ActionResult ResetEFro()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);
            scheme.ResetClientEFroOptions();
            return Fro();
        }

        public ActionResult Etv()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = etv();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object etv()
        {
            var scheme = Scheme.PensionScheme;

            var aetv = scheme.CurrentBasis.GetEtvData().After;

            return new
            {
                setup = new
                {
                    scheme.GetClientEtvOptions().Before.EquivalentTransferValue,
                    scheme.GetClientEtvOptions().Before.EnhancementToCetv,
                    scheme.GetClientEtvOptions().Before.TakeUpRate,
                    scheme.AnalysisParameters.ETVMinTransferValue,
                    scheme.AnalysisParameters.ETVMaxTransferValue
                },
                aetv.DeferredLiabilities,
                aetv.CetvSize,
                aetv.EquivalentEtv,
                aetv.CetvPaid,
                aetv.EtvPaid,
                aetv.LiabilityDischarged,
                aetv.CostToCompany,
                aetv.ChangeInPosition,
                scheme.GetClientEtvOptions().After.EquivalentTransferValue,
                scheme.GetClientEtvOptions().After.EnhancementToCetv,
                scheme.GetClientEtvOptions().After.TakeUpRate
            };
        }

        public ActionResult SetEtv(string etvData)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var etvDataMap = JsonConvert.DeserializeObject<Dictionary<string, double>>(etvData);

            var cetvSize = Convert.ToDouble(etvDataMap["equivalentTransferValue"]);
            var enhancementToCetv = Convert.ToDouble(etvDataMap["enhancementToCetv"]);
            var takeUpRate = Convert.ToDouble(etvDataMap["takeUpRate"]);

            scheme.SetClientEtvOptions(new EnhancedTransferValueOptions(cetvSize, enhancementToCetv, takeUpRate));

            return Etv();
        }

        public ActionResult ResetEtv()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            scheme.ResetClientEtvOptions();
            return Etv();
        }

        public ActionResult Abf()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = abf();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object abf()
        {
            var scheme = Scheme.PensionScheme;
            var abfData = scheme.CurrentBasis.GetAbfData().After;
            var maxSliderVal = scheme.CurrentBasis.Evolution[scheme.CurrentBasis.AnchorDate].TotalAssets * 0.5;

            return new
            {
                setup = new
                {
                    DeficitProportion = scheme.GetClientAbfOptions().Before.Value,
                    scheme.GetClientAbfOptions().Before.RecoveryPlanLength,
                    scheme.GetClientAbfOptions().Before.AbfLength,
                },
                fundingDeficit = abfData.FundingDeficit,
                lengthOfRecoveryPlan = scheme.GetClientAbfOptions().After.RecoveryPlanLength,
                annualPayments = abfData.AnnualPayments,
                netCashPosition = abfData.NetCashPosition1,
                valueOfAbf = scheme.GetClientAbfOptions().After.Value,
                lengthOfAbf = scheme.GetClientAbfOptions().After.AbfLength,
                annualContribution = abfData.AnnualContribution,
                newNetCashPosition = abfData.NetCashPosition2,
                cumulativeCashSaving = abfData.CostSaving,
                maxAbfSliderVal = maxSliderVal
            };
        }

        public ActionResult SetAbf(string abfData)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var abfDataMap = JsonConvert.DeserializeObject<Dictionary<string, double>>(abfData);

            //var deficit = Convert.ToDouble(abfDataMap["fundingDeficit"]);
            var assetValue = Convert.ToDouble(abfDataMap["assetValue"]);

            //var percentageOfAbf = Math.Round(assetValue / deficit, 4);

            scheme.SetClientAbfOptions(new AbfOptions(assetValue));

            return Abf();
        }

        public ActionResult ResetAbf()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);
            scheme.ResetClientAbfOptions();
            return Abf();
        }

        public ActionResult Pie()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = pie();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object pie()
        {
            var scheme = Scheme.PensionScheme;

            // :)
            var aPie = scheme.CurrentBasis.GetPieData().After;

            //TODO when the backend is refactored this should come from there
            var PIEPenLiabEligible = aPie.LiabilityExcludingLevelPensions * scheme.GetClientPieOptions().After.PortionEligible;
            var BA22 = aPie.ValueOfIncreases1 * scheme.GetClientPieOptions().After.PortionEligible;
            var PIEPenLiabNilIncs = PIEPenLiabEligible - BA22;

            return new
            {
                setup = new
                {
                    scheme.GetClientPieOptions().Before.PortionEligible,
                    scheme.GetClientPieOptions().Before.ActualUplift,
                    scheme.GetClientPieOptions().Before.TakeUpRate,
                },
                scheme.GetClientPieOptions().After.PortionEligible,
                scheme.GetClientPieOptions().After.ActualUplift,
                scheme.GetClientPieOptions().After.TakeUpRate,
                liabilityBeforePie = aPie.Liability.Before,
                liabilityAfterPie = aPie.Liability.After,
                aPie.LiabilityExcludingLevelPensions,
                EligibleForExchange = PIEPenLiabEligible,
                ZeroIncreases = PIEPenLiabNilIncs,
                ValueOfFutureIncreases = BA22,
                aPie.ValueOfIncreases1,
                changeInLiabilities = aPie.LiabilityChange,
                aPie.CostNeutralUplift,
                ValueShared = Utils.MathSafeDiv(scheme.GetClientPieOptions().After.ActualUplift, aPie.CostNeutralUplift)
            };
        }

        public ActionResult SetPie(string pieData)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var pieDataMap = JsonConvert.DeserializeObject<Dictionary<string, double>>(pieData);

            double eligibleLiability = Convert.ToDouble(pieDataMap["eligibleLiability"]);
            double actualUplift = Convert.ToDouble(pieDataMap["actualUplift"]);
            double takeUpRate = Convert.ToDouble(pieDataMap["takeUpRate"]);

            scheme.SetClientPieOptions(new PieOptions(eligibleLiability, actualUplift, takeUpRate));
            return Pie();
        }

        public ActionResult ResetPie()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);
            scheme.ResetClientPieOptions();
            return Pie();
        }

        public ActionResult FutureBenefits()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = futureBenefits();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object futureBenefits()
        {
            var scheme = Scheme.PensionScheme;

            var futureBenefitsData = scheme.CurrentBasis.GetFutureBenefits().After;
            var paramSettings = scheme.GetClientFutureBenefitsParameters().After;

            var setup = new
            {
                scheme.GetClientFutureBenefitsParameters().Before.MemberContributionsIncrease,
                adjustmentType = scheme.GetClientFutureBenefitsParameters().Before.AdjustmentType.ToString(),
                scheme.GetClientFutureBenefitsParameters().Before.AdjustmentRate
            };

            if (futureBenefitsData == null)
            {
                return new
                {
                    setup,
                    enabled = false,
                    annualCost = 0,
                    contributionRate = 0,

                    employeeRate = 0,
                    employerRate = 0,
                    changeInAnnualCost = 0,
                    changeInEmployerRate = 0,
                    changeInLiabilities = 0,
                    increaseMemberContributions = paramSettings.MemberContributionsIncrease,
                    adjustmentType = paramSettings.AdjustmentType.ToString(),
                    adjustmentRate = paramSettings.AdjustmentRate,
                    futureBenefitsServiceCost = 0,
                    futureBenefitsContRateEstimate = 0,
                    futureBenefitsEstimatedBenefitReduction = 0,
                    futureBenefitsChangeIn5Years = 0,
                    futureBenefitsChangeIn10Years = 0
                };
            }
            else
            {
                return new
                {
                    setup,
                    enabled = scheme.CurrentBasis.BasisType != BasisType.Buyout,
                    annualCost = futureBenefitsData.AnnualCost,
                    contributionRate = futureBenefitsData.ContributionRate,

                    employeeRate = futureBenefitsData.EmployeeRate,
                    employerRate = futureBenefitsData.EmployerRate,
                    changeInAnnualCost = futureBenefitsData.AnnualCostChange,
                    changeInEmployerRate = futureBenefitsData.EmployerRateChange,
                    changeInLiabilities = futureBenefitsData.LiabilitiesChange,
                    increaseMemberContributions = paramSettings.MemberContributionsIncrease,
                    adjustmentType = paramSettings.AdjustmentType.ToString(),
                    adjustmentRate = paramSettings.AdjustmentRate,
                    futureBenefitsServiceCost = futureBenefitsData.ServiceCostEstimate,
                    futureBenefitsContRateEstimate = futureBenefitsData.ContributionRateEstimate,
                    futureBenefitsEstimatedBenefitReduction = futureBenefitsData.EstimatedBenefitReduction,
                    futureBenefitsChangeIn5Years = futureBenefitsData.ChangeIn5Years,
                    futureBenefitsChangeIn10Years = futureBenefitsData.ChangeIn10Years
                };
            }
        }

        public ActionResult SetFutureBenefits(string futureBenefitsData)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var futureBenefitsDataMap = JsonConvert.DeserializeObject<Dictionary<string, object>>(futureBenefitsData);

            double increaseMemberContribs = Convert.ToDouble(futureBenefitsDataMap["increaseMemberContributions"]);
            double adjustmentRate = Convert.ToDouble(futureBenefitsDataMap["adjustmentRate"]);
            BenefitAdjustmentType adjustmentType;
            Enum.TryParse((String)futureBenefitsDataMap["adjustmentType"], out adjustmentType);

            scheme.SetClientFutureBenefitsParameters(new FutureBenefitsParameters(increaseMemberContribs, adjustmentType, adjustmentRate));

            return FutureBenefits();
        }

        [HttpPost]
        public ActionResult SetInsurance(string insuranceData)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);
            if (scheme.CurrentBasis.BasisType != BasisType.Buyout) return Json(null, JsonRequestBehavior.AllowGet);

            var insuranceDataMap = JsonConvert.DeserializeObject<Dictionary<string, double>>(insuranceData);

            var pensionerLiabilityInsured = Convert.ToDouble(insuranceDataMap["pen"]);
            var nonpensionerLiabilityInsured = Convert.ToDouble(insuranceDataMap["nonpen"]);

            scheme.SetClientInsuranceParameters(new InsuranceParameters(pensionerLiabilityInsured, nonpensionerLiabilityInsured));

            return Insurance();
        }

        [HttpPost]
        public ActionResult ResetInsurance()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);
            if (scheme.CurrentBasis.BasisType != BasisType.Buyout) return Json(null, JsonRequestBehavior.AllowGet);
            scheme.ResetClientInsuranceParameters();
            return Insurance();
        }

        [HttpGet]
        public ActionResult Insurance()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = insurance();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object insurance()
        {
            var scheme = Scheme.PensionScheme;

            var available = scheme.GetBasisTypes().Contains(BasisType.Buyout);

            var fundingLevelData = available ? scheme.CurrentBasis.GetFundingLevelData().After : new FundingData(0, 0, 0, 0, 0, 0);
            var insuranceData = scheme.CurrentBasis.GetBuyinInfo().After;

            var pensionerLiability = insuranceData.UninsuredPensionerLiability;
            var nonpensionerLiability = insuranceData.UninsuredNonPensionerLiability;
            var fiftyBasisPointImpact = available ? scheme.CurrentBasis.GetInsurancePointChangeImpact().After : 0;
            var buyoutBasis = scheme.GetDefaultBasis(BasisType.Buyout);
            string buyoutBasisName = string.Empty;

            if (buyoutBasis != null)
                buyoutBasisName = buyoutBasis.Name;

            return new
            {
                setup = new
                {
                    enabled = scheme.CurrentBasis.BasisType == BasisType.Buyout,
                    pensionerLiabilityPercInsured = scheme.GetClientInsuranceParameters().Before.PensionerLiabilityPercInsured,
                    nonpensionerLiabilityPercInsured = scheme.GetClientInsuranceParameters().Before.NonpensionerLiabilityPercInsured,
                    analysisDate = JsonHelper.ConvertDateToJSON(scheme.CurrentBasis.AttributionEnd),
                    buyoutBasisName = buyoutBasisName
                },
                pensionerLiability, //TODO why are  we passing the same value twice? 
                nonpensionerLiability,
                uninsuredPensionerLiability = pensionerLiability,
                uninsuredNonpensionerLiability = nonpensionerLiability,
                pensionerLiabilityPercInsured = scheme.GetClientInsuranceParameters().After.PensionerLiabilityPercInsured,
                pensionerLiabilityInsured = insuranceData.PensionerLiabilityToBeInsured,
                nonpensionerLiabilityPercInsured = scheme.GetClientInsuranceParameters().After.NonpensionerLiabilityPercInsured,
                nonpensionerLiabilityInsured = insuranceData.NonPensionerLiabilityToBeInsured,
                buyinCost = scheme.BuyinCost.Total,
                fiftyBasisPointImpact,
                assetVal = fundingLevelData.Assets,
                strains = Strains()
            };
        }

        [HttpGet]
        public ActionResult Strains()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var basisTypes = scheme.GetBasisTypes();
            var startDate = scheme.CurrentBasis.AttributionStart.ToShortDateString();
            if (!basisTypes.Contains(BasisType.Buyout) || basisTypes.Distinct().Count() == 1) return Json(null, JsonRequestBehavior.AllowGet);

            var json = GetStrainsFromScheme(scheme);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private dynamic GetStrainsFromScheme(IPensionScheme scheme)
        {
            return scheme.GetStrains().OrderBy(x => x.Key).Select(x => new
                    {
                        basis = x.Key,
                        curr = x.Value.Item1[StrainType.Current],
                        max = x.Value.Item1[StrainType.Max],
                        min = x.Value.Item1[StrainType.Min],
                        startDate = x.Value.Item2.ToShortDateString()
                    });
        }

        public ActionResult ResetFutureBenefits()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            scheme.ResetClientFutureBenefitsParameters();

            return FutureBenefits();
        }

        public ActionResult GetVarWaterfallData()
        {
            dynamic displayOptions = ControllerContext.HttpContext.Session["VaRWaterfallDisplayOptions"];
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            if (displayOptions == null)
            {
                //use the defaults
                var options = Scheme.PensionScheme.GetClientWaterfallOptions().Before;
                displayOptions = SetVaRWaterfallClientState(options.CI, options.Time);
            }

            return VarWaterFall(displayOptions);
        }

        public ActionResult SetVarWaterfallData(double ci, int time)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var t = Scheme.PensionScheme.GetClientWaterfallOptions().Before;

            scheme.SetClientWaterfallOptions(new WaterfallOptions(ci, time));
            dynamic displayOptions = SetVaRWaterfallClientState(ci, time);

            return VarWaterFall(displayOptions);
        }

        private ActionResult VarWaterFall(dynamic displayOptions)
        {
            var scheme = Scheme.PensionScheme;
            var varWaterfall = scheme.CurrentBasis.GetVarWaterfall();

            Func<WaterfallData, object> convertForDisplay = x =>
                new
                {
                    Interest = x.LiabRiskInterestInflation,
                    InterestInflationHedging = -x.InterestInflationHedging,
                    Longevity = x.Longevity,
                    Hedge = -x.LongevityHedging,
                    Credit = x.CreditRisk,
                    Equity = x.EquityRisk,
                    OtherGrowth = x.OtherGrowthRisk,
                    Diversification = x.TotalRisk - (x.LiabRiskInterestInflation + -x.InterestInflationHedging + x.Longevity + -x.LongevityHedging + x.CreditRisk + x.EquityRisk + x.OtherGrowthRisk),
                    TotalRisk = x.TotalRisk
                };

            var json = new
            {
                before = varWaterfall.IsDirty ? convertForDisplay(varWaterfall.Before) : null,
                after = convertForDisplay(varWaterfall.After),
                displayOptions = new
                {
                    ci = displayOptions == null ? true : displayOptions.ci,
                    time = displayOptions == null ? false : displayOptions.time
                }
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public object SetVaRWaterfallClientState(double ci, int time)
        {
            ControllerContext.HttpContext.Session["VaRWaterfallDisplayOptions"] = new
            {
                ci = ci,
                time = time
            };
            return ControllerContext.HttpContext.Session["VaRWaterfallDisplayOptions"];
        }

        public ActionResult VarFunnelData()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var data = scheme.CurrentBasis.GetVarFunnel();
            var after = varFunnelData(data.After);
            var before = data.IsDirty ? varFunnelData(data.Before) : null;

            var json = new
            {
                before,
                after
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object varFunnelData(IList<VarFunnelData> data)
        {
            var scheme = Scheme.PensionScheme;

            var percentile95 = new ArrayList();
            var percentile80 = new ArrayList();
            var median = new ArrayList();
            var percentile20 = new ArrayList();
            var percentile5 = new ArrayList();
            var years = new ArrayList();

            int year = scheme.CurrentBasis.AttributionEnd.Year;

            foreach (var varFunnelData in data)
            {
                percentile95.Add(varFunnelData.Percentile95);
                percentile80.Add(varFunnelData.Percentile80);
                median.Add(varFunnelData.Median);
                percentile20.Add(varFunnelData.Percentile20);
                percentile5.Add(varFunnelData.Percentile5);
                years.Add(year);
                year++;
            }

            var json = new
            {
                percentile95,
                percentile80 = percentile80,
                median,
                percentile20 = percentile20,
                percentile5,
                years
            };

            return json;
        }

        public ActionResult InvestmentStrategy(bool assetsValueChanged = false)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = investmentStrategy(assetsValueChanged);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private Dictionary<AssetClassType, double> scaleInvestmentStrategyAllocationsWithoutABF(bool useClientAssumptions = true)
        {
            var scheme = Scheme.PensionScheme;

            var assets = scheme.GetClientInvestmentStrategyAllocations().Before.Keys;

            // remove any abf proportion (this can't be edited) and scale the remaining proprotions accordingly
            var abfProp = scheme.GetClientInvestmentStrategyAllocations().Before.ContainsKey(AssetClassType.Abf)
                    ? scheme.GetClientInvestmentStrategyAllocations().Before[AssetClassType.Abf]
                    : 0;

            var scaled =
                assets
                    .Where(x => x != AssetClassType.Abf)
                    .ToDictionary(x => x,
                        x => (
                            useClientAssumptions
                            ? scheme.GetClientInvestmentStrategyAllocations().After[x]
                            : scheme.GetClientInvestmentStrategyAllocations().Before[x]
                            )
                            / (1 - abfProp));

            return scaled;
        }

        private Dictionary<AssetClassType, double> scaleInvestmentStrategyAllocationsWithABF(Dictionary<AssetClassType, double> allocations)
        {
            var scheme = Scheme.PensionScheme;

            //scale back down taking abf proportion in to account and add the abf proportion back in to the allocations
            var abfProp = scheme.GetClientInvestmentStrategyAllocations().Before.ContainsKey(AssetClassType.Abf)
                ? scheme.GetClientInvestmentStrategyAllocations().Before[AssetClassType.Abf]
                : 0;
            allocations = allocations.ToDictionary(x => x.Key, x => x.Value * (1 - abfProp));
            if (scheme.GetClientInvestmentStrategyAllocations().Before.ContainsKey(AssetClassType.Abf) && !allocations.ContainsKey(AssetClassType.Abf))
                allocations.Add(AssetClassType.Abf, abfProp);

            return allocations;
        }

        public ActionResult SetInvestmentStrategyCashInjection(double cashInjection)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var investmentStrategyData = scheme.CurrentBasis.GetInvestmentStrategyData();

            var cashInjectionDiff = cashInjection - scheme.GetClientInvestmentStrategyOptions().After.CashInjection;

            var newAssetTotal = investmentStrategyData.After.TotalCoreAssets + cashInjectionDiff;

            var allocations = scaleInvestmentStrategyAllocationsWithoutABF();
            
            foreach (var assetClassType in scheme.GetClientInvestmentStrategyAllocations().After.Keys.Where(x => x != AssetClassType.Abf))
            {
                var prop = .0;
                var currentAmount = allocations[assetClassType] * investmentStrategyData.After.TotalCoreAssets;
                if (assetClassType == AssetClassType.Cash)
                {
                    prop = (currentAmount + cashInjectionDiff) / newAssetTotal;
                }
                else
                {
                    prop = currentAmount / newAssetTotal;
                }
                allocations[assetClassType] = prop;
            }

            var currentAssumptions = scheme.GetClientInvestmentStrategyOptions().After;

            scheme.SetClientInvestmentStrategyOptions(new InvestmentStrategyOptions(cashInjection, currentAssumptions.SyntheticCredit, currentAssumptions.SyntheticEquity, currentAssumptions.LDIInForce, currentAssumptions.InterestHedge, currentAssumptions.InflationHedge));

            allocations = scaleInvestmentStrategyAllocationsWithABF(allocations);

            scheme.SetClientInvestmentStrategyAllocations(allocations);

            return InvestmentStrategy(true);
        }

        public ActionResult SetInvestmentStrategyAdvancedOptions(double syntheticEquity, double syntheticCredit, bool enableLdi, double hedgingInterest, double hedgingInflation)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var currentAssumptions = scheme.GetClientInvestmentStrategyOptions().After;

            scheme.SetClientInvestmentStrategyOptions(new InvestmentStrategyOptions(currentAssumptions.CashInjection, syntheticCredit, syntheticEquity, enableLdi, hedgingInterest, hedgingInflation));

            return InvestmentStrategy(true);
        }

        public ActionResult SetInvestmentStrategyAssetMix(string assetMix)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var allocations = new Dictionary<AssetClassType, double>();

            var assetMixMap = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(assetMix);

            foreach (Dictionary<string, object> assetClass in assetMixMap)
            {
                var type = (AssetClassType)Convert.ToInt32(assetClass["id"]);
                allocations.Add(type, Convert.ToDouble(assetClass["proportion"]));
            }

            allocations = scaleInvestmentStrategyAllocationsWithABF(allocations);

            scheme.SetClientInvestmentStrategyAllocations(allocations);

            return InvestmentStrategy(false);
        }

        private VarDataObject _varData;
        private VarDataObject investmentStrategy(bool assetsValueChanged = false)
        {
            if (_varData != null && assetsValueChanged == false)
                return _varData;

            var scheme = Scheme.PensionScheme;

            var investmentStrategyData = scheme.CurrentBasis.GetInvestmentStrategyData();

            var totalExistingAssets = investmentStrategyData.After.TotalCoreAssets; ;
            var totalOtherAssets = investmentStrategyData.After.TotalOtherAssets;
            var totalAssets = investmentStrategyData.After.TotalAssets;
            var abfTotal = investmentStrategyData.After.ABF;
            var buyinTotal = investmentStrategyData.After.Buyin;
            var syntheticAssetInfo = viewService.GetSchemeAssetData(scheme, AssumptionsSourceType.Client).SyntheticAssetInfo;

            var syntheticEquity = scheme.GetClientInvestmentStrategyOptions().After.SyntheticEquity;
            var syntheticCredit = scheme.GetClientInvestmentStrategyOptions().After.SyntheticCredit;

            var assets = scheme.GetClientInvestmentStrategyAllocations().Before.Keys;

            var assetsProportionsBefore = scaleInvestmentStrategyAllocationsWithoutABF(useClientAssumptions: false);
            var assetsProportionsAfter = scaleInvestmentStrategyAllocationsWithoutABF();

            var assetMixTotalOtherProportion = totalOtherAssets == 0 ? 0 : totalOtherAssets / totalAssets;

            var subItems = assetsProportionsAfter
                .Select(x => new KeyValuePair<AssetCategory, BreakdownItem>
                    (
                        Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(x.Key).Category,
                        new BreakdownItem(x.Key.DisplayName(), x.Value * totalExistingAssets)
                    ))
                .Union(new List<KeyValuePair<AssetCategory, BreakdownItem>>
                    {
                        { new KeyValuePair<AssetCategory, BreakdownItem>(Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(AssetClassType.Abf).Category, new BreakdownItem(AssetClassType.Abf.DisplayName(), abfTotal)) },
                        { new KeyValuePair<AssetCategory, BreakdownItem>(FlightDeck.Services.ViewService.BuyinCategory, new BreakdownItem("Buy in", buyinTotal)) }
                    });
            var assetsBreakdown =
                subItems
                    .GroupBy(x => x.Key)
                    .OrderBy(x => x.Key)
                    .Select(x => new BreakdownItem
                    {
                        Label = x.Key.ToString(),
                        Value = x.Sum(ac => ac.Value.Value),
                        SubItems = x.Select(ac => ac.Value).OrderBy(ac => ac.Label)
                    });

            var assetClasses = assets
                        .Where(x => x != AssetClassType.Abf)
                        .OrderBy(x => Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(x).Category).ThenBy(x => x.DisplayName())
                        .Select(x => new
                        {
                            Key = (int)x,
                            Label = x.DisplayName(),
                            Proportion = assetsProportionsBefore[x]
                        }).ToList();

            //cash has to come last
            var cash = assetClasses.SingleOrDefault(x => x.Key == (int)AssetClassType.Cash);
            assetClasses.Remove(cash);
            assetClasses.Add(cash);

            //hedging
            var hedgingBreakdown = scheme.Funded ? scheme.CurrentBasis.GetHedgingBreakdown().After : null;
            var hedgingInterest = scheme.GetClientInvestmentStrategyOptions().After.InterestHedge;
            var hedgingInflation = scheme.GetClientInvestmentStrategyOptions().After.InflationHedge;
            var hedgingInterestLowerLimit = .0;
            var hedgingInflationLowerLimit = .0;

            if (hedgingBreakdown != null && hedgingBreakdown.Cashflows != null)
            {
                //it's possible for the user to change asset allocations which would cause a recalculation of the hedging
                //info such that there is negative ldi overlay. 
                //this is the situation we are trying to avoid by having minimum hedging values.
                //therefore if this happens, manually adjust the figures to be mathematically equivalent to 0% ldi overlay. 
                Action<HedgeBreakdownItem> negativeLDIAdjustment = hb =>
                {
                    if (hb.LDIOverlay < 0)
                    {
                        hb.Total += -hb.LDIOverlay;
                        hb.LDIOverlay = 0;
                    }
                };
                negativeLDIAdjustment(hedgingBreakdown.Cashflows[HedgeType.Interest]);
                negativeLDIAdjustment(hedgingBreakdown.Cashflows[HedgeType.Inflation]);
                negativeLDIAdjustment(hedgingBreakdown.CurrentBasis[HedgeType.Interest]);
                negativeLDIAdjustment(hedgingBreakdown.CurrentBasis[HedgeType.Inflation]);

                hedgingInterestLowerLimit = hedgingBreakdown.Cashflows[HedgeType.Interest].PhysicalAssets + hedgingBreakdown.Cashflows[HedgeType.Interest].Buyin;
                hedgingInflationLowerLimit = hedgingBreakdown.Cashflows[HedgeType.Inflation].PhysicalAssets + hedgingBreakdown.Cashflows[HedgeType.Inflation].Buyin;

                hedgingInterest = Math.Max(hedgingInterest, hedgingInterestLowerLimit);
                hedgingInflation = Math.Max(hedgingInflation, hedgingInflationLowerLimit);
            }

            _varData = new VarDataObject
            {
                setup = new
                {
                    assetClasses = assetClasses,
                    giltYieldLabel = scheme.NamedProperties.ContainsKey(NamedPropertyGroupType.RefGiltYeild)
                        ? scheme.NamedProperties[NamedPropertyGroupType.RefGiltYeild].Properties.First(x => x.Key == NamedRefGiltYieldPropertyTypes.DisplayName.ToString()).Value
                        : null,
                    cashInjectionMax = totalExistingAssets - scheme.GetClientInvestmentStrategyOptions().After.CashInjection,
                    hedgingInterest = scheme.GetClientInvestmentStrategyOptions().Before.InterestHedge,
                    hedgingInflation = scheme.GetClientInvestmentStrategyOptions().Before.InflationHedge,
                },
                basisName = scheme.CurrentBasis.Name,
                assetsBreakdown = assetsBreakdown,
                assetsProportions = assetsProportionsAfter.Select(x => new { Key = x.Key, Proportion = x.Value }),
                strategy = VarStrategy.Custom.ToString(),
                buyin = buyinTotal,
                abf = abfTotal,
                totalExistingAssets = totalExistingAssets,
                totalOtherAssets = totalOtherAssets,
                totalAssets = totalAssets,
                cashAssetAmount = assetsProportionsAfter[AssetClassType.Cash] * totalExistingAssets,
                cashInjection = scheme.GetClientInvestmentStrategyOptions().After.CashInjection,
                syntheticEquity = syntheticEquity,
                syntheticCredit = syntheticCredit,
                syntheticAssetInfo = syntheticAssetInfo,
                ldiEnabled = scheme.GetClientInvestmentStrategyOptions().After.LDIInForce,
                hedgingInputsEnabled = scheme.GetClientInvestmentStrategyOptions().After.LDIInForce,
                hedgingInterest = hedgingInterest,
                hedgingInflation = hedgingInflation,
                hedgingInterestLowerLimit = hedgingInterestLowerLimit,
                hedgingInflationLowerLimit = hedgingInflationLowerLimit,
                assetMixTotalOtherProportion = assetMixTotalOtherProportion,
                hedgingBreakdown = hedgingBreakdown != null ? JsonConvert.SerializeObject(hedgingBreakdown) : null,
                weightedReturnParameters = scheme.CurrentBasis.GetWeightedReturnParameters(),
                giltYield = scheme.CurrentBasis.GetGiltYield(),
            };

            return _varData;
        }

        public ActionResult ResetInvestmentStrategy()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            scheme.ResetClientInvestmentStrategyAllocations();
            scheme.ResetClientInvestmentStrategyOptions();

            return InvestmentStrategy();
        }

        private class JourneyPlanChartDataItem
        {
            public class Proportion
            {
                public double liabs { get; set; }
                public double assets { get; set; }
                public double proportion { get; set; }
            }
            public Proportion before { get; set; }
            public Proportion after { get; set; }
            public int year { get; set; }
            public double commencementDate { get; set; }
        }

        public ActionResult JourneyPlanData()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var currentBasis = Scheme.PensionScheme.CurrentBasis;

            var jpBefore = currentBasis.GetJourneyPlan().BeforePlan.After;
            var jpAfter = currentBasis.GetJourneyPlan().AfterPlan.After;

            var yearX = currentBasis.AttributionEnd.Year;

            //show at least as many years as the recovery plan (and at least 7 years)
            var recoveryPlanSettings = currentBasis.GetClientRecoveryAssumptions().After;
            var recoveryPlanViewableYears = isDataDirty(RequestType.CashLens) ? currentBasis.GetRecoveryPlan().After.Count : currentBasis.GetRecoveryPlan().Before.Count;
            var fundingProgressionLength = Math.Min(Math.Max(7, recoveryPlanViewableYears), Math.Max(jpBefore.Count, jpAfter.Count)) + 1; // + 1 for the final fence post! https://basecamp.com/2305976/projects/3171199/messages/59765988#comment_437102753

            var plan = new List<JourneyPlanChartDataItem>();
            for (int i = 0; i < fundingProgressionLength; i++)
            {
                var before = new JourneyPlanChartDataItem.Proportion();
                if (jpBefore.Count > i)
                {
                    before.liabs = jpBefore[i].Liabilities;
                    before.assets = jpBefore[i].Assets;
                    before.proportion = jpBefore[i].Proportion;
                };
                var after = new JourneyPlanChartDataItem.Proportion();
                if (jpAfter.Count > i)
                {
                    after.liabs = jpAfter[i].Liabilities;
                    after.assets = jpAfter[i].Assets;
                    after.proportion = jpAfter[i].Proportion;
                };

                plan.Add(new JourneyPlanChartDataItem
                    {
                        before = before,
                        after = after,
                        year = yearX++,
                        commencementDate = JsonHelper.ConvertDateToJSON(currentBasis.AttributionEnd)
                    });
            }

            var json = new
            {
                plan,
                showAlternate = !scheme.Funded,
                isDirty = isDataDirty(RequestType.JourneyPlan)
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CashLens()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var isDirty = isDataDirty(RequestType.CashLens);

            var data = scheme.CurrentBasis.GetRecoveryPlan();

            var max = Math.Max(data.Before.Count, data.After.Count);

            var recoveryPlanData = new
            {
                before = scheme.Funded ? Utils.PaddedTake<double>(data.Before, max) : null,
                after = Utils.PaddedTake<double>(data.After, max),
                year = scheme.CurrentBasis.AttributionEnd.Year,
                commencementDate = JsonHelper.ConvertDateToJSON(scheme.CurrentBasis.AttributionEnd),
            };

            var fundingLevel = scheme.CurrentBasis.GetFundingLevelData();
            var after = fundingLevel.After;
            var before = fundingLevel.IsDirty ? fundingLevel.Before : null;

            Func<FundingData, object> tojson = x => new
                {
                    x.Assets,
                    x.Buyin,
                    x.AssetsTotal,
                    Deferred = x.Deferreds,
                    Active = x.Actives,
                    Pension = x.Pensioners,
                    LiabilityTotal = x.Liabilities,
                    Balance = x.Deficit
                };

            var json = new
            {
                basis = new { type = scheme.CurrentBasis.BasisType.ToString(), name = scheme.CurrentBasis.Name },
                isDirty,
                recoveryPlanData,
                fundingLevelData = new
                {
                    after = tojson(after),
                    before = before != null ? tojson(before) : null
                }
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult RecoveryPlan()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var json = recoveryPlan();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object recoveryPlan()
        {
            var scheme = Scheme.PensionScheme;

            var weightedReturnParameters = scheme.CurrentBasis.GetWeightedReturnParameters();

            return new
            {
                setup = new
                {
                    scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.Term,
                    scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.Increases,
                    scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.InvestmentGrowth,
                    scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.DiscountIncrement,
                    scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.StartMonth,
                    LumpSumPayment = scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.LumpSumPayment,
                    scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.MaxLumpSumPayment,
                    giltYieldLabel = scheme.NamedProperties.ContainsKey(NamedPropertyGroupType.RefGiltYeild)
                        ? scheme.NamedProperties[NamedPropertyGroupType.RefGiltYeild].Properties.First(x => x.Key == NamedRefGiltYieldPropertyTypes.DisplayName.ToString()).Value
                        : null
                },
                Length = scheme.CurrentBasis.GetClientRecoveryAssumptions().After.Term,
                IsGrowthFixed = scheme.CurrentBasis.GetClientRecoveryAssumptions().After.IsGrowthFixed,
                Increases = scheme.CurrentBasis.GetClientRecoveryAssumptions().After.Increases,
                InvestmentGrowth = scheme.CurrentBasis.GetClientRecoveryAssumptions().After.InvestmentGrowth,
                DiscountIncrement = scheme.CurrentBasis.GetClientRecoveryAssumptions().After.DiscountIncrement,
                StartMonth = scheme.CurrentBasis.GetClientRecoveryAssumptions().After.StartMonth,
                LumpSumPayment = scheme.CurrentBasis.GetClientRecoveryAssumptions().After.LumpSumPayment,
                StrategyType = scheme.GetClientRecoveryPlanOptions().After.ToString(),
                weightedReturnParameters,
                giltYield = scheme.CurrentBasis.GetGiltYield(),
                discountRateWarningThreshold = Math.Abs(weightedReturnParameters.MaxReturn - weightedReturnParameters.WeightedDiscountRate),
                fixedAssetWarningThreshold = weightedReturnParameters.MaxReturn
            };
        }

        [HttpPost]
        public ActionResult SetRecoveryPlan(string recoveryPlan)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var recoveryPlanMap = JsonConvert.DeserializeObject<Dictionary<string, object>>(recoveryPlan);
            var recoveryPlanAssumptions = new RecoveryPlanAssumptions(
                    Convert.ToDouble(recoveryPlanMap["recoveryPlanInvestmentGrowth"]),
                    Convert.ToDouble(recoveryPlanMap["recoveryPlanDiscountIncrement"]),
                    Convert.ToDouble(recoveryPlanMap["recoveryPlanLumpSumPayment"]),
                    scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.MaxLumpSumPayment,
                    Convert.ToDouble(recoveryPlanMap["recoveryPlanIncreases"]),
                    Convert.ToBoolean(recoveryPlanMap["recoveryPlanIsGrowthFixed"]),
                    Convert.ToInt32(recoveryPlanMap["recoveryPlanStartMonth"]),
                    Convert.ToInt32(recoveryPlanMap["recoveryPlanLength"])
                );

            scheme.CurrentBasis.SetClientRecoveryAssumptions(recoveryPlanAssumptions);

            RecoveryPlanType rpStrategyType;
            Enum.TryParse((String)recoveryPlanMap["recoveryPlanStrategyType"], out rpStrategyType);

            scheme.SetClientRecoveryPlanOptions(rpStrategyType);

            return RecoveryPlan();
        }

        [HttpPost]
        public ActionResult ResetRecoveryPlan()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            scheme.ResetClientRecoveryPlanOptions();
            scheme.CurrentBasis.ResetClientRecoveryAssumptions();

            return RecoveryPlan();
        }

        public ActionResult AccountingData()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            if (scheme.GetBasisTypes().Contains(BasisType.Accounting))
            {
                var periods = scheme.CurrentBasis.GetAccountingPeriods().OrderBy(x => x);

                var ias = scheme.CurrentBasis.GetIASData();
                var frs = scheme.CurrentBasis.GetFRSData();
                var usgaap = scheme.CurrentBasis.GetUSGAAPData();
                var balance = scheme.CurrentBasis.GetBalanceSheetEstimate();

                var data = new
                {
                    hasAccountingBasis = true,
                    anchorDate = (scheme.CurrentBasis.AnchorDate - JsonHelper.UnixEpoch).TotalMilliseconds,
                    refreshDate = (scheme.RefreshDate - JsonHelper.UnixEpoch).TotalMilliseconds,
                    analysisPeriodStart = (scheme.CurrentBasis.AttributionStart - JsonHelper.UnixEpoch).TotalMilliseconds,
                    analysisPeriodEnd = (scheme.CurrentBasis.AttributionEnd - JsonHelper.UnixEpoch).TotalMilliseconds,
                    periods = periods.Select(x => (x - JsonHelper.UnixEpoch).TotalMilliseconds),
                    balance = new
                        {
                            after = balance.After,
                            before = balance.IsDirty ? balance.Before : null
                        },
                    after = new
                        {
                            pnlIas19 = ias != null ? ias.After : null,
                            pnlFrs102 = frs != null ? frs.After : null,
                            pnlUsgaap = usgaap != null ? usgaap.After : null,
                        },
                    before = isDataDirty(RequestType.AccountingData) ?
                        new
                        {
                            pnlIas19 = ias != null ? ias.Before : null,
                            pnlFrs102 = frs != null ? frs.Before : null,
                            pnlUsgaap = usgaap != null ? usgaap.Before : null,
                        }
                        : null
                };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data = new
                {
                    hasAccountingBasis = false,
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [DateTimeValidator]
        public ActionResult SetAnalysisPeriod(string start, string end, bool setEvolutionDates = false)
        {
            var successfulDateChange = false;

            if (ModelState.IsValid)
            {
                var scheme = Scheme.PensionScheme;
                if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

                var startDate = string.IsNullOrEmpty(start) ? scheme.CurrentBasis.AnchorDate : JsonHelper.UnixEpoch.AddMilliseconds(long.Parse(start));
                var endDate = string.IsNullOrEmpty(end) ? scheme.CurrentBasis.AnchorDate : JsonHelper.UnixEpoch.AddMilliseconds(long.Parse(end));

                if (endDate < startDate && setEvolutionDates == false)
                {
                    //This is intended to handle when the user has selected a range on the evolution page, 
                    //then chooses a date on the analysis page which is before the evolution range start date.
                    //The analysis page shouldn't be affected by the attribution start date, so it shouldn't matter if
                    //up until this point the attribution start date was the evolution range start date and now we're changing it to the anchor date
                    //You may be wondering why we didn't just set it to the anchor date to start with - but changing attribution period resets all user assumptions.
                    startDate = scheme.CurrentBasis.AnchorDate;
                }

                if (startDate != scheme.CurrentBasis.AttributionStart || endDate != scheme.CurrentBasis.AttributionEnd)
                {
                    successfulDateChange = scheme.SetAttributionPeriod(startDate, endDate);

                    if (successfulDateChange)
                    {
                        ResetAdditionalItems(scheme);

                        if (setEvolutionDates)
                        {
                            scheme.CurrentBasis.EvolutionStartDate = startDate;
                            scheme.CurrentBasis.EvolutionEndDate = endDate;
                        }
                    }
                }
            }

            return Json(new { result = successfulDateChange }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ResetAllAssumptions()
        {
            Scheme.PensionScheme.ResetAttributionPeriod();
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetAssetAssumptions(string assetAssumptions)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var assetAssumptionMap = JsonConvert.DeserializeObject<Dictionary<string, double>>(assetAssumptions);

            scheme.CurrentBasis.SetClientAssetAssumptions(
                assetAssumptionMap.ToDictionary(x => (AssetClassType)Convert.ToInt32(x.Key), x => x.Value));

            var json = assets();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetLiabilityAssumptions(string liabilityAssumptions)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var liabilityAssumptionMap = JsonConvert.DeserializeObject<Dictionary<string, double>>(liabilityAssumptions);

            var liabAssumptionData = new AssumptionData(
                liabilityAssumptionMap["preRetirement"],
                liabilityAssumptionMap["postRetirement"],
                liabilityAssumptionMap["pensioner"],
                liabilityAssumptionMap["salaryIncreases"],
                liabilityAssumptionMap["RPI"],
                liabilityAssumptionMap["CPI"],
                liabilityAssumptionMap["assumptionLife65"],
                liabilityAssumptionMap["assumptionLife45"]
            );

            scheme.CurrentBasis.SetClientLiabilityAssumptions(liabAssumptionData);

            var json = liabilities();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetPensionIncreaseAssumptions(string pensionIncreaseAssumptions)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var penIncs =
                JsonConvert.DeserializeObject<Dictionary<string, double>>(pensionIncreaseAssumptions)
                    .ToDictionary(x => int.Parse(x.Key), x => x.Value);

            scheme.CurrentBasis.SetClientPensionIncreases(penIncs);

            var json = liabilities();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ResetLiabilityAssumptions()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            scheme.CurrentBasis.ResetClientLiabilityAssumptions();
            scheme.CurrentBasis.ResetClientPensionIncreasesAssumptions();

            var json = liabilities();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ResetAssetAssumptions()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            scheme.CurrentBasis.ResetClientAssetAssumptions();

            var json = assets();

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public class DataControl
        {
            public bool Visible { get; set; }
            public string Label { get; set; }
            public double Original { get; set; }
            public DataControl(string label, double original)
                : this(label, original, true)
            {
            }
            public DataControl(string label, double original, bool visible)
            {
                Label = label;
                Original = original;
                Visible = visible;
            }
        }

        [HttpPost]
        public ActionResult SetBasisAnalysis(int masterBasisId)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            if (scheme.GetBasesIdentifiers().Any(b => b.Id == masterBasisId))
            {
                scheme.SwitchBasis(masterBasisId);
            }

            var json = getBasisForAnalysisJSON(scheme);

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private object getBasisForAnalysisJSON(IPensionScheme scheme)
        {
            return new
            {
                basis = JsonHelper.ConvertBasisToJSON(scheme.CurrentBasis),
                showFutureBenefits = scheme.CurrentBasis.GetFutureBenefits() != null
            };
        }

        public ActionResult CashflowData(long d = 0)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null)
                return Json(null, JsonRequestBehavior.AllowGet);

            var cashflows = new CashflowData(Scheme.PensionScheme, d, isDataDirty(RequestType.Cashflow));

            return Json(cashflows, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AnalysisPlan(string components)
        {
            var scheme = Scheme.PensionScheme;

            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var wants = JsonConvert.DeserializeObject<string[]>(components);

            var fundingLevelData = scheme.CurrentBasis.GetFundingLevelData();

            var json = new
                {
                    anchor = JsonHelper.ConvertDateToJSON(scheme.CurrentBasis.AnchorDate),
                    start = JsonHelper.ConvertDateToJSON(scheme.CurrentBasis.AttributionStart),
                    date = JsonHelper.ConvertDateToJSON(scheme.CurrentBasis.AttributionEnd),
                    deficit = fundingLevelData.After.Deficit,
                    funded = scheme.Funded,
                    liabilities = wants.Contains("liabilities") ? liabilities() : null,
                    assets = wants.Contains("assets") ? assets() : null,
                    recoveryPlan = wants.Contains("recoveryPlan") ? recoveryPlan() : null,
                    investmentStrategy = wants.Contains("investmentStrategy") ? investmentStrategy() : null,
                    fro = wants.Contains("fro") ? fro() : null,
                    etv = wants.Contains("etv") ? etv() : null,
                    pie = wants.Contains("pie") ? pie() : null,
                    futureBenefits = wants.Contains("futureBenefits") ? futureBenefits() : null,
                    abf = wants.Contains("abf") ? abf() : null,
                    insurance = wants.Contains("insurance") ? insurance() : null
                };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSchemeAccess(int userId, int schemeId)
        {
            userProfileService.DeleteSchemeAccess(userId, schemeId);

            return new EmptyResult();
        }

        private void ResetAdditionalItems(IPensionScheme scheme)
        {
            // TODO: We shouldn't have to call this here. Previous step should have reset everything - refactor required?
            scheme.ResetClientInvestmentStrategyAllocations();
        }

        public class VarDataObject
        {
            public object setup { get; set; }
            public string basisName { get; set; }
            public IEnumerable<BreakdownItem> assetsBreakdown { get; set; }
            public object assetsProportions { get; set; }
            public string strategy { get; set; }
            public double buyin { get; set; }
            public double abf { get; set; }
            public double totalExistingAssets { get; set; }
            public double totalOtherAssets { get; set; }
            public double totalAssets { get; set; }
            public double cashAssetAmount { get; set; }
            public double cashInjection { get; set; }
            public double syntheticEquity { get; set; }
            public double syntheticCredit { get; set; }
            public bool ldiEnabled { get; set; }
            public bool hedgingInputsEnabled { get; set; }
            public double hedgingInterest { get; set; }
            public double hedgingInflation { get; set; }
            public double hedgingInterestLowerLimit { get; set; }
            public object hedgingInflationLowerLimit { get; set; }
            public object hedgingBreakdown { get; set; }
            public double assetMixTotalOtherProportion { get; set; }
            public object weightedReturnParameters { get; set; }
            public object giltYield { get; set; }
            public object syntheticAssetInfo { get; set; }
        }

        [HttpPost]
        public ActionResult SetPensionIncreasesUnlocked()
        {
            var scheme = Scheme.PensionScheme;

            scheme.CurrentBasis.DecoupleClientPensionIncreasesFromLiabilityAssumptions();

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult SetPensionIncreasesLocked()
        {
            var scheme = Scheme.PensionScheme;

            scheme.CurrentBasis.CoupleClientPensionIncreasesToLiabilityAssumptions();

            var pensionIncreases = viewService.GetPensionIncreasesCustom(scheme);

            var json = liabilities();

            return Json(json, JsonRequestBehavior.AllowGet);
        }
    }
}