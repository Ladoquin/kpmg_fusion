﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using FlightDeck.Services;
using Fusion.WebApp.Models;
using Fusion.WebApp.Filters;
using Fusion.WebApp.Models.Attributes;
using Fusion.WebApp.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml.Linq;
using WebMatrix.WebData;
using log4net;

namespace Fusion.WebApp.Controllers
{
    [Authorize]
    [OutputCache(Duration=0, NoStore=true, Location = System.Web.UI.OutputCacheLocation.None)]
    public class SchemeController : FusionBaseController
    {
        IPensionSchemeService _pensionSchemeService;
        public IPensionSchemeService PensionSchemeService
        {
            get
            {
                if (_pensionSchemeService == null) 
                    _pensionSchemeService = ServiceFactory.GetService<IPensionSchemeService>(this); 
                return _pensionSchemeService;
            }
            set
            {
                _pensionSchemeService = value;
            }
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [UserAdmit]
        public ActionResult Manage(int? id)
        {
            var model = new ManageSchemeViewModel();
            if (id.HasValue)
            {
                var scheme = PensionSchemeService.GetSummary(id.Value);
                if (scheme != null)
                {
                    model.AdditSchemeViewModel = mapSchemeDetailToAddit(scheme);
                    model.SchemeImportHistory = new List<IScheme> { scheme };
                }
            }
            else if (TempData.ContainsKey("ManageSchemeViewModel"))
            {
                model = (ManageSchemeViewModel)TempData["ManageSchemeViewModel"];
            }
            if (TempData.ContainsKey("AdditSchemeViewModel")) model.AdditSchemeViewModel = (AdditSchemeViewModel)TempData["AdditSchemeViewModel"];

            ViewBag.ReturnUrl = Url.Action("Manage");
            return View("Manage", model);
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        public ActionResult ListAll()
        {
            IEnumerable<IScheme> schemes = null;

            try
            {
                schemes = PensionSchemeService.GetAllSummaries().OrderBy(s => s.SchemeName);

                var model = new ManageSchemeViewModel
                {
                    ListSchemeViewModel = new ListSchemeViewModel
                    {
                        Schemes = schemes.ToList()
                    }
                };

                return View("List", model);
            }
            catch (Exception ex) 
            {
                LogManager.GetLogger(this.GetType()).ErrorFormat("User: {0}. {1}", User.Identity.Name, ex);
            }

            return View("List"); //no model and error message rendered for user.
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        public ActionResult Download(int id)
        {
            var source = PensionSchemeService.GetSchemeSource(id);

            if (source == null)
                return new EmptyResult();

            var schemeName = PensionSchemeService.GetSummary(id).SchemeName;

            var ms = new MemoryStream();
            source.Save(ms);
            ms.Position = 0;

            // force a 'save as...' prompt rather than linking directly to the xml output
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = schemeName + ".xml",
                Inline = false,
            };
            ControllerContext.HttpContext.Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(ms, "text/xml");
        }

        [HttpGet]
        public ActionResult List(SchemeRestrictionStatus state = SchemeRestrictionStatus.All)
        {
            return Json(GetSchemes(state), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Logo(String id = null)
        {
            byte[] logo = null;
            if (id == null)
            {
                logo = Scheme.Logo;
            }
            else
            {
                logo = PensionSchemeService.GetSummaryByName(id).Logo;
            }
            return new FileContentResult(logo, "image/png");
        }
        
        [HttpGet]
        public ActionResult Document(int id)
        {
            var doc = Scheme.Documents.SingleOrDefault(d => d.SchemeDocumentId == id);
            return new FileContentResult(doc.Content, doc.ContentType);
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [UserAdmit]
        public ActionResult Add()
        {
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View("Manage", new ManageSchemeViewModel());
        }

        [HttpPost]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AdditSchemeViewModel model)
        {
            return addit(model);
        }

        [HttpGet]
        public ActionResult Edit()
        {
            return RedirectToAction("Manage");
        }

        [HttpPost]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdditSchemeViewModel model)
        {
            if (ControllerContext.HttpContext.Request["update"] != null)
            {
                var refresh = string.Equals(model.CurrentSchemeName, Scheme != null ? Scheme.SchemeName : string.Empty);

                var result = addit(model);

                if (refresh) SessionManager.RefreshScheme();

                return result;
            }
            else
            {
                return delete(model);
            }
        }

        [HttpGet]
        public ActionResult Find()
        {
            return RedirectToAction("Manage");
        }

        [HttpPost]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        public ActionResult Find(FindSchemeViewModel model)
        {
            ViewBag.ReturnUrl = Url.Action("Manage");

            IScheme scheme = null;

            if (model.FindSchemeName != null)
            {
                try
                {
                    scheme = PensionSchemeService.GetSummaryByName(model.FindSchemeName);
                }
                catch { }
            }

            if (scheme != null)
            {
                var editModel = new ManageSchemeViewModel();
                editModel.AdditSchemeViewModel = mapSchemeDetailToAddit(scheme);
                editModel.AdditSchemeViewModel.Result = AdditSchemeResult.FindSchemeSuccess;
                editModel.SchemeImportHistory = new List<IScheme> { scheme };
                return View("Manage", editModel);
            }
            else
            {
                return View("Manage", new ManageSchemeViewModel { FindSchemeViewModel = new FindSchemeViewModel { Result = ResultEnum.Failure } });
            }
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [UserAdmit]
        public ActionResult ImportHistory(string schemeName)
        {
            var history = PensionSchemeService.GetImportHistory(schemeName).ToList();
            history.ForEach(x => x.Logo = null);
            return View(history.OrderByDescending(x => x.SchemeData_ImportedOnDate));
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [UserAdmit]
        public ActionResult Details(int id)
        {
            var scheme = PensionSchemeService.GetSchemeWithDetails(id);

            var model = new SchemeViewModel(scheme);

            return View(model);
        }

        [FusionAuthorize(RoleType.Admin, RoleType.Installer, RoleType.Consultant)]
        [HttpGet]
        public ActionResult CheckFavouriteScheme(string SchemeName)
        {
            var schemeId = 0;
            var scheme = PensionSchemeService.GetSummaryByName(SchemeName);
            if (scheme != null)
                schemeId = scheme.SchemeDetailId;
            var isFavourite = UserProfile.FavouriteSchemes.Any(x => string.Compare(x.SchemeName, SchemeName, true) == 0);
            return Json(new { isFavourite, schemeId }, JsonRequestBehavior.AllowGet);
        }

        private ActionResult delete(AdditSchemeViewModel model)
        {
            bool success = true;
            try
            {
                PensionSchemeService.DeleteById(model.Id);
            }
            catch
            {
                success = false;
            }

            if (success)
            {
                if (Scheme != null && Scheme.SchemeName == model.CurrentSchemeName) Session["UserProfile"] = null;

                TempData.Add("AdditSchemeViewModel", new AdditSchemeViewModel { Result = AdditSchemeResult.DeleteSchemeSuccess });

                return RedirectToAction("Manage");
            }
            else
            {
                model.Result = AdditSchemeResult.DeleteSchemeFailure;
                return View("Manage", new ManageSchemeViewModel { AdditSchemeViewModel = model });
            }
        }
      
        private ActionResult addit(AdditSchemeViewModel model)
        {
            if (Url != null)
                ViewBag.ReturnUrl = Url.Action("Manage");

            #region schemeDataXml
            XDocument schemeDataXml = null;
            try
            {
                schemeDataXml = model.SchemeData != null ? XDocument.Load(model.SchemeData.InputStream) : null;
            }
            catch
            {
                ModelState.AddModelError("SchemeData", "Unable to read XML");
            }
            #endregion
            #region logo
            Image logo = null;
            if (model.Logo != null)
            {
                try
                {
                    var rawLogo = Image.FromStream(model.Logo.InputStream);
                    logo = ConstrainSize(rawLogo, 240, 160);
                }
                catch
                {
                    ModelState.AddModelError("Logo", "Invalid image file for logo");
                }
            }
            #endregion
            #region documents
            // import documents
            var documents = new List<SchemeDocument>();

            if (ModelState.IsValid && model.DocumentIds.Any())
            {
                try
                {
                    for (int i = 0; i < model.DocumentIds.Count; i++)
                    {
                        int docId = model.DocumentIds[i];
                        bool deleteDoc = docId > 0 ? (model.DeleteDocuments[i] > 0) : false;
                        string docName = model.DocumentNames[i] == "Name..." ? null : model.DocumentNames[i]; // eek! sorry 
                        byte[] docContent = null;
                        if (model.Documents[i] != null)
                        {
                            using (var binaryReader = new BinaryReader(model.Documents[i].InputStream))
                            {
                                docContent = binaryReader.ReadBytes(model.Documents[i].ContentLength);
                            }
                        }

                        // if new doc
                        if (docId == -1)
                        {
                            if (!String.IsNullOrWhiteSpace(docName))
                            {
                                if(model.Documents[i] != null && !isAcceptableFileType(model.Documents[i].FileName))
                                {
                                    ModelState.AddModelError("Documents", "Documents accepted: " + string.Join(" ", AppSettings.AcceptedSchemeDocumentTypes)); 
                                    break;
                                }
                                else if (docContent == null)
                                {
                                    ModelState.AddModelError("Documents", "Missing file for doc " + docName.Trim());
                                    break;
                                }
                                else
                                {
                                    documents.Add(new SchemeDocument()
                                    {
                                        Name = docName.Trim(),
                                        FileName = model.Documents[i].FileName,
                                        Content = docContent,
                                        ContentType = model.Documents[i].ContentType
                                    });
                                }
                            }
                            else
                            {
                                if (docContent != null)
                                {
                                    ModelState.AddModelError("Documents", "Missing name for document " + model.Documents[i].FileName);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            // check if need to update 
                            if (!deleteDoc && !String.IsNullOrWhiteSpace(docName))
                            {
                                if (model.Documents[i] != null && !isAcceptableFileType(model.Documents[i].FileName))
                                {
                                    ModelState.AddModelError("Documents", "Documents accepted: " + string.Join(" ", AppSettings.AcceptedSchemeDocumentTypes));
                                    break;
                                }

                                documents.Add(new SchemeDocument()
                                {
                                    SchemeDocumentId = docId,
                                    Name = docName,
                                    FileName = docContent == null ? model.DocumentFileNames[i] : model.Documents[i].FileName,
                                    Content = docContent,
                                    ContentType = docContent == null ? null : model.Documents[i].ContentType
                                });
                            }
                        }
                    }
                }
                catch
                {
                    ModelState.AddModelError("Documents", "Error importing document content");
                }
            }
            #endregion

            SchemeDetail schemeDetail = null;
            DetailedResult<ISchemeAdminServiceResult, IScheme> result = null;

            try
            {
                // validate scheme name and xml
                if (ModelState.IsValid)
                {
                    schemeDetail = new SchemeDetail
                        {
                            SchemeName = model.NewSchemeName,
                            Logo = logo != null ? ImageToPngBytes(logo) : null,
                            IsRestricted = model.IsRestricted,
                            AccountingDownloadsPassword = (model.AccountingDownloadsPassword ?? "") == "Set password..." ? null : model.AccountingDownloadsPassword
                        };
                    
                    result =
                        PensionSchemeService.Save(
                            model.IsEditScheme ? model.CurrentSchemeName : null,
                            schemeDetail,
                            documents,
                            User.Identity.Name,
                            UserProfile.UserId,
                            schemeDataXml: schemeDataXml);
                    if(result.Success && documents != null && model.DeleteDocuments.Any(x => x > 0))
                    {
                        foreach (int i in model.DeleteDocuments.Where(x => x > 0).ToList())
                            PensionSchemeService.DeleteDocument(model.Id, i);
                    }
                    else if(!result.Success)
                    {
                        if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLError))
                        {
                            ModelState.AddModelError("ImportErrorDetails", "");
                        }
                        else
                        {
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.NameConflict))
                                ModelState.AddModelError("NewSchemeName", "Scheme name already in use");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.NameMismatch))
                                ModelState.AddModelError("SchemeData", "Mismatched scheme name");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.ReferenceConflict))
                                ModelState.AddModelError("SchemeData", "Scheme reference already in use");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLNameMissing))
                                ModelState.AddModelError("SchemeData", "Missing scheme name");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLReferenceMissing))
                                ModelState.AddModelError("SchemeData", "Missing scheme ref");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLDateMissing))
                                ModelState.AddModelError("SchemeData", "Missing date / time ");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLDateInvalid))
                                ModelState.AddModelError("SchemeData", "Invalid date / time");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLUsernameMissing))
                                ModelState.AddModelError("SchemeData", "Missing username ");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLSpreadsheetPathMissing))
                                ModelState.AddModelError("SchemeData", "Missing spreadsheet path");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLEffectiveDateMissing))
                                ModelState.AddModelError("SchemeData", "Missing effective date");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLEffectiveDateInvalid))
                                ModelState.AddModelError("SchemeData", "Invalid effective date");
                            if (result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLDataCaptureVersionMissing))
                                ModelState.AddModelError("SchemeData", "Missing data-capture/tracker version");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("ImportErrorDetails", e.Message);
            }

            // render appropriately
            var manageModel = new ManageSchemeViewModel();
            if (ModelState.IsValid)
            {
                manageModel.AdditSchemeViewModel = mapSchemeDetailToAddit(result.Result);
                manageModel.AdditSchemeViewModel.Result = model.IsEditScheme ? AdditSchemeResult.EditSchemeSuccess : AdditSchemeResult.AddSchemeSuccess;
                manageModel.SchemeImportHistory = new List<IScheme> { result.Result };
            }
            else
            {
                model.Result = model.IsEditScheme ? AdditSchemeResult.EditSchemeFailure : AdditSchemeResult.AddSchemeFailure;
                model.ImportFileSpreadsheetName = schemeDetail != null ? schemeDetail.SchemeData_Spreadsheet : string.Empty;
                model.HasLogo = schemeDetail != null && schemeDetail.Logo != null;

                if (result != null && result.ValidationErrors != null && result.ValidationErrors.Contains(ISchemeAdminServiceResult.XMLError))
                {
                    model.ImportErrorAt = result.DataImportProgress;
                    model.ImportErrorDetails = result.DataImportException.Message;
                }
                manageModel.AdditSchemeViewModel = model;
            }


            var userProfile = UserProfile;
            if (Scheme != null && Scheme.SchemeName == model.CurrentSchemeName) Session["UserProfile"] = null;
            
            return View("Manage", manageModel);
        }

        private bool isAcceptableFileType(string docName)
        {
            var accepted = false;

            foreach(var acceptableType in AppSettings.AcceptedSchemeDocumentTypes)
            {
                if(docName.EndsWith(acceptableType))
                {
                    accepted = true;
                    break;
                }
            }

            return accepted;
        }

        private AdditSchemeViewModel mapSchemeDetailToAddit(IScheme scheme)
        {
            return new AdditSchemeViewModel
            {
                IsEditScheme = true,
                CurrentSchemeName = scheme.SchemeName,
                IsRestricted = scheme.IsRestricted,
                Id = scheme.SchemeDetailId,
                HasLogo = scheme.Logo != null,
                ExportFile = scheme.SchemeData_ExportFile,

                NewSchemeName = scheme.SchemeName,
                ImportFileSpreadsheetName = scheme.SchemeData_Spreadsheet,

                DocumentIds = scheme.Documents != null ? scheme.Documents.Select(x => x.SchemeDocumentId).ToList() : new List<int>(),
                DocumentNames = scheme.Documents != null ? scheme.Documents.Select(x => x.Name).ToList() : new List<string>(),
                DocumentFileNames  = scheme.Documents != null ? scheme.Documents.Select(x => x.FileName).ToList() : new List<string>(),
                DeleteDocuments = new List<int>(),

                SourceXmlExists = scheme.SchemeDetailId > 0 && PensionSchemeService.GetSchemeSource(scheme.SchemeDetailId) != null
            };
        }

        private List<string> GetSchemes(SchemeRestrictionStatus state)
        {
            List<string> schemes = null;

            try
            {
                schemes = PensionSchemeService.GetAllSummaries()
                    .Where(s => state == SchemeRestrictionStatus.All || s.IsRestricted == (state == SchemeRestrictionStatus.Restricted))
                    .Select(s => s.SchemeName).ToList();
            }
            catch (Exception ex) 
            {
                LogManager.GetLogger(this.GetType()).ErrorFormat("User: {0}. {1}", User.Identity.Name, ex);
            }

            return schemes;
        }
    }
}
