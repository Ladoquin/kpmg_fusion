﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.WebApp.Filters;
using Fusion.WebApp.Models;
using Fusion.WebApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Fusion.WebApp.Controllers
{
    [Authorize]
    [SchemeAdmit]
    public class EvolutionController : FusionBaseController
    {
        private IViewService _viewService;
        public IViewService ViewService
        {
            private get
            {
                if (_viewService == null)
                    _viewService = ServiceFactory.GetService<IViewService>(this);
                return _viewService;
            }
            set { _viewService = value; }
        }

        [HttpGet]
        public ActionResult Index()
        {
            var scheme = Scheme.PensionScheme;
            if (scheme.CurrentBasis.EvolutionStartDate.HasValue && scheme.CurrentBasis.EvolutionEndDate.HasValue)
                scheme.SetAttributionPeriod(scheme.CurrentBasis.EvolutionStartDate.GetValueOrDefault(), scheme.CurrentBasis.EvolutionEndDate.GetValueOrDefault());

            return View("Evolution", new SchemeViewModel(Scheme));
        }

        //
        // GET: /Json/Tracker        
        [HttpGet]
        public ActionResult Tracker()
        {
            //TODO are all these conversions still necesssary now we have refactored back end?

            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var data = scheme.CurrentBasis.Evolution.Values;

            var tracker = from trackerItem in data
                          select new
                          {
                              date = JsonHelper.ConvertDateToJSON(trackerItem.Date),
                              asset = trackerItem.TotalAssetsPlusBuyin,
                              liability = trackerItem.TotalLiability
                          };

            var fundingLevelTracker = tracker.Select(x => new { x.date, value = Math.Round(x.asset / x.liability * 100, 1) });
            var expectedFundingLevelTracker = scheme.CurrentBasis.ExpectedDeficit.Select(x => new { date = JsonHelper.ConvertDateToJSON(x.Key), value = Math.Round(x.Value.FundingLevel * 100, 1) });

            var expectedBalances = new List<double>();
            if (scheme.CurrentBasis.BasisType == BasisType.TechnicalProvision)
            {
                expectedBalances = scheme.CurrentBasis.ExpectedDeficit.Select(x => x.Value.Deficit).ToList();
            }

            var assetImportDates = scheme.AssetEffectiveDates.Select(x => JsonHelper.ConvertDateToJSON(x));
            var liabilityImportDates = scheme.CurrentBasis.EffectiveDates.Select(x => JsonHelper.ConvertDateToJSON(x));

            var comparisonData = scheme.GetAllBases()
                .Select(x => new
                {
                    basis = x.Name,
                    points = x.Evolution
                        .Where(e => e.Key >= scheme.CurrentBasis.AnchorDate)
                        .Select(e => new { date = JsonHelper.ConvertDateToJSON(e.Value.Date), value = e.Value.TotalLiability })
                });

            return Json(new
            {
                tracker,
                showAssets = scheme.Funded,
                expectedBalances,
                fundingLevelTracker,
                expectedFundingLevelTracker,
                startDate = JsonHelper.ConvertDateToJSON(scheme.CurrentBasis.AttributionStart),
                endDate = JsonHelper.ConvertDateToJSON(scheme.CurrentBasis.AttributionEnd),
                assetImportDates,
                liabilityImportDates,
                basis = new { type = scheme.CurrentBasis.BasisType.ToString() },
                comparisonData
            }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Json/AnalysisOfSurplus
        public ActionResult AnalysisOfSurplus(long? start = null, long? end = null)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var startDate = start.HasValue ? JsonHelper.UnixEpoch.AddMilliseconds(start.Value) : scheme.CurrentBasis.AttributionStart;
            var endDate = end.HasValue ? JsonHelper.UnixEpoch.AddMilliseconds(end.Value) : scheme.CurrentBasis.AttributionEnd;

            if (startDate != scheme.CurrentBasis.AttributionStart || endDate != scheme.CurrentBasis.AttributionEnd)
            {
                scheme.SetAttributionPeriod(startDate, endDate);
            }

            var data = scheme.CurrentBasis.GetSurplusAnalysisData().After;
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EvolutionAssumptionsPanel()
        {
            var scheme = Scheme.PensionScheme;

            var labels = new LabelService(this).GetDefaultAssumptionLabels(Scheme.PensionScheme.CurrentBasis);
            var liabilityAssumptions = new
            {
                initial = ViewService.GetLiabilityAssumptions(scheme, scheme.CurrentBasis.AttributionStart, labels),
                current = ViewService.GetLiabilityAssumptionsCustom(scheme, labels).Select(x => new { x.Key, x.Label, Value = x.Value.Before })
            };

            var pensionIncreases = new
            {
                initial = ViewService.GetPensionIncreases(scheme, scheme.CurrentBasis.AttributionStart),
                current = ViewService.GetPensionIncreases(scheme, scheme.CurrentBasis.AttributionEnd)
            };

            var assetsInfo = ViewService.GetSchemeAssetData(scheme, AssumptionsSourceType.Default);
            var hedging = scheme.Funded ? scheme.CurrentBasis.GetHedgingBreakdown() : null;
            var ldiData = hedging == null ? null : hedging.Before;

            var assetsGrowth = ViewService.GetSchemeAssetDataWithGrowth(scheme)
                .Where(x => x.Amount > 0)
                .OrderBy(x => x.Category.ToString()).ThenBy(x => x.Class.DisplayName()).ThenBy(x => x.Name)
                .ToList();
            if (assetsGrowth.Any(x => x.IsSynthetic))
            {
                //move synthetics to top of list
                var syntheticGrowths = assetsGrowth.Where(x => x.IsSynthetic).ToList();
                assetsGrowth.RemoveAll(x => x.IsSynthetic);
                foreach (var synth in syntheticGrowths.OrderByDescending(x => x.Name))
                    assetsGrowth.Insert(0, synth);
            }

            assetsGrowth.ForEach(x => { if (x.Class == AssetClassType.Buyin)
                                            x.Growth = null;
                                      });

            var json = new
            {
                showAssets = scheme.Funded,
                liabilitiesBreakdown = ViewService.GetLiabilitiesSplit(scheme, scheme.CurrentBasis.AttributionEnd),
                assetsBreakdown = assetsInfo.Funds,
                assetsGrowth,
                syntheticAssetInfo = assetsInfo.SyntheticAssetInfo,
                liabilityAssumptions = liabilityAssumptions,
                pensionIncreases = pensionIncreases,
                marketData = scheme.GetMarketInfo(),
                schemeData = ViewService.GetEvolutionSchemeData(scheme),
                ldiData = ldiData != null
                    ?
                    new
                    {
                        interestRate = ldiData.CurrentBasis[HedgeType.Interest].Total,
                        inflation = ldiData.CurrentBasis[HedgeType.Inflation].Total,
                        cashflowInterestRate = ldiData.Cashflows[HedgeType.Interest].Total,
                        cashflowInflation = ldiData.Cashflows[HedgeType.Inflation].Total
                    }
                    : null
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EvolutionAssets()
        {
            return PartialView("_EvolutionAssets");
        }

        public ActionResult EvolutionPensionIncreases()
        {
            return PartialView("_EvolutionPensionIncreases");
        }

        public ActionResult EvolutionLiabilities()
        {
            return PartialView("_EvolutionLiabilities");
        }

        public ActionResult EvolutionMarketData()
        {
            return PartialView("_EvolutionMarketData");
        }

        public ActionResult EvolutionSchemeData()
        {
            return PartialView("_EvolutionSchemeData");
        }
    }
}
