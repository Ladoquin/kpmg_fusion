﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.WebApp.App_Start;
using Fusion.WebApp.Extensions;
using Fusion.WebApp.Models;
using Fusion.WebApp.Services;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace Fusion.WebApp.Controllers
{
    [Authorize]
    public class AccountController : FusionBaseController
    {
        private IMembershipProvider _membershipProvider;
        public IMembershipProvider MembershipProvider
        {
            get
            {
                if (_membershipProvider == null)
                    _membershipProvider = new BasicMembershipProvider();
                return _membershipProvider;
            }
            set { _membershipProvider = value; }
        }

        IUserProfileService _userProfileService;
        IUserProfileService userProfileService
        {
            get
            {
                if (_userProfileService == null) 
                    _userProfileService = ServiceFactory.GetService<IUserProfileService>(this); 
                return _userProfileService;
            }
        }

        IAccountService _accountService;
        IAccountService accountService
        {
            get
            {
                if (_accountService == null) 
                    _accountService = ServiceFactory.GetService<IAccountService>(this); 
                return _accountService;
            }
        }

        IEmailService _emailService;
        IEmailService emailService 
        {
            get
            {
                if (_emailService == null) 
                    _emailService = ServiceFactory.GetService<IEmailService>(this); 
                return _emailService;
            }
        }

        IPasswordManager _passwordManager;
        IPasswordManager PasswordManager
        {
            get
            {
                if (_passwordManager == null)
                    _passwordManager = ServiceFactory.GetService<IPasswordManager>(this);
                return _passwordManager;
            }
            set { _passwordManager = value; }
        }

        IApplicationSettings _applicationSettings;
        IApplicationSettings ApplicationSettings
        {
            get
            {
                if (_applicationSettings == null)
                    _applicationSettings = ServiceFactory.GetService<IApplicationSettings>(this);
                return _applicationSettings;
            }
        }

        private class Action
        {
            public enum TypeEnum { PasswordChange, UsernameChange, NameEmailChange, SchemeChange }
            public TypeEnum Type { get; set; }
            public ResultEnum Result { get; set; }
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(bool @new = false, int timedout = 0)
        {
            if (WebSecurity.IsAuthenticated)
            {
                checkLoginNotification(WebSecurity.CurrentUserName);

                return RedirectToRoute("Home");
            }
            return View(new LoginViewModel { ShowTimeoutMessage = timedout == 1 || !Session.IsNewSession && !@new });
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            var modelok = ModelState.IsValid;
            var userexists = modelok && WebSecurity.UserExists(model.UserName);
            var notlocked = userexists && WebSecurity.GetPasswordFailuresSinceLastSuccess(model.UserName) <= AppSettings.AllowedFailedLoginAttempts;
            var loginok = notlocked && WebSecurity.Login(model.UserName, model.Password);

            if (loginok)
            {
                checkLoginNotification(model.UserName);
            }
            else
            {
                if (modelok)
                {
                    var cause = string.Empty;
                    if (!userexists)
                        cause = "User not found";
                    else if (!notlocked)
                        cause = "Account locked";
                    else if (!loginok)
                        cause = "Credentials incorrect";
                    LogManager.GetLogger("account-login").WarnFormat("User login failed. Username: {0}, cause: {1}", model.UserName, cause);
                }
                model.ShowSignInFailed = true;
            }

            var results = new
                {
                    response = loginok,
                    partialView = RazorViewRenderer.RenderRazorViewToString("_LoginPanel", model)
                };

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Use this LogOff overload to successfully log off.
        /// </summary>
        /// <param name="form">Ignore, pass null if necessary, this argument is only being used to distinguish this method overload from the HttpGet one.</param>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogOff(object form)
        {
            if (WebSecurity.IsAuthenticated)
            {
                WebSecurity.Logout();
            }
            if (Session != null)
            {
                Session.Abandon();
            }
            removeCookie(FormsAuthentication.FormsCookieName);
            removeCookie("ASP.NET_SessionId");
            removeCookie("__RequestVerificationToken");

            return RedirectToAction("Login", new { @new = true });
        }

        /// <summary>
        /// Ignore this LogOff overload, it is only used for handling a GET request to the POST url, ie, in the case of a forms authentication timed-out redirect.
        /// </summary>
        [HttpGet]
        public ActionResult LogOff()
        {
            return RedirectToAction("Index", "Home", new { @new = true });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string username, string token)
        {
            var account = userProfileService.GetByUsername(username, true);

            if (account == null || account.PasswordMustChange)
            {
                return View(new PasswordResetViewModel()
                    {
                        Token = token,
                        OldPasswordRequired = string.IsNullOrEmpty(token),
                        UserName = !string.IsNullOrEmpty(username) ? username : WebSecurity.CurrentUserName,
                        IsCreate = true,
                        ShowPasswordRules = account != null || !string.IsNullOrEmpty(token)
                    });
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(PasswordResetViewModel model)
        {
            var logger = LogManager.GetLogger("account-login");

            var usingToken = !string.IsNullOrEmpty(model.Token);

            if (ModelState.IsValid)
            {
                if (!usingToken)
                {
                    if (string.IsNullOrEmpty(model.OldPassword))
                    {
                        ModelState.AddModelError("OldPassword", "Required");
                    }
                }
            }

            var tokenExpired = false;

            if (ModelState.IsValid)
            {
                //validate user
                if (!usingToken && !MembershipProvider.ValidateUser(User.Identity.Name, model.OldPassword)) //not using token so must supply old password
                {
                    //A failed password attempt here counts as a password failure
                    var profile = SessionManager.GetUserProfile();
                    profile.PasswordFailuresSinceLastSuccess++;
                    SessionManager.SetUserProfile(profile);

                    //Too many password failures then lock and logoff
                    if (profile.PasswordFailuresSinceLastSuccess > AppSettings.AllowedFailedLoginAttempts)
                        return LogOff(null);
                }
                else if (PasswordManager.IsPasswordValid(model.UserName, model.OldPassword, model.NewPassword)) //changing password
                {
                    bool changePasswordSucceeded;
                    if (string.IsNullOrEmpty(model.OldPassword)) //no old password so changing password with token (e.g. on first login)
                    {
                        changePasswordSucceeded = PasswordManager.ResetPassword(model.UserName, model.Token, model.NewPassword);
                        if (!changePasswordSucceeded)
                        {
                            tokenExpired = true;
                        }
                    }
                    else
                    {
                        logger.Debug("Calling ChangePassword as Reset");
                        changePasswordSucceeded = PasswordManager.ChangePassword(model.UserName, model.OldPassword, model.NewPassword);
                    }

                    new SessionManagerService(this).SetUserProfile(
                        accountService.SetPasswordFlag(model.UserName, false));

                    if (changePasswordSucceeded)
                    {
                        if (!WebSecurity.IsAuthenticated)
                        {
                            WebSecurity.Login(model.UserName, model.NewPassword);
                        }
                        checkLoginNotification(model.UserName);
                        return RedirectToRoute("Home");
                    }
                    else
                    {
                        if (tokenExpired)
                        {
                            ModelState.AddModelError("", "Temporary password has expired");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Unable to reset password.");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("NewPassword", "Invalid");
                }
            }

            // If we got this far, something failed, redisplay form
            model.HasError = true;
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgottenUsername()
        {
            if (WebSecurity.IsAuthenticated) return RedirectToAction("Manage");

            var model = TempData["ForgottenUsernameViewModel"] == null ? new ForgottenUsernameViewModel() : (ForgottenUsernameViewModel)TempData["ForgottenUsernameViewModel"];

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgottenUsername(ForgottenUsernameViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = userProfileService.GetByEmail(model.Email, true);

                    model.RequestReceived = true;

                    TempData.Add("ForgottenUsernameViewModel", model);

                    if (user != null)
                    {
                        emailService.SendEmail(
                            user.EmailAddress,
                            user.FirstName + " " + user.LastName,
                            "KPMG Fusion.",
                            RazorViewRenderer.RenderRazorViewToString("_ForgottenUsernameEmail", user),
                            string.Format("We have received a username reminder reset for your account with KPMG Fusion. Your username is: {0}", user.UserName));
                    }
                    else
                    {
                        LogManager.GetLogger("account-login").DebugFormat("ForgottenUsername: email '{0}' not found", model.Email);
                    }

                    return RedirectToAction("ForgottenUsername");
                }
                catch
                {
                    ModelState.AddModelError("Email", "Cannot process request");
                }
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgottenPassword()
        {
            if (WebSecurity.IsAuthenticated) return RedirectToAction("Manage");

            return View(new ForgottenPasswordViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgottenPassword(ForgottenPasswordViewModel model)
        {
            if (WebSecurity.IsAuthenticated) return RedirectToAction("Manage");

            if (ModelState.IsValid)
            {
                model.Submitted = true;

                var userfound = false;
                try
                {
                    var user = userProfileService.GetByUsername(model.UserName, true);
                    if (user != null)
                    {
                        userfound = true;

                        accountService.SetPasswordFlag(model.UserName, true);

                        emailService.SendEmail(
                            user.EmailAddress,
                            user.FirstName + " " + user.LastName,
                            "KPMG Fusion.",
                            RazorViewRenderer.RenderRazorViewToString("_ForgottenPasswordEmail",
                                new UserEmailViewModel
                                {
                                    UserName = model.UserName,
                                    Token = WebSecurity.GeneratePasswordResetToken(model.UserName)
                                }),
                            "Plain text."
                        );
                    }
                    else
                    {
                        LogManager.GetLogger("account-login").DebugFormat("ForgottenPassword: username '{0}' not found", model.UserName);

                        // pretend that we're doing something
                        System.Threading.Thread.Sleep(5000);
                    }
                }
                catch
                {
                    if (userfound)
                        model.HasError = true;
                }
            }
            else
            {
                model.HasError = true;
            }

            // send all 
            return View(model);
        }

        /******************
         * Ready switch in for when this functionality is activated on landing page
         * 
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgottenUsername(string email)
        {
            if (!string.IsNullOrWhiteSpace(email))
            {
                try
                {
                    var user = _userProfileService.GetByEmail(email, true);
                    if (user != null)
                    {
                        _emailService.SendEmail(
                            user.EmailAddress,
                            user.FirstName + " " + user.LastName,
                            "KPMG Fusion.",
                            RenderRazorViewToString("_ForgottenUsernameEmail", user),
                            string.Format("We have received a username reminder reset for your account with KPMG Fusion. Your username is: {0}", user.UserName));
                    }
                }
                catch { }
            }

            return new EmptyResult();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgottenPassword(string username)
        {
            if (WebSecurity.IsAuthenticated) return RedirectToAction("Manage");

            ViewBag.ReturnUrl = Url.Action("Login");

            if (!string.IsNullOrWhiteSpace(username))
            {
                try
                {
                    var user = _userProfileService.GetByUsername(username, true);
                    if (user != null)
                    {
                        _emailService.SendEmail(
                            user.EmailAddress,
                            user.FirstName + " " + user.LastName,
                            "KPMG Fusion.",
                            RenderRazorViewToString("_ForgottenPasswordEmail",
                                new UserEmailViewModel
                                {
                                    UserName = username,
                                    Token = WebSecurity.GeneratePasswordResetToken(username)
                                }),
                            "Plain text."
                        );
                    }
                }
                catch { }
            }

            return new EmptyResult();
        }
        */

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetStarted(GetStartedModel model)
        {
            try
            {
                emailService.SendEmail(
                    AppSettings.ContactEmail,
                    "Fusion",
                    "Fusion demonstration request",
                    RazorViewRenderer.RenderRazorViewToString("_GetStartedEmail", model),
                    "");
            }
            catch(Exception ex)
            {
                LogManager.GetLogger(this.GetType()).ErrorFormat("User: {0}. {1}", User.Identity.Name, ex);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Manage()
        {
            return View(AccountModel);
        }


        private AccountUpdateModel AccountModel
        {
            get
            {
                var debugger = log4net.LogManager.GetLogger(this.GetType().ToString() + ".get_AccountModel");

                debugger.Debug("Logging get_AccountModel to discover when null exception is occurring");

                var account = new AccountUpdateModel();
                var profile = UserProfile;

                debugger.Debug("Set PasswordUpdateModel");

                account.PasswordUpdateModel.ChangePasswordRequired = profile.PasswordMustChange;

                debugger.Debug("Set UserNameUpdateModel");

                account.UserNameUpdateModel.NewUserName = User.Identity.Name;

                debugger.Debug("Set NameEmailUpdateModel");

                account.NameEmailUpdateModel.FirstName = profile.FirstName;
                account.NameEmailUpdateModel.LastName = profile.LastName;
                account.NameEmailUpdateModel.EmailAddress = profile.EmailAddress;

                debugger.Debug("Set account");
                
                account.AllowSchemeChange = profile.IsInRole(RoleType.Admin, RoleType.Installer, RoleType.Consultant);
                account.AllowUsernameChange = true;

                if (profile.ActiveSchemeDetailId != null)
                {
                    debugger.Debug("Set SchemeNameUpdateModel");

                    account.SchemeNameUpdateModel.SchemeName = Scheme == null /* might be null if problems loading */ ? SessionManager.GetSchemeName() : Scheme.SchemeName;
                    account.SchemeNameUpdateModel.SchemeDetailId = profile.ActiveSchemeDetailId.Value;

                    debugger.Debug("Set SchemeNameUpdateModel.IsFavourite");

                    if (Scheme != null)
                        account.SchemeNameUpdateModel.IsFavourite = profile.FavouriteSchemes.Any(x => x.SchemeDetailId == Scheme.SchemeDetailId);
                }
                if (TempData.ContainsKey("action"))
                {
                    debugger.Debug("Handle action");

                    var action = (Action)TempData["action"];

                    debugger.DebugFormat("Action type: {0}", action.Type.ToString());

                    switch (action.Type)
                    {
                        case Action.TypeEnum.PasswordChange:
                            account.PasswordUpdateModel.Result.Add(action.Result);
                            break;
                        case Action.TypeEnum.NameEmailChange:
                            account.NameEmailUpdateModel.Result.Add(action.Result);
                            break;
                        case Action.TypeEnum.SchemeChange:
                            // check if broken scheme selected
                            if (TempData.ContainsKey("InvalidScheme") && (bool)TempData["InvalidScheme"])
                                action.Result = ResultEnum.Failure;
                            account.SchemeNameUpdateModel.Result.Add(action.Result);
                            break;
                        case Action.TypeEnum.UsernameChange:
                            account.UserNameUpdateModel.Result.Add(action.Result);
                            break;
                        default:
                            break;
                    }
                }
                return account;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(PasswordUpdateModel model)
        {
            var lockedOut = false;

            if (changePassword(model, ref lockedOut))
            {
                TempData.Add("action", new Action { Type = Action.TypeEnum.PasswordChange, Result = ResultEnum.Success });
                return RedirectToAction("Manage");
            } else if(lockedOut == true)
            {
                return LogOff(null);
            }

            // If we got this far, something failed, redisplay form
            var account = AccountModel;
            account.PasswordUpdateModel = model;
            account.PasswordUpdateModel.Result.Add(ResultEnum.Failure);
            return View("Manage", account);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeUserName(UserNameUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (MembershipProvider.ValidateUser(User.Identity.Name, model.CurrentPassword))
                    {
                        var success = false;
                        
                        if (User.Identity.Name != model.NewUserName)
                        {
                            if (userProfileService.Update(User.Identity.Name, updatedusername: model.NewUserName)
                                    .Success)
                            {
                                WebSecurity.Logout();
                                WebSecurity.Login(model.NewUserName, model.CurrentPassword);

                                success = true;
                            }
                            else
                            {
                                ModelState.AddModelError("NewUserName", "Username already in use");
                            }
                        }
                        else
                        {
                            success = true;
                        }
                        if (success)
                        {
                            TempData.Add("action", new Action { Type = Action.TypeEnum.UsernameChange, Result = ResultEnum.Success });
                            return RedirectToAction("Manage");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("CurrentPassword", "Incorrect password");
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger(this.GetType()).ErrorFormat("Change user name error. {0}", ex);
                }

            }

            // If we got this far, something failed, redisplay form
            var account = AccountModel;
            account.UserNameUpdateModel.NewUserName = model.NewUserName;
            account.UserNameUpdateModel.Result.Add(ResultEnum.Failure);
            return View("Manage", account);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeNameEmail(NameEmailUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (userProfileService.Update(
                            User.Identity.Name,
                            firstname: model.FirstName,
                            lastname: model.LastName,
                            email: model.EmailAddress)
                        .Success)
                    {
                        new SessionManagerService(this).RefreshUserProfile();

                        TempData.Add("action", new Action { Type = Action.TypeEnum.NameEmailChange, Result = ResultEnum.Success });

                        return RedirectToAction("Manage");
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger(this.GetType()).Error("ChangeNameEmail", ex);
                }
            }

            // If we got this far, something failed, redisplay form
            var account = AccountModel;
            account.NameEmailUpdateModel = model;
            account.NameEmailUpdateModel.Result.Add(ResultEnum.Failure);
            return View("Manage", account);
        }

        [HttpPost]
        public ActionResult ChangeSchemeJson(string schemeName)
        {
            if(userProfileService.ChangeActiveScheme(User.Identity.Name, schemeName ?? string.Empty)
                .Success)
            {
                new SessionManagerService(this)
                    .RefreshUserProfile()
                    .RefreshScheme();
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeScheme(SchemeNameUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (userProfileService.ChangeActiveScheme(User.Identity.Name, model.SchemeName ?? string.Empty)
                        .Success)
                    {
                        new SessionManagerService(this)
                            .RefreshUserProfile()
                            .RefreshScheme();

                        TempData.Add("action", new Action { Type = Action.TypeEnum.SchemeChange, Result = ResultEnum.Success });
                        return RedirectToAction("Manage");
                    }
                    else
                    {
                        ModelState.AddModelError("SchemeName", "Unknown scheme");
                        var failureModel = AccountModel;
                        failureModel.SchemeNameUpdateModel = model;
                        failureModel.SchemeNameUpdateModel.Result.Add(ResultEnum.Failure);
                        return View("Manage", failureModel);
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger(this.GetType()).Error("ChangeScheme", ex);
                }
            }

            // If we got this far, something failed, redisplay form
            var account = AccountModel;
            account.SchemeNameUpdateModel = model;
            account.SchemeNameUpdateModel.Result.Add(ResultEnum.Failure);
            return View("Manage", account);

        }

        [HttpPost]
        public ActionResult AddRemoveFavouriteScheme(int SchemeId, bool removeFavourite)
        {
            var userService = userProfileService;
            var result = false;
            var schemeName = "";

            if (removeFavourite)
                result = userService.RemoveFavouriteScheme(UserProfile, SchemeId);
            else
                result = userService.AddFavouriteScheme(UserProfile, SchemeId);

            if (result && !removeFavourite)
            {
                var scheme = UserProfile.FavouriteSchemes.Where(x => x.SchemeDetailId == SchemeId).First();
                schemeName = scheme != null ? scheme.SchemeName : "";
            }
            return Json(new { result, SchemeId, schemeName }, JsonRequestBehavior.AllowGet);
        }

        #region Helpers

        private bool changePassword(PasswordUpdateModel model, ref bool lockedOut)
        {
            var logger = LogManager.GetLogger("account-login");

            var changePasswordSucceeded = false;

            if (ModelState.IsValid)
            {
                if (MembershipProvider.ValidateUser(User.Identity.Name, model.OldPassword))
                {
                    if (PasswordManager.IsPasswordValid(User.Identity.Name, model.OldPassword, model.NewPassword))
                    {
                        if (PasswordManager.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                        {
                            new SessionManagerService(this).SetUserProfile(
                                accountService.SetPasswordFlag(User.Identity.Name, false));

                            changePasswordSucceeded = true;
                        }
                    }
                }
                else
                {
                    logger.Warn("Old password incorrect");

                    //A failed password attempt here counts as a password failure
                    var profile = SessionManager.GetUserProfile();
                    profile.PasswordFailuresSinceLastSuccess++;
                    SessionManager.SetUserProfile(profile);

                    //Too many password failures then lock and logoff
                    if (profile.PasswordFailuresSinceLastSuccess > AppSettings.AllowedFailedLoginAttempts)
                        lockedOut = true;
                }
                if (!changePasswordSucceeded)
                {
                    ModelState.AddModelError("NewPassword", "Invalid password");
                }
            }

            return changePasswordSucceeded;
        }

        private void removeCookie(string cookieName)
        {
            var cookie = new HttpCookie(cookieName, string.Empty);
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Username already exists. Please enter a different username.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A username for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The username provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        private void checkLoginNotification(string username)
        {
            try
            {
                var role = (RoleType)Enum.Parse(typeof(RoleType), Roles.GetRolesForUser(username)[0]);
                var user = userProfileService.GetByUsername(username, allowAnonymous: true);

                if (role == RoleType.Admin && !AppSettings.AdminLoginNotificationEmail.IsNullOrEmpty())
                {
                    sendClientLoginNotificationEmail(role.ToString(), user, AppSettings.AdminLoginNotificationEmail);
                }
                else if ((role == RoleType.Client || role == RoleType.Adviser) && !AppSettings.LoginNotificationEmail.IsNullOrEmpty())
                {
                    var recipients = getListOfRecipients(user);
                    sendClientLoginNotificationEmail(role.ToString(), user, recipients);
                }
            }
            catch(Exception e)
            {
                LogManager.GetLogger("account-login").ErrorFormat("Exception when calling checkLoginNotification(). {0}", e.ToString());
            }
        }

        private IEnumerable<string> getListOfRecipients(UserProfile user)
        {
            var recipients = new List<string>();
            recipients.AddRange(AppSettings.LoginNotificationEmail);

            if(user.ActiveSchemeDetail != null && !string.IsNullOrEmpty(user.ActiveSchemeDetail.ContactEmail))
            {
                recipients.Add(user.ActiveSchemeDetail.ContactEmail);
            }

            return recipients;
        }

        private void sendClientLoginNotificationEmail(string role, UserProfile user, IEnumerable<string> recipients)
        {
            
            var schemeName = user.ActiveSchemeDetail != null ? user.ActiveSchemeDetail.SchemeName : "Unassigned";
            var dataCaptureVersion = user.ActiveSchemeDetail != null ? (user.ActiveSchemeDetail.DataCaptureVersion ?? string.Empty) : string.Empty;
            var schemeSupported = ApplicationSettings.IsSchemeSupported(dataCaptureVersion);
            emailService.SendEmailAsync(
                recipients.Select(x => new Tuple<string, string>(x, x)),
                string.Format("Fusion login: {0}", user.UserName),
                RazorViewRenderer.RenderRazorViewToString("_LoginNotificationEmail", new LoginNotificationViewModel { Role = role, FirstName = user.FirstName, LastName = user.LastName, UserName = user.UserName, Scheme = schemeName, Email = user.EmailAddress, Date = DateTime.Now, SchemeSupported = schemeSupported, DataCaptureVersion = dataCaptureVersion }),
                string.Format("{0} user, {1} {2} has logged in to Fusion", role, user.FirstName, user.LastName));
                    }

        #endregion
    }
}
