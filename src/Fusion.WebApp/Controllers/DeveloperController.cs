﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fusion.WebApp.Controllers
{
    [Authorize(Users="logviewer")]
    public class DeveloperController : FusionBaseController
    {
        private ILogReader _logReader;

        public ILogReader LogReader
        {
            private get
            {
                if (_logReader == null)
                    _logReader = ServiceFactory.GetService<ILogReader>(this);
                return _logReader;
            }
            set //TODO internal? 
            {
                _logReader = value;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Log(string date, string level)
        {
            DateTime d;
            if (!DateTime.TryParse(date, out d))
            {
                d = DateTime.Now;
            }

            var entries = LogReader.GetAll(d, level) ?? new List<Log>();

            var model = new Fusion.WebApp.Models.LogViewModel
                {
                    Date = d,
                    Level = level,
                    Entries = entries.OrderBy(x => x.Date)
                };

            return View(model);
        }
    }
}
