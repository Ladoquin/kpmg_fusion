﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.WebApp.Filters;
using Fusion.WebApp.Services;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;

namespace Fusion.WebApp.Controllers
{    
    [HandleError(ExceptionType = typeof(HttpAntiForgeryException), View = "Unauthorised")]
    [HandleExceptions]
    public abstract class FusionBaseController : Controller
    {
        private IAccountSecurityService _webSecurity;
        public IAccountSecurityService WebSecurity
        {
            protected get
            {
                if (_webSecurity == null)
                    _webSecurity = new BasicWebSecurity();
                return _webSecurity;
            }
            set { _webSecurity = value; }
        }

        private ISessionManager _sessionManager;
        public ISessionManager SessionManager
        {
            protected get
            {
                if (_sessionManager == null)
                    _sessionManager = new SessionManagerService(this);
                return _sessionManager;
            }
            set { _sessionManager = value; }
        }

        private IRazorViewRenderer _razorViewRenderer;
        public IRazorViewRenderer RazorViewRenderer
        {
            protected get
            {
                if (_razorViewRenderer == null)
                    _razorViewRenderer = new RazorViewRenderer(this);
                return _razorViewRenderer;
            }
            set { _razorViewRenderer = value; }
        }

        public UserProfile UserProfile 
        { 
            get 
            {
                var profile = SessionManager.GetUserProfile();
                if (profile == null)
                {
                    if (WebSecurity.IsAuthenticated)
                    {
                        WebSecurity.Logout();
                    }
                    RedirectToRoute("Home");
                    throw new Exception("Thread aborted");
                }
                return profile;
            } 
        }

        public IScheme Scheme
        {
            get
            {
                var scheme = SessionManager.GetScheme();
                // check if scheme broken
                if (scheme != null && scheme.PensionScheme == null)
                {
                    // to add warning if broken scheme
                    if (!TempData.ContainsKey("InvalidScheme"))
                        TempData.Add("InvalidScheme", true);
                }
                else
                {
                    TempData.Remove("InvalidScheme");
                }

                return scheme;
            }
        }

        public void RefreshScheme()
        {
            SessionManager.RefreshScheme();
        }

        public static Image ConstrainSize(Image image, int width, int height)
        {
            float scale = (float) width/image.Width;

            float interimHeight = image.Height*scale;

            if (interimHeight > height)
            {
                scale = scale*(height/interimHeight);
            }

            width = Math.Min((int)Math.Round(scale*image.Width), width);
            height = Math.Min((int)Math.Round(scale*image.Height), height);

            var resizedImage = new Bitmap(width, height, PixelFormat.Format32bppPArgb);
            resizedImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            Graphics graphics = Graphics.FromImage(resizedImage);
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            graphics.DrawImage(image,
                new Rectangle(0, 0, width, height),
                new Rectangle(0, 0, image.Width, image.Height),
                GraphicsUnit.Pixel);

            graphics.Dispose();

            return resizedImage;
        }

        public UserAgentType UserAgent
        {
            get
            {
                if (ControllerContext.HttpContext.Request.UserAgent.ToLower().Contains("ipad"))
                    return UserAgentType.iPad;

                return UserAgentType.Default;
            }
        }

        public static byte[] ImageToPngBytes(Image img)
        {
            if (img == null) return null;

            byte[] bytes = null;
            using (var stream = new MemoryStream())
            {
                img.Save(stream, ImageFormat.Png);
                stream.Close();
                bytes = stream.ToArray();
            }
            return bytes;
        }
    }
}
