﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using FlightDeck.Services;
using Fusion.WebApp.Filters;
using Fusion.WebApp.Models;
using Fusion.WebApp.Models.Attributes;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Fusion.WebApp.Controllers
{
    public class CurveDataController : FusionBaseController
    {

        private ICurveImportDetailService _curveService;
        public ICurveImportDetailService CurveService
        {
            private get
            {
                if (_curveService == null)
                    _curveService = ServiceFactory.GetService<ICurveImportDetailService>(this);
                return _curveService;
            }
            set { _curveService = value; }
        }

        private IXmlSchemaValidator _xmlSchemaValidator;
        public IXmlSchemaValidator XmlSchemaValidator
        {
            private get
            {
                if (_xmlSchemaValidator == null)
                    _xmlSchemaValidator = ServiceFactory.GetService<IXmlSchemaValidator>(this);
                return _xmlSchemaValidator;
            }
            set { _xmlSchemaValidator = value; }
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [UserAdmit]
        public ActionResult Manage()
        {
            var lastestImportDetail = CurveService.GetLatest();
            return View("Manage", new CurveImportViewModel() { ImportDetail = lastestImportDetail });
        }

        [HttpGet]
        public ActionResult Save()
        {
            return RedirectToAction("Manage");
        }

        [HttpPost]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        [ValidateAntiForgeryToken]
        public ActionResult Save(CurveImportViewModel model)
        {
            var logger = LogManager.GetLogger("curve-import");
            var stopwatch = new Stopwatch();

            model.Submitted = true;

            var allowInvalidSubmission = User.IsInRole(RoleType.Admin.ToString());

            if (ModelState.IsValid)
            {
                logger.InfoFormat("Uploading curves file '{0}'", model.ImportFile.FileName);
                stopwatch.Start();

                var curves = new BinaryReader(model.ImportFile.InputStream).ReadBytes((int)model.ImportFile.InputStream.Length);

                stopwatch.Stop();
                logger.InfoFormat("Uploaded curves file, time taken: {0} seconds", stopwatch.Elapsed.TotalSeconds.ToString());

                using (var xsd = Assembly.GetExecutingAssembly().GetManifestResourceStream("Fusion.WebApp.Resources.curves.xsd"))
                {
                    logger.InfoFormat("Validate xml");

                    var xmlok = XmlSchemaValidator.Validate(new MemoryStream(curves), xsd);

                    if (xmlok)
                    {
                        logger.InfoFormat("Validation ok");

                        model.ImportDetail = save(model, curves);
                    }
                    else
                    {
                        logger.InfoFormat("Validation failed");

                        ModelState.AddModelError("ImportFile", "Xml not valid");

                        model.ValidationMessages = XmlSchemaValidator.Messages;

                        XmlSchemaValidator.Messages.ForEach(m => logger.InfoFormat("Import file '{0}' validation message: {1}", model.ImportFile.FileName, m));

                        if (allowInvalidSubmission)
                        {
                            TempData["curves"] = curves;
                            TempData["model"] = model;
                        }
                    }
                }
            }

            if (ModelState.IsValid)
            {
                model.Result = ResultEnum.Success;
            }
            else
            {
                var lastestImportDetail = CurveService.GetLatest();
                model.Result = ResultEnum.Failure;
                model.ImportDetail = lastestImportDetail;
                model.AllowInvalidSubmission = allowInvalidSubmission;
            }

            return View("Manage", model);
        }

        /// <summary>
        /// Ignore this ForceSave overload, it is only used for handling a GET request to the POST url, ie, in the case of a forms authentication timed-out redirect.
        /// </summary>
        [HttpGet]
        public ActionResult ForceSave()
        {
            return RedirectToAction("Manage");
        }

        /// <summary>
        /// Use this ForceSave overload to attempt the save of the upload.
        /// </summary>
        /// <param name="form">Ignore, pass null if necessary, this argument is only being used to distinguish this method overload from the HttpGet one.</param>
        [HttpPost]
        [FusionAuthorize(RoleType.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult ForceSave(object form)
        {
            if (TempData.ContainsKey("curves"))
            {
                var stats = TempData["curves"] as byte[];
                var model = TempData["model"] as CurveImportViewModel;

                model.ImportDetail = save(model, stats);
                model.Result = ModelState.IsValid ? ResultEnum.Success : model.Result = ResultEnum.Failure;

                return View("Manage", model);
            }

            return RedirectToAction("Manage");
        }

        private CurveImportDetail save(CurveImportViewModel model, byte[] curves)
        {
            var ok = false;
            var importDetail = new CurveImportDetail();
            var xdoc = new XDocument();
            var logger = LogManager.GetLogger("curve-data-import");
            var stopwatch = new Stopwatch();

            model.ValidationMessages = new List<string>();

            try
            {
                logger.Info("Load curves stream");

                var xml = new MemoryStream(curves);

                logger.Info("Load xml doc");

                xdoc = XDocument.Load(xml);

                logger.Info("Importing curves file");
                stopwatch.Start();

                CurveService.ImportCurves(xdoc);

                stopwatch.Stop();
                logger.InfoFormat("Imported curves file '{0}', time taken: {1} seconds", model.ImportFile.FileName, stopwatch.Elapsed.TotalSeconds.ToString());

                ok = true;
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ImportFile", "Failures during import");
                logger.Error(string.Format("Import curves failed, file '{0}'", model.ImportFile.FileName), ex);
            }

            if (ok)
            {
                importDetail = CurveService.Save(xdoc, User);
            }
            else
            {
                importDetail = CurveService.GetLatest();
            }

            return importDetail;
        }

    }
}
