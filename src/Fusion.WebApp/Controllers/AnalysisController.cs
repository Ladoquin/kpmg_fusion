﻿namespace Fusion.WebApp.Controllers
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.ServiceInterfaces.Reports;
    using Fusion.WebApp.Filters;
    using Fusion.WebApp.Models;
    using Fusion.WebApp.Models.Attributes;
    using System;
    using System.Linq;
    using System.Web.Mvc;

    [FusionAuthorize(RoleType.Admin, RoleType.Client, RoleType.Consultant, RoleType.Installer)]
    [SchemeAdmit]
    public class AnalysisController : FusionBaseController
    {
        #region Injectable services

        private IAccountingReporter _accountingReporter;
        public IAccountingReporter AccountingReporter
        {
            get
            {
                if (_accountingReporter == null) 
                    _accountingReporter = ServiceFactory.GetService<IAccountingReporter>(this); 
                return _accountingReporter;
            }
            set
            {
                _accountingReporter = value;
            }
        }

        private IClientAssumptionService _clientAssumptionService;
        public IClientAssumptionService clientAssumptionService
        {
            get
            {
                if (_clientAssumptionService == null) 
                    _clientAssumptionService = ServiceFactory.GetService<IClientAssumptionService>(this);
                return _clientAssumptionService;
            }
            set
            {
                _clientAssumptionService = value;
            }
        }

        private ISchemeSnapshotService _schemeSnapshotService;
        public ISchemeSnapshotService SchemeSnapshotService
        {
            get
            {
                if (_schemeSnapshotService == null)
                    _schemeSnapshotService = ServiceFactory.GetService<ISchemeSnapshotService>(this);
                return _schemeSnapshotService;
            }
            set
            {
                _schemeSnapshotService = value;
            }
        }

        #endregion

        public ActionResult Index()
        {
            var futureBen = Scheme.PensionScheme.CurrentBasis.GetFutureBenefits();
            ViewBag.FutureServiceEnabled = futureBen != null && futureBen.After.AnnualCost > 0;
            ViewBag.HasAccountingBasis = Scheme.PensionScheme.GetBasisTypes().Contains(BasisType.Accounting);
            ViewBag.EnableAccountingLens = Scheme.PensionScheme.CurrentBasis.BasisType == BasisType.Accounting;
            ViewBag.EnableInsurancePanel = Scheme.PensionScheme.GetBasisTypes().Contains(BasisType.Buyout);
            ViewBag.EnableABFPanel = Scheme.PensionScheme.CurrentBasis.BasisType != BasisType.Accounting;

            return View("Analysis", new AnalysisViewModel(Scheme)
                {
                    SavedScenarios = clientAssumptionService.GetAll(Scheme.SchemeDetailId),
                    AllowScenarioDownload = User.IsInRole(RoleType.Admin.ToString()) || User.IsInRole(RoleType.Installer.ToString())
                });
        }

        [HttpGet]
        [DateTimeValidator]
        public ActionResult Disclosure(AccountingStandardType type, string start, string end)
        {
            var s = Scheme.PensionScheme.CurrentBasis.AttributionStart;
            var e = Scheme.PensionScheme.CurrentBasis.AttributionEnd;

            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(start))
                    s = JsonHelper.UnixEpoch.AddMilliseconds(long.Parse(start));
                if (!string.IsNullOrEmpty(end))
                    e = JsonHelper.UnixEpoch.AddMilliseconds(long.Parse(end));
            }

            var outputType = UserAgent == UserAgentType.iPad ? AccountingReportOutputType.ToText : AccountingReportOutputType.Default;
                
            var ms = AccountingReporter.Create(Scheme, type, s, e, outputType);

            return File(ms, @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", type.ToString() + ".xlsx");
        }

        [HttpGet]
        [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
        public ActionResult ResultsDump()
        {
            var ms = SchemeSnapshotService.GetSnapshot(Scheme.PensionScheme);

            // force a 'save as...' prompt rather than linking directly to the xml output
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = string.Format("FusionCalcResults_{0}_{1}_v{2}_{3}.xml", Scheme.SchemeName.Replace(" ", "").Replace(".", ""), Request.Url.Host, AppSettings.Version.ToString().Replace(".", "-"), DateTime.Now.ToString("yyMMddHHmmss")),
                Inline = false,
            };
            ControllerContext.HttpContext.Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(ms, "text/xml");
        }
    }
}