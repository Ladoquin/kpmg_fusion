﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.WebApp.Models;
using Fusion.WebApp.Filters;
using System.Web.Mvc;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Collections;

namespace Fusion.WebApp.Controllers
{
    [Authorize]
    [UserAdmit]
    public class HomeController : FusionBaseController
    {
        private IApplicationSettings applicationSettings 
        { 
            get 
            { 
                return ServiceFactory.GetService<IApplicationSettings>(this); 
            } 
        }

        private IViewService _viewService;
        private IViewService viewService
        {
            get
            {
                if (_viewService == null)
                    _viewService = ServiceFactory.GetService<IViewService>(this);
                return _viewService;
            }
        }

        public ActionResult Index()
        {
            if (Scheme == null || Scheme.PensionScheme == null)
            {
                if (UserProfile.ActiveSchemeDetailId.GetValueOrDefault() > 0)
                {
                    return View("SchemeError", null, SessionManager.GetSchemeName());
                }
                return View("NoSchemeHome");
            }
            else
            {
                if (!applicationSettings.IsSchemeSupported(Scheme.DataCaptureVersion))
                {
                    return RedirectToRoute("UnsupportedScheme");
                }
            }

            var importantMessage = applicationSettings.GetImportantMessage() ?? new ImportantMessage();

            return View("SchemeHome", new SchemeHomeViewData(Scheme)
                    {
                        ImportantMessage = importantMessage.Message,
                        ImportantMessageKey = importantMessage.Key.ToString(),
                        Ias19Disclosure = Scheme.PensionScheme.DataSource.Ias19Disclosure,
                        ValuationReport = Scheme.PensionScheme.DataSource.ValuationReport,
                        AssetData = Scheme.PensionScheme.DataSource.AssetData,
                        MemberData = Scheme.PensionScheme.DataSource.MemberData,
                        BenefitData = Scheme.PensionScheme.DataSource.BenefitData
                    });
        }

        [HttpGet]
        public ActionResult Get()
        {
            var scheme = Scheme.PensionScheme;

            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var date = scheme.RefreshDate;

            var summaryData = viewService.GetSummary(scheme, date);
            var evolutionData = viewService.GetEvolution(scheme, date);
            var buyinState = viewService.GetBuyinStatus(scheme);

            var isd = scheme.CurrentBasis.GetInvestmentStrategyData(AttributionEndType.RefreshDate);
            var syntheticAssetInfo = viewService.GetSchemeAssetDataAtRefreshDate(scheme).SyntheticAssetInfo;
            var assetTotal = isd.TotalAssets;

            var assetsBreakdown = viewService.GetAssetsBreakdownAt(scheme, scheme.RefreshDate);

            var liabs = scheme.CurrentBasis.GetLiabilityData(AttributionEndType.RefreshDate);
            var liabilitySplit = new List<BreakdownItem>
                {
                    new BreakdownItem("Active", liabs.ActivePast), 
                    new BreakdownItem("Deferred", liabs.Deferred),
                    new BreakdownItem("Pensioner", liabs.Pensioners)
                };

            //TODO is the balance shown on the home page the difference between the asset and liab total above,
            //or should it match the funding level Tracker data, which takes in to account FRO/ETV etc? 
            var balance = assetTotal - (liabs.ActivePast + liabs.Deferred + liabs.Pensioners);

            var ldiData = scheme.Funded ? scheme.CurrentBasis.GetHedgingDataAtRefreshDate() : null;
            
            Func<EvolutionDataItem, object> tojson = e => e == null ? null : new { e.Asset, e.Liability, e.Balance, Date = e.Date.ToString("dd MMM yyyy") };

            var json = new
            {
                date = JsonHelper.ConvertDateToJSON(date),
                basis = JsonHelper.ConvertBasisToJSON(scheme.CurrentBasis),
                balance = balance,
                fundingTarget = summaryData.FundingTarget,
                assetsBreakdown,
                liabilitiesBreakdown = liabilitySplit,
                buyinState = buyinState.HasValue ? (buyinState.Value - JsonHelper.UnixEpoch).TotalMilliseconds : (double?)null,
                syntheticAssetInfo,
                evolution = new
                {
                    d0 = tojson(evolutionData[EvolutionElapsedPeriodType.NoElapse]),
                    d1 = tojson(evolutionData[EvolutionElapsedPeriodType.Day1]),
                    m1 = tojson(evolutionData[EvolutionElapsedPeriodType.Month1]),
                    m3 = tojson(evolutionData[EvolutionElapsedPeriodType.Month3]),
                    m6 = tojson(evolutionData[EvolutionElapsedPeriodType.Month6]),
                    y1 = tojson(evolutionData[EvolutionElapsedPeriodType.Year1])
                },
                ldiData = ldiData != null
                    ? new {
                        interestRate = ldiData.Breakdown.CurrentBasis[HedgeType.Interest].Total,
                        inflation = ldiData.Breakdown.CurrentBasis[HedgeType.Inflation].Total,
                        cashflowInterestRate = ldiData.Breakdown.Cashflows[HedgeType.Interest].Total,
                        cashflowInflation = ldiData.Breakdown.Cashflows[HedgeType.Inflation].Total
                    }
                    : null
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [AjaxAuthorize]
        public ActionResult CashflowData(long d = 0)
        {

            var scheme = Scheme.PensionScheme;
            if (scheme == null)
                return Json(null, JsonRequestBehavior.AllowGet);

            var cashflows = new CashflowData(Scheme.PensionScheme, d, false);

            return Json(cashflows, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetBasis(int masterBasisId)
        {
            var scheme = Scheme.PensionScheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            if (scheme.GetBasesIdentifiers().Any(b => b.Id == masterBasisId))
            {
                scheme.SwitchBasis(masterBasisId);
            }

            var json = new { basis = JsonHelper.ConvertBasisToJSON(scheme.CurrentBasis) };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

    }
}
