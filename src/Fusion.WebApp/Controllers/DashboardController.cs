﻿using FlightDeck.DomainShared;
using Fusion.WebApp.Models;
using Fusion.WebApp.Models.Attributes;
using Fusion.WebApp.Models.Analysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fusion.WebApp.Controllers
{
    [FusionAuthorize(RoleType.Admin, RoleType.Client, RoleType.Consultant, RoleType.Installer)]
    public class DashboardController : FusionBaseController
    {
        public ActionResult AnalysisAssets()
        {
            return PartialView("_AnalysisAssets");
        }

        public ActionResult AnalysisLiabilities()
        {
            var futureBen = Scheme.PensionScheme.CurrentBasis.GetFutureBenefits();
            ViewBag.FutureServiceEnabled = futureBen != null && futureBen.After.AnnualCost > 0;

            return PartialView("_AnalysisLiabilities");
        }
        public ActionResult RecoveryPlan()
        {
            return PartialView("_RecoveryPlan");
        }
        public ActionResult Fro()
        {
            return PartialView("_Fro");
        }
        public ActionResult FutureBenefits()
        {
            return PartialView("_FutureBenefits");
        }
        public ActionResult Pie()
        {
            return PartialView("_Pie");
        }
        public ActionResult Etv()
        {
            return PartialView("_Etv");
        }
        public ActionResult Abf()
        {
            return PartialView("_Abf");
        }
        public ActionResult Insurance()
        {
            return PartialView("_Insurance");
        }
        public ActionResult InvestmentStrategy()
        {
            var scheme = Scheme;
            if (scheme == null) return Json(null, JsonRequestBehavior.AllowGet);

            var classes =
                Utils.EnumToArray<AssetClassType>()
                    .Select(x => new
                        {
                            id = (int)x,
                            name = x.ToString(),
                            category = Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(x).Category,
                            @readonly = x == AssetClassType.Abf
                        })
                    .Where(x => x.category != AssetCategory.None)
                    .OrderBy(x => x.category).ThenBy(x => x.name)
                    .ToList();

            //cash has to come last
            var cash = classes.SingleOrDefault(x => x.id == (int)AssetClassType.Cash);
            classes.Remove(cash);
            classes.Add(cash);

            var abf = classes.SingleOrDefault(x => x.id == (int)AssetClassType.Abf);
            if (abf != null)
            {
                // move abf to the end
                classes.Remove(abf);
                classes.Add(abf); 
            }

            var model = new InvestmentStrategyViewModel(scheme) 
                { 
                    AssetClasses = classes.Select(x => new KeyedToggleItem { Key = x.id.ToString(), Label = x.name, Readonly = x.@readonly, Disabled = x.id < 0 }).ToList()
                };

            return PartialView("_InvestmentStrategy", model);
        }


    }
}
