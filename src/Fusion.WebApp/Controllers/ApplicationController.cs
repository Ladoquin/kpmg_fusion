﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.WebApp.Models.Attributes;
using System.Web.Mvc;
using Fusion.WebApp.Extensions;

namespace Fusion.WebApp.Controllers
{
    [FusionAuthorize(RoleType.Admin)]
    public class ApplicationController : Controller
    {
        IApplicationSettings _applicationSettings;
        IApplicationSettings applicationSettings 
        { 
            get 
            { 
                if (_applicationSettings == null) 
                    _applicationSettings = ServiceFactory.GetService<IApplicationSettings>(this); 
                return _applicationSettings; 
            } 
        }

        [HttpGet]
        public ActionResult Manage()
        {
            var model = new SettingsViewData { ImportantMessage = (applicationSettings.GetImportantMessage() ?? new ImportantMessage()).Message };

            model.ShowSuccess = TempData.GetValue<bool>("ShowSuccess");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(SettingsViewData data)
        {
            if (ModelState.IsValid)
            {
                applicationSettings.SaveImportantMessage(data.ImportantMessage);

                TempData.Add("ShowSuccess", true);

                return RedirectToAction("Manage");
            }
            else
            {
                return View(data);
            }
        }
    }
}
