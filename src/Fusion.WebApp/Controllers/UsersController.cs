﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.WebApp.Models;
using Fusion.WebApp.Filters;
using Fusion.WebApp.Models.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using System.ComponentModel.DataAnnotations;
using FlightDeck.Services;
using Fusion.WebApp.Services;
using log4net;

namespace Fusion.WebApp.Controllers
{
    [FusionAuthorize(RoleType.Admin, RoleType.Installer)]
    public class UsersController : FusionBaseController
    {
        #region Services

        private IMembershipProvider _membershipProvider;
        public IMembershipProvider MembershipProvider
        {
            get
            {
                if (_membershipProvider == null)
                    _membershipProvider = new BasicMembershipProvider();
                return _membershipProvider;
            }
            set { _membershipProvider = value; }
        }

        private IUserProfileService _userProfileService;
        public IUserProfileService UserProfileService
        {
            get
            {
                if (_userProfileService == null)
                    _userProfileService = ServiceFactory.GetService<IUserProfileService>(this);
                return _userProfileService;
            }
            set { _userProfileService = value; }
        }

        private IEmailService _emailService;
        public IEmailService EmailService
        {
            get
            {
                if (_emailService == null)
                    _emailService = ServiceFactory.GetService<IEmailService>(this);
                return _emailService;
            }
            set { _emailService = value; }
        }

        private IAccountService _accountService;
        public IAccountService AccountService
        {
            get
            {
                if (_accountService == null)
                    _accountService = ServiceFactory.GetService<IAccountService>(this);
                return _accountService;
            }
            set { _accountService = value; }
        }

        private IRoleManager _roleManager;
        public IRoleManager RoleManager
        {
            private get
            {
                if (_roleManager == null)
                    _roleManager = ServiceFactory.GetService<IRoleManager>(this);
                return _roleManager;
            }
            set { _roleManager = value; }
        }

        private IApplicationSettings _applicationSettings;
        public IApplicationSettings ApplicationSettings
        {
            private get
            {
                if (_applicationSettings == null)
                    _applicationSettings = ServiceFactory.GetService<IApplicationSettings>(this);
                return _applicationSettings;
            }
            set { _applicationSettings = value; }
        }

        private ITemplateService _templateService;
        public ITemplateService TemplateService
        {
            private get
            {
                if (_templateService == null)
                    _templateService = ServiceFactory.GetService<ITemplateService>(this);
                return _templateService;
            }
            set { _templateService = value; }
        }

        #endregion

        private enum PageAction { Find, Edit, Delete, Add };

        private class ManageData
        {
            public ManageData(PageAction action) 
            { 
                Action = action;
                Model = new ManageUserViewModel();
            }
            public PageAction Action { get; set; }
            public ManageUserViewModel Model { get; set; }
        }

        //
        // GET: /Users/Manage
        [HttpGet]
        [UserAdmit]
        public ActionResult Manage(int? id)
        {
            var model = new ManageUserViewModel
                {
                    AdditUserViewModel = new AdditUserViewModel { Role = RoleType.Client, RolesAvailable = getAvailableRoles(), RandomPassword = AppSettings.AddUserRandomPassword }
                };

            if (id.GetValueOrDefault() > 0)
            {
                var profile = UserProfileService.GetById(id.Value);
                if (profile != null)
                {
                    model.IsEditing = true;
                    model.AdditUserViewModel.Result = AdditUserResult.FindUserSuccess;
                    model.AdditUserViewModel = new AdditUserViewModel
                        {
                            CurrentUserName = profile.UserName,
                            Role = profile.Role,
                            UserName = profile.UserName,
                            RolesAvailable = getAvailableRoles(),
                            FirstName = profile.FirstName,
                            LastName = profile.LastName,
                            EmailAddress = profile.EmailAddress,
                            Schemes = !profile.SchemeDetails.IsNullOrEmpty() ? profile.SchemeDetails.Select(s => s.SchemeName) : new List<string>()
                        };
                }
            }
            else if (TempData.ContainsKey("data"))
            {
                var data = (ManageData)TempData["data"];
                switch (data.Action)
                {
                    case PageAction.Add:
                    case PageAction.Edit:
                        model.AdditUserViewModel.Result = AdditUserResult.UserSaveSuccess;
                        break;                
                    case PageAction.Delete:
                        model.AdditUserViewModel.Result = AdditUserResult.DeleteUserSuccess;
                        break;
                    case PageAction.Find:
                    default:
                        break;
                }
            }

            return View("Manage", model);
        }

        //
        // GET: /Users/List
        [HttpGet]
        public ActionResult List()
        {
            var users = GetUsers();
            return Json(users.Keys, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListAll(UserListParams searchParams = null)
        {
            searchParams = searchParams ?? new UserListParams();
            searchParams.PageNumber = searchParams.PageNumber == 0 ? 1 : searchParams.PageNumber;
            var sc = new UserProfileSearchCriteria
                {
                    NameDescending = searchParams.NameDescending,
                    RoleFilter = searchParams.RoleFilter,
                    SchemeFilter = searchParams.SchemeName
                };

            var searchResults = UserProfileService.Get(sc, searchParams.PageNumber - 1, AppSettings.PageSize);

            searchParams.TotalMatches = searchResults.TotalMatches;

            var records = searchParams.NameDescending ? searchResults.UserPageSet.OrderByDescending(u => u.LastName) : searchResults.UserPageSet.OrderBy(u => u.LastName);

            var users = records.ToList()
                    .Select(u => new UserListItem { 
                        Id = u.UserId,
                        Email = u.EmailAddress, 
                        Forename = u.FirstName,
                        Surname = u.LastName,
                        UserName = u.UserName, 
                        Role = (RoleType)Enum.Parse(typeof(RoleType), Roles.GetRolesForUser(u.UserName).First()),
                        Schemes = u.SchemeDetails.OrderBy(x => x.SchemeName),
                        IsLocked = u.PasswordFailuresSinceLastSuccess > AppSettings.AllowedFailedLoginAttempts
                    })
                    .ToList();

            var model = new ManageUserViewModel { ListUserViewModel = new ListUserViewModel { Users = users, Params = searchParams } };

            if (Request.IsAjaxRequest())
                return PartialView("_List", model.ListUserViewModel);

            return View("List", model);
        }

        private Dictionary<string, int> GetUsers()
        {
            Dictionary<string, int> userCache = null;

            try
            {
                userCache = UserProfileService.GetSearchIndex(new List<int> { UserProfile.UserId });
            }
            catch (Exception ex) 
            {
                LogManager.GetLogger(this.GetType()).ErrorFormat("User: {0}. {1}", User.Identity.Name, ex);
            }

            return userCache; //when userCache is null the admin user sees an error message when searching for a given user
        }

        [HttpGet]
        public ActionResult Add()
        {
            return RedirectToAction("Manage");
        }
       
        //
        // POST: /Users/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AdditUserViewModel model)
        {
            // remove password validation (if configured) FUSN-630 - Kam: 29/07/14
            if (AppSettings.AddUserRandomPassword && model.Role != RoleType.Admin )
            { 
                ModelState.Remove("Password");

                // Add random password
                model.Password = GetRandomPassword(8);
            }

            // set random password property
            model.RandomPassword = AppSettings.AddUserRandomPassword;

            foreach (var error in model.Validate(new ValidationContext(model, null, null)))
                foreach (var memberName in error.MemberNames)
                    ModelState.AddModelError(memberName, error.ErrorMessage);

            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    MembershipProvider.CreateUserAndAccount(model.UserName, model.Password, new Dictionary<string, object> { 
                            { "FirstName", model.FirstName }, 
                            { "LastName", model.LastName },
                            { "EmailAddress", model.EmailAddress}, 
                            { "TermsAccepted", false},
                            { "CreatedByUserId", UserProfile.UserId }
                        });
                    RoleManager.AddUserToRole(model.UserName, model.Role.Value.ToString());

                    if (UserProfileService.Update(model.UserName, model.Role.Value, model.Schemes, model.FirstName, model.LastName, model.EmailAddress, model.UserName).Success)
                    {
                        if (model.Role == RoleType.Consultant)
                        {
                            UserProfileService.ChangeActiveScheme(model.UserName, ApplicationSettings.GetConsultantDefaultSchemeName());
                        }

                        var content = 
                            TemplateService.GenerateContent(
                                Path.Combine(new PathProvider(this.HttpContext).GetTemplatesFolder(), "welcome_email.htm"),
                                new Dictionary<string, string>
                                    {
                                        { "@username", model.UserName },
                                        { "@forename", model.FirstName },
                                        { "@surname", model.LastName },
                                        { "@url", Url.Action("ResetPassword", "Account", new { username = model.UserName, token = MembershipProvider.GeneratePasswordResetToken(model.UserName, AppSettings.WelcomeEmailExpirationDays * 24 * 60) }, "http" )},
                                        { "@expiration", string.Format("{0} day{1}", AppSettings.WelcomeEmailExpirationDays.ToString(), AppSettings.WelcomeEmailExpirationDays > 1 ? "s" : "") }
                                    });

                        EmailService.SendEmailAsync(
                            model.EmailAddress,
                            model.FirstName + " " + model.LastName,
                            "KPMG Fusion - Welcome.",
                            content,
                            "Plain text."
                        );

                        // send a copy of welcome email to KPMG (FUSN-577) - Kam: 29/07/14
                        string sToEmailAddress = AppSettings.WelcomeNotificationEmail;
                        if (!string.IsNullOrWhiteSpace(sToEmailAddress))
                        {
                            EmailService.SendEmailAsync(
                                sToEmailAddress,
                                sToEmailAddress,
                                "KPMG Fusion - Welcome.",
                                content,
                                "Plain text."
                            );
                        }

                        TempData.Add("data", new ManageData(PageAction.Add));
                        return RedirectToAction("Manage");
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    var error = NewUserErrorMessage(e.StatusCode);
                    ModelState.AddModelError(error.Key, error.Value);
                }
            }

            // If we got this far, something failed, redisplay form
            model.Result = AdditUserResult.AddUserFailure;
            model.RolesAvailable = getAvailableRoles();
            return View("Manage", new ManageUserViewModel { IsEditing = true, AdditUserViewModel = model });
        }

        [HttpGet]
        public ActionResult Find()
        {
            return RedirectToAction("Manage");
        }

        [HttpPost]
        public ActionResult Find(FindUserViewModel model)
        {
            var allUsers = GetUsers();

            if (model.FindUserName != null && allUsers.ContainsKey(model.FindUserName))
            {
                return RedirectToAction("Manage", new { id = allUsers[model.FindUserName] });
            }

            return View("Manage", new ManageUserViewModel
            {
                FindUserViewModel = new FindUserViewModel { Result = ResultEnum.Failure } ,
                AdditUserViewModel = new AdditUserViewModel {RolesAvailable = getAvailableRoles()}
            });
        }

        [HttpGet]
        public ActionResult Edit()
        {
            return RedirectToAction("Manage");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdditUserViewModel model)
        {
            return Request["delete"] != null ? delete(model) : edit(model);
        }

        [HttpGet]
        public ActionResult Unlock()
        {
            return RedirectToAction("ListAll");
        }

        [HttpPost]
        public ActionResult Unlock(string username)
        {
            var status = true;

            try
            {
                AccountService.Unlock(username);
            }
            catch
            {
                status = false;
            }

            return Json(new { status }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Delete()
        {
            return RedirectToAction("Manage");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(IEnumerable<int> ids)
        {
            UserProfileService.DeleteUsers(ids);

            return new EmptyResult();
        }

        private ActionResult delete(AdditUserViewModel model)
        {
            bool success = false;

            if (UserProfile.UserName != model.CurrentUserName)
            {
                UserProfileService.DeleteUser(model.CurrentUserName);

                success = true;
            }

            if (success)
            {
                TempData.Add("data", new ManageData(PageAction.Delete));
                return RedirectToAction("Manage");
            }
            else
            {
                ModelState.Remove("Password");
                TempData.Add("Result", AdditUserResult.DeleteUserFailure);
                model.RolesAvailable = getAvailableRoles(); //TODO this is repeated too often
                return View("Manage", new ManageUserViewModel { AdditUserViewModel = model });
            }
        }

        private ActionResult edit(AdditUserViewModel model)
        {
            if (model.Password == "Reset password...")
                ModelState.Remove("Password");

            foreach (var error in model.Validate(new ValidationContext(model, null, null)))
                foreach (var memberName in error.MemberNames)
                    ModelState.AddModelError(memberName, error.ErrorMessage);

            if (ModelState.IsValid)
            {
                try
                {
                    var result =
                        UserProfileService.Update(model.CurrentUserName,
                            role: model.Role.Value,
                            schemeNames: model.Schemes.Where(x => x.Trim().Length > 0),
                            firstname: model.FirstName,
                            lastname: model.LastName,
                            email: model.EmailAddress,
                            updatedusername: model.UserName != model.CurrentUserName ? model.UserName : null);

                    if (!result.Success)
                    {
                        if (result.Has(IUserProfileServiceResult.SchemeNotFound))
                        {
                            ModelState.AddModelError("SchemeName", "Unknown scheme");
                        }
                        else if (result.Has(IUserProfileServiceResult.UsernameConflict))
                        {
                            ModelState.AddModelError("UserName", "Username already in use");
                        }
                        model.Result = AdditUserResult.EditUserFailure;
                        model.RolesAvailable = getAvailableRoles();
                        return View("Manage", new ManageUserViewModel() { AdditUserViewModel = model });
                    }
                    else
                    {
                        model.CurrentUserName = model.UserName;

                        if (model.Password != null && model.Password != "Reset password...")
                        {
                            string tempResetToken = WebSecurity.GeneratePasswordResetToken(model.UserName);
                            WebSecurity.ResetPassword(tempResetToken, model.Password);
                            if (AppSettings.PasswordChangeRequiredAfterReset && User.IsInRole(RoleType.Admin.ToString()))
                                AccountService.SetPasswordFlag(model.UserName, true);
                        }

                        // then all is good
                        TempData.Add("data", new ManageData(PageAction.Edit));
                        return RedirectToAction("Manage");
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "System error");
                }
            }

            // then there was an error
            model.Result = AdditUserResult.EditUserFailure;
            model.RolesAvailable = getAvailableRoles();
            return View("Manage", new ManageUserViewModel { AdditUserViewModel = model, IsEditing = true });
        }

        private IEnumerable<RoleType> getAvailableRoles()
        {
            switch (UserProfile.Role)
            {
                case RoleType.Admin:
                    return Utils.EnumToArray<RoleType>();
                case RoleType.Installer:
                    return Utils.EnumToArray<RoleType>().Except(new List<RoleType> { RoleType.Admin });
                default:
                    return new List<RoleType> { UserProfile.Role };
            }
        }

        private static KeyValuePair<string, string> NewUserErrorMessage(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return new KeyValuePair<string, string>("UserName", "Username already in use");

                case MembershipCreateStatus.DuplicateEmail:
                    return new KeyValuePair<string, string>("EmailAddress", "Email address already in use");

                case MembershipCreateStatus.InvalidPassword:
                    return new KeyValuePair<string, string>("Password", "Invalid password");

                case MembershipCreateStatus.InvalidEmail:
                    return new KeyValuePair<string, string>("EmailAddress", "Invalid email address");

                case MembershipCreateStatus.InvalidAnswer:
                    return new KeyValuePair<string, string>("PasswordAnswer", "Invalid password retrieval answer");

                case MembershipCreateStatus.InvalidQuestion:
                    return new KeyValuePair<string, string>("PsswordQuestion", "Invalid password retrieval question");

                case MembershipCreateStatus.InvalidUserName:
                    return new KeyValuePair<string, string>("UserName", "Invalid username");

                case MembershipCreateStatus.ProviderError:
                    return new KeyValuePair<string, string>("", "Provider error");

                case MembershipCreateStatus.UserRejected:
                    return new KeyValuePair<string, string>("", "Rejected");

                default:
                    return new KeyValuePair<string, string>("", "Unknown error");
            }
        }

        private string GetRandomPassword(int passwordLength)
        {
            if (passwordLength <= 0)
            {
                passwordLength = 8; //default length
            }
            const string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            const string specialCharacters = @"!#$%&'()*+,-./:;<=>?@[\]_";

            Random rnd = new Random();
            int seed = rnd.Next(1, int.MaxValue); 
            char[] chars = new char[passwordLength];
            Random rd = new Random(seed);

            int i;
            for (i = 0; i < passwordLength - 1; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            
            // add special character at the end
            chars[i] = specialCharacters[rd.Next(0, specialCharacters.Length)];

            return new string(chars);
        }
    }
}
