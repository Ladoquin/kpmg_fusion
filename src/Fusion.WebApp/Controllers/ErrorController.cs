﻿namespace Fusion.WebApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    
    public class ErrorController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View("Error");
        }

        public ActionResult Maintenance()
        {
            return View("Maintenance");
        }

        public ActionResult UnsupportedScheme()
        {
            return View("UnsupportedScheme");
        }
    }
}
