﻿using FlightDeck.DomainShared;
using Fusion.WebApp.Filters;
using Fusion.WebApp.Models.Attributes;
using System.Configuration;
using System.Web.Mvc;

namespace Fusion.WebApp.Controllers
{
    [FusionAuthorize(RoleType.Admin, RoleType.Client, RoleType.Consultant, RoleType.Installer)]
    [SchemeAdmit]
    public class Solutions2Controller : FusionBaseController
    {
        [HttpGet]
        public ActionResult Header()
        {
            return PartialView("_SolutionsHeader", UserProfile);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View("Solutions2");
        }

        [HttpGet]
        public ActionResult Risk()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Cash()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Efficiency()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Reward()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Investment()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_Investment"]);
        }

        [HttpGet]
        public ActionResult InsuranceSolutions()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_InsuranceSolutions"]);
        }

        [HttpGet]
        public ActionResult MemberOptions()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_MemberOptions"]);
        }

        [HttpGet]
        public ActionResult BenefitChanges()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_BenefitChanges"]);
        }

        [HttpGet]
        public ActionResult ABF()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_AssetBackedFunding"]);
        }

        [HttpGet]
        public ActionResult SchemeFunding()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_SchemeFunding"]);
        }

        [HttpGet]
        public ActionResult SponsorCovenant()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_SponserCovenant"]);
        }

        [HttpGet]
        public ActionResult PPP()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_PeoplePoweredPerformance"]);
        }

        [HttpGet]
        public ActionResult Communication()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_Communication"]);
        }

        [HttpGet]
        public ActionResult DC()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TaxSolutions()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_TaxSolutions"]);
        }

        [HttpGet]
        public ActionResult PPFLevy()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_PPFLevy"]);
        }

        [HttpGet]
        public ActionResult GovernanceEfficiency()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_GovernanceAndEfficiency"]);
        }

        [HttpGet]
        public ActionResult PSA()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_Accounting"]);
        }

        [HttpGet]
        public ActionResult MemberBenefitData()
        {
            return Redirect(ConfigurationManager.AppSettings["SolutionsLink_MemberAndBenefitData"]);
        }

    }
}
