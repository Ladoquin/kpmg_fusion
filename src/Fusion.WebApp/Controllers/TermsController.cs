﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fusion.WebApp.Models;
using FlightDeck.ServiceInterfaces;

namespace Fusion.WebApp.Controllers
{
    [Authorize]
    public class TermsController : FusionBaseController
    {
        IAccountService _accountService;
        IAccountService accountService
        {
            get
            {
                if (_accountService == null)
                    _accountService = ServiceFactory.GetService<IAccountService>(this);
                return _accountService;
            }
        }

        //
        // GET: /Terms/

        public ActionResult Index()
        {
            var model = new TermsViewModel();

            if (Request.IsAuthenticated)
            {
                if (!UserProfile.TermsAccepted)
                {
                    model.ShowAcceptPanel = true;
                }
            }

            return View("Terms", model);
        }

        /// <summary>
        /// Ignore this AcceptTerms overload, it is only used for handling a GET request to the POST url, ie, in the case of a forms authentication timed-out redirect.
        /// </summary>
        [HttpGet]
        public ActionResult AcceptTerms()
        {
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Use this AcceptTerms overload to update users profile.
        /// </summary>
        /// <param name="form">Ignore, pass null if necessary, this argument is only being used to distinguish this method overload from the HttpGet one.</param>
        [HttpPost]
        public ActionResult AcceptTerms(object form)
        {
            Session["UserProfile"] = accountService.AcceptTerms(User.Identity.Name);
            return RedirectToAction("Index", "Home");
        } 
    }
}
