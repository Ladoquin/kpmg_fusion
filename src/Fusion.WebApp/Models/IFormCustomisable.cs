﻿namespace Fusion.WebApp.Models
{
    public interface IFormCustomisable
    {
        FormContext FormHandler { set; }
    }
}