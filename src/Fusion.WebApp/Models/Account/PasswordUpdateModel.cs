﻿namespace Fusion.WebApp.Models
{
    using Fusion.WebApp.Models.Attributes;
    using System.ComponentModel.DataAnnotations;

    public class PasswordUpdateModel : UpdateModel<ResultEnum>, IFormCustomisable
    {
        public FormContext FormHandler { get; set; }

        public bool ChangePasswordRequired { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(AppSettings.PasswordRegEx, ErrorMessage = "Invalid")]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        [NotEqualTo("OldPassword", ErrorMessage = "Cannot equal current password")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "Must match")]
        public string ConfirmPassword { get; set; }
    }
}