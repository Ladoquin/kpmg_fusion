﻿using System.ComponentModel.DataAnnotations;
namespace Fusion.WebApp.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public bool ShowTimeoutMessage { get; set; }
        public bool ShowSignInFailed { get; set; }
        /*
         * For new login page when using login panel
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool ShowSignInFailed { get; set; }
        public bool ShowTimeoutMessage { get; set; }
         */ 
    }
}