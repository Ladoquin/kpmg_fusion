﻿using Fusion.WebApp.Models.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Fusion.WebApp.Models
{
    public class ForgottenUsernameViewModel
    {
        public bool RequestReceived { get; set; }
        public bool HasError { get; set; }

        [Required(ErrorMessage = "Required")]
        [Email(ErrorMessage = "Not a valid email address")]
        [Display(Name = "Email address")]
        public string Email { get; set; }
    }
}