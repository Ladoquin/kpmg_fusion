﻿using System.ComponentModel.DataAnnotations;

namespace Fusion.WebApp.Models
{
    public class ForgottenPasswordViewModel
    {
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        public bool Submitted { get; set; }
        public bool HasError { get; set; }
    }
}