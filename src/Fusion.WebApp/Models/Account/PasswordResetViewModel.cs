﻿namespace Fusion.WebApp.Models
{
    using Fusion.WebApp.Models.Attributes;
    using System.ComponentModel.DataAnnotations;

    public class PasswordResetViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Token")]
        public string Token { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(AppSettings.PasswordRegEx, ErrorMessage = "Invalid")]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        [NotEqualTo("OldPassword", ErrorMessage = "Cannot equal current password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "Must match")]
        public string ConfirmPassword { get; set; }

        public bool OldPasswordRequired { get; set; }
        public bool HasError { get; set; }
        public bool IsCreate { get; set; }
        public bool ShowPasswordRules { get; set; }
    }
}