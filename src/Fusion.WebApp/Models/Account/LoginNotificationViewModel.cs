﻿using System;

namespace Fusion.WebApp.Models
{
    public class LoginNotificationViewModel
    {
        public string Role { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Scheme { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Date { get; set; }
        public string DataCaptureVersion { get; set; }
        public bool SchemeSupported { get; set; }
    }
}