﻿namespace Fusion.WebApp.Models
{
    public class AccountUpdateModel
    {
        public bool AllowSchemeChange { get; set; }
        public bool AllowUsernameChange { get; set; }
        public PasswordUpdateModel PasswordUpdateModel = new PasswordUpdateModel();
        public NameEmailUpdateModel NameEmailUpdateModel = new NameEmailUpdateModel();
        public UserNameUpdateModel UserNameUpdateModel = new UserNameUpdateModel();
        public SchemeNameUpdateModel SchemeNameUpdateModel = new SchemeNameUpdateModel();
    }
}