﻿using Fusion.WebApp.Models.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Fusion.WebApp.Models
{
    public class NameEmailUpdateModel : UpdateModel<ResultEnum>
    {
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [Email(ErrorMessage = "Not a valid email address")]
        public string EmailAddress { get; set; }
    }
}