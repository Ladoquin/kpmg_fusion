﻿using System.ComponentModel.DataAnnotations;

namespace Fusion.WebApp.Models
{
    public class UserNameUpdateModel : UpdateModel<ResultEnum>
    {
        [Required(ErrorMessage = "Required")]
        [StringLength(100, ErrorMessage = "Min {2} chars long", MinimumLength = 5)]
        [DataType(DataType.Text)]
        [Display(Name = "New username")]
        public string NewUserName { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string CurrentPassword { get; set; }

    }
}