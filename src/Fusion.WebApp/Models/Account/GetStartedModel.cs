﻿namespace Fusion.WebApp.Models
{
    public class GetStartedModel
    {
        public string contactName { get; set; }
        public string contactemail { get; set; }
        public string contactRole { get; set; }
        public string contactOrg { get; set; }
        public string contactPhone { get; set; }
    }
}