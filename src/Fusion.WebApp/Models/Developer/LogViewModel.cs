﻿namespace Fusion.WebApp.Models
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    
    public class LogViewModel
    {
        public DateTime Date { get; set; }
        public string Level { get; set; }
        public IEnumerable<Log> Entries { get; set; }
    }
}