﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using System.Collections.Generic;

namespace Fusion.WebApp.Models
{
    public class ManageSchemeViewModel
    {
        public FindSchemeViewModel FindSchemeViewModel = new FindSchemeViewModel();
        public AdditSchemeViewModel AdditSchemeViewModel = new AdditSchemeViewModel();
        public ListSchemeViewModel ListSchemeViewModel = new ListSchemeViewModel();
        public IEnumerable<IScheme> SchemeImportHistory = new List<IScheme>();
    }
}