﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using System.Collections.Generic;

namespace Fusion.WebApp.Models
{
    public class ListSchemeViewModel
    {
        public List<IScheme> Schemes { get; set; }
    }
}