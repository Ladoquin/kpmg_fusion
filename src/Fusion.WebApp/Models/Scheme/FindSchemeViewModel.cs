﻿namespace Fusion.WebApp.Models
{
    using FlightDeck.ServiceInterfaces;
    using System.ComponentModel.DataAnnotations;
    
    public class FindSchemeViewModel
    {
        public ResultEnum Result { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Search scheme details")]
        public string FindSchemeName { get; set; }

        public SchemeRestrictionStatus RestrictionStatus { get; set; }
        public string SubmitLabel { get; set; }
        public bool ShowListAll { get; set; }

        public FindSchemeViewModel()
        {
            RestrictionStatus = SchemeRestrictionStatus.All;
        }
    }
}