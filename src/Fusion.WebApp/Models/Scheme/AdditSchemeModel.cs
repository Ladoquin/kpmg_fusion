﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Fusion.WebApp.Models
{
    public class AdditSchemeViewModel
    {
        public AdditSchemeViewModel()
        {
            this.Documents = new List<HttpPostedFileBase>();
            this.DocumentIds = new List<int>();
            this.DocumentNames = new List<string>();
            this.DocumentFileNames = new List<string>();
            this.DeleteDocuments = new List<int>();
        }

        public AdditSchemeResult Result { get; set; }
        public int Id { get; set; }

        [DataType(DataType.Text)]
        public string CurrentSchemeName { get; set; }

        public bool IsEditScheme { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Scheme name")]
        public string NewSchemeName { get; set; }

        public bool HasLogo { get; set; }

        [Display(Name = "Company logo")]
        public HttpPostedFileBase Logo { get; set; }

        [Display(Name = "Restricted")]
        public bool IsRestricted { get; set; }

        [Display(Name = "Scheme import file")]
        public HttpPostedFileBase SchemeData { get; set; }

        [Display(Name = "Accounting downloads password")]
        [DataType(DataType.Password)]
        public string AccountingDownloadsPassword { get; set; }

        [Display(Name = "Source")]
        public string ExportFile { get; set; }

        public string ImportFileSpreadsheetName { get; set; }

        [Display(Name = "Documents")]
        public IList<HttpPostedFileBase> Documents { get; set; }

        public IList<String> DocumentNames { get; set; }
        public IList<String> DocumentFileNames { get; set; }
        public IList<int> DocumentIds { get; set; }
        public IList<int> DeleteDocuments { get; set; }
        public string ImportErrorAt { get; set; }
        public string ImportErrorDetails { get; set; }
        public bool SourceXmlExists { get; set; }
    }
}