﻿using System.ComponentModel.DataAnnotations;

namespace Fusion.WebApp.Models
{
    public class SchemeNameUpdateModel : UpdateModel<ResultEnum>
    {
        [DataType(DataType.Text)]
        [Display(Name = "Scheme name")]
        public string SchemeName { get; set; }
        public int SchemeDetailId { get; set; }
        public bool IsFavourite { get; set; }
    }
}