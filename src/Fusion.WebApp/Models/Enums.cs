﻿namespace Fusion.WebApp
{
    public enum ResultEnum
    {
        Success,
        Failure
    }

    public enum AdditUserResult
    {
        None,
        UserSaveSuccess,
        FindUserSuccess,
        AddUserFailure,
        EditUserFailure,
        DeleteUserSuccess,
        DeleteUserFailure
    }

    public enum AdditSchemeResult
    {
        FindSchemeSuccess,
        AddSchemeSuccess,
        AddSchemeFailure,
        EditSchemeSuccess,
        EditSchemeFailure,
        DeleteSchemeSuccess,
        DeleteSchemeFailure
    }

    public enum VideoType { Solutions, MemberOptions, AssetBackedFunding }
}