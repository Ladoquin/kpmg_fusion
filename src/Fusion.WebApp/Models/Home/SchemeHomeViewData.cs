﻿namespace Fusion.WebApp.Models
{
    using System;
    using FlightDeck.ServiceInterfaces;

    public class SchemeHomeViewData : SchemeViewModel
    {
        public SchemeHomeViewData(IScheme scheme)
            : base(scheme)
        {
        }
        public string ImportantMessage { get; set; }
        public string ImportantMessageKey { get; set; }
        public DateTime? Ias19Disclosure { get; set; }
        public DateTime? ValuationReport { get; set; }
        public DateTime? AssetData { get; set; }
        public DateTime? MemberData { get; set; }
        public DateTime? BenefitData { get; set; }
    }
}