﻿namespace Fusion.WebApp.Models
{
    public interface IUserProfile
    {
        string UserName { get; }
        string FirstName { get; }
        string LastName { get; }
        string EmailAddress { get; }
    }
}
