﻿namespace Fusion.WebApp.Models
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public class VaRCalcsViewModel
    {
        public IDictionary<AssetClassType, double> Volatilities { get; set; }
        public IDictionary<AssetClassType, IDictionary<AssetClassType, double>> VarCorrelations { get; set; }
    }
}