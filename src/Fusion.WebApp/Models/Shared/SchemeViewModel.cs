﻿namespace Fusion.WebApp.Models
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;

    public class SchemeViewModel
    {
        private readonly IScheme _scheme;

        public SchemeViewModel(IScheme scheme)
        {
            _scheme = scheme;
        }

        public IScheme SchemeSummary { get { return _scheme; } }
        public IPensionScheme SchemeData { get { return _scheme.PensionScheme; } }

        // shorts cuts to help the views stay as dumb as possible
        public string SchemeName { get { return _scheme.SchemeName; } }
        public DateTime RefreshDate { get { return _scheme.PensionScheme.RefreshDate; } }
        public IBasis CurrentBasis { get { return _scheme.PensionScheme.CurrentBasis; } }
        public string CurrentBasisName { get { return _scheme.PensionScheme.CurrentBasis.Name; } }
        public BasisType CurrentBasisType { get { return _scheme.PensionScheme.CurrentBasis.BasisType; } }
        public IEnumerable<BasisType> BasisTypes { get { return _scheme.PensionScheme.GetBasisTypes(); } }
        public IList<SchemeDocument> Documents { get { return _scheme.Documents; } }
        public string CurrencySymbol { get { return _scheme.CurrencySymbol; } }
        public AccountingSettings AccountingSettings { get { return _scheme.PensionScheme.AccountingSettings; } }
    }
}