﻿namespace Fusion.WebApp.Models
{
    public class KeyedToggleItem
    {
        public string Key { get; set; }
        public string Label { get; set; }
        public bool Readonly { get; set; }
        public bool Disabled { get; set; }
    }
}