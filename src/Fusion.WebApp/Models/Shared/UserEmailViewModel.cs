﻿using FlightDeck.DomainShared;

namespace Fusion.WebApp.Models
{
    public class UserEmailViewModel
    {
        public RoleType Role { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Token { get; set; }

        public UserEmailViewModel()
        {
        }
        public UserEmailViewModel(IUserProfile user)
        {
            UserName = user.UserName;
            FirstName = user.FirstName;
            LastName = user.LastName;
            EmailAddress = user.EmailAddress;
        }
    }
}