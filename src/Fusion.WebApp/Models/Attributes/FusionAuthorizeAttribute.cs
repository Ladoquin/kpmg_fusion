﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Fusion.WebApp.Models.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class FusionAuthorizeAttribute : AuthorizeAttribute
    {
        public IList<RoleType> _roles { get; set; }

        public FusionAuthorizeAttribute(params RoleType[] roles)
            : base()
        {
            _roles = roles;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.Roles = _roles.Select(x => x.ToString()).Aggregate((x, y) => x + ", " + y);
            base.OnAuthorization(filterContext);
        }
    }
}