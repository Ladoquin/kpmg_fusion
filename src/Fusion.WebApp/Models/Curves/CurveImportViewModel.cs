﻿using FlightDeck.DomainShared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Fusion.WebApp.Models
{
    public class CurveImportViewModel
    {
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Curve import file")]
        public HttpPostedFileBase ImportFile { get; set; }

        public CurveImportDetail ImportDetail { get; set; }

        public bool Submitted { get; set; }
        public ResultEnum Result { get; set; }

        public IEnumerable<string> ValidationMessages { get; set; }

        public bool AllowInvalidSubmission { get; set; }

        public CurveImportViewModel()
        {
            ValidationMessages = new List<string>();
        }
    }
}