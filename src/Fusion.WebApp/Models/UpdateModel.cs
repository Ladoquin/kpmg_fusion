﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fusion.WebApp.Models
{
    public class UpdateModel<TResultType> where TResultType : struct
    {
        public Result<TResultType> Result { get; set; }

        public UpdateModel()
        {
            Result = new Result<TResultType>();
        }
    }
}