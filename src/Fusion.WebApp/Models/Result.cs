﻿using System.Collections.Generic;
using System.Linq;

namespace Fusion.WebApp.Models
{
    public class Result<TResultEnum> where TResultEnum : struct
    {
        private List<TResultEnum> _results;

        public Result()
        {
        }
        public void Add(TResultEnum result)
        {
            Add(new List<TResultEnum> { result });
        }
        public void Add(IEnumerable<TResultEnum> results)
        {
            if (_results == null) _results = new List<TResultEnum>();
            _results.AddRange(results);
        }

        public bool HasFlag(TResultEnum flag)
        {
            return _results != null && _results.Contains(flag);
        }
    }
}