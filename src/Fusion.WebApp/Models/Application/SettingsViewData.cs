﻿using System.ComponentModel.DataAnnotations;
namespace Fusion.WebApp
{
    public class SettingsViewData
    {
        [StringLength(250, ErrorMessage = "Max {1} chars long")]
        [DataType(DataType.Text)]
        [Display(Name = "Important message")]
        public string ImportantMessage { get; set; }

        public bool ShowSuccess { get; set; }
    }
}