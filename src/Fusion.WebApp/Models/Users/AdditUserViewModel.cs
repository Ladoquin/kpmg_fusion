﻿using FlightDeck.DomainShared;
using Fusion.WebApp.Models.Attributes;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Fusion.WebApp.Models
{
    public class AdditUserViewModel : IUserProfile, IValidatableObject
    {
        public AdditUserViewModel()
        {
            Schemes = new List<string>();
            RolesAvailable = new List<RoleType>();
            FindScheme = new FindSchemeViewModel();
        }

        public AdditUserResult Result { get; set; }

        [DataType(DataType.Text)]
        public string CurrentUserName { get; set; }

        [Required(ErrorMessage = "Required")]
        [StringLength(20, ErrorMessage = "Between {2} and {1} chars long", MinimumLength = 5)]
        [DataType(DataType.Text)]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(AppSettings.PasswordRegEx, ErrorMessage = "Invalid")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Role")]
        public RoleType? Role { get; set; }

        [Required(ErrorMessage = "Required")]
        [StringLength(25, ErrorMessage = "Max {1} chars")]
        [DataType(DataType.Text)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Required")]
        [StringLength(25, ErrorMessage = "Max {1} chars")]
        [DataType(DataType.Text)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [Email(ErrorMessage = "Not a valid email address")]
        public string EmailAddress { get; set; }

        public IEnumerable<string> Schemes { get; set; }

        public IEnumerable<RoleType> RolesAvailable { get; set; }

        public FindSchemeViewModel FindScheme { get; set; }

        public bool RandomPassword { get; set; } // FUSN-630 Kam - 29/07/14

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Role.HasValue & (Role.Value == RoleType.Client || Role.Value == RoleType.Adviser) && Schemes.IsNullOrEmpty())
            {
                yield return new ValidationResult("Must be assigned to a scheme", new string[] { "Role" });
            }
        }
    }
}