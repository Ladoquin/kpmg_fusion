﻿namespace Fusion.WebApp.Models
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    
    public class UserListItem
    {
        public UserListItem()
        {
            Schemes = new List<SchemeDetail>();
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public RoleType Role { get; set; }
        public IEnumerable<SchemeDetail> Schemes { get; set; }
        public bool IsLocked { get; set; }
    }
}