﻿namespace Fusion.WebApp.Models
{
    public class ManageUserViewModel
    {
        public bool IsEditing { get; set; }
        public string UsernameSaved { get; set; }
        public FindUserViewModel FindUserViewModel = new FindUserViewModel();
        public AdditUserViewModel AdditUserViewModel = new AdditUserViewModel();
        public ListUserViewModel ListUserViewModel = new ListUserViewModel();
    }
}