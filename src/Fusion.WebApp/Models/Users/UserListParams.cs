﻿using FlightDeck.DomainShared;
namespace Fusion.WebApp.Models
{
    public class UserListParams
    {
        public int TotalMatches { get; set; }
        public int PageNumber { get; set; }
        public bool NameDescending { get; set; }
        public RoleType? RoleFilter { get; set; }
        public string SchemeName { get; set; }
    }
}