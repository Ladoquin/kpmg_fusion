﻿using System.Collections.Generic;

namespace Fusion.WebApp.Models
{
    public class ListUserViewModel
    {
        public List<UserListItem> Users { get; set; }
        public UserListParams Params { get; set; }
    }
}