﻿using System.ComponentModel.DataAnnotations;

namespace Fusion.WebApp.Models
{
    public class FindUserViewModel
    {
        public ResultEnum Result { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Search user details")]
        public string FindUserName { get; set; }
    }
}