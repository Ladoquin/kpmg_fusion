﻿namespace Fusion.WebApp.Models
{
    public class FormContext
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
    }
}