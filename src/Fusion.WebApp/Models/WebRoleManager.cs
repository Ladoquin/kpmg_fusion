﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using System.Linq;
using System;
using System.Web.Security;

namespace Fusion.WebApp.Models
{
    public class WebRoleManager : IRoleManager
    {
        public RoleType GetRole(string username)
        {
            return (RoleType)Enum.Parse(typeof(RoleType), Roles.GetRolesForUser(username).First());
        }

        public void Delete(string username)
        {
            foreach (var role in Roles.GetRolesForUser(username))
            {
                Roles.RemoveUserFromRole(username, role);
            }
        }

        public void AddUserToRole(string username, string roleName)
        {
            if (!Roles.GetRolesForUser(username).Contains(roleName))
            {
                Roles.AddUserToRole(username, roleName);
            }
        }
    }
}