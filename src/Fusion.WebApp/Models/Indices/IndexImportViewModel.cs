﻿using FlightDeck.DomainShared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Fusion.WebApp.Models
{
    public class IndexImportViewModel
    {
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Index import file")]
        public HttpPostedFileBase ImportFile { get; set; }

        public IndexImportDetail ImportDetail { get; set; }

        public bool Submitted { get; set; }
        public ResultEnum Result { get; set; }

        public IEnumerable<string> ValidationMessages { get; set; }

        public bool AllowInvalidSubmission { get; set; }

        public IndexImportViewModel()
        {
            ValidationMessages = new List<string>();
        }
    }
}