﻿namespace Fusion.WebApp.Models
{
    public class PlanCache
    {
        public object Accounting { get; set; }
        public object FundingLevel { get; set; }
        public object VarFunnel { get; set; }
        public object VarWaterfall { get; set; }
    }
}
