﻿namespace Fusion.WebApp.Models
{
    using FlightDeck.ServiceInterfaces;
    using System.Collections;

    public class CashflowData
    {

        public ArrayList beforeActives { get; private set; }
        public ArrayList beforeDeferreds { get; private set; }
        public ArrayList beforePensioners { get; private set; }

        public ArrayList afterActives { get; private set; }
        public ArrayList afterDeferreds { get; private set; }
        public ArrayList afterPensioners { get; private set; }
        public int count { get; private set; }
        public double commencementDate { get; private set; }
        public int year { get; private set; }
        public bool isDirty { get; private set; }

        public CashflowData(IPensionScheme scheme, long d = 0, bool isDataDirty = false)
        {
            var date = d > 0 ? JsonHelper.UnixEpoch.AddMilliseconds(d) : scheme.CurrentBasis.AttributionEnd;

            var dynamicData = d == 0 ? scheme.CurrentBasis.GetCashFlowData() : null;
            var staticData = d == 0 ? null : scheme.CurrentBasis.GetCashFlowData(AttributionEndType.RefreshDate);

            this.beforeActives = new ArrayList();
            this.beforeDeferreds = new ArrayList();
            this.beforePensioners = new ArrayList();

            this.afterActives = new ArrayList();
            this.afterDeferreds = new ArrayList();
            this.afterPensioners = new ArrayList();

            count = dynamicData != null ? dynamicData.Before.Count : staticData.Count;
            
            for (int i = 0; i < count; i++)
            {
                var before = dynamicData != null ? dynamicData.Before[i] : staticData[i];
                var after = dynamicData != null ? dynamicData.After[i] : null;

                beforeActives.Add(before.ActivePast);
                beforeDeferreds.Add(before.Deferred);
                beforePensioners.Add(before.Pensioners);

                if (after != null)
                {
                    afterActives.Add(after.ActivePast);
                    afterDeferreds.Add(after.Deferred);
                    afterPensioners.Add(after.Pensioners);
                }
            }
            commencementDate = JsonHelper.ConvertDateToJSON(date);
            year = date.Year;
            isDirty = isDataDirty;
        }
    }
}