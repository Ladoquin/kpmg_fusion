﻿namespace Fusion.WebApp.Models
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System.Collections.Generic;

    public class AnalysisViewModel : SchemeViewModel
    {
        public IEnumerable<ClientAssumption> SavedScenarios { get; set; }
        public bool AllowScenarioDownload { get; set; }

        public AnalysisViewModel(IScheme scheme)
            : base(scheme)
        {
            SavedScenarios = new List<ClientAssumption>();
        }
    }
}