﻿namespace Fusion.WebApp.Models
{
    public class PanelState
    {
        public bool liabilityAssumptions { get; set; }
        public bool pensionIncreaseAssumptions { get; set; }
        public bool assetAssumptions { get; set; }
        public bool recoveryPlan { get; set; }
        public bool investmentStrategy { get; set; }
        public bool investmentStrategyOptions { get; set; }
        public bool FRO { get; set; }
        public bool ETV { get; set; }
        public bool PIE { get; set; }
        public bool futureBenefits { get; set; }
        public bool ABF { get; set; }
        public bool insurance { get; set; }
    }
}