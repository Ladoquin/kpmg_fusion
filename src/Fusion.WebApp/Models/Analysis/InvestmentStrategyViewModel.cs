﻿namespace Fusion.WebApp.Models.Analysis
{
    using FlightDeck.ServiceInterfaces;
    using Fusion.WebApp.Models;
    using System.Collections.Generic;
    
    public class InvestmentStrategyViewModel : SchemeViewModel
    {
        public IList<KeyedToggleItem> AssetClasses { get; set; }

        public InvestmentStrategyViewModel(IScheme scheme) : base(scheme)
        {
            AssetClasses = new List<KeyedToggleItem>();
        }
    }
}