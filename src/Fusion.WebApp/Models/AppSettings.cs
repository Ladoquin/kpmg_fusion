﻿namespace Fusion.WebApp
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net.Configuration;
    using System.Reflection;
    using System.Web;
using System.Web.Mvc;

    public static class AppSettings
    {
        private static Version _v;
        public static Version Version
        {
            get
            {
                if (_v == null) _v = Assembly.GetExecutingAssembly().GetName().Version;
                return _v;
            }
        }

        public const string PasswordRegEx = @"^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(.{8,20})$";

        public static SmtpSection SMTP { get { return ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection; } }
        public static string SupportEmail { get { return ConfigurationManager.AppSettings["SupportEmail"]; } }        
        public static string[] GoogleAnalytics { get { return ConfigurationManager.AppSettings["GoogleAnalytics"].Split(',').Select(x => x.Trim()).ToArray(); } }
        public static string ContactEmail { get { return ConfigurationManager.AppSettings["ContactEmail"]; } }
        public static string WelcomeNotificationEmail { get { return ConfigurationManager.AppSettings["WelcomeNotificationEmail"]; } }
        public static int AllowedFailedLoginAttempts { get { return int.Parse(ConfigurationManager.AppSettings["AllowedFailedLoginAttempts"]); } }
        public static int PageSize { get { return int.Parse(ConfigurationManager.AppSettings["PageSize"]); } }
        public static int WelcomeEmailExpirationDays { get { return int.Parse(ConfigurationManager.AppSettings["WelcomeEmailExpirationDays"]); } }
        public static bool AddUserRandomPassword { 
            get 
            {
                bool e;
                bool.TryParse(ConfigurationManager.AppSettings["AddUserRandomPassword"], out e);
                return e;
            }
        }
        public static bool PasswordChangeRequiredAfterReset
        { 
            get 
            {
                bool e;
                bool.TryParse(ConfigurationManager.AppSettings["PasswordChangeRequiredAfterReset"], out e);
                return e;
            }
        }
        public static IEnumerable<string> LoginNotificationEmail 
        { 
            get 
            {
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["LoginNotificationEmail"]))
                    return ConfigurationManager.AppSettings["LoginNotificationEmail"].Split(',').Select(x => x.Trim());
                return new List<string>();
            } 
        }
        public static IEnumerable<string> AdminLoginNotificationEmail
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["AdminLoginNotificationEmail"]))
                    return ConfigurationManager.AppSettings["AdminLoginNotificationEmail"].Split(',').Select(x => x.Trim());
                return new List<string>();
            }
        }
        public static bool IndexFillEndGap
        {
            get
            {
                bool e;
                bool.TryParse(ConfigurationManager.AppSettings["IndexFillEndGap"], out e);
                return e;
            }
        }

        public static IEnumerable<string> AcceptedSchemeDocumentTypes
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["AcceptedSchemeDocumentTypes"]))
                    return ConfigurationManager.AppSettings["AcceptedSchemeDocumentTypes"].Split(',').Select(x => x.Trim());
                return new List<string>();
            }
        }

        public static bool EnableLocalResources
        {
            get
            {
                bool e;
                bool.TryParse(ConfigurationManager.AppSettings["enableLocalResources"], out e);
                return e;
            }
        }

        public static Hashtable ToHash()
        {
            var hash = new Hashtable(1);

            hash.Add("SMTP", SMTP);
            hash.Add("DefaultConnection", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            return hash;
        }
    }
}