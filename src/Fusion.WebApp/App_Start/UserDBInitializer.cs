﻿using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using WebMatrix.WebData;

namespace Fusion.WebApp.App_Start
{
    public class UserDbInitializer
    {
        public const string DeveloperUser = "logviewer";

        public static void InitializeUsersAndDb()
        {
            if (!WebSecurity.Initialized)
                WebSecurity.InitializeDatabaseConnection("DefaultConnection",
                    "UserProfile", "UserId", "UserName", autoCreateTables: true);

            seed();
        }

        private static void seed()
        {
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (membership.GetUser("admin", false) == null)
            {
                membership.CreateUserAndAccount("admin", "?Fu5ion!", new Dictionary<string, object> { 
                    { "FirstName", "System" }, 
                    { "LastName", "Administrator" }, 
                    { "EmailAddress", "sys.admin@kpmgfusion.co.uk"},
                    { "TermsAccepted", false},
                    { "PasswordMustChange", false }
                });
            }
            if (!roles.GetRolesForUser("admin").Contains(RoleType.Admin.ToString()))
            {
                roles.AddUsersToRoles(new[] { "admin" }, new[] { RoleType.Admin.ToString() });
            }

            if (membership.GetUser(DeveloperUser, false) == null)
            {
                membership.CreateUserAndAccount(DeveloperUser, "?Cryst4lP4l4c3!", new Dictionary<string, object> { 
                    { "FirstName", "Developer" }, 
                    { "LastName", "Administrator" }, 
                    { "EmailAddress", "noreply@fusion.co.uk"},
                    { "TermsAccepted", true },
                    { "PasswordMustChange", false },
                    { "IsSystemUser", true }
                });
            }
            if (!roles.GetRolesForUser(DeveloperUser).Contains(RoleType.Admin.ToString()))
            {
                roles.AddUsersToRoles(new[] { DeveloperUser }, new[] { RoleType.Admin.ToString() });
            }
        }
    }
}