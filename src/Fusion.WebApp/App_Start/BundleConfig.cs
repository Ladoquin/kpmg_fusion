﻿using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;
using System.Linq;

namespace Fusion.WebApp
{
    public class FIFOBundleOrderer : IBundleOrderer
    {
        public IEnumerable<System.IO.FileInfo> OrderFiles(BundleContext context, IEnumerable<System.IO.FileInfo> files)
        {
            return files;
        }
    }

    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/browserCheck").Include(
                    "~/Scripts/libs/jquery.browser.js",
                    "~/Scripts/browserwarning.js"
                ));

            bundles.Add(new StyleBundle("~/Content/landingcss").Include(
                    "~/Scripts/libs/css/jquery.fancybox.css",
                    "~/Content/landing.css"
                ));
            bundles.Add(new ScriptBundle("~/bundles/landingjs").Include(
                    "~/Scripts/libs/modernizr.js",
                    "~/Scripts/libs/jquery.fancybox.js",
                    "~/Scripts/landing.js"
                ));

            bundles.Add(new StyleBundle("~/Content/fusioncss").Include(
                    "~/Content/browserwarning.css",
                    "~/Content/global.css",
                    "~/Content/tabs.css",
                    "~/Content/accordion.css",
                    "~/Content/isotope.css",
                    "~/Content/home.css",
                    "~/Content/solutions.css",
                    "~/Content/progresscycle.css",
                    "~/Content/baseline.css",
                    "~/Content/analysis.css",
                    "~/Content/login.css",
                    "~/Content/evolution.css",
                    "~/Content/plan.css",
                    "~/Content/caves.css",
                    "~/Content/admin.css",                    
                    "~/Scripts/libs/css/base/jquery.ui.slider.css",
                    "~/Scripts/libs/css/base/jquery.ui.autocomplete.css",
                    "~/Scripts/libs/css/base/jquery.ui.datepicker.css",
                    "~/Scripts/libs/css/jquery.overide.css",
                    "~/Scripts/libs/css/jquery.fancybox.css",
                    "~/Scripts/libs/css/jquery.mCustomScrollbar.css",
                    "~/Content/date-picker.css",
                    "~/Content/responsive.css",
                    "~/Content/sliderTextBox.css",
                    "~/Content/journeyplan.css",
                    "~/Content/legendexpando.css",
                    "~/Content/styleableSelect.css"
                ));


            bundles.Add(new ScriptBundle("~/bundles/fusionlibs").Include(
                    "~/Scripts/libs/jquery-ui.js",
                    "~/Scripts/libs/jquery.ui.touch-punch.js",
                    "~/Scripts/libs/d3.v3.js",
                    "~/Scripts/libs/jquery.isotope.js",
                    "~/Scripts/libs/jquery.browser.js",
                    "~/Scripts/libs/native-console.js",
                    "~/Scripts/libs/jquery.cycle.all.js",
                    "~/Scripts/libs/jquery.easing.1.3.js",
                    "~/Scripts/libs/modernizr.js",
                    "~/Scripts/libs/jquery.fancybox.js",
                    "~/Scripts/libs/jquery.mCustomScrollbar.js",
                    "~/Scripts/libs/gauge.js",
                    "~/Scripts/libs/jquery.touchwipe.js",
                    "~/Scripts/preload.image.plugin.js",
                    "~/Scripts/gauge.plugin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/loginjs").Include(
                    "~/Scripts/login.js",
                    "~/Scripts/util-formsubmit.js",
                    "~/Scripts/plugin-spin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/fusioncore").Include(
                    "~/Scripts/fusion.js",
                    "~/Scripts/favourite-schemes.js",
                    "~/Scripts/util.js",
                    "~/Scripts/functions.js",
                    "~/Scripts/page.js",
                    "~/Scripts/plugin-expandingarea.js",
                    "~/Scripts/plugin-fit.js",
                    "~/Scripts/plugin-linkable.js",
                    "~/Scripts/plugin-slider.js",
                    "~/Scripts/sliderWithText.js",
                    "~/Scripts/sliderWithSoftLimit.js",
                    "~/Scripts/basis-menu.js",
                    "~/Scripts/scroll.js",
                    "~/Scripts/compacting.header.js",
                    "~/Scripts/tabs.js",
                    "~/Scripts/gauge.js",
                    "~/Scripts/util-form.js",
                    "~/Scripts/util-headerselectormenu.js",
                    "~/Scripts/util-donuthelper.js",
                    "~/Scripts/util-assumptionspairhandler.js",
                    "~/Scripts/util-assetsbreakdownhelper.js",
                    "~/Scripts/charts-donuthandler.js",
                    "~/Scripts/dashboard.handler.js",
                    "~/Scripts/plugin-legendexpando.js",
                    "~/Scripts/plugin-styleableselect.js",
                    "~/Scripts/navigation.js",
                    "~/Scripts/importantmessage.js",
                    "~/Scripts/lightbox.js",
                    "~/Scripts/customscrollbar.js",
                    "~/Scripts/preloadimages.js",
                    "~/Scripts/jquery.appear.js"                    
            ));

            bundles.Add(new ScriptBundle("~/bundles/highcharts").Include(
                    "~/Scripts/libs/highcharts/js/highcharts.js",
                    "~/Scripts/libs/highcharts/js/highcharts-more.js",
                    "~/Scripts/libs/highcharts/js/modules/exporting.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/account").Include(
                    "~/Scripts/plugin-schemesearcher.js",
                    "~/Scripts/util-formsubmit.js",
                    "~/Scripts/account.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/analysis").Include(

                    "~/Scripts/analysis-panelstate.js",
                    "~/Scripts/analysis-originalvalues.js",
                    "~/Scripts/analysis-scenarios.js",
                    
                    "~/Scripts/analysis-lens-cash.js",
                    "~/Scripts/analysis-lens-time.js",
                    "~/Scripts/analysis-lens-risk.js",
                    "~/Scripts/analysis-lens-accounting.js",

                    "~/Scripts/journey-plan-lens.js",
                    "~/Scripts/cashflow-lens.js",
                    "~/Scripts/var-waterfall-lens.js",
                    "~/Scripts/var-funnel-lens.js",
                    "~/Scripts/recoveryplan.js",
                    "~/Scripts/util-liabilitiestabler.js",
                    "~/Scripts/analysis-panel-liability.js",
                    "~/Scripts/analysis-panel-asset.js",
                    "~/Scripts/investmentstrategy.js",
                    "~/Scripts/fro.js",
                    "~/Scripts/futurebenefits.js",
                    "~/Scripts/pie.js",
                    "~/Scripts/etv.js",
                    "~/Scripts/abf.js",
                    "~/Scripts/calcs-strain.js",
                    "~/Scripts/insurance.js",

                    "~/Scripts/analysis.js"

#if DEBUG
                    , "~/Scripts/test.js"
                    , "~/Scripts/test-insurance.js"
                    , "~/Scripts/test-runner.js"
#endif
                ));

            bundles.Add(new ScriptBundle("~/bundles/baseline").Include(                    
                    "~/Scripts/util-liabilitiestabler.js",
                    "~/Scripts/valuationresults.js",
                    "~/Scripts/baseline.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/evolution").Include(
                    "~/Scripts/tracker.js",
                    "~/Scripts/analysisofsurplus.js",
                    "~/Scripts/util-liabilitiestabler.js",
                    "~/Scripts/util-assetstabler.js",
                    "~/Scripts/evolution-assumptions-panel.js",
                    "~/Scripts/evolution.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/journeyplan").Include(
                    "~/Scripts/journeyplan2.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/maintenance").Include(
                    "~/Scripts/maintenance.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/solutions").Include(
                    "~/Scripts/accordion.js",
                    "~/Scripts/responsive.js",
                    "~/Scripts/progresscycle.js",
                    "~/Scripts/solutions.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/home").Include(
                    "~/Scripts/mini-evolution-lens.js",
                    "~/Scripts/home.js",
                    "~/Scripts/cashflow-lens.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/indices").Include(
                    "~/Scripts/admin.js",
                    "~/Scripts/util-formsubmit.js",
                    "~/Scripts/fileinput.js",
                    "~/Scripts/indices.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/curvedata").Include(
                    "~/Scripts/admin.js",
                    "~/Scripts/util-formsubmit.js",
                    "~/Scripts/fileinput.js",
                    "~/Scripts/curvedata.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/scheme").Include(
                    "~/Scripts/plugin-schemesearcher.js",
                    "~/Scripts/admin.js",
                    "~/Scripts/admin-scheme.js",
                    "~/Scripts/util-formsubmit.js",
                    "~/Scripts/fileinput.js",
                    "~/Scripts/ghosting.js",
                    "~/Scripts/scheme-documents.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/users-manage").Include(
                    "~/Scripts/util-findschemestrict.js",
                    "~/Scripts/plugin-usersearcher.js",
                    "~/Scripts/util-formsubmit.js",
                    "~/Scripts/admin.js",
                    "~/Scripts/admin-user.js", 
                    "~/Scripts/admin-useraddit.js", 
                    "~/Scripts/ghosting.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/users-list").Include(
                    "~/Scripts/plugin-schemesearcher.js",
                    "~/Scripts/admin.js",
                    "~/Scripts/admin-userlist.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/application").Include(
                    "~/Scripts/plugin-spin.js",
                    "~/Scripts/util-formsubmit.js",
                    "~/Scripts/admin.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/cover").Include(
                    "~/Scripts/plugin-spin.js",
                    "~/Scripts/util-formsubmit.js"
                ));
        }
    }
}