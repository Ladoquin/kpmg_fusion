﻿namespace Fusion.WebApp.Services
{
    using System.IO;
    using System.Web.Mvc;

    public class RazorViewRenderer : IRazorViewRenderer
    {
        readonly Controller controller;

        public RazorViewRenderer(Controller controller)
        {
            this.controller = controller;
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}