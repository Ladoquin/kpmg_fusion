﻿namespace Fusion.WebApp.Services
{
    using FlightDeck.ServiceInterfaces;
    using WebMatrix.WebData;

    public class BasicWebSecurity : IAccountSecurityService
    {
        public bool IsAuthenticated
        {
            get
            {
                return WebSecurity.IsAuthenticated;
            }
        }

        public string CurrentUserName
        {
            get
            {
                return WebSecurity.CurrentUserName;
            }
        }

        public void Logout()
        {
            WebSecurity.Logout();
        }

        public bool UserExists(string username)
        {
            return WebSecurity.UserExists(username);
        }

        public int GetPasswordFailuresSinceLastSuccess(string username)
        {
            return WebSecurity.GetPasswordFailuresSinceLastSuccess(username);
        }

        public bool Login(string username, string password)
        {
            return WebSecurity.Login(username,password);
        }

        public bool ResetPassword(string passwordResetToken, string newPassword)
        {
            return WebSecurity.ResetPassword(passwordResetToken, newPassword);
        }

        public bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            return WebSecurity.ChangePassword(username, oldPassword, newPassword);
        }

        public string GeneratePasswordResetToken(string username)
        {
            return WebSecurity.GeneratePasswordResetToken(username);
        }
    }
}