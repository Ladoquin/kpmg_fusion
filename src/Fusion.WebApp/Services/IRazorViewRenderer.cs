﻿namespace Fusion.WebApp.Services
{
    public interface IRazorViewRenderer
    {
        string RenderRazorViewToString(string viewName, object model);
    }
}