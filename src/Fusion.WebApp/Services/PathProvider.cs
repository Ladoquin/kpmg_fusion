﻿namespace Fusion.WebApp
{
    using System.Web;

    public class PathProvider
    {
        private readonly HttpContextBase context;

        public PathProvider(HttpContextBase context)
        {
            this.context = context;
        }

        public string GetTemplatesFolder()
        {
            return context.Server.MapPath("~/Content/templates");
        }
    }
}