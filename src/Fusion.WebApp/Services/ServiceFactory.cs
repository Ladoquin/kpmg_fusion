﻿namespace Fusion.WebApp // abreviated namespace for convenience of consumers. 
{
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.ServiceInterfaces.Reports;
    using FlightDeck.Services;
    using FlightDeck.Services.Reports;
    using Fusion.WebApp.Models;
    using Fusion.WebApp.Services;
    using System.Web;
    using System.Web.Mvc;

    public static class ServiceFactory
    {
        public static T GetService<T>(Controller controller)
        {
            return ServiceFactory.GetService<T>(controller.ControllerContext.HttpContext);
        }

        public static T GetService<T>(HttpContextBase context)
        {
            var paramType = typeof(T);

            var user = context != null ? context.User : HttpContext.Current.User;

            object o = null;

            var servicesFactory = new FlightDeck.Services.ServiceFactory();

            if (paramType == typeof(IApplicationSettings))
                o = new ApplicationSettings();

            if (paramType == typeof(IRoleManager))
                o = new WebRoleManager();

            if (paramType == typeof(IEmailService))
                o = (IEmailService)new EmailService(AppSettings.SMTP);

            if (paramType == typeof(IIndexImportDetailService))
                o = servicesFactory.GetIndexImportDetailService();

            if (paramType == typeof(ICurveImportDetailService))
                o = servicesFactory.GetCurveImportDetailService();

            if (paramType == typeof(IPensionSchemeService))
                o = servicesFactory.GetPensionSchemeService(user);

            if (paramType == typeof(IUserProfileService))
                o = new UserProfileService(user, new WebRoleManager());

            if (paramType == typeof(IAccountingReporter))
                o = new AccountingReporter(new PathProvider(context).GetTemplatesFolder());

            if (paramType == typeof(IAccountService))
                o = new AccountService();
       
            if (paramType == typeof(IClientAssumptionService))
                o = new ClientAssumptionService(new Serializer());

            if (paramType == typeof(IViewService))
                o = new ViewService();

            if (paramType == typeof(IGlobalParametersService))
                o = new GlobalParametersService();

            if (paramType == typeof(ILogReader))
                o = new LogReader();

            if (paramType == typeof(IXmlSchemaValidator))
                o = new XmlSchemaValidator();

            if (paramType == typeof(ITemplateService))
                o = new TemplateService();

            if (paramType == typeof(IPasswordManager))
                o = new PasswordManager(new BasicWebSecurity());

            if (paramType == typeof(IIndicesService))
                o = servicesFactory.GetIndicesService();

            if (paramType == typeof(ISchemeSnapshotService))
                o = new SchemeSnapshotService();

            if (paramType == typeof(IPreCalculationService))
                o = servicesFactory.GetPreCalculationService();

            if (o != null)
                return (T)o;

            return default(T);
        }
    }
}
