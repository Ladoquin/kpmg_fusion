﻿namespace Fusion.WebApp.Services
{
    using System.Collections.Generic;
    using System.Web.Security;
    using WebMatrix.WebData;

    public class BasicMembershipProvider : IMembershipProvider
    {
        private readonly SimpleMembershipProvider provider;

        public BasicMembershipProvider()
        {
            provider = (SimpleMembershipProvider)Membership.Provider;
        }

        public string CreateUserAndAccount(string username, string password, IDictionary<string, object> values)
        {
            return provider.CreateUserAndAccount(username, password, values);
        }

        public virtual bool ValidateUser(string username, string password)
        {
            return provider.ValidateUser(username, password);
        }

        public string GeneratePasswordResetToken(string username)
        {
            return provider.GeneratePasswordResetToken(username);
        }

        public string GeneratePasswordResetToken(string username, int tokenExpirationInMinutesFromNow)
        {
            return provider.GeneratePasswordResetToken(username, tokenExpirationInMinutesFromNow);
        }
    }
}