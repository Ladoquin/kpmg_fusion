﻿namespace Fusion.WebApp.Services
{
    using System.Collections.Generic;

    public interface IMembershipProvider
    {
        string CreateUserAndAccount(string username, string password, IDictionary<string, object> values);
        bool ValidateUser(string username, string password);
        string GeneratePasswordResetToken(string username);
        string GeneratePasswordResetToken(string username, int tokenExpirationInMinutesFromNow);
    }
}
