﻿namespace Fusion.WebApp.Services
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System.Collections.Generic;
    
    public interface ISessionManager
    {
        UserProfile GetUserProfile();
        IScheme GetScheme();
        string GetSchemeName();
        ISessionManager SetUserProfile(UserProfile profile);
        ISessionManager RefreshUserProfile();
        ISessionManager RefreshScheme();
    }
}