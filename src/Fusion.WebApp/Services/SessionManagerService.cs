﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using log4net;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Fusion.WebApp.Services
{
    public class SessionManagerService : ISessionManager
    {
        private readonly ControllerBase controller;

        private HttpSessionStateBase Session { get { return controller.ControllerContext.HttpContext.Session; } }
        private IPrincipal User { get { return controller.ControllerContext.HttpContext.User; } }

        public SessionManagerService(ControllerBase controller)
        {
            this.controller = controller;
        }

        public UserProfile GetUserProfile()
        {
            UserProfile profile = Session["UserProfile"] as UserProfile;
            if (profile == null || profile.UserName != User.Identity.Name || profile.PasswordFailuresSinceLastSuccess > AppSettings.AllowedFailedLoginAttempts)
            {
                if (!string.IsNullOrEmpty(User.Identity.Name))
                {
                    profile = ServiceFactory.GetService<IUserProfileService>(controller.ControllerContext.HttpContext).GetByUsername(User.Identity.Name);
                    Session["UserProfile"] = profile;
                }
            }
            return profile;
        }

        public string GetSchemeName()
        {
            if (Session["SchemeName"] == null)
                Session["SchemeName"] = ServiceFactory.GetService<IPensionSchemeService>(controller.ControllerContext.HttpContext).GetActiveSchemeName();

            return (string)Session["SchemeName"];
        }

        public IScheme GetScheme()
        {
            var profile = GetUserProfile();
            var username = string.Empty;

            if (profile == null || profile.ActiveSchemeDetailId == null)
            {
                Session["Scheme"] = null;
                return null;
            }

            var scheme = Session["Scheme"] as IScheme;
            if (scheme != null) 
            {
                Session["SchemeName"] = scheme.SchemeName;
                return scheme;
            }

            if (profile != null)
                username = profile.UserName;

            // just to get scheme name and set it in session (for better error handling)
            Session["SchemeName"] = ServiceFactory.GetService<IPensionSchemeService>(controller.ControllerContext.HttpContext).GetActiveSchemeName();

            try
            {
                scheme = ServiceFactory.GetService<IPensionSchemeService>(controller.ControllerContext.HttpContext).GetActive(); // deliberately getting fresh service from factory to help with index caching 
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(this.GetType()).ErrorFormat("Cannot load scheme: '{0}', username: '{1}', exception: {2}", Session["SchemeName"], username, ex.ToString());
            }

            Session["Scheme"] = scheme;
            return scheme;
        }

        public ISessionManager SetUserProfile(UserProfile profile)
        {
            Session["UserProfile"] = profile;
            return this;
        }

        public ISessionManager RefreshUserProfile()
        {
            Session["UserProfile"] = null;
            GetUserProfile();
            return this;
        }

        public ISessionManager RefreshScheme()
        {
            Session["Scheme"] = null;
            ResetJourneyPlanDisplayOptions();
            GetScheme();
            return this;
        }

        public ISessionManager ResetJourneyPlanDisplayOptions()
        {
            Session["JourneyPlanDisplayOptions"] = null;
            return this;
        }
    }
}