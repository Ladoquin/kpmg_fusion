﻿namespace Fusion.WebApp.Services
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Resources;
    using System.Web.Caching;
    using System.Web.Mvc;
    
    public class LabelService
    {
        private readonly Controller controller;

        public LabelService(Controller controller)
        {
            this.controller = controller;
        }

        public Dictionary<string, string> GetDefaultAssumptionLabels(IBasis currentBasis)
        {
            Dictionary<string, string> labels;
            if (controller.HttpContext.Cache["Fusion.WebApp.LabelResources"] == null)
            {
                var rm = new ResourceManager("Fusion.WebApp.LabelResources", Assembly.GetExecutingAssembly());

                string[] labelKeys =
                {
                    AssumptionType.PreRetirementDiscountRate.ToString(),
                    AssumptionType.PostRetirementDiscountRate.ToString(),
                    AssumptionType.PensionDiscountRate.ToString(),
                    AssumptionType.SalaryIncrease.ToString(),
                    AssumptionType.InflationRetailPriceIndex.ToString(),
                    AssumptionType.InflationConsumerPriceIndex.ToString(),
                    SimpleAssumptionType.LifeExpectancy65.ToString(),
                    SimpleAssumptionType.LifeExpectancy65_45.ToString(),
                    SimpleAssumptionType.SacrificeProportion.ToString(),
                    "DefaultSacrificeProportion"
                };

                controller.HttpContext.Cache.Add("Fusion.WebApp.LabelResources", labelKeys.ToDictionary(x => x, x => rm.GetString(x)), null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 20, 0), CacheItemPriority.Normal, null);
                labels = (Dictionary<string, string>)controller.HttpContext.Cache["Fusion.WebApp.LabelResources"];
                labels["DefaultSacrificeProportion"] = labels[SimpleAssumptionType.SacrificeProportion.ToString()];
            }
            else
                labels = (Dictionary<string, string>)controller.HttpContext.Cache["Fusion.WebApp.LabelResources"];

            var commAllowanceLabel = currentBasis.GetSimpleAssumptions()[SimpleAssumptionType.CommutationAllowance].Key;

            //replace SacrificeProportion label with commAllowanceLabel
            if (!string.IsNullOrWhiteSpace(commAllowanceLabel)
              && labels.ContainsKey(SimpleAssumptionType.SacrificeProportion.ToString()))
                labels[SimpleAssumptionType.SacrificeProportion.ToString()] = commAllowanceLabel;
            else if (labels.ContainsKey(SimpleAssumptionType.SacrificeProportion.ToString()))
                labels[SimpleAssumptionType.SacrificeProportion.ToString()] = labels["DefaultSacrificeProportion"];

            return labels;
        }
    }
}