﻿namespace Fusion.WebApp
{
    using System;
    using System.IO;
    using System.Web;

    public class AppInfo
    {
        public static DateTime DeploymentDate
        {
            get
            {
                return Directory.GetLastWriteTime(HttpContext.Current.Server.MapPath("~/bin"));
            }
        }
    }
}