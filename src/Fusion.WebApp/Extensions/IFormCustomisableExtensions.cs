﻿namespace Fusion.WebApp.Extensions
{
    using Fusion.WebApp.Models;

    public static class IFormCustomisableExtensions
    {
        public static IFormCustomisable WithFormHandler(this IFormCustomisable form, string actionName, string controllerName)
        {
            form.FormHandler = new FormContext { ActionName = actionName, ControllerName = controllerName };
            return form;
        }
    }
}