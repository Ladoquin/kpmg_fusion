﻿using System.Web.Mvc;

namespace Fusion.WebApp.Extensions
{
    public static class TempDataExtensions
    {
        public static T GetValue<T>(this TempDataDictionary TempData, string key)
        {
            var d = default(T);
            if (TempData.ContainsKey(key) && TempData[key] != null)
            {
                d = (T)TempData[key];
            }
            return d;
        }
    }
}