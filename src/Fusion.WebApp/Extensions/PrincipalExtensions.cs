﻿using FlightDeck.DomainShared;
using System.Linq;
using System.Security.Principal;

namespace Fusion.WebApp.Extensions
{
    public static class PrincipalExtensions
    {
        public static bool IsInRole(this IPrincipal user, params RoleType[] roles)
        {
            foreach (var role in roles)
            {
                if (user.IsInRole(role.ToString()))
                    return true;
            }

            return false;
        }

        public static bool IsInRole(this UserProfile user, params RoleType[] roles)
        {
            return roles.Intersect(new RoleType[] { user.Role }).Any();
        }
    }
}