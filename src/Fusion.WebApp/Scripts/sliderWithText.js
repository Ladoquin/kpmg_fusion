﻿// Extends the jQuery UI Slider widget to display the value in an assigned element.
// 
(function ($) {
    var extensionMethods = {
        multiplier: 1, // to cope with different value formats in existing server code.
        updatedMin: null,
        minExact: null,
        format: function (args) {
            var el = args.el,
                value = args.value,
                fix = typeof args.fix == "undefined" ? true : args.fix,
                fixPrecision = null;

            if (el.hasClass("years")) {

            } else if (el.hasClass("years1dp")) {

            } else if (el.hasClass("yearsNoUnits")) {

            } else if (el.hasClass("months")) {

            } else if (el.hasClass("millionsNoUnits")) {

            } else if (el.hasClass("poundsMillions")) {

            } else if (el.hasClass("poundsMillionsRoundUp1dp")) {
                value = (value * 0.000001);
                fixPrecision = 1;
            } else if (el.hasClass("poundsMillionsRoundUp2dp")) {
                value = (value * 0.000001);
                fixPrecision = 2;
            }
            else if (el.hasClass("wholenumber")) {
                value = (value * 100);
                fixPrecision = 0;
            }
            else if (el.hasClass("wholePercentage")) {
                value = (value * 100);
                fixPrecision = 0;
            }
            else {
                value = (value * 100);
                fixPrecision = 2;
            }

            if (fix && fixPrecision !== null)
                value = value.toFixed(fixPrecision);

            return value;
        },
        setMin: function (uMin, slider) {
            this.updatedMin = this.format({
                el: slider,
                value: uMin
            });// uMin / scope.multiplier;
            if (slider.hasClass("allowUnderlyingPrecision")) {
                this.minExact = this.format({
                    el: slider,
                    value: uMin,
                    fix: false
                });
            };
        },
        setTextValueElement: function (initialValue) {

            var el = this.options.elementSelector,
                min = this.options.min,
                max = this.options.max,
                value = initialValue || this.options.value,
                postBackFunction = this.options.stop,
                $textElement = this.options.textElement,
                submitCallback = this.options.submitCallback || null,
                leadingSymbol = '',
                trailingSymbol = '',
                precision,
                $textValue,
                textSelected = false,
                dataType = '',
                scope = this,
                initialText = '',
                sign = '';

            //Supported classes on an assumptionsSlider
            //Default is %
            if (el.hasClass("years")) {
                trailingSymbol = 'yrs';
                precision = 0;
            } else if (el.hasClass("years1dp")) {
                trailingSymbol = 'yrs';
                precision = 1;
                max = (max * 1.0).toFixed(1);
                min = (min * 1.0).toFixed(1);
            } else if (el.hasClass("yearsNoUnits")) {
                precision = 0;
            } else if (el.hasClass("months")) {
                precision = 0;
            } else if (el.hasClass("millionsNoUnits")) {
                precision = 0;
            } else if (el.hasClass("poundsMillions")) {
                leadingSymbol = fusion.util.getCurrencySymbol();
                trailingSymbol = 'M';
                precision = 0;
            } else if (el.hasClass("poundsMillionsRoundUp1dp")) {
                leadingSymbol = fusion.util.getCurrencySymbol();
                trailingSymbol = 'M';
                precision = 1;
                max = (max * 0.000001).toFixed(1);
                min = (min * 0.000001).toFixed(1);
                scope.multiplier = 1000000;
            } else if (el.hasClass("poundsMillionsRoundUp2dp")) {
                leadingSymbol = fusion.util.getCurrencySymbol();
                trailingSymbol = 'M';
                precision = 2;
                max = (max * 0.000001).toFixed(2);
                min = (min * 0.000001).toFixed(2);
                scope.multiplier = 1000000;
            }
            else if (el.hasClass("wholenumber")) {
                trailingSymbol = '%';
                max = (max * 100).toFixed(0);
                min = (min * 100).toFixed(0);
                precision = 0;
            }
            else if (el.hasClass("wholePercentage")) {
                trailingSymbol = '%';
                max = (max * 100).toFixed(0);
                min = (min * 100).toFixed(0);
                precision = 0;
                scope.multiplier = 0.01;
            }
            else {
                trailingSymbol = '%';
                max = (max * 100).toFixed(2);
                min = (min * 100).toFixed(2);
                precision = 2;
                scope.multiplier = 0.01;
            }

            value = this.format({
                el: el,
                value: value
            });

            if (el.hasClass("gilts-plus")) {
                leadingSymbol = "<span class='leading-gilts'>gilts</span>";
                if (value >= 0) {
                    sign = "<span class='sign'>+</span>";
                }
                else {
                    sign = "<span class='sign'>-</span>";;
                }
                value = Math.abs(value).toFixed(2);
            }
            if (el.hasClass("hedging-cashflows")) {
                leadingSymbol = "<span class='leading-hedging-cashflows'></span><span>(</span>";
                trailingSymbol = "% of cashflows)";
            }

            $textElement.addClass('can-edit');
            $textElement.addClass('can-edit-container');

            $textValue = $("<span class='slider-text-container'>" + leadingSymbol + "<span class='slider-text-value' contenteditable='true'>" + sign + value + "</span>" + trailingSymbol + "</span>");

            $textElement.html('');
            $textElement.append($textValue);

            initialText = $textValue.find('.slider-text-value').html();
            el.data('editableNumber', $textValue.find('.slider-text-value'));

            //Billion support '£1000.00M' - 7 numeric chars
            if (String(max).length >= 7) {
                el.closest("table").find('.slider-text-container').addClass('billion-support');
            }

            var selectText = function ($textElement) {
                var range, selection;

                if (window.getSelection && document.createRange) {
                    selection = window.getSelection();
                    range = document.createRange();
                    range.selectNodeContents($textElement[0]);
                    selection.removeAllRanges();
                    selection.addRange(range);
                    textSelected = true;
                } else if (document.selection && document.body.createTextRange) {
                    range = document.body.createTextRange();
                    range.moveToElementText($textElement[0]);
                    range.select();
                    textSelected = true;
                }

                $textElement.parent().addClass('slider-text-container-selected');
            };

            //Select text on single click.
            $textValue.find('.slider-text-value').click(function () {
                selectText($(this));
                $(this).trigger("focus");//FF can't fix! http://bugs.jquery.com/ticket/13363
            });

            //Same effect when the container is clicked.
            $textValue.click(function () {
                selectText($(this).find('.slider-text-value'));
                $(this).find('.slider-text-value').trigger("focus");
            });

            //Drop focus on enter/return.
            //Don't validate selected text.
            //Don't allow length of text to be greater than that of the maximum value.
            $textValue.find('.slider-text-value').keydown(function (event) {
                var keyCode = event.keyCode,
                    lengthLimitReached = false;


                $(this).parent().addClass('dummyIE9Redraw');
                $(this).parent().removeClass('dummyIE9Redraw');

                if (event.charCode != typeof 'undefined' && event.charCode != null && event.keyCode == 0) {
                    keyCode = event.charCode;
                }

                if (keyCode == 10 || keyCode == 13) {
                    $(this).blur();
                    event.preventDefault();
                } else if (((keyCode > 31 &&
                    (keyCode < 48 || keyCode > 57) && (keyCode < 96 || keyCode > 105) &&
                     keyCode != 46 &&
                     keyCode != 45 &&
                     keyCode != 37 &&
                     keyCode != 39 &&
                     keyCode != 109 &&
                     keyCode != 110 &&
                     keyCode != 190 &&
                     keyCode != 189) || keyCode == 16) == true) {
                    event.preventDefault();
                    return;
                }

                if (textSelected === true) {
                    textSelected = false;
                    return;
                }

                lengthLimitReached = $(this).html().length >= String(max).length;
                if (lengthLimitReached === true && keyCode != 8 && keyCode != 46 && keyCode != 37 && keyCode != 39) {
                    event.preventDefault();
                }

            });

            $textValue.find('.slider-text-value').focus(function (event) {
                initialText = $(this).html()
            });

            //Process the text and update the slider when the field loses focus.
            //Default to min/max values if they are exceeded.
            //Send new values to the server
            $textValue.find('.slider-text-value').blur(function (event) {
                var changes = true,
                    $slider = $(this).closest("tr").find('.assumptionsSlider'),
                    $sliderHandle = $(this).closest("tr").find('.ui-slider-handle'),
                    textSelected = false,
                    finalMin = scope.minExact || scope.updatedMin || min;

                $(this).parent().removeClass('slider-text-container-selected');

                var value = parseFloat($(this).html());

                if (initialText == $(this).html()) {
                    return; //no changes
                } else if ($.isNumeric($(this).html()) == false) {
                    $(this).html(initialText);
                    return; //no changes
                } else if (parseFloat($(this).html()) > max) {
                    $(this).html(max);
                    value = max;
                } else if (parseFloat($(this).html()) < finalMin || $(this).html() == '' || $(this).html() == null || $(this).html() == typeof 'undefined') {
                    value = finalMin;
                }

                //Get user friendly display value to (temporarily) display between setting and postback
                var friendlyValue = parseFloat(value).toFixed(precision);
                //Scale precision number to appropriate units
                value = parseFloat(value) * scope.multiplier;
                //Store our manually entered value in the correct units where it can't be coerced by the slider
                $(this).data('finalValue', value);
                //Using the option approach to set the value will allow a value outside of an increment to be set
                el.slider("option", "value", value);

                //Make sure we carry on displaying our manually entered value.
                $(this).html(friendlyValue);

                if (submitCallback != null) {
                    submitCallback(null, {
                        handle: $sliderHandle,
                        value: value
                    }, $slider);
                }

                //trigger slidestop here to set as dirty.
                $slider.trigger('slidestop');

                postBackFunction(null, {
                    handle: $sliderHandle,
                    value: value
                }, $slider);

            });

        },
        updateSliderValue: function (args) {
            var el = args.el || $(this)[0].element,
                value = args.value;

            value = this.format({
                el: el,
                value: value
            });

            if (el.hasClass("gilts-plus")) {
                if (value >= 0) {
                    el.data('editableNumber').html('+' + Math.abs(value).toFixed(2));
                }
                else {
                    el.data('editableNumber').html('-' + Math.abs(value).toFixed(2));
                }
            } else {
                el.data('editableNumber').html(value);
            }

            el.closest("tr").find('.slider-text-value').data('finalValue', value * this.multiplier);
        },
        disableEditableElement: function (scope) {
            var editableElement = $(scope).closest("tr").find('.slider-text-value'),
                container = $(scope).closest("tr").find('.slider-text-container');

            editableElement.addClass('slider-text-disabled');
            editableElement.unbind();
            editableElement.attr('contenteditable', 'false');
            editableElement.closest("td").removeClass('can-edit');

            container.addClass('slider-text-disabled');
            container.unbind();
        }
    };

    $.extend(true, $['ui']['slider'].prototype, extensionMethods);


})(jQuery);