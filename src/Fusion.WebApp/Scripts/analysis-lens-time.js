﻿fusion.analysis.timeLens = function() {

    var that = this;
    var initialised = false;
    var journeyPlanLens = new JourneyPlanLens();
    var cashflowLens = new fusion.analysis.Cashflow();
    var activeTab = "journeyPlanTab";

    var initialise = function() {

        $("#timeLensTabs").tabs({
            activate: function (event, ui) {
                if (ui.newPanel.attr('id') == 'journeyPlanTab') {
                    cashflowLens.deactivateLens();
                    journeyPlanLens.activateLens();
                }
                else if (ui.newPanel.attr('id') == 'cashflowTab') {
                    journeyPlanLens.deactivateLens();
                    cashflowLens.activateLens();
                }
            }
        });

        initialised = true;
    };

    this.activate = function () {

        if (!initialised) {
            initialise();
        };

        if ($("#timeLensTabs").tabs("option", "active") == 0) {
            journeyPlanLens.activateLens();
            cashflowLens.deactivateLens();
        } else {
            journeyPlanLens.deactivateLens();
            cashflowLens.activateLens();
        }
    };

    this.deactivate = function() {
        journeyPlanLens.deactivateLens();
        cashflowLens.deactivateLens();
    };

    this.update = function () {
        journeyPlanLens.updateLens();
        cashflowLens.updateLens();
    };
};



