﻿fusion.test = {};

fusion.test.assert = function (expected, actual, msg) {
    if (expected != actual) {
        console.error(msg + " ERROR: expected '" + expected + "', actual '" + actual + "'");
    };
};