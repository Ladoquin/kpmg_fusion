/*
*	KPMG
*	preloadimages.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This helps with performance preloading images into the cache.*/

$.Core.preloadimages ={
	init: function(){
		this.preloadFollowingImages();
	},
	preloadFollowingImages: function(){		
		var preloadArray = new Array();
			//_preload logo
			//preloadArray.push('images/logo.png');			
		$(preloadArray).preload();
	}	
};