﻿fusion.analysis.PiePanel = function() {

    var that = this;
    var initialised = false;

    this.changed = $.Callbacks();

    this.reset = function () {

        $.getJSON('/Json/ResetPie', function (pieData) {
            that.refresh(pieData);
            that.changed.fire({ id: that.id, data: pieData });
        });
    };

    var setPie = function () {
        var pie = {},
            finalEl,
            finalAu,
            finalTur

        finalEl = $('#pieEligibleLiabilityPerc').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalEl == null || finalEl == typeof 'undefined')
            finalEl = $('#pieEligibleLiabilityPerc').slider("option", "value");
        pie.eligibleLiability = finalEl;

        finalAu = $('#pieActualUplift').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalAu == null || finalAu == typeof 'undefined')
            finalAu = $('#pieActualUplift').slider("option", "value");
        pie.actualUplift = finalAu;

        finalTur = $('#pieTakeUpRate').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalTur == null || finalTur == typeof 'undefined')
            finalTur = $('#pieTakeUpRate').slider("option", "value");
        pie.takeUpRate = finalTur;

        var json = {
            pieData: JSON.stringify(pie)
        };

        $.post('/Json/SetPie', json, function (pieData) {
            that.refresh(pieData);
            that.changed.fire({ id: that.id, data: pieData });
        });
    };

    var initialise = function (pieData) {

        var updateCell = function (event, ui) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: ui.value
            });
        };

        var updatePie = function (event, ui) {
            setPie();
        };

        $("#pieEligibleLiabilityPerc").data("original", pieData.setup.LiabilityExchanged);
        $("#pieActualUplift").data("original", pieData.setup.ValueShared);
        $("#pieTakeUpRate").data("original", pieData.setup.TakeUpRate);

        $('#pieInputTable .assumptionsSlider')
            .each(function () {
                $(this).slider({
                    animate: 400,
                    range: 'min',
                    min: $(this).data('min'),
                    max: $(this).data('max'),
                    value: 0,
                    step: $(this).data('increment'),
                    slide: updateCell,
                    stop: updatePie,
                    create: function () { $(this).slider('setTextValueElement') },
                    elementSelector: $(this),
                    textElement: $(this).parent().siblings().last()
                });
            });

        $('#resetPie').click(function () {
            that.reset();
        });

        initialised = true;
    };
    
    this.refresh = function (pieData) {

        if (!initialised) { initialise(pieData); }

        $('#piePensionerLiability').html(fusion.util.formatCurrency(pieData.liabilityBeforePie));        
        $('#pieLiabilityExcludingLevelPensions').html(fusion.util.formatCurrency(pieData.LiabilityExcludingLevelPensions));
        $('#pieValueOfPensionIncreases').html(fusion.util.formatCurrency(pieData.ValueOfIncreases1));
        $('#pieEligibleLiability').html(fusion.util.formatCurrency(pieData.EligibleForExchange));
        $('#pieLiabilityWithIncsRemoved').html(fusion.util.formatCurrency(pieData.ZeroIncreases));
        $('#pieValueOfFutureIncreases').html(fusion.util.formatCurrency(pieData.ValueOfFutureIncreases));
        $('#pieCostNeutralUplift').html((pieData.CostNeutralUplift * 100).toFixed(2) + '%');
        $('#pieValueShared').html((pieData.ValueShared * 100).toFixed(2) + '%');
        $('#piePensionerLiability2').html(fusion.util.formatCurrency(pieData.liabilityBeforePie));
        $('#pieLiabilityAfterPie').html(fusion.util.formatCurrency(pieData.liabilityAfterPie));
        $('#pieChangeInLiabilities').html(fusion.util.formatCurrency(pieData.changeInLiabilities));

        $('#pieEligibleLiabilityPerc').slider('value', pieData.PortionEligible);
        $('#pieEligibleLiabilityPerc').slider('setTextValueElement', pieData.PortionEligible);

        $('#pieActualUplift').slider('value', pieData.ActualUplift);
        $('#pieActualUplift').slider('setTextValueElement', pieData.ActualUplift);

        $('#pieTakeUpRate').slider('value', pieData.TakeUpRate);
        $('#pieTakeUpRate').slider('setTextValueElement', pieData.TakeUpRate);
    };
};


