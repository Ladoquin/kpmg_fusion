var VarFunnelLens = function() {

    var that = this;

    this.dataUrl = '/Json/VarFunnelData';

    this.chart = null;

    this.isStale = true;
    this.isActiveLens = false;
    this.beforeData;

    this.activateLens = function() {
        this.isActiveLens = true;
        if (this.isStale) {
            var thisLens = this;
            $.getJSON(this.dataUrl, function (data) {
                thisLens.refreshChart(data);
                thisLens.isStale = false;
            });
        }
    };


    this.deactivateLens = function () {
        this.isActiveLens = false;
    };


    this.updateLens = function() {
        if (this.isActiveLens) {
            var thisLens = this;
            $.getJSON(this.dataUrl, function (data) {
                thisLens.refreshChart(data);
                thisLens.isStale = false;
            });

        } else {
            this.isStale = true;
        }
    };



    this.initialiseLens = function(chartDiv) {

        this.chart = new Highcharts.Chart({
            chart: {
                type:'spline',
                renderTo: chartDiv,
                backgroundColor: 'none',
                plotBorderWidth: 1,
                plotBackgroundColor: 'white'

            },

            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: null,
            },
            xAxis: {
                categories: [''],
            },
            yAxis: [{
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels
                },
                maxPadding: 0.1,
                minPadding: 0.1,
            }, {
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels
                },
                maxPadding: 0.1,
                minPadding: 0.1,
                linkedTo: 0,
                opposite: true,
            }],
            plotOptions: {
                spline: {
                    lineWidth: 4,
                    states: {
                        hover: {
                            lineWidth: 5
                        }
                    },
                    marker: {
                        enabled: true,
                        symbol:'circle',
                        radius: 5,
                    },
                }
            },
            tooltip: {
                hideDelay: 0,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                followPointer: true,
                formatter: function () {
                    var tt = this.x + "<br/><br/>" + this.series.name + "<br/><br/>Before: ";
                    if (that.beforeData != null) {
                        var before = that.beforeData[this.series.name][this.point.x];
                        tt += fusion.util.formatCurrencyAbsolute(before) + ' ' + (before < 0 ? 'deficit' : 'surplus');
                        tt += "<br/><br/>After: ";
                    }
                    tt += fusion.util.formatCurrencyAbsolute(this.y) + ' ' + (this.y < 0 ? 'deficit' : 'surplus');
                    return tt;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            legend: {
                layout: 'horizontal',
                verticalAlign: 'bottom',
                borderWidth: 0,
                borderRadius: 0,
                padding: 10,

            },
            series: [{
                name: '95th percentile',
                data: [0, 0, 0],
                color: '#006400'
            }, {
                name: '80th percentile',
                data: [0, 0, 0],
                color: '#90EE90'
            }, {
                name: 'Median',
                data: [0, 0, 0],
                color: '#00BFFF'
            }, {
                name: '20th percentile',
                data: [0, 0, 0],
                color: '#FFA500'
            }, {
                name: '5th percentile',
                data: [0, 0, 0],
                color: '#8B0000'
            }],
        });
    };



    this.refreshChart = function (varFunnelData) {

        var positiveColor = '#9e3039';
        var negativeColor = '#6a7f10';

        var after = varFunnelData.after;

        if (varFunnelData.before != null) {
            that.beforeData = {
                '95th percentile': varFunnelData.before.percentile95,
                '80th percentile': varFunnelData.before.percentile80,
                'Median': varFunnelData.before.median,
                '20th percentile': varFunnelData.before.percentile20,
                '5th percentile': varFunnelData.before.percentile5
            };
        }
        else {
            that.beforeData = null;
        }

        this.chart.xAxis[0].setCategories(after.years, false);

        if (after.percentile95.length != this.chart.series[0].data.length) {
            this.chart.series[0].setData(after.percentile95, false);
            this.chart.series[1].setData(after.percentile80, false);
            this.chart.series[2].setData(after.median, false);
            this.chart.series[3].setData(after.percentile20, false);
            this.chart.series[4].setData(after.percentile5, false);
        } else {
            for (var i = 0; i < after.percentile95.length; i++) {
                this.chart.series[0].data[i].update(after.percentile95[i], false);
                this.chart.series[1].data[i].update(after.percentile80[i], false);
                this.chart.series[2].data[i].update(after.median[i], false);
                this.chart.series[3].data[i].update(after.percentile20[i], false);
                this.chart.series[4].data[i].update(after.percentile5[i], false);
            }
        }


        this.chart.redraw();
    };
};
