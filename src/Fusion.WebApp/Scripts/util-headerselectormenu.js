﻿fusion.util.headerSelectMenu = (function () {

    var initialise = function() {
        $(".header-selector-menu").prev("a").click(function(e) {
            var menuId = this.id;
            $.each($('.header-selector-menu'), function (idx, menu) {
                if ($(this).prev("a")[0].id === menuId) {
                    $(this).slideDown(100);
                }
                else {
                    $(this).slideUp(100);
                };
            });
            e.preventDefault();
            e.stopPropagation();
        });
        $('body').click(function () {
            $('.header-selector-menu').slideUp();
        });
    };

    return {
        init: function () {
            initialise();
        }
    };

}());