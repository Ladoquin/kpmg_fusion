/*
*	KPMG
*	customscrollbar.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This invokes a customscrollbar. */

$.Core.customscrollbar ={
	invoke: function(el){
		var that = this;

		//only apply scroll ONCE
		if(!$(el).hasClass("customscrolljob")){
			//console.log(el);
		
			$(el).mCustomScrollbar({
				scrollInertia:550,
				horizontalScroll:true,
				mouseWheelPixels:116,
				scrollButtons:{
					enable:true,
					scrollType:"pixels",
					scrollAmount:116
				},
				theme:"dark",
				callbacks:{
					onScroll:function(){ 
						that.snapScrollbar(); 
					}
				},
				advanced:{
					updateOnContentResize: true
				}				
			});
			$(el).addClass("customscrolljob");
		}

	},
	snapScrollbar: function(){

	},
	updateCustomScrolls: function(){
		$('.customscrolljob:visible').each(function() {
			$(this).mCustomScrollbar("update");
		});			
	}
};