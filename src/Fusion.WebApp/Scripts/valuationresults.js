﻿
var ValuationResultsClass = function() {

    var that = this;

    this.balanceChart = null;
    this.valuationChart = null;

    var valuationChartLabelRotation = 0;

    var initialiseValuationChart = function () {

        that.balanceChart = new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: 'valuationResultBalanceChartDiv',
                marginBottom: 120
            },

            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: null
            },
            xAxis: {
                labels: {
                    y: 20,
                    rotation: valuationChartLabelRotation,
                    formatter: function () {
                        return this.value.name + "<br/>" + fusion.util.formatDateShort(this.value.anchorDate);
                    }
                }
            },
            yAxis: [{
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                },
            }, {
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                },
                linkedTo: 0,
                opposite: true,
            }],
            plotOptions: {
                column: {
                    pointPadding: 0,
                    borderWidth: 0,
                    events: {
                        click: function (p) {
                            fusion.selectBasis(p.point.category.id)
                        },
                        mouseOut: function () {
                            $('.otherBasisColumnHovered').removeClass('otherBasisColumnHovered');
                        }
                    },
                }
            },
            tooltip: {
                hideDelay: 0,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                followPointer: true,
                formatter: function () {
                    $('.otherBasisColumnHovered').removeClass('otherBasisColumnHovered');
                    $('.otherBasisColumn:contains(' + this.x.name + ')').addClass('otherBasisColumnHovered');

                    return this.x.name + '<br/><br/>' + (this.y < 0 ? 'Deficit: ' : 'Surplus: ') + fusion.util.formatCurrencyAbsolute(this.y);
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            legend: {
                enabled: false,
            },
            series: [{
                name: 'Bases',
                color: '#6a7f10',
                negativeColor: '#9e3039',
            }],

        });
    };

    this.refresh = function (data) {

        var bases = [];
        var balances = [];
                    
        valuationChartLabelRotation = data.bases.length < 6 ? 0 : -45;

        initialiseValuationChart();

        for (var i = 0; i < data.bases.length; i++) {

            var basis = data.bases[i];

            var balance = basis.balance;
            var isCurrentBasis = basis.active == true;

            var color = isCurrentBasis ? '#6a7f10' : fusion.util.colorGradient('#6a7f10', '#ffffff', 0.5);
            var negativeColor = isCurrentBasis ? '#9e3039' : fusion.util.colorGradient('#9e3039', '#ffffff', 0.5);

            bases.push({ id: basis.id, name: basis.name, anchorDate: basis.anchorDate });
            balances.push({
                y: balance,
                color: balance < 0 ? negativeColor : color,
            });
        }

        var current = data.current;

        if (this.balanceChart.series[0].data.length == 0) {
            this.balanceChart.xAxis[0].categories = bases;
            this.balanceChart.series[0].remove(false);
            this.balanceChart.addSeries({
                name: 'Bases',
                data: balances,
            });
        } else {
            this.balanceChart.series[0].setData(balances);
        }

        if (this.valuationChart.series[0].data.length == 0) {   
            this.valuationChart.series[0].setData([{
                    y: current.assetTotal,
                    color: '#E87424'
                }, {
                    y: current.liabilityTotal,
                    color: '#8F258D'
                }]);
        } else {
            this.valuationChart.series[0].data[0].update(current.assetTotal, false);
            this.valuationChart.series[0].data[1].update(current.liabilityTotal, false);
        }
                    
        this.balanceChart.redraw();
        this.valuationChart.redraw();

        $('#startingDateBalanceDate').text((current.balance < 0 ? 'Deficit at ' : 'Surplus at ') + Highcharts.dateFormat('%e %B %Y', current.anchorDate));
        $('#startingDateBalance').html(fusion.util.formatCurrencyAbsolute(current.balance));
        $('#startingDateBalanceDiv')
            .removeClass(current.balance < 0 ? 'darkolivegreen' : 'rougered')
            .addClass(current.balance < 0 ? 'rougered' : 'darkolivegreen');

        // workaround for highcharts rendering behaviour
        // useHTML for the x-axis label is causing display issues, so can't put the labels in a div and catch the click on that div
        // so having to turn useHTML off and deal with the elements created by highcharts
        // which means dealing with click event and styling ourselves
        $('.highcharts-axis-labels text', $("#valuationResultBalanceChartDiv")).each(function () {
            var label = $(this);
            var basisDisplayName = "";
            var tspans = $("tspan", label);
            for (var i = 1; i < tspans.length; i++) { // deliberately missing last tspans element as that should be the anchor date
                basisDisplayName += $("tspan:nth-child(" + i + ")", label).text() + " ";
            };
            basisDisplayName = basisDisplayName.trim();
            if (basisDisplayName === $("#currentBasis").text()) {
                label.css("fill", "#9e3039");
                label.off("mouseenter mouseleave");
            }
            else {
                label.hover(function () { $(this).css("fill", "#cf989c").css("cursor", "pointer"); }, function () { $(this).css("fill", "#666"); });
            }
            label.off("click");
            label.on("click", function (e) {
                for (var i = 0; i < bases.length; i++) {
                    if (bases[i].name === basisDisplayName) {
                        if (bases[i].name !== $('#currentBasis').text()) {
                            fusion.selectBasis(bases[i].id);
                        }
                        return;
                    }
                }
            });
        });

        $('#namedpropertygroupname').html(current.namedPropertyGroup.Name);
        $('#namedpropertygrouptable').empty();
        for (i = 0 ; i < current.namedPropertyGroup.Properties.length; i++) {
            $('#namedpropertygrouptable').append('<tr><th>' + current.namedPropertyGroup.Properties[i].Key + '</th><td class="values">' + current.namedPropertyGroup.Properties[i].Value + '</td><tr/>');
        }
        //TODO wtf is causing these empty rows?!
        $('#namedpropertygrouptable tr').each(function (idx, el) {
            if ($(el).find("td").length == 0) {
                $(el).remove();
            }
        });
    };
};


$.Core.valuationResults = new ValuationResultsClass();

$(function() {

    $.Core.valuationResults.valuationChart = new Highcharts.Chart({
        chart: {
            type: 'column',
            renderTo: 'valuationResultValuationChartDiv',
            backgroundColor: 'none'
        },
        credits: false,
        exporting: {
            enabled: false
        },
        title: {
            text: null
        },
        xAxis: {
            categories: ['Assets', 'Liabilities']
        },
        yAxis: [{
            title: {
                text: null
            },
            labels: {
                useHTML: true,
                formatter: fusion.util.formatChartCurrencyLabels,
            },
        }, {
            title: {
                text: null
            },
            labels : {
                useHTML: true,
                formatter: fusion.util.formatChartCurrencyLabels,
            },
            linkedTo: 0,
            opposite: true,
        }],
        plotOptions: {
            column: {
                pointPadding: 0.05,
                borderWidth: 0
            }
        },
        legend: { enabled: false },
        tooltip: {
            borderWidth: 0,
            borderRadius: 0,
            backgroundColor: '#007c92',
            shadow: false,
            useHTML: true,
            formatter: function () {
                return this.x + ': ' + fusion.util.formatCurrencyAbsolute(this.y);
            },
            style: {
                color: '#ffffff',
                fontSize: '1.1em',
                padding: '20px',
                opacity: 0.8,
            }
        },
        series: [{
            name: 'Balance',
            pointWidth: 45,
            data: [
                { y: 0, color: '#E87424' },
                { y: 0, color: '#8F258D' }
            ]
        }]
    });

});