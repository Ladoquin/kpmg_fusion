/*
*	KPMG
*	lightbox.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This controls lightbox elements. */

fusion.lightbox ={
	init: function(){
	    this.bindLightbox();
	},
	bindLightbox: function(){
		$('.fancybox').fancybox({
			'minWidth' :250,
			//'maxWidth' :570,
			'afterShow' : function(){
				//console.log("on after show");
			},
			'afterClose'		: function() {
			    //console.log("on close");
			}
		});
	}
};


$(function () {
    fusion.lightbox.init();
});


