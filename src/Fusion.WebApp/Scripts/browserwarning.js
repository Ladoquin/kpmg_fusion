﻿$(function () {
    //alert($.browser.msie + " " + $.browser.version);
    //if ($.browser.msie && $.browser.version < 9)

    // temporarilly remove browser version block
    // does not allow chrome on ipad...

    if ( $(".loginsection").length>0)
    {
        var browserok = false;
        if ($.browser.ipad && $.browser.webkit && !$.browser.safari)
        {
            //ipad chrome not detected by name so work out by webkit and not safari
            browserok = true;
        }
        else if ($.browser.win && $.browser.msie && $.browser.versionNumber >= 9)
        {
            browserok = true;
            browserok = (document.documentMode && document.documentMode > 8);
        }
        else if ($.browser.chrome && ($.browser.win || $.browser.mac || $.browser.linux)) {
            browserok = true;
        }
        else if ($.browser.mozilla && ($.browser.win || $.browser.mac || $.browser.linux)) {
            browserok = true;
        }

        if (!browserok)
        {
            var browserwarningmsg = "This site is optimised to work with Chrome, Mobile Chrome on iPad, Firefox and IE9 and above only. If you are using IE, please ensure it is not running in compatibility mode.";
            $(".contents").prepend("<div class='browser-warning'><p>" + browserwarningmsg + "<p></div>");

            //$("input#UserName").attr("disabled", "disabled").attr("title", browserwarningmsg);
            //$("input#Password").attr("disabled", "disabled").attr("title", browserwarningmsg);
            //$(".submitters input").hide();
            //$(".ForgottenPasswordContainer").hide();
            //$(".loginsection label").css("color", "#cccccc");

        }
    }

    
});

