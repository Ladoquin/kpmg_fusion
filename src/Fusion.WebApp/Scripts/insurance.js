﻿fusion.analysis.Insurance = function () {

    var that = this;
    var initialised = false;

    this.changed = $.Callbacks();
    this.analysisDate = null;

    var initialise = function (setup) {

        if (!initialised) {

            $('#resetInsurance').click(function () {
                that.reset();
            });
        }

        var updateCell = function (event, ui) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: ui.value
            });
        };

        var updateInsurance = function (event, ui) {
            var finalPen,
                finalNonPen;

            finalPen = $("#insurance-pensioner-liability-insured").parent().parent().find('.slider-text-value').data('finalValue');
            if (finalPen == null || finalPen == typeof 'undefined')
                finalPen = $("#insurance-pensioner-liability-insured").slider("option", "value");

            finalNonPen = $("#insurance-nonpensioner-liability-insured").parent().parent().find('.slider-text-value').data('finalValue');
            if (finalNonPen == null || finalNonPen == typeof 'undefined')
                finalNonPen = $("#insurance-nonpensioner-liability-insured").slider("option", "value");

            var parameters = {

                pen: finalPen,
                nonpen: finalNonPen
            };
            var json = {
                insuranceData: JSON.stringify(parameters)
            };
            $.post('/Json/SetInsurance', json, function (data) {
                that.refresh(data);
                that.changed.fire({ id: that.id });
            });
        };
        
        $("#insurance-pensioner-liability-insured").slider({
            disabled: !setup.enabled,
            animate: 400,
            range: 'min',
            max: 1,
            min: 0,
            step: 0.01,
            slide: updateCell,
            stop: updateInsurance,
            create: function () { $("#insurance-pensioner-liability-insured").slider('setTextValueElement') },
            elementSelector: $("#insurance-pensioner-liability-insured"),
            textElement: $("#insurance-pensioner-liability-insured").parent().siblings().last()
        });
        $("#insurance-pensioner-liability-insured").data("original", setup.pensionerLiabilityPercInsured);

        $("#insurance-nonpensioner-liability-insured").slider({
            disabled: !setup.enabled,
            animate: 400,
            range: 'min',
            max: 1,
            min: 0,
            step: 0.01,
            slide: updateCell,
            stop: updateInsurance,
            create: function () { $("#insurance-nonpensioner-liability-insured").slider('setTextValueElement') },
            elementSelector: $("#insurance-nonpensioner-liability-insured"),
            textElement: $("#insurance-nonpensioner-liability-insured").parent().siblings().last()
        });
        $("#insurance-nonpensioner-liability-insured").data("original", setup.nonpensionerLiabilityPercInsured);

        initialised = true;
    };

    var setMode = function (readonly) {
        $("#insurance-pensioner-liability-insured, #insurance-nonpensioner-liability-insured")
            .each(function () {
                $(this).closest("td").removeClass("greySlider").addClass(readonly ? "greySlider" : "");
                $(this).slider({ disabled: readonly });

                if(readonly == true)
                    $(this).slider('disableEditableElement', this);
            });
        $("#resetInsurance").toggle(!readonly);
    };

    var setWarning = function (assetVal) {
        $("#insurance-available-assets").css('color', (assetVal < 0) ? 'red' : 'inherit');
    }
    this.refresh = function (data) {

        if (data == null)
            return;

        if (!initialised) 
            initialise(data.setup);

        that.analysisDate = data.setup.analysisDate;

        $("#td-uninsured-pensioner-liability").html(fusion.util.formatCurrency(data.uninsuredPensionerLiability));
        $("#td-uninsured-nonpensioner-liability").html(fusion.util.formatCurrency(data.uninsuredNonpensionerLiability));

        $("#insurance-pensioner-liability-insured").slider("value", data.pensionerLiabilityPercInsured);
        $("#insurance-pensioner-liability-insured").slider("setTextValueElement", data.pensionerLiabilityPercInsured);

        $("#insurance-pensioner-liability-insured-amount").html(fusion.util.formatCurrency(data.pensionerLiabilityInsured));

        $("#insurance-nonpensioner-liability-insured").slider("value", data.nonpensionerLiabilityPercInsured);
        $("#insurance-nonpensioner-liability-insured").slider("setTextValueElement", data.nonpensionerLiabilityPercInsured);

        $("#insurance-nonpensioner-liability-insured-amount").html(fusion.util.formatCurrency(data.nonpensionerLiabilityInsured));
        $("#insurance-available-assets").html(fusion.util.formatCurrency(data.assetVal));
        $("#insurance-buyin-cost").html(fusion.util.formatCurrency(data.buyinCost));
        $("#insurance-impact-of-50-basis").html(fusion.util.formatCurrency(data.fiftyBasisPointImpact));

        updateInsOutputTable(data.strains.Data);

        $('.insuranceOutputBasisName').html(data.setup.buyoutBasisName); //needs to alway sbe buyout basis name

        //TODO without backend refactor, fifty basis point value can only be calculated when on the buyout basis
        if (data.setup.enabled) {
            $(".buyoutBasisContent").show();
            $(".nonBuyoutBasisContent").hide();
            $("#insurance-impact-of-50-basis").html(fusion.util.formatCurrency(data.fiftyBasisPointImpact));
        } else {
            $(".buyoutBasisContent").hide();
            $(".nonBuyoutBasisContent").show();
            $("#insuranceOutputTable tbody td.strainResult").each(function (ind, val) {
                $(val).html($(val).html().replace(fusion.util.formatCurrency(0), '-'));
                $(val).html($(val).html().replace('n/a', '-'));
            });
        }
        
        setWarning(data.assetVal);
        setMode(!data.setup.enabled);
    };

    this.reset = function () {
        $.post("/Json/ResetInsurance", function (data) {
            that.refresh(data);
            that.changed.fire({ id: that.id });
        });        
    };

    this.analysisDateChanged = function () {
        refreshStrains();
    };
    
    var refreshStrains = function () {
        $.get("/Json/Strains", function (strains) {
            updateInsOutputTable(strains);
        });
    }

    var updateInsOutputTable = function (strains) {
        var tbody = $("#insuranceOutputTable tbody");
        tbody.empty();
        if (strains != null) {
            for (var i = 0; i < strains.length; i++) {
                var td = strains[i],
                    current = fusion.util.formatCurrency(td.curr),
                    max = fusion.util.formatCurrency(td.max),
                    min = fusion.util.formatCurrency(td.min),
                    notApplicable = 'n/a';

                if (that.analysisDate < fusion.util.convertDateToMilliseconds(td.startDate))
                {
                    if (td.curr == 0)
                        current = notApplicable;               
                    if(td.max == 0)
                        max = notApplicable;                
                    if(td.min == 0)
                        min = notApplicable;              
                }

                tbody.append($("<tr><td>vs. " + td.basis + "</td><td>" + td.startDate + "</td><td class='strainResult' style='width:70px'>" + current + "</td><td class='strainResult'>" + max + "</td><td class='strainResult'>" + min + "</td></tr>"));
            }
        }     
    }
}

