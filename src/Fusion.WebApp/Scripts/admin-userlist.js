﻿$(function () {

    var init = function () {
        registerEvents();
    };

    var refresh = function (url) {
        showOverlay();
        $.get(url, function (html) {
            $("#user-list-view").html(html);
            hideOverlay();
            registerEvents();
        });
    };

    var registerEvents = function () {
        registerSearchEvents();
        registerActionEvents();
    };

    var registerSearchEvents = function () {
        $(".paging a").click(function (e) {
            e.preventDefault();
            refresh($(this).attr("href"));
        });
        $("#user-list-name-sort").click(function (e) {
            e.preventDefault();
            refresh($(this).attr("href"));
        });
        $("select[name=role-filter]").change(function () {
            refresh($("select[name=role-filter] option:selected").val());
        });
        $("#scheme-filter").schemeSearcher(function (val) {
            var dataUrl = $("#scheme-filter").data("url");
            var schemeName = "";
            dataUrl.split("&").forEach(function (item) {
                if (item.split("=")[0] == "SchemeName") {
                    schemeName = item.split("=")[1];
                }
            });
            var url = dataUrl.replace("SchemeName=" + schemeName, "SchemeName=" + val);
            refresh(url);
        });
        $("#filter-reset").click(function (e) {
            e.preventDefault();
            refresh($(this).attr("href"));
        });
    };

    var registerActionEvents = function () {
        
        var setActionPanel = function () {
            $("#user-bulk-actions").toggle(
                $("input:checked", $("#tbl-user-list")).length > 0);
        };

        $("#chk-user-selectall").click(function () {
            var on = this.checked;
            $(".user-select")
                .addClass(on ? "" : "hidden")
                .removeClass(on ? "hidden" : "")
                .find("input")
                    .prop("checked", on);
            setActionPanel();
        });

        $(".user-select input").click(function () {
            var on = this.checked;
            $(this).closest("td")
                .addClass(on ? "" : "hidden")
                .removeClass(on ? "hidden" : "");
            setActionPanel();
        });

        $(".scheme-list-toggle").click(function (e) {
            var expanding = $(this).hasClass("scheme-list-toggle-collapsed");
            $(this)
                .removeClass(expanding ? "scheme-list-toggle-collapsed" : "scheme-list-toggle-expanded")
                .addClass(expanding ? "scheme-list-toggle-expanded" : "scheme-list-toggle-collapsed");
            if (expanding)
                $(this).next("div").show(100);
            else
                $(this).next("div").hide(100);
            e.preventDefault();
        });

        $("#user-list-filters-toggle").click(function () {
            var filtersVisible = !$("#user-list-filters").is(":visible");
            $("#user-list-filters").toggle(filtersVisible);
            $(this).addClass(filtersVisible ? "selected" : "").removeClass(filtersVisible ? "" : "selected");
        });

        $(".remove-scheme-user").click(function (e) {
            $(this).next("span").show();
            e.preventDefault();
        });

        $(".remove-scheme-cancel").click(function (e) {
            $(this).closest("span").hide();
            e.preventDefault();
        });

        $(".remove-scheme-ok").click(function (e) {
            var list = $(this).closest(".scheme-list");
            $(this).closest("div").remove();
            if (list.children().length == 0) {
                var td = $(list).closest("td");
                td.find("a").remove();
                td.text(td.text().replace("+", "").trim())
            };
            $.post("/Json/DeleteSchemeAccess", { userId: $(this).data("user"), schemeId: $(this).data("scheme") });
            e.preventDefault();
        });

        $(".user-unlock").click(function (e) {
            $(this).next("span.user-unlock-confirmation").show();
            e.preventDefault();
        });

        $(".user-unlock-cancel").click(function (e) {
            $(this).closest("td").find("span.user-unlock-confirmation").hide();
            e.preventDefault();
        });

        $(".user-unlock-ok").click(function (e) {
            var href = $(this).attr("href");
            var username = href.split("?").pop().split("=").pop();
            var url = href.substring(0, href.split("?")[0].length);
            $.post(url, { username: username });
            $(this).closest("td").empty().append("<span><img src='../Images/tick.png' width='16' height='16' style='padding: 0; margin: 0;'/> Lock removed</span>").hide(600);
            e.preventDefault();
        });

        $("#delete_user").click(function (e) {
            $("#dialog-user-delete-confirm").dialog("open");
            e.preventDefault();
        });

        // Want to add extra margin to the right of the scheme column so when the delete scheme confirmation spans is toggled the unlock column doesn't move
        $("#tbl-user-list th:nth-child(6)").css("min-width", (parseInt($("#tbl-user-list th:nth-child(6)").width()) + 110) + "px");

        // set delete confirmation
        $("#dialog-user-delete-confirm").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            dialogClass: 'white-dialog',
            buttons: [{
                text: "Yes",
                click: function () {
                    $(this).dialog("close");
                    var array = [];
                    $.each($("input[name=user-select-chk]:checked"), function (idx, val) { array.push($(val).val()); });
                    $.post('/Users/Delete', $.param({ ids: array, __RequestVerificationToken: $("input[name=__RequestVerificationToken]").val() }, true), function () {
                        refresh($("#delete_user").attr("href"));
                    });                    
                }
            },
            {
                text: "Cancel",
                click: function () {
                    $(this).dialog("close");
                },
                'class': "btn-dialog-cancel"
            }],
            create: function (event, ui) {
                var widget = $(this).dialog("widget");
                $(".ui-dialog-titlebar-close .ui-button-text", widget).text("");
            },
            open: function () {
                $(this).parent().find('button:nth-child(2)').focus();
            }
        });
    };

    var showOverlay = function () {
        var docHeight = $(document).height();
        $("body").append("<div id='overlay'></div>");
        $("#overlay")
          .height(docHeight)
          .css({
              'opacity': 0.3,
              'position': 'absolute',
              'top': 0,
              'left': 0,
              'background-color': 'black',
              'width': '100%',
              'z-index': 5000
          });
    };

    var hideOverlay = function () {
        $("#overlay").remove();
    };

    $(document).ready(function () {
        init();
    });

}());