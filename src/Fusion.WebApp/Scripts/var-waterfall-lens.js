﻿
var VarWaterfallLens = function() {

    this.chart = null;

    this.isStale = true;
    this.isActiveLens = false;

    this.activateLens = function () {

        this.isActiveLens = true;
        if (this.isStale) {
            this.getChartData(this);
        }

    };

    this.setChartData = function (thisLens) {

        var json = {
            ci: $("#varChanceSelect .selected").data('value'),
            time: $("#varPeriodSelect .selected").data('value')
        };

        $.getJSON('/Json/SetVarWaterfallData', json, function (data) {
            thisLens.refreshChart(data);
            thisLens.isStale = false;
        });
    };

    this.getChartData = function (thisLens) {

        $.getJSON('/Json/GetVarWaterfallData', function (data) {
            thisLens.refreshChart(data);
            thisLens.isStale = false;
        });
    };

    this.deactivateLens = function () {
        this.isActiveLens = false;
    };

    this.updateLens = function () {

        if (this.isActiveLens) {

            this.getChartData(this)

        } else {
            this.isStale = true;
        }
    };

    this.initialiseLens = function(chartDiv) {

        var that = this;

        this.$varPeriodSelect =  $("#varPeriodSelect").styleableSelect({
            onChanged: function () {
                that.setChartData(that);
            }
        });
        this.$varChanceSelect = $("#varChanceSelect").styleableSelect({
            onChanged: function () {
                that.setChartData(that);
            }
        });

        this.chart = new Highcharts.Chart({
            chart: {
                type: 'waterfall',
                renderTo: chartDiv,
                backgroundColor: 'none',
                plotBorderWidth: 1,
                plotBackgroundColor: 'white'
            },
            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: null,
                y: 25
            },
            xAxis: {
                type: 'category',
                title: {
                    useHTML: true,
                    text: 'Classes',
                    style: {
                        fontFamily: 'Lato, sans-serif',
                        fontSize: '14px',
                        color: '#f0f0f0'
                    }
                }
            },
            yAxis: [{
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels
                },
                maxPadding: 0.1,
                minPadding: 0.1
            }, {
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels
                },
                maxPadding: 0.1,
                minPadding: 0.1,
                linkedTo: 0,
                opposite: true
            }],
            plotOptions: {
                waterfall: {

                    dataLabels: {
                        useHTML: true,
                        enabled: true,
                        verticalAlign: 'top',
                        y: -20,
                        formatter: function () {
                            return fusion.util.formatCurrencyAbsolute(this.point.y);
                        }
                    },
                    pointPadding: 0.1,
                    groupPadding: 0.1,
                    borderWidth: 0
                }
            },
            tooltip: {
                hideDelay: 0,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                followPointer: true,
                formatter: function () {
                    var tt = this.point.name + "<br/><br/>Before: ";
                    if (this.point.before != undefined) {
                        tt += fusion.util.formatCurrencyAbsolute(this.point.before) + "<br/>After: ";
                    };
                    tt += fusion.util.formatCurrencyAbsolute(this.y);
                    return tt;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8
                }
            },
            legend: {
                enabled: false
            },
            labels:{
                CSSObject: {"whiteSpace": "nowrap"}
            },
            series: [{
                data: [{
                    name: 'Interest rate & inflation',
                    y: 0
                }, {
                    name: 'Interest rate <br> & inflation hedging',
                    y: 0
                }, {
                    name: 'Longevity',
                    y: 0
                }, {
                    name: 'Hedging of longevity',
                    y: 0
                }, {
                    name: 'Credit',
                    y: 0
                }, {
                    name: 'Equity',
                    y: 0
                }, {
                    name: 'Other Growth',
                    y: 0
                }, {
                    name: 'Diversification',
                    y: 0
                }, {
                    name: 'Total Risk',
                    isSum: true
                }]
            }]
        });
    };



    this.refreshChart = function (data) {

        var varWaterfallData = data.after;
        var original = data.before != undefined ? data.before : {}

        var positiveColor = '#9e3039';
        var negativeColor = '#6a7f10';

        this.chart.series[0].data[0].update({
            y: varWaterfallData.Interest,
            color: varWaterfallData.Interest < 0 ? negativeColor : positiveColor,
            before: original.Interest
        }, false);

        this.chart.series[0].data[1].update({
            y: varWaterfallData.InterestInflationHedging,
            color: varWaterfallData.InterestInflationHedging < 0 ? negativeColor : positiveColor,
            before: original.InterestInflationHedging
        }, false);

        this.chart.series[0].data[2].update({
            y: varWaterfallData.Longevity,
            color: varWaterfallData.Longevity < 0 ? negativeColor : positiveColor,
            before: original.Longevity
        }, false);

        this.chart.series[0].data[3].update({
            y: varWaterfallData.Hedge,
            color: varWaterfallData.Hedge < 0 ? negativeColor : positiveColor,
            before: original.Hedge
        }, false);

        this.chart.series[0].data[4].update({
            y: varWaterfallData.Credit,
            color: varWaterfallData.Credit < 0 ? negativeColor : positiveColor,
            before: original.Credit
        }, false);

        this.chart.series[0].data[5].update({
            y: varWaterfallData.Equity,
            color: varWaterfallData.Equity < 0 ? negativeColor : positiveColor,
            before: original.Equity
        }, false);

        this.chart.series[0].data[6].update({
            y: varWaterfallData.OtherGrowth,
            color: varWaterfallData.OtherGrowth < 0 ? negativeColor : positiveColor,
            before: original.OtherGrowth
        }, false);

        this.chart.series[0].data[7].update({
            y: varWaterfallData.Diversification,
            color: varWaterfallData.Diversification < 0 ? negativeColor : positiveColor,
            before: original.Diversification
        }, false);


        this.chart.series[0].data[8].update({
            y: varWaterfallData.TotalRisk,
            color: varWaterfallData.TotalRisk < 0 ? negativeColor : positiveColor,
            before: original.TotalRisk
        }, false);

        var displayOptions = data.displayOptions;

        this.$varPeriodSelect.select(displayOptions.time, false);
        this.$varChanceSelect.select(displayOptions.ci, false);

        var that = this;
        setTimeout(function () {
            that.chart.redraw();
        }, 0);
       // this.chart.redraw();
    };
};

