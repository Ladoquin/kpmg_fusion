﻿$(function () {

    var panelState = new fusion.analysis.panelState();
    var originalValues = new fusion.analysis.originalValues();
    var scenarios = new fusion.analysis.scenarios();
    var prefetchedAssumptions;
    var initialTabActivated = false;
    var lenses = {};
    var panels = {};

    // *************
    // * helpers
    // *************
    var activeTabIdx = function (base) {
        return (base || 0) + parseInt($("#analysisdashboard").tabs("option", "active"));
    };
    var setBalance = function (data) {
        $('#analysisPeriodBalanceLabel').html(data.deficit < 0 ? 'Deficit at end date' : 'Surplus at end date');
        $('#analysisPeriodBalance').html(fusion.util.formatCurrencyAbsolute(data.deficit));
    };

    var isiPad = navigator.userAgent.match(/iPad/i) != null;

    var setAnalysisPeriod = function (data) {
        $('.analysisPeriodEnd', $("#analysis_options")).each(function () {
            $(this).data("start-date", Highcharts.dateFormat('%d/%m/%Y', data.start));
            $(this).datepicker({
                minDate: $(this).data("range-min-date"),
                maxDate: $(this).data("max-date"),
                inline: true,
                dateFormat: 'dd/mm/yy',
                showOn: isiPad ? "button" : "both",
                buttonImage: 'Images/datepicker-button.png',
                buttonText: null,
                onClose: function (dateText, inst) {
                    if (isiPad)
                        $(this).attr("disabled", false);
                },
                beforeShow: function (dateText, inst) {
                    if (isiPad)
                        $(this).attr("disabled", true);
                }
            }).addClass("pointer");
            $(this).datepicker("option", "minDate", Highcharts.dateFormat('%d/%m/%Y', data.anchor));
            $(this).val(Highcharts.dateFormat('%d/%m/%Y', data.date));
        });
    };
    var setAccountingState = function (enabled) {
        if (enabled == true) {
            $('#accountingLensLink').attr('title', '');
            $('#accountingLensLink').removeClass('degrayed');
            $('#opts').tabs('enable', 3);
        } else {
            if ($("#opts").tabs('option', 'active') == 3) {
                setTimeout(function () {
                    $("#opts").tabs({
                        active: 1
                    });
                }, 200);
            }
            $('#opts').tabs('disable', 3);
            $('#accountingLensLink').attr('title', $('#accountingLensLink').data('disabled-title'));
            $('#accountingLensLink').addClass('degrayed');
        }
    };
    var setPage = function (data) {
        if (data.funded)
            return;
        $.each([1, 2, 3, 4, 5, 6, 7, 8, 9], function (idx, val) {
            $("#analysisdashboard").tabs("disable", val);
        });
        $("#analysisdashboard").data("preventOverride", true);
    }
    var setPanelState = function (idx, li, enabled) {
        if ($("#analysisdashboard").tabs("option", "active") == idx) {
            $("#analysisdashboard").tabs({ active: 0 });
        };
        $("#analysisdashboard").tabs(enabled ? "enable" : "disable", idx);
        $(li).attr('title', enabled ? '' : $(li).data('disabled-title'));
    };

    // *************
    // * initialisers
    // *************
    var initPanelStateManagerEvents = function () {
        var panelid = function () {
            return $("#analysisdashboard").find(".panelTab:visible")[0].id;
        };
        $("#analysisdashboard").on("click", ".reset", function () {
            panelState.set(panelid(), false);
        });
        $("#analysisdashboard").on("slidestop", ".assumptionsSlider", function (event, ui) {
            panelState.set(panelid(), true);
        });
        $("#analysisdashboard").on("change", "input[type=radio], input[type=checkbox]", function () {
            panelState.set(panelid(), true);
        });
    };
    var initOriginalValuesEvents = function () {
        $("#analysisdashboard").on("tabsactivate", function (event, ui) {
            originalValues.panelActivated(ui.newPanel[0].id);
        });
    };
    var initLenses = function () {
        lenses = {
            time: new fusion.analysis.timeLens(),
            cash: new fusion.analysis.cashLens(),
            risk: new fusion.analysis.riskLens(),
            accounting: new fusion.analysis.accountingLens()
        };
        setAccountingState($("input[name=EnableAccountingLens]").val() == "true");
        $('.tempHide').css('opacity', 1);
    };
    var initPanels = function () {
        panels = {
            liabilities: new fusion.analysis.LiabilityPanel(),
            assets: new fusion.analysis.AssetPanel(),
            recoveryPlan: new fusion.analysis.RecoveryPlanPanel(),
            investmentStrategy: new fusion.analysis.InvestmentStrategyPanel(),
            fro: new fusion.analysis.FroPanel(),
            etv: new fusion.analysis.EtvPanel(),
            pie: new fusion.analysis.PiePanel(),
            futureBenefits: new fusion.analysis.FutureBenefitsPanel(),
            abf: new fusion.analysis.AbfPanel(),
            insurance: new fusion.analysis.Insurance()
        };

        panels.liabilities.id = "liabilities";
        panels.assets.id = "assets";
        panels.recoveryPlan.id = "recoveryPlan";
        panels.investmentStrategy.id = "investmentStrategy";
        panels.fro.id = "fro";
        panels.etv.id = "etv";
        panels.pie.id = "pie";
        panels.futureBenefits.id = "futureBenefits";
        panels.abf.id = "abf";
        panels.insurance.id = "insurance";

        panels.liabilities.dependents = ["assets", "recoveryPlan", "investmentStrategy", "futureBenefits", "fro", "etv", "pie", "abf", "insurance"];
        panels.assets.dependents = ["recoveryPlan", "investmentStrategy"];
        panels.recoveryPlan.dependents = [];
        panels.investmentStrategy.dependents = ["assets", "recoveryPlan"];
        panels.fro.dependents = ["liabilities", "assets", "recoveryPlan", "investmentStrategy", "abf", "insurance"];
        panels.etv.dependents = ["liabilities", "assets", "recoveryPlan", "investmentStrategy", "abf", "insurance"];
        panels.pie.dependents = ["liabilities", "recoveryPlan", "investmentStrategy", "abf", "insurance", "futureBenefits"];
        panels.futureBenefits.dependents = ["liabilities"];
        panels.abf.dependents = ["assets", "investmentStrategy", "insurance"];
        panels.insurance.dependents = ["assets", "investmentStrategy"];

        panels.liabilities.changed.add(handlePlanUpdate);
        panels.assets.changed.add(handlePlanUpdate);
        panels.recoveryPlan.changed.add(handlePlanUpdate);
        panels.investmentStrategy.changed.add(handlePlanUpdate);
        panels.fro.changed.add(handlePlanUpdate);
        panels.etv.changed.add(handlePlanUpdate);
        panels.futureBenefits.changed.add(handlePlanUpdate);
        panels.abf.changed.add(handlePlanUpdate);
        panels.pie.changed.add(handlePlanUpdate);
        panels.insurance.changed.add(handlePlanUpdate);

        setPanelState(8, $("#li-analysisdashboard_abf"), $("input[name=EnableABFPanel]").val() == "true");
        setPanelState(9, $("#li-analysisdashboard_insurance"), $("input[name=EnableInsurancePanel]").val() == "true");
    };
    var initAnalysisPeriod = function () {
        $('input.analysisPeriodEnd').change(function () {
            var endTimeDatePicker = fusion.util.getDateFromDatepicker($('input.analysisPeriodEnd').val());

            var startDate = $('input.analysisPeriodEnd').data("start-date");
            var startTimeDatePicker = new Date(Date.UTC(
                parseInt(startDate.split("/")[2]),
                parseInt(startDate.split("/")[1]) - 1,
                parseInt(startDate.split("/")[0]), 0, 0, 0, 0)).getTime();

            $.post('/Json/SetAnalysisPeriod', { start: startTimeDatePicker, end: endTimeDatePicker }, function (data) {
                data.trackerItemDate = endTimeDatePicker;
                analysisPeriodChanged(data);
            });
        });
    };
    var initSavedAssumptions = function () {
        panelState.panelStateChangedEvent.add(scenarios.toggleVisibility);
        scenarios.init();
    };

    // *************
    // * event handlers
    // *************
    var lensActivated = function (el, ui) {
        var tab = ui.newPanel.selector.replace("#", "");
        for (var lens in lenses) {
            if (lens === tab) {
                lenses[lens].activate();
            }
            else {
                lenses[lens].deactivate();
            }
        };
    };

    var handlePlanUpdate = function (args, callback) {

        var key = args.id;

        updateLenses(key);

        var dependents = key === "all" ? Object.keys(panels) : panels[key].dependents;
        var components = JSON.stringify(dependents);

        $.getJSON("/Json/AnalysisPlan", { components: components }, function (data) {

            setBalance(data);
            setAnalysisPeriod(data);
            setPage(data);

            dependents.forEach(function (i) {
                panels[i].refresh(data[i]);
            });

            if (callback != undefined)
                callback(data);
            preventSliderDrag();
        });
    };

    var updateLenses = function (key) {

        lenses.time.update();
        lenses.risk.update();

        if ($.inArray(key, ["all", "liabilities", "assets", "fro", "etv", "pie", "abf", "investmentStrategy"]) > -1) {
            lenses.cash.update();
            lenses.accounting.update();
        }
        else if ($.inArray(key, ["recoveryPlan", "insurance", "futureBenefits"]) > -1) {
            lenses.cash.update();
        };
    };

    var resetBasis = function () {
        $('#reset-scheme').css('visibility', 'hidden');
        $.post('/Json/ResetAllAssumptions', function () {
            originalValues.reset();
            refresh();
        });
    };

    var toggleResetButton = function (visible) {
        $('#reset-scheme').css('visibility', visible ? 'visible' : 'hidden');
    };

    var refreshResetButton = function () {
        $('#reset-scheme').click(resetBasis);
    };

    var refresh = function (callbacks) {

        callbacks = callbacks || {};

        panels.liabilities.reInit();
        panels.assets.reInit();
        panelState.refresh();

        handlePlanUpdate({ id: "all" }, function (data) {
            originalValues.panelActivated($("#analysisdashboard .panelTab:nth-child(" + activeTabIdx(1) + ")").attr("id")); // 'tabsactivate' for initially visible tab
            if (callbacks.onPlanUpdated)
                callbacks.onPlanUpdated();
        });

        if (!initialTabActivated) {
            switch ($("#opts").tabs("option", "active")) {
                case 0: lenses.time.activate(); break;
                case 1: lenses.cash.activate(); break;
                case 2: lenses.risk.activate(); break;
                case 3: lenses.accounting.activate(); break;
            };
            initialTabActivated = true;
        };

        if (callbacks.onCompleted)
            callbacks.onCompleted();
    };

    var basisChanged = function (basis, custom) {
        // expecting data to be result from analysis select basis overload, which contains extra analysis page specific info
        $("#currentBasis").data("masterBasisId", basis.masterBasisId);
        originalValues.reset();
        setAccountingState(basis.type == "Accounting");
        refresh({
            // don't mess with panels before plan is updated else original values can't work properly
            onPlanUpdated: function () {
                setPanelState(8, $("#li-analysisdashboard_abf"), basis.type != "Accounting");
            }
        });
    };

    var analysisPeriodChanged = function (data) {
        lenses.accounting.analysisDateChanged(data);
        originalValues.reset();
        refresh();
        panels.insurance.analysisDateChanged(data);
    };

    var preventSliderDrag = function () {
        $('.ui-slider-disabled a').attr('unselectable', 'on').attr('draggable', 'false');
    };

    var disableScenarioDownload = function () {
        $("#scenario-download-btn").css("opacity", "0.4");
        $("#scenario-download-btn").on("click", function (e) {
            e.preventDefault();
            return false;
        });
    };

    var init = function () {

        fusion.page.init({ idx: 3, name: "analysis" });
        fusion.tabs.init();
        fusion.lightbox.init();

        $("#opts").tabs({
            activate: lensActivated
        });

        // Display issues workaround
        $("#analysisdashboard").tabs({
            activate: function (el, ui) {
                if (ui.newPanel.selector === "#analysisdashboard_liabilities") {
                    panels.liabilities.tabActivated();
                }
            }
        });

        initPanelStateManagerEvents();
        initOriginalValuesEvents();
        initLenses();
        initPanels();
        initAnalysisPeriod();
        initSavedAssumptions();

        refreshResetButton();
        panelState.panelStateChangedEvent.add(toggleResetButton);

        //disable downloads on ipad
        if (isiPad == true) {
            lenses.accounting.disableDownload();
            disableScenarioDownload();
        }

        refresh();
    };

    // *************
    // * events
    // *************

    fusion.basisChangedEvent.add(basisChanged);

    $(document).ready(function () {
        init();
    });

}());
