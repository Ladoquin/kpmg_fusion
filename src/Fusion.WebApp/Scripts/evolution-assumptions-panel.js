﻿var EvolutionAssumptionsPanel = function () {

    var that = this;
    var liabilitytabler = new fusion.util.LiabilitiesTabler("liability-assumptions-table");
    var pensionincreasetabler = new fusion.util.LiabilitiesTabler("pension-increase-assumptions-table");
    var donutHandler = new fusion.util.AssumptionsPairHandler({
        liabilityChartDivId: "liabilityBreakdownPie",
        assetChartDivId: "assetBreakdownPie",
    });

    var populateInitialAssumptions = function (tableName, assumptions) {
        $("#" + tableName + " [id^=td-init]").html("");
        $.each(assumptions, function (idx, assumption) {            
            if (typeof assumption.IsVisible == "undefined" || assumption.IsVisible == true) {
                var td = $("#" + tableName + " #td-init-" + assumption.Key);
                var unit = td.next("td").data("unit");
                if (unit == "%") {
                    var precision = td.next("td").data("precision") != undefined ? td.next("td").data("precision") : 2;
                    td.html((assumption.Value * 100).toFixed(precision) + "%");
                }
                else {
                    td.html(assumption.Value + " yrs");
                }
            }
        });
    };

    var populateLDITable = function (ldiData) {
        if (ldiData != null) {
            $("#hedged_currentBasis_interest_rate").html(Math.round(ldiData.interestRate * 100) + '%');
            $("#hedged_currentBasis_inflation").html(Math.round(ldiData.inflation * 100) + '%');
            $("#hedged_cashflows_interest_rate").html(Math.round(ldiData.cashflowInterestRate * 100) + '%');
            $("#hedged_cashflows_inflation").html(Math.round(ldiData.cashflowInflation * 100) + '%');
            $("#hedgedCashflowsTable").show();
        }
        else {
            $("#hedgedCashflowsTable").hide();
        }
    };

    var populateMarketDataTable = function (marketData) {
        $('#marketDataYieldsInflation td:first').html(marketData.Yields.Inflation.Before.toFixed(2) + '%');
        $('#marketDataYieldsInflation td:last').html(marketData.Yields.Inflation.After.toFixed(2) + '%');
        $('#marketDataYieldsGilts td:first').html(marketData.Yields.Gilts.Before.toFixed(2) + '%');
        $('#marketDataYieldsGilts td:last').html(marketData.Yields.Gilts.After.toFixed(2) + '%');
        $('#marketDataYieldsLinkedGilts td:first').html(marketData.Yields.IndexLinkedGilts.Before.toFixed(2) + '%');
        $('#marketDataYieldsLinkedGilts td:last').html(marketData.Yields.IndexLinkedGilts.After.toFixed(2) + '%');
        $('#marketDataYieldsCorporateBonds td:first').html(marketData.Yields.CorporateBonds.Before.toFixed(2) + '%');
        $('#marketDataYieldsCorporateBonds td:last').html(marketData.Yields.CorporateBonds.After.toFixed(2) + '%');
        $('#marketDataReturnsUKEquity td:last').html(marketData.Returns.UkEquity.toFixed(2) + '%');
        $('#marketDataReturnsGlobalEquity td:last').html(marketData.Returns.GlobalEquity.toFixed(2) + '%');
        $('#marketDataReturnsProperty td:last').html(marketData.Returns.Property.toFixed(2) + '%');
        $('#marketDataReturnsDGFs td:last').html(marketData.Returns.Dgfs.toFixed(2) + '%');
        $('#marketDataReturnsGilts td:last').html(marketData.Returns.Gilts.toFixed(2) + '%');
        $('#marketDataReturnsIndexLinkedGilts td:last').html(marketData.Returns.IndexLinkedGilts.toFixed(2) + '%');
        $('#marketDataReturnsCorporateBonds td:last').html(marketData.Returns.CorporateBonds.toFixed(2) + '%');

        populateMarketDataIndicesRows(marketData.MarketDataIndices);
    };
    var populateMarketDataIndicesRows = function (indices) {
        if (typeof indices === "undefined")
            return;
        $('table#marketdata_yields tr[id^=customIdx-]').remove();
        $.each(indices, function (idx, indexData) {
            $('#marketdata_yields tr:last').after('<tr id="customIdx-' + idx + '"><th>' + indexData.Label + '</th><td class="values">' + indexData.IndexValue.Before.toFixed(2) + '%</td><td class="values">' + indexData.IndexValue.After.toFixed(2) + '%</td></tr>');
        });
    };
    populateSchemeDataTable = function (schemeData) {
        $("#evolution-scheme-data-regular-employee").html(fusion.util.formatCurrency(schemeData.RegularEmployee));
        $("#evolution-scheme-data-regular-employer").html(fusion.util.formatCurrency(schemeData.RegularEmployer));
        $("#evolution-scheme-data-deficit").html(fusion.util.formatCurrency(schemeData.Deficit));
        $("#evolution-scheme-data-total").html(fusion.util.formatCurrency(schemeData.Total));
        $("#evolution-scheme-data-benefit-payments").html(fusion.util.formatCurrency(schemeData.Payment));
        $("#evolution-scheme-data-net-cashflow").html(fusion.util.formatCurrency(schemeData.NetCashflow));
    };

    this.refresh = function () {

        $.getJSON('/Evolution/EvolutionAssumptionsPanel', function (data) {

            $("#evolutiondashboard").tabs(data.showAssets ? "enable" : "disable", 2); //Assets tab

            // tables
            liabilitytabler.refresh(data.liabilityAssumptions.current, true);
            populateInitialAssumptions("liability-assumptions-table", data.liabilityAssumptions.initial);

            var pincsVisibleAtBothInitialAndCurrent =
                $.map($.grep(data.pensionIncreases.current, function (pinc, idx) {
                    return pinc.IsVisible || $.grep(data.pensionIncreases.initial, function (x) { return x.Key == pinc.Key; })[0].IsVisible;
                }), function (pinc) {
                    return {
                        Key: pinc.Key,
                        Label: pinc.Label,
                        Value: pinc.Value,
                        IsVisible: true
                    };
                });
            pensionincreasetabler.refresh(pincsVisibleAtBothInitialAndCurrent, true); //initialise to set row as hidden/visible as required
            pensionincreasetabler.refresh(data.pensionIncreases.current); //call again so that the correct visibility is set on the cells
            populateInitialAssumptions("pension-increase-assumptions-table", data.pensionIncreases.initial);

            populateLDITable(data.ldiData);

            populateMarketDataTable(data.marketData);

            populateSchemeDataTable(data.schemeData);
            
            // donuts
            new fusion.util.AssetsBreakdownHelper().draw({
                containerId: 'assetBreakdownPie',
                synthetic: {
                    data: data.syntheticAssetInfo
                },
                categories: data.assetsBreakdown,
                hideClasses: false,
                showZeroEntries: false,
                showFunds: false,
                borderColorMode: 'blue',
                alignment: 'left',
                dimensions: {
                    marginTop: 20,
                    marginBottom: 20
                },
            });

            var donutHelper = new fusion.util.DonutHelper();

            var liabSeries = donutHelper.getLiabilitySeriesData(data.liabilitiesBreakdown, { innerSize: 72 });
            fusion.charts.donutHandler().redraw(
                liabSeries,
                donutHelper.getLiabilitySpec(data.liabilitiesBreakdown, {
                    chartDivId: 'liabilityBreakdownPie',
                    legendAlign: 'right',
                    height: 235,
                    marginTop: 20,
                    marginBottom: 20
                }),
                true);

            //set height of container holding assets breakdown and legend
            if ($("#evolution-assumptions-assets-container").css("height") == "0px") {
                var minheight = Math.max($(".legend-expando-leftaligned").height(), $("#assetBreakdownPie").height());
                $("#evolution-assumptions-assets-container").css("height", (minheight + 53) + "px");
            }

            //assets growth
            var $assetsGrowthTable = $("table#evolution-assets-growth tbody");
            $assetsGrowthTable.find("tr:not(.head)").remove();
            var synthFlag = false;
            $.each(data.assetsGrowth, function (idx, val) {
                if (val.IsSynthetic)
                    synthFlag = true;
                if (!val.IsSynthetic && synthFlag) {
                    $assetsGrowthTable.find("tr:last").addClass("row-space-bottom25");
                    synthFlag = false;
                }
                $assetsGrowthTable.append(
                    "<tr>"
                    + "<th>" + val.Name + "</th>"
                    + "<td class='values'>" + (val.Growth != null ? fusion.util.formatPercentage(val.Growth) : "n/a") + "</td>"
                    + "<td class='values'>" + fusion.util.formatCurrency(val.Amount) + "</td>"
                    + "</tr>");
            });
        });
    };
};

$.Core.evolutionAssumptionsPanel = new EvolutionAssumptionsPanel();
