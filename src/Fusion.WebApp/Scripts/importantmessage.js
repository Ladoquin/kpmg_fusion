/*
*	KPMG
*	homewarningmessage.js
*	Space01 Ltd. 2014
*/


fusion.importantmessage = {
    init: function () {
        if ($("#importantmessage-container").length > 0)
        {
            var msgid = $("#importantmessage-container").attr("data-importantmessage-id");
            if (this.hide(msgid)) {
                $("#importantmessage-container").hide();
            }
            else {
                $("#importantmessage-container").show();
                this.showclosebtn(msgid);
            }

        }
    },
    showclosebtn: function (msgid) {
        $("#btn-importantmessage-close a").click(function (e) {
            fusion.importantmessage.createCookie("fusion_importantmessage_" + msgid, "hidden", 365);
            $("#importantmessage-container").hide();
            e.preventDefault();
        });
    },
    hide: function (msgid) {
        var ret = 0;
        if (this.getCookie("fusion_importantmessage_" + msgid) == "") {
            ret = false;
        }
        else {
            this.deleteCookie("fusion_importantmessage_" + msgid);
            this.createCookie("fusion_importantmessage_" + msgid, "hidden", 365);
            ret = true;
        }
        return ret;

    },
    createCookie: function (cookieName, cookieValue, nDays, cookiePath) {
        var today = new Date();
        var expire = new Date();
        var path = cookiePath;
        if (!path) {
            path = "/";
        }
        if (nDays == null || nDays == 0) {
            nDays = 1;
        }
        if (nDays == -1) {
            expire.setTime("Thu, 01-Jan-70 00:00:01 GMT;");
        } else {
            expire.setTime(today.getTime() + 3600000 * 24 * nDays);
        }
        document.cookie = cookieName + "=" + escape(cookieValue) + ";path=" + path + ";expires=" + expire.toGMTString();
    },
    getCookie: function (cookieName) {
        var theCookie = "" + document.cookie;
        var ind = theCookie.indexOf(cookieName);
        if (ind == -1 || cookieName == "") return "";
        var ind1 = theCookie.indexOf(';', ind);
        if (ind1 == -1) ind1 = theCookie.length;
        return unescape(theCookie.substring(ind + cookieName.length + 1, ind1));
    },
    deleteCookie: function (cookieName) {
        this.createCookie(name, "", -1);
    }

};


$(function () {
    fusion.importantmessage.init();
});


