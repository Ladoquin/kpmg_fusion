/*
*	KPMG
*	progresscycle.js
*	Space01 Ltd. 2013
* 	author : Jeremy Robson
*/


fusion.progresscycle ={
	init: function(){
	    this.progresscycle();
	},
	progresscycle: function () {
	    //console.log('progresscycle');
	    if ($(".progress-cycle-container").length > 0)
	    {
	        $(".progress-cycle-container").each(function () {
	            var container = $(this);
	            var totalitems = container.find(".progress-cycle").find(".progress-cycle-item").length;
	            if(totalitems>1)
	            {
	                $(container).data("selitem", 0);
	                $(container).data("totalitems", totalitems);
	                $(container).find(".progress-cycle-dots").css("width", (25 * totalitems));
	                $(container).find(".progress-cycle-nav-container-left").html("<span></span>");
	                $(container).find(".progress-cycle-nav-container-right").html("<span></span>");
	                for (var i = 0; i < totalitems; i++) {
	                    $(container).find(".progress-cycle-dots").append("<div class='progress-cycle-dot-container " + (i == 0 ? "selected" : "") + "'><div data-cycle-item='" + i + "' class='progress-cycle-dot'></div></div>")

	                }
	                $(container).find(".progress-cycle-dot").click(function () {
	                    var clickeditem = parseInt($(this).data("cycle-item"));
	                    $(this).parents(".progress-cycle-container").find(".progress-cycle").stop();
	                    $(this).parents(".progress-cycle-container").data("selitem", clickeditem);
	                    $(this).parents(".progress-cycle-container").find(".progress-cycle").cycle(clickeditem);

	                });

                    var cycleopts = {
                                fx: 'scrollHorz',
                                speed: 600,
                                timeout: (isNaN(parseInt($(this).data("timeout")))?0:parseInt($(this).data("timeout"))),
                                pause: 1,
                                slideResize: 1,
                                fit: 1,
                                next: $(this).find('.progress-cycle-nav-container-right span'),
                                prev: $(this).find('.progress-cycle-nav-container-left span'),
                                onPrevNextEvent: function (isNext, zeroBasedSlideIndex, slideElement) {
                                    var container = $(slideElement).parents(".progress-cycle-container");
                                    var myselitem = container.data("selitem");
                                    var totalitems = container.data("totalitems");
                                    if (isNext) {
                                        myselitem++;
                                    }
                                    else {
                                        myselitem--;

                                    }
                                    if (myselitem < 0) {
                                        myselitem = totalitems - 1;
                                    }
                                    else if (myselitem > totalitems - 1) {
                                        myselitem = 0;
                                    }
                                    container.data("selitem", myselitem);
                                },
                                before: function (currSlideElement, nextSlideElement, options, forwardFlag) {

                                },
                                after: function (currSlideElement, nextSlideElement, options, forwardFlag) {
                                    var container = $(currSlideElement).parents(".progress-cycle-container");
                                    var myselitem = container.data("selitem");
                                    container.find(".progress-cycle-dot-container").removeClass("selected");
                                    container.find(".progress-cycle-dot-container:eq(" + myselitem + ")").addClass("selected");

                                }
                            };
	                        cycleopts.width = $(container).find(".progress-cycle-body").width();
                            $(container).find(".progress-cycle").cycle(cycleopts);

                            $(container).find(".progress-cycle").touchwipe({
                                wipeLeft: function (e) {
                                    $(container).find(".progress-cycle").cycle("next");
                                },
                                wipeRight: function (e) {
                                    $(container).find(".progress-cycle").cycle("prev");
                                },
                                wipeUp: function (e) { e.preventDefault() },
                                wipeDown: function (e) { e.preventDefault() }
                            });

                            $(window).resize(function () {
                                cycleopts.width = $(container).find(".progress-cycle-body").width();
                                cycleopts.height = $(container).find(".progress-cycle-item:eq(0)").height()+1;
                                $(container).find(".progress-cycle").cycle('destroy');
                                $(container).find(".progress-cycle").cycle(cycleopts);
                            });


	            }
	        });



	    }
	}
};