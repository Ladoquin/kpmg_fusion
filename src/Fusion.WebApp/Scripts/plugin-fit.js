﻿(function ($) {

    $.fn.fit = function () {
        if (this.length > 0) {
            var originalFontSize = window.getComputedStyle(this[0]).fontSize.replace("px", "");
            var sectionWidth = this.closest("div").width();
            var spanWidth = this.width();
            var newFontSize = Math.min(originalFontSize, (sectionWidth / spanWidth) * originalFontSize);
            this.css({ "font-size": newFontSize });
        }
        return this;
    };

}(jQuery));