﻿var JourneyPlanLens = function () {

    var initialised = false;
    var dataUrl = '/Json/JourneyPlanData';
    var chart = null;
    var isStale = true;
    var isActiveLens = false;

    this.activateLens = function () {
        isActiveLens = true;
        if (isStale) {
            refresh();
        }
    };
    
    this.deactivateLens = function () {
        isActiveLens = false;
    };
    
    this.updateLens = function () {
        if (isActiveLens) {
            refresh();
        } 
        else {
            isStale = true;
        }
    };

    var initialiseFundingProgressionChart = function () {
        chart = new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: 'journeyPlanChartDiv',
                backgroundColor: 'none',
                plotBorderWidth: 1,
                plotBackgroundColor: 'white'
            },
            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: null,
            },
            xAxis: {
                title: {
                    text: ' ',
                    style: {
                        fontFamily: 'Lato, sans-serif',
                        fontSize: '14px',
                    },
                },
            },
            yAxis: [{
                title: {
                    useHTML: true,
                    text: 'Funding level',
                    style: {
                        fontFamily: 'Lato, sans-serif',
                        fontSize: '14px',
                        //                backgroundColor: '#f0f0f0',
                    },
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartPercentageLabels
                },
                plotLines: [{
                    value: 100,
                    width: 1,
                    color: 'green',
                    dashStyle: 'dash',
                    zIndex: 10,
                    label: {
                        text: 'Fully funded',
                        align: 'left',
                        y: -4,
                        x: 23,
                        useHTML: true,
                        style: {
                            margin: '3px',
                        }
                    }
                }],
                minRange: 110,
                min: 0
            }, {
                title: {
                    useHTML: true,
                    text: 'Funding level',
                    style: {
                        fontFamily: 'Lato, sans-serif',
                        fontSize: '14px',
                        //                backgroundColor: '#f0f0f0',
                    },
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartPercentageLabels
                },
                linkedTo: 0,
                opposite: true,
            }],
            plotOptions: {
                column: {
                    pointPadding: 0,
                    borderWidth: 0,
                },
                series: {
                    events: {
                        legendItemClick: function () {
                            return !(this.name == 'Before')
                        }
                    }
                }
            },
            tooltip: {
                shared: true,
                hideDelay: 0,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                followPointer: true,
                formatter: function () {
                    var html = 'Year ' + this.x + '<br/>';
                    $.each(this.points, function (i, point) {
                        if (i > 0) {
                            html += '<br/>';
                        };
                        html += '<br/>' + point.series.name + ': ' + point.y.toFixed(0) + '%';
                        html += '<br/>Liabilities: ' + fusion.util.formatCurrency(point.point.liabs);
                        html += '<br/>Assets: ' + fusion.util.formatCurrency(point.point.assets);
                    });

                    return html;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            legend: {
                layout: 'horizontal',
                floating: true,
                align: 'left',
                verticalAlign: 'bottom',
                borderWidth: 0,
                borderRadius: 0,
                x: 60,
            },


            series: [{
                name: 'Before',
                color: '#EBB700',
                data: [0]
            }, {
                name: 'After',
                color: '#C84E00',
                data: [0]
            }],

        });
    };

    var initialiseLiabilityProgressionChart = function () {
        $("#funding-progression-chart-title").text("Liability Progression");
        $("#funding-progression-chart-footer").text("Progression of liability levels based on selected liability assumptions");
        $("#funding-progression-tab-link").text("Liability Progression");
        chart = new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: 'journeyPlanChartDiv',
                backgroundColor: 'none',
                plotBorderWidth: 1,
                plotBackgroundColor: 'white'
            },
            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: null,
            },
            xAxis: {
                title: {
                    text: ' ',
                    style: {
                        fontFamily: 'Lato, sans-serif',
                        fontSize: '14px',
                    },
                },
            },
            yAxis: [{
                title: {
                    text: ' '
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                    overflow: 'justify'
                },
            }, {
                title: {
                    text: ' '
                },
                linkedTo: 0,
                opposite: true,
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                    overflow: 'justify'
                },
            }],
            plotOptions: {
                column: {
                    pointPadding: 0,
                    borderWidth: 0,
                },
                series: {
                    events: {
                        legendItemClick: function () {
                            return !(this.name == 'Before')
                        }
                    }
                }
            },
            tooltip: {
                shared: true,
                hideDelay: 0,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                followPointer: true,
                formatter: function () {
                    var html = 'Year ' + this.x + '<br/>';
                    $.each(this.points, function (i, point) {
                        if (i > 0) {
                            html += '<br/>';
                        };
                        html += '<br/>' + point.series.name;
                        html += '<br/>Liabilities: ' + fusion.util.formatCurrency(point.point.y);
                    });

                    return html;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            legend: {
                layout: 'horizontal',
                floating: true,
                align: 'left',
                verticalAlign: 'bottom',
                borderWidth: 0,
                borderRadius: 0,
                x: 60,
            },
            series: [{
                name: 'Before',
                color: '#EBB700',
                data: [0]
            }, {
                name: 'After',
                color: '#C84E00',
                data: [0]
            }],
        });
    };

    var initialiseLens = function(opts) {

        if (opts != null && typeof opts.chartType != "undefined" && opts.chartType == "lp") {
            initialiseLiabilityProgressionChart();
        }
        else {
            initialiseFundingProgressionChart();
        }

        initialised = true;
    };

    var getFundingProgressionChartData = function (plan) {
        var before = [];
        var after = [];
        var categories = [];
        for (var i = 0; i < plan.length; i++) {
            var p = plan[i];
            before.push({ y: p.before.proportion, liabs: p.before.liabs, assets: p.before.assets });
            after.push({ y: p.after.proportion, liabs: p.after.liabs, assets: p.after.assets });
            categories.push(plan[i].year);
        };
        return {
            before: before,
            after: after,
            categories: categories
        };
    };

    var getLiabilityProgressionChartData = function (plan) {
        var before = [];
        var after = [];
        var categories = [];
        for (var i = 0; i < plan.length; i++) {
            var p = plan[i];
            before.push({ y: p.before.liabs });
            after.push({ y: p.after.liabs });
            categories.push(plan[i].year);
        };
        return {
            before: before,
            after: after,
            categories: categories
        };
    };

    var refresh = function() {

        $.getJSON(dataUrl, function (data) {

            if (!initialised) {
                initialiseLens({
                    chartType: data.showAlternate ? "lp" : ""
                });
            };

            var plan = data.plan;

            var chartData = data.showAlternate
                ? getLiabilityProgressionChartData(plan)
                : getFundingProgressionChartData(plan);

            var before = chartData.before;
            var after = chartData.after;
            var categories = chartData.categories;

            chart.xAxis[0].setCategories(categories, false);

            if (chart.series[0].data.length == before.length) {
                for (i = 0; i < before.length; i++) {
                    chart.series[0].data[i].update(before[i], false);
                }
            } else {
                chart.series[0].setData(before, false);
            }

            if (chart.series[1].data.length == after.length) {
                for (i = 0; i < after.length; i++) {
                    chart.series[1].data[i].update(after[i], false);
                }
            } else {
                chart.series[1].setData(after, false);
            }

            chart.series[1].setVisible(data.isDirty, false);

            chart.xAxis[0].setTitle({
                text: 'Annual progression commencing ' + Highcharts.dateFormat('%e %B', plan[0].commencementDate)
            });

            chart.redraw();

            isStale = false; 
        });
    };
};

