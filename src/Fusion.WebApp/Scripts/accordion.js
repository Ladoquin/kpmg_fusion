/*
*	KPMG
*	accordion.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This invokes an accordion*/

fusion.accordion ={
	init: function(){
		$('[data-accordion="true"]').each(function(index) {			
			var configurations = {
				heightStyle: "content",
				collapsible: true
			};

			$(this).accordion(configurations);
		});		
	}
};