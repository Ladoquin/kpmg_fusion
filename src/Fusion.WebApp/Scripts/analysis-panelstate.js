﻿/*
 * panelState:
 * Manage visually representing if the data on a panel has been changed. 
 */
fusion.analysis.panelState = function () {

    var that = this;

    var getEl = function (panelid) {
        return $("a[href=#" + panelid + "]");
    };

    var setPanelAsDirty = function (el) {
        if ($("img", el).length == 0) {
            el.append("<img src='/Images/tick2.png' title='The assumptions on this panel have been amended' class='panelChanged' />");
        }
    };

    var setPanelAsClean = function (el) {
        $("img", el).remove();
    };

    var setAllAsClean = function () {
        $(".panelChanged").remove();
    };

    var notify = function () {
        that.panelStateChangedEvent.fire($(".panelChanged").length > 0);
    };

    this.panelStateChangedEvent = $.Callbacks();

    this.refresh = function () {
        setAllAsClean();
        $.getJSON("/Json/PanelState", function (data) {
            var els = [];
            if (data.liabilityAssumptions)
                els.push(getEl("analysisdashboard_liabilities"));
            if (data.assetAssumptions)
                els.push(getEl("analysisdashboard_assets"));
            if (data.recoveryPlan)
                els.push(getEl("analysisdashboard_recoveryplan"));
            if (data.investmentStrategy || data.investmentStrategyOptions)
                els.push(getEl("analysisdashboard_investmentstrategy"));
            if (data.FRO)
                els.push(getEl("analysisdashboard_fro"));
            if (data.ETV)
                els.push(getEl("analysisdashboard_etv"));
            if (data.PIE)
                els.push(getEl("analysisdashboard_pie"));
            if (data.futureBenefits)
                els.push(getEl("analysisdashboard_futurebenefits"));
            if (data.ABF)
                els.push(getEl("analysisdashboard_abf"));
            if (data.insurance)
                els.push(getEl("analysisdashboard_insurance"));

            $.each(els, function (idx, val) {
                setPanelAsDirty(val);
            });

            notify();
        });
    };

    this.set = function (panelid, dirty) {
        var el = getEl(panelid);
        if (dirty) {
            setPanelAsDirty(el);
        }
        else {
            setPanelAsClean(el);
        };
        notify();
    };
};

