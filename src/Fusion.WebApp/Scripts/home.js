﻿$(function () {

    $("#expandingSchemeCashflow").expandingarea();
    
    // ********
    // * load
    // ********

    var miniEvolution;

    var refreshFundingTarget = function (basis, fundingTarget) {
        if (basis.type === "TechnicalProvision") {
            $("#funding_target_box").css("display", "block");
            $("#funding_target").html((fundingTarget < 0 ? "-" : "+") + fusion.util.formatCurrencyAbsolute(fundingTarget).replace("K", "k").replace("M", "m"));
            $("#funding_target")
                .removeClass(fundingTarget < 0 ? "greencolor" : "rougeredcolor")
                .addClass(fundingTarget < 0 ? "rougeredcolor" : "greencolor");
            $("#funding_target_text").html((fundingTarget > 0 ? "ahead of" : "behind") + $("#funding_target_text").html().replace("ahead of", "").replace("behind", ""));
        }
        else {
            $("#funding_target_box").css("display", "none");
        };
    };
    var refreshInitialBalance = function (balance, date) {
        $('#initialBalanceDate').html((balance < 0 ? 'Deficit at ' : 'Surplus at ') + Highcharts.dateFormat('%e %B %Y', date));
        $('#initialBalance').html(fusion.util.formatCurrencyAbsolute(balance)).fit();
        $('#initialBalanceLabel')
            .removeClass(balance < 0 ? 'darkolivegreen' : 'rougered')
            .addClass(balance < 0 ? 'rougered' : 'darkolivegreen');
    };
    var refreshHedgedCashflows = function (ldiData) {
        if (ldiData == null) {
            $("#hedged_cashflows").css("display", "none");
        }
        else {
            $("#hedged_cashflows").css("display", "block");
            $("#hedged_currentBasis_interest_rate").html(Math.round(ldiData.interestRate * 100) + '%');
            $("#hedged_currentBasis_inflation").html(Math.round(ldiData.inflation * 100) + '%');
            $("#hedged_cashflows_interest_rate").html(Math.round(ldiData.cashflowInterestRate * 100) + '%');
            $("#hedged_cashflows_inflation").html(Math.round(ldiData.cashflowInflation * 100) + '%');
        }
    };
    var refreshCashflowChart = function (date) {
        // highcharts problem means if the div isn't visible it will get the width wrong
        // so expand the area for rendering and hide (or return to original state) afterwards.
        var collapse = $("#expandingSchemeCashflow").expandingarea("options", "state") == "collapsed";
        $("#expandingSchemeCashflow").expandingarea("options", "expand");
        var cashflowLens = new fusion.analysis.Cashflow();
        cashflowLens.setOptions({ chartDivId: 'cashflowChartDiv', excludeAfterSeries: true, date: date, dataUrl: '/Home/CashflowData' });
        cashflowLens.activateLens();
        if (collapse) {
            $("#expandingSchemeCashflow").expandingarea("options", "collapse");
        };
    };
    var refreshDonuts = function (data) {

        var donutHelper = new fusion.util.DonutHelper();
        var liabilitySeries = donutHelper.getLiabilitySeriesData(data.liability);

        fusion.charts.donutHandler().redraw(liabilitySeries, {
            chartDivId: 'liabilityBreakdownPie',
            height: 240,
            marginTop: 20,
            marginBottom: 20
        },
            true);

        if (data.asset != null && data.asset.length > 0) {
            new fusion.util.AssetsBreakdownHelper().draw({
                containerId: 'assetBreakdownPie',
                synthetic: {
                    data: data.syntheticAssetInfo
                },
                categories: data.asset,
                hideClasses: true,
                showZeroEntries: false,
                showFunds: false,
                dimensions: {
                    height: 240,
                    marginTop: 20,
                    marginBottom: 20
                },
                borderColorMode: 'grey',
                aligntment: 'bottom'
            });
        }
        else {
            $("#assetBreakdownPie").text("N/A");
        }
    };
    var refreshBuyinState = function (buyinDate) {
        $("#li-documentchecklist-buyin").removeClass("active");
        $("#li-documentchecklist-buyin-date").text("");
        if (buyinDate != null) {
            $("#li-documentchecklist-buyin").addClass("active");
            $("#li-documentchecklist-buyin-date").text("(" + Highcharts.dateFormat('%d/%m/%Y', buyinDate) + ")");
        };
    };

    var refresh = function() {

        $.get('/Home/Get', function(data) {

            refreshFundingTarget(data.basis, data.fundingTarget);
            refreshInitialBalance(data.balance, data.date);
            refreshHedgedCashflows(data.ldiData);
            refreshCashflowChart(data.date);
            refreshBuyinState(data.buyinState);
            refreshDonuts({
                asset: data.assetsBreakdown,
                liability: data.liabilitiesBreakdown,
                syntheticAssetInfo: data.syntheticAssetInfo
            });
            miniEvolution.refresh(data.evolution);            
        });
    };



    fusion.basisChangedEvent.add(function (newBasis) {
        refresh();
    });

    $(document).ready(function () {

        fusion.page.init({ idx: 0, name: "baseline" });

        $(".ball").linkable();
        fusion.lightbox.init();

        miniEvolution = new fusion.MiniEvolutionLens();
        miniEvolution.initialise();

        refresh();
    });
});


