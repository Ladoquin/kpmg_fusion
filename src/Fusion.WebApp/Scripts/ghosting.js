﻿$(function () {

    $('.ghost').each(function (index) {// Adds the Ghost effect on textbox

        if ($(this).val() === "" || $(this).val() === $(this).attr("data-ghost")) {
            $(this).attr("value", $(this).attr("data-ghost")).css({ "color": "#ccc" });
        }
    });
    
    $('.ghost').on("focus", function () {
        if ($(this).val() === $(this).attr("data-ghost")) {
            $(this).val("").css({ "color": "#000000" });//Clear text and return normal color
        }
    }).blur(function () {
        if ($(this).val() === "") {//Nothing has changed
            $(this).val($(this).attr("data-ghost")).css({ "color": "#ccc" });//return back to ghost state
        }
    });
}());




