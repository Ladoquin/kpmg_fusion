﻿fusion.util.LiabilitiesTabler = function (id) {

    var that = this;
    var _init = false;
    var _id = id;

    var buildTable = function (assumptions) {
        $("#" + _id + " tr:not(.head)").hide();
        $.each(assumptions, function (idx, o) {
            var $th = $("#" + _id + " #tr-" + o.Key + " th");
            if (!$th.hasClass('preventSchemeLabelOverride'))
                $th.html(o.Label);
            if (typeof o.IsVisible == "undefined" || o.IsVisible)
                $th.closest("tr").show();
            else
                $th.closest("tr").hide();
        });
    };

    var populateTable = function (assumptions) {
        $.each(assumptions, function (idx, o) {
            var td = $("#" + _id + " #tr-" + o.Key + " td:last");
            var html = '';
            
            if (td.hasClass('allowLabelOverride') && o.Label != '-') //todo: too hacky!
                html = '<span>' + o.Label + '</span>';
            else if (td.data("unit") == "yrs")
                html = o.Value + " yrs";
            else 
                html = (o.Value * 100).toFixed(td.data("precision") !== undefined ? td.data("precision") : 2) + "%";

            if (typeof o.IsVisible != "undefined" && o.IsVisible == false) {
                td.css("visibility", "hidden");
            }
            else {
                td.closest("tr").addClass("assumption-visible");
                td.css("visibility", "visible");
            }
            td.html(html);
        });
        $("#" + _id + " tr.assumption-visible").first().addClass("no-border-top");
    };

    this.init = function (assumptions) {
        buildTable(assumptions);
        _init = true;
    };

    this.refresh = function (data, reinit) {
        if (!_init || reinit) {
            that.init(data);
        };
        populateTable(data);
    };
};
