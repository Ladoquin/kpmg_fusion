﻿(function ($) {

    $.fn.schemeSearcher = function (onSelectCallback) {
        $.each(this, function (idx, el) {
            $.getJSON($(el).attr('data-schemesource'), function (data) {
                $(el).autocomplete({
                    source: data,
                    minLength: 2,
                    select: function (event, ui) {
                        if (onSelectCallback) {
                            onSelectCallback(ui.item.value);
                        }
                    }
                });
            });
        });
        return this;
    };

}(jQuery));


