﻿fusion.util = {

    showBasisMenu: function () {
        if ($('#currentBasisMenu').is(":visible")) {
            $('#currentBasisMenu').slideUp(100);
        }
        else {
            $('#currentBasisMenu').slideDown(100);
        }
    },

    showTechnicalProvisionsBasisMenu: function () {
        if ($('#technicalProvisionsBasisMenu').is(":visible")) {
            $('#technicalProvisionsBasisMenu').slideUp(100);
        }
        else {
            $('#technicalProvisionsBasisMenu').slideDown(100);
        }
    },

    formatCurrencyAbsolute: function (val) {
        return fusion.util.formatCurrency(Math.abs(val));
    },

    getCurrencySymbol: function(){
        return $("#currency-symbol").val();
    },

    getDateFromDatepicker: function (datepickerVal) {
        var date = new Date(Date.UTC(
            datepickerVal.split("/")[2],
            datepickerVal.split("/")[1] - 1,
            datepickerVal.split("/")[0], 0, 0, 0, 0)).getTime();
        return date;
    },

    formatCurrency: function (val) {
        var sym = $("#currency-symbol").val();
        var prefix = (val < 0 ? '-' : '') + sym;
        val = Math.abs(val);
        if (val > 0 && val % 1 != 0) {
            val = parseInt(val.toFixed(2));
        };

        var unit;

        if (val < 1000) {
            unit = '';
            val = Number(val.toPrecision(3));
        } else if (val < 1000000) {
            unit = 'K';
            val /= 1000;
            val = Number(val.toPrecision(3));
        } else if (val < 1000000000) {
            unit = 'M';
            val /= 1000000;
            val = Number(val.toPrecision(3));
        } else {
            unit = 'M';
            val /= 1000000;
            val = Math.round(val);
        }

        return prefix + fusion.util.formatNumberWithSeparators(val) + unit;
    },

    formatNumberWithSeparators: function (val) {
        var parts = val.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    },

    formatChartCurrencyLabels: function () {
        var val = fusion.util.formatCurrencyAbsolute(this.value);
        if (this.value == 0) {
            return '<b class="originLabel">' + val + '</b>';
        } else return val;
    },

    formatChartPercentageLabels: function () {
        if (this.value == 100) {
            return '<b class="originLabel">' + this.value.toFixed(0) + '%</b>';
        } else return this.value.toFixed(0) + '%';
    },
    
    formatPercentage: function (val, fixed) {
        if (typeof fixed == "undefined" || fixed == null)
            fixed = 2;

        var r = (val * 100).toFixed(fixed) + '%';

        if (r === "-0%")
            r = "0%";

        return r;
    },

    formatDateShort: function (date) {
        var d = new Date(date);
        var m = d.getMonth() + 1;
        var month = (m < 10) ? '0' + m.toString() : m.toString();
        return d.getDate() + "/" + month + "/" + d.getFullYear().toString().substring(2);
    },

    convertDateToMilliseconds: function (val) {
        var d =
            new Date(Date.UTC(
                val.split("/")[2],
                val.split("/")[1] - 1,
                val.split("/")[0], 0, 0, 0, 0)).getTime();

        return d;
    },

    colorGradient: function (startColor, endColor, distance) {
        var start = this.hexToRGB(startColor);
        var end = this.hexToRGB(endColor);

        var r = Math.round(((end[0] - start[0]) * distance) + start[0]);
        var g = Math.round(((end[1] - start[1]) * distance) + start[1]);
        var b = Math.round(((end[2] - start[2]) * distance) + start[2]);

        r = r < 0 ? 0 : r > 255 ? 255 : r;
        g = g < 0 ? 0 : g > 255 ? 255 : g;
        b = b < 0 ? 0 : b > 255 ? 255 : b;

        r = r < 16 ? '0' + r.toString(16) : r.toString(16);
        g = g < 16 ? '0' + g.toString(16) : g.toString(16);
        b = b < 16 ? '0' + b.toString(16) : b.toString(16);

        return '#' + r + g + b;
    },

    hexToRGB: function (hex) {
        hex = hex.replace('#', '');
        var bigint = parseInt(hex, 16);
        var r = (bigint >> 16) & 255;
        var g = (bigint >> 8) & 255;
        var b = bigint & 255;
        return [r, g, b];
    },

    rgbToCSS: function (rgb) {
        return "rgb(" + rgb[0] + ", " + rgb[1] + ", " + rgb[2] + ")";
    },

    rgbaToCSS: function (rgba) {
        return "rgba(" + rgba[0] + ", " + rgba[1] + ", " + rgba[2] + ", " + rgba[3] + ")";
    },

    rgbFromCSS: function (css) {
        var rgb = [];
        var cssStripped = css.replace("rgb", "").replace("a", "").replace("(", "").replace(")").trim();
        for (var i = 0; i <= 2; i++) {
            rgb.push(parseInt(cssStripped.split(",")[i].trim()));
        }
        return rgb;
    },
    
    createDialogStyle: function (event, ui) {
        var widget = $(event.target).dialog("widget");
        $(".ui-dialog-titlebar-close .ui-button-text", widget).text("");
        $(".ui-dialog-buttonpane", widget).addClass("clearfix");
        var buttons = $(".ui-dialog-buttonpane", widget).find("button");
        buttons.has("span:contains('Cancel')").addClass("btn-dialog-cancel");
    },

    toJavaScriptDate: function (value) {
        var pattern = /Date\(([^)]+)\)/;
        var results = pattern.exec(value);
        return new Date(parseFloat(results[1]));
        //return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
    },
    
    enableSlider: function ($slider) {
        $slider.removeClass("readonly-rp");
        $slider.find('.ui-slider-tick-mark').css('background-color', '#98c6ea');
        $slider.closest("td").removeClass("greySlider");
        $slider.slider('enable');
    },
    
    disableSlider: function ($slider, $ignoreLabelText) {
        $slider.addClass("readonly-rp").closest("td").addClass("greySlider");
        $slider.find('.ui-slider-tick-mark').css('background-color', 'lightgrey');
        $slider.slider('disable');
        $slider.slider('disableEditableElement', $slider);
        if (!$ignoreLabelText)
            $slider.parent().siblings().last().html('<span class="slider-text-container fro-disabled-text">n/a</span>');
    },

    lightenColor: function(color, percent) {  
        var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
        return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (G<255?G<1?0:G:255)*0x100 + (B<255?B<1?0:B:255)).toString(16).slice(1);
    }

}
