﻿function classer() {
    this.location = 'home';
    this.check = true;
}

$.fn.exists = function () {
    return this.length !== 0;
}

$.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
};

var isiPad = navigator.userAgent.match(/iPad/i) != null;


classer.prototype.getUri = function () {

    var x = document.location.href;
    var url = x.split('index.php');
    return url[0];
}




//Initiate the class
var kpmg = new classer();



//--------------------------------------------------

$(window).resize(function () { });


$(document).ready(function () {

    //---------Sign in FORM  behavior---------------------
    //disabled panel drop down for the moment, not deleting because it's coming back
    //$('.signin').click(function () {        
    //    $('#login_header').css('height', '137px');
    //    $('#contactpanel').hide();
    //    $('#forgottenUsername').hide();
    //    $('#forgottenPassword').hide();
    //    $('#login_header,#loginpanel').slideDown(100);
    //});

    $('.close_login').click(function () { $('#login_header,#loginpanel').slideUp(100); });

    //---------Request FORM  behavior---------------------
    $('.request').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $('#loginpanel').hide();
        $('#forgottenPassword').hide();
        $('#forgottenUsername').hide();
        $('#login_header').css('height', '280px');
        $('#login_header,#contactpanel').slideDown(100);

    });

    $('.close_contact').click(function () { $('#login_header,#contactpanel').slideUp(100); });

    //---------Forgot username FORM  behavior---------------------

    $('.usernamex').click(function () {
        $('#login_header').slideUp(100);
        $('#forgottenPassword').hide();
        $('#contactpanel').hide();


        $('#loginpanel').slideUp(100, function () {
            $('#login_header,#forgottenUsername').slideDown(100);
            $('#login_header').css('height', '137px').show();
        });


    });

    $('.close_username').click(function () { $('#login_header,#forgottenUsername').slideUp(100); });

    //---------Forgot password FORM  behavior---------------------

    $('.passwordx').click(function () {
        $('#login_header').slideUp(100);
        $('#forgottenUsername').hide();
        $('#contactpanel').hide();

        $('#loginpanel').slideUp(100, function () {
            $('#login_header,#forgottenPassword').slideDown(100);
            $('#login_header').css('height', '137px');
        });


    });

    $('.close_password').click(function () { $('#login_header,#forgottenPassword').slideUp(100); });


    $(".various").fancybox();

    function GetUri() {

    }


    $("#contactName,#contactEmail,#contactRole,#contactOrg,#contactPhone").click(function () {
        $(this).parent().find('label span').hide();
    });

    $('#kpmg_form input[type=submit]').click(function (e) {
        var submitLogin = true;
        var un = $.trim($("#UserName").val());
        var pw = $.trim($("#Password").val());
        if (un.length < 1) {
            submitLogin = false;
            $('#UserName').css({ "background": "#F00", "color": "#FFF" }).val('Required').focus();
        };
        if (pw.length < 1) {
            submitLogin = false;
            $('#Password').css({ "background": "#F00", "color": "#FFF" }).focus();
        };
        if (!submitLogin) e.preventDefault();
    });

    $('#forgottenUsernameSubmit').click(function (e) {
        e.preventDefault();
        var emailVal = $.trim($("#forgottenUsernameEmail").val());
        if (emailVal.length < 1 || emailVal === 'Required') {
            $('#forgottenUsernameEmail').css({ "background": "#F00", "color": "#FFF" }).val('Required').focus();
        }
        else {
            $.post($(this).closest("form").attr("action"),
                {
                    email: emailVal,
                    __RequestVerificationToken: $(this).closest("form").find("input[name=__RequestVerificationToken]").val()
                });
            $('#forgottenUsername').slideUp(100);
            $('#contactpanel').slideUp(100, function () {
                $('#login_header').css('height', '137px');
                $('#login_header,#thankyou').slideDown(100);
            });
        }
    });

    $('#forgottenPasswordSubmit').click(function (e) {
        e.preventDefault();
        var usernameVal = $.trim($("#forgottenPasswordUsername").val());
        if (usernameVal.length < 1 || usernameVal === 'Required') {
            $('#forgottenPasswordUsername').css({ "background": "#F00", "color": "#FFF" }).val('Required').focus();
        }
        else {
            $.post($(this).closest("form").attr("action"),
                {
                    username: usernameVal,
                    __RequestVerificationToken: $(this).closest("form").find("input[name=__RequestVerificationToken]").val()
                });
            $('#forgottenPassword').slideUp(100);
            $('#contactpanel').slideUp(100, function () {
                $('#login_header').css('height', '137px');
                $('#login_header,#thankyou').slideDown(100);
            });
        }
    });

    $('#kpmg_contact_form .trigger').click(function (e) {
        kpmg.check = true;
        var cn = $.trim($("#contactName").val());
        var ce = $.trim($("#contactEmail").val());
        var cr = $.trim($("#contactRole").val());
        var co = $.trim($("#contactOrg").val());
        var cp = $.trim($("#contactPhone").val());

        if (cn.length < 1) {
            kpmg.check = false;
            $('#contactName').parent().find('label span').show().focus();
        }

        if (cr.length < 1) {
            kpmg.check = false;
            $('#contactRole').parent().find('label span').show().focus();
        }

        if (co.length < 1) {
            kpmg.check = false;
            $('#contactOrg').parent().find('label span').show().focus();
        }

        if (ce.length < 1) {
            kpmg.check = false;
            $('#contactEmail').parent().find('label span').show().focus();
        } else {

            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(ce)) {
                kpmg.check = false;
                $('#contactEmail').parent().find('label span').show().focus();
            }
        }

        if (cp.length < 1) {
            kpmg.check = false;
            $('#contactPhone').parent().find('label span').show().focus();
        } else {

            var regex = /^[0-9]{1,12}$/;
            if (!regex.test(cp)) {
                kpmg.check = false;
                $('#contactPhone').parent().find('label span').show().focus();
            }
        }



        if (kpmg.check) {

            var url = $(this).closest("form").attr("action");

            $.post(url, $('#kpmg_contact_form').serialize());

            $('#login_header').slideUp(100);
            $('#contactpanel').slideUp(100, function () {
                $('#login_header').css('height', '137px');
                $('#login_header,#thankyou').slideDown(100);
            });

            $('#contactName').val("");
            $('#contactPhone').val("");
            $('#contactEmail').val("");
            $('#contactRole').val("");
            $('#contactOrg').val("");
        }

        e.preventDefault();

    });

    $('.close_thankyou').click(function () { $('#login_header,#thankyou').slideUp(100); });



});


