﻿fusion.analysis.AssetPanel = function () {

    var that = this;
    var _init = false;
    var weightedAssetLabel = "Weighted asset return";

    this.changed = $.Callbacks();

    var buildAssetAssumptionRows = function (assetReturns) {
        $("#assetAssumptionsTable tr:not(.head)").remove();
        $.each(assetReturns, function (idx, asset) {
            addAssetAssumptionRow(asset);
        });
    };

    var addAssetAssumptionRow = function (asset, noslider) {
        var tr = "<tr id='tr-asset-" + asset.Key + "'><th>" + asset.Label + "</th>";
        if (!noslider) {
            tr += "<td><div class='assumptionsSlider' data-original='" + asset.Value.Before + "'></div></td>";
        } else {
            tr += "<td></td>";
        }

        tr += "<td class='values'>" + (asset.Value.After * 100).toFixed(2) + "%</td>";
        $("#assetAssumptionsTable").append(tr);
    };

    var init = function (data) {

        buildAssetAssumptionRows(data.assetReturns);

        addAssetAssumptionRow({ Key: 0, Label: weightedAssetLabel, Value: { After: data.weightedAssetReturn } }, true);

        $('#resetAssetAssumptions').unbind('click').click(function () {
            $.getJSON('/Json/ResetAssetAssumptions', function (data) {
                that.refresh(data);
                that.changed.fire({ id: that.id, data: data });
            });
        });

        _init = true;
    };

    var drawDonut = function (data) {
        new fusion.util.AssetsBreakdownHelper().draw({
            containerId: 'assetBreakdownPie',
            synthetic: {
                data: data.syntheticAssetInfo
            },
            categories: data.assetsBreakdown,
            hideClasses: false,
            showZeroEntries: false,
            showFunds: false,
            dimensions: {
                marginTop: 30,
                marginBottom: 30
            },
            borderColorMode: 'blue',
            alignment: 'left'
        });
    };

    this.reInit = function () {
        _init = false;
    };

    this.setAssumptions = function () {
        var assetAssumptions = {};
        $('#assetAssumptionsTable .assumptionsSlider').each(function (index) {
            var finalValue = $(this).parent().parent().find('.slider-text-value').data('finalValue');

            if (finalValue == null || finalValue == typeof 'undefined')
                finalValue = $(this).slider("option", "value");

            assetAssumptions[$(this).closest("tr")[0].id.replace("tr-asset-", "")] = finalValue;
        });

        var json = {
            assetAssumptions: JSON.stringify(assetAssumptions)
        };

        $.post('/Json/SetAssetAssumptions', json, function (data) {
            that.refresh(data);
            that.changed.fire({ id: that.id, data: data });
        });
    };


    this.refresh = function (data) {

        if (!_init) init(data);

        drawDonut(data);
        
        var updateCell = function (event, ui) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: ui.value
            });
        }

        var updateSliderValue = function (el, value) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: value
            });
        };

        $('#assetAssumptionsTable .assumptionsSlider').each(function (index) {

            var key = $(this).closest("tr")[0].id.replace("tr-asset-", "");
            var assetReturn = $.grep(data.assetReturns, function (o) {
                return o.Key == key;
            })[0];
            var value = assetReturn.Value.After;

            $(this).slider({
                animate: 400,
                range: 'min',
                min: assetReturn.UserMin,
                max: assetReturn.UserMax,
                value: value,
                step: assetReturn.UserIncrement,
                slide: updateCell,
                stop: that.setAssumptions,
                elementSelector: $(this),
                textElement: $(this).parent().siblings().last()
            });
            $(this).slider('setTextValueElement');
        });
        
        var ldiData = data.ldiData;

        if (ldiData != null) {
            $("#hedged_currentBasis_interest_rate").html(Math.round(ldiData.interestRate * 100) + '%');
            $("#hedged_currentBasis_inflation").html(Math.round(ldiData.inflation * 100) + '%');
            $("#hedged_cashflows_interest_rate").html(Math.round(ldiData.cashflowInterestRate * 100) + '%');
            $("#hedged_cashflows_inflation").html(Math.round(ldiData.cashflowInflation * 100) + '%');            
        }
        else {
            $("#hedgedCashflowsTable-container").hide();
        }

        $("#tr-asset-0").find(":last-child").html((data.weightedAssetReturn * 100).toFixed(2) + "%");

        $('.analysisPeriodEnd', $("#analysis_assets")).html(Highcharts.dateFormat('%d/%m/%Y', data.date));
    };
}

