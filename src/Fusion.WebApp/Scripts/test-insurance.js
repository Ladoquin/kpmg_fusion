﻿fusion.test.insuranceTest = (function () {

    return {
        run: function () {

            var input = {
                pensioners: 100,
                nonpensioners: 150,
                pensionerLiabilityInsured: 100,
                nonpensionerLiabilityInsured: 0
            };

            var output = fusion.calcs.strain(input);

            fusion.test.assert(1666.67, output.accounting.strain.toFixed(2), "accounting.strain");
            fusion.test.assert(2435.26, output.accounting.max.toFixed(2), "accounting.max");
            fusion.test.assert(1282.37, output.accounting.min.toFixed(2), "accounting.min");
            fusion.test.assert(909.09, output.funding.strain.toFixed(2), "funding.strain");
            fusion.test.assert(973.14, output.funding.max.toFixed(2), "funding.max");
            fusion.test.assert(716.94, output.funding.min.toFixed(2), "funding.min");
            fusion.test.assert(476.19, output.gilts.strain.toFixed(2), "gilts.strain");
            fusion.test.assert(540.24, output.gilts.max.toFixed(2), "gilts.max");
            fusion.test.assert(284.04, output.gilts.min.toFixed(2), "gilts.min");
            fusion.test.assert(-192.15, output.impact.toFixed(2), "impact");

            console.info("fusion.test.insurance run complete");
        }
    }
}());