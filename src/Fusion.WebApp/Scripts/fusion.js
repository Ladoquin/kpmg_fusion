﻿var fusion = new (function() {
    
    $.Core = {}; // until all references are removed

    var that = this;
    var sessionExpiresOn;

    //************
    //* setup namespaces
    //************
    this.charts = {};
    this.analysis = {};
    this.calcs = {};

    //************
    //* constants
    //************
    this.loginUrl = "/account/login";
    
    //************
    //* bootstrapping
    //************
    $.ajaxSetup({
        cache: false,
        dataFilter: function (data, type) {
            refreshTimeout();
            if (data != null && type != "json" && data == "TIMEOUT") {
                window.location.href = that.loginUrl;
            };
            return data;
        }
    });

    this.showSchemeChangeLoadingCover = function () {
        $(".scheme-change-cover").show();
        var el = $(".scheme-change-cover .spinner-container .spinner")[0];
        setTimeout(function () {
            $(".scheme-change-cover .spinner-container").show();
            showSpinner(el);
        }, 2000);
    };

    //************
    //* global events
    //************
    $(document).ready(function () {
        // timeout
        refreshTimeout();
        if ($("input#disable-timeout").length === 0 || $("input#disable-timeout").val() !== "true") {
            setInterval(function () {
                if (sessionExpiresOn < Date.now()) {
                    $.post("/account/logoff", { }, function () {
                        window.location.href = that.loginUrl + "?returnUrl=" + window.location.pathname + "&timedout=1";
                    });
                };
            }, 60000);
        };
        //scheme change
        $("#schemeSelectorMenu li a").click(function () {
            if ($(this).data('selected') == false) {
                that.showSchemeChangeLoadingCover();
                $.post("/Account/ChangeSchemeJson", { schemeName: $(this).text() }, function (data) {
                    location = "/Home";
                });
            };
        });
    });

    // basis change
    // allow others to override and use own custom data, but come through here to manage the basisChangedEvent call
    this.basisChangedEvent = $.Callbacks();
    this.selectBasis = function (id, custom) {
        if (custom == null) {
            $.post("/Home/SetBasis", { masterBasisId: id }, function (data) {
                that.basisChangedEvent.fire(data.basis);
            });
        }
        else {
            that.basisChangedEvent.fire(custom.basis, custom);
        }
    };

    // for new journey plan: changing funding basis
    this.fundingBasisChangedEvent = $.Callbacks();
    this.selectJPFundingBasis = function (basis) {    
            that.fundingBasisChangedEvent.fire(basis);
    };

    //************
    //* helpers
    //************
    var refreshTimeout = function () {
        sessionExpiresOn = Date.now() + parseInt($("#timeout-value").val() || 10800000);
    }

})();
