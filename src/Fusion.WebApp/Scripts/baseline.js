﻿$(function () {
    $(document).ready(function () {

        fusion.page.init({ idx: 1, name: "baseline" });
        fusion.tabs.init();

        $(".ball").linkable();

        var donutHandler =
                new fusion.util.AssumptionsPairHandler({
                    liabilityChartDivId: "liabilityBreakdownPie",
                    assetChartDivId: "assetBreakdownPie",
                });

        var populateLdiTable = function (data) {
            if (data != null) {
                $("#hedged_currentBasis_interest_rate").html(Math.round(data.interestRate * 100) + '%');
                $("#hedged_currentBasis_inflation").html(Math.round(data.inflation * 100) + '%');
                $("#hedged_cashflows_interest_rate").html(Math.round(data.cashflowInterestRate * 100) + '%');
                $("#hedged_cashflows_inflation").html(Math.round(data.cashflowInflation * 100) + '%');

                // show div
                $("#ldiHedgingTableContainer").removeClass("hide");
            }
            else if (!$("#ldiHedgingTableContainer").hasClass("hide")) {
                // hide div
                $("#ldiHedgingTableContainer").addClass("hide");
            }
        };

        var liabilitytabler = new fusion.util.LiabilitiesTabler("liability-assumptions-table");
        var pensionIncreaseTabler = new fusion.util.LiabilitiesTabler("pension-increase-assumptions-table");

        var refresh = function () {

            $.getJSON('/Baseline/Get', function (data) {

                $("#valuation-date").html(Highcharts.dateFormat('%d/%m/%Y', data.current.anchorDate));

                $.Core.valuationResults.refresh(data);

                liabilitytabler.refresh(data.current.assumptions, true);
                pensionIncreaseTabler.refresh($.grep(data.current.pensionIncreases, function(pinc, i) { return pinc.IsVisible; }), true);

                var donutHelper = new fusion.util.DonutHelper();
                
                var liabilitySeries = donutHelper.getLiabilitySeriesData(data.current.liabilitiesBreakdown);

                fusion.charts.donutHandler().redraw(liabilitySeries, {
                        chartDivId: 'liabilityBreakdownPie',
                        height: 240,
                        marginTop: 20,
                        marginBottom: 20
                    },
                    true);

                if (data.current.assetsBreakdown != null && data.current.assetsBreakdown.length > 0) {
                    new fusion.util.AssetsBreakdownHelper().draw({
                        containerId: 'assetBreakdownPie',
                        synthetic: {
                            data: data.current.syntheticAssetInfo,
                            showBreakdown: true
                        },
                        categories: data.current.assetsBreakdown,
                        hideClasses: false,
                        showZeroEntries: false,
                        showFunds: true,
                        dimensions: {
                            height: 240,
                            marginTop: 20,
                            marginBottom: 20
                        },
                        borderColorMode: 'grey',
                        aligntment: 'bottom'
                    });
                }
                else {
                    $("#assetBreakdownPie").text("N/A");
                }

                populateLdiTable(data.ldiData);

            });
        }

        fusion.basisChangedEvent.add(function () { refresh(); });

        refresh();
    });
});
