﻿fusion.calcs.strain = function (input) {

    var pensioners = input.pensioners;
    var nonpensioners = input.nonpensioners;
    var pensionerLiabilityInsured = input.pensionerLiabilityInsured / 100;
    var nonpensionerLiabilityInsured = input.nonpensionerLiabilityInsured / 100;

    var liabilityAdjustmentFactors = {
        pensioner: {
            buyout: 1,
            accounting: 1.2,
            funding: 1.1,
            gilts: 1.05
        },
        nonpensioner: {
            buyout: 1,
            accounting: 1.4,
            funding: 1.4,
            gilts: 1.15
        }
    };

    var indicativeLiabilities = {
        pensioner: {
            buyout: pensioners / liabilityAdjustmentFactors.pensioner.buyout,
            accounting: pensioners / liabilityAdjustmentFactors.pensioner.accounting,
            funding: pensioners / liabilityAdjustmentFactors.pensioner.funding,
            gilts: pensioners / liabilityAdjustmentFactors.pensioner.gilts
        },
        nonpensioner: {
            buyout: nonpensioners / liabilityAdjustmentFactors.nonpensioner.buyout,
            accounting: nonpensioners / liabilityAdjustmentFactors.nonpensioner.accounting,
            funding: nonpensioners / liabilityAdjustmentFactors.nonpensioner.funding,
            gilts: nonpensioners / liabilityAdjustmentFactors.nonpensioner.gilts
        }
    };

    var indicativeStrains = {
        pensioner: {
            buyout: indicativeLiabilities.pensioner.buyout - indicativeLiabilities.pensioner.buyout,
            accounting: indicativeLiabilities.pensioner.accounting - indicativeLiabilities.pensioner.buyout,
            funding: indicativeLiabilities.pensioner.funding - indicativeLiabilities.pensioner.buyout,
            gilts: indicativeLiabilities.pensioner.gilts - indicativeLiabilities.pensioner.buyout
        },
        nonpensioner: {
            buyout: indicativeLiabilities.nonpensioner.buyout - indicativeLiabilities.nonpensioner.buyout,
            accounting: indicativeLiabilities.nonpensioner.accounting - indicativeLiabilities.nonpensioner.buyout,
            funding: indicativeLiabilities.nonpensioner.funding - indicativeLiabilities.nonpensioner.buyout,
            gilts: indicativeLiabilities.nonpensioner.gilts - indicativeLiabilities.nonpensioner.buyout
        }
    };

    var indicativeStrainsWithInsuredLiability = {
        pensioner: {
            buyout: indicativeStrains.pensioner.buyout * pensionerLiabilityInsured,
            accounting: indicativeStrains.pensioner.accounting * pensionerLiabilityInsured,
            funding: indicativeStrains.pensioner.funding * pensionerLiabilityInsured,
            gilts: indicativeStrains.pensioner.gilts * pensionerLiabilityInsured
        },
        nonpensioner: {
            buyout: indicativeStrains.nonpensioner.buyout * nonpensionerLiabilityInsured,
            accounting: indicativeStrains.nonpensioner.accounting * nonpensionerLiabilityInsured,
            funding: indicativeStrains.nonpensioner.funding * nonpensionerLiabilityInsured,
            gilts: indicativeStrains.nonpensioner.gilts * nonpensionerLiabilityInsured
        }
    };

    indicativeStrainsWithInsuredLiability.total = {
        buyout: indicativeStrainsWithInsuredLiability.pensioner.buyout + indicativeStrainsWithInsuredLiability.nonpensioner.buyout,
        accounting: indicativeStrainsWithInsuredLiability.pensioner.accounting + indicativeStrainsWithInsuredLiability.nonpensioner.accounting,
        funding: indicativeStrainsWithInsuredLiability.pensioner.funding + indicativeStrainsWithInsuredLiability.nonpensioner.funding,
        gilts: indicativeStrainsWithInsuredLiability.pensioner.gilts + indicativeStrainsWithInsuredLiability.nonpensioner.gilts
    };

    var impactOfCreditSpread = {
        pensioner: {
            discountBefore: 0.03,
            duration: 12
        },
        nonpensioner: {
            discountBefore: 0.033,
            duration: 25
        },
        total: {}
    };

    impactOfCreditSpread.pensioner.discountAfter = impactOfCreditSpread.pensioner.discountBefore + 0.005 / 3;
    impactOfCreditSpread.pensioner.adjustment = Math.pow((1 + impactOfCreditSpread.pensioner.discountBefore) / (1 + impactOfCreditSpread.pensioner.discountAfter), impactOfCreditSpread.pensioner.duration) -1;
    impactOfCreditSpread.pensioner.liabAndPerc = impactOfCreditSpread.pensioner.adjustment * pensioners * pensionerLiabilityInsured;

    impactOfCreditSpread.nonpensioner.discountAfter = impactOfCreditSpread.nonpensioner.discountBefore + 0.005 / 3;
    impactOfCreditSpread.nonpensioner.adjustment = Math.pow((1 + impactOfCreditSpread.nonpensioner.discountBefore) / (1 + impactOfCreditSpread.nonpensioner.discountAfter), impactOfCreditSpread.nonpensioner.duration) - 1;
    impactOfCreditSpread.nonpensioner.liabAndPerc = impactOfCreditSpread.nonpensioner.adjustment * nonpensioners * nonpensionerLiabilityInsured;

    impactOfCreditSpread.total.liabAndPerc = impactOfCreditSpread.pensioner.liabAndPerc + impactOfCreditSpread.nonpensioner.liabAndPerc;

    var output = {
        accounting: { strain: -1 * indicativeStrainsWithInsuredLiability.total.accounting },
        funding: { strain: -1 * indicativeStrainsWithInsuredLiability.total.funding },
        gilts: { strain: -1 * indicativeStrainsWithInsuredLiability.total.gilts }
    };

    output.accounting.max = output.accounting.strain - 4 * impactOfCreditSpread.total.liabAndPerc;
    output.accounting.min = output.accounting.strain + 2 * impactOfCreditSpread.total.liabAndPerc;

    output.funding.max = output.funding.strain - (1 / 3) * impactOfCreditSpread.total.liabAndPerc;
    output.funding.min = output.funding.strain + 1 * impactOfCreditSpread.total.liabAndPerc;

    output.gilts.max = output.gilts.strain - (1 / 3) * impactOfCreditSpread.total.liabAndPerc;
    output.gilts.min = output.gilts.strain + 1 * impactOfCreditSpread.total.liabAndPerc;

    output.impact = impactOfCreditSpread.total.liabAndPerc;

    return output;
};