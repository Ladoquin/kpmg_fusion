﻿(function ($) {

    $.fn.linkable = function () {
        $.each(this, function(idx, el) {
            if ($(el).data("url")) {
                $(el).click(function () {
                    window.location.href = $(el).data("url");
                });
            }
        });
        return this;
    };

}(jQuery));