﻿fusion.analysis.AbfPanel = function() {

    var that = this;
    var initialised = false;

    this.changed = $.Callbacks();
    var fundingDeficit = 0;

    var reset = function () {

        $.getJSON('/Json/ResetAbf', function (abfData) {
            that.refresh(abfData);
            that.changed.fire({ id: that.id, data: abfData });
        });
    };

    var setAbf = function () {
        var abf = {},
            finalAv;

        finalAv = $('#abfPercentageOfAbf').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalAv == null || finalAv == typeof 'undefined')
            finalAv = $('#abfPercentageOfAbf').slider("option", "value");
        abf.assetValue = finalAv;
        
        var json = {
            abfData: JSON.stringify(abf)
        };

        $.post('/Json/SetAbf', json, function (abfData) {
            that.refresh(abfData);
            that.changed.fire({ id: that.id, data: abfData });
        });
    };

    var initialise = function (abfData) {

        var updateCell = function (event, ui) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: ui.value
            });
        };

        var updateAbf = function (event, ui) {
            setAbf();
        };

        var maxAbfSliderVal = abfData.maxAbfSliderVal;
        var sliderIncrement = maxAbfSliderVal / 500;

        $('#abfInputTable .assumptionsSlider')
            .each(function () {
                $(this).slider({
                    animate: 400,
                    range: 'min',
                    min: $(this).data('min'),
                    max: maxAbfSliderVal,
                    value: 0,
                    step: sliderIncrement,
                    slide: updateCell,
                    stop: updateAbf,
                    create: function () { $(this).slider('setTextValueElement') },
                    elementSelector: $(this),
                    textElement: $(this).parent().siblings().last()
                });
            });

        $('#resetAbf').click(function () {
            reset();
        });

        initialised = true;
    };
    
    this.refresh = function (abfData) {

        if (!initialised) { initialise(abfData); }

        // set global variable for funding deficit
        fundingDeficit = abfData.fundingDeficit;

        $('#abfPercentageOfAbf').slider('value', abfData.valueOfAbf);
        $('#abfPercentageOfAbf').slider('setTextValueElement', abfData.valueOfAbf);

        $('#abfFundingDeficit').html(fusion.util.formatCurrency(abfData.fundingDeficit));
        $('#abfLengthOfRecoveryPlan').html(abfData.lengthOfRecoveryPlan + ' years');
        $('#abfAnnualPayments').html(fusion.util.formatCurrency(abfData.annualPayments));
        $('#abfNetCashPosition').html(fusion.util.formatCurrency(abfData.netCashPosition));
        $('#abfLengthOfAbf').html(abfData.lengthOfAbf + ' years');
        $('#abfAnnualContribution').html(fusion.util.formatCurrency(abfData.annualContribution));
        $('#abfNewNetCashPosition').html(fusion.util.formatCurrency(abfData.newNetCashPosition));
        $('#abfCumulativeCashSaving').html(fusion.util.formatCurrency(abfData.cumulativeCashSaving));

    };
};

