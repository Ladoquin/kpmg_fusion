/*
*	KPMG
*	solutions.js
*	Space01 Ltd. 2013
* 	author : Jeremy Robson
*/


$.Core.solutions ={
    init: function () {
        fusion.tabs.init();
        //fusion.lightbox.init();
        fusion.navigation.init();
        fusion.accordion.init();
        fusion.responsive.init();
        fusion.progresscycle.init();
	    this.showourawards();
	    this.solutionsdropdown();
	    this.countup();
	    this.equalise();
	    $(window).resize(function () {
	        $.Core.solutions.equalise();
	    });
	    //__mock up
	    $(window).load(function () {
	        if ($('.contents,.contents-full').hasClass('solutions')) {
	            $('.firstmenu li').removeClass("active");
	            $('.firstmenu li').eq(5).addClass("active");
	            $('.secondmenu').show();
	            $('.controlpanel').hide();

	            if ($('.contents,.contents-full').hasClass('risk')) {
	                $('.secondmenu li').removeClass("active");
	                $('.secondmenu li').eq(0).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('cash')) {
	                $('.secondmenu li').removeClass("active");
	                $('.secondmenu li').eq(1).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('efficiency')) {
	                $('.secondmenu li').removeClass("active");
	                $('.secondmenu li').eq(2).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('rewards')) {
	                $('.secondmenu li').removeClass("active");
	                $('.secondmenu li').eq(3).addClass("active");
	            }

	            if ($('.contents,.contents-full').hasClass('pensionschemeaccounting')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(0).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('assetbackedfunding')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(1).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('benefitchanges')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(2).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('communication')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(3).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('governanceandefficiency')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(4).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('insurancesolutions')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(5).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('investment')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(6).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('memberandbenefitdata')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(7).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('memberoptions')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(8).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('peoplepoweredperformance')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(9).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('ppflevy')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(10).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('schemefunding')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(11).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('sponsorcovenant')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(12).addClass("active");
	            }
	            else if ($('.contents,.contents-full').hasClass('taxsolutions')) {
	                $('#solutionsMenu li').removeClass("active");
	                $('#solutionsMenu li').eq(13).addClass("active");
	            }
	        }
	    });
	},
	equalise: function () {
	    if ($(".equalise-parent").length > 0) {
	        $(".equalise-parent").each(function () {
	            var container = $(this);
	            container.data("max-body-height", 0);
	            container.find(".equalise-item").each(function () {
	                if ($(this).height() > container.data("max-body-height")) {
	                    container.data("max-body-height", $(this).height());
	                }
	            });
	            container.find(".equalise-item").css("min-height", container.data("max-body-height"));

	        });
	    }
	},
	countup: function () {
	    if ($(".count-up").length > 0) {
	        $(".count-up").each(function () {
	            var that = $(this);
	            that.data("final", that.text());
	            that.data("done", false);
	            that.text("0");
	        });
	        $('.count-up').appear();
	        $(document.body).on('appear', '.count-up', function (e, $affected) {
	            $($affected).each(function () {
	                var that = $(this);
	                if (!that.data("done"))
	                {
	                    that.text("0");
	                    window.setTimeout(function () {
	                        var interval = setInterval(function () {
	                            //console.log(parseInt(that.data('final')));
	                            that.text(parseInt(that.text()) + 1);
	                            if (parseInt(that.text()) >= parseInt(that.data('final'))) {
	                                clearInterval(interval);
	                                that.text(parseInt(that.data('final')));
	                                that.data("done", true);
	                            }
	                        }, 40);

	                    }, 1000);

	                }
	            });
	        });
	    }
	},
	countupX: function () {
	    if ($(".count-up").length > 0) {
	        $(".count-up").each(function(){
	            var that = $(this);
	            that.data("final", that.text());
	            that.text("0");
	            var interval = setInterval(function () {
	                that.text(parseInt(that.text()) + 1);
	                if (parseInt(that.text()) == parseInt(that.data('final')))
	                {
	                    clearInterval(interval);
	                }
	            }, 25);
	        });
	    }
	},
	solutionsdropdown: function () {

	    $('#solutionsSelector').click(function () {
	        if ($('#solutionsMenu').is(":visible"))
	        {
	            $('#solutionsMenu').slideUp(100, function () { $('#solutionsSelector').removeClass("selected"); });
	            

	        }
	        else
	        {
	            $('#solutionsSelector').addClass("selected");
	            $('#solutionsMenu').slideDown(100, function () {  });
	            

	        }
	        return false;
	    });


	    $('body').click(function () {
	        $('#solutionsMenu').slideUp(100, function () { $('#solutionsSelector').removeClass("selected"); });
	    });


	},
	showourawards: function () {
	    if($(".see-our-awards-tab").length>0)
	    {
	        $(".see-our-awards-body").hide();
	        $(".see-our-awards-tab").click(function () {
	            if($(".see-our-awards-body").is(":visible"))
	            {
	                $(".see-our-awards-tab").addClass("see-our-awards-tab-down");
	                $(".see-our-awards-tab").removeClass("see-our-awards-tab-up");
	                $(".see-our-awards-body").slideUp("slow");

	            }
	            else
	            {
	                $(".see-our-awards-tab").addClass("see-our-awards-tab-up");
	                $(".see-our-awards-tab").removeClass("see-our-awards-tab-down");
	                $(".see-our-awards-body").slideDown("slow");

	            }

	        });
	    }
	}
};

$(document).ready(function () { $.Core.solutions.init(); });

window.onresize = function (event) {
    fusion.responsive.resized();
}
