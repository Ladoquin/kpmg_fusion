﻿(function () {

    var findSchemes = [
        new fusion.util.FindSchemeStrict($("div#find-schemes-all .schemeNameSearch")),
        new fusion.util.FindSchemeStrict($("div#find-schemes-restricted-only .schemeNameSearch"))];

    var selectors = {
        userType: "select[name=Role]",
        schemeSelect: ".find-scheme-btn",
        schemeRemove: ".remove-scheme",
        schemeList: "#scheme-list",
        hiddenSchemeList: "input[type=hidden][name=Schemes]",
        form: "form#user-form",
    };

    var hidePassword = $(selectors.userType).data("hidepassword");

    var selectedScheme = "";

    var setSchemesfromType = function () {
        $("input[name=FindSchemeName]").val("");
        var usertype = ($(selectors.userType).children("option:selected").val() || "").toLowerCase();
        $(".scheme-access-info").hide();
        $("#find-schemes-all").hide();
        $("#find-schemes-restricted-only").hide();
        $("#schemes-" + usertype).show();
        $("#restricted-label").hide();
        switch (usertype) {
            case "admin":
                $("#schemes").hide();
                addPassword();
                break;
            case "installer":
            case "consultant":
                $("#schemes").show();
                $("#find-schemes-restricted-only").show();
                $("#restricted-label").show();
                removePassword();
                break;
            case "adviser":
            case "client":
                $("#find-schemes-all").show();
                $("#schemes").show();
                removePassword();
                break;
        }
    };

    var removeSchemeFromPost = function (scheme) {
        var scheme = $(selectors.hiddenSchemeList).filter(function () { return $(this).val() === scheme; });
        scheme.remove();
    };

    var addSchemeToPost = function (scheme) {
        $(selectors.form).append($("<input type='hidden' name='Schemes' value=\"" + scheme + "\" />"));
    };

    var schemeSelected = function (e) {
        if (selectedScheme.length > 0) {
            if ($("span.scheme-name:contains('" + selectedScheme + "')").length === 0) {
                var div = $("#clonable-scheme-section").clone(true);
                div.removeAttr("id").removeAttr("style");
                $(".scheme-name", div).text(selectedScheme);
                $(selectors.schemeList).append(div);
                addSchemeToPost(selectedScheme);
                $(".schemeNameSearch").val("");
            };
        }
        e.preventDefault();
    };

    var schemeRemoved = function (e) {
        var div = $(this).closest("div");
        removeSchemeFromPost($(".scheme-name", div).text());
        div.remove();
        e.preventDefault();
    };

    var expandEditSection = function () {
        if ($("#find-user").is(":visible")) {
            $("#find-user").hide();
            $("#user-details-section").switchClass("right", "left-space-30pc", 100);
            $("#user-details-section").switchClass("left-space-30pc", "left", 200);
            $("#scheme-section").show();
            $("#user-form-container").addClass("left").addClass("hundred");
            $("#btn-home").hide();
            $("#btn-cancel").show();
            $(".changeSucceeded").hide();
        }
    };

    var init = function () {
        // usertype
        $(selectors.userType).change(function () {
            $(selectors.schemeList).find(".scheme-name").closest("div").remove();
            $(selectors.hiddenSchemeList).val("");
            setSchemesfromType();
        });
        setSchemesfromType();
        // scheme search
        $.each(findSchemes, function(key, value) { value.selected.add(function (scheme) { selectedScheme = scheme; }) });
        $.each(findSchemes, function (key, value) { value.reset.add(function () { selectedScheme = ""; }) });
        // scheme select
        $(selectors.schemeSelect).click(schemeSelected);
        // scheme delete
        $(selectors.schemeRemove).click(schemeRemoved);
        // section animation
        if ($("input[name=disable-expanding]").val() === "true") {
            $("input", $(selectors.form)).keydown(function () {
                expandEditSection();
            });
            $("select", $(selectors.form)).change(function () {
                expandEditSection();
            });
        }
        // set delete confirmation
        $("#dialog-user-delete-confirm").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            dialogClass: 'white-dialog',
            buttons: {
                "Yes": function () {
                    $(this).dialog("close");
                    $(selectors.form).append($("<input type='hidden' name='delete' value='true'/>"));
                    $(selectors.form).submit();
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            create: function (event, ui) {
                var widget = $(this).dialog("widget");
                $(".ui-dialog-titlebar-close .ui-button-text", widget).text("");
                var buttons = $(".ui-dialog-buttonpane", widget).find("button");
                $(buttons[1]).addClass("btn-dialog-cancel");
            },
            open: function () {
                $(this).parent().find('button:nth-child(2)').focus();
            }
        });
        $("#delete_user").click(function (e) {
            $("#dialog-user-delete-confirm").dialog("open");
            e.preventDefault();
        });
    };

    var removePassword = function () {
        // check data-hidepassword 
        if (hidePassword) {
            // hide div
            $("#manual-password").hide();
            // clear value and enable
            $("#Password").val('');
            $("#Password").attr('disabled', 'disabled');
        }

    }

    var addPassword = function () {
        if (hidePassword) {
            // show div
            $("#manual-password").show();
            // clear value and disable
            $("#Password").val('');
            $("#Password").removeAttr("disabled");
        }
    }

    $(document).ready(function () {
        init();
    });

}());
