﻿$(function () {

    $('.filebutton').click(function (e) {
        var input = $(this).siblings().filter('.fileinput');
        input.click();
        e.preventDefault();
    });

    $(".fileinput")
        .css({ height: 0, width: 0, visibility: 'hidden' })
        .change(function (e) {
            var file = $(this).val();
            var fileview = $(this).siblings().filter('.fileview');

            if (file === "") {
                fileview.val(fileview.attr('data-ghost')).css({ "color": "#ccc" });
            } else {
                var fileName = file.split("\\");
                fileview.val(fileName[fileName.length - 1]).css({ "color": "#000" });
            }
            e.preventDefault();
        });


    $('.fileview')
        .each(function() {
            if ($(this).val() === "" || $(this).val() === $(this).attr("data-ghost")) {
                $(this).attr("value", $(this).attr("data-ghost")).css({ "color": "#ccc" });
            } else {
                $(this).css({ "color": "#000" });
            }
        });

});




