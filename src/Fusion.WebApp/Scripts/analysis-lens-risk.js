﻿fusion.analysis.riskLens = function() {

    var initialised = false;
    var varWaterfallLens = new VarWaterfallLens();
    var varFunnelLens = new VarFunnelLens();

    var initialise = function() {
        
        varWaterfallLens.initialiseLens('varWaterfallChartDiv');
        varFunnelLens.initialiseLens('varFunnelChartDiv');

        $("#riskLensTabs").tabs({
            activate: function (event, ui) {
                if (ui.newPanel.attr('id') == 'varWaterfallTab') {
                    varFunnelLens.deactivateLens();
                    varWaterfallLens.activateLens();
                }
                else if (ui.newPanel.attr('id') == 'varFunnelTab') {
                    varWaterfallLens.deactivateLens();
                    varFunnelLens.activateLens();
                }
            }
        });

        initialised = true;
    };

    this.activate = function () {

        if (!initialised) {
            initialise();
        };

        if ($("#riskLensTabs").tabs("option", "active") == 0) {
            varWaterfallLens.activateLens();
            varFunnelLens.deactivateLens();
        }
        else {
            varWaterfallLens.deactivateLens();
            varFunnelLens.activateLens();
        }
    };

    this.deactivate = function() {
        varWaterfallLens.deactivateLens();
        varFunnelLens.deactivateLens();
    };

    this.update = function () {
        varWaterfallLens.updateLens();
        varFunnelLens.updateLens();
    };

};



