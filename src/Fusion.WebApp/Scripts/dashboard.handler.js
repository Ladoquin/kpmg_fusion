/*
*	KPMG
*	dashboard.handler.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This invokes the dashboard events*/

fusion.dashboardHandler ={
	init: function(){
		this.bindEvents();	
	},
	bindEvents: function(){
		var that = this;

		$('.controlpanel .cog').click(function() {			
			if($('.paneconsole').hasClass("open")){
				that.closeDashboard();
			}
			else{
				that.openDashboard();
			}			
		});		
	},
	openDashboard: function () {
	    $('.controlpanel .cog').removeClass("closed").addClass("open");
	    $('.paneconsole').slideDown(400, function () {
			$('.paneconsole').removeClass("close").addClass("open");
		});	
	},
	closeDashboard: function () {
	    $('.controlpanel .cog').removeClass("open").addClass("closed");
	    $('.paneconsole').slideUp(400, function () {
			$('.paneconsole').removeClass("open").addClass("close");
		});		
	}
};