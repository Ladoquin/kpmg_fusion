﻿// ****************
// * Sets up autocomplete on a scheme search box
// * which only allows submission of an entry matched in the data source.
// ****************
fusion.util.FindSchemeStrict = function (searchbox) {

    var that = this;

    this.selected = $.Callbacks();
    this.reset = $.Callbacks();

    $.getJSON(searchbox.attr('data-schemesource'), function (data) {
        searchbox.autocomplete({
            source: data,
            minLength: 2,
            select: function (event, ui) {
                that.selected.fire(ui.item.value);
            },
            response: function (event, ui) {
                var matches = ui.content.filter(function (i) { return i.value == searchbox.val(); });
                if (matches.length === 1) {
                    // user has typed in full scheme name so is a valid selection
                    that.selected.fire(matches[0].value);
                };
            }
        });
    });

    searchbox.keydown(function () { that.reset.fire(); });

};
