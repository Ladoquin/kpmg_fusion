﻿$(function () {
    $("#loginpane").on("click", "#login-btn", function (e) {

        e.preventDefault();
        e.stopPropagation();

        fusion.util.form.hideWarnings();

        var returnUrl = getReturnUrl();

        var token = $('input[name="__RequestVerificationToken"]').val();
        $.ajax({
            url: '/Account/Login',
            type: 'POST',
            data: {
                __RequestVerificationToken: token,
                UserName: $("#UserName").val(),
                Password: $("#Password").val()
            },
            success: function (result) {
                if (result.response) {
                    $("#spinner-container").show();
                    showSpinner($("#spinnerDiv")[0]);
                    window.location.href = returnUrl.length == 0 ? "/Home" : returnUrl;
                }
                else {
                    $("#login-panel").replaceWith(result.partialView);
                };
            }
        });
        return false;
    });

    getReturnUrl = function () {
        var returnUrl = "";
        var href = window.location.href.toLowerCase();
        if (href.indexOf("returnurl") > 0) {
            returnUrl = $.grep(href.split("?")[1].split("&"), function (x) {
                return x.length > "returnurl=".length && x.substring(0, "returnurl=".length) == "returnurl=";
            })
            [0].split("=")[1];

            if (returnUrl.substring(0, 1) == "/")
                returnUrl = returnUrl.substring(1);
            else if (returnUrl.substring(0, 3) == "%2f") // encoded '/'
                returnUrl = returnUrl.substring(3);

            returnUrl = "/" + returnUrl;
        }
        return returnUrl;
    };
});
