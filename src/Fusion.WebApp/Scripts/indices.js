﻿$(function () {

    $("#save-indices-btn").click(function () {
        $("#xml-validation-errors").fadeOut();
    });

    $("table#indices-all tr td.index").on("click", function () {
        var td = $(this)[0];
        var indexId = $(this).data("indexid");
        if ($(td).find("table").length == 0) {
            $.get("/Indices/ViewAllIndexValues", { id: indexId }, function (content) {
                $(td).append(content);
                $("img", td).attr("src", "/Images/collapse.png");
                $("img", td).addClass("expanded");
            });
        }
        else {
            $(td).find("table").toggle();
            if ($("img", td).hasClass("expanded")) {
                $("img", td).attr("src", "/Images/expand.png");
                $("img", td).removeClass("expanded");
            }
            else {
                $("img", td).attr("src", "/Images/collapse.png");
                $("img", td).addClass("expanded");
            };
        }
    });

});
