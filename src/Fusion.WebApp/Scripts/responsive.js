/*
*	KPMG
*	responsive.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This helps with responsive aspects of the site. The grid system will ensure the images are scaled properly.*/

fusion.responsive ={
	init: function(){

		var that = this;
		this.hasImgLoaded($('.grid img'), function(msg){
			that.retainMaxImgWidthOnGrid();
		});
		that.zoomipadportrait();

	},
	hasImgLoaded: function($img, callback){

			var img_index = 0;
			var img_length = $img.length;
			$img.each(function() {
				//Hidden images will not trigger the load event, duplicate images will not trigger either				
				if(window.getComputedStyle(this).display == 'none' || this.complete) {
					img_length--;
				}
				else {
					$(this).on('load error abort', function() {
						img_index++;
						if(img_index == img_length) {
							callback("contents fully loaded in");
						}
					});
				}
			});
			
			if(!img_length) {
				callback("contents fully loaded in");
			}
	},
	getOriginalWidthOfImg: function(img_element) {
	    var t = new Image();
	    t.src = (img_element.getAttribute ? img_element.getAttribute("src") : false) || img_element.src;
	    return t.width;
	},	
	retainMaxImgWidthOnGrid: function(){
		var that = this;

		$(window).load(function() {
			//everything is loaded
			$('.grid img').each(function() {

				var maxWidth = that.getOriginalWidthOfImg($(this)[0]);
				if(maxWidth == 0){
					maxWidth = "100%";
				}

				$(this).css("max-width", maxWidth);
				$(this).css("width", "100%");
			});
		});		

	},	
	resized: function(){
		this.retainMaxImgWidthOnGrid();
	},
	zoomipadportrait: function () {
	    // BUG orientation portrait/lanscape IOS //
	    //if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
	    if (navigator.userAgent.match(/iPad/i)) {
	            var viewportmeta = document.querySelector('meta[name="viewport"]');
	        if (viewportmeta) {
	            viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
	            document.addEventListener('orientationchange', function () {
	                viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1';
	            }, false);
	        }
	    }
	}



};