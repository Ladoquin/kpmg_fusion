﻿fusion.analysis.InvestmentStrategyPanel = function () {

    var that = this;
    var initialised = false;
    var proportions = null;
    var dummySliderOpts = { slide: function () { return false; } };
    var cashInjectionAmount;
    var cashAssetAmount;
    var enableLdi = true;
    var totalExistingAssets;

    this.changed = $.Callbacks();

    var initialise = function () {
        $('#resetAssetMix').click(function () {
            that.reset();
        });

        $('#enableLdi').click(function () {
            enableLdi = $(this).is(':checked');
            that.setAdvancedOptions();
        });

        initialised = true;
    };

    var refreshForBasis = function (varData) {

        var maxTotal = 1;

        $("#investmentStrategy_refGiltYieldRow").toggle(varData.setup.giltYieldLabel != null);
        $("#investmentStrategy_refGiltYieldRow th").html(varData.setup.giltYieldLabel);

        var assetMixData = varData.setup.assetClasses;

        var updateAdvancedTabCell = function (event, ui) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: ui.value
            });
        };

        var updateAdvancedTabCellWithLowerLimit = function (event, ui) {
            var flag = true;
            $(this).parent().parent().find('.slider-text-value').data('finalValue', null);
            var val = ui.value;
            var increment = $(this).data("increment");
            var lowerlimit = $(this).data("lowerlimit");            
            if (val < lowerlimit) {
                if (val + increment > lowerlimit) {
                    val = lowerlimit;
                    flag = false; //to prevent slider being positioned at step below lower limit messing up further slides down
                }
                else {
                    return false;
                }
            }
            $(this).slider("option", "value", val);
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: val
            });
            return flag;
        };

        var updateCell = function (event, ui) {

            var assetClassId = $(this).data('classid');

            //need to see if we have a value that overrides the slider step value
            var finalValue = $(ui.handle).closest("tr").find('.slider-text-value').data('finalValue');

            if (finalValue == null || finalValue == typeof 'undefined')
                finalValue = ui.value;

            if (assetClassId > 0 && assetClassId != $("#assetMixTable .assumptionsSlider:last").data("classid")) {
                var veto = reBalance(assetClassId, finalValue / totalExistingAssets, maxTotal);
                var aboveTotal = 0;
                for (var i = 0; i < proportions.length; i++) {
                    var slider = $('.assumptionsSlider[data-classid=' + proportions[i].id + ']');
                    slider
                        .slider('value', proportions[i].proportion * totalExistingAssets)
                        .parent().siblings(".values:first").html(fusion.util.formatPercentage(proportions[i].proportion, 1));

                    //slider is based on proportions which are then calculated for the currency in million field
                    slider.slider("updateSliderValue", {
                        el: slider,
                        value: proportions[i].proportion * totalExistingAssets
                    });

                    slider.find('.ui-slider-handle').css('opacity', aboveTotal >= maxTotal ? '0.3' : '1');

                    aboveTotal += proportions[i].proportion;
                }
                $("#assetMixTable").find(".ui-slider-handle:last").css('opacity', '0.3');
                return !veto;
            }
            else {
                return false;
            }
        };

        var initCashInjectionSlider = function () {

            var updateCashInjectionCell = function (event, ui) {
                var block = false;
                var val = ui.value;
                if (val < cashInjectionAmount) {
                    if (cashInjectionAmount - val > cashAssetAmount) {
                        val = cashInjectionAmount - cashAssetAmount;
                        block = true;
                    }
                }
                $(this).slider("updateSliderValue", {
                    el: $(this),
                    value: val
                });
                if (block) {
                    return false;
                }
            };

            //Deliberately no step value - it does not work properly with the editable text box due to differnt rounding
            $('#cashInjectionSlider').slider({
                animate: 400,
                range: 'min',
                min: 0,
                max: varData.setup.cashInjectionMax,
                value: varData.cashInjection,
                slide: updateCashInjectionCell,
                stop: function (event, ui, scope) {
                    that.setCashInjection();
                },
                elementSelector: $('#cashInjectionSlider'),
                textElement: $('#cashInjectionSlider').parent().siblings().last()
            });
            $('#cashInjectionSlider').slider('setTextValueElement');
        };
        initCashInjectionSlider();

        proportions = [];
        var aboveTotal = 0;
        var assetMixTable = $('#assetMixTable');
        $("tbody tr:not(.greyed-out-row)", assetMixTable).hide();
        for (var i = 0; i < assetMixData.length; i++) {
            if (assetMixData[i].Key > 0) {
                var tr = $("tr.assetmix-" + assetMixData[i].Key),
                    proportion = $.grep(assetMixData, function (ac) { return ac.Key == assetMixData[i].Key; })[0].Proportion,
                    value = proportion * totalExistingAssets;
                tr.show();
                tr.find("th").text(assetMixData[i].Label);
                var slider = tr.find(".assumptionsSlider");
                slider.slider({
                    animate: 400,
                    range: 'min',
                    min: 0,
                    max: totalExistingAssets,
                    value: value,
                    step: totalExistingAssets / 100,
                    slide: function (event, ui) {
                        $(ui.handle).closest("tr").find('.slider-text-value').data('finalValue', null);
                        return updateCell.call(this, event, ui);
                    },
                    stop: function (event, ui, scope) {
                        $(ui.handle).closest("tr").find('.slider-text-value').data('finalValue', null);
                        updateCell.call(scope || this, event, ui);
                        that.setAssetMix();
                    },
                    elementSelector: slider,
                    textElement: slider.parent().siblings().last().prev()
                });
                slider.slider('setTextValueElement');

                if (slider.length == 1 && !slider.hasClass("ignore")) {
                    proportions.push({ id: assetMixData[i].Key, proportion: assetMixData[i].Proportion });
                }
                slider.data("original", assetMixData[i].Proportion * totalExistingAssets);
                slider.data("original-tooltip", assetMixData[i].Proportion);
                slider.find('.ui-slider-handle').css('opacity', aboveTotal >= maxTotal ? '0.3' : '1');
                aboveTotal += assetMixData[i].Proportion;
            }
        };
        $("#assetMixTable").find(".ui-slider-handle:last").css('opacity', '0.3');
        $("#assetMixTable").find(".ui-slider-handle:last").closest('tr').find('.assumptionsSlider').each(function () {
            $(this).slider('disableEditableElement', this);
        });

        $(".assumptionsSlider.ignore").slider(dummySliderOpts).find('.ui-slider-handle').css('opacity', '1');

        var $syntheticEquitySlider = $("#assets-synthetic-growth-equity");
        $syntheticEquitySlider.slider({
            animate: 400,
            range: 'min',
            min: $syntheticEquitySlider.data('min'),
            max: $syntheticEquitySlider.data('max'),
            value: varData.syntheticEquity,
            step: $syntheticEquitySlider.data('increment'),
            slide: updateAdvancedTabCell,
            stop: function (event, ui, scope) {
                that.setAdvancedOptions();
            },
            elementSelector: $syntheticEquitySlider,
            textElement: $syntheticEquitySlider.parent().siblings().last()
        });
        $syntheticEquitySlider.slider('setTextValueElement');

        var $syntheticCreditSlider = $("#assets-synthetic-credit");
        $syntheticCreditSlider.slider({
            animate: 400,
            range: 'min',
            min: $syntheticCreditSlider.data('min'),
            max: $syntheticCreditSlider.data('max'),
            value: varData.syntheticCredit,
            step: $syntheticCreditSlider.data('increment'),
            slide: updateAdvancedTabCell,
            stop: function (event, ui, scope) {
                that.setAdvancedOptions();
            },
            elementSelector: $syntheticCreditSlider,
            textElement: $syntheticCreditSlider.parent().siblings().last()
        });
        $syntheticCreditSlider.slider('setTextValueElement');

        var $hedgingInterestSlider = $("#liability-hedging-interest-rates");
        $hedgingInterestSlider.slider({
            animate: 400,
            range: 'min',
            min: $hedgingInterestSlider.data('min'),
            max: $hedgingInterestSlider.data('max'),
            value: varData.hedgingInterest,
            step: $hedgingInterestSlider.data('increment'),
            slide: updateAdvancedTabCellWithLowerLimit,
            stop: function (event, ui, scope) {
                that.setAdvancedOptions();
            },
            elementSelector: $hedgingInterestSlider,
            textElement: $hedgingInterestSlider.parent().siblings().last()
        });
        $hedgingInterestSlider.slider('setTextValueElement');
        $hedgingInterestSlider.data('original', varData.setup.hedgingInterest);

        var $hedgingInflationSlider = $("#liability-hedging-inflation");
        $hedgingInflationSlider.slider({
            animate: 400,
            range: 'min',
            min: $hedgingInflationSlider.data('min'),
            max: $hedgingInflationSlider.data('max'),
            value: varData.hedgingInflation,
            step: $hedgingInflationSlider.data('increment'),
            slide: updateAdvancedTabCellWithLowerLimit,
            stop: function (event, ui, scope) {
                that.setAdvancedOptions();
            },
            elementSelector: $hedgingInflationSlider,
            textElement: $hedgingInflationSlider.parent().siblings().last()
        });
        $hedgingInflationSlider.slider('setTextValueElement');
        $hedgingInflationSlider.data('original', varData.setup.hedgingInflation);
    };

    var populateHedgeBreakdownTable = function (currentBasisName, hedgingBreakdown) {

        if (hedgingBreakdown == null)
            return;

        $("table#breakdown-of-hedge tr th#breakdown-of-hedge-current-title").html(currentBasisName);
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-physical-assets td:eq(0)").html(fusion.util.formatPercentage(hedgingBreakdown.CurrentBasis.Interest.PhysicalAssets, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-physical-assets td:eq(1)").html(fusion.util.formatPercentage(hedgingBreakdown.CurrentBasis.Inflation.PhysicalAssets, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-physical-assets td:eq(2)").html(fusion.util.formatPercentage(hedgingBreakdown.Cashflows.Interest.PhysicalAssets, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-physical-assets td:eq(3)").html(fusion.util.formatPercentage(hedgingBreakdown.Cashflows.Inflation.PhysicalAssets, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-buyin td:eq(0)").html(fusion.util.formatPercentage(hedgingBreakdown.CurrentBasis.Interest.Buyin, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-buyin td:eq(1)").html(fusion.util.formatPercentage(hedgingBreakdown.CurrentBasis.Inflation.Buyin, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-buyin td:eq(2)").html(fusion.util.formatPercentage(hedgingBreakdown.Cashflows.Interest.Buyin, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-buyin td:eq(3)").html(fusion.util.formatPercentage(hedgingBreakdown.Cashflows.Inflation.Buyin, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-ldi-overlay td:eq(0)").html(fusion.util.formatPercentage(hedgingBreakdown.CurrentBasis.Interest.LDIOverlay, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-ldi-overlay td:eq(1)").html(fusion.util.formatPercentage(hedgingBreakdown.CurrentBasis.Inflation.LDIOverlay, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-ldi-overlay td:eq(2)").html(fusion.util.formatPercentage(hedgingBreakdown.Cashflows.Interest.LDIOverlay, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-ldi-overlay td:eq(3)").html(fusion.util.formatPercentage(hedgingBreakdown.Cashflows.Inflation.LDIOverlay, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-total td:eq(0)").html(fusion.util.formatPercentage(hedgingBreakdown.CurrentBasis.Interest.Total, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-total td:eq(1)").html(fusion.util.formatPercentage(hedgingBreakdown.CurrentBasis.Inflation.Total, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-total td:eq(2)").html(fusion.util.formatPercentage(hedgingBreakdown.Cashflows.Interest.Total, 0));
        $("table#breakdown-of-hedge tr#breakdown-of-hedge-total td:eq(3)").html(fusion.util.formatPercentage(hedgingBreakdown.Cashflows.Inflation.Total, 0));
    };

    var resetAssetMixSliderRanges = function (varData) {
        $.each(varData.setup.assetClasses, function (index, value) {
            $("tr.assetmix-" + value.Key).find(".assumptionsSlider").slider("option", {
                max: varData.totalExistingAssets,
                step: varData.totalExistingAssets / 100
            });
        });
    };

    this.reset = function () {
        $.getJSON('/Json/ResetInvestmentStrategy', function (varData) {
            resetAssetMixSliderRanges(varData);
            that.refresh(varData);
            that.changed.fire({ id: that.id, data: varData });
        });
    },

    this.setCashInjection = function () {
        var finalCashInjection = $('#cashInjectionSlider').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalCashInjection == null || finalCashInjection == typeof 'undefined')
            finalCashInjection = $('#cashInjectionSlider').slider("option", "value");

        $.post('/Json/SetInvestmentStrategyCashInjection', {
            cashInjection: finalCashInjection
        },
        function (varData) {
            resetAssetMixSliderRanges(varData);
            that.refresh(varData);
            that.changed.fire({
                id: that.id, data: varData
            });
        });
    };

    this.setAdvancedOptions = function () {
        var finalSyntheticEquity = $('#assets-synthetic-growth-equity').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalSyntheticEquity == null || finalSyntheticEquity == typeof 'undefined')
            finalSyntheticEquity = $('#assets-synthetic-growth-equity').slider("option", "value");

        var finalSyntheticCredit = $('#assets-synthetic-credit').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalSyntheticCredit == null || finalSyntheticCredit == typeof 'undefined')
            finalSyntheticCredit = $('#assets-synthetic-credit').slider("option", "value");

        var finalHedgingInterest = $('#liability-hedging-interest-rates').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalHedgingInterest == null || finalHedgingInterest == typeof 'undefined')
            finalHedgingInterest = $('#liability-hedging-interest-rates').slider("option", "value");

        var finalHedgingInflation = $('#liability-hedging-inflation').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalHedgingInflation == null || finalHedgingInflation == typeof 'undefined')
            finalHedgingInflation = $('#liability-hedging-inflation').slider("option", "value");

        $.post('/Json/SetInvestmentStrategyAdvancedOptions', {
            syntheticEquity: finalSyntheticEquity,
            syntheticCredit: finalSyntheticCredit,
            enableLdi: enableLdi,
            hedgingInterest: finalHedgingInterest,
            hedgingInflation: finalHedgingInflation
        },
        function (varData) {
            that.refresh(varData);
            that.changed.fire({
                id: that.id, data: varData
            });
        });
    };

    this.setAssetMix = function () {
        $.post('/Json/SetInvestmentStrategyAssetMix', {
            assetMix: JSON.stringify(proportions)
        },
        function (varData) {
            that.refresh(varData);
            that.changed.fire({
                id: that.id, data: varData
            });
        });
    };

    var reBalance = function (assetClassId, newProportion, maxTotal) {

        var aboveTotal = 0;

        var belowProportions = [];
        var belowTotal = 0;

        var below = false;
        var veto = false;

        for (var i = 0; i < proportions.length; i++) {

            var iProportion = proportions[i];

            if (iProportion.id == assetClassId) {
                if (aboveTotal + newProportion > maxTotal) {
                    newProportion = maxTotal - aboveTotal;
                    veto = true;
                }
                iProportion.proportion = newProportion;
                aboveTotal += iProportion.proportion;
                below = true;
            } else if (below) {
                belowProportions.push(iProportion);
                belowTotal += iProportion.proportion;
            } else {
                aboveTotal += iProportion.proportion;
            }
        }

        var balancedBelowTotal = maxTotal - aboveTotal;

        if (balancedBelowTotal == 0) {
            for (i = 0; i < belowProportions.length; i++) {
                belowProportions[i].proportion = 0;
            }
        } else {

            if (belowTotal == 0) {
                for (i = 0; i < belowProportions.length; i++) {
                    belowProportions[i].proportion = balancedBelowTotal / belowProportions.length;
                }

            } else {
                var scaler = balancedBelowTotal / belowTotal;

                for (i = 0; i < belowProportions.length; i++) {
                    belowProportions[i].proportion *= scaler;
                }
            }
        }

        return veto;
    };

    this.refresh = function (varData) {

        totalExistingAssets = varData.totalExistingAssets;

        if (!initialised) {
            initialise();
            refreshForBasis(varData);
        }

        var assetMixTable = $('#assetMixTable');
        proportions = [];
        var maxTotal = 1;

        $("#investmentStrategy_refGiltYieldRow td:last").html((varData.giltYield * 100).toFixed(2) + '%');

        var aboveTotal = 0;

        cashAssetAmount = varData.cashAssetAmount;
        cashInjectionAmount = varData.cashInjection;

        $('#cashInjectionSlider').slider("setMin", Math.max(0, cashInjectionAmount - cashAssetAmount), $('#cashInjectionSlider'));
        $('#cashInjectionSlider').slider('value', cashInjectionAmount);
        $('#cashInjectionSlider').slider("setTextValueElement", cashInjectionAmount);

        for (var i = 0; i < varData.setup.assetClasses.length; i++) {

            var assetClass = $.grep(varData.assetsProportions, function (ac) {
                return ac.Key == varData.setup.assetClasses[i].Key;
            })[0];

            var tr = $("tr.assetmix-" + assetClass.Key, assetMixTable);

            var prop = Math.max(assetClass.Proportion, 0);

            var slider = tr.find(".assumptionsSlider");
            if (slider.length == 1 && !slider.hasClass("ignore")) {
                proportions.push({ id: assetClass.Key, proportion: prop });
            };
            slider.slider('value', prop * totalExistingAssets);
            slider.find('.ui-slider-handle').css('opacity', aboveTotal >= maxTotal ? '0.3' : '1');

            $("td.values:first", tr).html(fusion.util.formatPercentage(prop, 1));

            slider.slider("updateSliderValue", {
                el: slider,
                value: prop * totalExistingAssets
            });

            aboveTotal += assetClass.Proportion;
        }
        $("#assetMixTable").find(".ui-slider-handle:last").css('opacity', '0.3');

        $('#assetMixBuyin').html(varData.buyin != null ? fusion.util.formatCurrency(varData.buyin) : "-");
        $('#assetMixNewABF').html(varData.abf != null ? fusion.util.formatCurrency(varData.abf) : "-");
        $('#assetMixExistingProportion').html(fusion.util.formatPercentage(1 - varData.assetMixTotalOtherProportion, 1));
        $('#assetMixTotalOtherProportion').html(fusion.util.formatPercentage(varData.assetMixTotalOtherProportion, 1));
        $('#assetMixTotalOther').html(fusion.util.formatCurrency(varData.totalOtherAssets));
        $('#assetMixTotalAssets').html(fusion.util.formatCurrency(varData.totalAssets));
        $("#assetmix-subtotal").text(fusion.util.formatCurrency(totalExistingAssets));

        // synthetic assets
        $('#assetMixTotalSynthetic').html(fusion.util.formatCurrency(varData.syntheticAssetInfo.TotalAmount));
        $('#assetMixTotalSyntheticPercentage').html(fusion.util.formatPercentage(varData.syntheticAssetInfo.TotalProportion, 1));

        $('#assets-synthetic-growth-equity').slider('value', varData.syntheticEquity);
        $('#assets-synthetic-growth-equity').slider("setTextValueElement", varData.syntheticEquity);

        $('#assets-synthetic-credit').slider('value', varData.syntheticCredit);
        $('#assets-synthetic-credit').slider("setTextValueElement", varData.syntheticCredit);        

        enableLdi = varData.ldiEnabled;

        var hedgingBreakdown = $.parseJSON(varData.hedgingBreakdown);
        var cashflowsInterestTotal = hedgingBreakdown == null ? .0 : hedgingBreakdown.Cashflows.Interest.Total;
        var cashflowsInflationTotal = hedgingBreakdown == null ? .0 : hedgingBreakdown.Cashflows.Inflation.Total;

        var $hedgingInterestSlider = $("#liability-hedging-interest-rates");
        var $hedgingInflationSlider = $("#liability-hedging-inflation");

        $hedgingInterestSlider.slider('option', 'value', enableLdi ? varData.hedgingInterest : cashflowsInterestTotal); //use 'option', 'value' (instead of just 'value') to allow a non-step value to be set
        $hedgingInterestSlider.data('original', varData.setup.hedgingInterest);
        $hedgingInterestSlider.slider('setTextValueElement', enableLdi ? varData.hedgingInterest : cashflowsInterestTotal);
        $hedgingInterestSlider.slider("setMin", varData.hedgingInterestLowerLimit, $hedgingInterestSlider);
        $hedgingInterestSlider.data('lowerlimit', varData.hedgingInterestLowerLimit);

        $hedgingInflationSlider.slider('option', 'value', enableLdi ? varData.hedgingInflation : cashflowsInflationTotal); //use 'option', 'value' (instead of just 'value') to allow a non-step value to be set
        $hedgingInflationSlider.data('original', varData.setup.hedgingInflation);
        $hedgingInflationSlider.slider('setTextValueElement', enableLdi ? varData.hedgingInflation : cashflowsInflationTotal);
        $hedgingInflationSlider.slider("setMin", varData.hedgingInflationLowerLimit, $hedgingInflationSlider);
        $hedgingInflationSlider.data('lowerlimit', varData.hedgingInflationLowerLimit);

        if (!varData.hedgingInputsEnabled) {
            fusion.util.disableSlider($hedgingInterestSlider, true);
            fusion.util.disableSlider($hedgingInflationSlider, true);
        }
        else {
            fusion.util.enableSlider($hedgingInterestSlider);
            fusion.util.enableSlider($hedgingInflationSlider);
        }

        var interestTotal = fusion.util.formatPercentage(hedgingBreakdown == null ? 0 : hedgingBreakdown.CurrentBasis.Interest.Total, 0);
        var inflationTotal = fusion.util.formatPercentage(hedgingBreakdown == null ? 0 : hedgingBreakdown.CurrentBasis.Inflation.Total, 0);
        $("#liability-hedging-interest-rates").closest("tr").find(".leading-hedging-cashflows").html(interestTotal);
        $("#liability-hedging-inflation").closest("tr").find(".leading-hedging-cashflows").html(inflationTotal);
        populateHedgeBreakdownTable(varData.basisName, hedgingBreakdown);

        $('#enableLdi').prop('checked', enableLdi);

        setTimeout(function () {
            that.refreshChart(varData);
        }, 100);
    };

    this.refreshChart = function (varData) {

        $('#originalAssetReturn').html((varData.weightedReturnParameters.CurrentWarRelativeToGilts * 100).toFixed(2) + '%');
        $('#investmentStrategryWeightedAssetReturn').html((varData.weightedReturnParameters.WeightedAssetReturn * 100).toFixed(2) + '%');
        $('#changeInExpectedReturn').html((varData.weightedReturnParameters.ChangeInExpectedReturn * 100).toFixed(2) + '%');
        $('#investmentStrategyWeightedDiscountRateRelative').html((varData.weightedReturnParameters.WdrRelativeToGilts * 100).toFixed(2) + '%');

        new fusion.util.AssetsBreakdownHelper().draw({
            containerId: 'assetMixPie',
            synthetic: {
                data: varData.syntheticAssetInfo
            },
            categories: varData.assetsBreakdown,
            hideClasses: false,
            showZeroEntries: true,
            showFunds: false,
            dimensions: {
                marginTop: 30,
                marginBottom: 30
            },
            borderColorMode: 'blue',
            alignment: 'left'
        });
    };
};
