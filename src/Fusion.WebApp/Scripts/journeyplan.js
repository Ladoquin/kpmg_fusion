﻿
var JourneyPlanClass = function() {

    this.chart = null;

    this.hideAfter = true;

    this.refresh = function (journeyPlanData, revealAfter) {

        var before = [];
        var after = [];
        var categories = [];

        for (var i = 0; i < journeyPlanData.length; i++) {
            before.push(journeyPlanData[i].before);
            after.push(journeyPlanData[i].after);
            categories.push(journeyPlanData[i].year);
        }

        this.chart.xAxis[0].setCategories(categories);

        if (this.chart.series[0].data.length != journeyPlanData.length) {
            this.chart.series[1].remove(false);
            this.chart.series[0].remove(false);

            this.chart.addSeries({
                name: 'Current strategy',
                color: '#EBB700',
                data: before,
            }, false);

            this.chart.addSeries({
                name: 'New strategy',
                color: '#C84E00',
                data: after,
            }, false);

        } else {
            this.chart.series[1].show(false);
            for (var i = 0; i < journeyPlanData.length; i++) {
                this.chart.series[0].data[i].update(journeyPlanData[i].before, false);
                this.chart.series[1].data[i].update(journeyPlanData[i].after, false);
            }
        }
        
        if (revealAfter) {
            this.hideAfter = false;
        }

        if (this.hideAfter) {
            this.chart.series[1].hide(false);
        }
        
        this.chart.xAxis[0].setTitle({
            text: 'Annual progression commencing ' + Highcharts.dateFormat('%e %B', journeyPlanData[0].commencementDate)
        });
        this.chart.redraw();
    };
};


$.Core.journeyPlan = new JourneyPlanClass();

$(function() {

    $.Core.journeyPlan.chart = new Highcharts.Chart({
        chart: {
            type: 'column',
            renderTo: 'journeyPlanChartDiv',
            backgroundColor: 'none',
            plotBorderWidth: 1,
            plotBackgroundColor: 'white'
        },

        credits: false,
        exporting: {
            enabled: false
        },
        title: {
            text: 'Funding Progression',
            style: {
                fontSize: '28px',
                lineHeight: '28px',
                fontWeight: 'normal',
                fontFamily: 'Lato, sans-serif',
                color: '#3e3e3e',
            },
            y:25,
        },
        xAxis: {
            title: {
                useHTML: true,
                text: ' ',
                style: {
                    fontFamily: 'Lato, sans-serif',
                    fontSize: '14px',
                    //                backgroundColor: '#f0f0f0',
                },
            },
        },
        yAxis: [{
            title: {
                useHTML: true,
                text: 'Funding level',
                style: {
                    fontFamily: 'Lato, sans-serif',
                    fontSize: '14px',
                    //                backgroundColor: '#f0f0f0',
                },
            },
            labels: {
                useHTML: true,
                formatter: fusion.util.formatChartPercentageLabels
            },
            plotLines: [{
                value: 100,
                width: 1,
                color: 'green',
                dashStyle: 'dash',
                zIndex: 10,
                label: {
                    text: 'Fully funded',
                    align: 'left',
                    y: -4,
                    x: 23,
                    useHTML: true,
                    style: {
                        margin: '3px',
                    }
                }
            }],
            minRange: 110,
            min:0
        }, {
            title: {
                useHTML: true,
                text: 'Funding level',
                style: {
                    fontFamily: 'Lato, sans-serif',
                    fontSize: '14px',
                    //                backgroundColor: '#f0f0f0',
                },
            },
            labels: {
                useHTML: true,
                formatter: fusion.util.formatChartPercentageLabels
            },

            linkedTo: 0,
            opposite: true,
        }],
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0,
            }
        },
        tooltip: {
            shared: true,
            hideDelay: 0,
            borderWidth: 0,
            borderRadius: 0,
            backgroundColor: '#007c92',
            shadow: false,
            useHTML: true,
            followPointer: true,
            formatter: function() {
                var html = 'Year ' + this.x + '<br/>';
                $.each(this.points, function(i, point) {
                    html += '<br/>' + point.series.name + ': ' + point.y.toFixed(0) + '%';
                });

                return html;
            },
            style: {
                color: '#ffffff',
                fontSize: '1.1em',
                padding: '20px',
                opacity: 0.8,
            }
        },
        legend: {
            layout: 'horizontal',
            floating: true,
            align: 'left',
            verticalAlign: 'top',
            borderWidth: 0,
            borderRadius: 0,
            //            backgroundColor: '#F0F0F0',
            padding: 10,
            y: 10,

        },



        series: [{
            name: 'Before',
            color: '#EBB700',
            data: []
        }, {
            name: 'After',
            color: '#C84E00',
            data: []
        }],


    });



});