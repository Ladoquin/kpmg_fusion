﻿$(function () {
    
    var newJPChart = null;
    var that = this;
    var initialised = false;

    var $jpStartYearObject;
    var $jpAnnualContObject;
    var $jpTermObject;
    var $jpTriggerFundingObject;
    var $jpTriggerStepsObject;
    var $jpTriggerSwitchTimeObject;
    var $jpTriggerSwitchFundingObject;
    var $jpTriggerInitialReturnObject;
    var $jpTriggerFinalReturnObject;
    var $jpSelfSufficiencyObject;
    var $jpCurrentReturnObject;
    var $jpTriggerTimeStartObject;
    var $jpTriggerTimeEndObject;
    var $jpPeriodSelect;
    var fundingBasisId;
    var visibleChartLines = [];
    var setJPdataCompleted = false;
    var isiPad = navigator.userAgent.match(/iPad/i) != null;

    var setAnalysisPeriod = function (data) {
        $('input.analysisPeriodEnd').data("range-min-date", Highcharts.dateFormat('%d/%m/%Y', data.min));
        $('input.analysisPeriodEnd').data("max-date", Highcharts.dateFormat('%d/%m/%Y', data.max));
        $('.analysisPeriodEnd', $("#journeyplan_options")).each(function () { //todo: FUSN-972 only allow curve data range.
            $(this).datepicker({
                minDate: $(this).data("range-min-date"),
                maxDate: $(this).data("max-date"),
                inline: true,
                dateFormat: 'dd/mm/yy',
                showOn: isiPad ? "button" : "both",
                buttonImage: 'Images/datepicker-button.png',
                buttonText: null,
                onClose: function (dateText, inst) {
                    if (isiPad)
                        $(this).attr("disabled", false);
                },
                beforeShow: function (dateText, inst) {
                    if (isiPad)
                        $(this).attr("disabled", true);
                }
            }).addClass("pointer");
            $(this).datepicker("option", "minDate", Highcharts.dateFormat('%d/%m/%Y', data.min));
            $(this).datepicker("option", "maxDate", Highcharts.dateFormat('%d/%m/%Y', data.max));
            $(this).val(Highcharts.dateFormat('%d/%m/%Y', data.val));
        });
    };

    var initAnalysisPeriod = function() {
        $('input.analysisPeriodEnd').change(function () {
            var endTimeDatePicker = fusion.util.getDateFromDatepicker($('input.analysisPeriodEnd').val());

            var minDate = $('input.analysisPeriodEnd').data("range-min-date");
            var startTimeDatePicker = new Date(Date.UTC(
                parseInt(minDate.split("/")[2]),
                parseInt(minDate.split("/")[1]) - 1,
                parseInt(minDate.split("/")[0]), 0, 0, 0, 0)).getTime();

            $.post('/Json/SetAnalysisPeriod', { start: startTimeDatePicker, end: endTimeDatePicker }, function (data) {
                redirectOnError(data);
                setJPData();
                refreshExistingAssumptions();
            });
        });
    };

    var init = function () {

        $jpStartYearObject = $('#jpStartYear');
        $jpAnnualContObject = $('#jpAnnualCont');
        $jpTermObject = $('#jpTerm');
        $jpTriggerSwitchTimeObject = $('#jpTriggerSwitchTime');
        $jpTriggerSwitchFundingObject = $('#jpTriggerSwitchFunding');
        $jpTriggerFundingObject = $('#jpTriggerFundingSlider');
        $jpTriggerStepsObject = $('#jpTriggerStepsSlider');
        $jpTriggerInitialReturnObject = $('#jpTriggerInitialReturnSlider');
        $jpTriggerFinalReturnObject = $('#jpTriggerFinalReturnSlider');
        $jpTriggerTimeStartObject = $('#jpTimeFirstStepSlider');
        $jpTriggerTimeEndObject = $('#jpTimeSwitchingPeriodSlider');

        $jpSelfSufficiencyObject = $('#jpSelfSufficiencySlider');
        $jpCurrentReturnObject = $('#jpCurrentReturnSlider');

        initAnalysisPeriod();

        initResetButton();

        refreshExistingAssumptions();

        initValuationChart();

        initSliders();

        $('.toggle-switch').click(function () {
            var toggleGroup = $(this).data('group');
            $("a.toggle-switch[data-group='" + toggleGroup + "']").toggleClass('toggle-off').toggleClass('toggle-on');
            switch ($(this).attr('id')) {
                case 'jpTriggerSwitchTime':
                case 'jpTriggerSwitchFunding':
                    $('#jpTriggerInputTimeTable').toggleClass('hide');
                    $('#jpTriggerInputFundingTable').toggleClass('hide');
                    $jpTriggerTimeStartObject.slider().addTick({ overwrite: true });
                    $jpTriggerTimeEndObject.slider().addTick({ overwrite: true });
                    break;
                default:
                    break;
            };
            setJPData();
        });

        $jpPeriodSelect = $("#jpPeriodSelect").styleableSelect({
            onChanged: function () {
                setJPData();                
            }
        });

        initialised = true;
    };

    var refreshExistingAssumptions = function () {
        $.getJSON('/JourneyPlan/GetExistingAssumptions', function (data) {
            redirectOnError(data);
            var fundingAssumptions = data.fundingBasisAssumptions;
            var buyoutAssumptions = data.buyoutBasisAssumptions;
            var rpAssumptions = data.recoveryPlanAssumptions;

            var giltYieldLabel = data.giltYieldLabel != null ? data.giltYieldLabel : 'Gilt yield';
            $('#jpGiltYieldLabel').text(giltYieldLabel);

            var preDisc = 'n/a';
            var postDisc = 'n/a';
            var penDisc = 'n/a';
            var giltYield = 'n/a';
            if (fundingAssumptions) {
                preDisc = !isNaN(fundingAssumptions.preDiscountRate) ? (fundingAssumptions.preDiscountRate * 100).toFixed(2) + '%' : 'n/a';
                postDisc = !isNaN(fundingAssumptions.postDiscountRate) ? (fundingAssumptions.postDiscountRate * 100).toFixed(2) + '%' : 'n/a';
                penDisc = !isNaN(fundingAssumptions.pensionerDiscountRate) ? (fundingAssumptions.pensionerDiscountRate * 100).toFixed(2) + '%' : 'n/a';
                giltYield = !isNaN(fundingAssumptions.giltYield) ? (fundingAssumptions.giltYield * 100).toFixed(2) + '%' : 'n/a';
            }
            $("#jpFundingBasisPreRetirementDiscount").text(preDisc);
            $("#jpFundingBasisPostRetirementDiscount").text(postDisc);
            $("#jpFundingBasisPensionerDiscount").text(penDisc);
            $("#jpFundingBasisGiltYield").text(giltYield);

            preDisc = 'n/a';
            postDisc = 'n/a';
            penDisc = 'n/a';
            giltYield = 'n/a';
            if (buyoutAssumptions) {
                preDisc = !isNaN(buyoutAssumptions.preDiscountRate) ? (buyoutAssumptions.preDiscountRate * 100).toFixed(2) + '%' : 'n/a';
                postDisc = !isNaN(buyoutAssumptions.postDiscountRate) ? (buyoutAssumptions.postDiscountRate * 100).toFixed(2) + '%' : 'n/a';
                penDisc = !isNaN(buyoutAssumptions.pensionerDiscountRate) ? (buyoutAssumptions.pensionerDiscountRate * 100).toFixed(2) + '%' : 'n/a';
                giltYield = !isNaN(buyoutAssumptions.giltYield) ? (buyoutAssumptions.giltYield * 100).toFixed(2) + '%' : 'n/a';
            }
            $("#jpBuyoutBasisPreRetirementDiscount").text(preDisc);
            $("#jpBuyoutBasisPostRetirementDiscount").text(postDisc);
            $("#jpBuyoutBasisPensionerDiscount").text(penDisc);
            $("#jpBuyoutBasisGiltYield").text(giltYield);

            if (rpAssumptions) {
                var rpLen = (!isNaN(rpAssumptions.length) && rpAssumptions.length >= 0) ? rpAssumptions.length : 'n/a';
                var rpInc = !isNaN(rpAssumptions.increase) ? (rpAssumptions.increase * 100).toFixed(2) + '%' : 'n/a';
                var rpLump = (!isNaN(rpAssumptions.lumpSumAmount) && rpAssumptions.lumpSumAmount >= 0) ? fusion.util.formatCurrency(rpAssumptions.lumpSumAmount) : 'n/a';
                var rpCommence = (!isNaN(rpAssumptions.commencement) && rpAssumptions.commencement >= 0) ? rpAssumptions.commencement : 'n/a';
                var rpYear1Cont = (!isNaN(rpAssumptions.firstYearCont) && rpAssumptions.firstYearCont >= 0) ? fusion.util.formatCurrency(rpAssumptions.firstYearCont) : 'n/a';
                var rpLastPaymentDate = rpAssumptions.lastPaymentDate == null ? '' : Highcharts.dateFormat('%d/%m/%Y', rpAssumptions.lastPaymentDate);
                $("#jpExistingRpLength").text(rpLen);
                $("#jpExistingRpIncrease").text(rpInc);
                $("#jpExistingRpLumpSum").text(rpLump);
                $("#jpExistingRpCommencement").text(rpCommence);
                $('#jpExistingRpFirstYearContribution').text(rpYear1Cont);
                $('#jpExistingRpCurrentRPContsDue').text(rpYear1Cont);
                $('#jpExistingRpCurrentRPLastPaymentDate').text(rpLastPaymentDate);
                $('#jpRpCurrentSummary').toggle(rpAssumptions.type == "current");
                $('#jpRpNewSummary').toggle(rpAssumptions.type != "current");
            }
        });
    };

    //todo: to sliderWithText.js
    var getFinalValue = function($slider){
        var finalValue = $slider.parent().parent().find('.slider-text-value').data('finalValue');
        if (finalValue == null || finalValue == typeof 'undefined')
            finalValue = $slider.slider("option", "value");

        return finalValue;
    }

    var setJPData = function (callback) {
        var jpJson = {
            effectiveDate: $('.analysisPeriodEnd').val(),
            technicalProvisionsBasisId: fundingBasisId,
            currentReturn: getFinalValue($jpCurrentReturnObject),
            initialReturn: getFinalValue($jpTriggerInitialReturnObject),
            finalReturn: getFinalValue($jpTriggerFinalReturnObject),
            numberOfSteps: getFinalValue($jpTriggerStepsObject),
            discountRate: getFinalValue($jpSelfSufficiencyObject),
            addExtraContributions: $("#addExtraContributions").is(':checked'),
            includeBuyIns: $("#includeBuyIns").is(':checked'),
            startYear: getFinalValue($jpStartYearObject), 
            AnnualContributions: getFinalValue($jpAnnualContObject),
            term:  getFinalValue($jpTermObject),
            addCurrentRecoveryPlan: $("#addIncludeExistingRecoveryPlan").is(':checked'),
            triggerTimeStartYear: getFinalValue($jpTriggerTimeStartObject),
            triggerTimeEndYear: getFinalValue($jpTriggerTimeEndObject),
            showCurrentAssets: visibleChartLines[0],
            showNewAssets: visibleChartLines[1],
            showFunding: visibleChartLines[2],
            showSelfSufficiency: visibleChartLines[3],
            showBuyOut: visibleChartLines[4],
            yearsToView: $jpPeriodSelect.getValue()
        };

        $.post('/JourneyPlan/SetJourneyPlanData', jpJson, function (data) {
            redirectOnError(data);
            refreshJPChart(data);
            setInputControls(data);
            if (callback != null && callback != 'undefined') {
                callback();
            }
            setJPdataCompleted = true;
        });
    };

    var stateToServer = function () {
        var stateData = {
            showCurrentAssets: visibleChartLines[0],
            showNewAssets: visibleChartLines[1],
            showFunding: visibleChartLines[2],
            showSelfSufficiency: visibleChartLines[3],
            showBuyOut: visibleChartLines[4],
            yearsToView: $jpPeriodSelect.getValue()
        }

        $.post('/JourneyPlan/SetClientState', stateData);

    };

    var redirectOnError = function (data) {
        if (data.jpLoadError === true) {
            window.location = 'Error';
            throw new Error("Failed to load Journey Plan.");
        }
    }

    var initSliders = function () {

        var updateCell = function (event, ui) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: ui.value
            });
        };

        var updateJP = function (event, ui) {
            setJPData();
        };

        $('#jpTriggerFundingSlider, #jpSelfSufficiencySlider, #jpCurrentReturnSlider')
            .each(function () {
                $(this).slider({
                    animate: 400,
                    range: 'min',
                    min: $(this).data('min'),
                    value: 0,
                    step: $(this).data('increment'),
                    slide: updateCell,
                    stop: updateJP,
                    create: function () { $(this).slider('setTextValueElement') },
                    elementSelector: $(this),
                    textElement: $(this).parent().siblings().last()
                });
            });

        $('#jpTriggerInitialReturnSlider, #jpTriggerFinalReturnSlider, #jpTimeFirstStepSlider, #jpTimeSwitchingPeriodSlider, #jpStartYear, #jpTerm, #jpAnnualInc')
            .each(function () {
                $(this).slider({
                    animate: 400,
                    range: 'min',
                    min: $(this).data('min'),
                    value: 0,
                    step: $(this).data('increment'),
                    slide: updateCell,
                    stop: function (event, ui) {
                        setNewAssetsVisible();
                        updateJP(event, ui);
                    },
                    create: function () { $(this).slider('setTextValueElement') },
                    elementSelector: $(this),
                    textElement: $(this).parent().siblings().last()
                });
            }); 


        var triggerStepsDependentsHandler = function () {
            if (getFinalValue($('#jpTriggerStepsSlider')) <= 1) {
                fusion.util.disableSlider($('#jpTimeSwitchingPeriodSlider'));
            } else {
                fusion.util.enableSlider($('#jpTimeSwitchingPeriodSlider'));
            }
            if (getFinalValue($('#jpTriggerStepsSlider')) == 0) {
                fusion.util.disableSlider($('#jpTimeFirstStepSlider'))
                fusion.util.disableSlider($('#jpTriggerFinalReturnSlider'))
            } else {
                fusion.util.enableSlider($('#jpTimeFirstStepSlider'));
                fusion.util.enableSlider($('#jpTriggerFinalReturnSlider'))
            }
        };

        $('#jpTriggerStepsSlider').slider({
            animate: 400,
            range: 'min',
            min: $('#jpTriggerStepsSlider').data('min'),
            value: 0,
            step: $('#jpTriggerStepsSlider').data('increment'),
            slide: function (event, ui) {
                updateCell.call(this, event, ui);
            },
            stop: function (event, ui) {
                setNewAssetsVisible();
                triggerStepsDependentsHandler();
                updateJP(event, ui);
            },
            create: function () { $('#jpTriggerStepsSlider').slider('setTextValueElement') },
            elementSelector: $('#jpTriggerStepsSlider'),
            textElement: $('#jpTriggerStepsSlider').parent().siblings().last()
        });

        $('#jpAnnualCont').slider({
            animate: 400,
            range: 'min',
            min: $('#jpAnnualCont').data('min'),
            value: 0,
            slide: updateCell,
            stop: function (event, ui) {
                setNewAssetsVisible();
                updateJP(event, ui);
            },
            create: function () { $('#jpAnnualCont').slider('setTextValueElement') },
            elementSelector: $('#jpAnnualCont'),
            textElement: $('#jpAnnualCont').parent().siblings().last()
        });
    };

    var setNewAssetsVisible = function() {
        visibleChartLines[1] = true;
    }

    var newAssetsIsVisible = function(){
        return visibleChartLines[1];
    }

    var setCrossOverVisibility = function (chartLine, chart) {
        //show hide entries in the corssover table.
        //visability is actual set after this event
        visibleChartLines[chartLine._i] = !chartLine.visible;

        if (visibleChartLines[0] === true) {
            $('#jpCrossOverTable tr').eq(1).find('span').show();
            $('#jpCrossOverTable tr').eq(1).find('span.not-applicable').hide();
        }

        if (visibleChartLines[1] === true) {
            $('#jpCrossOverTable tr').eq(2).find('span').show();
            $('#jpCrossOverTable tr').eq(2).find('span.not-applicable').hide();
            showTriggerPlotLinesAndBands(chart);
        }

        for (var i = 2; i < 5; i++)
            if (visibleChartLines[i] === true) {
                $('#jpCrossOverTable th:nth-child(' + i + '), #jpCrossOverTable td:nth-child(' + i + ')').find('span').show();
                $('#jpCrossOverTable th:nth-child(' + i + '), #jpCrossOverTable td:nth-child(' + i + ')').find('span.not-applicable').hide();
            }

        if (visibleChartLines[0] === false) {
            $('#jpCrossOverTable tr').eq(1).find('span').hide();
            $('#jpCrossOverTable tr').eq(1).find('span.not-applicable').show();
        }

        if (visibleChartLines[1] === false) {
            $('#jpCrossOverTable tr').eq(2).find('span').hide();
            $('#jpCrossOverTable tr').eq(2).find('span.not-applicable').show();
            hideTriggerPlotLinesAndBands(chart);
        }

        for (var i = 2; i < 5; i++)
            if (visibleChartLines[i] === false) {
                $('#jpCrossOverTable th:nth-child(' + i + '), #jpCrossOverTable td:nth-child(' + i + ')').find('span').hide();
                $('#jpCrossOverTable th:nth-child(' + i + '), #jpCrossOverTable td:nth-child(' + i + ')').find('span.not-applicable').show();
            }
    };

    var hideTriggerPlotLinesAndBands = function (chart) {
        if (chart.xAxis[0].plotLinesAndBands.length > 0) {
            for (var i = 0; i < chart.xAxis[0].plotLinesAndBands.length; i++)
                if (chart.xAxis[0].plotLinesAndBands[i].svgElem != null)
                    chart.xAxis[0].plotLinesAndBands[i].svgElem.hide();
        }
    }
    var showTriggerPlotLinesAndBands = function (chart) {
        if (chart.xAxis[0].plotLinesAndBands.length > 0)
            for (var i = 0; i < chart.xAxis[0].plotLinesAndBands.length; i++)
                if (chart.xAxis[0].plotLinesAndBands[i].svgElem != null)
                    chart.xAxis[0].plotLinesAndBands[i].svgElem.show();
    }

    var initValuationChart = function () {
        newJPChart = new Highcharts.Chart({
            chart: {
                renderTo: 'newJourneyPlanChartDiv',
                backgroundColor: 'none',
                plotBackgroundColor: 'white',
                type: 'spline',
                animation: true,
                marginBottom: 40,
                marginTop: 40,
                marginLeft: 60,
                reflow: false,
                align: 'center',
                ignoreHiddenSeries: false
            },
            credits: false,
            exporting: { enabled: false },
            plotOptions: {
                spline: {
                    lineWidth: 2,
                    states: {
                        hover: {
                            enabled: true,
                            lineWidth: 3
                        }
                    },
                    marker: {
                        enabled: true,
                        radius: 0,
                        symbol: "circle",
                        states: {
                            hover: {
                                enabled: true,
                                radius: 6
                            }
                        }
                    },
                    pointInterval: 1
                },
                series: {
                    events: {
                        legendItemClick: function () {
                            setCrossOverVisibility(this, newJPChart);
                            stateToServer();
                        }
                    }
                }
            },
            title: {
                text: null,
            },
            xAxis: {
                title: {
                    text: 'Years',
                    style: {
                        color: "#3F3F3F",
                        fontWeight: "normal",
                        margin: 15
                    }
                },
                tickInterval: 5,
                tickLength: 0,
                max: 40,
                minPadding: 0
            },
            yAxis: [{
                title: {
                    text: '',
                    style: {
                        color: "#3F3F3F",
                        fontWeight: "normal",
                    },
                    margin: 20,
                    align: "high"
                },
                min: 0,
                labels:{                    
                    formatter: function () {
                        if(this.value !=0){
                            return fusion.util.formatCurrencyAbsolute(this.value);
                        }
                    }
                }
            }],
            legend: {
                layout: 'horizontal',
                floating: false,
                align: 'left',
                verticalAlign: 'top',
                borderWidth: 0,
                y: -15,
                x: 50,
                itemDistance: 24,
                itemMarginTop: 1
            },
            tooltip: {
                shared: true,
                hideDelay: 0,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                followPointer: true,
                formatter: function () {
                    var year = this.x;
                    var html = 'Year: ' + year;
                    $.each(this.points, function (i, point) {
                        if (point.series.state == 'hover')
                            html += '<br /><b>' + point.series.name + ': ' + fusion.util.formatCurrency(this.y) + '</b>';
                        else
                            html += '<br />' + point.series.name + ': ' + fusion.util.formatCurrency(this.y);
                    });
                    return html;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8
                }
            }
        });

    };

    var setChartData = function (data) {

        if (data.series != null && data.series != typeof 'undefined') {

            initValuationChart();

            var crossOverpoints = data.newJourneyPlan.crossOverPoints;

            $("#tpca_crossover").text(getYearFormat(crossOverpoints.tpca));
            $("#ssca_crossover").text(getYearFormat(crossOverpoints.ssca));
            $("#boca_crossover").text(getYearFormat(crossOverpoints.boca));
            $("#tpna_crossover").text(getYearFormat(crossOverpoints.tpna));
            $("#ssna_crossover").text(getYearFormat(crossOverpoints.ssna));
            $("#bona_crossover").text(getYearFormat(crossOverpoints.bona));

            var displayOptions = data.displayOptions;

            visibleChartLines[0] = displayOptions.showCurrentAssets;
            visibleChartLines[1] = displayOptions.showNewAssets;
            visibleChartLines[2] = displayOptions.showFunding;
            visibleChartLines[3] = displayOptions.showSelfSufficiency;
            visibleChartLines[4] = displayOptions.showBuyOut;

            $jpPeriodSelect.select(displayOptions.yearsToView, false);

            $.each(data.series, function (itemNo, item) {
                newJPChart.addSeries({
                    name: item.displayName,
                    data: item.dataPoints,
                    dashStyle: item.dashedLine ? 'dash' : 'solid',
                    color: item.colour,
                    visible: visibleChartLines[itemNo]
                }, false);
                setCrossOverVisibility(item, newJPChart);
            });

            if (data.newJourneyPlan.triggerData.triggerYears.length > 0) {
                if (data.newJourneyPlan.triggerData.triggerYears.length > 1) {
                    newJPChart.xAxis[0].addPlotBand({
                        from: data.newJourneyPlan.triggerData.triggerYears[0],
                        to: data.newJourneyPlan.triggerData.triggerYears[data.newJourneyPlan.triggerData.triggerYears.length - 1],
                        color: '#F7FBFD',
                        id: 'triggerSpanPlotBand'
                    });
                }
                for (var i = 0; i < data.newJourneyPlan.triggerData.triggerYears.length; i++) {
                    newJPChart.xAxis[0].addPlotLine({
                        value: data.newJourneyPlan.triggerData.triggerYears[i],
                        color: '#ECF4F9',
                        width: 2,
                        id: 'triggerPlotLine' + i
                    });
                }
            }

            newJPChart.xAxis[0].options.tickInterval = data.series[0].dataPoints.length <= 11 ? 1 : 5;
            newJPChart.xAxis[0].setExtremes(0, data.series[0].dataPoints.length - 1);
            newJPChart.redraw();

            if (newAssetsIsVisible()) {
                showTriggerPlotLinesAndBands(newJPChart);
            } else {
                hideTriggerPlotLinesAndBands(newJPChart);
            }
        }
    };

    var getYearFormat = function (year) {
        if (year == 0)
            return 'This year';
        if (year == 1)
            return year + ' year';
        if (year > 1)
            return year + ' years';
        return 'n/a';
    };

    var refreshJPChart = function (data) {
        if (!initialised) {
            init(data);
        }
        if (data != null)
            setChartData(data);
    };

    var enableExtraRecoveryPlan = function () {
        $("#jpRecoveryInputTable1").find(".readonly-rp").removeClass("readonly-rp");
        $("#jpRecoveryInputTable1").find(".greySlider").removeClass("greySlider");
        $("#jpRecoveryInputTable1").find(".assumptionsSlider").each(function () {
            $(this).slider('enable');
        });
    };

    var disableExtraRecoveryPlan = function () {
        $("#jpRecoveryInputTable1").find(".assumptionsSlider").addClass("readonly-rp").closest("td").addClass("greySlider");
        $("#jpRecoveryInputTable1").find(".assumptionsSlider").each(function () {
            $(this).slider('disable');
            $(this).slider('disableEditableElement', this);
        });
    };

    var setInputControls = function (data) {

        // analysis date
        setAnalysisPeriod(data.dates);

        var jpData = data.newJourneyPlan,
            recoveryPlanData = jpData.recoveryPlanData,
            triggerData = jpData.triggerData,
            selfSufficiencyData = jpData.selfSufficiencyData,
            assetGrowthData = jpData.assetGrowthData;

        $('#includeBuyIns').prop('checked', jpData.includeBuyIns);
        $('#addIncludeExistingRecoveryPlan').prop('checked', recoveryPlanData.addCurrentRecoveryPlan);
        $('#addExtraContributions').prop('checked', recoveryPlanData.addExtraContributions);
        


        //Recovery Plan
        $jpStartYearObject.slider('option', 'max', recoveryPlanData.startYear.max);
        $jpStartYearObject.slider('value', recoveryPlanData.startYear.val);
        $jpStartYearObject.slider('setTextValueElement', recoveryPlanData.startYear.val);
        $jpStartYearObject.slider('updateSliderValue', { value: recoveryPlanData.startYear.val });
        $jpStartYearObject.data('original', recoveryPlanData.startYear.original);
        $jpStartYearObject.slider().addTick();

        $jpAnnualContObject.slider('option', 'max', recoveryPlanData.annualCont.max);
        $jpAnnualContObject.slider('option', 'step', recoveryPlanData.annualCont.max / 500);
        $jpAnnualContObject.slider('value', recoveryPlanData.annualCont.val);
        $jpAnnualContObject.slider('setTextValueElement', recoveryPlanData.annualCont.val);
        $jpAnnualContObject.slider('updateSliderValue', { value: recoveryPlanData.annualCont.val });
        $jpAnnualContObject.data('original', recoveryPlanData.annualCont.original);
        $jpAnnualContObject.data('original-tooltip', fusion.util.formatCurrency(recoveryPlanData.annualCont.val));
        $jpAnnualContObject.slider().addTick();

        $jpTermObject.slider('option', 'max', recoveryPlanData.term.max);
        $jpTermObject.slider('value', recoveryPlanData.term.val);
        $jpTermObject.slider("setTextValueElement", recoveryPlanData.term.val);
        $jpTermObject.data('original', recoveryPlanData.term.original);
        $jpTermObject.data('original-tooltip', recoveryPlanData.term.original + ' years');
        $jpTermObject.slider().addTick();

        //Trigger
        if (triggerData.fundingLevelTrigger)
        {
            $('#jpTriggerSwitchTime').addClass('toggle-off');
            $('#jpTriggerSwitchTime').removeClass('toggle-on');
            $('#jpTriggerSwitchFunding').removeClass('toggle-off');
            $('#jpTriggerSwitchFunding').addClass('toggle-on');
            $('#jpTriggerInputTimeTable').addClass('hide');
            $('#jpTriggerInputFundingTable').removeClass('hide');
        }
        else
        {
            $('#jpTriggerSwitchTime').addClass('toggle-on');
            $('#jpTriggerSwitchTime').removeClass('toggle-off');
            $('#jpTriggerSwitchFunding').removeClass('toggle-on');
            $('#jpTriggerSwitchFunding').addClass('toggle-off');
            $('#jpTriggerInputTimeTable').removeClass('hide');
            $('#jpTriggerInputFundingTable').addClass('hide');
        }
        $jpTriggerInitialReturnObject.slider('option', 'min', triggerData.initialReturnAssets.min);
        $jpTriggerInitialReturnObject.slider('option', 'max', triggerData.initialReturnAssets.max);
        $jpTriggerInitialReturnObject.slider('value', triggerData.initialReturnAssets.val);
        $jpTriggerInitialReturnObject.slider('setTextValueElement', triggerData.initialReturnAssets.val);
        $jpTriggerInitialReturnObject.slider('updateSliderValue', { value: triggerData.initialReturnAssets.val });
        $jpTriggerInitialReturnObject.data('original', triggerData.initialReturnAssets.original);
        $jpTriggerInitialReturnObject.slider().addTick();

        $jpTriggerFinalReturnObject.slider('option', 'min', triggerData.finalReturnAssets.min);
        $jpTriggerFinalReturnObject.slider('option', 'max', triggerData.finalReturnAssets.max);
        $jpTriggerFinalReturnObject.slider('value', triggerData.finalReturnAssets.val);
        $jpTriggerFinalReturnObject.slider('setTextValueElement', triggerData.finalReturnAssets.val);
        $jpTriggerFinalReturnObject.slider('updateSliderValue', { value: triggerData.finalReturnAssets.val });
        $jpTriggerFinalReturnObject.data('original', triggerData.finalReturnAssets.original);
        $jpTriggerFinalReturnObject.slider().addTick();

        $jpTriggerFundingObject.slider('option', 'min', triggerData.fundingLevel.min);
        $jpTriggerFundingObject.slider('option', 'max', triggerData.fundingLevel.max);
        $jpTriggerFundingObject.slider('value', triggerData.fundingLevel.val);
        $jpTriggerFundingObject.slider("setTextValueElement", triggerData.fundingLevel.val);
        $jpTriggerFundingObject.slider('updateSliderValue', { value: triggerData.fundingLevel.val });
        $jpTriggerFundingObject.data('original', triggerData.fundingLevel.original);
        $jpTriggerFundingObject.slider().addTick();

        $jpTriggerStepsObject.slider('option', 'min', triggerData.numberOfSteps.min);
        $jpTriggerStepsObject.slider('option', 'max', triggerData.numberOfSteps.max);
        $jpTriggerStepsObject.slider('value', triggerData.numberOfSteps.val);
        $jpTriggerStepsObject.slider('setTextValueElement', triggerData.numberOfSteps.val);
        $jpTriggerStepsObject.slider('updateSliderValue', { value: triggerData.numberOfSteps.val });
        $jpTriggerStepsObject.data('original', triggerData.numberOfSteps.original);
        $jpTriggerStepsObject.data('original-tooltip', triggerData.numberOfSteps.original);
        $jpTriggerStepsObject.slider().addTick();

        $jpTriggerTimeStartObject.slider('option', 'min', triggerData.triggerTimeStartYear.min);
        $jpTriggerTimeStartObject.slider('option', 'max', triggerData.triggerTimeStartYear.max);
        $jpTriggerTimeStartObject.slider('value', triggerData.triggerTimeStartYear.val);
        $jpTriggerTimeStartObject.slider('setTextValueElement', triggerData.triggerTimeStartYear.val);
        $jpTriggerTimeStartObject.slider('updateSliderValue', { value: triggerData.triggerTimeStartYear.val });
        $jpTriggerTimeStartObject.data('original', triggerData.triggerTimeStartYear.original);
        $jpTriggerTimeStartObject.data('original-tooltip', triggerData.triggerTimeStartYear.original);
        $jpTriggerTimeStartObject.slider().addTick();

        $jpTriggerTimeEndObject.slider('option', 'min', triggerData.triggerTimeEndYear.min);
        $jpTriggerTimeEndObject.slider('option', 'max', triggerData.triggerTimeEndYear.max);
        $jpTriggerTimeEndObject.slider('value', triggerData.triggerTimeEndYear.val);
        $jpTriggerTimeEndObject.slider('setTextValueElement', triggerData.triggerTimeEndYear.val);
        $jpTriggerTimeEndObject.slider('updateSliderValue', { value: triggerData.triggerTimeEndYear.val });
        $jpTriggerTimeEndObject.data('original', triggerData.triggerTimeEndYear.original);
        $jpTriggerTimeEndObject.data('original-tooltip', triggerData.triggerTimeEndYear.original);
        $jpTriggerTimeEndObject.slider().addTick();

        //Self Sufficiency
        $jpSelfSufficiencyObject.slider('option', 'max', selfSufficiencyData.discountRate.max);
        $jpSelfSufficiencyObject.slider('value', selfSufficiencyData.discountRate.val);
        $jpSelfSufficiencyObject.slider('setTextValueElement', selfSufficiencyData.discountRate.val);
        $jpSelfSufficiencyObject.slider('updateSliderValue', { value: selfSufficiencyData.discountRate.val});
        $jpSelfSufficiencyObject.data('original', selfSufficiencyData.discountRate.original);
        $jpSelfSufficiencyObject.slider().addTick();

        //Asset Growth
        $jpCurrentReturnObject.slider('option', 'min', assetGrowthData.currentReturn.min);
        $jpCurrentReturnObject.slider('option', 'max', assetGrowthData.currentReturn.max);
        $jpCurrentReturnObject.slider('value', assetGrowthData.currentReturn.val);
        $jpCurrentReturnObject.slider('setTextValueElement', assetGrowthData.currentReturn.val);
        $jpCurrentReturnObject.slider('updateSliderValue', { value: assetGrowthData.currentReturn.val });
        $jpCurrentReturnObject.data('original', assetGrowthData.currentReturn.original);
        $jpCurrentReturnObject.slider().addTick();

        if (recoveryPlanData.addExtraContributions === true) {
            enableExtraRecoveryPlan();
        }
        else {
            disableExtraRecoveryPlan();
        }

        if (triggerData.numberOfSteps.val <= 1)
            fusion.util.disableSlider($('#jpTimeSwitchingPeriodSlider'));
        if (triggerData.numberOfSteps.val == 0) {
            fusion.util.disableSlider($('#jpTimeFirstStepSlider'));
            fusion.util.disableSlider($('#jpTriggerFinalReturnSlider'));
        }
    };

    var cashflowBasisChanged = function (basis, custom) {
        $("#currentBasis").data("masterBasisId", basis.masterBasisId);
        setJPData();
    };

    var setFundingBasis = function (basis) {
        fundingBasisId = basis.id;
        setJPData();
    };

    var resetInputs = function () {
        resetControls(true);
    };

    var initResetButton = function () {
        $('#resetInputs').click(resetInputs);
    };

    var resetControls = function (clearCache) {
        var url = clearCache ? '/JourneyPlan/ResetJourneyPlanData' : '/JourneyPlan/NewJourneyPlanData';
        $.getJSON(url, function (jpData) {
            redirectOnError(jpData);
            fundingBasisId = jpData.newJourneyPlan.technicalProvisionsBasisId;
            refreshJPChart(jpData);
            setInputControls(jpData);
            $('#overlayToDisableClick').hide();
        });
    };

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(document).ready(function () {
        fusion.page.init({ idx: 4, name: "journeyplan" });
        fusion.tabs.init();
        fusion.dashboardHandler.init();
        fusion.basisChangedEvent.add(cashflowBasisChanged);
        fusion.fundingBasisChangedEvent.add(setFundingBasis);

        init();

        resetControls(false);

        $('#addExtraContributions, #addIncludeExistingRecoveryPlan').click(function () {
            setNewAssetsVisible();
            setJPData();
        });

        $('#includeBuyIns').click(function () {
            setJPData();
        });

        $(window).resize(function () {
            delay(function () {
                if (setJPdataCompleted)
                    setJPData();
            }, 500);
        });
    });
});
