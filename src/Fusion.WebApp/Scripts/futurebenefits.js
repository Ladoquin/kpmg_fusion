﻿fusion.analysis.FutureBenefitsPanel = function() {

    var that = this;
    var initialised = false;

    this.changed = $.Callbacks();

    var reset = function () {

        $.getJSON('/Json/ResetFutureBenefits', function (futureBenefitsData) {
            that.refresh(futureBenefitsData);
            that.changed.fire({ id: that.id, data: futureBenefitsData });
        });
    };

    var setFutureBenefits = function () {
        var futureBenefits = {},
            finalImc,
            finalAr;

        finalImc = $('#futureBenefitsIncreaseMemberContributions').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalImc == null || finalImc == typeof 'undefined')
            finalImc = $('#futureBenefitsIncreaseMemberContributions').slider("option", "value");
        futureBenefits.increaseMemberContributions = finalImc;

        var adjustmentOption = $("input:radio[name=futureBenefitsAdjustment]:checked");
        var adjustmentSlider = adjustmentOption.closest('tr').find('.fbAdjustmentSlider');

        futureBenefits.adjustmentType = adjustmentOption.val();

        finalAr = adjustmentSlider.parent().parent().find('.slider-text-value').data('finalValue');
        if (finalAr == null || finalAr == typeof 'undefined')
            finalAr = adjustmentSlider.slider("option", "value");
        futureBenefits.adjustmentRate = futureBenefits.adjustmentType == 'None' ? 0 : finalAr;

        var json = {
            futureBenefitsData: JSON.stringify(futureBenefits)
        };

        $.post('/Json/SetFutureBenefits', json, function (futureBenefitsData) {
           that.refresh(futureBenefitsData);
           that.changed.fire({ id: that.id, data: futureBenefitsData });
        });
    };
    
    var initialise = function (futureBenefitsData) {

        var updateCell = function(event, ui) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: ui.value
            });
        };

        var updateFutureBenefits = function(event, ui) {
            setFutureBenefits();
        };

        $("#futureBenefitsIncreaseMemberContributions").data("original", futureBenefitsData.setup.MemberContributionsIncrease);
        $(".fbAdjustmentSlider." + futureBenefitsData.setup.adjustmentType).data("original", futureBenefitsData.setup.AdjustmentRate);

        $('.fbAdjustmentSlider').add('#futureBenefitsIncreaseMemberContributions')
            .each(function() {
                $(this).slider({
                    animate: 400,
                    range: 'min',
                    min: $(this).data('min'),
                    max: $(this).data('max'),
                    value: 0,
                    step: $(this).data('increment'),
                    slide: updateCell,
                    //change: updateCell,
                    stop: updateFutureBenefits,
                    disabled: !futureBenefitsData.enabled,
                    create: function () { $(this).slider('setTextValueElement') },
                    elementSelector: $(this),
                    textElement: $(this).parent().siblings().last()
                });
            });

        $('input:radio[name=futureBenefitsAdjustment]').change(function () {

            var adjustmentOption = $(this);
            var adjustmentSlider = adjustmentOption.closest('tr').find('.fbAdjustmentSlider');

            $('.fbAdjustmentSlider').not(adjustmentSlider).hide()
                .each(function () {
                    $(this)
                        .slider('value', $(this).data('default'))
                        .closest('tr').find('.values span').hide();
                });

            adjustmentSlider.show()
                .closest('tr').find('.values span').show();
            
            setFutureBenefits();
        });

        $('#resetFutureBenefits').click(function() {
            reset();
        });

        initialised = true;
    };

    var setMode = function (readonly) {
        $('.fbAdjustmentSlider').add('#futureBenefitsIncreaseMemberContributions')
            .each(function () {
                $(this).slider({ disabled: readonly });

                if (readonly == true)
                    $(this).slider('disableEditableElement', this);

                $(this).closest("td").removeClass("greySlider").addClass(readonly ? "greySlider" : "");                    
            });
        $("#futureBenefitsInputTable").find("input[type=radio]").attr("disabled", readonly);
        $("#resetFutureBenefits").toggle(!readonly);
    };
    
    this.reInit = function () {
        initialised = false;
    };
    
    this.refresh = function (futureBenefitsData) {

        if (!initialised)
            initialise(futureBenefitsData);

        // results
        $('.futureBenefitsAnnualCost').html(fusion.util.formatCurrency(futureBenefitsData.annualCost));
        $('.futureBenefitsContributionRate').html((futureBenefitsData.contributionRate * 100).toFixed(2) + '%');

        $('.futureBenefitsEmployeeRate').html((futureBenefitsData.employeeRate * 100).toFixed(2) + '%');
        $('.futureBenefitsEmployerRate').html((futureBenefitsData.employerRate * 100).toFixed(2) + '%');
        $('#futureBenefitsChangeInAnnualCost').html(fusion.util.formatCurrency(futureBenefitsData.changeInAnnualCost));
        $('#futureBenefitsChangeInEmployerRate').html( (futureBenefitsData.changeInEmployerRate * 100).toFixed(2) + '%');
        $('#futureBenefitsChangeInLiabilities').html(fusion.util.formatCurrency(futureBenefitsData.changeInLiabilities));

        // New results for Contracting-Out FUSN-616 : Kam 01/08/2014
        $('.futureBenefitsServiceCost').html(fusion.util.formatCurrency(futureBenefitsData.futureBenefitsServiceCost));
        $('.futureBenefitsContRateEstimate').html((futureBenefitsData.futureBenefitsContRateEstimate * 100).toFixed(2) + '%');
        $('.futureBenefitsEstimatedBenefitReduction').html((futureBenefitsData.futureBenefitsEstimatedBenefitReduction * 100).toFixed(2) + '%');

        // parameters
        $('#futureBenefitsIncreaseMemberContributions').slider('value', futureBenefitsData.increaseMemberContributions);
        $('#futureBenefitsIncreaseMemberContributions').slider('setTextValueElement', futureBenefitsData.increaseMemberContributions);

        var adjustmentOption = $('input[name=futureBenefitsAdjustment][value=' + futureBenefitsData.adjustmentType + ']');
        adjustmentOption.prop("checked", true);

        var adjustmentSlider = adjustmentOption.closest('tr').find('.fbAdjustmentSlider');

        $('.fbAdjustmentSlider').not(adjustmentSlider).hide()
                .each(function () {
                    $(this)
                        .slider('value', $(this).data('default'))
                        .slider('setTextValueElement', $(this).data('default'))
                        .closest('tr').find('.values span').hide();
                });

        adjustmentSlider.show()
            .slider('value', futureBenefitsData.adjustmentRate)
            .closest('tr').find('.values span').show();
        adjustmentSlider.slider('setTextValueElement', futureBenefitsData.adjustmentRate);

        setMode(!futureBenefitsData.enabled);        
    };
};


