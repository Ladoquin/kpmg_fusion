﻿(function ($) {

    var greyedOutOpacity = 0.2; //alpha rgba component to set greyed-out pie items to
    var slideDownSpeed = 400;   //how quickly to expand breakdown panels
    var slideUpSpeed = 200;     //how quickly to collapse expanded breakdown panels

    $.fn.addLegend = function (options) {

        var itemContainerSelector;  //class name for the div containing each data item - dependent on required position of legend
        var $legend;
        var $chart = this.highcharts();
        var cid = this.attr("id");

        if (typeof $chart == "undefined") {
            return;
        }
                
        options = options || {};

        var lid = "legend-expando-" + cid;

        if ($("#" + lid).length > 0)
            $("#" + lid).remove();

        if (typeof options.alignment != "undefined" && options.alignment == "left") {
            $legend = $("<div id='" + lid + "' class='legend-expando-leftaligned'></div>");
            $('#' + cid).before($legend);
            itemContainerSelector = "legend-expando-item-container-leftaligned";
        }
        else if (typeof options.alignment != "undefined" && options.alignment == "right") {
            $legend = $("<div id='" + lid + "' class='legend-expando-rightaligned'></div>");
            $('#' + cid).before($legend);
            itemContainerSelector = "legend-expando-item-container-rightaligned";
        }
        else {
            $legend = $("<div id='" + lid + "' class='legend-expando'></div>");
            $('#' + cid).after($legend);
            itemContainerSelector = "legend-expando-item-container";
        };

        var html = "";
        for (var seriesIdx = 0; seriesIdx < $chart.series.length; seriesIdx++) {
            var data = $chart.series[seriesIdx].data;
            for (var i = 0; i < data.length; i++) {
                var custom = typeof data[i].custom != "undefined";
                if (!itemIsHidden(data[i])) {
                    var expandable = typeof data[i].breakdown != "undefined" && data[i].breakdown != null;
                    html += "<div data-series='" + seriesIdx + "' data-id='" + i + "' data-expandable='" + expandable + "' class='" + itemContainerSelector + "'>"; //opened 1
                    html += "<div style='background-color:" + data[i].color + "' class='legend-expando-colorbox " + (expandable ? "legend-expando-colorbox-expandable" : "") + "'>&nbsp;</div>";
                    html += "<div class='legend-expando-data-item'>" + data[i].name; //opened 2
                    if (custom && typeof data[i].custom.showAsPercentage != "undefined" && data[i].custom.showAsPercentage) {
                        //spec currently says to show no amount label
                    }
                    else {
                        html += " (" + fusion.util.formatCurrencyAbsolute(data[i].y) + ")";
                    }
                    html += "</div>"; //closed 2
                    html += "</div>"; //closed1
                    if (expandable) {
                        html += "<div class='legend-expando-breakdown-panel-container'>"; //opened 1
                        html += "<div class='legend-expando-breakdown-panel-topbar'></div>";
                        html += "<div class='legend-expando-breakdown-panel'>"; //opened 2

                        html += "<table class='legend-expando-breakdown-barchart'>";
                        html += "<thead>";
                        html += "<tr><th class='legend-expando-breakdown-panel-title'>" + data[i].name + "</th><th class='legend-expando-breakdown-panel-title-amount'>" + fusion.util.formatCurrencyAbsolute(data[i].y) + "</th></tr>";
                        html += "</thead>";
                        for (var j = 0; j < data[i].breakdown.length; j++) {
                            var proportion = Math.round(data[i].breakdown[j].y / data[i].y * 100);
                            html += "<tr>";
                            html += "<td class='legend-expando-breakdown-barchart-label'>" + data[i].breakdown[j].name + " (" + fusion.util.formatCurrencyAbsolute(data[i].breakdown[j].y) + ")</td>";
                            html += "<td class='legend-expando-breakdown-barchart-bar'><div style='background-color: " + data[i].color + "; width:" + proportion + "%'>&nbsp;</div></td>";
                            html += "</tr>";
                        }
                        html += "</table>";

                        html += "</div>"; //closed 2
                        html += "</div>"; //closed 1
                    }
                }
            }
            html += "<div style='clear:left; height:10px'></div>";
        }
        html += "</div>";
        $legend.append(html);        

        $legend.on("click", { cid: cid, lid: lid }, function (event) {

            var $legend = $("#" + lid);

            var x = legend;
            x.$chart = $("#" + cid).highcharts();
            x.$legend = $legend;

            var parent = $(event.target).parent();

            if (typeof parent.attr("data-id") != "undefined") { //user might have clicked in gap between divs

                var seriesIdx = parent.data("series");
                var id = parent.data("id");
                var expandable = parent.data("expandable");
                var deselecting = $chart.series[seriesIdx].data[id].selected == true;

                if (expandable && $legend.css("min-height") == "0px") {
                    $legend.animate({ 'min-height': ($legend.height() + 195) + "px" });
                }

                if (!deselecting) { //ie, display one item as selected and expand it if it has a breakdown section

                    x.greyAll();
                    x.collapseAll(function () {
                        if (expandable) {
                            x.expandSelected(seriesIdx, id, itemContainerSelector);
                        }
                    }, itemContainerSelector);
                    x.unboldAll();
                    x.ungreySelected(seriesIdx, id);
                    x.boldSelected(seriesIdx, id, itemContainerSelector);

                } else { //ie, turn all selections off
                    x.ungreyAll();
                    x.collapseAll(undefined, itemContainerSelector);
                    x.unboldAll();
                }

                $chart.series[seriesIdx].data[id].select(null, false); //null to enable point toggling, false to not allow multiple points to be selected

                x.updateChart();
            }

        });
    }

    var legend = {

        $chart: {},
        $legend: {},
        colorToRGB: function (color) {
            return color.substring(0, 1) == "#" ? fusion.util.hexToRGB(color) : fusion.util.rgbFromCSS(color);
        },
        greyAll: function () {
            for (var i = 0; i < this.$chart.series.length; i++) {
                for (var j = 0; j < this.$chart.series[i].data.length; j++) {
                    var data = this.$chart.series[i].data[j];
                    if (!itemIsHidden(data)) {
                        var rgba = this.colorToRGB(data.options.color);
                        rgba.push(greyedOutOpacity);
                        this.$chart.series[i].data[j].options.color = fusion.util.rgbaToCSS(rgba);
                    }
                }
            }
        },
        ungreyAll: function () {
            for (var i = 0; i < this.$chart.series.length; i++) {
                for (var j = 0; j < this.$chart.series[i].data.length; j++) {
                    var data = this.$chart.series[i].data[j];
                    if (!itemIsHidden(data)) {
                        var rgb = this.colorToRGB(data.options.color);
                        this.$chart.series[i].data[j].options.color = fusion.util.rgbToCSS(rgb);
                    }
                }
            }
        },
        ungreySelected: function (i, j) {
            this.$chart.series[i].data[j].options.color = fusion.util.rgbToCSS(this.colorToRGB(this.$chart.series[i].data[j].options.color));
        },
        updateChart: function () {
            for (var i = 0; i < this.$chart.series.length; i++)
                this.$chart.series[i].update();
        },
        collapseAll: function (callback, itemContainerSelector) {
            var $expanded = this.$legend.find(".legend-expando-colorbox-expanded");
            if ($expanded.length > 0) {
                $expanded.removeClass("legend-expando-colorbox-expanded").addClass("legend-expando-colorbox-expandable");
                $expanded.closest("." + itemContainerSelector).find(".legend-expando-data-item").css("color", "").css("font-weight", "");
                this.$legend.find(".legend-expando-breakdown-panel-container:visible").animate({
                    height: "toggle"
                }, {
                    duration: slideUpSpeed,
                    queue: false,
                    complete: function () {
                        if (typeof callback != "undefined")
                            callback();
                    }
                });
            }
            else {
                if (typeof callback != "undefined")
                    callback();
            }
        },
        boldSelected: function (i, j, itemContainerSelector) {
            var $container = this.$legend.find("div." + itemContainerSelector + "[data-series=" + i + "][data-id=" + j + "]");
            $container.find(".legend-expando-data-item")
                .css("color", $container.find(".legend-expando-colorbox").css("background-color"))
                .css("font-weight", "bold");
        },
        unboldAll: function () {
            this.$legend.find(".legend-expando-data-item").css("color", "").css("font-weight", "");
        },
        expandSelected: function (i, j, itemContainerSelector) {
            var $container = this.$legend.find("div." + itemContainerSelector + "[data-series=" + i + "][data-id=" + j + "]");
            var $colorbox = $container.find(".legend-expando-colorbox");
            $colorbox.removeClass("legend-expando-colorbox-expandable").addClass("legend-expando-colorbox-expanded");
            $container.next(".legend-expando-breakdown-panel-container").animate({
                height: "toggle"
            }, {
                duration: slideDownSpeed,
                queue: false
            });
        }
    };
    var itemIsHidden = function (item) {
        return typeof item.custom != "undefined" && typeof item.custom.hidden != "undefined" && item.custom.hidden;
    }

}(jQuery));