﻿$(function () {
    $(document).ready(function () {

        // set favourite drop down
        var favIconDiv = $("#favouriteIcon");
        //var ajaxLoader = $("#ajaxLoader");
        var inProcess = false;
        var favouriteList = $("#favouriteList");

        var setFavIcon = function (imgType) {
            var imgUrl = "../Images/favIcon-" + imgType + ".png";
            favIconDiv.css('background-image', 'url(' + imgUrl + ')');
        };
        
        var isFavourite = function(){
            return favIconDiv.data("type") == "gold";
        };

        var init = function () {
            setFavIcon(favIconDiv.data("type"));
            favIconDiv.prop('title', isFavourite() ? "Remove from favourites" : "Add to favourites");
            //ajaxLoader.css('visibility', 'hidden');
        };

        favIconDiv.click(function () {
            if (inProcess)
                return false;
            inProcess = true;
            var schemeId = favIconDiv.data("schemeid");
            //setFavIcon('');
            //ajaxLoader.css('visibility', 'visible');
            $.post("/Account/AddRemoveFavouriteScheme", { SchemeId: schemeId, removeFavourite: favIconDiv.data("type") == "gold" }, function (data) {
                
                //setTimeout(function () {
                    if (data.result) {
                        favIconDiv.data("type", isFavourite() ? "silver" : "gold");
                        init();
                        addRemoveFromList(data.SchemeId, data.schemeName);
                    }
                    inProcess = false;
                    //ajaxLoader.css('visibility', 'hidden');
                //}, 700); 
           });
        });

        var addRemoveFromList = function (schemeId, schemeName) {
            if ($("#schemeSelectorMenu") == undefined)
                return false;
            var isAdd = schemeName != "";
            if (isAdd && favouriteList.css('display') == 'block')
                $("#schemeSelectorMenu").append('<li data-schemeid="' + schemeId + '"><a style="width: 100%; margin:0" href="#" data-selected="true">' + schemeName + '</a></li>');
            else if(!isAdd)
            {
                var liItem = $('li[data-schemeid="' + schemeId + '"]');
                liItem.remove();
            }
            
        };

        //favIconDiv.hover(function () {
        //    if (inProcess)
        //        return false;
        //    setFavIcon(isFavourite() ? "remove" : "add");
        //}, function () {
        //    if (inProcess)
        //        return false;
        //    setFavIcon(favIconDiv.data("type"));
        //});

        init();
    });
});