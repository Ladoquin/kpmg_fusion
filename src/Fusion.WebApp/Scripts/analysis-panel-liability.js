﻿fusion.analysis.LiabilityPanel = function () {

    var that = this;
    var _init = false;
    var cachedDonutData = {};
    var donutRefreshedWhileOffScreen = false;
    var tabler = new fusion.util.LiabilitiesTabler("liabilityAssumptionsTable");
    var pensionIncreaseTabler = new fusion.util.LiabilitiesTabler("pension-increase-assumptions-table");
    var donutHelper = new fusion.util.DonutHelper();
    var donut = fusion.charts.donutHandler();
    var donutSpecOpts = {
        chartDivId: 'liabilityBreakdownPie',
        legendAlign: 'right',
        height: 200,
        marginTop: 20,
        marginBottom: 20
    };
    var fixedPenIncKeys = [];

    this.changed = $.Callbacks();

    var init = function (data) {

        var setup = [];
        $.each(data.assumptions, function (idx, o) {
            setup.push({ Key: o.Key, Label: o.Label, Value: o.Value.Before });
            $("#liabilityAssumptionsTable #tr-" + o.Key + " td:first > div.assumptionsSlider").data("original", o.Value.Before);
        });
        tabler.refresh(setup, true);

        if (data.basisBondYield != null) {
            $("#gilt-yield-index-table").show();
            $("#trGiltYieldIndex td").first().html(data.basisBondYield.label);
            $("#trGiltYieldIndex td").last().html(fusion.util.formatPercentage(data.basisBondYield.value, 2)); 
        } else {
            $("#gilt-yield-index-table").hide();
        }

        fixedPenIncKeys =
            $.map(
                $.grep(data.pensionIncreases, function (penInc) {
                    return penInc.IsFixed;
                }),
                function (penInc) {
                    return penInc.Key;
            });

        $.each(data.pensionIncreases, function (idx, o) {
            setup.push({ Key: o.Key, Label: o.Label, Value: o.Value.Before, IsVisible: o.IsVisible });
        });
        pensionIncreaseTabler.refresh(setup, true);

        $('#resetLiabilityAssumptions').unbind('click').click(function () {
            $.getJSON('/Json/ResetLiabilityAssumptions', function (data) {
                that.refresh(data);
                that.changed.fire({ id: that.id, data: data });
            });
        });

        if (!foo) {
            $("#pension-increases-lock").click(function () {
                if (this.checked) {
                    $.post("/Json/SetPensionIncreasesLocked", null, function (data) {
                        setPensionIncreasesLocked(data.pensionIncreases);
                        that.refresh(data);
                        that.changed.fire({ id: that.id, data: data });
                    });
                }
                else {
                    setPensionIncreasesUnlocked();
                    $.post("/Json/SetPensionIncreasesUnlocked", null);
                }
            });
            foo = true;
        }

        _init = true;
    };

    var foo = false;

    var setPensionIncreasesUnlocked = function () {
        $('#pension-increase-assumptions-table .assumptionsSlider').each(function (index) {            
            if ($.inArray(parseInt(this.id.replace('pensionIncrease_', '')), fixedPenIncKeys) == -1) {
                $(this).slider("enable");
                $(this).removeClass("readonly");
                $(this).closest("td").removeClass("greySlider");
                $(this).data("original", $(this).slider("value"));
                $(this).addTick();
            }
            else {
                $(this).closest("tr").attr("title", "This is a fixed pension increase and cannot be unlocked.");
            }
        });        
    };

    var setPensionIncreasesLocked = function (pensionIncreases) {
        $('#pension-increase-assumptions-table .assumptionsSlider').each(function (index) {
            $(this).closest("tr").attr("title", "");
            var key = parseInt(this.id.replace('pensionIncrease_', ''));
            if ($.inArray(key, fixedPenIncKeys) == -1) {
                $(this).slider("value", $.grep(pensionIncreases, function (pi) { return pi.Key == key; })[0].Value.After);
                $(this).slider("disable");
                $(this).addClass("readonly");
                $(this).closest("td").addClass("greySlider");
                $(this).slider('setTextValueElement');
                $(this).removeTick();
            }
        });                
    };

    var drawDonut = function (data) {

        var series = donutHelper.getLiabilitySeriesData(data.liabilitiesBreakdown, { innerSize: 70 });

        var spec = donutHelper.getLiabilitySpec(data.liabilitiesBreakdown, donutSpecOpts);

        donut.redraw(series, spec, true);

        // rendering bug - if the chart is refreshed while not on screen then legends are lost
        // so cache this data and allow the chart to be redrawn if necessary when it's shown on screen again.
        donutRefreshedWhileOffScreen = !$("#" + spec.chartDivId).is(":visible");
        cachedDonutData.series = series;
        cachedDonutData.spec = spec;
    };

    this.tabActivated = function () {
        if (donutRefreshedWhileOffScreen) {
            donutRefreshedWhileOffScreen = false;
            donut.redraw(cachedDonutData.series, cachedDonutData.spec, true);
        };
    };

    this.reInit = function () {
        _init = false;
    };

    var getAssumptionValuesFromSliderCollection = function (sliderTableId) {
        var assumptions = {};

        $('#' + sliderTableId + ' .assumptionsSlider').each(function (index) {
            var finalValue = null;
            if ($(this).slider().closest("tr")[0].style.display == "none") {
                assumptions[$(this).attr('id')] = $(this).closest("tr").data("original");
            }
            else {
                finalValue = $(this).parent().parent().find('.slider-text-value').data('finalValue');

                if (finalValue == null || finalValue == typeof 'undefined')
                    finalValue = $(this).slider("option", "value");

                assumptions[$(this).attr('id')] = finalValue;
            }
        });

        return assumptions;
    };

    this.setLiabilityAssumptions = function () {
        var liabilityAssumptions = getAssumptionValuesFromSliderCollection("liabilityAssumptionsTable");
        var json = {
            liabilityAssumptions: JSON.stringify(liabilityAssumptions)
        };

        $.post('/Json/SetLiabilityAssumptions', json, function (data) {
            that.refresh(data);
            that.changed.fire({ id: that.id, data: data });
        });
    };

    this.setPensionIncreaseAssumptions = function () {
        var pensionIncreaseAssumptions = {};
        $.each(getAssumptionValuesFromSliderCollection("pension-increase-assumptions-table"), function(i, val) {
            pensionIncreaseAssumptions[i.replace('pensionIncrease_', '')] = val;
        });
        var json = {
            pensionIncreaseAssumptions: JSON.stringify(pensionIncreaseAssumptions)
        };
        $.post('/Json/SetPensionIncreaseAssumptions', json, function (data) {
            that.refresh(data);
            that.changed.fire({ id: that.id, data: data });
        });
    };

    this.refresh = $.proxy(function (data) {

        if (!_init) init(data);

        drawDonut(data);

        if (data.pensionIncreasesActive) {
            $('#liabilityAssumptionsTable .assumptionsSlider').removeClass("readonly");
            $('#liabilityAssumptionsTable .assumptionsSlider').closest("td").removeClass("greySlider");
        };

        var updateLinkedCell = function (event, ui) {
            var cls = "." + $.grep($(this)[0].className.split(" "), function (el) { return el.endsWith("-linked"); })[0];
            $.each($("#liabilityAssumptionsTable .assumptionsSlider" + cls), function () {
                $(this).slider("updateSliderValue", {
                    el: $(this),
                    value: ui.value
                });
            });
            $(cls).slider("value", ui.value);
        };

        var updateCell = function (event, ui) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: ui.value
            });
        };

        $('#liabilityAssumptionsTable .assumptionsSlider').each(function (index) {
            that.initAssumptionSlider({
                sliderElement: this, 
                assumptions: data.assumptions, 
                basis: data.basis, 
                hiddenAssumptions: data.hiddenAssumptions,
                updateCell: updateCell,
                updateLinkedCell: updateLinkedCell,
                setAssumptions: that.setLiabilityAssumptions
            });
        });
        $('#pension-increase-assumptions-table .assumptionsSlider').each(function (index) {
            that.initAssumptionSlider({
                sliderElement: this,
                assumptions: data.pensionIncreases,
                basis: data.basis,
                updateCell: updateCell,
                setAssumptions: that.setPensionIncreaseAssumptions
            });
        });

        if (data.pensionIncreasesActive) {
            setPensionIncreasesCheckboxStatus(false);
            setPensionIncreasesUnlocked();
        }
        else {
            setPensionIncreasesCheckboxStatus(true);
            setPensionIncreasesLocked(data.pensionIncreases);
        };

        $.each(data.pensionIncreases, function (idx, pinc) {
            var $tr = $('#pension-increase-assumptions-table tbody tr#tr-' + pinc.Key);
            $tr.css("display", pinc.IsVisible ? "table-row" : "none");
            $tr.data("original", pinc.Value.After); //means that a non-visible row will post a value
        });

        $('.analysisPeriodEnd', $("#analysis_liabilities")).html(Highcharts.dateFormat('%d/%m/%Y', data.date));
    }, this);

    var setPensionIncreasesCheckboxStatus = function(checked) {
        $("#pension-increases-lock").prop("checked", checked);
    };

    this.initAssumptionSlider = function (args) {

        var sliderElement = args.sliderElement,
            assumptions = args.assumptions,
            basis = args.basis,
            hiddenAssumptions = args.hiddenAssumptions,
            updateCell = args.updateCell,
            updateLinkedCell = args.updateLinkedCell,
            setAssumptions = args.setAssumptions,
            key = $(sliderElement).closest("tr")[0].id.replace("tr-", ""),
            assumption = $.grep(assumptions, function (o) {
                return o.Key == key;
            })[0];

        if (assumption != null) {

            var value = assumption.Value.After;

            $(sliderElement).closest("tr").show();

            $(sliderElement).slider({
                animate: 400,
                range: 'min',
                min: $(sliderElement).data('min'),
                max: $(sliderElement).data('max'),
                value: value,
                step: $(sliderElement).data('increment'),
                slide: $(sliderElement).hasClass(basis.type.toLowerCase() + "-linked") ? updateLinkedCell.bind(sliderElement) : updateCell,
                stop: setAssumptions,
                elementSelector: $(sliderElement),
                textElement: $(sliderElement).parent().siblings().last(),
                submitCallback: $(sliderElement).hasClass(basis.type.toLowerCase() + "-linked") ? function (event, ui, scope) {
                    updateLinkedCell.call(scope, event, ui);
                } : null
            });
            $(sliderElement).slider('setTextValueElement');

            if ($(sliderElement).hasClass("readonly")) {
                $(sliderElement).slider("disable");
                $(sliderElement).slider('disableEditableElement', sliderElement);
                $(sliderElement).parent().siblings().last().find('.slider-text-value').attr('contenteditable', 'false');
            };
        }
        else {
            var originalVal = 0;
            var hiddenAssumption = $.grep(hiddenAssumptions, function (o) {
                return o.Key == key;
            })[0];
            if (hiddenAssumption != undefined)
                originalVal = hiddenAssumption.Value;
            $(sliderElement).closest("tr").data("original", originalVal)
            $(sliderElement).closest("tr").hide();
        }
    };
};

