﻿fusion.util.AssumptionsPairHandler = function (ids) {

    var _ids = ids;

    var liabilityDonutHeight = $("#" + _ids.liabilityChartDivId).height();
    var assetDonutHeight = $("#" + _ids.assetChartDivId).height();
    var containerHeight = Math.max($("#" + _ids.liabilityChartDivId).closest("div.chartcontainer").height(), $("#" + _ids.assetChartDivId).closest("div.chartcontainer").height());
    var donutHelper = new fusion.util.DonutHelper();
    
    this.draw = function (data, options) {

        options = options || {};

        var liabSeries = donutHelper.getLiabilitySeriesData(data.liability);
        var assetSeries = donutHelper.getAssetSeriesData(data.asset, { showAssetClasses: options.showAssetClasses });

        var liabSpec = donutHelper.getLiabilitySpec(data.liability, {
            chartId: _ids.liabilityChartDivId,
            height: liabilityDonutHeight,
            align: 'center',
            verticalAlign: 'bottom'
        });

        var assetSpec = {
            chartDivId: _ids.assetChartDivId,
            height: assetDonutHeight,
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                splitSeries: true,
                showValues: true
            }
        }

        setSharedVerticalAlignmentSettings({
            donut1: { series: assetSeries, spec: assetSpec },
            donut2: { series: liabSeries, spec: liabSpec }
        });

        var extraSpaceRequired = Math.max(liabSpec.height - liabilityDonutHeight, assetSpec.height - assetDonutHeight);
        if (extraSpaceRequired > 0) {
            $("#" + liabSpec.chartDivId).closest("div.chartcontainer").height(containerHeight + extraSpaceRequired);
        };

        fusion.charts.donutHandler().redraw(liabSeries, liabSpec);
        fusion.charts.donutHandler().redraw(assetSeries, assetSpec);
    };

    var setSharedVerticalAlignmentSettings = function (params) {

        var donut1 = params.donut1;
        var donut2 = params.donut2;

        var getLegendLinesNo = function (series, spec) {
            var lines = series[0].data.length;
            if (series.length > 1) {
                lines += series[1].data.length;
                if (spec.legend.splitSeries && series[0].data.length > 0 && series[1].data.length > 0) {
                    lines++;
                }
            }
            return lines;
        };

        var donut1LegendLines = getLegendLinesNo(donut1.series, donut1.spec);
        var donut2LegendLines = getLegendLinesNo(donut2.series, donut2.spec);

        var legendLineHeight = Math.ceil(fusion.charts.donutHelper.getLegendLineHeight());

        var legendSpace = (Math.max(donut1LegendLines, donut2LegendLines) * legendLineHeight) + 10;

        donut1.spec.marginBottom = legendSpace;
        donut2.spec.marginBottom = legendSpace;

        donut1.spec.height += legendSpace;
        donut2.spec.height += legendSpace;

        if (donut1LegendLines > donut2LegendLines) {
            donut2.spec.legend.y = 0 - (donut1LegendLines - donut2LegendLines) * legendLineHeight;
        }
        else if (donut2LegendLines > donut1LegendLines) {
            donut1.spec.legend.y = 0 - (donut2LegendLines - donut1LegendLines) * legendLineHeight;
        };

        return this;
    };
};