﻿(function ($) {

    $.fn.addTick = function (options) {

        options = options || {};
        options.titleFormatter = options.titleFormatter || function (val, units) {
            if (!options.disableTitle) {
                if (units !== undefined && units == "int") {
                    return val.toString();
                }
                else if (units !== undefined && units == "years") {
                    return val.toString() + "yrs";
                }
                else {
                    return (val * 100).toFixed(2).toString() + "%";
                }
            }
            return "";
        };

        for (var i = 0; i < this.length; i++) {
            var el = this[i];
            var existing = $(el).find(".ui-slider-tick-mark");
            if (existing.length === 0 || options.overwrite || options.allowMultiple) {
                if (options.overwrite) {
                    existing.remove();
                };
                at = options.at || $(el).data("original");
                if (at !== undefined) {
                    var tooltip = $(el).data("original-tooltip") || at;
                    var min = $(el).slider("option", "min");
                    var max = $(el).slider("option", "max")
                    var tickAt = (at - min) / (max - min) * $(el).width();
                    if (!$(el).hasClass("readonly") || $(el).hasClass("allow-tick")) {
                        $("<span class='ui-slider-tick-mark' title='" + options.titleFormatter(tooltip, $(el).data("units")) + "'></span>").css("left", tickAt + "px").appendTo(el);
                    };
                    $(".ui-slider-tick-mark", $(el)).tooltip({
                        tooltipClass: 'slider-tooltip',
                    });
                }
            }
        };

        return this;
    };

    $.fn.removeTick = function () {

        for (var i = 0; i < this.length; i++) {
            var el = this[i];
            var existing = $(el).find(".ui-slider-tick-mark");
            if (existing.length > 0) {
                existing.remove();
                $(el).removeData("original");
            };
        };

        return this;
    };

}(jQuery));