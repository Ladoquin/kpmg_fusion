﻿$(function () {

    fusion.util.headerSelectMenu.init();

    var schemeSelected = function (schemeName) {
        $.get("/Scheme/CheckFavouriteScheme", { SchemeName: schemeName }, function (data) {
            updateFavouriteIcon(data.isFavourite, data.schemeId);
        });
    };

    var updateFavouriteIcon = function (isFavourite, schemeId) {
        var favIconDiv = $("#favouriteIcon");
        var imgType = (isFavourite ? "gold" : "silver");
        var imgUrl = "../Images/favIcon-" + imgType + ".png";
        favIconDiv.css('background-image', 'url(' + imgUrl + ')');
        favIconDiv.data("type", imgType);
        favIconDiv.data("schemeid", schemeId);
        favIconDiv.prop('title', isFavourite ? "Remove from favourites" : "Add to favourites");
    };

    $(".schemeNameSearch").schemeSearcher(schemeSelected);

});

