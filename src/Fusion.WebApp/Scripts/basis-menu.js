
$(function() {
    
    $('#currentBasis').add('.basisSelector').click(function () {
        fusion.util.showBasisMenu();
        $('#technicalProvisionsBasisMenu').slideUp(100);
        return false;
    });

    $('#technicalProvisionsBasis').add('.techProvBasisSelector').click(function () {
        fusion.util.showTechnicalProvisionsBasisMenu();
        $('#currentBasisMenu').slideUp(100);
        return false;
    });
    
    if ($('#currentBasisMenu li a').length > 0) { //TODO this script is included on pages it's not needed
        if ($._data($('#currentBasisMenu li a')[0], "events") == undefined) {
            $('#currentBasisMenu li a').click(function (e) {
                fusion.selectBasis($(this).data('id'));
                $('#currentBasisMenu').fadeOut(200);
                return false;
            });
        }
    }
    if ($('#technicalProvisionsBasisMenu li a').length > 0) {
        if ($._data($('#technicalProvisionsBasisMenu li a')[0], "events") == undefined) {
            $('#technicalProvisionsBasisMenu li a').click(function (e) {
                fusion.selectJPFundingBasis({ id: $(this).data('id'), basis: $(this).data('basis'), name: $(this).text() });
                $('#technicalProvisionsBasisMenu').trigger("technicalProvisionsBasisSelected", $(this).data('id'));
                $('#technicalProvisionsBasisMenu').fadeOut(200);
                return false;
            });
        }
    }

    $('body').click(function () {
        $('#currentBasisMenu').slideUp(100);
        $('#technicalProvisionsBasisMenu').slideUp(100);
    });

    fusion.basisChangedEvent.add(function (newBasis) {
        $('#currentBasis').data('masterbasisid', newBasis.masterBasisId);
        $('#currentBasis').data('basistypeid', newBasis.typeId);
        $('#currentBasis').html(newBasis.name);
    });

    fusion.fundingBasisChangedEvent.add(function (basis) {
        $('#technicalProvisionsBasis').html(basis.name);
    });

});