﻿fusion.MiniEvolutionLens = function () {

    var valuationChartPast;
    var valuationChartPresent;

    var balanceChartPast;
    var balanceChartPresent;

    var evolutionData;
    
    this.initialise = function () {

        valuationChartPast = this.instantiateSubChart('miniEvolutionValuationPastChartDiv', [{
            name: 'Assets',
            color: '#ff8d3d',
            data : [0]
        }, {
            name: 'Liabilities',
            color: '#8e258d',
            data: [0]
        }]);

        valuationChartPresent = this.instantiateSubChart('miniEvolutionValuationPresentChartDiv', [{
            name: 'Assets',
            color: '#ff8d3d',
            data: [0]
        }, {
            name: 'Liabilities',
            color: '#8e258d',
            data: [0]
        }]);

        balanceChartPast = this.instantiateSubChart('miniEvolutionBalancePastChartDiv', [{
            name: 'Surplus',
            color: '#6a7f10',
            data: [0]
        }, {
            name: 'Deficit',
            color: '#9e3039',
            data: [0]
        }]);

        balanceChartPresent = this.instantiateSubChart('miniEvolutionBalancePresentChartDiv', [{
            name: 'Surplus',
            color: '#6a7f10',
            data: [0]
        }, {
            name: 'Deficit',
            color: '#9e3039',
            data: [0]
        }]);

        var chart = this;

        $('#miniEvolutionPeriods span').click(function () {
            chart.choosePeriod($(this));
        });
    };


    this.choosePeriod = function(periodButton) {
        var period = periodButton.data('period');
        periodButton.siblings().removeClass('miniEvolutionPeriodButtonSelected');
        periodButton.addClass('miniEvolutionPeriodButtonSelected');

        this.changeChangeIndicators(period);
        this.changeChartPeriod(evolutionData[period], valuationChartPast, balanceChartPast, $('#miniEvolutionSurplusPastTitle'), $('#miniEvolutionDeficitPastTitle'));
    };    


    this.changeChangeIndicators = function(period) {

        var calculatePercentage = function (a, b) {
            if (a == 0 || b == 0)
                return 0;
            return (a / b) - 1;
        };

        $('.miniEvolutionDate.left').html(evolutionData[period].Date);
        $('.miniEvolutionDate.right').html(evolutionData.d0.Date);

        var assetDifference = evolutionData.d0.Asset.Value - evolutionData[period].Asset.Value;
        var assetPercentage = calculatePercentage(evolutionData.d0.Asset.Value, evolutionData[period].Asset.Value);
        var assetDown = (assetDifference < 0);

        $('#assetIndicatorAmount').toggleClass('up-indicator-good', !assetDown);
        $('#assetIndicatorAmount').toggleClass('down-indicator-bad', assetDown);

        $('#assetIndicatorAmount').html(fusion.util.formatCurrencyAbsolute(assetDifference));
        $('#assetIndicatorPercentage').html('(' + fusion.util.formatPercentage(assetPercentage)+ ')');


        var liabilityDifference = evolutionData.d0.Liability.Value - evolutionData[period].Liability.Value;
        var liabilityPercentage = calculatePercentage(evolutionData.d0.Liability.Value, evolutionData[period].Liability.Value);
        var liabilityDown = (liabilityDifference >= 0);

        $('#liabilityIndicatorAmount').toggleClass('up-indicator-bad', liabilityDown);
        $('#liabilityIndicatorAmount').toggleClass('down-indicator-good', !liabilityDown);

        $('#liabilityIndicatorAmount').html(fusion.util.formatCurrencyAbsolute(liabilityDifference));
        $('#liabilityIndicatorPercentage').html('(' + fusion.util.formatPercentage(liabilityPercentage) + ')');


        var balanceDifference = evolutionData.d0.Balance.Value - evolutionData[period].Balance.Value;
        var balancePercentage = calculatePercentage(evolutionData.d0.Balance.Value, evolutionData[period].Balance.Value);
        var balanceDown = (balanceDifference < 0);

        $('#balanceIndicatorAmount').toggleClass('up-indicator-bad', balanceDown);
        $('#balanceIndicatorAmount').toggleClass('down-indicator-good', !balanceDown);

        $('#balanceIndicatorAmount').html(fusion.util.formatCurrencyAbsolute(balanceDifference));
        $('#balanceIndicatorPercentage').html('(' + fusion.util.formatPercentage(balancePercentage) + ')');

    };



    this.refresh = function (data) {
        evolutionData = data;

        var maximums = this.getMaximums(data);

        this.showHidePeriods(data);
        var period = this.getOldestPeriod(data);
        
        this.setChartBounds(maximums.maxValuation, valuationChartPast);
        this.setChartBounds(maximums.maxValuation, valuationChartPresent);
        this.setChartBounds(maximums.maxBalance, balanceChartPast);
        this.setChartBounds(maximums.maxBalance, balanceChartPresent);

        this.changeChartPeriod(data.d0, valuationChartPresent, balanceChartPresent, $('#miniEvolutionSurplusPresentTitle'), $('#miniEvolutionDeficitPresentTitle'));
        this.choosePeriod($('.miniEvolutionPeriodButton[data-period=' + period + ']'));

    };


    this.showHidePeriods = function(data) {
        $('#miniEvolutionPeriods span').each(function() {
            var period = $(this).data('period');
            $(this).toggle(data[period] != null);
        });
    };

    this.getOldestPeriod = function (data) {
        if (data.y1 != null) return 'y1';
        if (data.m6 != null) return 'm6';
        if (data.m3 != null) return 'm3';
        if (data.m1 != null) return 'm1';
        if (data.d1 != null) return 'd1';
        return 'd0';
    };

    this.changeChartPeriod = function(period, valuationChart, balanceChart, surplusTitle, deficitTitle) {

        valuationChart.series[0].points[0].update(period.Asset.Value);
        valuationChart.series[1].points[0].update(-period.Liability.Value);

        var surplus = 0;
        var deficit = 0;

        if (period.Balance.Value < 0) {
            deficit = period.Balance.Value;
            surplusTitle.text('');
            deficitTitle.text('Deficit');
        } else {
            surplus = period.Balance.Value;
            surplusTitle.text('Surplus');
            deficitTitle.text('');
        }
        
        balanceChart.series[0].points[0].update(surplus);
        balanceChart.series[1].points[0].update(deficit);
    };


    this.getMaximums = function (data) {
        return {
            maxValuation: Math.max(
                data.d0 != null ? data.d0.Asset.Value : 0,
                data.d1 != null ? data.d1.Asset.Value : 0,
                data.m1 != null ? data.m1.Asset.Value : 0,
                data.m3 != null ? data.m3.Asset.Value : 0,
                data.m6 != null ? data.m6.Asset.Value : 0,
                data.y1 != null ? data.y1.Asset.Value : 0,
                data.d0 != null ? data.d0.Liability.Value : 0,
                data.d1 != null ? data.d1.Liability.Value : 0,
                data.m1 != null ? data.m1.Liability.Value : 0,
                data.m3 != null ? data.m3.Liability.Value : 0,
                data.m6 != null ? data.m6.Liability.Value : 0,
                data.y1 != null ? data.y1.Liability.Value : 0
            ),
            maxBalance: Math.max(
                data.d0 != null ? Math.abs(data.d0.Balance.Value) : 0,
                data.d1 != null ? Math.abs(data.d1.Balance.Value) : 0,
                data.m1 != null ? Math.abs(data.m1.Balance.Value) : 0,
                data.m3 != null ? Math.abs(data.m3.Balance.Value) : 0,
                data.m6 != null ? Math.abs(data.m6.Balance.Value) : 0,
                data.y1 != null ? Math.abs(data.y1.Balance.Value) : 0
            )
        };
    };



    this.setChartBounds = function (maxValue, chart) {
        chart.yAxis[0].setExtremes(-maxValue, maxValue);
    };




    this.instantiateSubChart = function (renderTo, series) {
        return new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: renderTo,
                backgroundColor: 'none',
                plotBorderWidth: 0,
                plotBackgroundColor: 'white',
                margin: [26, 0, 26, 0],
                animation:1000
            },
            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: null,
            },
            xAxis: {
                title: {
                    text: null
                },
                labels: {
                    enabled: false
                },
                gridLineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                minorTickLength: 0,
                tickLength: 0
            },
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    enabled: false
                },
                gridLineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                minorTickLength: 0,
                tickLength: 0,

                stackLabels: {
                    useHTML: true,
                    enabled: true,
                    style: {
                        fontSize: '16px',
                        fontFamily: 'Lato, sans-serif',
                        color: '#3e3e3e',
                        lineHeight:'26px'
                    },
                    formatter: function () {
                        if (!this.total) {
                            return '';
                        }
                        return fusion.util.formatCurrencyAbsolute(this.total);
                    },
                },

            },

            legend: {
                enabled: false,
            },

            plotOptions: {
                series: {
                    stacking: 'normal',
                    pointPadding: 0,
                    groupPadding: 0,
                    borderWidth: 0,
                    shadow: false
                }
            },

            tooltip: {
                borderWidth: 0,
                borderRadius: 0,
                enabled: false,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                formatter: function () {
                    return this.series.name + ': ' + fusion.util.formatCurrencyAbsolute(this.y);
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },

            series: series
        });
    };





};
