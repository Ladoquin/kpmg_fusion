﻿(function ($) {

    /*
        The select control must have the following mark up. Only the class markup is necessary for functionality.
        TODO: render markup from plugin.

        <div id="varPeriodSelect" class="styleable-select h2">
            <div class="select-selected"></div>
            <div class="select-options">
                <div class="select-option selected" data-value="1">1 year</div>
                <div class="select-option" data-value="2">2 years</div>
                <div class="select-option" data-value="3">3 years</div>
                <div class="select-option" data-value="4">4 years</div>
                <div class="select-option" data-value="5">5 years</div>
            </div>
        </div>

        Example usage:

        $("#varPeriodSelect").styleableSelect({
            onChanged: function () {
                that.updateLens();
            }
        });

        Notes: 
         Width isn't dynmaic and should be set manually for #varPeriodSelect in this example.
         The selected item can be selected by using the css 'select' class on a 'select-option' but if you want 
         to drive selection from the server call the public select() method like this:

            this.$myStyleableSelect.select(displayOptions.time, false);   //...where displayOptions.time is the data-value of the option to select

    */

    $.fn.styleableSelect = function (options) {

        var settings = $.extend({
                highlightBackgroundColour: '#0f7bb9',
                defaultBackgroundColour: '#e7e7e7',
                highlightTextColor: 'White',
                defaultTextColor: 'Black',
                animationSpeed: 200,
                onChanged: {}
            }, options),
            $firstItem = $(this).find('.select-option').first(),
            $allItems = $(this).find('.select-options'),
            $initialSelection = $(this).find('.selected'),
            that = this;

        $(this).unbind();
        $(document).unbind("mousedown.styleableSelect");

        var highlight = function ($option) {
            $option.css('background-color', settings.highlightBackgroundColour);
            $option.css('color', settings.highlightTextColor);
        };

        var unhighlight = function ($option) {
            $option.css('background-color', settings.defaultBackgroundColour);
            $option.css('color', settings.defaultTextColor);
        };

        var selectOption = function ($option, fireOnChanged) {
            $(that).find('.select-selected .select-option').remove();
            unhighlight($option);
            $(that).find('.select-selected').append($option.clone());
            $(that).find('.selected').removeClass('selected');
            $option.addClass('selected');

            if (fireOnChanged != false) {
                settings.onChanged();
            }
        };

        var close = function () {
            $allItems.slideUp(settings.animationSpeed);
        };

        $(this).find('.select-option').on('mouseover',
            function () {
                unhighlight($(that).find('.select-option'));
                highlight($(this));
            });

        $(this).find('.select-option').on('click', function () {
            selectOption($(this));
        });

        selectOption($initialSelection, false);

        $(this).click(function () {
            if ($allItems.is(":hidden")) {
                unhighlight($(that).find('.select-option'));
                highlight($(that).find('.selected'));
                $allItems.slideDown(settings.animationSpeed);
            } else {
                close();
            }
        });

        $(this).bind("closeStyleableSelect", function () {
            close();
        });

        $('.styleable-select').on('mousedown', function (e) {
            $('.styleable-select').not($(this)).trigger("closeStyleableSelect"); //close everything else
            e.stopPropagation();
        });

        $(document).bind("mousedown.styleableSelect", function () {
            $('.styleable-select').trigger("closeStyleableSelect");//close everything
        });

        this.select = function (value, fireOnChanged) {
            var $optionToSelect = $(that).find('.select-options *[data-value="' + value + '"]').first();
            selectOption($optionToSelect, fireOnChanged);
        };

        this.getValue = function () {
            return $(that).find('.selected').data('value');
        };

        return this;
    };  
}(jQuery));