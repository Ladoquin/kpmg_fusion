﻿
var AnalysisOfSurplusClass = function() {

    this.chart = null;
    var chartMinVal = 0;
    var chartMaxVal = 0;
    var labelSpacerMultiplier = 1.2;    // to fit label outside bars

    var setMinMax = function (checkVal) {

        if (checkVal < chartMinVal)
            chartMinVal = checkVal;
        if (checkVal > chartMaxVal)
            chartMaxVal = checkVal;

        if (chartMinVal < 0 && (chartMinVal * -1) > chartMaxVal)
            chartMaxVal = chartMinVal * -1;
        
        if (chartMinVal < 0 && chartMinVal > (chartMaxVal * -1))
            chartMinVal = chartMaxVal * -1;
    };

    var resetMinMax = function() 
    {
        chartMinVal = 0;
        chartMaxVal = 0;
    }

    this.refresh = function (data) {
        var positiveColor = fusion.util.colorGradient('#6a7f10', '#ffffff', 0.4);
        var negativeColor = fusion.util.colorGradient('#9e3039', '#ffffff', 0.4);

        this.chart.xAxis[0].categories = [
            'Starting ' + (data.InitialDeficit < 0 ? 'deficit' : 'surplus'),
            'Interest on liabilities',
            'Discount rate / Change in inflation',
            'Liability experience',
            'Contributions',
            'Asset return',
            'Asset experience',
            'End ' + (data.InitialDeficit < 0 ? 'deficit' : 'surplus')
        ];

        // reset min max values;
        resetMinMax();

        setMinMax(data.InitialDeficit);

        this.chart.series[0].data[0].update({
            color: data.InitialDeficit < 0 ? '#9e3039' : '#6a7f10',
            y: data.InitialDeficit
        }, false);

        this.chart.series[0].data[1].update({
            color: data.InterestOnLiabilities < 0 ? negativeColor : positiveColor,
            y: data.InterestOnLiabilities
        }, false);

        setMinMax(data.InterestOnLiabilities);

        this.chart.series[0].data[2].update({
            color: data.MarketChange < 0 ? negativeColor : positiveColor,
            y: data.MarketChange
        }, false);

        setMinMax(data.MarketChange);

        this.chart.series[0].data[3].update({
            color: data.LiabExperience < 0 ? negativeColor : positiveColor,
            y: data.LiabExperience
        }, false);

        setMinMax(data.LiabExperience);

        this.chart.series[0].data[4].update({
            color: data.Contributions < 0 ? negativeColor : positiveColor,
            y: data.Contributions
        }, false);

        setMinMax(data.Contributions);

        this.chart.series[0].data[5].update({
            color: data.AssetReturn < 0 ? negativeColor : positiveColor,
            y: data.AssetReturn
        }, false);

        setMinMax(data.AssetReturn);

        this.chart.series[0].data[6].update({
            color: data.AssetExperience < 0 ? negativeColor : positiveColor,
            y: data.AssetExperience
        }, false);

        setMinMax(data.AssetExperience);

        this.chart.series[0].data[7].update({
            color: data.EndDeficit < 0 ? '#9e3039' : '#6a7f10',
            y: data.EndDeficit
        }, false);

        setMinMax(data.EndDeficit);

        this.chart.yAxis[0].update({
            min: chartMinVal * labelSpacerMultiplier,
            max: chartMaxVal * labelSpacerMultiplier
        });

        this.chart.redraw();
    };
};


$.Core.analysisOfSurplus = new AnalysisOfSurplusClass();

$(function() {

    $.Core.analysisOfSurplus.chart = new Highcharts.Chart({
        chart: {
            type: 'column',
            renderTo: 'analysisOfSurplusChartDiv',
            backgroundColor: 'none',
            inverted: true,
        },

        credits: false,
        exporting: {
            enabled: false
        },
        title: {
            text: null
        },
        xAxis: [{
            gridLineWidth: 1,
            categories: ['Starting surplus', 'Interest on liabilities', 'Discount rate / Change in inflation', 'Liability experience', 'Contribution', 'Asset return', 'Asset experience', 'End deficit'],
            plotBands: [{
                from: -0.5,
                to: 0.5,
                color: '#ffffff',
            },{
                from: 0.5,
                to: 3.5,
                color: fusion.util.colorGradient('#a83ea6', '#ffffff', 0.8),
            },{
                from: 3.5,
                to: 6.5,
                color: fusion.util.colorGradient('#e87424', '#ffffff', 0.8),
            },{
                from: 6.5,
                to: 7.5,
                color: '#ffffff',
            }],
            labels: {
                useHTML:true,
            },
        }, {
            gridLineWidth: 0,
            categories: ['', 'Liability impact', '', '', 'Asset impact', '', '', ''],
            labels: {
                y:45,
                useHTML: true,
                style: {
                    minWidth: '190px'
                },
            },
            opposite: true,
            linkedTo:0
        }],
        yAxis: {
            gridLineWidth: 1,
            maxPadding: 0.1,
            minPadding:0.1,
            title: {
                text: null
            },
            plotLines: [{
                value: 0,
                color: '#555',
                width: 2,
                zIndex: 10,
            }],
            labels: {
//                enabled: false,
                useHTML:true,
                formatter: fusion.util.formatChartCurrencyLabels
            },
        },
        plotOptions: {
            column: {
                dataLabels: {
                    useHTML:true,
                    enabled: true,
                    formatter: function() {
                        return fusion.util.formatCurrencyAbsolute(this.point.y);
                    }
                },
                pointPadding: 0,
                borderWidth: 0,
            }
        },
        tooltip: {
            hideDelay:0,
            borderWidth: 0,
            borderRadius: 0,
            backgroundColor: '#007c92',
            shadow: false,
            useHTML: true,
            followPointer: true,
            formatter: function () {
                return this.x + '<br/><br/>' + fusion.util.formatCurrencyAbsolute(this.y);
            },
            style: {
                color: '#ffffff',
                fontSize: '1.1em',
                padding: '20px',
                opacity: 0.8,
            }
        },
        legend: {
            enabled: false,
        },
        series: [{
            name: 'values',
            data: [0,0,0,0,0,0,0,0],
        }],

    });
    



});