﻿$.Core.assumptions = $.Core.assumptions || {};

$.Core.assumptions.tabler = function (readonly) {

    var _readonly = readonly;
    var _assetTable = $('#assetAssumptionsTable');
    var _initialised = false;

    var buildAssetsTable = function (assetClasses) {

        var head = _assetTable.find('tr.head');
        _assetTable.empty();
        if (head.length > 0) {
            _assetTable.append(head);
        };

        var tbl = "";
        assetClasses.forEach(function (ac) {
            tbl += "<tr><th>" + ac.ClassName + "</th>";
            if (!_readonly) {
                tbl += "<td><div class='assumptionsSlider' data-categoryid='" + ac.ClassId + "' data-original='" + ac.Before + "'></div></td>";
            }
            tbl += "<td class='values asset_" + ac.ClassId + "'></td></tr>";
        });
        tbl += "<tr><th>Weighted asset return</th>" + (!_readonly ? "<td></td>" : "") + "<td class='values weightedAssetReturn'></td></tr>"

        _assetTable.append(tbl);
    };

    var buildLiabilitiesTable = function (setup) {
        $('#preRetirementRow th').html(setup.preRetirementDiscountRate.Label).closest("tr").toggle(setup.preRetirementDiscountRate.Visible);
        $('#postRetirementRow th').html(setup.postRetirementDiscountRate.Label).closest("tr").toggle(setup.postRetirementDiscountRate.Visible);
        $('#pensionerRow th').html(setup.pensionDiscountRate.Label).closest("tr").toggle(setup.pensionDiscountRate.Visible);
        $('#salaryIncreasesRow th').html(setup.salaryIncrease.Label).closest("tr").toggle(setup.salaryIncrease.Visible);
        $('#RPIRow th').html(setup.inflationRetailPriceIndex.Label).closest("tr").toggle(setup.inflationRetailPriceIndex.Visible);
        $('#CPIRow th').html(setup.inflationConsumerPriceIndex.Label).closest("tr").toggle(setup.inflationConsumerPriceIndex.Visible);
        $('#life65Row th').html(setup.lifeExpectancy65.Label).closest("tr").toggle(setup.lifeExpectancy65.Visible);
        $('#life45Row th').html(setup.lifeExpectancy65_45.Label).closest("tr").toggle(setup.lifeExpectancy65_45.Visible);
    };

    var populateAssetsTable = function (assetClasses, weightedAssetReturn) {
        assetClasses.forEach(function (ac) {
            $("td.values.asset_" + ac.ClassId, _assetTable).html((ac.Value * 100).toFixed(2) + "%");
        });
        $("td.values.weightedAssetReturn", _assetTable).html((weightedAssetReturn * 100).toFixed(2) + "%");
    };

    var populateLiabilitiesTable = function (currentAssumptions) {
        $('#preRetirementRow td:last').html((currentAssumptions.preRetirementDiscountRate.Value * 100).toFixed(2) + '%');
        $('#postRetirementRow td:last').html((currentAssumptions.postRetirementDiscountRate.Value * 100).toFixed(2) + '%');
        $('#pensionerRow td:last').html((currentAssumptions.pensionDiscountRate.Value * 100).toFixed(2) + '%');
        $('#salaryIncreasesRow td:last').html((currentAssumptions.salaryIncrease.Value * 100).toFixed(2) + '%');
        $('#RPIRow td:last').html((currentAssumptions.inflationRetailPriceIndex.Value * 100).toFixed(2) + '%');
        $('#CPIRow td:last').html((currentAssumptions.inflationConsumerPriceIndex.Value * 100).toFixed(2) + '%');
        $('#life65Row td:last').html(currentAssumptions.lifeExpectancy65 + ' yrs');
        $('#life45Row td:last').html(currentAssumptions.lifeExpectancy65_45 + ' yrs');
    };

    var initialise = function (data) {
        buildAssetsTable(data.assetClasses);
        buildLiabilitiesTable(data);
        _initialised = true;
    };

    return {
        init: function (data) {
            initialise(data);
            return this;
        },
        refresh: function (data) {
            if (!_initialised) {
                initialise(data);
            };
            populateAssetsTable(data.assets);
            if (data.assetsOnly === undefined || data.assetsOnly == false) {
                populateLiabilitiesTable(data.liabilities, data.weightedAssetReturn);
            }
        }
    };
};
