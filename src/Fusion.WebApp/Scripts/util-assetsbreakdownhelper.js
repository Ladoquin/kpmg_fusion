﻿fusion.util.AssetsBreakdownHelper = function () {

    this.draw = function (args) {

        var total = 0;
        $.each(args.categories, function () { total += this.Value; });

        var series = getSeries(args, total);
       
        var dimensions = args.dimensions || {};
        dimensions.marginRight = dimensions.marginRight || 0;
        dimensions.marginLeft = dimensions.marginLeft || 0;
        dimensions.marginTop = dimensions.marginTop || 0;
        dimensions.marginBottom = dimensions.marginBottom || 0;

        drawChart(args.containerId, series, {
            dimensions: dimensions,
            title: {
                text: fusion.util.formatCurrencyAbsolute(total),
                fontSize: total < 1000000000 ? '24px' : (total < 10000000000 ? '20px' : '18px') //unless you'd like to work out the inner pie diameter? 
            },
            borderColor: parseBorderColor(args.borderColorMode) //else transparent sections still have a border
        });

        $("#" + args.containerId).addLegend({
            alignment: args.alignment || "bottom"
        });
    };

    var parseBorderColor = function (mode) {
        var borderColor = "#ffffff";
        if (typeof mode != "undefined") {
            if (mode == "grey")
                borderColor = "#eaeaea";
            else if (mode == "blue")
                borderColor = "#e0effb";
        };
        return borderColor;
    };

    var convertToSeries = function (collection, color1, color2, includeZeroes, dataCreatedCallback) {
        var step = 1 / Math.max((collection.length - 1), 1);
        var seriesData = [];
        for (var i = 0; i < collection.length; i++) {
            if (collection[i].Value != 0 || includeZeroes) {
                var data = {
                    name: collection[i].Label,
                    y: collection[i].Value,
                    color: fusion.util.colorGradient(color1, color2, i * step)
                };
                if (typeof dataCreatedCallback != "undefined")
                    dataCreatedCallback(i, data);
                seriesData.push(data);
            }
        };
        return seriesData;
    };

    var getSeries = function (args, total) {

        var series = [];

        if (typeof args.synthetic != "undefined") {
            var syntheticTotal = args.synthetic.data.TotalAmount;
            if (args.showZeroEntries || syntheticTotal > 0) {
                var syntheticSeries = [
                    convertToSeries([{ Label: "Synthetic Growth", Value: syntheticTotal}], "#22B14C", "#22B14C", true, function(i, data) {
                        data.custom = { showAsPercentage: false };
                        if (args.synthetic.showBreakdown) {
                            data.breakdown = [
                                { y: args.synthetic.data.EquityAmount, name: "Equity" },
                                { y: args.synthetic.data.CreditAmount, name: "Credit" }
                            ];
                        };
                        })[0],
                    {
                        y: total - syntheticTotal,
                        color: 'rgba(255, 255, 255, 0.1)', //transparent
                        custom: {
                            hidden: true
                        }
                    }];
                series.push({
                    size: '115%',
                    innerSize: '100%',
                    showInLegend: false,
                    dataLabels: {
                        enabled: false
                    },
                    data: syntheticSeries
                });
            }
        }

        var categorySeries = convertToSeries(args.categories, '#e87424', '#683410', false);

        series.push({
            size: '100%',
            innerSize: args.hideClasses ? '75%' : '85%',
            showInLegend: true,
            dataLabels: {
                enabled: false
            },
            data: categorySeries
        });

        if (typeof args.hideClasses == "undefined" || args.hideClasses == false) {

            var classes = $.map(args.categories, function (n) {
                return n.SubItems;
            });
            classSeries =
                convertToSeries(classes, '#00a7c5', '#003b46', args.showZeroEntries, function (i, data) {
                    if (typeof args.showFunds != "undefined" && args.showFunds) {
                        data.breakdown = $.map(classes[i].SubItems, function (fund) { return { y: fund.Value, name: fund.Label } });
                    }
            });

            series.push({
                size: '85%',
                innerSize: '70%',
                showInLegend: true,
                dataLabels: {
                    enabled: false
                },
                data: classSeries
            });
        }

        return series;
    };

    var drawChart = function (id, series, settings) {

        new Highcharts.Chart({
            chart: {
                renderTo: id,
                type: 'pie',
                backgroundColor: 'transparent',
                height: settings.dimensions.height,
                marginTop: settings.dimensions.marginTop,
                marginBottom: settings.dimensions.marginBottom,
                marginRight: settings.dimensions.marginRight,
                marginLeft: settings.dimensions.marginLeft
            },
            credits: false,
            exporting: {
                enabled: false,
            },
            legend: {
                enabled: false
            },
            tooltip: {
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: null,
                shadow: false,
                useHTML: true,
                formatter: function () {
                    var html = null;
                    this.point.custom = this.point.custom || {};
                    this.point.custom.hidden = this.point.custom.hidden || false;
                    if (this.point.custom.hidden == false) {
                        html = "<div class='highcharts-tooltip assets-breakdown-tooltip'>";
                        if (typeof this.point.custom.showAsPercentage == "undefined" || this.point.custom.showAsPercentage == false) {
                            html += this.point.name + ': ' + fusion.util.formatCurrencyAbsolute(this.y) + ' (' + this.percentage.toFixed(1) + '%' + ')';
                        }
                        else {
                            html += this.point.name + ' (' + (this.y * 100).toFixed(1) + '%)';
                        }
                        html += "</div>";
                    }
                    return html;
                }
            },
            title: {
                text: settings.title.text,
                useHTML: true,
                floating: true,
                style: {
                    fontFamily: 'Lato, sans-serif',
                    fontSize: settings.title.fontSize,
                    zIndex: -1,
                    textAlign: 'center',
                },
                align: 'center',
                verticalAlign: 'middle'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: false,
                    slicedOffset: 0,
                    center: ['50%', '50%'],
                    borderColor: settings.borderColor
                }
            },
            series: series
        },
        function (c) {
            var x = 0 - (c.chartWidth * 0.5) + (c.plotWidth * 0.5) + c.plotLeft;
            var y = 0 - (c.chartHeight * 0.5) + (c.plotHeight * 0.5) + c.plotTop + 12; // 12 is half the font size
            c.setTitle({ x: x, y: y });
        });
    }
};