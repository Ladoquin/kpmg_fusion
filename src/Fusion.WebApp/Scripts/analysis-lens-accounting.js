﻿fusion.analysis.accountingLens = function () {

    var initialised = false;
    var isActiveLens = false;
    var isStale = true;
    var dataUrl = "/Json/AccountingData";
    var balanceChart = null;
    var iasChart = null;
    var frsChart = null;
    var gaapChart = null;
    var dataCache;
    var startingPosition = "";
    var startdatePicker = $("#disclosure-daterange-start");
    var enddatePicker = $("#disclosure-daterange-end");
    var datepickerSettings = { disabled: true, inline: true, dateFormat: 'dd/mm/yy', buttonImage: 'Images/datepicker-button.png', buttonText: null };
    var disclosureType;
    var iasIdx;
    var frsIdx;
    var gaapIdx;
    var active = null;
    var positiveColor = fusion.util.colorGradient('#6a7f10', '#ffffff', 0.4);
    var negativeColor = fusion.util.colorGradient('#9e3039', '#ffffff', 0.4);
    var periods = [];
    var dateMarkers = {};
    var that = this;
    var isiPad = navigator.userAgent.match(/iPad/i) != null;

    var getPandLChartSettings = function (renderTo, categories, band1end, band2end) {
        return {
            chart: {
                type: 'column',
                renderTo: renderTo,
                backgroundColor: 'none',
                inverted: true,
                marginLeft: 180
            },
            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: null
            },
            xAxis: [{
                gridLineWidth: 1,
                categories: categories,
                plotBands: [{
                    from: -0.5,
                    to: band1end,
                    color: fusion.util.colorGradient('#a83ea6', '#ffffff', 0.8),
                }, {
                    from: band1end,
                    to: band2end,
                    color: fusion.util.colorGradient('#e87424', '#ffffff', 0.8),
                }, {
                    from: band2end,
                    to: categories.length - 0.5,
                    color: '#ffffff',
                }],
                labels: {
                    useHTML: true,
                },
            }],
            yAxis: {
                gridLineWidth: 1,
                maxPadding: 0.25,
                minPadding: 0.25,
                title: {
                    text: null
                },
                plotLines: [{
                    value: 0,
                    color: '#555',
                    width: 1,
                    zIndex: 10,
                }],
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                    overflow: 'justify'
                },
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        useHTML: true,
                        enabled: true,
                        formatter: function () {
                            return fusion.util.formatCurrencyAbsolute(this.point.y);
                        },
                    },
                    pointPadding: 0,
                    borderWidth: 0,
                }
            },
            tooltip: {
                hideDelay: 0,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                followPointer: true,
                formatter: function () {
                    var tt = this.x + '<br/><br/>Before: ';
                    if (this.point.before !== undefined) {
                        tt += fusion.util.formatCurrencyAbsolute(this.point.before) + "<br/>After: ";
                    }
                    tt += fusion.util.formatCurrencyAbsolute(this.y);
                    return tt;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            legend: {
                enabled: false,
            },
            series: [{
                name: 'values',
                data: categories.map(function (x) { return 0; }),
            }]
        }
    };

    this.activate = function () {
        isActiveLens = true;
        if (isStale) {
            refresh();
        }
    };

    this.deactivate = function () {
        isActiveLens = false;
    };

    this.update = function () {
        if (isActiveLens) {
            refresh();
        } else {
            isStale = true;
        }
    };

    this.analysisDateChanged = function () {
        // Disable the accounting download button until everything is initialised, else it 
        // can use the old date range if clicked before ready
        that.disableDownload();
    };

    this.disableDownload = function () {
        $("#btn-accounting-download").off("click");
        $("#btn-accounting-download").css("opacity", isiPad ? "0.4" : "1");
        $("#btn-accounting-download").on("click", function (e) {
            e.preventDefault();
            return false;
        });
    };

    var enableDownload = function () {
        $("#btn-accounting-download").off("click");
        $("#btn-accounting-download").css("opacity", "1");
        $("#btn-accounting-download").on("click", downloadClickHandler);
    };

    var refresh = function () {

        if (!initialised) {
            initialise();
        };

        $.getJSON(dataUrl, function (data) {

            if (data.hasAccountingBasis == false) return;
            if (data.after == null) return;

            dateMarkers = {
                anchor: Highcharts.dateFormat('%d/%m/%Y', data.anchorDate),
                refresh: Highcharts.dateFormat('%d/%m/%Y', data.refreshDate),
                start: Highcharts.dateFormat('%d/%m/%Y', data.analysisPeriodStart),
                end: Highcharts.dateFormat('%d/%m/%Y', data.analysisPeriodEnd)
            };

            $("#pandl-title").html(Highcharts.dateFormat('%d/%m/%Y', data.analysisPeriodEnd));

            refreshBalanceChart(data);
            refreshAccountingPeriods(data.periods);

            switch (active) {
                case iasIdx: refreshIASChart(data); break;
                case frsIdx: refreshFRSChart(data); break;
                case gaapIdx: refreshUSGAAPChart(data); break;
                default: break;
            }

            if(isiPad == false)
                enableDownload();

            dataCache = data;
        })
    };

    var refreshBalanceChart = function (data) {

        if (data.balance == null || data.balance.after == null)
            return;

        var assets = data.balance.after.Assets;
        var liabilities = data.balance.after.Liabilities;
        var deficit = assets - liabilities;

        var before = data.balance.before || {};
        before.deficit = before.Assets != null ? before.Assets - before.Liabilities : undefined;
        if (before.deficit !== undefined) {
            if (startingPosition == "") {
                startingPosition = before.deficit < 0 ? "Deficit" : "Surplus";
            }
        };

        if (startingPosition == "") {
            startingPosition = deficit < 0 ? "Deficit" : "Surplus";
        }

        balanceChart.series[0].data[0].update({ y: assets, before: before.Assets }, false);
        balanceChart.series[0].data[1].update({ y: liabilities, before: before.Liabilities }, false);
        //before.deficit as 'undefined' is valid here because it's used in the tooltip check
        balanceChart.series[0].data[2].update({ y: Math.abs(deficit), color: deficit < 0 ? '#9e3039' : '#6a7f10', before: before.deficit }, false);
        balanceChart.series[1].data[2].update({ y: Math.min(assets, liabilities) });
        balanceChart.xAxis[0].categories[2] = deficit < 0 ? 'Deficit' : 'Surplus';
        balanceChart.redraw();
    };

    var refreshAccountingPeriods = function (accountingPeriods) {
        periods = accountingPeriods;
    }

    var setSeriesDataItem = function (chart, idx, afterVal, beforeVal) {
        chart.series[0].data[idx].update({
            color: afterVal > 0 ? negativeColor : positiveColor,
            y: afterVal,
            before: beforeVal
        }, false);
    };

    var sortOutVisuals = function (chart) {
        // feel free to replace this with native highchart settings to make this code redundant. I couldn't find the necessary settings.
        centraliseYAxis(chart);
        rightAlignLabels(chart);
    };

    var centraliseYAxis = function (chart) {
        // ensure max and min are same on horizontal axis to keep graph centre aligned
        // must call setExtremes first to reset any previous getExtremes which may now not relate to refreshed data
        chart.yAxis[0].setExtremes();
        var extremes = chart.yAxis[0].getExtremes();
        if (Math.abs(extremes.min) != Math.abs(extremes.max)) {
            var extreme = Math.max(Math.abs(extremes.min), Math.abs(extremes.max));
            chart.yAxis[0].setExtremes(-extreme, extreme);
        }
    };

    var rightAlignLabels = function (chart) {
        $("div.highcharts-axis-labels:first", chart.container).css("text-align", "right");
    };

    var getCommonIasFrsChart = function (srcChart, chartDiv, before, after)
    {
        if (srcChart == undefined) {
            srcChart = new Highcharts.Chart(
                getPandLChartSettings(chartDiv, ['Service cost', 'Scheme expenses', 'Interest charge', 'Interest credit', 'Total P&amp;L charge'], 2.5, 3.5)
            );
        };

        var chart = srcChart;

        setSeriesDataItem(chart, 0, after.CurrentServiceCost, before.CurrentServiceCost);
        setSeriesDataItem(chart, 1, after.SchemeAdminExpenses, before.SchemeAdminExpenses);
        setSeriesDataItem(chart, 2, after.InterestCharge, before.InterestCharge);
        setSeriesDataItem(chart, 3, after.InterestCredit, before.InterestCredit);
        setSeriesDataItem(chart, 4, after.Total, before.Total);

        chart.xAxis[0].categories[4] = 'Total P&amp;L ' + (after.Total > 0 ? 'charge' : 'credit');

        return chart
    }

    var refreshIASChart = function (data) {

        if (data.after == null || data.after.pnlIas19 == null)
            return;
        
        var before = (data.before || {}).pnlIas19 || {};
        var chart = getCommonIasFrsChart(iasChart, "accountingIASChartDiv", before, data.after.pnlIas19);

        chart.redraw();

        sortOutVisuals(chart);
    };

    var refreshFRSChart = function (data) {

        if (data.after == null || data.after.pnlFrs102 == null)
            return;

        var before = (data.before || {}).pnlFrs102 || {};
        var chart = getCommonIasFrsChart(frsChart, "accountingFRSChartDiv", before, data.after.pnlFrs102);

        chart.redraw();

        sortOutVisuals(chart);
    };

    var refreshUSGAAPChart = function (data) {

        if (data.after == null || data.after.pnlUsgaap == null)
            return;

        if (gaapChart == undefined) {
            gaapChart = new Highcharts.Chart(
                getPandLChartSettings("accountingGAAPChartDiv", ['Service cost', 'Interest cost', 'Expected return<br/>on scheme assets', 'Amortisation of prior<br/>service (cost) / credit', 'Amortisation of<br/>actuarial gains / (losses)', 'Total net periodic<br/>pension cost / (credit)'], 1.5, 4.5)
            );
        }

        var chart = gaapChart;
        var after = data.after.pnlUsgaap;
        var before = (data.before || {}).pnlUsgaap || {};

        setSeriesDataItem(chart, 0, after.ServiceCost, before.ServiceCost);
        setSeriesDataItem(chart, 1, after.InterestCost, before.InterestCost);
        setSeriesDataItem(chart, 2, after.ExpectedReturn, before.ExpectedReturn);
        setSeriesDataItem(chart, 3, after.AmortisationOfPriorService, before.AmortisationOfPriorService);
        setSeriesDataItem(chart, 4, after.AmortisationOfActuarial, before.AmortisationOfActuarial);
        setSeriesDataItem(chart, 5, after.TotalNetPeriodicPension, before.TotalNetPeriodicPension);

        chart.redraw();

        sortOutVisuals(chart);

        // weird behaviour with labels jumping to a top position with around -10000. 
        // going to manually reposition the labels. 
        // please replace when highcharts is acting normally.        
        var labelSpans = $("#accountingGAAPChartDiv > div > div.highcharts-axis-labels:first span");
        var pos = ((parseInt($(labelSpans[3]).css("top").replace("px", "")) - parseInt($(labelSpans[1]).css("top").replace("px", ""))) / 2) + parseInt($(labelSpans[1]).css("top").replace("px", ""));
        $(labelSpans[2]).css("top", pos + "px");
    };

    var initialise = function () {

        var i = -1;
        iasidx = $("#ias19Tab").is(":visible") ? ++i : -1;
        frsIdx = $("#frs102Tab").is(":visible") ? ++i : -1;
        gaapIdx = $("#gaapTab").is(":visible") ? ++i : -1;

        switch ($("#AccountingDefaultStandard").val()) {
            case "IAS19": active = iasIdx; break;
            case "FRS102": active = frsIdx; break;
            case "USGAAP": active = gaapIdx; break;
            default: break;
        }
        disclosureType = $("#AccountingDefaultStandard").val();

        $("#accountingLensTabs").tabs(
            {
                active: active,
                activate: function (event, ui) {
                    if (ui.newPanel.attr('id') == "ias19Tab") {
                        active = iasIdx;
                        refreshIASChart(dataCache);
                    }
                    else if (ui.newPanel.attr('id') == "frs102Tab") {
                        active = frsIdx;
                        refreshFRSChart(dataCache);
                    }
                    else {
                        active = gaapIdx;
                        refreshUSGAAPChart(dataCache);
                    };
                    disclosureType = ui.newPanel.data("type");
                    $("#btn-accounting-download").attr("href", $("#btn-accounting-download").attr("href").substring(0, $("#btn-accounting-download").attr("href").indexOf("?")) + "?type=" + disclosureType);
                }
            });

        initDownload();

        initDownloadDateDialogs();

        initBalanceChart();

        initialised = true;
    }

    var initDownload = function () {
        $("#accounting-dialog-warning").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            dialogClass: 'white-dialog',
            create: fusion.util.createDialogStyle,
            buttons: {
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
        if(isiPad == false)
            $("#btn-accounting-download").on("click", downloadClickHandler);
    };

    var downloadClickHandler = function (e) {
        $.ajax({
            url: "/Json/PanelState",
            async: false,
            cache: false,
            complete: function (data, status) {
                var state = $.parseJSON(data.responseText);
                if (state.recoveryPlan ||
                    state.investmentStrategy ||
                    state.FRO ||
                    state.ETV ||
                    state.PIE ||
                    state.futureBenefits ||
                    state.ABF ||
                    state.insurance) {
                    $("#accounting-dialog-warning").dialog("open");
                }
                else {
                    if (disclosureType == "USGAAP") {
                        $("#accounting-dialog-period-datepicker").dialog("open");
                    }
                    else {
                        $("#accounting-dialog-custom-datepicker").dialog("open");
                    };
                }
                e.preventDefault();
            }
        });
    };

    var initDownloadDateDialogs = function () {

        var minStartDate = startdatePicker.data("min-date");
        var maxEndDate = enddatePicker.data("max-date");

        startdatePicker.datepicker(
            $.extend(datepickerSettings, {
                showOn: isiPad ? "button" : "both",
                minDate: minStartDate,
                maxDate: maxEndDate,
                onSelect: function () {
                    var d = $(this).val();
                    if (fusion.util.getDateFromDatepicker(enddatePicker.val()) < fusion.util.getDateFromDatepicker(d)) {
                        enddatePicker.val(d);
                    };
                    enddatePicker.datepicker("option", "minDate", d);
                    $(this).datepicker("hide");
                },
                onClose: function () {
                    if (isiPad)
                        $(this).attr("disabled", false);
                    $('#accounting-dialog-custom-datepicker').focus();//fixes ie9 picker won't close
                },
                beforeShow: function (dateText, inst) {
                    if (isiPad)
                        $(this).attr("disabled", true);
                }
            }))
            .addClass("pointer");

        enddatePicker.datepicker(
            $.extend(datepickerSettings, {
                showOn: isiPad ? "button" : "both",
                minDate: minStartDate,
                maxDate: maxEndDate,
                onClose: function (dateText, inst) {
                    if (isiPad)
                        $(this).attr("disabled", false);
                },
                beforeShow: function (dateText, inst) {
                    if (isiPad)
                        $(this).attr("disabled", true);
                }
            }))
            .addClass("pointer");

        $("#accounting-dialog-custom-datepicker").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            dialogClass: 'accounting-dialog-datepicker',
            create: fusion.util.createDialogStyle,
            open: function () {
                //finding that the start date picker shows with the calendar expanded when the dialog is shown.
                //keeping it disabled until the dialog is open fixes this.
                $(this).closest(".ui-dialog").find(".btn-dialog-cancel").focus();

                startdatePicker.datepicker("option", {
                    disabled: false,
                    minDate: dateMarkers.anchor,
                    maxDate: dateMarkers.refresh
                });
                startdatePicker.datepicker("setDate", dateMarkers.start);

                enddatePicker.datepicker("option", {
                    disabled: false,
                    minDate: dateMarkers.anchor,
                    maxDate: dateMarkers.refresh
                });
                enddatePicker.datepicker("setDate", dateMarkers.end);
            },
            close: function () {
                startdatePicker.datepicker("option", "disabled", true);
                enddatePicker.datepicker("option", "disabled", true);
            },
            buttons: {
                Ok: function () {
                    var start = fusion.util.getDateFromDatepicker(startdatePicker.val());
                    var end = fusion.util.getDateFromDatepicker(enddatePicker.val());
                    var url = $("#disclosure-url-base").val() + "?type=" + disclosureType + "&start=" + start + "&end=" + end;
                    window.location.href = url;
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
        });

        $("#accounting-dialog-period-datepicker").on("click", "a.accounting-period-select", function (e) {
            $("#accounting-dialog-period-datepicker").dialog("close");
        });

        $("#accounting-dialog-period-datepicker").dialog({
            modal: true,
            autoOpen: false,
            draggable: false,
            dialogClass: 'accounting-dialog-datepicker',
            create: fusion.util.createDialogStyle,
            open: function () {
                var url = $("#disclosure-url-base").val() + "?type=" + disclosureType;
                $("#accounting-dialog-period-datepicker .dialog-body").empty();
                for (var i = 0; i < periods.length - 1; i++) {
                    var start = Highcharts.dateFormat('%d/%m/%Y', periods[i]);
                    var end = Highcharts.dateFormat('%d/%m/%Y', periods[i + 1]);
                    var href = url + "&start=" + periods[i] + "&end=" + periods[i + 1];
                    $("#accounting-dialog-period-datepicker .dialog-body").append($("<a class='accounting-period-select' href='" + href + "'>" + start + " - " + end + "</a><br/>"));
                }
            },
            buttons: {
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
        });
    };

    var initBalanceChart = function () {
        balanceChart = new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: 'accountingBalanceChartDiv',
                backgroundColor: 'none',
                plotBorderWidth: 1,
                plotBackgroundColor: 'white'
            },
            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: null,
            },
            xAxis: {
                categories: ['Assets', 'Liabilities', 'Deficit'],
            },
            yAxis: [{
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                },
                stackLabels: {
                    useHTML: true,
                    enabled: true,
                    formatter: function () {
                        if (this.x == 2) {
                            return fusion.util.formatCurrencyAbsolute(Math.abs(this.points[0][0] - this.points[0][1]));
                        }
                        else {
                            return fusion.util.formatCurrencyAbsolute(this.total);
                        }
                    },
                    maxPadding: 0.08,
                }
            }, {
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                },
                linkedTo: 0,
                opposite: true,
            }],

            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        useHTML: true,
                        enabled: false,
                        formatter: function () {
                            return fusion.util.formatCurrencyAbsolute(this.point.y);
                        },
                        overflow: "none",
                        crop: false
                    },
                    pointPadding: 0.08,
                    groupPadding: 0.05,
                    borderWidth: 0,
                    borderColor: '#f0f0f0',
                }
            },
            legend: { enabled: false },
            tooltip: {
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                formatter: function () {
                    var isBalanceCol = this.x == "Deficit" || this.x == "Surplus";
                    var header = isBalanceCol ? "" : this.x + "<br/><br/>";
                    var beforePostfix = isBalanceCol ? " " + startingPosition : "";
                    var afterPostfix = isBalanceCol ? " " + this.x : "";
                    var tt = header + "Before: ";
                    if (this.point.before != undefined) {
                        tt += fusion.util.formatCurrencyAbsolute(this.point.before) + beforePostfix;
                        tt += "<br/>After: ";
                    };
                    tt += fusion.util.formatCurrencyAbsolute(this.y) + afterPostfix;
                    return tt;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            series: [{
                name: 'Balance',
                data: [
                    { y: 0, color: '#E87424' },
                    { y: 0, color: '#8F258D' },
                    { y: 0 }
                ]
            }, {
                name: 'Blank',
                color: 'none',
                data: [
                    { y: 0 },
                    { y: 0 },
                    { y: 0 }
                ]
            }]
        });
    };
};

