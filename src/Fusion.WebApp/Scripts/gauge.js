/*
*	KPMG
*	gauge.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This invokes a gauge. */

$.Core.gauge = {
	init: function(){	
		var opts = {
			  lines: 12, // The number of lines to draw
			  angle: 0.16, // The length of each line
			  lineWidth: 0.2, // The line thickness
			  pointer: {
				length: 0.7, // The radius of the inner circle
				strokeWidth: 0.046, // The rotation offset
				color: '#a79e70' // Fill color
			  },
			  limitMax: 'false',   // If true, the pointer will not go past the end of the gauge

			  colorStart: '#97c5ea',   // Colors
			  colorStop: '#283370',    // just experiment with them
			  strokeColor: '#E0E0E0',   // to see which ones work best for you
			  generateGradient: true
			};	
			//$('#canvas-newgauge').gauge(opts);	
			
			
			var target = $('#canvas-newgauge')[0]; // your canvas element
			var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
			//console.log("gauge", gauge);
			gauge.maxValue = 1500; // set max gauge value
			gauge.animationSpeed = 32; // set animation speed (32 is default value)
			gauge.set(1250); // set actual value			
	}	
};