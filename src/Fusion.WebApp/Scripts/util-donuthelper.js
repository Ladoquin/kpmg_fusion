﻿fusion.util.DonutHelper = function () {

    var that = this;
    var innerSize;

    var convertToSeriesData = function (d, c1, c2, includeZeroes, inner, innerSize) {
        var step = 1 / Math.max((d.length - 1), 1);
        var data = [];
        for (var i = 0; i < d.length; i++) {
            if (d[i].Value != 0 || includeZeroes) {
                data.push({
                    name: d[i].Label,
                    y: d[i].Value,
                    color: fusion.util.colorGradient(c1, c2, i * step),
                    breakdown: inner ? $.map(d[i].SubItems, function(b) { return { y: b.Value, name: b.Label } }) : null
                });
            }
        };
        innerSize = innerSize || 75;
        var series = [{
            size: !inner ? '100%' : '85%',
            innerSize: !inner ? innerSize + '%' : (innerSize - 10) + '%',
            showInLegend: true,
            dataLabels: {
                enabled: false
            },
            data: data
        }];
        return series;
    };

    this.getLiabilitySeriesData = function (data, options) {
        if (typeof options != 'undefined')
            innerSize = options.innerSize || 75;
        return convertToSeriesData(data, '#8e258d', '#4e144e', undefined, undefined, innerSize);
    };

    this.getAssetSeriesData = function (data, options) {
        var assetData = convertToSeriesData(data, '#e87424', '#683410', options.showZeroEntries);
        if (options.showAssetClasses) {
            var innerData = $.map(data, function (n) {
                return $.map(n.SubItems, function (m) {
                    var item = m;
                    item.breakdown = $.map(m.SubItems, function (p) {
                        return {
                            y: p.Value,
                            name: p.Label
                        };
                    });
                    return item;
                });
            });
            assetData.push(convertToSeriesData(innerData, '#00a7c5', '#003b46', options.showZeroEntries, true)[0]);
        }
        return assetData;
    };

    this.getLiabilitySpec = function (data, opts) {
        opts = opts || {};
        return {
            chartDivId: opts.chartId || 'liabilityBreakdownPie',
            height: opts.height,
            marginTop: opts.marginTop,
            marginBottom: opts.marginTop,
            legendAlign: opts.legendAlign
        };
    };
};