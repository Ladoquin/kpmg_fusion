﻿$(function () {
    $(".schemeNameSearch").schemeSearcher();
    $("#ImportErrorDetailsTooltip").tooltip({
        tooltipClass: 'scheme-import-error',
        content: function () { return $("#model-importerrordetails").html(); },
        position: {
            my: "right top",
            at: "right bottom"
        }
    });
    $("#ImportErrorDetailsTooltip").click(function (e) { e.preventDefault(); });
    $("#dialog-scheme-delete-confirm").dialog({
        modal: true,
        autoOpen: false,
        draggable: false,
        dialogClass: 'white-dialog',
        buttons: {
            "Yes": function () {
                $(this).dialog("close");
                $("form").submit();
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        },
        create: function (event, ui) {
            var widget = $(this).dialog("widget");
            $(".ui-dialog-titlebar-close .ui-button-text", widget).text("");
            var buttons = $(".ui-dialog-buttonpane", widget).find("button");
            $(buttons[1]).addClass("btn-dialog-cancel");
        },
        open: function () {
            $(this).parent().find('button:nth-child(2)').focus();
        }
    });
    $("#delete_scheme").click(function (e) {
        $("#dialog-scheme-delete-confirm").dialog("open");
        e.preventDefault();
    });
    $(".deleteCheckbox").click(function (e) {
        var id = $(this).val();
        var setId = this.checked ? id : 0;
        $("#DeleteDocument-" + id).val(setId);
    });
});

