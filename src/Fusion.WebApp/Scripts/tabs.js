/*
*	KPMG
*	tabs.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This invokes tabs - its possible to have different types of tabs - horizontal or vertical*/

fusion.tabs = {
    
	init: function() {

	    var that = this;

		$('[data-tabs="true"]').each(function () {
			var el = $(this);		

			if (el.closest('[data-orientation=horizontalbox]').size()) {
			    el.addClass("horizontalboxmode");
			    el.find("li").last().addClass("lastelement");
			    el.tabs({
			        activate: function (event, ui) {
			            that.tabhandler(el, ui);
			        }
			    });
            }
			else if (el.closest('[data-orientation=horizontal]').size()) {
			    //horizontal tabs
			    el.addClass("horizontalmode");
			    el.find("li").last().addClass("lastelement");
			    el.tabs({
			        activate: function (event, ui) {
			            that.tabhandler(el, ui);
			        },
			        active: el.data('default'),
			    });

			} else {
			    //vertical tabs
			    el.addClass("verticalmode");
			    el.find("li").last().addClass("lastelement");
			    that.generateAccordionVariant(el);
			    el.tabs({
			        activate: function (event, ui) {
			            that.tabhandler(el, ui);
			        }
			    });
			}
		});
	},
	generateAccordionVariant: function(el){
		var mobileAccordionId = el.attr("id")+'accordion';

		var mobileAccordionVariant = '<div data-accordion="true" id="'+mobileAccordionId+'" class="mobileVerticalTabAccordion '+el.attr("class")+'"></div>';
		
		el.after(mobileAccordionVariant);

		/*add contents to mobile accordion variant*/
		var innnerData = '';
		el.find('.tabMenu').find('li').each(function( index ) {
			innnerData+= '<h3>'+$(this).text()+'</h3><div>'+el.find('.panelTab').eq(index).html()+'</div>';
		});
		$('#'+mobileAccordionId).html(innnerData);
	},
	tabhandler: function (el, ui) {

		//if this is a parent tab - select first child
		//if there is any functionality that needs to occur on a new tab state, the code can be placed here.
		//console.log("ui", ui.newTab);
		if(el.attr("id") == "plans"){
			$.Core.tongue.closeTongue();
		}
	}
};