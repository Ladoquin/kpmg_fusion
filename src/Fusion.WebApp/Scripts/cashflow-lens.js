﻿fusion.analysis.Cashflow = function () {

    var initialised = false;
    var opts = {
        chartDivId: "cashflowChartDiv",
        excludeAfterSeries: false,
        dataUrl: '/Json/CashflowData'
    };

    var chart = null;
    var isStale = true;
    var isActiveLens = false;

    this.activateLens = function () {
        isActiveLens = true;
        if (isStale) {
            refresh();
        }
    };
    
    this.deactivateLens = function () {
        isActiveLens = false;
    };

    this.updateLens = function () {
        if (isActiveLens) {
            refresh();
        } else {
            isStale = true;
        }
    };

    this.setOptions = function (options) {
        opts.chartDivId = options.chartDivId || opts.chartDivId;
        opts.excludeAfterSeries = options.excludeAfterSeries || opts.excludeAfterSeries;
        opts.date = options.date;
        if (options.dataUrl != "")
            opts.dataUrl = options.dataUrl;
    };
    
    var initialiseLens = function () {

        var series = [{
            name: 'Deferreds' + (!opts.excludeAfterSeries ? ' (before)' : ''),
            color: '#c84e00',
            data: [0],
            stack: 'before'
        }, {
            name: 'Actives' + (!opts.excludeAfterSeries ? ' (before)' : ''),
            color: '#9e3039',
            data: [0],
            stack: 'before'
        }, {
            name: 'Pensioners' + (!opts.excludeAfterSeries ? ' (before)' : ''),
            color: '#673327',
            data: [0],
            stack: 'before'
        }];
        
        if (!opts.excludeAfterSeries) {
            series.push({
                name: 'Deferreds (after)',
                color: '#ff6d0d',
                data: [0],
                stack: 'after'
            }, {
                name: 'Actives (after)',
                color: '#c5414a',
                data: [0],
                stack: 'after'
            }, {
                name: 'Pensioners (after)',
                color: '#964838',
                data: [0],
                stack: 'after'
            });
        };

        chart = new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: opts.chartDivId,
                backgroundColor: 'none',
                plotBorderWidth: 1,
                plotBackgroundColor: 'white',
                animation: {
                    duration: 1000
                }
            },

            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: null,
            },

            xAxis: {
                title: {
                    text: ' ',
                    style: {
                        fontFamily: 'Lato, sans-serif',
                        fontSize: '14px',
                    },
                },
                labels: {
                    formatter: function () {
                        return this.value % 5 == 0 ? this.value : '';
                    }
                },
                categories: []
            },

            yAxis: [{
                title: {
                    useHTML: true,
                    text: 'Payments',
                    style: {
                        fontFamily: 'Lato, sans-serif',
                        fontSize: '14px',
                    },
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels
                },
            }, {
                title: {
                    useHTML: true,
                    text: 'Payments',
                    style: {
                        fontFamily: 'Lato, sans-serif',
                        fontSize: '14px',
                    },
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels
                },

                linkedTo: 0,
                opposite: true,
            }],


            tooltip: {
                shared: true,
                hideDelay: 0,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                followPointer: true,
                formatter: function () {

                    var year = this.x;
                    var html = 'Year ' + year + '<br/>';
                    var aftersIdx = 999;
                    var endIdx = this.points.length - 1;

                    $.each(this.points, function (i, point) {
                        if (point.series.name.indexOf('(after)') > 0) {
                            aftersIdx = Math.min(aftersIdx, i);
                        };
                    });

                    if (!opts.excludeAfterSeries) {
                        html += '<br/>Before:<br/>';
                    };

                    $.each(this.points, function (i, point) {
                        if (i == aftersIdx) {
                            html += '<br/>After:<br/>';
                            afters = true;
                        }
                        html += '<br/>' + point.series.name.replace(' (before)', '').replace(' (after)', '') + ': ' + fusion.util.formatCurrencyAbsolute(this.y);
                        if (i === aftersIdx - 1 || i === endIdx) {
                            html += '<br/>Total: ' + fusion.util.formatCurrencyAbsolute(point.total) + '<br/>';
                        }
                    });

                    return html;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },

            plotOptions: {
                column: {
                    stacking: 'normal',
                    pointPadding: 0,
                    groupPadding: 0,
                    pointPlacement: 'between',
                    turboThreshold: 0,
                    borderWidth: 0

                }
            },
            legend: {
                layout: 'horizontal',
                verticalAlign: 'bottom',
                borderWidth: 0,
                borderRadius: 0,
                padding: 10,
                y: -10,
            },
            series: series
        });

        initialised = true;
    };

    var refresh = function () {

        if (!initialised) {
            initialiseLens();
        };

        $.getJSON(opts.dataUrl, { d: opts.date }, function (data) {

            var categories = [];
            var year = data.year;

            for (var i = 0; i < data.count; i++) {
                categories.push(year + i);
            }

            chart.xAxis[0].setCategories(categories, false);

            var set = function (idx, d) {
                if (chart.series[idx].data.length == d.length) {
                    for (i = 0; i < d.length; i++) {
                        chart.series[idx].data[i].update(d[i], false);
                    }
                } else {
                    chart.series[idx].setData(d, false);
                }
            };

            set(0, data.beforeDeferreds);
            set(1, data.beforeActives);
            set(2, data.beforePensioners);
            if (!opts.excludeAfterSeries) {
                set(3, data.afterDeferreds);
                set(4, data.afterActives);
                set(5, data.afterPensioners);
                if (data.isDirty) {
                    chart.series[3].setVisible(chart.series[0].visible, false);
                    chart.series[4].setVisible(chart.series[1].visible, false);
                    chart.series[5].setVisible(chart.series[2].visible, false);
                } else {
                    chart.series[3].setVisible(false, false);
                    chart.series[4].setVisible(false, false);
                    chart.series[5].setVisible(false, false);
                }
            };

            chart.xAxis[0].setTitle({
                text: 'Annual progression commencing ' + Highcharts.dateFormat('%e %B', data.commencementDate)
            });
            chart.redraw();

            isStale = false;
        });
    };
};

