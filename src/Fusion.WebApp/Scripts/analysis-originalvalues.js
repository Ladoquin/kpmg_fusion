﻿/*
 * originalValues:
 * Problems adding the ticks on the sliders marking the original value because if the
 * slider isn't visible (ie, on a tab that isn't visible) then can't easily work out the width.
 * So this class manages setting the tick when the tab (and therefore sliders) first become 
 * visible and have a width property.
 */
fusion.analysis.originalValues = function () {

    var processed = [];

    return {
        panelActivated: function (panelid) {
            if ($.inArray(panelid, processed) == -1) {
                $(".assumptionsSlider:visible").addTick({ overwrite: true });
                $(".assumptionsSliderWithSoftLimit:visible").slider("setSoftLimitTick");
                $(window).resize(function () {
                    $(".assumptionsSlider:visible").addTick({ overwrite: true });
                    $(".assumptionsSliderWithSoftLimit:visible").slider("setSoftLimitTick");
                });

                processed.push(panelid);
            }
        },
        reset: function () {
            processed = [];
        }
    }

};
