/*
*	KPMG
*	compacting.header.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This invokes compacting the header*/

$.Core.compactingHeader = {
	contractHeader: function(){
		$("header").stop().animate({
			marginTop: "-154"
		}, 350, function() {
			//Animation complete.
		});
	},
	expandHeader: function(){
		$("header").stop().animate({
			marginTop: "0"
		}, 350, function() {
			//Animation complete.
		});			
	}
};