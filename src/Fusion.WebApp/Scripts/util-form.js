﻿fusion.util.form = {
	_warningElementsSelector: ".changeFailed, .changeSucceeded, .field-validation-error",
	hideWarnings: function () {
		$(this._warningElementsSelector).hide();
	},
	fadeOutWarnings: function () {
		$(this._warningElementsSelector).fadeOut();
	}
};