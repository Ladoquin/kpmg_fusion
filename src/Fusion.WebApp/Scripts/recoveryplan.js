﻿fusion.analysis.RecoveryPlanPanel = function () {

    var beforeData;
    var recoveryChart = null;
    var initialised = false;    
    var that = this;
    var readonly;
    
    this.changed = $.Callbacks();

    var reset = function () {
        $.post('/Json/ResetRecoveryPlan', function (data) {
            refreshControls(data);
            that.changed.fire({ id: that.id, data: data });
        });
    };

    $("#analysisdashboard").on("tabsactivate", function (event, ui) {
        if (ui.newPanel[0].id == "analysisdashboard_recoveryplan") {
            $(".assumptionsSliderWithSoftLimit", $("#analysis_recoveryplan")).slider("setSoftLimitTick");
            $(".assumptionsSliderWithSoftLimit", $("#analysis_recoveryplan")).addTick({ overwrite: true });
        }
    });

    var setRecoveryPlan = function () {
        var plan = {};

        $('#recoveryPlanTable .assumptionsSlider').each(function (index) {
            var finalValue = $(this).parent().parent().find('.slider-text-value').data('finalValue');

            if (finalValue == null || finalValue == typeof 'undefined')
                finalValue = $(this).slider("option", "value");

            plan[$(this).attr('id')] = finalValue;
        });

        $('#outperformanceTypeTable .assumptionsSlider').each(function (index) {
            var finalValue = $(this).parent().parent().find('.slider-text-value').data('finalValue');

            if (finalValue == null || finalValue == typeof 'undefined')
                finalValue = $(this).slider("option", "value");
            plan[$(this).attr('id')] = finalValue;
        });

        plan.recoveryPlanIsGrowthFixed = $('#recoveryPlanGrowthFixed').prop('checked');

        plan.recoveryPlanStrategyType = $("input:radio[name=recoveryPlanStrategyType]:checked").val();


        var json = {
            recoveryPlan: JSON.stringify(plan)
        };

        $.post('/Json/SetRecoveryPlan', json, function (data) {
            refreshControls(data);
            that.changed.fire({ id: that.id, data: data });
        });
    };

    var initialise = function (recoveryPlanData) {

        $("#recoveryPlanInvestmentGrowth").data("original", recoveryPlanData.setup.InvestmentGrowth);
        $("#recoveryPlanDiscountIncrement").data("original", recoveryPlanData.setup.DiscountIncrement);
        $("#recoveryPlanLength").data("original", recoveryPlanData.setup.Term);
        $("#recoveryPlanIncreases").data("original", recoveryPlanData.setup.Increases);
        $("#recoveryPlanLumpSumPayment").data("original", recoveryPlanData.setup.LumpSumPayment);

        $("#recoverPlan_refGiltYieldRow").toggle(recoveryPlanData.setup.giltYieldLabel != null);
        $("#recoverPlan_refGiltYieldRow th").html(recoveryPlanData.setup.giltYieldLabel);

        // using readonly-rp as classname as just using readonly means the slider won't plugin won't add the 
        // original data tick on basis change

        var updatePercentCell = function (event, ui) {
            if ($(this).hasClass("readonly-rp")) {
                return false;
            }
            else {
                $(this).slider("updateSliderValue", {
                    el: $(this),
                    value: ui.value
                });
            }
        };

        var updateIntCell = function (event, ui) {
            if ($(this).hasClass("readonly-rp")) {
                return false;
            }
            else {
                $(this).slider("updateSliderValue", {
                    el: $(this),
                    value: ui.value
                });
            };
        };

        var updateRecoveryPlan = function (event, ui) {
            if ($(this).hasClass("readonly-rp")) {
                return false;
            }
            else {
                setRecoveryPlan();
            }
        };

        $("#recoveryPlanLumpSumPayment").data("max", recoveryPlanData.setup.MaxLumpSumPayment);

        $('#recoveryPlanTable .assumptionsSlider[data-units="percent"]').add('#outperformanceTypeTable .assumptionsSlider').each(function (index) {
            $(this).slider({
                animate: 400,
                range: 'min',
                min: $(this).data('min'),
                max: $(this).data('max'),
                value: recoveryPlanData[$(this).attr('id').replace('recoveryPlan', '')],
                step: $(this).data('increment'),
                slide: updatePercentCell,
                stop: updateRecoveryPlan,
                elementSelector: $(this),
                textElement: $(this).parent().siblings().last()
            });
            $(this).slider('setTextValueElement');
        });

        $('#recoveryPlanTable .assumptionsSlider[data-units="int"]').each(function (index) {
            $(this).slider({
                animate: 400,
                range: 'min',
                min: $(this).data('min'),
                max: $(this).data('max'),
                value: recoveryPlanData[$(this).attr('id').replace('recoveryPlan', '')],
                step: $(this).data('increment'),
                slide: updateIntCell,
                stop: updateRecoveryPlan,
                elementSelector: $(this),
                textElement: $(this).parent().siblings().last()
            });
            $(this).slider('setTextValueElement');
        });

        $('#resetRecoveryPlan').click(function () {
            reset();
        });

        $('input:radio[name="performancetype"]').change(function () {            
            var isGrowthFixed = $(this).attr('id') == 'recoveryPlanGrowthFixed';
            if (isGrowthFixed) {
                $('#recoveryPlanInvestmentGrowth').show();
                $('#recoveryPlanInvestmentGrowth').addTick({ overwrite: true });
                $('#recoveryPlanDiscountIncrement').hide().parent().siblings().last().html('');

                $('#recoveryPlanInvestmentGrowth').slider("updateSliderValue", {
                    el: $('#recoveryPlanInvestmentGrowth'),
                    value: $('#recoveryPlanInvestmentGrowth').slider('value')
                });
            } else {
                $('#recoveryPlanDiscountIncrement').show();
                $('#recoveryPlanDiscountIncrement').addTick({ overwrite: true });
                $('#recoveryPlanInvestmentGrowth').hide().parent().siblings().last().html('');
                $('#recoveryPlanDiscountIncrement').slider("updateSliderValue", {
                    el: $('#recoveryPlanDiscountIncrement'),
                    value: $('#recoveryPlanDiscountIncrement').slider('value')
                });
            }
            var el = isGrowthFixed ? $("#recoveryPlanInvestmentGrowth") : $("#recoveryPlanDiscountIncrement");
            el.slider("setSoftLimitTick");            
            $("#outperformance-warning").css("visibility", el.slider("value") > el.data("softlimit") ? "visible" : "hidden");
            $("#" + el.attr("id") + "Asterix").css("visibility", el.slider("value") > el.data("softlimit") ? "visible" : "hidden");
            $("#" + (isGrowthFixed ? "recoveryPlanDiscountIncrement" : "recoveryPlanInvestmentGrowth") + "Asterix").css("visibility", "hidden");
            setRecoveryPlan();
        });

        $('input:radio[name="recoveryPlanStrategyType"]').change(function () {            
            setRecoveryPlan();
        });

        initialised = true;
    };

    this.refresh = function (data) {
        if (!initialised) {
            clearReadonly();
            initialise(data);
        };
        refreshControls(data);
    };

    var clearReadonly = function () {
        $("#analysisdashboard_recoveryplan").find("input[type=radio]").removeAttr("disabled");
        $("#analysisdashboard_recoveryplan").find(".readonly-rp").removeClass("readonly-rp");
        $("#analysisdashboard_recoveryplan").find(".greySlider").removeClass("greySlider");
    }

    var refreshControls = function (data) {

        clearReadonly();

        var isGrowthFixed = data.IsGrowthFixed;

        $('input[name=recoveryPlanStrategyType][value=' + data.StrategyType + ']').prop("checked", true);

        $('#recoveryPlanGrowthFixed').prop('checked', isGrowthFixed);
        $('#recoveryPlanGrowthNotFixed').prop('checked', !isGrowthFixed);

        $('#recoveryPlanLength').slider("value", data.Length);
        $('#recoveryPlanLength').slider('setTextValueElement', data.Length);

        $('#recoveryPlanIncreases').slider("value", data.Increases);
        $('#recoveryPlanIncreases').slider('setTextValueElement', data.Increases);

        $('#recoveryPlanInvestmentGrowth').slider("value", data.InvestmentGrowth);
        $('#recoveryPlanInvestmentGrowth').slider('setTextValueElement', data.InvestmentGrowth);

        $('#recoveryPlanDiscountIncrement').slider("value", data.DiscountIncrement);
        $('#recoveryPlanDiscountIncrement').slider('setTextValueElement', data.DiscountIncrement);

        $('#recoveryPlanLumpSumPayment').slider("value", data.LumpSumPayment);
        $('#recoveryPlanLumpSumPayment').slider('setTextValueElement', data.LumpSumPayment);

        $('#recoveryPlanStartMonth').slider("value", data.StartMonth);
        $('#recoveryPlanStartMonth').slider('setTextValueElement', data.StartMonth);


        $("#recoverPlan_refGiltYieldRow td:last").html((data.giltYield * 100).toFixed(2) + '%');

        $('#absoluteAssetReturn').html((data.weightedReturnParameters.AbsoluteAssetReturn * 100).toFixed(2) + '%');
        $('#recoveryPlanWeightedAssetReturn').html((data.weightedReturnParameters.WeightedAssetReturn * 100).toFixed(2) + '%');
        $('#recoveryPlanWeightedDiscountRate').html((data.weightedReturnParameters.WeightedDiscountRate * 100).toFixed(2) + '%');
        $('#recoveryPlanWeightedDiscountRateRelative').html((data.weightedReturnParameters.WdrRelativeToGilts * 100).toFixed(2) + '%');

        var thresholdExceeded = function (id) {
            $("#outperformance-warning").css("visibility", "visible");
            $("#" + id + "Asterix").css("visibility", "visible");
        };
        var thresholdDeceeded = function (id) {
            $("#outperformance-warning").css("visibility", "hidden");
            $("#" + id + "Asterix").css("visibility", "hidden");
        };

        $('#recoveryPlanInvestmentGrowth').slider("setSoftLimit", {
            value: data.fixedAssetWarningThreshold,
            onThresholdExceeded: function() { thresholdExceeded('recoveryPlanInvestmentGrowth') },
            onThresholdDeceeded: function () { thresholdDeceeded('recoveryPlanInvestmentGrowth') }
        });
        $('#recoveryPlanDiscountIncrement').slider("setSoftLimit", {
            value: data.discountRateWarningThreshold,
            onThresholdExceeded: function() { thresholdExceeded('recoveryPlanDiscountIncrement') },
            onThresholdDeceeded: function() { thresholdDeceeded('recoveryPlanDiscountIncrement') }
        });

        $("#outperformance-warning").css("visibility", "hidden");
        $("#recoveryPlanInvestmentGrowthAsterix").css("visibility", "hidden");
        $("#recoveryPlanDiscountIncrementAsterix").css("visibility", "hidden");
        if (isGrowthFixed) {
            $('#recoveryPlanInvestmentGrowth').show();
            $('#recoveryPlanDiscountIncrement').hide().parent().siblings().last().html('');
            $('#recoveryPlanInvestmentGrowth').slider('setTextValueElement', data.InvestmentGrowth);
            if (data.InvestmentGrowth > data.fixedAssetWarningThreshold) {
                $("#outperformance-warning").css("visibility", "visible");
                $("#recoveryPlanInvestmentGrowthAsterix").css("visibility", "visible");
            }
        } else {
            $('#recoveryPlanDiscountIncrement').show();
            $('#recoveryPlanInvestmentGrowth').hide().parent().siblings().last().html('');
            $('#recoveryPlanDiscountIncrement').slider('setTextValueElement', data.DiscountIncrement);
            if (data.DiscountIncrement > data.discountRateWarningThreshold) {
                $("#outperformance-warning").css("visibility", "visible");
                $("#recoveryPlanDiscountIncrementAsterix").css("visibility", "visible");
            }
        }

        $('#recoveryPlanInvestmentGrowth').toggle(isGrowthFixed);
        $('#recoveryPlanDiscountIncrement').toggle(!isGrowthFixed);

        if ($("#analysis_recoveryplan").is(":visible")) {            
            $(".assumptionsSliderWithSoftLimit", $("#analysis_recoveryplan")).slider("setSoftLimitTick");
        };

        // set readonly mode
        if (data.StrategyType == "NewFixed") {

            $("#recoveryPlanInvestmentGrowth a").css("background", "");
            $("#recoveryPlanDiscountIncrement a").css("background", "");

            $("#outperformanceTypeTable").find("input[type=radio]").attr("disabled", "disabled");

            $("#outperformanceTypeTable").find(".assumptionsSlider").addClass("readonly-rp").closest("td").addClass("greySlider");
            $("#outperformanceTypeTable").find(".assumptionsSlider").each(function () {
                $(this).slider('disableEditableElement', this);
                $(this).slider("option", "disabled", true);
            });

            $("#recoveryPlanTable").find(".assumptionsSlider").addClass("readonly-rp").closest("td").addClass("greySlider");
            $("#recoveryPlanTable").find(".assumptionsSlider").each(function () {
                $(this).slider('disableEditableElement', this);
            });
        }
        else {
            $("#outperformanceTypeTable").find(".assumptionsSlider").each(function () {
                $(this).slider("option", "disabled", false);
            });
        }
    };
};
