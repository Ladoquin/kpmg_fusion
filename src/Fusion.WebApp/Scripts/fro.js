﻿fusion.analysis.FroPanel = function() {

    var that = this;
    var initialised = false;
    var isBuyoutBasis = false;

    this.changed = $.Callbacks();

    var updateCell = function (event, ui) {
        $(this).slider("updateSliderValue", {
            el: $(this),
            value: ui.value
        });
    };

    var updateFro = function (event, ui) {
        that.setFro();
    };

    var updateEFro = function (event, ui) {
        that.setEfro();
    };

    var initialise = function (froData) {

        var froETV = $('#froETV');
        var froTakeUpRate = $('#froTakeUpRate');

        froETV.data("original", froData.setup.FROMinTransferValue);
        froETV.data("original-tooltip", fusion.util.formatCurrency(froData.setup.FROMinTransferValue));
        froETV.slider({
            animate: 400,
            range: 'min',
            min: froData.setup.FROMinTransferValue,
            max: froData.setup.FROMaxTransferValue,
            value: froData.EquivalentTansferValue,
            step: (froData.setup.FROMaxTransferValue - froData.setup.FROMinTransferValue) / 100,
            slide: updateCell,
            stop: updateFro,
            elementSelector: froETV,
            textElement: froETV.parent().siblings().last()
        });
        froETV.slider('setTextValueElement');

        froTakeUpRate.data("original", froData.setup.TakeUpRate);
        froTakeUpRate.slider({
            animate: 400,
            range: 'min',
            min: froTakeUpRate.data('min'),
            max: froTakeUpRate.data('max'),
            value: froData.TakeUpRate,
            step: froTakeUpRate.data('increment'),
            slide: updateCell,
            stop: function (event, ui) {
                froSlidersHandler();
                updateFro(event, ui);
            },
            create: function(){froTakeUpRate.slider('setTextValueElement')},
            elementSelector: froTakeUpRate,
            textElement: froTakeUpRate.parent().siblings().last()
        });
        froTakeUpRate.slider('setTextValueElement');


        var eFroETV = $('#eFroTransferValue');
        var eFroTakeUpRate = $('#eFroTakeUpRate');

        eFroETV.data("original", Math.max(eFroETV.data('min'), froData.eFroData.setup.EmbeddedFroBasisChange));
        eFroETV.slider({
            animate: 400,
            range: 'min',
            min: eFroETV.data('min'),
            max: eFroETV.data('max'),
            value: froData.eFroData.EmbeddedFroBasisChange,
            step: eFroETV.data('increment'),
            slide: updateCell,
            stop: updateEFro,
            create: function () { eFroETV.slider('setTextValueElement') },
            elementSelector: eFroETV,
            textElement: eFroETV.parent().siblings().last()
        });
        eFroETV.slider('setTextValueElement');

        eFroTakeUpRate.data("original", froData.eFroData.setup.AnnualTakeUpRate);
        eFroTakeUpRate.slider({
            animate: 400,
            range: 'min',
            min: eFroTakeUpRate.data('min'),
            max: eFroTakeUpRate.data('max'),
            value: froData.eFroData.AnnualTakeUpRate,
            step: eFroTakeUpRate.data('increment'),
            slide: updateCell,
            stop: function (event, ui) {
                slidersDependentsHandler(eFroTakeUpRate, eFroETV, 0);
                updateEFro(event, ui);
            },
            create: function () { eFroTakeUpRate.slider('setTextValueElement') },
            elementSelector: eFroTakeUpRate,
            textElement: eFroTakeUpRate.parent().siblings().last()
        });
        eFroTakeUpRate.slider('setTextValueElement');


        $('#resetFro').click(function () {
            that.resetFro();
        });

        $('#reseteFro').click(function () {
            that.reseteFro();
        });

        $('input:radio[name=froOption]').change(function () {
            if (!isBuyoutBasis) {
                if ($('#froOptionBulk').is(':checked')) {
                    disableFroEmbedOptions();
                    updateFro();
                }
                else {
                    disableFroBulkOptions();
                    updateEFro();
                }
            }
        });

        initialised = true;
    };

    var disableFroEmbedOptions = function () {
        $('h4, th, td, span', $('#froEmbedControls')).each(function () {
            $(this).addClass('fro-disabled-text');
        });
        $('h4, th, td, span', $('#froBulkControls')).each(function () {
            $(this).removeClass('fro-disabled-text');
        });
        fusion.util.enableSlider($('#froETV'));
        fusion.util.enableSlider($('#froTakeUpRate'));
        fusion.util.disableSlider($('#eFroTransferValue'));
        fusion.util.disableSlider($('#eFroTakeUpRate'));
        $('#resetFro').attr('disabled', false);
        $('#reseteFro').attr('disabled', true);
        $('#resetFro').removeClass('fro-disabled-button');
        $('#reseteFro').addClass('fro-disabled-button');
        setPanelState(5, $("#li-analysisdashboard_etv"), true, '');
        setPanelState(7, $("#li-analysisdashboard_futurebenefits"), true, 'Unavailable for selected basis');
    };

    var disableFroBulkOptions = function () {
        $('h4, th, td, span', $('#froBulkControls')).each(function () {
            $(this).addClass('fro-disabled-text');
        });
        $('h4, th, td, span', $('#froEmbedControls')).each(function () {
            $(this).removeClass('fro-disabled-text');
        });
        fusion.util.enableSlider($('#eFroTransferValue'));
        fusion.util.enableSlider($('#eFroTakeUpRate'));
        fusion.util.disableSlider($('#froETV'));
        fusion.util.disableSlider($('#froTakeUpRate'));
        $('#froSizeOfCetv').html('<span class="fro-disabled-text">n/a</span>');
        $('#resetFro').attr('disabled', true);
        $('#reseteFro').attr('disabled', false);
        $('#resetFro').addClass('fro-disabled-button');
        $('#reseteFro').removeClass('fro-disabled-button');
        setPanelState(5, $("#li-analysisdashboard_etv"), false, '');
        setPanelState(7, $("#li-analysisdashboard_futurebenefits"), false, 'Unavailable for embedded FRO');
    };

    var setPanelState = function (idx, li, enabled, disabledTitleText) {
        var $analysisdashboard = $("#analysisdashboard")
        if ($analysisdashboard.data("preventOverride"))
            return;
        if ($analysisdashboard.tabs("option", "active") == idx) {
            $analysisdashboard.tabs({ active: 0 });
        };
        if (disabledTitleText != '')
            $(li).data('disabled-title', disabledTitleText);
        $analysisdashboard.tabs(enabled ? "enable" : "disable", idx);
        $(li).attr('title', enabled ? '' : $(li).data('disabled-title'));
    };

    var froSlidersHandler = function () {
        var froETV = $('#froETV');
        var froTakeUpRate = $('#froTakeUpRate');
        var froSizeOfCetv = $('#froSizeOfCetv');

        slidersDependentsHandler(froTakeUpRate, froETV, 0);
        if (getSliderValue(froTakeUpRate) <= 0)
            froSizeOfCetv.html('<span class="fro-disabled-text">n/a</span>');
    };

    var slidersDependentsHandler = function (parentSlider, childSlider, minValue) {
        if (getSliderValue(parentSlider) <= minValue)
            fusion.util.disableSlider($(childSlider));
        else
            fusion.util.enableSlider($(childSlider));
    };

    var getSliderValue = function (sliderObject) {
        var sliderValue = sliderObject.parent().parent().find('.slider-text-value').data('finalValue');
        if (sliderValue == null || sliderValue == typeof 'undefined')
            sliderValue = sliderObject.slider("option", "value");
        return sliderValue;
    };

    this.resetFro = function () {

        $.getJSON('/Json/ResetFro', function (froData) {
            that.refreshControls(froData);
            that.changed.fire({ id: that.id, data: froData });
        });
    };

    this.reseteFro = function () {

        $.getJSON('/Json/ResetEFro', function (eFroData) {
            that.refreshControls(eFroData);
            that.changed.fire({ id: that.id, data: eFroData });
        });
    };
    
    this.setFro = function () {
        var fro = {},
            finalEtf,
            finalTur;

        finalEtf = $('#froETV').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalEtf == null || finalEtf == typeof 'undefined')
            finalEtf = $('#froETV').slider("option", "value");
        fro.equivalentTansferValue = finalEtf;

        finalTur = $('#froTakeUpRate').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalTur == null || finalTur == typeof 'undefined')
            finalTur = $('#froTakeUpRate').slider("option", "value");
        fro.takeUpRate = finalTur;

        var json = {
            froData: JSON.stringify(fro)
        };

        $.post('/Json/SetFro', json, function (froData) {
            that.refreshControls(froData);
            that.changed.fire({ id: that.id, data: froData });
        });
    };

    this.setEfro = function () {
        var eFro = {},
            eFroTV,
            eFroTakeUp;

        eFroTV = $('#eFroTransferValue').parent().parent().find('.slider-text-value').data('finalValue');
        if (eFroTV == null || eFroTV == typeof 'undefined')
            eFroTV = $('#eFroTransferValue').slider("option", "value");
        eFro.equivalentTansferValue = eFroTV;

        eFroTakeUp = $('#eFroTakeUpRate').parent().parent().find('.slider-text-value').data('finalValue');
        if (eFroTakeUp == null || eFroTakeUp == typeof 'undefined')
            eFroTakeUp = $('#eFroTakeUpRate').slider("option", "value");
        eFro.takeUpRate = eFroTakeUp;

        var json = {
            eFroData: JSON.stringify(eFro)
        };

        $.post('/Json/SetEFro', json, function (eFroData) {
            that.refreshControls(eFroData);
            that.changed.fire({ id: that.id, data: eFroData });
        });
    };
    
    this.reInit = function () {
        initialised = false;
    };
    
    this.refreshControls = function (froData)
    {

        isBuyoutBasis = froData.isBuyoutBasis != null && froData.isBuyoutBasis == true;
        if (isBuyoutBasis) {
            $('#froOptionEmbedding').attr('disabled', true);
            $('#froOptionEmbedding').removeAttr('checked');
            $('#froOptionBulk').prop('checked', true);
            $('#eFroRadioText').addClass('fro-disabled-text');
        }
        else {
            $('#froOptionEmbedding').attr('disabled', false);
            $('#eFroRadioText').removeClass('fro-disabled-text');
        }

        if (!initialised) {
            initialise(froData);
        };

        var isBulkFro = froData.froType == 1 || isBuyoutBasis;
        if (isBulkFro) {
            disableFroEmbedOptions();
            $('#froOptionBulk').prop('checked', true);
            $('#froDeferredLiabilities').html(fusion.util.formatCurrency(froData.Over55));
            $('#froSizeOfCetv').html((froData.SizeOfTV * 100).toFixed(2) + "%");

            $('#froETV').slider('value', froData.EquivalentTansferValue);
            $('#froETV').slider('setTextValueElement', froData.EquivalentTansferValue);
            $('#froETV').slider('updateSliderValue', { value: froData.EquivalentTansferValue });

            $('#froTakeUpRate').slider('value', froData.TakeUpRate);
            $('#froTakeUpRate').slider('setTextValueElement', froData.TakeUpRate);
            $('#froTakeUpRate').slider('updateSliderValue', { value: froData.TakeUpRate });
            froSlidersHandler();
        }
        else {
            disableFroBulkOptions();
            $('#froOptionEmbedding').prop('checked', true);

            $('#eFroTransferValue').slider('value', froData.eFroData.EmbeddedFroBasisChange);
            $('#eFroTransferValue').slider('setTextValueElement', froData.eFroData.EmbeddedFroBasisChange);
            $('#eFroTransferValue').slider('updateSliderValue', { value: froData.eFroData.EmbeddedFroBasisChange });

            $('#eFroTakeUpRate').slider('value', froData.eFroData.AnnualTakeUpRate);
            $('#eFroTakeUpRate').slider('setTextValueElement', froData.eFroData.AnnualTakeUpRate);
            $('#eFroTakeUpRate').slider('updateSliderValue', { value: froData.eFroData.AnnualTakeUpRate });
            slidersDependentsHandler($('#eFroTakeUpRate'), $('#eFroTransferValue'), 0);
        }
        $('#froCetvPaid').html(fusion.util.formatCurrency(froData.CetvPaid));
        $('#froLiabilityDischarged').html(fusion.util.formatCurrency(froData.LiabilityDischarged));
        $('#froFundingImprovement').html(fusion.util.formatCurrency(froData.Improvement));
        $('#eFroDeferredLiabilities').html(fusion.util.formatCurrency(froData.eFroData.NonPensionersLiabilities));
        $('#eFroFundingImprovement').html(fusion.util.formatCurrency(froData.eFroData.Improvement));
    };

    this.refresh = function (froData) {

        if (!initialised) {
            initialise(froData);
        };

        $('#eFroTransferValue').slider('value', froData.eFroData.EmbeddedFroBasisChange);
        $('#eFroTakeUpRate').slider('value', froData.eFroData.AnnualTakeUpRate);

        that.refreshControls(froData);

    };
};

