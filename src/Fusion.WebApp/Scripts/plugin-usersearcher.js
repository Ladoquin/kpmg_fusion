﻿(function ($) {

    $.fn.userSearcher = function () {
        $.each(this, function (idx, el) {
            $.getJSON($(el).attr('data-usersource'), function (data) {
                $(el).autocomplete({
                    source: data,
                    minLength: 2,
                });
            });
        });
    };

}(jQuery));
