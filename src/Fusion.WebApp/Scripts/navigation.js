/*
*	KPMG
*	navigation.js
*	Space01 Ltd. 2013
* 	author : Rob Lone - info@fusionrobotdesign.com
*/

/*This controls how the navigation operates. */

fusion.navigation ={
	init: function(){
		$('.firstmenu li').hover(function() {
			$('.firstmenu li').removeClass("hover");
			$(this).addClass("hover");
		},function() {
		    $(this).removeClass("hover");
		});
		
		$('.secondmenu li').hover(function() {
			$('.secondmenu li').removeClass("hover");
			$(this).addClass("hover");
		},function() {
		    $(this).removeClass("hover");
		});
	}
};