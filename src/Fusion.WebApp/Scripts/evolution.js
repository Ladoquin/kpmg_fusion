﻿$(function () {
    $(document).ready(function () {

        fusion.page.init({ idx: 2, name: "evolution" });
        fusion.tabs.init();
        fusion.dashboardHandler.init();

        $('#expandingEvolutionOfLiabilities').expandingarea();

        var setComparisonChartWidth = function () {
            $('#trackerComparisonChartDiv').css('width', $('#trackerValuationChartDiv').outerWidth() + 'px');
        };
        $(window).resize(function () {
            setComparisonChartWidth();
        });
        setComparisonChartWidth();

        var tracker = new fusion.TrackerClass();

        fusion.basisChangedEvent.add(function () {
            $.getJSON('/Evolution/Tracker', function (trackerData) {
                tracker.refresh(trackerData);
            });
        });

        $('input.analysisPeriodStart,input.analysisPeriodEnd').change(function () {
            var startTimeDatePicker = new Date(Date.UTC(
                parseInt($('input.analysisPeriodStart').val().split("/")[2]),
                parseInt($('input.analysisPeriodStart').val().split("/")[1]) - 1,
                parseInt($('input.analysisPeriodStart').val().split("/")[0]))).getTime();
            var endTimeDatePicker = new Date(Date.UTC(
                parseInt($('input.analysisPeriodEnd').val().split("/")[2]),
                parseInt($('input.analysisPeriodEnd').val().split("/")[1]) - 1,
                parseInt($('input.analysisPeriodEnd').val().split("/")[0]))).getTime();


            tracker.selectRange(startTimeDatePicker, endTimeDatePicker);

        });

        $.getJSON('/Evolution/Tracker', function (trackerData) {
            tracker.refresh(trackerData);
        });

        tracker.analysisPeriodChanged.add($.Core.evolutionAssumptionsPanel.refresh);
        tracker.analysisPeriodChanged.add(function (data) {
            $('input.analysisPeriodStart').val(Highcharts.dateFormat('%d/%m/%Y', data.start));
            $('input.analysisPeriodEnd').val(Highcharts.dateFormat('%d/%m/%Y', data.end));
        });

        $.Core.evolutionAssumptionsPanel.refresh();
    });
});
