﻿fusion.charts.donutHelper = {
    // 
    // Highcharts does not behave as one would like if you attempt to manipulate the margin in the post-draw event handler,
    // but in order to set the margin during the draw stage you need to know the legend height before it has been drawn. 
    // Due to the user possibly changing their browser zoom setting the most accurate way of getting the legend line height
    // is by creating a new Highchart instance and querying that. 
    getLegendLineHeight: function () {
        if ($("#container div#dummyChart").length === 0) {
            $("#container").append("<div id='dummyChart' style='display:none'></div>");
        };
        var dummyChart = new Highcharts.Chart({
            chart: { renderTo: 'dummyChart' },
            series: [1],
        });
        return dummyChart.legend.itemHeight;
    }
};

fusion.charts.donutHandler = function () {

    var chart;
    var chartDivId;
    var nextDivMarginTop = false;

    var getSettings = function (spec, series) {

        var settings = {
            height: spec.height || $("#" + spec.chartDivId).height(),
            marginTop: spec.marginTop,
            marginBottom: spec.marginBottom,
            spacingTop: 0,
            spacingBottom: 0,
            legendAlign: spec.legendAlign
        };

        var l = spec.legend || {};

        var total = 0;
        $.each(series[0].data, function () { total += this.y; });

        settings.title = {
            text: fusion.util.formatCurrencyAbsolute(total),
            fontSize: total < 1000000000 ? '24px' : (total < 10000000000 ? '20px' : '18px') // unless you'd like to work out the inner pie diameter? 
        };

        settings.tooltip = spec.tooltip || {};
        settings.tooltip.formatter = settings.tooltip.formatter ||
                function () { return this.point.name + ': ' + fusion.util.formatCurrencyAbsolute(this.y) + ' (' + this.percentage.toFixed(1) + '%' + ')'; };

        settings.legend = {
            enabled: typeof l.enabled == "undefined" ? true : l.enabled,
            align: l.align || 'center',
            verticalAlign: l.verticalAlign || 'bottom',
            layout: 'vertical',
            borderWidth: 0,
            borderRadius: 0,
            useHTML: true,
            y: l.y || 0,
            labelFormatter: function () {
                var enders = [];
                if (l.splitSeries && series.length > 0) {
                    for (var i = 0; i < series.length - 1; i++) {
                        enders.push(series[i].data[series[i].data.length - 1].name);
                    }
                };
                var label = this.name + (l.showValues ? " (" + fusion.util.formatCurrencyAbsolute(this.y) + ")" : "");
                if ($.inArray(this.name, enders) > -1) {
                    label += "<br/><br/>";
                }
                return label;
            }
        };

        if (settings.legend.align === 'left') {
            var lineHeight = Math.ceil(fusion.charts.donutHelper.getLegendLineHeight());
            var legendHeight = ((series[0].data.length + (series.length > 1 ? series[1].data.length : 0) + (spec.splitSeries ? 2 : 0)) * lineHeight) + 3;
            var pieHeight = (settings.height - settings.spacingTop - settings.spacingBottom);
            if (legendHeight > pieHeight) {
                var s = legendHeight - pieHeight;
                settings.height = settings.height + s;
                settings.marginBottom = settings.spacingBottom + s;
                var container = $("#" + spec.chartDivId).next();
                if (container.length > 0) {
                    if (nextDivMarginTop === undefined) {
                        nextDivMarginTop = container[0].style["margin-top"] !== "" ? parseInt(container[0].style["margin-top"].replace("px", "")) : 0;
                    };
                    $(container[0]).css({ position: 'relative', top: (nextDivMarginTop + s + 20).toString() + "px" });
                };
            };
            ;
        };

        return settings;
    };

    return {
        redraw: function (series, spec, forceRedraw) {

            var settings = getSettings(spec, series);

            chartDivId = spec.chartDivId;

            if (chart === undefined || !$("#" + spec.chartDivId).is(":visible") || forceRedraw) {
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: spec.chartDivId,
                        type: 'pie',
                        backgroundColor: 'none',
                        height: settings.height,
                        marginTop: settings.marginTop,
                        marginBottom: settings.marginBottom,
                        spacingTop: settings.spacingTop,
                        spacingBottom: settings.spacingBottom
                    },
                    credits: false,
                    exporting: {
                        enabled: false,
                    },
                    tooltip: {
                        borderWidth: 0,
                        borderRadius: 0,
                        backgroundColor: '#007c92',
                        shadow: false,
                        useHTML: true,
                        formatter: settings.tooltip.formatter,
                        style: {
                            color: '#ffffff',
                            fontSize: '1.1em',
                            padding: '20px',
                            opacity: 0.8,
                        }
                    },
                    title: {
                        text: settings.title.text,
                        useHTML: true,
                        floating: true,
                        style: {
                            fontFamily: 'Lato, sans-serif',
                            fontSize: settings.title.fontSize,
                            zIndex: -1,
                            textAlign: 'center',
                        },
                        align: 'center',
                        verticalAlign: 'middle'
                    },
                    legend: { enabled: false }, //settings.legend,
                    plotOptions: {
                        pie: {
                            allowPointSelect: false,
                            slicedOffset: 0,
                            center: ['50%', '50%']
                        }
                    },
                    series: series
                },
                function (c) {
                    var x = 0 - (c.chartWidth * 0.5) + (c.plotWidth * 0.5) + c.plotLeft;
                    var y = 0 - (c.chartHeight * 0.5) + (c.plotHeight * 0.5) + c.plotTop + 12; // 12 is half the font size
                    c.setTitle({ x: x, y: y });
                });

                $("#" + spec.chartDivId).addLegend({
                    alignment: settings.legendAlign || "bottom"
                });
            }
            else {
                chart.setTitle({ text: settings.title.text, style: { fontSize: settings.title.fontSize } });
                for (var i = 0; i < series.length; i++) {
                    if (chart.series[i].data.length == series[i].data.length) {
                        for (var j = 0; j < series[i].data.length; j++) {
                            if (chart.series[i].data.length > j) {
                                chart.series[i].data[j].update(series[i].data[j], false);
                            }
                        }
                    }
                    else {
                        chart.series[i].setData(series[i].data, false);
                    }
                };
                chart.redraw();
            };
        }
    }
};