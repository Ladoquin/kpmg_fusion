﻿fusion.TrackerClass = function () {

    var that = this;
    var initialised = false;
    var valuationChart = null;
    var comparisonChart = null;
    var balanceChart = null;
    var fundingChart = null;
    var activeTrackerBasis = "";
    var basesRangeSelected = {};
    var showExpectedDeficit = false;
    var hasTouch = $.browser.ipad;

    this.analysisPeriodChanged = $.Callbacks();

    this.startDate = null;
    this.endDate = null;

    var init = function (data) {

        $("li#assetsLegend").toggle(data.showAssets);
        $("section#funding-level-section").toggle(data.showAssets);

        $("#fundingLevelChartTabs").tabs({
            active: 0,
            activate: function (event, ui) {
                if (ui.newPanel.attr('id') == "balanceTab") {
                    $("#balanceChartLegend").show();
                    $("#fundingChartLegend").hide();
                }
                else {
                    $("#balanceChartLegend").hide();
                    $("#fundingChartLegend").show();
                }
            }
        });

        Highcharts.setOptions({
            lang: {
                resetZoom: 'Reset window',
                resetZoomTitle: '',
            }
        });

        initialised = true;
    };

    var initValuationChart = function () {

        var resetting = false;

        var valuationChartEvents = {
            selection: function (event) {
                if (event.xAxis) {
                    that.selectRange(event.xAxis[0].min, event.xAxis[0].max);
                    valuationChart.showResetZoom();
                    return false;
                } else {
                    that.resetRange();
                }
            }
        };
        
        if (hasTouch) {
            valuationChartEvents = null
            $("#schemeevolution .subtitle").html("Touch to select the start and end dates for your analysis period");
        }

        var touchSelectDateRange = function (event) {
            if (!$("#trackerValuationChartDiv").attr("data-first-touch") || $("#trackerValuationChartDiv").attr("data-first-touch").length == 0) {
                var firsttouch = event.x;
                $("#trackerValuationChartDiv").attr("data-first-touch", firsttouch);
            }
            else {
                var firsttouch = $("#trackerValuationChartDiv").attr("data-first-touch");
                $("#trackerValuationChartDiv").removeAttr("data-first-touch");
                var secondtouch = event.x;

                var tempTouch = null;
                if (firsttouch > secondtouch) {
                    tempTouch = secondtouch;
                    secondtouch = firsttouch;
                    firsttouch = tempTouch;
                }

                if (resetting === false)
                    that.selectRange(firsttouch, secondtouch);
            }
        };

        var clearTouchHistory = function () {
            $("#trackerValuationChartDiv").removeAttr("data-first-touch");
        };

        valuationChart = new Highcharts.Chart({
            chart: {
                renderTo: 'trackerValuationChartDiv',
                type: 'spline',
                animation: true,
                events: valuationChartEvents,
                resetZoomButton: {
                    relativeTo: 'plot',
                    theme: {
                        fill: '#3e3e3e',
                        stroke: '#3e3e3e',
                        style: {
                            color: 'white',
                            padding: '20px'
                        },
                        r: 2,
                        states: {
                            hover: {
                                fill: '#3e3e3e',
                                stroke: '#3e3e3e',
                                style: {
                                    color: 'white'
                                }
                            }
                        }
                    },
                    position: {
                        x: -10,
                        y: -50
                    }
                },
                zoomType: (hasTouch ? null : 'x'),
                marginBottom: 40,
            },
            credits: false,
            exporting: {
                enabled: false
            },
            plotOptions: {
                spline: {
                    lineWidth: 2,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2
                    },
                    events: {
                        legendItemClick: function () {
                            return false;
                        }
                    }
                }
            },
            title: {
                text: 'Scheme evolution',
                style: {
                    fontFamily: 'Lato, sans-serif',
                    fontSize: '32px',
                    color: '#3e3e3e'
                }
            },
            subtitle: {
                text: 'Click the chart and drag to select your analysis period',
                style: {
                    fontFamily: 'Lato, sans-serif',
                    fontSize: '18px',
                    color: '#3e3e3e'
                }
            },
            xAxis: {
                type: 'datetime',
                labels: {
                    y: 30,
                    maxStaggerLines: 1,
                },
                dateTimeLabelFormats: {
                    day: '%e/%m/%y',
                    week: '%e/%m/%y',
                    month: '%b %Y',
                    year: '%Y'
                },
                tickLength: 10,
                events: {
                    setExtremes: function (a) {    // fires when resetZoomButton pressed
                        if (valuationChart.resetZoomButton) { // this also fires on intial load at which time the button may not exist
                            valuationChart.resetZoomButton = valuationChart.resetZoomButton.destroy(); //Every time the resetZoomButton is shown ANOTHER button is added to the svg. This works around this highcharts issue.
                            resetting = true; // flag used to prevent touch events being registered which the range is being reset
                            window.setTimeout(function () { // another highcharts work around
                                that.resetRange(); // reset the range here as valuationChartEvents is set to null above for touch enabled devices
                                resetting = false;
                                clearTouchHistory(); // as another touch event (in addition to resetZoom) is fired form the chart when the button overlaying the chart is pressed
                            }, 0);
                        }
                    }
                }
            },
            yAxis: [{
                title: {
                    text: null
                },
                labels: {
                    style: {
                        minWidth: '50px'
                    },
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                }
            }, {
                title: {
                    text: null
                },
                labels: {
                    style: {
                        minWidth: '50px'
                    },
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                },
                linkedTo: 0,
                opposite: true,
            }],
            tooltip: {
                shared: true,
                crosshairs: true,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: null,
                shadow: false,
                useHTML: true,
                formatter: function () {
                    if (resetting === true) {
                        return false;
                    }

                    if (hasTouch)
                        touchSelectDateRange(this);

                    var assetPointsIdx = 0;
                    var liabilityPointsIdx = valuationChart.series[0].visible ? 1 : 0;
                    var importText = "";
                    if (this.points[assetPointsIdx].point.i.a) {
                        importText += "IMPORT OF: " + this.points[assetPointsIdx].point.series.name;
                    };
                    if (this.points[liabilityPointsIdx].point.i.l) {
                        if (importText.length === 0) {
                            importText += "IMPORT OF: "
                        }
                        else {
                            importText += ", ";
                        };
                        importText += this.points[liabilityPointsIdx].point.series.name;
                    };
                    var bg = importText.length > 0 ? "#00925f" : "#007c92";
                    var html = "<div style='background-color: " + bg + "; padding: 20px; color: #ffffff; fontSize: 1.1em; opacity: 0.8;'>";
                    html += Highcharts.dateFormat('%a, %e %B %Y', this.x) + '<br/>';
                    if (importText.length > 0) {
                        html += "<br/>" + importText + "<br/>";
                    }
                    $.each(this.points, function (i, point) {
                        html += '<br/>' + point.series.name + ': ' +
                            fusion.util.formatCurrencyAbsolute(point.y);
                    });
                    if (valuationChart.series[0].visible && valuationChart.series[1].visible) {
                        html += "<br/>Funding level: " + ((this.points[0].point.y / this.points[1].point.y) * 100).toFixed(1) + "%";
                    }
                    html += "</div>";
                    return html;
                }
            },
            legend: {
                enabled: true,
                layout: 'vertical',
                floating: true,
                align: 'left',
                verticalAlign: 'top',
                borderWidth: 0,
                borderRadius: 0,
                padding: 10,
                y: -10,
                itemMarginTop: 8,
                itemStyle: {
                    fontFamily: 'Lato, sans-serif',
                    fontSize: '14px',
                    color: '#3e3e3e'
                }
            },
            series: [
                {
                    turboThreshold: 0, // defaults to a 1000, 0 disables it, this series might have more than a 1000 data items
                    name: 'Assets',
                    color: '#E87424',
                }, {
                    turboThreshold: 0,
                    name: 'Liabilities',
                    color: '#8F258D',
                }
            ]
        });
    };

    var initComparisonChart = function () {

        comparisonChart = new Highcharts.Chart({
            chart: {
                renderTo: 'trackerComparisonChartDiv',
                type: 'spline',
                animation: true,
                marginBottom: 70
            },
            credits: false,
            exporting: {
                enabled: false
            },
            plotOptions: {
                spline: {
                    lineWidth: 2,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2
                    },
                    events: {
                        legendItemClick: function () {
                            return false;
                        }
                    }
                }
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                labels: {
                    y: 30,
                    maxStaggerLines: 1,
                },
                dateTimeLabelFormats: {
                    day: '%e/%m/%y',
                    week: '%e/%m/%y',
                    month: '%b %Y',
                    year: '%Y'
                },
                tickLength: 10,
            },
            yAxis: [{
                title: {
                    text: null
                },
                labels: {
                    style: {
                        minWidth: '50px'
                    },
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                }
            }, {
                title: {
                    text: null
                },
                labels: {
                    style: {
                        minWidth: '50px'
                    },
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                },
                linkedTo: 0,
                opposite: true,
            }],
            tooltip: {
                shared: true,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                formatter: function () {
                    var html = Highcharts.dateFormat('%a, %e %B %Y', this.x) + '<br/><br/>';
                    for (var i = 0; i < this.points.length; i++) {
                        html += this.points[i].series.name + ": " + fusion.util.formatCurrency(this.points[i].y) + "<br/>";
                    };
                    return html;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            legend: {
                borderWidth: 0,
                verticalAlign: 'bottom',
                y: 10
            }
        });
    }

    var initBalanceChart = function () {

        balanceChart = new Highcharts.Chart({
            chart: {
                renderTo: 'trackerBalanceChartDiv',
                animation: true,
                events: {
                    selection: function (event) {
                        if (event.xAxis) {
                            that.selectRange(event.xAxis[0].min, event.xAxis[0].max);
                            valuationChart.showResetZoom();
                            return false;
                        } else {
                            that.resetRange();
                        }
                    }
                },
                zoomType: hasTouch ? null : 'x',
                marginTop: 30,
                width: 1080
            },
            credits: false,
            exporting: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                },
                spline: {
                }
            },
            title: {
                text: null,
            },
            xAxis: {
                type: 'datetime',
                labels: {
                    y: -20,
                    maxStaggerLines: 1
                },
                dateTimeLabelFormats: {
                    day: '%e/%m/%y',
                    week: '%e/%m/%y',
                    month: '%b %Y',
                    year: '%Y'
                },
                tickLength: 10,
                opposite: true,
            },
            yAxis: [{
                title: {
                    text: null
                },
                labels: {
                    style: {
                        minWidth: '50px'
                    },
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                }
            }, {
                title: {
                    text: null
                },
                labels: {
                    style: {
                        minWidth: '50px'
                    },
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                },
                linkedTo: 0,
                opposite: true,
            },
             {
                 title: {
                     text: null
                 },
                 labels: {
                     enabled: false
                 }
             }],
            tooltip: {
                shared: true,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                formatter: function () {
                    var html = Highcharts.dateFormat('%a, %e %B %Y', this.x) + '<br/>';
                    html += '<br/>' + (this.points[0].y < 0 ? 'Deficit: ' : 'Surplus: ') + fusion.util.formatCurrencyAbsolute(this.points[0].y);
                    if (showExpectedDeficit) {
                        html += '<br/>' + (this.points[1].y < 0 ? 'Expected Deficit: ' : 'Expected Surplus: ') + fusion.util.formatCurrencyAbsolute(this.points[1].y);
                    }

                    return html;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            legend: {
                enabled: false,
                layout: 'horizontal',
                floating: true,
                align: 'right',
                verticalAlign: 'bottom',
                borderWidth: 0,
                borderRadius: 0,
                padding: 10,
                y: 20,
            },
            series: [{
                name: 'Balance',
                type: 'areaspline',
                color: '#6a7f10',
                negativeColor: '#9e3039',
                lineWidth: 0,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2
                }
            }, {
                yAxis: 1,
                name: 'Expected Balance',
                type: 'spline',
                color: 'darkgreen',
                negativeColor: 'darkred',
                dashStyle: 'shortdash',
                lineWidth: 2,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2
                }
            }]
        });
    };

    var initFundingChart = function () {

        fundingChart = new Highcharts.Chart({
            chart: {
                renderTo: 'trackerFundingChartDiv',
                animation: true,
                events: {
                    selection: function (event) {
                        if (event.xAxis) {
                            that.selectRange(event.xAxis[0].min, event.xAxis[0].max);
                            valuationChart.showResetZoom();
                            return false;
                        } else {
                            that.resetRange();
                        }
                    }
                },
                zoomType: hasTouch ? null: 'x',
                marginTop: 30,
                width: 1080
            },
            credits: false,
            exporting: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                },
                spline: {
                }
            },
            title: {
                text: null,
            },
            xAxis: {
                type: 'datetime',
                labels: {
                    y: -20,
                    maxStaggerLines: 1
                },
                dateTimeLabelFormats: {
                    day: '%e/%m/%y',
                    week: '%e/%m/%y',
                    month: '%b %Y',
                    year: '%Y'
                },
                tickLength: 10,
                opposite: true,
            },
            yAxis: [{
                title: {
                    text: null
                },
                labels: {
                    style: {
                        minWidth: '50px'
                    },
                    useHTML: true,
                    formatter: fusion.util.formatChartPercentageLabels,
                }
            }, {
                title: {
                    text: null
                },
                labels: {
                    style: {
                        minWidth: '50px'
                    },
                    useHTML: true,
                    formatter: fusion.util.formatChartPercentageLabels,
                },
                linkedTo: 0,
                opposite: true,
            }],
            tooltip: {
                shared: true,
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                formatter: function () {
                    var html = Highcharts.dateFormat('%a, %e %B %Y', this.x) + '<br/>';
                    html += '<br/>Funding Level: ' + this.points[0].y + '%';
                    if (showExpectedDeficit)
                        html += '<br/>Expected: ' + this.points[1].y + '%';

                    return html;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            legend: {
                enabled: false,
                layout: 'horizontal',
                floating: true,
                align: 'right',
                verticalAlign: 'bottom',
                borderWidth: 0,
                borderRadius: 0,
                padding: 10,
                y: 20,
            },
            series: [{
                name: 'Funding Level',
                //type: 'line',
                color: 'darkblue',
                negativeColor: 'darkblue',
                //dashStyle: 'shortdash',
                lineWidth: 2,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2
                }
            }, {
                yAxis: 1,
                name: 'Expected Funding Level',
                type: 'spline',
                color: 'darkgreen',
                negativeColor: 'darkred',
                dashStyle: 'shortdash',
                lineWidth: 2,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2
                }
            }]
        });
    };

    this.resetRange = function () {
        if (basesRangeSelected[activeTrackerBasis]) {
            this.selectRange(this.anchorDate, this.refreshDate);
        }
    },

    this.selectRange = function (start, end) {

        var nearestToStart = null;
        var nearestToStartIndex = 0;
        var nearestToEnd = null;
        var nearestToEndIndex = 0;

        for (var i = 0 ; i < balanceChart.series[0].data.length; i++) {
            var date = balanceChart.series[0].data[i].x;
            if (nearestToStart == null) {
                nearestToStart = date;
            } else {
                if (Math.abs(start - date) < Math.abs(start - nearestToStart)) {
                    nearestToStart = date;
                    nearestToStartIndex = i;
                }
            }
            if (nearestToEnd == null) {
                nearestToEnd = date;
            } else {
                if (Math.abs(end - date) < Math.abs(end - nearestToEnd)) {
                    nearestToEnd = date;
                    nearestToEndIndex = i;
                }
            }
        }

        this.renderRange(valuationChart, nearestToStart, nearestToEnd);
        this.renderRange(balanceChart, nearestToStart, nearestToEnd);
        this.renderRange(fundingChart, nearestToStart, nearestToEnd);

        var startBalance = balanceChart.series[0].data[nearestToStartIndex].y;
        var endBalance = balanceChart.series[0].data[nearestToEndIndex].y;
        
        var isiPad = navigator.userAgent.match(/iPad/i) != null;

        $('.analysisPeriodStart').each(function () {
            if ('value' in this) {
                $(this).datepicker({
                    showOn: isiPad ? "button" : "both",
                    minDate: $(this).data("min-date"),
                    inline: true,
                    dateFormat: 'dd/mm/yy',
                    buttonImage: 'Images/datepicker-button.png',
                    buttonText: null,
                    onClose: function (dateText, inst) {
                        if (isiPad)
                            $(this).attr("disabled", false);
                        $("input.analysisPeriodEnd").datepicker("option", "minDate", dateText);
                    },
                    beforeShow: function (dateText, inst) {
                        if (isiPad)
                            $(this).attr("disabled", true);
                    }
                }).addClass("pointer");
                $(this).val(Highcharts.dateFormat('%d/%m/%Y', nearestToStart));
            } else {
                $(this).html(Highcharts.dateFormat('%d/%m/%Y', nearestToStart));
            }
        });

        $('.analysisPeriodEnd').each(function () {
            if ('value' in this) {
                $(this).datepicker({
                    showOn: isiPad ? "button" : "both",
                    onClose: function (dateText, inst) {
                        if (isiPad)
                            $(this).attr("disabled", false);
                        $("input.analysisPeriodStart").datepicker("option", "maxDate", dateText);
                    },
                    beforeShow: function (dateText, inst) {
                        if (isiPad)
                            $(this).attr("disabled", true);
                    },
                    maxDate: $(this).data("max-date"),
                    inline: true,
                    dateFormat: 'dd/mm/yy',
                    buttonImage: 'Images/datepicker-button.png',
                    buttonText: null
                }).addClass("pointer");
                $(this).val(Highcharts.dateFormat('%d/%m/%Y', nearestToEnd));
            } else {
                $(this).html(Highcharts.dateFormat('%d/%m/%Y', nearestToEnd));
            }
        });

        $('input.analysisPeriodStart').datepicker("option", "maxDate", $('input.analysisPeriodEnd').val());
        $('input.analysisPeriodEnd').datepicker("option", "minDate", $('input.analysisPeriodStart').val());

        $('#analysisPeriodBalanceLabel').html(endBalance < 0 ? 'Deficit at end date' : 'Surplus at end date');
        $('#analysisPeriodBalance').html(fusion.util.formatCurrencyAbsolute(endBalance));

        $('#startDate').text((startBalance < 0 ? 'Deficit at ' : 'Surplus at ') + Highcharts.dateFormat('%e %B %Y', nearestToStart));
        $('#endDate').text((endBalance < 0 ? 'Deficit at ' : 'Surplus at ') + Highcharts.dateFormat('%e %B %Y', nearestToEnd));
        $('#startDateBalance').html(fusion.util.formatCurrencyAbsolute(startBalance)).fit();
        $('#endDateBalance').html(fusion.util.formatCurrencyAbsolute(endBalance)).fit();
        $('#startDateDiv').removeClass('rougeredindicator').removeClass('darkolivegreenindicator')
            .addClass(startBalance < 0 ? 'rougeredindicator' : 'darkolivegreenindicator');
        $('#endDateDiv').removeClass('rougeredindicator').removeClass('darkolivegreenindicator')
            .addClass(endBalance < 0 ? 'rougeredindicator' : 'darkolivegreenindicator');
        var balanceChange = endBalance - startBalance;
        $('#balanceChange').html('Change ' + fusion.util.formatCurrencyAbsolute(balanceChange));

        this.startDate = nearestToStart;
        this.endDate = nearestToEnd;

        if (this.anchorDate !== this.startDate || this.refreshDate !== this.endDate) {
            basesRangeSelected[activeTrackerBasis] = true;
            valuationChart.showResetZoom();
        }
        else {
            basesRangeSelected[activeTrackerBasis] = false;
            valuationChart.zoomOut();
            $("tspan:contains('Reset window')").closest("g").hide();
        };

        var range = { start: this.startDate, end: this.endDate, setEvolutionDates: true };
        $.post('/Json/SetAnalysisPeriod', range, function (data) {
            that.analysisPeriodChanged.fire(range);
            $.post('/Evolution/AnalysisOfSurplus', { start: nearestToStart, end: nearestToEnd }, function (analysisOfSurplus) {
                $.Core.analysisOfSurplus.refresh(analysisOfSurplus);
            });
        });

    };

    this.renderRange = function (chart, start, end) {
        chart.xAxis[0].removePlotBand('left');
        chart.xAxis[0].removePlotBand('right');
        chart.xAxis[0].addPlotBand({
            color: '#FFEEEE',
            from: chart.series[0].data[0].x,
            to: start,
            id: 'left',
        });
        chart.xAxis[0].addPlotBand({
            color: '#FFEEEE',
            from: end,
            to: chart.series[0].data[chart.series[0].data.length - 1].x,
            id: 'right',
        });
    };

    this.refresh = function (data) {

        if (!initialised) {
            init(data);            
        }

        activeTrackerBasis = data.basis.type;
        
        initValuationChart();
        initComparisonChart();
        initBalanceChart();
        initFundingChart();

        var assets = [];
        var liabilities = [];
        var balances = [];
        var expecteds = [];
        var expectedFundingLevels = [];
        var fundingLevels = [];

        var makePoint = function (date, value, type) {
            var point = { x: date, y: value, i: {} };
            if (type === "asset" && $.inArray(date, data.assetImportDates) > -1) { point.i.a = true; point.marker = { enabled: true, symbol: "triangle-down", radius: 7 }; }
            if (type === "liabs" && $.inArray(date, data.liabilityImportDates) > -1) { point.i.l = true; point.marker = { enabled: true, symbol: "triangle-down", radius: 7 }; }
            return point;
        };

        for (var i = 0; i < data.tracker.length; i++) {
            assets.push(makePoint(data.tracker[i].date, data.tracker[i].asset, "asset"));
            liabilities.push(makePoint(data.tracker[i].date, data.tracker[i].liability, "liabs"));
        };

        for (var i = 0; i < data.tracker.length; i++) {
            balances.push([data.tracker[i].date, data.tracker[i].asset - data.tracker[i].liability]);            
            fundingLevels.push([data.fundingLevelTracker[i].date, data.fundingLevelTracker[i].value]);
            expectedFundingLevels.push([data.tracker[i].date, data.expectedFundingLevelTracker[i].value]);
        }

        if (data.expectedBalances.length > 0) {
            for (i = 0; i < data.tracker.length; i++) {
                expecteds.push([data.tracker[i].date, data.expectedBalances[i]]);                
            }
        }

        valuationChart.series[0].setData(assets);
        if (!data.showAssets) {
            valuationChart.series[0].hide();
        }
        valuationChart.series[1].setData(liabilities);
        balanceChart.series[0].setData(balances);
        balanceChart.series[1].setData(expecteds);
        fundingChart.series[0].setData(fundingLevels);
        fundingChart.series[1].setData(expectedFundingLevels);

        for (var i = 0; i < data.comparisonData.length; i++) {
            var points = [];
            for (var j = 0; j < data.comparisonData[i].points.length; j++) {
                points.push([data.comparisonData[i].points[j].date, data.comparisonData[i].points[j].value]);
            };
            comparisonChart.addSeries({
                turboThreshold: 0,
                name: data.comparisonData[i].basis
            }, false);
            comparisonChart.series[i].setData(points);
        };

        showExpectedDeficit = activeTrackerBasis == "TechnicalProvision";

        if (!showExpectedDeficit) {
            balanceChart.series[1].hide();
            $("#balanceChartLegend li.expectedLegend").css("visibility", "hidden");
            fundingChart.series[1].hide();
            $("#fundingChartLegend li.fundingLegendExpected").css("visibility", "hidden");
        }
        else {
            balanceChart.series[1].show();
            $("#balanceChartLegend li.expectedLegend").css("visibility", "visible");
            fundingChart.series[1].show();
            $("#fundingChartLegend li.fundingLegendExpected").css("visibility", "visible");
        }

        this.anchorDate = data.tracker[0].date;
        this.refreshDate = data.tracker[data.tracker.length - 1].date;

        this.startDate = data.startDate == null ? this.anchorDate : data.startDate;
        this.endDate = data.endDate == null ? this.refreshDate : data.endDate;

        this.selectRange(this.startDate, this.endDate);

        valuationChart.redraw();
        comparisonChart.redraw();
        balanceChart.redraw();
        fundingChart.redraw();
    };
};
