﻿$(document).ready(function () {
    var clicked = [];
    $(".submit-limit").click(function (e) {

        $('.submit-limit').addClass('disabled-button');

        var spinnerDiv = document.getElementById('spinnerDiv');
        if (spinnerDiv != null)
            showSpinner(spinnerDiv);

        if ($.inArray($(this)[0].id, clicked) != -1) {
            e.preventDefault();
        }
        else {
            clicked.push($(this)[0].id);
        }
    });
});

var showSpinner = function (target) {
    var opts = {
        lines: 13, // The number of lines to draw
        length: 6, // The length of each line
        width: 2, // The line thickness
        radius: 7, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 15, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#0f7bb9', // #rgb or #rrggbb or array of colors
        speed: 0.8, // Rounds per second
        trail: 78, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: true, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '18px', // Top position relative to parent
        left: '18px' // Left position relative to parent
    };
    var spinner = new Spinner(opts).spin(target);
};