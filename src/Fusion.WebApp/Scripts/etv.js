﻿fusion.analysis.EtvPanel = function () {

    var that = this;
    var initialised = false;

    this.changed = $.Callbacks();

    var reset = function () {

        $.getJSON('/Json/ResetEtv', function (etvData) {
            that.refresh(etvData);
            that.changed.fire({ id: that.id, data: etvData });
        });
    };
    
    var setEtv = function() {
        var etv = {},
            finalEtv,
            finalEtc,
            finalTur;

        finalEtv = $('#etvEquivalentTransferValue').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalEtv == null || finalEtv == typeof 'undefined')
            finalEtv = $('#etvEquivalentTransferValue').slider("option", "value");
        etv.equivalentTransferValue = finalEtv;

        finalEtc = $('#etvEnhancementToCetv').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalEtc == null || finalEtc == typeof 'undefined')
            finalEtc = $('#etvEnhancementToCetv').slider("option", "value");
        etv.enhancementToCetv = finalEtc;

        finalTur = $('#etvTakeUpRate').parent().parent().find('.slider-text-value').data('finalValue');
        if (finalTur == null || finalTur == typeof 'undefined')
            finalTur = $('#etvTakeUpRate').slider("option", "value");
        etv.takeUpRate = finalTur;

        var json = {
            etvData: JSON.stringify(etv)
        };

        $.post('/Json/SetEtv', json, function (etvData) {
            that.refresh(etvData);
            that.changed.fire({ id: that.id, data: etvData });
        });
    };
    
    var initialise = function (etvData) {

        var updateCell = function (event, ui) {
            $(this).slider("updateSliderValue", {
                el: $(this),
                value: ui.value
            });
        };

        var updateEtv = function (event, ui) {
            setEtv();
        };

        $("#etvEquivalentTransferValue").data("original", etvData.setup.ETVMinTransferValue);
        $("#etvEquivalentTransferValue").data("original-tooltip", fusion.util.formatCurrency(etvData.setup.ETVMinTransferValue));
        $("#etvEnhancementToCetv").data("original", etvData.setup.EnhancementToCetv);
        $("#etvTakeUpRate").data("original", etvData.setup.TakeUpRate);

        $("#etvEquivalentTransferValue").slider({
            animate: 400,
            range: 'min',
            min: etvData.setup.ETVMinTransferValue,
            max: etvData.setup.ETVMaxTransferValue,
            value: etvData.EquivalentTransferValue,
            step: (etvData.setup.ETVMaxTransferValue - etvData.setup.ETVMinTransferValue) / 100,
            slide: updateCell,
            //change: updateCell,
            stop: updateEtv,
            create: function () { $(this).slider('setTextValueElement') },
            elementSelector: $("#etvEquivalentTransferValue"),
            textElement: $("#etvEquivalentTransferValue").parent().siblings().last()
        });

        $('#etvEnhancementToCetv, #etvTakeUpRate')
            .each(function () {
                $(this).slider({
                    animate: 400,
                    range: 'min',
                    min: $(this).data('min'),
                    max: $(this).data('max'),
                    value: 0,
                    step: $(this).data('increment'),
                    slide: updateCell,
                    //change: updateCell,
                    stop: updateEtv,
                    create: function () { $(this).slider('setTextValueElement') },
                    elementSelector: $(this),
                    textElement: $(this).parent().siblings().last()
                });
            });

        $('#resetEtv').click(function () {
            reset();
        });

        initialised = true;
    };
    
    this.refresh = function (etvData) {

        if (!initialised) { initialise(etvData); }

        $('#etvDeferredLiabilities').html(fusion.util.formatCurrency(etvData.DeferredLiabilities));

        $('#etvEquivalentTransferValue').slider('value', etvData.EquivalentTransferValue);
        $('#etvEquivalentTransferValue').slider("setTextValueElement", etvData.EquivalentTransferValue);

        $('#etvCetvSize').html((etvData.CetvSize * 100).toFixed(2) + '%');

        $('#etvEnhancementToCetv').slider('value', etvData.EnhancementToCetv);
        $('#etvEnhancementToCetv').slider("setTextValueElement", etvData.EnhancementToCetv);

        $('#etvEquivalentEtv').html(fusion.util.formatCurrency(etvData.EquivalentEtv));

        $('#etvTakeUpRate').slider('value', etvData.TakeUpRate);
        $('#etvTakeUpRate').slider("setTextValueElement", etvData.TakeUpRate);

        $('#etvCetvPaid').html(fusion.util.formatCurrency(etvData.CetvPaid));
        $('#etvEtvPaid').html(fusion.util.formatCurrency(etvData.EtvPaid));
        $('#etvLiabilityDischarged').html(fusion.util.formatCurrency(etvData.LiabilityDischarged));

        $('#etvCostToCompany').html(fusion.util.formatCurrency(etvData.CostToCompany));
        $('#etvChangeInPosition').html(fusion.util.formatCurrency(etvData.ChangeInPosition));
    };
};
