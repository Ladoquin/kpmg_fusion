﻿$(document).scroll(function () {
    var distanceFromTop = $(document).scrollTop();

    if (distanceFromTop > 60) {
        $.Core.compactingHeader.contractHeader();
    } else {
        $.Core.compactingHeader.expandHeader();
    }
});

