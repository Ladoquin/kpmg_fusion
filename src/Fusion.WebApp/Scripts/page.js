﻿fusion.page = new (function () {

    this.init = function (args) {

        $('.firstmenu li').removeClass("active");
        $('.firstmenu li').eq(args.idx).addClass("active");
        $('.secondmenu').hide();
        $('.controlpanel').show();

        fusion.navigation.init();

        if (args.name != undefined) {
            $('#' + args.name + '_options').removeClass("hide").addClass("show");
            $('#' + args.name + '_dashboard').removeClass("hide").addClass("show");
        };

        fusion.util.headerSelectMenu.init();

        $(".currency").before($("#currency-symbol").val()).remove();
    };
})();
