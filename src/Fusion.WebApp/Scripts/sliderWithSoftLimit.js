﻿// Extends the jQuery UI Slider widget to display the soft limit tick marker
// and toggle the slider colour when the tick marker is passed.
// 
(function ($) {
    var extensionMethods = {
        setSoftLimit: function (args) {

            var set = function (el, currentVal, onThresholdExceededCallback, onThresholdDeceededCallback) {

                var line = $('div#' + el[0].id + ' div');
                var button = $('div#' + el[0].id + ' a');

                if (currentVal > $(el).data("softlimit")) {
                    line.addClass("ui-slider-range-softlimitexceeded");
                    button.css('background', 'url("../images/swipehandler-red.png") 0 0 no-repeat');
                    if (typeof onThresholdExceededCallback != "undefined")
                        onThresholdExceededCallback();
                }
                else {
                    line.removeClass("ui-slider-range-softlimitexceeded");
                    button.css('background', 'url("../images/swipehandler.png") 0 0 no-repeat');
                    if (typeof onThresholdDeceededCallback != "undefined")
                        onThresholdDeceededCallback();
                };
            };

            var el = this.options.elementSelector;

            var softlimit = args.value;
            var onThresholdExceededCallback = args.onThresholdExceeded;
            var onThresholdDeceededCallback = args.onThresholdDeceeded;

            if (typeof el.data("softlimit") == "undefined") {
                $(el).on("slide", function (event, ui) {
                    set(el, ui.value, onThresholdExceededCallback, onThresholdDeceededCallback);
                });
            };

            $(el).data("softlimit", softlimit);

            //can't just use this.value() because if you type in a value then this.value() is set to a slider increment rather than what the user typed
            //can't get the pattern used elsewhere working, may be because don't have specific id's? 
            var val = parseFloat(el.closest("tr").find("td.values span.slider-text-container span.slider-text-value").text()) / 100;
            set(el, val, onThresholdExceededCallback, onThresholdDeceededCallback);
        },
        setSoftLimitTick: function () {
            var el = this.options.elementSelector;
            var val = el.data("softlimit");            
            var min = parseFloat(this.options.min);
            var max = parseFloat(this.options.max);
            var tickAt = (val - min) / (max - min) * $(el).width();
            el.find(".ui-slider-softlimit-tick-mark").remove();
            if (val >= min && val <= max) {
                $("<span class='ui-slider-softlimit-tick-mark' title='Outperformance return limit: " + (val * 100).toFixed(2) + "%'></span>").css("left", tickAt + "px").appendTo(el);
                $(".ui-slider-softlimit-tick-mark", $(el)).tooltip({
                    tooltipClass: 'slider-softlimit-tooltip',
                });
            }
        }
    };

    $.extend(true, $['ui']['slider'].prototype, extensionMethods);


})(jQuery);