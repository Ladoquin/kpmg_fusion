﻿/*
*	KPMG
*	progresscycle.js
*	Space01 Ltd. 2013
* 	author : Jeremy Robson
*/

(function ($) {

    $.fn.expandingarea = function (options, args) {

        var collapse = function (el) {
            if ($(".expandingarea-body", el).is(":visible")) {
                $(".expandingarea-tab", el).addClass("expandingarea-tab-down");
                $(".expandingarea-tab", el).removeClass("expandingarea-tab-up");
                $(".expandingarea-body", el).slideUp("slow");
            }
        };

        var expand = function (el) {
            if (!$(".expandingarea-body", el).is(":visible")) {
                $(".expandingarea-tab", el).addClass("expandingarea-tab-up");
                $(".expandingarea-tab", el).removeClass("expandingarea-tab-down");
                $(".expandingarea-body", el).slideDown("slow");
            }
        };

        if (options == "options") {
            if (args == "state") {
                return $(".expandingarea-body", this).is(":visible") ? "expanded" : "collapsed";
            }
            if (args == "expand")
                $.each(this, function (idx, el) { expand(el); });
            if (args == "collapse")
                $.each(this, function (idx, el) { collapse(el); });
        }
        else {

            options = options || {};

            return $.each(this, function (idx, el) {

                var contents = $(el).contents();
                $(el).empty();

                var hide = options.initialmode != "show";

                $(el).append("<div class='expandingarea-wrapper'></div");
                $(".expandingarea-wrapper", el).append("<div class='expandingarea-tab expandingarea-tab-" + (hide ? "down" : "up") + "'>" + el.title + "</div>");
                $(".expandingarea-tab", el).append("<div class='expandingarea-body'></div>");
                $(".expandingarea-body", el).append(contents);

                if (hide) {
                    $(".expandingarea-body", el).hide();
                };

                $(".expandingarea-tab", el).click(function () {
                    if ($(".expandingarea-body", el).is(":visible")) {
                        collapse(el);
                    }
                    else {
                        expand(el);
                    }
                });

                if (options.onloadcomplete !== undefined) {
                    options.onloadcomplete();
                };
            });
            return this;
        }
    };

}(jQuery));