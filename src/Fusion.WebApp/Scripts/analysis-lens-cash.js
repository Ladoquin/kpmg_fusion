﻿fusion.analysis.cashLens = function () {

    var dataUrl = '/Json/CashLens';
    var initialised = false;
    var isStale = true;
    var isActiveLens = false;
    var fundingChart;
    var fundingChartBeforeData;
    var startingPosition = "";

    this.activate = function () {
        isActiveLens = true;
        if (isStale) {
            refresh();
        }
    };

    this.deactivate = function () {
        isActiveLens = false;
    };

    this.update = function () {
        if (isActiveLens) {
            refresh();
        } else {
            isStale = true;
        }
    };

    var init = function () {

        // Highchart rending problems means the recovery plan chart is going to 
        // be re-initialised upon update.

        // ************
        // * funding chart 
        // ************
        fundingChart = new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: 'fundingChartDiv',
                backgroundColor: 'none',
                plotBorderWidth: 1,
                plotBackgroundColor: 'white',

            },
            credits: false,
            exporting: {
                enabled: false
            },
            title: {
                text: 'Funding position',
                align: 'center',
                style: {
                    fontSize: '18px',
                    lineHeight: '18px',
                    fontWeight: 'bold',
                    fontFamily: 'Lato, sans-serif',
                    color: '#3e3e3e',
                },
                y: 6,
            },
            xAxis: {
                categories: ['Assets', 'Liabilities', 'Deficit'],
                title: {
                    useHTML: true,
                    text: 'Funding',
                    style: {
                        fontFamily: 'Lato, sans-serif',
                        fontSize: '14px',
                        color: '#f0f0f0'
                    },
                },
            },
            yAxis: [{
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                },
                stackLabels: {
                    useHTML: true,
                    enabled: true,
                    formatter: function () {
                        if (this.x == 0) {
                            if (this.total != null && this.total >= 0)
                                return fusion.util.formatCurrencyAbsolute(this.axis.series[0].yData[0] + this.axis.series[1].yData[0]);
                            else
                                return ''; //for cases of negative assets, don't want a stack label at the bottom
                        }
                        else if (this.x == 1) {
                            return fusion.util.formatCurrencyAbsolute(this.total);
                        }
                        if (this.x == 2) {
                            return fusion.util.formatCurrencyAbsolute(Math.max(this.points[5][0], this.points[5][1]) - Math.min(this.points[5][0], this.points[5][1]));
                        }
                    },
                },
                maxPadding: 0.08,
            }, {
                title: {
                    text: null
                },
                labels: {
                    useHTML: true,
                    formatter: fusion.util.formatChartCurrencyLabels,
                },
                linkedTo: 0,
                opposite: true,
                maxPadding: 0.08,
            }],
            legend: {
                enabled: false,
            },
            tooltip: {
                borderWidth: 0,
                borderRadius: 0,
                backgroundColor: '#007c92',
                shadow: false,
                useHTML: true,
                formatter: function () {
                    var header = this.series.name + "<br/><br/>";
                    var beforePostfix = "";
                    var afterPostfix = "";
                    if (this.series.name == "Deficit" || this.series.name == "Surplus") {
                        header = "";
                        beforePostfix = " " + startingPosition;
                        afterPostfix = " " + this.series.name;
                    }
                    var tt = header + "Before: ";
                    if (fundingChartBeforeData != null) {
                        tt += fusion.util.formatCurrency(fundingChartBeforeData[this.series.index]) + beforePostfix;
                        tt += "<br/>After: ";
                    }
                    tt += fusion.util.formatCurrency(this.y) + afterPostfix;
                    return tt;
                },
                style: {
                    color: '#ffffff',
                    fontSize: '1.1em',
                    padding: '20px',
                    opacity: 0.8,
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        useHTML: true,
                        enabled: false,
                        formatter: function () {
                            return fusion.util.formatCurrencyAbsolute(this.point.y);
                        },
                        backgroundColor: 'white',
                    },
                    pointPadding: 0.08,
                    groupPadding: 0.05,
                    borderWidth: 1,
                    borderColor: '#f0f0f0',
                }
            },
            series: [{
                name: 'Buy-in',
                data: [0, 0, 0],
                color: '#A8541A',
            }, {
                name: 'Assets',
                data: [0, 0, 0],
                color: '#E87424',
            }, {
                name: 'Deferreds',
                data: [0, 0, 0],
                color: '#4e144e'
            }, {
                name: 'Actives',
                data: [0, 0, 0],
                color: '#6e1d6e'
            }, {
                name: 'Pensioners',
                data: [0, 0, 0],
                color: '#8e258d'
            }, {
                name: 'Deficit',
                data: [0, 0, 0],
                color: '#9e3039'
            }, {
                name: 'Blank',
                data: [0, 0, 0],
                color: 'none',
                borderWidth: 0,
            }]
        });
        initialised = true;
    };

    var refresh = function () {
        if (!initialised) {
            init();
        }
        $.getJSON(dataUrl, function (data) {
            data.recoveryPlanData.isDirty = data.isDirty;
            data.fundingLevelData.basis = data.basis;
            refreshRecoveryPlanChart(data.recoveryPlanData);
            refreshFundingChart(data.fundingLevelData);
            isStale = false;
        });
    };

    var refreshRecoveryPlanChart = function (data) {

        if (data.before != null) { //non-funded schemes will not have recovery plan data

            var chartData = {
                series: [{
                    name: 'Current plan',
                    color: '#aabaa5',
                    data: data.before,
                },
                {
                    name: 'New plan',
                    color: '#e667af',
                    data: data.after,
                    visible: data.isDirty ? true : false
                }]
            };

            var numYears = Math.max(data.before.length, data.after.length);
            var years = [];
            for (i = 0; i < numYears; i++) {
                years.push(data.year + i);
            }

            chartData.years = years;

            chartData.xAxis = {
                title: 'Annual progression commencing ' + Highcharts.dateFormat('%e %B', data.commencementDate),
                fontSize: years.length > 18 ? '10px' : '11px',
                staggerLines: years.length > 18 ? 2 : 1
            };

            $("#recoveryPlanChartDiv").highcharts({
                chart: {
                    type: 'column',
                    backgroundColor: 'none',
                    plotBorderWidth: 1,
                    plotBackgroundColor: 'white'
                },
                credits: false,
                exporting: {
                    enabled: false
                },
                title: {
                    text: 'Recovery plan',
                    style: {
                        fontSize: '18px',
                        lineHeight: '18px',
                        fontWeight: 'bold',
                        fontFamily: 'Lato, sans-serif',
                        color: '#3e3e3e',
                    },
                    y: 6,
                },
                xAxis: {
                    title: {
                        text: chartData.xAxis.title,
                        style: {
                            fontFamily: 'Lato, sans-serif',
                            fontSize: '14px',
                        },
                    },
                    categories: chartData.years,
                    labels: {
                        style: { fontSize: chartData.xAxis.fontSize },
                        staggerLines: chartData.xAxis.staggerLines,
                    }
                },
                yAxis: [{
                    min: 0,
                    title: {
                        text: 'Contributions',
                        style: {
                            fontFamily: 'Lato, sans-serif',
                            fontSize: '14px',
                        },
                    },
                    labels: {
                        useHTML: true,
                        formatter: fusion.util.formatChartCurrencyLabels
                    },
                }, {
                    title: {
                        text: 'Contributions',
                        style: {
                            fontFamily: 'Lato, sans-serif',
                            fontSize: '14px',
                        },
                    },
                    labels: {
                        useHTML: true,
                        formatter: fusion.util.formatChartCurrencyLabels
                    },
                    linkedTo: 0,
                    opposite: true,
                }],

                plotOptions: {
                    column: {
                        pointPadding: 0,
                        borderWidth: 0,
                    }
                },
                tooltip: {
                    shared: true,
                    hideDelay: 0,
                    borderWidth: 0,
                    borderRadius: 0,
                    backgroundColor: '#007c92',
                    shadow: false,
                    useHTML: true,
                    followPointer: true,
                    formatter: function () {
                        var html = 'Year ' + this.x + '<br/>';
                        $.each(this.points, function (i, point) {
                            html += '<br/>' + point.series.name + ': ' + fusion.util.formatCurrencyAbsolute(this.y);
                        });
                        return html;
                    },
                    style: {
                        color: '#ffffff',
                        fontSize: '1.1em',
                        padding: '20px',
                        opacity: 0.8,
                    }
                },
                legend: {
                    layout: 'horizontal',
                    floating: true,
                    align: 'left',
                    verticalAlign: 'top',
                    borderWidth: 0,
                    borderRadius: 0,
                    padding: 10,
                    y: -10,

                },
                series: chartData.series
            });
        }
        else {
            $("#recoveryPlanChartDiv").hide();
            $("#recoveryPlanChartDivNAContainer").show();
        }
    };

    var refreshFundingChart = function (data) {

        if (data.before != null) {
            fundingChartBeforeData = {
                0: data.before.Buyin,
                1: data.before.Assets,
                2: data.before.Deferred,
                3: data.before.Active,
                4: data.before.Pension,
                5: Math.abs(data.before.Balance),
                6: Math.min(data.before.AssetsTotal, data.before.LiabilityTotal)
            };
            startingPosition = data.before.Balance < 0 ? "Deficit" : "Surplus";
        }
        else {
            fundingChartBeforeData = null;
        };

        var after = data.after;
        
        if (startingPosition === "") {
            startingPosition = after.Balance < 0 ? "Deficit" : "Surplus";
        };

        fundingChart.series[0].data[0].update(after.Buyin, false);
        fundingChart.series[1].data[0].update(after.Assets, false);
        fundingChart.series[2].data[1].update(after.Deferred, false);
        fundingChart.series[3].data[1].update(after.Active, false);
        fundingChart.series[4].data[1].update(after.Pension, false);
        fundingChart.series[5].data[2].update({
            y: Math.abs(after.Balance),
            color: after.Balance < 0 ? '#9e3039' : '#6a7f10',
        }, false);
        fundingChart.series[6].data[2].update(Math.min(after.AssetsTotal, after.LiabilityTotal), false);

        fundingChart.xAxis[0].categories[2] = after.Balance < 0 ? 'Deficit' : 'Surplus';
        fundingChart.series[5].name = after.Balance < 0 ? 'Deficit' : 'Surplus';
        fundingChart.redraw();
    };
};

