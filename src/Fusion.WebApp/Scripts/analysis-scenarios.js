﻿fusion.analysis.scenarios = function () {

    var nameSelector = "#scenario-new-name";
    var loadUrl = "Json/LoadAnalysisAssumptions";
    var saveUrl = "Json/SaveAnalysisAssumptions";
    var deleteUrl = "Json/DeleteAnalysisAssumptions";
    var existing;

    // might want to make this global to all dialogs
    // if so, would need to check that the eq(0) button is the one always to be clicked
    $(document).delegate('#scenario-save-dialog', 'keyup', function (e) {
        var tagName = e.target.tagName.toLowerCase();
        tagName = (tagName === 'input' && e.target.type === 'button') ? 'button' : tagName;
        if (e.which === $.ui.keyCode.ENTER && tagName !== 'textarea' && tagName !== 'select' && tagName !== 'button') {
            save();
        }
        e.preventDefault();
    });

    var saveClickHandler = function (e) {
        $("#scenario-save-dialog").dialog("open");
        e.preventDefault();
    };

    var loadClickHandler = function (e) {
        if ($(this).data("scenario") != null) {
            load($(this).data("scenario"));
        }
        else {
            // Need more information back from basis change than other pages
            // so override basis change call
            $.post("/Json/SetBasisAnalysis", { masterBasisId: $(this).data('id') }, function (data) {
                fusion.selectBasis(data.basis.masterBasisId, data);
            });
            $('#currentBasisMenu').fadeOut(200);
        }
        e.preventDefault();
    };

    var removeClickHandler = function (e) {
        var id = $(this).prev("a").data("scenario");
        remove(id);
        e.preventDefault();
    };

    var countClickHandler = function (e) {
        fusion.util.showBasisMenu();
        e.stopPropagation();
        e.preventDefault();
    };

    var load = function (id) {
        $.get(loadUrl, { id: id }, function (data) {
            fusion.selectBasis(data.basis.masterBasisId, data);
        });
    };

    var save = function () {
        var err = "";
        var name = $(nameSelector).val().trim();
        var masterBasisId = $("#currentBasis").data("masterbasisid");
        if (name.length == 0) {
            err = "Required";
        }
        else if (!$.isEmptyObject(existing)) {
            for (var i = 0; i < existing.length; i++) {
                if (existing[i].masterBasisId == masterBasisId) {
                    for (var j = 0; j < existing[i].scenarios.length; j++) {
                        if (existing[i].scenarios[j].name.toLowerCase() == name.toLowerCase()) {
                            err = "Already exists";
                            break;
                        }
                    }
                }
            }
        }
        if (err.length > 0) {
            $(".field-validation-error", $("#scenario-save-dialog"))
                .text(err)
                .show();
        }
        else {

            $.post(saveUrl, { key: name }, function (data) {

                addLi(masterBasisId, data.id, name);
                
                if ($.isEmptyObject(existing)) {
                    existing = [];
                };
                if (existing.filter(function (o) { return o.masterBasisId == masterBasisId; }).length === 0) {
                    existing.push({ masterBasisId: masterBasisId });
                };
                for (var i = 0; i < existing.length; i++) {
                    if (existing[i].masterBasisId == masterBasisId) {
                        if (existing[i].scenarios == null) {
                            existing[i].scenarios = [];
                        }
                        existing[i].scenarios.push({ name: name, id: data.id });
                    }
                }
                $("input[name=scenarios-existing]").val(JSON.stringify(existing));
                setCount();
            });

            close();
        }
    };

    var remove = function (id) {
        $.post(deleteUrl, { id: id });
        var li = $("ul").find("[data-scenario='" + id + "']").closest("li");
        li.remove();
        var spliceIdx = -1;
        for (var i = 0; i < existing.length; i++) {
            for (var j = 0; j < existing[i].scenarios.length; j++) {
                if (existing[i].scenarios[j].id == id) {
                    spliceIdx = j;
                    break;
                }
            }
            if (spliceIdx > -1) {
                existing[i].scenarios.splice(spliceIdx, 1);
                break;
            }
        };
        $("input[name=scenarios-existing]").val(JSON.stringify(existing));
        setCount();
    }

    var close = function () {
        $(".field-validation-error", $("#scenario-save-dialog")).hide();
        $(nameSelector).val("");
        $("#scenario-save-dialog").dialog("close");
    }

    var getBasisName = function (id) {
        var name;
        $.each($("#currentBasisMenu li:not('.scenario') a"), function (idx, val) {
            if ($(val).data("id") == id) {
                name = $(val).text();
            };
        });
        return name;
    };

    var addLi = function (masterBasisId, id, name) {
        var el = $("#cloneable-scenario-li > li").clone();
        $("a:first", el).attr("data-scenario", id).text(getBasisName(masterBasisId) + " - " + name); //TODO why doesn't .data work here?
        $("a:last", el).attr("data-id", id);
        $("#currentBasisMenu").append(el);
    };

    var setCount = function () {
        var total = 0;
        for (var i = 0; i < existing.length; i++) {
            total += existing[i].scenarios.length;
        };
        $("#scenario-save-count").text(total);
        $("#scenarios-flag").css("visibility", total > 0 ? "visible" : "hidden");
    };

    return {
        init: function () {

            $('#currentBasisMenu li a').off('click');
            $(document).on("click", "#currentBasisMenu li a", loadClickHandler);
            $("#scenario-save-btn").click(saveClickHandler);
            $(document).on("click", ".remove-scenario", removeClickHandler);
            $("#scenario-save-count").click(countClickHandler);

            $(document).ready(function () {
                // get existing scenarios from page
                existing = $("input[name=scenarios-existing]").val().length > 0 ? $.parseJSON($("input[name=scenarios-existing]").val()) : {};
                // build li items
                for (var i = 0; i < existing.length; i++) {
                    for (var j = 0; j < existing[i].scenarios.length; j++) {
                        addLi(existing[i].masterBasisId, existing[i].scenarios[j].id, existing[i].scenarios[j].name);
                    }
                };                
                setCount();
            });

            $("#scenario-save-dialog").dialog({
                modal: true,
                autoOpen: false,
                draggable: false,
                dialogClass: 'white-dialog',
                create: function (event, ui) {
                    var widget = $(this).dialog("widget");
                    $(".ui-dialog-titlebar-close .ui-button-text", widget).text("");
                    var buttons = $(".ui-dialog-buttonpane", widget).find("button");
                    $(buttons[1]).addClass("btn-dialog-cancel");
                },
                buttons: {
                    Save: save,
                    Cancel: function () {
                        close();
                    }
                }
            });
        },
        toggleVisibility: function (visible) {
            //TODO if no-one changes their mind about having this visible only when a change has been made then can get rid of this
            //$("#scenario-save-btn").css('visibility', visible ? 'visible' : 'hidden');
        }
    };
};