﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FlightDeck.DomainShared;

namespace Fusion.WebApp
{
    public static class ListHelper
    {
        public static MvcHtmlString ListToString(this HtmlHelper helper, IEnumerable<string> source, string delimiter = ", ")
        {
            return MvcHtmlString.Create(
                source.IsNullOrEmpty()
                    ? string.Empty
                    : source.Aggregate((x, y) => x + delimiter + y));
        }
    }
}