﻿namespace Fusion.WebApp
{
    using FlightDeck.DomainShared;
    using Fusion.WebApp.Models;
    using System.Text;
    using System.Web.Mvc;
    using System.Linq;

    public static class RoleHelper
    {
        public static MvcHtmlString RoleSelect(this HtmlHelper helper, AdditUserViewModel model)
        {
            var sb = new StringBuilder(250);

            if (!model.Role.HasValue)
            {   
                model.Role = RoleType.Client;
                model.RandomPassword = true;
            }

            // Functionality to hide password textbox and generate random password (if specified in web.config)
            // data-hidePassword - custom attribute added to dropdown, jQuery detects it to show/hide on client side
            // FUSN-630 Kam - 29/07/14
            string dataAttrRandomPassword = "";
            if(model.RandomPassword)
                dataAttrRandomPassword= " data-hidepassword='true'";

            sb.AppendFormat("<select name='Role'{0}>", dataAttrRandomPassword);

            foreach (var role in model.RolesAvailable.OrderBy(x => x.DisplayName()))
            {
                sb.AppendFormat("<option value='{0}'{1}>{2}</option>",
                   role.ToString(),
                   model.Role == role ? " selected" : "",
                   role.DisplayName());
            }
            sb.Append("</select>");

            return MvcHtmlString.Create(sb.ToString());
        }
    }
}