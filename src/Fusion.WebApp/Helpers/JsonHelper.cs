﻿namespace Fusion.WebApp
{
    using FlightDeck.ServiceInterfaces;
    using System;

    public static class JsonHelper
    {
        public static DateTime UnixEpoch = new DateTime(1970, 1, 1);

        public static double ConvertDateToJSON(DateTime date)
        {
            return (date - UnixEpoch).TotalMilliseconds;
        }

        public static object ConvertBasisToJSON(IBasis basis)
        {
            return new
            {
                masterBasisId = basis.MasterBasisId,
                type = basis.BasisType.ToString(),
                name = basis.Name,
                typeId = (int)basis.BasisType
            };
        }

        public static object ConvertBondYieldToJson(IBasis basis)
        {
            var bondYieldData = basis.GetBondYield();

            if (bondYieldData == null)
                return null;

            return new
            {
                index = bondYieldData.IndexName,
                label = bondYieldData.Label,
                value = bondYieldData.Value
            };
        }
    }
}
