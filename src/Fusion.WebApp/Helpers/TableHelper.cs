﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Fusion.WebApp
{
    public class TableHelper
    {
        private readonly int _cols;
        private readonly Dictionary<string, string> _attributes;

        public TableHelper(int cols, Dictionary<string, string> attributeDefinitions)
        {
            _cols = cols;
            _attributes = attributeDefinitions ?? new Dictionary<string, string>();
        }

        public MvcHtmlString WriteEmptyRow()
        {
            return MvcHtmlString.Create("<tr>" + new StringBuilder().Append("<td>", 0, _cols).ToString() + "</tr>");
        }
    }

    public static class TableHelperExtension
    {
        public static TableHelper Table(this HtmlHelper helper, int cols, Dictionary<string, string> attributeDefinitions = null)
        {
            return new TableHelper(cols, attributeDefinitions);
        }
    }
}