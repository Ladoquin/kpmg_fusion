﻿using FlightDeck.DomainShared;
using Fusion.WebApp.Services;
using System.Web.Mvc;

namespace Fusion.WebApp
{
    public static class UserHelper
    {
        public static UserProfile GetUser(this HtmlHelper helper)
        {
            return new SessionManagerService(helper.ViewContext.Controller).GetUserProfile();
        }
    }
}