﻿namespace Fusion.WebApp
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    public static class BasisHelper
    {
        public static MvcHtmlString BasisDropdown(this HtmlHelper helper, IPensionScheme scheme)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var sb = new StringBuilder(200);
            sb.Append("<ul id='currentBasisMenu' class='dropDownMenuX' style='font-size: 14pt; padding:10px 10px 10px 6px'>");
            foreach (var basis in scheme.GetBasesIdentifiers())
            {
                sb.AppendFormat("<li><a data-id='{0}' data-basis='{1}' href='{2}'>{3}</a></li>",
                    basis.Id.ToString(),
                    basis.Type.ToString(),
                    urlHelper.Action("Baseline", "Json"),
                    basis.Name);
            }
            sb.Append("</ul>");

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString TechnicalProvisionsBasisDropdown(this HtmlHelper helper, IPensionScheme scheme)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var sb = new StringBuilder(200);
            sb.Append("<ul id='technicalProvisionsBasisMenu' class='dropDownMenuX' style='font-size: 14pt; padding:10px 10px 10px 6px'>");

            var hasTpBasis = scheme.GetBasesIdentifiers().Where(b => b.Type == BasisType.TechnicalProvision).Any();

            foreach (var basis in scheme.GetBasesIdentifiers().Where(b => b.Type == BasisType.TechnicalProvision || b.Type == BasisType.Accounting))
            {
                sb.AppendFormat("<li><a data-id='{0}' data-basis='{1}' href='{2}'>{3}</a></li>",
                    basis.Id.ToString(),
                    basis.Type.ToString(),
                    urlHelper.Action("Baseline", "Json"),
                    basis.Name);
            }
            sb.Append("</ul>");

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString BasisSelectControl(this HtmlHelper helper, IPensionScheme scheme, string controlId, bool addSelectItem)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var sb = new StringBuilder(200);
            sb.AppendFormat("<select id='basis-{0}' class='jp-basis-control'>", controlId);

            if (addSelectItem)
                sb.AppendLine("<option value=''>(Select Basis)</option>");

            foreach (var basis in scheme.GetBasesIdentifiers())
                sb.AppendFormat("<option value='{0}'>{1}</option>", basis.Id.ToString(), basis.Name);

            sb.AppendLine("</select>");

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString BasisDropdownWithScenarios(this HtmlHelper helper, IPensionScheme scheme, IEnumerable<ClientAssumption> scenarios)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var sb = new StringBuilder(400);
            sb.Append("<ul id='currentBasisMenu' class='dropDownMenuX' style='font-size: 14pt; padding:10px 10px 10px 6px'>");
            foreach (var basis in scheme.GetBasesIdentifiers())
            {
                sb.AppendFormat("<li><a data-id='{0}' data-basistypeid='{1}' href='{2}'>{3}</a></li>",
                    basis.Id.ToString(),
                    ((int)basis.Type).ToString(),
                    urlHelper.Action("Baseline", "Json"),
                    basis.Name);
            }
            sb.Append("<li id='saved-scenario-hdr'>Saved scenarios</li>");
            sb.Append("</ul>");

            return MvcHtmlString.Create(sb.ToString());
        }
    }
}