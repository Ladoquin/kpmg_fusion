﻿namespace Fusion.WebApp
{
    using System.Web.Mvc;

    public static class FigureHelper
    {
        public static MvcHtmlString BalanceLabel(this HtmlHelper helper, double figure, bool capitalise = true)
        {
            return MvcHtmlString.Create(
                figure < 0
                ? (capitalise ? "D" : "d") + "eficit"
                : (capitalise ? "S" : "s") + "urplus");
        }
    }
}