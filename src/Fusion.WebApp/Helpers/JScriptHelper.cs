﻿using System.Web.Mvc;

namespace Fusion.WebApp
{
    public class JScriptHelper
    {
        public MvcHtmlString ToBool(bool val)
        {
            return MvcHtmlString.Create(val.ToString().ToLower());
        }
    }

    public static class JScriptHelperExtensions
    {
        public static JScriptHelper JScript(this HtmlHelper helper)
        {
            return new JScriptHelper();
        }
    }
}