﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Fusion.WebApp
{
    public static class VideoHelper
    {
        public class VideoView
        {
            private Dictionary<VideoType, string[]> _lookup = new Dictionary<VideoType, string[]>
                {
                    { VideoType.Solutions, new string[] {"9z2NQfWMO6d", "The pensions landscape", "01:27"} },
                    { VideoType.MemberOptions, new string[] {"9z2NQfWMO6d", "The pensions landscape", "01:27"} },
                    { VideoType.AssetBackedFunding, new string[] {"9z2NQfWMO6d", "The pensions landscape", "01:27"} }
                };

            private readonly VideoType _type;

            public VideoView(VideoType type)
            {
                _type = type;
            }

            public MvcHtmlString Graphic
            {
                get
                {
                    // https://kpmg.kulu.net/docs/player-parameters/
                    return MvcHtmlString.Create(
                        string.Format("<a class='fancybox fancybox.iframe video-play' href='//kpmg.kulu.net/view/{0}?embed=true&amp;t=sl&amp;autoplay=true&amp;autoscale=true'></a>",
                            _lookup[_type][0]));
                }
            }

            public MvcHtmlString Description { get { return MvcHtmlString.Create(_lookup[_type][1]); } }

            public MvcHtmlString Length { get { return MvcHtmlString.Create(_lookup[_type][2]); } }
        }

        public static VideoView Video(this HtmlHelper helper, VideoType video)
        {
            return new VideoView(video);
        }
    }
}