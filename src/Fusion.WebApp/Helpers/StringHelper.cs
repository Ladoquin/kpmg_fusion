﻿using System.Web.Mvc;

namespace Fusion.WebApp
{
    public static class StringHelper
    {
        public static MvcHtmlString DefaultLabel(this HtmlHelper helper, object label, string @default)
        {
            return MvcHtmlString.Create(
                label != null && !string.IsNullOrEmpty(label as string)
                    ? label as string
                    : @default);
        }

        public static bool Has(this HtmlHelper helper, string property)
        {
            return !string.IsNullOrEmpty(property);
        }

        public static MvcHtmlString WriteIfValueExists(this HtmlHelper helper, string value, string output)
        {
            return MvcHtmlString.Create(
                !string.IsNullOrEmpty(value)
                    ? output
                    : string.Empty);
        }

        public static MvcHtmlString FormatMailTo(this HtmlHelper helper, string to, string cc, string subject)
        {
            return new MvcHtmlString(string.Format("mailto:{0}?cc={1}&subject={2}", to, cc, subject));
        }
    }
}