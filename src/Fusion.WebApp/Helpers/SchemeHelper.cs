﻿namespace Fusion.WebApp
{
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.Services;
    using Fusion.WebApp.Services;
    using System.Web.Mvc;
    
    public static class SchemeHelper
    {
        public static IScheme GetScheme(this HtmlHelper helper)
        {
            return new SessionManagerService(helper.ViewContext.Controller).GetScheme()
                ?? new Scheme();
        }
    }
}