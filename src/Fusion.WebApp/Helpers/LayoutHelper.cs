﻿namespace Fusion.WebApp
{
    using FlightDeck.DomainShared;
    using Fusion.WebApp.Services;
    using System.Web.Mvc;
    
    public static class LayoutHelper
    {
        public static bool EnableJourneyPlan(this HtmlHelper helper)
        {
            var scheme = new SessionManagerService(helper.ViewContext.Controller).GetScheme();
            if (scheme != null)
                return scheme.PensionScheme.Funded;

            return false;
        }

        public static bool UserIsNotAdviser(this HtmlHelper helper)
        {
            var userProfile = new SessionManagerService(helper.ViewContext.Controller).GetUserProfile();

            if (userProfile != null)
                return userProfile.Role != RoleType.Adviser;

            return userProfile != null;
        }
    }
}