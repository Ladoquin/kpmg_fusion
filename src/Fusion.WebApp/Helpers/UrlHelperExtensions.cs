﻿namespace Fusion.WebApp
{
    using System.Web.Mvc;
    
    public static class UrlHelperExtensions
    {
        public static MvcHtmlString LinkableIf(this UrlHelper urlHelper, bool condition, string actionName, string controllerName, string text)
        {
            if (condition)
            {
                return new MvcHtmlString(string.Format("<a href='{0}'>{1}</a>", urlHelper.Action(actionName, controllerName), text));
            }
            else
            {
                return new MvcHtmlString(string.Format("<span class='disabled-link'>{0}</span>", text));
            }
        }
    }
}