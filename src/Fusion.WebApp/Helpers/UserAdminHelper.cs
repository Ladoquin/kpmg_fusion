﻿namespace Fusion.WebApp
{
    using FlightDeck.DomainShared;
using Fusion.WebApp.Models;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

    public class UserAdminHelper
    {
        private readonly HtmlHelper _helper;
        private readonly UrlHelper _urlHelper;

        public UserAdminHelper(HtmlHelper helper)
        {
            _helper = helper;
            _urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
        }

        public bool ShowSearchFilter(UserListParams parms)
        {
            return parms.RoleFilter.HasValue || !string.IsNullOrEmpty(parms.SchemeName);
        }

        public string GetFirstPageUrl(UserListParams parms)
        {
            var linkparams = (UserListParams)new UserListParams().InjectFrom(parms);
            linkparams.PageNumber = 1;
            return _urlHelper.Action("ListAll", linkparams);
        }

        public MvcHtmlString SchemeMembershipDisplay(UserListItem user)
        {
            var sb = new StringBuilder(100);

            switch (user.Role)
            {
                case RoleType.Admin:
                    sb.Append("[all]");
                    break;
                case RoleType.Installer:
                case RoleType.Consultant:
                    sb.Append("[all unrestricted]");
                    if (!user.Schemes.IsNullOrEmpty())
                    {
                        sb.Append(" + ");
                        sb.Append(buildSchemeList("[restricted]", user, forceMultipleDiv: true));
                    }
                    break;
                case RoleType.Adviser:
                case RoleType.Client:
                    sb.Append(buildSchemeList("[multiple]", user));
                    break;
                default:
                    break;
            }
           
            return MvcHtmlString.Create(sb.ToString());
        }

        private string buildSchemeList(string label, UserListItem user, bool forceMultipleDiv = false)
        {
            var sb = new StringBuilder(100);

            if (user.Schemes.Count() > 1 || forceMultipleDiv)
            {
                sb.AppendFormat("<a href='#' class='scheme-list-toggle scheme-list-toggle-collapsed'>{0}<span class='scheme-list-icon'></span></a>", label);
                sb.Append("<div class='scheme-list' style='display:none;'>");
            }
            foreach (var scheme in user.Schemes)
            {
                sb.Append("<div>");
                sb.Append("<a href='#' class='remove-scheme-user remove-scheme-smaller' title='Remove access to scheme'>");
                sb.Append("<img src='/Images/remove-icon-disabled.png' onmouseover=\"this.src='/Images/remove-icon.png'\" onmouseout='this.src=\"/Images/remove-icon-disabled.png\"'/>");
                sb.Append("</a>");
                sb.AppendFormat("<span style='display: none;'>[<a href='#' class='remove-scheme-ok' data-user='{0}' data-scheme='{1}' title='Click to confirm removal'>ok</a> | <a href='#' class='remove-scheme-cancel' title='Undo removal'>cancel</a>]&nbsp;</span>", user.Id.ToString(), scheme.SchemeDetailId.ToString());
                sb.Append(_helper.ActionLink(scheme.SchemeName, "Manage", "Scheme", new { id = scheme.SchemeDetailId }, null));
                sb.Append("</div>");
            }
            if (user.Schemes.Count() > 1)
            {
                sb.Append("</div>");
            }

            return sb.ToString();
        }

        public MvcHtmlString GetRoleFilterOptions(UserListParams parms)
        {
            var sb = new StringBuilder(250);

            var linkparams = (UserListParams)new UserListParams().InjectFrom(parms);
            linkparams.PageNumber = 1;
            linkparams.RoleFilter = null;

            sb.AppendFormat("<option value=\"{0}\" {1}>All</option>", _urlHelper.Action("ListAll", linkparams), parms.RoleFilter == null ? "selected=\"selected\"" : string.Empty);

            foreach (var role in Utils.EnumToArray<RoleType>().OrderBy(x => x.DisplayName()))
            {
                linkparams.RoleFilter = role;
                sb.AppendFormat("<option value=\"{0}\" {1}>{2}</option>", 
                    _urlHelper.Action("ListAll", linkparams), 
                    parms.RoleFilter != null && parms.RoleFilter.Value == role ? "selected=\"selected\"" : string.Empty,
                    role.DisplayName());
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public string GetNameOrderActionLink(UserListParams parms)
        {
            var linkparams = (UserListParams)new UserListParams().InjectFrom(parms);
            linkparams.PageNumber = 1;
            linkparams.NameDescending = !parms.NameDescending;

            return _urlHelper.Action("ListAll", linkparams);
        }

        public string GetSchemeFilterUrl(UserListParams parms)
        {
            var linkparams = (UserListParams)new UserListParams().InjectFrom(parms);
            linkparams.PageNumber = 1;
            linkparams.SchemeName = string.IsNullOrEmpty(linkparams.SchemeName) ? "x" : linkparams.SchemeName;

            return _urlHelper.Action("ListAll", linkparams);
        }

        public MvcHtmlString GetPagingLinks(UserListParams parms)
        {
            var sb = new StringBuilder(250);

            if (parms.TotalMatches > 0)
            {
                var currentPage = parms.PageNumber;

                var totalPages = (parms.TotalMatches / AppSettings.PageSize) + ((parms.TotalMatches % AppSettings.PageSize) > 0 ? 1 : 0);

                Func<int, string> getUrl = x => 
                    {
                        parms.PageNumber = x;
                        return _urlHelper.Action("ListAll", parms);
                    };

                if (currentPage > 1)
                {
                    sb.AppendFormat("<a href=\"{0}\" id=\"paging-previous\">Previous</a>", getUrl(currentPage - 1));
                }

                for (int i = 1; i <= totalPages; i++)
                {
                    if (i == currentPage)
                    {
                        sb.AppendFormat("<span>{0}</span>", i.ToString());
                    }
                    else
                    {
                        sb.AppendFormat("<a href=\"{0}\">{1}</a>", getUrl(i), i.ToString());
                    }
                }

                if (currentPage < totalPages)
                {
                    sb.AppendFormat("<a href=\"{0}\" id=\"paging-next\">Next</a>", getUrl(currentPage + 1));
                }

                parms.PageNumber = currentPage;
            }

            return MvcHtmlString.Create(sb.ToString());
        }
    }

    public static class UserAdminHelperExtension
    {
        public static UserAdminHelper GetUserAdminHelper(this HtmlHelper helper)
        {
            return new UserAdminHelper(helper);
        }
    }
}