﻿namespace Fusion.WebApp
{
    using FlightDeck.DomainShared;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    
    public static class UtilHelper
    {
        public static MvcHtmlString VisibleIf(this HtmlHelper helper, bool condition)
        {
            if (condition)
            {
                return MvcHtmlString.Empty;
            }
            else
            {
                return MvcHtmlString.Create("style='display: none'");
            }
        }

        public static MvcHtmlString DisableTimeout(this HtmlHelper helper)
        {
            return MvcHtmlString.Create("<input type='hidden' name='disable-timeout' id='disable-timeout' value='true' />");
        }

        public static MvcHtmlString SavedScenariosToJson(this HtmlHelper helper, IEnumerable<ClientAssumption> scenarios)
        {
            if (scenarios.IsNullOrEmpty())
                return MvcHtmlString.Empty;

            var json = scenarios
                .GroupBy(x => x.MasterBasisId)
                .Select(x => new
                {
                    masterBasisId = x.Key,
                    scenarios = x.Select(v => new { id = v.Id, name = v.Name })
                });

            return MvcHtmlString.Create(JsonConvert.SerializeObject(json, Formatting.None));
        }

        public static string ToUnixEpochDate(this HtmlHelper helper, DateTime d)
        {
            return (d - JsonHelper.UnixEpoch).TotalMilliseconds.ToString();
        }
    }
}