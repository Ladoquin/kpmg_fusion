﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fusion.WebApp.RequestObjects
{
    public class JourneyPlanRequest
    {
        public DateTime EffectiveDate { get; set; }
        public int TechnicalProvisionsBasisId { get; set; }
        public double CurrentReturn { get; set; }
        public double InitialReturn { get; set; }
        public double FinalReturn { get; set; }
        public double FundingLevel { get; set; }
        public int NumberOfSteps { get; set; }
        public double DiscountRate { get; set; }
        public int StartYear { get; set; }
        public double AnnualContributions { get; set; }
        public int Term { get; set; }
        public double AnnualIncrements { get; set; }
        public bool AddCurrentRecoveryPlan { get; set; }
        public bool AddExtraContributions { get; set; }
        public bool IncludeBuyIns { get; set; }
        public bool FundingLevelTrigger { get; set; }
        public int TriggerTimeStartYear { get; set; }
        public int TriggerTimeEndYear { get; set; }
        public bool ShowCurrentAssets { get; set; }
        public bool ShowNewAssets { get; set; }
        public bool ShowFunding { get; set; }
        public bool ShowSelfSufficiency { get; set; }
        public bool ShowBuyOut { get; set; }
        public int YearsToView { get; set; }
    }
}