﻿namespace Fusion.WebApp.Filters
{
    using System.Web.Mvc;
    
    public class AjaxAuthorize : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Session != null &&
                filterContext.RequestContext.HttpContext.Session.IsNewSession == false)
            {
                filterContext.Result = new ContentResult { Content = "TIMEOUT" };
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}