﻿namespace Fusion.WebApp.Filters
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Web.Mvc;
    
    public class HandleExceptionsAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            var ex = filterContext.Exception;

            var username = string.Empty;
            var schemeName = string.Empty;
            try
            {
                var session = filterContext.Controller.ControllerContext.HttpContext.Session;

                if (session["SchemeName"] != null)
                    schemeName = session["SchemeName"].ToString();

                if (session["UserProfile"] != null)
                    username = (session["UserProfile"] as UserProfile).UserName;

                var controllerName = filterContext.RouteData.Values["controller"];
                var actionName = filterContext.RouteData.Values["action"];
                if (controllerName.ToString().Equals("Indices", System.StringComparison.OrdinalIgnoreCase)
                     && actionName.ToString().Equals("Save", System.StringComparison.OrdinalIgnoreCase))
                {
                    // exception occurred on saving indices, so clear the application state flag
                    System.Web.HttpContext.Current.Application["IndicesUploadInProgress"] = "false";
                }
            }
            catch(Exception e)
            {
                log4net.LogManager.GetLogger(this.GetType()).ErrorFormat("Exception on handling an exception... {0}", e);
            }

            if (filterContext.Exception is HttpAntiForgeryException)
            {
                // log as warning
                log4net.LogManager.GetLogger("account-login").WarnFormat("User login failed. AntiForgeryToken error. Exception: {0}", ex.ToString());
            }
            else
            {
                // log as error
                log4net.LogManager.GetLogger(this.GetType()).Error(
                    string.Format("Unhandled exception, scheme: '{0}', username: '{1}', ex: {2}",
                        schemeName, username, ex.ToString()));
            }
        }
    }
}