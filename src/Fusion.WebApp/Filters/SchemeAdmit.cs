﻿namespace Fusion.WebApp.Filters
{
    using Fusion.WebApp.Services;
    using System.Web.Mvc;
    using System.Web.Routing;
    
    public class SchemeAdmit : UserAdmit
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var scheme = new SessionManagerService(filterContext.Controller).GetScheme();

            if (!redirecting)
            {

                if (scheme == null)
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary { { "Controller", "Home" }, { "Action", "Index" } });
                    base.OnActionExecuting(filterContext);
                }
            }

            var appSettings = new FlightDeck.Services.ApplicationSettings();

            if (filterContext.ActionDescriptor.ActionName.ToLower() != "manage"
                && scheme != null
                && !appSettings.IsSchemeSupported(scheme.DataCaptureVersion))
            {
                redirecting = true;
                filterContext.Result = new RedirectToRouteResult("UnsupportedScheme", null);
                base.OnActionExecuting(filterContext);
            }
        }
    }
}