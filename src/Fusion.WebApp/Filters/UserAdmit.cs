﻿namespace Fusion.WebApp.Filters
{
    using Fusion.WebApp.Services;
    using System.Web.Mvc;
    using System.Web.Routing;
    using WebMatrix.WebData;
    
    public class UserAdmit : ActionFilterAttribute
    {
        protected bool redirecting = false;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var profile = new SessionManagerService(filterContext.Controller).GetUserProfile();

            var action = string.Empty;
            var controller = string.Empty;

            if (profile == null)
            {
                if (WebSecurity.IsAuthenticated)
                {
                    WebSecurity.Logout();
                }
                controller = "Account";
                action = "Login";
            }
            else if (profile.PasswordMustChange)
            {
                controller = "Account";
                action = "ResetPassword";
            }
            else if (!profile.TermsAccepted)
            {
                controller = "Terms";
                action = "Index";
            }
            
            if (!string.IsNullOrEmpty(controller))
            {
                redirecting = true;
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary { { "Controller", controller }, { "Action", action } });
                base.OnActionExecuting(filterContext);
            }

            bool indicesUploadInProgress = false;
            
            if (System.Web.HttpContext.Current.Application["IndicesUploadInProgress"] != null)
                bool.TryParse(System.Web.HttpContext.Current.Application["IndicesUploadInProgress"].ToString(), out indicesUploadInProgress);

            if (indicesUploadInProgress) // check if indices upload is in progress
            {
                redirecting = true;
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary { { "Controller", "Error" }, { "Action", "Maintenance" } });
                base.OnActionExecuting(filterContext);
            }

        }
    }
}