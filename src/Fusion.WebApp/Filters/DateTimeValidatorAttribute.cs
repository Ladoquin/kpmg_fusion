﻿namespace Fusion.WebApp.Filters
{
    using Fusion.WebApp.Controllers;
    using System;
    using System.Web.Mvc;

    public class DateTimeValidatorAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var start = !filterContext.ActionParameters.ContainsKey("start") || filterContext.ActionParameters["start"] == null ? string.Empty : filterContext.ActionParameters["start"].ToString();
            var end = !filterContext.ActionParameters.ContainsKey("end") || filterContext.ActionParameters["end"] == null ? string.Empty : filterContext.ActionParameters["end"].ToString();

            double d; // no comment :(            
            Func<string, bool> dateIsParsable = date =>
                date != "NaN" && !string.IsNullOrEmpty(date) && double.TryParse(date, out d);
            Func<double, bool> dateIsValid = date =>
                date >= 0 &&
                date <= new TimeSpan(DateTime.MaxValue.Ticks).TotalMilliseconds - new TimeSpan(JsonHelper.UnixEpoch.Ticks).TotalMilliseconds;

            var startOk = string.IsNullOrEmpty(start) || (dateIsParsable(start) && dateIsValid(double.Parse(start)));
            var endOk = string.IsNullOrEmpty(end) || (dateIsParsable(end) && dateIsValid(double.Parse(end)));

            if (!startOk || !endOk)
            {
                filterContext.Controller.ViewData.ModelState.AddModelError(!startOk ? "start" : "end", "Not a valid date");
            }
        }
    }
}