﻿namespace FlightDeck.Services
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Security.Cryptography;
    
    public class UserPasswordEncrypter : IUserPasswordEncrypter
    {
        public PasswordData GetEncrypted(string password)
        {
            var salt = new byte[1];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(salt);
            }

            return GetEncrypted(password, salt);
        }

        public PasswordData GetEncrypted(string password, byte[] salt)
        {
            var pwd = GetBytes(password);

            var h = new byte[pwd.Length + salt.Length];
            Buffer.BlockCopy(pwd, 0, h, 0, pwd.Length);
            Buffer.BlockCopy(salt, 0, h, pwd.Length, salt.Length);

            var encrypted = new SHA1CryptoServiceProvider().ComputeHash(h);

            return new PasswordData(encrypted, salt);
        }

        private byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
