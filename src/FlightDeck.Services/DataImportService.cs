using FlightDeck.DataAccess;
using FlightDeck.Domain;
using FlightDeck.Domain.Assets;
using FlightDeck.Domain.LDI;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FlightDeck.Services
{
    public class DataImportService
    {
        private const int DefaultId = -1;
        private const string Yes = "Yes";

        private readonly HashSet<string> assumptionsToIgnore = new HashSet<string>
            {
                "Mortality", "LifeExp65Label", "LifeExp65_45Label"//, "ExpenseLoading", "PropMarried", "SacrificeProp"
            };

        private IApplicationSettings _applicationSettings;
        public IApplicationSettings ApplicationSettings
        {
            get
            {
                if (_applicationSettings == null)
                    _applicationSettings = new ApplicationSettings();
                return _applicationSettings;
            }
            set { _applicationSettings = value; }
        }

        private ICustomIndexService _customIndexService;
        public ICustomIndexService CustomIndexService
        {
            get
            {
                if (_customIndexService == null)
                    _customIndexService = new CustomIndexService();
                return _customIndexService;
            }
            set { _customIndexService = value; }
        }

        private ICachedResultsService _cachedResultsService;
        public ICachedResultsService CachedResultsService
        {
            get
            {
                if (_cachedResultsService == null)
                    _cachedResultsService = new CachedResultsService();
                return _cachedResultsService;
            }
            set { _cachedResultsService = value; }
        }

        private readonly IDbInteractionScopeFactory _dbScopeFactory;
        private readonly Lazy<int> versionNumber;

        private double PensionIncreasesMaxAllowedValue;
        private ProgressTracker<string, int> _importProgress;

        private struct AccountingParameters
        {
            public bool IAS19Visible { get; set; }
            public bool FRS17Visible { get; set; }
            public bool USGAAPVisible { get; set; }
            public AccountingStandardType Default { get; set; }
            public SalaryType USGAAPSalaryOption { get; set; }
        }

        public DataImportService()
            : this(new DbInteractionScopeFactory())
        {
        }

        public DataImportService(IDbInteractionScopeFactory dbScopeFactory)
        {
            _dbScopeFactory = dbScopeFactory;

            versionNumber = new Lazy<int>(() =>
            {
                using (var scope = _dbScopeFactory.GetScope())
                {
                    return scope.GetVersionNumber();
                }
            });
        }

        private static IDictionary<string, AssetClass> CreateAssetClasses(IAssetClassRepository repository)
        {
            var result = new Dictionary<string, AssetClass> //Todo enum
            {
                {"equity", repository.GetById(10)},
                {"abf", repository.GetById(9)},
                {"gilts", repository.GetById(3)},
                {"non gilts", repository.GetById(11)},
                {"property", repository.GetById(4)},
                {"hedge funds", repository.GetById(5)},
                {"cash", repository.GetById(8)},
                {"fixed interest gilts", repository.GetById(12)},
                {"corporate bonds", repository.GetById(6)},
                {"diversified growth", repository.GetById(13)},
                {"diversified credit", repository.GetById(14)},
                {"private markets", repository.GetById(15)},
                {"index linked gilts", repository.GetById(7)}
            };

            return result;
        }

        private DateTime GetAnchorDate(XElement el)
        {
            return getDate(el, "EffectiveDate");
        }

        private BasisType GetBasisType(XElement element)
        {
            var basis = getString(element, "Basis").ToLower();
            switch (basis)
            {
                case "accounting" :
                    return BasisType.Accounting;
                case "buyout" :
                    return BasisType.Buyout;
                case "technical provisions" :
                    return BasisType.TechnicalProvision;
                case "gilts":
                    return BasisType.Gilts;
                case "neutral":
                    return BasisType.Neutral;
                default:
                    throw new ApplicationException(string.Format("Unknown basis {0}", basis));
            }
        }

        private string GetBasisDisplayName(XElement element)
        {
            return getString(element, "BasisName", false);
        }

        private AssumptionType ParseAssumptionType(string name)
        {
            switch (name)
            {
                case "DiscountPreRet":
                    return AssumptionType.PreRetirementDiscountRate;
                case "DiscountPostRet":
                    return AssumptionType.PostRetirementDiscountRate;
                case "DiscountPen" :
                    return AssumptionType.PensionDiscountRate;
                case "SalaryInc" :
                    return AssumptionType.SalaryIncrease;
                case "RPI" :
                    return AssumptionType.InflationRetailPriceIndex;
                case "CPI" :
                    return AssumptionType.InflationConsumerPriceIndex;
                default :
                    throw new ApplicationException(string.Format("Unknown assumption type {0}", name));
            }
        }

        private Dictionary<int, dynamic> GetPensionIncreaseDefinitions(XElement el)
        {
            var result = new Dictionary<int, dynamic>();
            foreach (var d in getChildElements(el, "PensionIncreases"))
            {
                var min = getDouble(d, "Min", false);
                var max = getDouble(d, "Max", false);
                var fixedIncrease = .0;
                var isFixed = false;
                if (double.TryParse(getString(d, "Fixed", false), out fixedIncrease))
                {
                    isFixed = true;
                }
                var inflationType = ParseInfllationType(getString(d, "InflationIndex", false));
                var def = new
                {
                    Label = getString(d, "Label", false),
                    Minimum = min,
                    Maximum = max,
                    FixedIncrease = fixedIncrease,
                    InflationType = inflationType,
                    IsFixed = isFixed
                };
                result.Add(getIntAttr(d, "Number"), def);
            }
            return result;
        }

        private InflationType ParseInfllationType(string s)
        {
            if (string.IsNullOrEmpty(s))
                return InflationType.None;
            if (s.ToLower().Equals("rpi"))
                return InflationType.Rpi;
            if (s.ToLower().Equals("cpi"))
                return InflationType.Cpi;
            return InflationType.None;
        }

        private void PopulateAssumptions(XElement liabs, ICollection<FinancialAssumption> assumptions,
            IDictionary<SimpleAssumptionType, SimpleAssumption> simpleAssumptions,
            ICollection<PensionIncreaseAssumption> pensionIncrease, IDictionary<int, dynamic> pensionIncreaseDefinitions,
            double inflationVolatility, IFinancialIndexRepository indexRepository)
        {
            foreach(var a in getChildElements(liabs, "Assumptions"))
            {
                var assumptionName = a.Name.LocalName;
                if (assumptionsToIgnore.Contains(assumptionName))
                    continue;
                if (a.Element("Type") != null)
                {
                    var initialValue = getDouble(a, "Assumption");
                    var indexName = getString(a, "UnderlyingIndex", false);

                    var index = string.IsNullOrEmpty(indexName) ? null : indexRepository.GetByName(indexName);
                    if (assumptionName == "PenInc")
                    {
                        var reference = getIntAttr(a, "Num");
                        if (initialValue > PensionIncreasesMaxAllowedValue)
                            throw new Exception(string.Format("{0} PenInc '{1}' can not be greater than {2}", getLocation(a), reference, PensionIncreasesMaxAllowedValue.ToString()));
                        var def = pensionIncreaseDefinitions[reference];
                        var c = ParseAssumptionCategory(getString(a, "Type"));

                        var visibleEl = getElement(a, "Visible", false);
                        var isVisible = visibleEl == null || string.IsNullOrEmpty(visibleEl.Value) || getBool(a, "Visible", false);

                        pensionIncrease.Add(new PensionIncreaseAssumption(DefaultId, DefaultId, initialValue,
                            index, reference, def.Label, def.Minimum, def.Maximum, def.FixedIncrease,
                            c, inflationVolatility, def.InflationType, def.IsFixed, isVisible));
                    }
                    else
                    {
                        var assumptionType = ParseAssumptionType(assumptionName);

                        var isVisibleVal = getString(a, "Visible", false);
                        var isVisible = string.IsNullOrEmpty(isVisibleVal) || isVisibleVal.ToLower() != "no";

                        string label = getString(a, "Label", false);
                        label = string.IsNullOrWhiteSpace(label) ? null : label;

                        assumptions.Add(new FinancialAssumption(DefaultId, DefaultId, initialValue, index,
                            assumptionType, label, isVisible));
                    }
                }
                else
                {
                    var assumptionType = ParseSimpleAssumtionType(a);
                    var isVisible = assumptionType == SimpleAssumptionType.LifeExpectancy65 ||
                                    assumptionType == SimpleAssumptionType.LifeExpectancy65_45 ||
                                    assumptionType == SimpleAssumptionType.SacrificeProportion ||
                                    assumptionType == SimpleAssumptionType.CommutationAllowance;
                    string label = null;
                    if (isVisible &&
                        new List<SimpleAssumptionType> { SimpleAssumptionType.SacrificeProportion, SimpleAssumptionType.CommutationAllowance }
                            .Contains(assumptionType) == false)
                    {
                        label =
                            getString(liabs.Element("Assumptions"),
                                 assumptionType == SimpleAssumptionType.LifeExpectancy65
                                    ? "LifeExp65Label"
                                    : "LifeExp65_45Label",
                                 false);
                        label = string.IsNullOrWhiteSpace(label) ? null : label;
                    }
                    else if (isVisible && assumptionType == SimpleAssumptionType.CommutationAllowance)
                    {
                        label = getString(liabs.Element("Assumptions"), "CommutationAllowance", false);
                    }
                    double assumptionValue;
                    if (!double.TryParse(a.Value, out assumptionValue))
                        assumptionValue = 0;
                    simpleAssumptions.Add(assumptionType, new SimpleAssumption(assumptionType, assumptionValue, label, isVisible));
                }
            }
        }

        private SimpleAssumptionType ParseSimpleAssumtionType(XElement assumptionElement)
        {
            var assumtionName = assumptionElement.Name.LocalName;
            switch (assumtionName)
            {
                case "ExpenseLoading" :
                    return SimpleAssumptionType.ExpenseLoading;
                case "PropMarried":
                    return getStringAttr(assumptionElement, "Sex") == "M" ?
                        SimpleAssumptionType.MaleMarriedProportion :
                        SimpleAssumptionType.FemaleMarriedProportion;
                case "SacrificeProp":
                    return SimpleAssumptionType.SacrificeProportion;
                case "LifeExp65":
                    return SimpleAssumptionType.LifeExpectancy65;
                case "LifeExp65_45":
                    return SimpleAssumptionType.LifeExpectancy65_45;
                case "CommutationAllowance":
                    return SimpleAssumptionType.CommutationAllowance;
                default:
                    throw new ApplicationException(string.Format("Unknown assumption type {0}", assumtionName));
            }
        }

        private AssumptionCategory ParseAssumptionCategory(string name)
        {
            switch (name)
            {
                case "Black-Scholes" :
                    return AssumptionCategory.BlackScholes;
                case "Fixed" :
                    return AssumptionCategory.Fixed;
                case "Curved" :
                    return AssumptionCategory.Curved;
                default :
                    throw new ApplicationException("Unknown assumption category");
            }
        }

        private MemberStatus ParseMemberStatus(string name)
        {
            switch (name)
            {
                case "act":
                    return MemberStatus.ActivePast;
                case "def":
                case "pup":
                    return MemberStatus.Deferred;
                case "pen":
                    return MemberStatus.Pensioner;
                case "fut":
                    return MemberStatus.ActiveFuture;
                default:
                    throw new ApplicationException(string.Format("Unknown member status {0}.", name));
            }
        }

        private IEnumerable<CashflowData> GetCashFlows(XElement liabs, IDictionary<int, PensionIncreaseAssumption> assumptions)
        {
            var result = new List<CashflowData>();
            foreach (var d in getChildElements(liabs, "LiabCashflows"))
            {
                var cashflows = new SortedList<int, double>();
                foreach (var v in d.Descendants("Value"))
                {
                    var value = getDouble(v);
                    if (Math.Abs(value) > 0.0000000001)
                        cashflows.Add(getIntAttr(v, "Year"), value);
                }
                var cf = FillGapYears(cashflows);
                var status = ParseMemberStatus(getStringAttr(d, "Status").ToLower());
                var age = getIntAttr(d, "MinAge");
                var maxAge = getIntAttr(d, "MaxAge");
                var ss = getStringAttr(d, "Revaluation", false);
                var revaluation = ParseRevaluationType(ss);
                var retirementAge = getIntAttr(d, "RetAge");
                var isBuyin = !string.IsNullOrWhiteSpace(getStringAttr(d, "BuyIn", false));//.ToLower().Equals("true", StringComparison.InvariantCultureIgnoreCase);
                if(isBuyin && status != MemberStatus.Pensioner)
                    throw new ApplicationException("Only Pensioner buy-in cash-flows supported, but was " + status);

                if (getStringAttr(d, "BenefitType").ToLower() == "lump sum")
                    result.Add(new CashflowData(DefaultId, DefaultId, status, age, maxAge, revaluation, retirementAge, BenefitType.LumpSum, cf, 0, 0, isBuyin, null));
                else
                {
                    var pensionIncreaseId = getIntAttr(d, "PenInc");
                    if (!assumptions.ContainsKey(pensionIncreaseId))
                        throw new ApplicationException(string.Format("Undefined pension increase assumption id {0}", pensionIncreaseId));
                    result.Add(new CashflowData(DefaultId, DefaultId, status, age, maxAge, revaluation, retirementAge, BenefitType.Pension, cf, 0, 0, isBuyin, pensionIncreaseId));
                }
            }
            return result;
        }

        private Dictionary<int, double> FillGapYears(SortedList<int, double> cf)
        {
            var result = new Dictionary<int, double>();
            if (cf == null || cf.Count == 0)
                return result;
            var i = 0;

            foreach (var c in cf)
            {
                result.Add(c.Key, c.Value);
                if (i < cf.Count - 1) // && indexValues.Keys[i + 1] > indexValue.Key.AddDays(1))
                {
                    var diff = cf.Keys[i + 1] - c.Key;
                    for (var k = 1; k < diff; k++)
                        result.Add(c.Key + k, 0);
                }
                i++;
            }
            return result;
        }
        private static RevaluationType ParseRevaluationType(string value)
        {
            if(String.IsNullOrEmpty(value))
                return RevaluationType.None;
            if(value.ToLower().StartsWith("c"))
                return RevaluationType.Cpi;
            if(value.ToLower().StartsWith("r"))
                return RevaluationType.Rpi;
            return RevaluationType.Fixed;
        }

        public ProgressTracker<string, int> ImportScheme(string schemeName, string userName, XDocument doc, IFinancialIndexRepository indexRepository, IAssetClassRepository assetClassRepository)
        {
            var logger = LogManager.GetLogger("scheme-import");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            logger.InfoFormat("Importing scheme: {0}", schemeName);

            _importProgress = new ProgressTracker<string, int>();

            try
            {
                CachedResultsService.ClearAllForScheme(schemeName);

                var root = doc.Root;

                PensionIncreasesMaxAllowedValue = ApplicationSettings.GetPensionIncreasesMaxValue();
                var pensionIncreaseDefinitions = GetPensionIncreaseDefinitions(root);
                int id;

                using (var tdb = new DbTransactionalScope())
                {
                    var mainX = getElement(root, "Main");

                    var refGiltYield = getElement(mainX, "RefGiltYield", false);
                    var giltsIndex =
                        refGiltYield != null
                            ? indexRepository.GetByName(getString(refGiltYield, "IndexName"))
                            : indexRepository.GiltsIndex;

                    var bases = new List<Basis>();

                    var inflationVolatility = getDouble(mainX, "InflationVol");
                    var cashflowTotal = 0;

                    foreach (var l in root.Descendants("Liabs"))
                    {
                        var basisType = GetBasisType(l);
                        var basisDisplayName = GetBasisDisplayName(l);
                        var anchorDate = GetAnchorDate(l);
                        var lastPensionIncrease = getDate(l, "LastPenIncDate");
                        var linkedToCorpBonds = getBool(l, "LinkedToCorpBonds");

                        var assumptions = new List<FinancialAssumption>();
                        var pensionAssumptions = new List<PensionIncreaseAssumption>();
                        var simpleAssumptions = new Dictionary<SimpleAssumptionType, SimpleAssumption>();

                        PopulateAssumptions(l, assumptions, simpleAssumptions, pensionAssumptions, pensionIncreaseDefinitions,
                            inflationVolatility, indexRepository);
                        var liabIncreases = GetLiabIncreases(l);
                        var bondYield = GetBondYield(l, indexRepository);
                        var cashFlows = GetCashFlows(l, pensionAssumptions.ToDictionary(x => x.ReferenceNumber));

                        cashflowTotal += cashFlows.Count();

                        var salaries = new List<SalaryProgression>();
                        foreach (var salaryProgression in getChildElements(l, "SalaryProgression"))
                        {
                            var salaryRoll = new Dictionary<int, double>();

                            foreach (var v in salaryProgression.Descendants("Value"))
                            {
                                var value = getDouble(v);
                                if (value != 0)
                                    salaryRoll.Add(getIntAttr(v, "Year"), value);
                            }

                            salaries.Add(new SalaryProgression(DefaultId, DefaultId,
                                getIntAttr(salaryProgression, "MinAge"),
                                getIntAttr(salaryProgression, "MaxAge"),
                                getIntAttr(salaryProgression, "RetAge", false),
                                salaryRoll));
                        }
                        var totalCosts = GetTotalCosts(l);

                        var aoci = getDouble(l, "USGAAP_AOCI", false);
                        var unrecognisedPriorService = getDouble(l, "USGAAP_UnrecognisedPriorService", false);
                        var amortisationPriorService = getDouble(l, "USGAAP_AmortisationOfPriorService", false);
                        var amortisationPeriod = getIntNullable(l, "USGAAP_AmortisationPeriodForActGain");

                        _importProgress.SetProgress("Saving basis '" + basisType.ToString() + "'");

                        bases.Add(new Basis(DefaultId, DefaultId, DefaultId, basisDisplayName, basisType, anchorDate, lastPensionIncrease, linkedToCorpBonds,
                            salaries, cashFlows,
                            assumptions.ToDictionary(x => x.Type, x => x.InitialValue),
                            pensionAssumptions.ToDictionary(x => x.ReferenceNumber, x => x.InitialValue),
                            liabIncreases,
                            bondYield,
                            aoci, unrecognisedPriorService, amortisationPriorService, amortisationPeriod.GetValueOrDefault(), simpleAssumptions, giltsIndex.Id,
                            assumptions.ToDictionary(x => x.Type),
                            pensionAssumptions.ToDictionary(x => x.ReferenceNumber),
                            giltsIndex)
                        {
                            TotalCosts = totalCosts //TODO still not sure what this is for
                        });
                    }

                    logger.InfoFormat("Total Cashflow count for {0}: {1}", schemeName, cashflowTotal);

                    var pensionScheme = GetPensionScheme(root, indexRepository, assetClassRepository, bases);

                    _importProgress.SetProgress("Saving pension scheme");
                    logger.InfoFormat("Saving pension scheme: {0}", schemeName);

                    var r = (IPensionSchemeRepository)tdb.GetRepository<PensionScheme>();
                    id = r.Insert(pensionScheme);

                    tdb.Commit();
                }
                _importProgress.SetComplete(id);
                
                stopwatch.Stop();
                logger.InfoFormat("Scheme Import complete: {0}. Import duration: {1} milliseconds.", schemeName, stopwatch.ElapsedMilliseconds.ToString());
            }
            catch (Exception ex)
            {
                _importProgress.SetException(ex);
                logger.ErrorFormat("Scheme import error: {0}. User: {1}. {2}", schemeName, userName, ex.ToString());
            }

            return _importProgress;
        }

        private IDictionary<int, double> GetLiabIncreases(XElement xElement)
        {
            var e = getElement(xElement, "MortalityAdjustments");
            return e.Elements("Adj").ToDictionary(element => getIntAttr(element, "Age"), getDouble);
        }

        private BondYield GetBondYield(XElement xElement, IFinancialIndexRepository indexRepository)
        {
            var element = getElement(xElement, "LAPanelBondYield");

            return new BondYield()
            {
                Index = indexRepository.GetByName(getStringAttr(element, "Index")),
                Label = getStringAttr(element, "Label")
            };
        }

        private TotalCosts GetTotalCosts(XElement liabs)
        {
            var costs = getElement(liabs, "ValuationResults");
            return new TotalCosts(-1,
                getDouble(costs, "ActLiab"),
                getDouble(costs, "DefLiab"),
                getDouble(costs, "PenLiab"),
                getDouble(costs, "ServiceCost"));
        }

        private PensionSchemeBase GetPensionScheme(XElement root, IFinancialIndexRepository indexRepository, IAssetClassRepository assetClassRepository,
            IEnumerable<Basis> bases)
        {
            ValidateVersion(root);

            var assets = GetAssets(root, indexRepository, assetClassRepository);

            var main = getElement(root, "Main");

            var defaultBasis = getString(main, "DefaultBasis");

            var contributions = getChildElements(root, "ContributionRates").Select(c =>
                new Contribution(DefaultId, getDate(c, "Date"), getDouble(c, "EmployerRate"), getDouble(c, "EmployeeRate"))).ToList();

            var recoveries = new List<RecoveryPayment>();
            foreach (var p in getChildElements(root, "Payments2"))
            {
                var amountElement = getString(p, "Amount", false);
                if (!string.IsNullOrEmpty(amountElement))
                    recoveries.Add(new RecoveryPayment(DefaultId, getDate(p, "StartDate"), getDate(p, "EndDate"),
                        double.Parse(amountElement), getInt(p, "Gap")));
            }
            var dataSource = GetSchemeDataSource(root);
            var name = getString(root, "SchemeName");
            var reference = getString(root, "SchemeRef");
            var namedProperties = GetNamedProperties(root);

            var contributionsX = getElement(root, "Contributions");
            var otherParameters = getElement(main, "OtherParameters");
            var contractingOutContributionRate = getDouble(otherParameters, "COutContRate", false);
            if (contractingOutContributionRate == 0)
                contractingOutContributionRate = 0.03;
            var rp = getElement(contributionsX, "RecoveryPlan");
            var returnOnAssets = getDouble(rp, "ReturnOnAssets");
            var isGrowthFixed = getString(rp, "OutperformanceType").ToLower().StartsWith("fixed rate");
            var ias = getDouble(root, "IAS19ExpenseLoading");
            var ldiCashflowBasis = getLDICashflowBasis(main, indexRepository);
            var syntheticAssetIndices = getSyntheticAssetIndices(main, indexRepository);
            var varCalcs = getElement(main, "VaRCalcs", false);
            var volatilities = GetVolatilities(varCalcs);
            var varCorrelations = GetVarCorrelations(varCalcs);
            var funded = getBoolIfPresent(main, "Funded", true);

            var analysisParameters = getAnalysisParameters(main);
            var accountingParameters = parseAccountingParameters(name, getElement(main, "AccountingParameters"));

            var aboSalaryType =  (SalaryType?)null;

            _importProgress.SetProgress("Compiling pension scheme");

            return new PensionSchemeBase(DefaultId, name, reference, bases, assets,
                contributions, recoveries, dataSource, volatilities, null, varCorrelations, namedProperties,
                returnOnAssets, isGrowthFixed, ias,
                accountingParameters.Default, accountingParameters.IAS19Visible, accountingParameters.FRS17Visible, accountingParameters.USGAAPVisible,
                accountingParameters.USGAAPSalaryOption, null, analysisParameters, contractingOutContributionRate, ldiCashflowBasis, syntheticAssetIndices, funded, defaultBasis);
        }

        private LDICashflowBasis getLDICashflowBasis(XElement el, IFinancialIndexRepository indexRepository)
        {
            var LDIBasis = getString(el, "LDIBasis", false);
            var LDIOverrideAssumptions = getBool(el, "LDIOverrideAssumptions", false);
            var LDIDiscRateIndex = string.IsNullOrEmpty(getString(el, "LDIDiscRateIndex", false)) ? null : indexRepository.GetByName(getString(el, "LDIDiscRateIndex"));
            var LDIDiscRatePremium = getDouble(el, "LDIDiscRatePremium", false);
            var LDIInflationIndex = string.IsNullOrEmpty(getString(el, "LDIInflationIndex", false)) ? null : indexRepository.GetByName(getString(el, "LDIInflationIndex"));
            var LDIInflationGap = getDouble(el, "LDIInflationGap", false);

            return new LDICashflowBasis(LDIBasis, LDIOverrideAssumptions, LDIDiscRateIndex, LDIDiscRatePremium, LDIInflationIndex, LDIInflationGap);
        }

        private SyntheticAssetIndices getSyntheticAssetIndices(XElement el, IFinancialIndexRepository indexRepository)
        {
            Func<string, FinancialIndex> createSyntheticIndex = x => string.IsNullOrEmpty(x)
                ? null
                : indexRepository.GetByName(x);

            var syntheticEquityIncomeIndex = getString(el, "SyntheticEquityIncomeIndex", false);
            var syntheticEquityOutgoIndex = getString(el, "SyntheticEquityOutgoIndex", false);
            var syntheticCreditIncomeIndex = getString(el, "SyntheticCreditIncomeIndex", false);
            var syntheticCreditOutgoIndex = getString(el, "SyntheticCreditOutgoIndex", false);

            return new SyntheticAssetIndices
                (
                    createSyntheticIndex(syntheticEquityIncomeIndex),
                    createSyntheticIndex(syntheticEquityOutgoIndex),
                    createSyntheticIndex(syntheticCreditIncomeIndex),
                    createSyntheticIndex(syntheticCreditOutgoIndex)
                );
        }

        private AnalysisParametersNullable getAnalysisParameters(XElement el)
        {
            double? froMin = null;
            double? froMax = null;
            double? etvMin = null;
            double? etvMax = null;

            var froParameters = getElement(el, "FROParameters", false);
            var etvParameters = getElement(el, "ETVParameters", false);

            if (froParameters != null)
            {
                froMin = getDoubleNullable(froParameters, "MinTV");
                froMax = getDoubleNullable(froParameters, "MaxTV");
            }
            if (etvParameters != null)
            {
                etvMin = getDoubleNullable(etvParameters, "MinTV");
                etvMax = getDoubleNullable(etvParameters, "MaxTV");
            }

            return new AnalysisParametersNullable(froMin, froMax, etvMin, etvMax);
        }

        private AccountingParameters parseAccountingParameters(string schemeName, XElement el)
        {
            var salaryOption = getString(el, "USGAAPSalaryOption", false) ?? string.Empty;

            var parms = new AccountingParameters
            {
                IAS19Visible = string.Equals("yes", getString(el, "IAS19Visible", false), StringComparison.CurrentCultureIgnoreCase),
                FRS17Visible = string.Equals("yes", getString(el, "FRS17Visible", false), StringComparison.CurrentCultureIgnoreCase),
                USGAAPVisible = string.Equals("yes", getString(el, "USGAAPVisible", false), StringComparison.CurrentCultureIgnoreCase),
                USGAAPSalaryOption = Enum.GetNames(typeof(SalaryType)).Contains(salaryOption) ? (SalaryType)Enum.Parse(typeof(SalaryType), salaryOption) : SalaryType.Zero
            };
            AccountingStandardType accountingDefaultStandard;
            if (new AccountingStandardHelper().TryParseAccountingStandardType(getString(el, "DefaultStandard"), out accountingDefaultStandard))
            {
                parms.Default = accountingDefaultStandard;

                parms.IAS19Visible = parms.IAS19Visible || parms.Default == AccountingStandardType.IAS19;
                parms.FRS17Visible = parms.FRS17Visible || parms.Default == AccountingStandardType.FRS17;
                parms.USGAAPVisible = parms.USGAAPVisible || parms.Default == AccountingStandardType.USGAAP;
            }
            else
            {
                if (parms.IAS19Visible)
                    parms.Default = AccountingStandardType.IAS19;
                else if (parms.FRS17Visible)
                    parms.Default = AccountingStandardType.FRS17;
                else if (parms.USGAAPVisible)
                    parms.Default = AccountingStandardType.USGAAP;
                else
                    parms.Default = AccountingStandardType.IAS19;
            }

            //if importing FRS17 default standard update this to FRS102, which replaces FRS17            
            if (parms.Default == AccountingStandardType.FRS17)
            {
                LogManager.GetLogger("scheme-import").InfoFormat("{0} set as FRS17 default, import will override and set as FRS012", schemeName);
                parms.Default = AccountingStandardType.FRS102;
            }

            return parms;
        }

        private void ValidateVersion(XContainer doc)
        {
            var version = doc.Descendants("SystemVersion").FirstOrDefault();
            if (version == null)
                throw new ApplicationException("SystemVersion not found.");
            int schemeVersion;
            if (!int.TryParse(version.Value, out schemeVersion))
                throw new ApplicationException("Undefined system version.");
            if (schemeVersion != versionNumber.Value)
                throw new ApplicationException(string.Format(
                    "Version mismatch. Expected version number is {0}, supplied is {1}", versionNumber, schemeVersion));
        }

        private IDictionary<NamedPropertyGroupType, NamedPropertyGroup> GetNamedProperties(XElement root)
        {
            var result = new Dictionary<NamedPropertyGroupType, NamedPropertyGroup>();
            var mainX = root.Element("Main");
            var contributionsX = getElement(root, "Contributions");
            GetNamedProperties(getElement(mainX, "BaselineInfo", false), NamedPropertyGroupType.BaselineInfo, result);
            GetNamedProperties(getElement(contributionsX, "ScheduleOfConts", false), NamedPropertyGroupType.ScheduleOfConts, result);
            GetRefGiltYieldNamedProperties(mainX, NamedPropertyGroupType.RefGiltYeild, result);
            GetNamedPropertyGroupData(mainX, NamedPropertyGroupType.MarketData, result);
            GetNamedPropertyGroupData(mainX, NamedPropertyGroupType.AssetGrowth, result);
            GetRecoveryPlanNamedProperties(getElement(contributionsX, "RecoveryPlan"), NamedPropertyGroupType.RecoveryPlan, result);
            return result;
        }

        private void GetNamedProperties(XElement element, NamedPropertyGroupType type, IDictionary<NamedPropertyGroupType, NamedPropertyGroup> result)
        {
            if (element == null)
                return;

            var p = element.Elements("Item").Where(i => !string.IsNullOrWhiteSpace(getString(i, "Label", false)) && !string.IsNullOrWhiteSpace(getString(i, "Value", false)))
                                            .Select(i => new KeyValuePair<string, string>(getString(i, "Label"), getString(i, "Value"))).ToList();

            if (p.Any() && !string.IsNullOrWhiteSpace(getString(element, "Title", false)))
                result.Add(type, new NamedPropertyGroup(type, getString(element, "Title"), p));
        }

        private void GetRefGiltYieldNamedProperties(XElement element, NamedPropertyGroupType type, IDictionary<NamedPropertyGroupType, NamedPropertyGroup> result)
        {
            var el = element.Element("RefGiltYield");

            if (el != null)
            {
                result.Add(type,
                    new NamedPropertyGroup(type, NamedPropertyGroupType.RefGiltYeild.DisplayName(),
                        new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>(NamedRefGiltYieldPropertyTypes.DisplayName.ToString(), getString(el, NamedRefGiltYieldPropertyTypes.DisplayName.ToString())) }));
            }
        }

        private void GetNamedPropertyGroupData(XElement element, NamedPropertyGroupType type, IDictionary<NamedPropertyGroupType, NamedPropertyGroup> result)
        {
            var elementName = type.ToString();

            if (element == null || element.Element(elementName) == null)
                return;

            var el = element.Element(elementName);

            var p = el.Elements("Item").Where(i => !string.IsNullOrWhiteSpace(getString(i, "Label", false)) && !string.IsNullOrWhiteSpace(getString(i, "IndexName", false)))
                                       .Select(i => new KeyValuePair<string, string>(getString(i, "Label"), getString(i, "IndexName"))).ToList();
            if (p.Any())
                result.Add(type, new NamedPropertyGroup(type, type.DisplayName(), p));
        }

        private void GetRecoveryPlanNamedProperties(XElement element, NamedPropertyGroupType type, IDictionary<NamedPropertyGroupType, NamedPropertyGroup> result)
        {
            var values =
                Enum.GetNames(typeof(NamedRecoveryPlanPropertyTypes))
                    .Select(x => new KeyValuePair<string, string>(x, getString(element, x)))
                    .ToList();
            result.Add(type, new NamedPropertyGroup(type, NamedPropertyGroupType.RecoveryPlan.DisplayName(), values));
        }

        private SchemeDataSource GetSchemeDataSource(XElement root)
        {
            var sourceData = getElement(root.Element("Main"), "SourceData");

            return new SchemeDataSource(
                getDate(sourceData, "IAS19DisclosureDate", false),
                getDate(sourceData, "ValReportDate", false),
                getDate(sourceData, "AssetDataDate", false),
                getDate(sourceData, "MemberDataDate", false),
                getDate(sourceData, "BenefitDataDate", false));
        }

        private static DateTime? ParseDate(string date)
        {
            return string.IsNullOrWhiteSpace(date) ? (DateTime?)null : DateTime.Parse(date);
        }

        private IEnumerable<SchemeAsset> GetAssets(XElement root, IFinancialIndexRepository indexRepository, IAssetClassRepository assetClassRepository)
        {
            var schemeAssets = new List<SchemeAsset>();
            IDictionary<string, AssetClass> assetClasses = CreateAssetClasses(assetClassRepository);
            foreach (var ass in getElements(root, "Assets"))
            {
                var assets = new List<Asset>();
                var assetFunds = new List<AssetFund>();

                // get assetFunds
                foreach (var f in getChildElements(ass, "AssetAllocation"))
                {
                    var assetClass = getStringAttr(f, "Name").ToLower();
                    if (!assetClasses.ContainsKey(assetClass))
                        throw new ApplicationException(string.Format("Undefined asset class {0} in AssetAllocation", assetClass));

                    AssetClassType assetClassType = (AssetClassType)Enum.Parse(typeof(AssetClassType), getStringAttr(f, "Name").Replace(" ",""), true);

                    assetFunds.Add(new AssetFund(DefaultId, getStringAttr(f, "FundName"), DefaultId, assetClassRepository.GetById((int)assetClassType),
                        indexRepository.GetByName(getStringAttr(f, "IndexName")), getDoubleAttr(f, "Amount"), false));
                }

                // get asset classes
                foreach (var a in getChildElements(ass, "AssetClasses"))
                {
                    var assetClass = getStringAttr(a, "ClassName").ToLower();
                    if (!assetClasses.ContainsKey(assetClass))
                        throw new ApplicationException(string.Format("Undefined asset class {0} in AssetClasses", assetClass));
                    assets.Add(new Asset(DefaultId, DefaultId, assetClasses[assetClass], getStringAttr(a, "IncludeInIAS19") == Yes,
                                    getDoubleAttr(a, "DefaultReturn"), assetFunds.Where(x => x.AssetClass.Type.ToString().ToLower() == assetClass.Replace(" ","")).ToList()));
                }

                schemeAssets.Add(getSchemeAsset(ass, assets));
            }
            return schemeAssets;
        }

        private SchemeAsset getSchemeAsset(XElement element, IEnumerable<Asset> assets)
        {
            var date = getDate(element, "EffectiveDate");
            var liabHedging = getElement(element, "LiabilityHedging");
            var ldiInForce = getString(liabHedging, "LDIInForce").ToLower() == "yes" ? true : false;
            var ldiFund = getString(liabHedging, "LDIFund", false);
            var interestHedge = getDouble(liabHedging, "InterestHedge", false);
            var inflationHedge = getDouble(liabHedging, "InflationHedge", false);

            var syntheticAssets = getElement(element, "SyntheticAssets");
            var syntheticEquity = getDouble(syntheticAssets, "PropSyntheticEquity", false);
            var syntheticCredit = getDouble(syntheticAssets, "PropSyntheticCredit", false);

            var bondDurations = getElement(element, "BondDurations");
            var giltDuration = getInt(bondDurations, "GiltDuration");
            var corpDuration = getInt(bondDurations, "CorpDuration");

            var schemeAsset = new SchemeAsset(DefaultId, DefaultId, date, ldiInForce, ldiFund, interestHedge, inflationHedge,
                                                syntheticEquity, syntheticCredit, giltDuration, corpDuration, assets);
            return schemeAsset;
        }

        private IDictionary<AssetClassType, double> GetVolatilities(XElement el)
        {
            if (el != null)
            {
                var volatility = getElement(el, "Volatility", false);

                if (volatility != null && getBoolAttr(volatility, "globalOverride", false))
                {
                    var volatilityResult = new ServiceFactory().GetGlobalParametersService().GetVolatilitiesFromXDoc(el);

                    if (volatilityResult.Success && volatilityResult.Get.Any())
                    {
                        return volatilityResult.Get;
                    }
                }
            }

            return new Dictionary<AssetClassType, double>();
        }

        private IDictionary<AssetClassType, IDictionary<AssetClassType, double>> GetVarCorrelations(XElement el)
        {
            if (el != null)
            {
                var varCorrelations = getElement(el, "Correlation", false);

                if (varCorrelations != null && getBoolAttr(varCorrelations, "globalOverride", false))
                {
                    var correlationsResults = new ServiceFactory().GetGlobalParametersService().GetVarCorrelationsFromXDoc(el);

                    if (correlationsResults.Success && correlationsResults.Get.Any())
                    {
                        return correlationsResults.Get;
                    }
                }
            }

            return new Dictionary<AssetClassType, IDictionary<AssetClassType, double>>();
        }

        private List<int> insertedIndices;
        public List<int> InsertedIndices
        {
            get
            {
                return insertedIndices;
            }
        }

        public void ImportIndices(XDocument doc, IFinancialIndexRepository cachedIndexRepository)
        {
            var logger = LogManager.GetLogger("indices-import");
            insertedIndices = new List<int>();

            var customIndices = doc.Element("Data").Element("CalculatedIndices");

            if (customIndices != null)
            {
                foreach (var customIndex in customIndices.Elements("CompositeIndex"))
                {
                    var customIndexName = customIndex.Attribute("Name").Value;
                    var customIndexType = customIndex.Attribute("Type").Value;
                    var rounding = customIndex.Attribute("Rounding") == null ? .0 : double.Parse(customIndex.Attribute("Rounding").Value);

                    var components = new Dictionary<FinancialIndex, double>();

                    using (var tdb = new DbTransactionalScope())
                    {
                        var repository = (IFinancialIndexRepository)tdb.GetRepository<FinancialIndex>();

                        var existingCoreIndexOfTheSameName = repository.GetByName(customIndexName);

                        if (existingCoreIndexOfTheSameName != null && existingCoreIndexOfTheSameName.Custom == false)
                            throw new Exception("A custom index cannot have the same name as a core index");

                        var sourceIndices = customIndex.Element("SourceIndices").Elements("SourceIndex");

                        if (sourceIndices == null || sourceIndices.Count() == 0)
                            throw new Exception("A composite index must have at least one component");

                        foreach (var coreIndex in sourceIndices)
                        {
                            var existingIndex = repository.GetByNameComplete(coreIndex.Attribute("Name").Value);

                            if (existingIndex == null)
                                throw new Exception("Core existing index not found when building custom index.");

                            if (existingIndex.Custom == true)
                                throw new Exception("A custom index cannot be used to create another custom index.");

                            if(components.Keys.Any(k => k.Name == existingIndex.Name))
                                throw new Exception("Components of a composite index cannot be included more than once");

                            var weighting = coreIndex.Attribute("Proportion").Value;
                            components.Add(existingIndex, double.Parse(weighting)); //validation should have been done by now.
                        }

                        var isTotalReturn = customIndexType == "Total Return";//todo: to enumerated type?
                        var weightedCustomIndex = CustomIndexService.CreateCustomIndex(isTotalReturn, components, rounding);
                        var values = new SortedList<DateTime, double>(weightedCustomIndex);

                        //We've created the final weighted index. Time to import it.


                        bool isCustomIndex = true;
                        int indexId = InsertUpdateIndex(repository, cachedIndexRepository, customIndexName, values, isCustomIndex);

                        if (indexId > 0)
                            insertedIndices.Add(indexId);

                        logger.InfoFormat("Imported custom index '{0}'. {1} values saved.", customIndexName, values.Count.ToString());

                        tdb.Commit();
                    }
                }
            }

            Parallel.ForEach(doc.Element("Data").Elements("Index"), indexElement =>
            {
                using (var tdb = new DbTransactionalScope())
                {
                    var repository = tdb.GetRepository<FinancialIndex>();

                    var name = indexElement.Element("IndexName").Value;

                    var tempValues = new Dictionary<DateTime, double>();
                    foreach (var val in indexElement.Elements("Value").Where(val => !string.IsNullOrEmpty(val.Value)))
                    {
                        tempValues.Add(DateTime.Parse(val.Attribute("Date").Value), double.Parse(val.Value));
                    }
                    var values = new SortedList<DateTime, double>(tempValues);

                    logger.InfoFormat("Importing index '{0}'", name);

                    int indexId = InsertUpdateIndex(repository, cachedIndexRepository, name, values);

                    // save index id's in a public list - to be inserted into IndexImportIndices (after inserting record in IndexImportDetail table)
                    if (indexId > 0)
                        insertedIndices.Add(indexId);

                    logger.InfoFormat("Imported index '{0}'. {1} values saved.", name, values.Count.ToString());

                    tdb.Commit();
                }
            });
        }

        private Dictionary<string, DateTime> _indexEarliestUpdateDate = new Dictionary<string, DateTime>();

        /// <summary>
        /// This will only be populated after ImportIndices has been called
        /// </summary>
        public Dictionary<string, DateTime> IndexEarliestUpdateDate
        {
            get { return _indexEarliestUpdateDate; }
        }

        private int InsertUpdateIndex(IRepository<FinancialIndex> repository, IFinancialIndexRepository cachedIndexRepository, string name, SortedList<DateTime, double> values, bool isCustom = false)
        {
            FinancialIndex index = null;
            int indexId = 0;
            try
            {
                index = cachedIndexRepository.GetByName(name);
            }
            catch
            {
                indexId = repository.Insert(new FinancialIndex(DefaultId, name, values, isCustom));
            }
            if (index != null)
            {
                indexId = index.Id;
                repository.Update(new FinancialIndex(index.Id, index.Name, values, isCustom));

                //Keep track of the earliest date an index has changed
                foreach (var value in values)
                {
                    var maxDateCurrentlyAvailable = index.RawValues.Max(d => d.Key);

                    if (value.Key >= index.BeginDate && value.Key <= maxDateCurrentlyAvailable && index.GetValue(value.Key) != value.Value) 
                    {
                        if (!_indexEarliestUpdateDate.ContainsKey(index.Name)) 
                        {
                            _indexEarliestUpdateDate.Add(index.Name, value.Key); 
                        }
                        else if(_indexEarliestUpdateDate[index.Name] > value.Key)
                        {
                            _indexEarliestUpdateDate[index.Name] = value.Key;
                        }
                    }
                }

            }

            return indexId;
        }

        public virtual void ImportCurveData(XDocument doc)
        {
            var logger = LogManager.GetLogger("curve-data-import");
            bool indexExists;

            using (var tdb = _dbScopeFactory.GetTransactionalScope())
            {
                var repository = (ICurveDataRepository)tdb.GetRepository<CurveDataProjection>();

                foreach (var index in doc.Element("Data").Elements("Index").Elements("IndexName")) //we're expecting 2 (inflation and interest rates)
                {
                    indexExists = false;

                    var name = index.Value;

                    CurveDataProjection curve = repository.GetByName(name);

                    if (curve != null)
                        indexExists = true;

                    var spots = new List<SpotRate>();

                    foreach (var dateElement in doc.Element("Data").Elements("Index").Elements("Date")) //BOE currently publishes 25 years worth.
                    {
                        var date = DateTime.Parse(dateElement.Attribute("Value").Value);

                        foreach (var val in dateElement.Elements("Spot").Where(val => !string.IsNullOrEmpty(val.Value)))
                        {
                            spots.Add(new SpotRate(int.Parse(val.Attribute("Maturity").Value), double.Parse(val.Value), date));
                        }

                        Fill50YearsCurveData(spots, date);
                    }

                    logger.InfoFormat("Importing curve data for '{0}'", name);

                    if (indexExists == false)
                    {
                        var newId = repository.Insert(new CurveDataProjection(DefaultId, name, spots));
                        curve = new CurveDataProjection(newId, name, spots);
                        indexExists = true;
                    }
                    else
                    {
                        repository.Update(new CurveDataProjection(curve.Id, name, spots));
                    }

                    logger.InfoFormat("Imported curve data for '{0}'. {1} values saved.", name, spots.Count.ToString());
                }

                tdb.Commit();
            }
        }

        public virtual void Fill50YearsCurveData(List<SpotRate> spots, DateTime projectionDate)
        {
            for (int i = 25; i < 50; i++)
            {
                spots.Add(new SpotRate(i + 1, spots[24].Rate, projectionDate));
            }
        }

        #region xml value readers

        private string getString(XElement el, string name, bool mandatory = true)
        {
            var val = getValFromElement(el, name, mandatory);
            if (mandatory && string.IsNullOrWhiteSpace(val))
            {
                throw new Exception("Value empty.");
            }
            return val;
        }
        private string getStringAttr(XElement el, string name, bool mandatory = true)
        {
            var val = getValFromAttribute(el, name, mandatory);
            if (mandatory && string.IsNullOrWhiteSpace(val))
            {
                throw new Exception("Value empty.");
            }
            return val;
        }
        private double getDoubleAttr(XElement el, string name, bool mandatory = true)
        {
            var val = getValFromAttribute(el, name, mandatory);
            if (mandatory && string.IsNullOrWhiteSpace(val))
            {
                throw new Exception("Value empty.");
            }
            double returnVal = 0;
            if (!double.TryParse(val, out returnVal))
                throw new Exception("Invalid value.");
            return returnVal;
        }
        private double getDouble(XElement el)
        {
            return getDouble(el, null, true);
        }
        private double getDouble(XElement el, string name, bool mandatory = true)
        {
            double x;
            if (!double.TryParse(getValFromElement(el, name, mandatory), out x)
                && mandatory)
            {
                throw new Exception("Value empty or in unexpected format.");
            }
            return x;
        }
        private int getInt(XElement el, string name, bool mandatory = true)
        {
            int x;
            if (!int.TryParse(getValFromElement(el, name, mandatory), out x)
                && mandatory)
            {
                throw new Exception("Value empty or in unexpected format.");
            }
            return x;
        }
        private int? getIntNullable(XElement el, string name)
        {
            int x;
            if (!int.TryParse(getValFromElement(el, name, false), out x))
            {
                return null;
            }
            return x;
        }
        private double? getDoubleNullable(XElement el, string name)
        {
            double x;
            if (!double.TryParse(getValFromElement(el, name, false), out x))
            {
                return null;
            }
            return x;
        }
        private int getIntAttr(XElement el, string name, bool mandatory = true)
        {
            int x;
            if (!int.TryParse(getValFromAttribute(el, name, mandatory), out x)
                && mandatory)
            {
                throw new Exception("Value empty or in unexpected format.");
            }
            return x;
        }
        private bool getBool(XElement el, string name, bool mandatory = true)
        {
            bool x;
            if (!tryParseBoolString(getValFromElement(el, name, mandatory), out x)
                && mandatory)
            {
                throw new Exception("Value empty or in unexpected format.");
            }
            return x;
        }
        private bool getBoolAttr(XElement el, string name, bool mandatory = true)
        {
            bool x;
            if (!tryParseBoolString(getValFromAttribute(el, name, mandatory), out x)
                && mandatory)
            {
                throw new Exception("Value empty or in unexpected format.");
            }
            return x;
        }
        private bool tryParseBoolString(string val, out bool x)
        {
            x = false;
            var parsed = false;
            var v = val.ToLower().Trim();
            if (v == "true" || v == "y" || v == "yes" || v == "1")
            {
                parsed = true;
                x = true;
            }
            else if (v == "false" || v == "n" || v == "no" || v == "0")
            {
                parsed = true;
            }

            return parsed;
        }
        private bool getBoolIfPresent(XElement el, string name, bool dft)
        {
            var r = dft;
            if (el.Element(name) != null)
            {
                bool x;
                if (tryParseBoolString(getValFromElement(el, name, true), out x))
                {
                    r = x;
                }
            }
            return r;
        }
        private DateTime getDate(XElement el, string name)
        {
            return getDateNullable(el, name, true).Value;
        }
        private DateTime? getDate(XElement el, string name, bool mandatory)
        {
            return getDateNullable(el, name, mandatory);
        }
        private DateTime? getDateNullable(XElement el, string name, bool mandatory = true)
        {
            DateTime d;
            DateTime? result = null;
            if (DateTime.TryParse(getValFromElement(el, name, mandatory), out d))
            {
                result = d;
            }
            else if (mandatory)
            {
                throw new Exception("Value empty or in unexpected format.");
            }
            return result;
        }
        private XElement getElement(XElement el, string name, bool mandatory = true)
        {
            _importProgress.SetProgress(string.Format("Attempting to read '{0}' at {1}.", name, getLocation(el)));
            var element = el.Element(name);
            if (element == null && mandatory)
            {
                throw new Exception("Element not found.");
            }
            return element;
        }
        private IEnumerable<XElement> getElements(XElement el, string name)
        {
            _importProgress.SetProgress(string.Format("Attempting to read '{0}' at {1}.", name, getLocation(el)));
            return el.Descendants(name);
        }
        private IEnumerable<XElement> getChildElements(XElement el, string name)
        {
            _importProgress.SetProgress(string.Format("Attempting to read '{0}' at {1}.", name, getLocation(el)));
            return el.Descendants(name).Elements();
        }
        private string getValFromElement(XElement el, string name, bool mandatory)
        {
            var val = string.Empty;
            _importProgress.SetProgress(string.Format("Attempting to read {0} at {1}.", string.IsNullOrEmpty(name) ? "value" : "'" + name + "'", getLocation(el)));
            if (string.IsNullOrEmpty(name))
            {
                val = el.Value;
            }
            else if (!el.Descendants(name).IsNullOrEmpty())
            {
                val = el.Descendants(name).First().Value;
            }
            if (string.IsNullOrEmpty(val) && mandatory)
            {
                throw new Exception("Value not found.");
            }
            return val;
        }
        private string getValFromAttribute(XElement el, string name, bool mandatory)
        {
            var val = string.Empty;
            _importProgress.SetProgress(string.Format("Attempting to read attribute '{0}' at {1}.", name, getLocation(el)));
            var attr = el.Attribute(name);
            if (attr != null)
            {
                val = attr.Value;
            }
            if (string.IsNullOrEmpty(val) && mandatory)
            {
                throw new Exception("Value not found.");
            }
            return val;
        }

        private string getLocation(XElement el)
        {
            var elements = new List<string>(10);
            var sb = new StringBuilder(100);
            var e = el;
            while (e != null)
            {
                elements.Insert(0, e.Name.LocalName);
                // record if in a particular basis
                if (e.Name.LocalName == "Liabs")
                {
                    var basisEl = e.Descendants("Basis").FirstOrDefault();
                    if (basisEl != null & !string.IsNullOrEmpty(basisEl.Value))
                    {
                        var nameEl = e.Descendants("BasisName").FirstOrDefault();
                        var name = nameEl != null && !string.IsNullOrEmpty(nameEl.Value) ? nameEl.Value : basisEl.Value;
                        elements[0] += " [Basis='" + name + "']";
                    }
                }
                e = e.Parent;
            }
            foreach (var name in elements)
            {
                if (sb.Length > 0) sb.Append(" > ");
                sb.Append(name);
            }
            return sb.ToString();
        }

        #endregion
    }
}
