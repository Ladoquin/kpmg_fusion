﻿using FlightDeck.DataAccess;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.Services
{
    public class AccountService : IAccountService
    {

        public UserProfile SetPasswordFlag(string username, bool passwordMustChange)
        {
            using (var db = new DbInteractionScope())
            {
                var repo = (IUserProfileRepository)db.GetRepository<UserProfile>();

                var profile = repo.GetByUsername(username);

                profile.PasswordMustChange = passwordMustChange;

                repo.Update(profile);

                return profile;
            }
        }

        public UserProfile AcceptTerms(string username)
        {
            using (var db = new DbInteractionScope())
            {
                var repo = (IUserProfileRepository)db.GetRepository<UserProfile>();

                var profile = repo.GetByUsername(username);

                profile.TermsAccepted = true;

                repo.Update(profile);

                return profile;
            }
        }

        public UserProfile Unlock(string username)
        {
            using (var db = new DbInteractionScope())
            {
                var profile = ((IUserProfileRepository)db.GetRepository<UserProfile>()).GetByUsername(username);

                var repo = (IUserProfileMembershipRepository)db.GetRepository<UserProfileMembership>();

                repo.Unlock(profile.UserId);

                return profile;
            }
        }
    }
}
