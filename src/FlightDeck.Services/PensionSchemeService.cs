using FlightDeck.DataAccess;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using FlightDeck.Services.Listeners;
using log4net;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;

namespace FlightDeck.Services
{
    public class PensionSchemeService : IPensionSchemeService
    {
        private KeyValuePair<string, UserProfile> _profileCache;
        private readonly IDbInteractionScopeFactory _dbScopeFactory;
        private readonly IEncryptionService _encryptionService;
        private readonly IPrincipal _principal;
        private readonly Lazy<IFinancialIndexRepository> _financialIndexRepository;
        private readonly IAssetClassRepository _assetClassRepository;
        private readonly ILog log;
        private readonly List<IPensionSchemeCreatedListener> _createdListeners =
            new List<IPensionSchemeCreatedListener>
                {
                    new IPensionSchemeCreatedListenerUserAccess()
                };
        private readonly List<IPensionSchemeUpdatedListener> _updatedListeners =
            new List<IPensionSchemeUpdatedListener> 
                { 
                    new PensionSchemeUpdatedListenerClientAssumptions(), 
                    new PensionSchemeUpdatedListenerUserAcess()
                };

        public GetResult<ISchemeAdminServiceResult, Dictionary<string, object>> ValidationResult;
        public PensionSchemeService(IPrincipal principal, Lazy<IFinancialIndexRepository> financialIndexRepository)
            : this(principal, financialIndexRepository, new EncryptionService(), new DbInteractionScopeFactory())
        {
        }
        public PensionSchemeService(IPrincipal principal, Lazy<IFinancialIndexRepository> financialIndexRepository, IEncryptionService encryptionService, IDbInteractionScopeFactory dbScopeFactory)
        {
            _dbScopeFactory = dbScopeFactory;
            _principal = principal;
            _financialIndexRepository = financialIndexRepository;
            _encryptionService = encryptionService;
            using (var db = _dbScopeFactory.GetScope())
            {
                _assetClassRepository = (IAssetClassRepository)db.GetRepository<AssetClass>();//todo: pass in
            }
            log = LogManager.GetLogger(GetType());
        }

        public IScheme GetActive()
        {
            var profile = getProfile();

            if (profile.ActiveSchemeDetailId.HasValue)
            {
                return getScheme(profile.ActiveSchemeDetailId.Value, true);
            }
            else
            {
                return null;
            }
        }

        public string GetActiveSchemeName()
        {
            var schemeName = string.Empty;
            var profile = getProfile();

            if (profile.ActiveSchemeDetailId.HasValue)
            {
                using (var db = new DbInteractionScope())
                {
                    var record = ((ISchemeDetailRepository)db.GetRepository<SchemeDetail>()).GetById(profile.ActiveSchemeDetailId.Value);
                    if (record != null)
                        schemeName = record.SchemeName;
                }
            }
            return schemeName;
        }

        private IScheme getScheme(int id, bool includePensionData)
        {
            using (var db = new DbInteractionScope(IsolationLevel.ReadUncommitted))
            {
                var scheme = new Scheme();

                var record = ((ISchemeDetailRepository)db.GetRepository<SchemeDetail>()).GetById(id);

                if (record != null)
                {
                    scheme.InjectFrom(record);

                    if (string.IsNullOrEmpty(scheme.CurrencySymbol))
                        scheme.CurrencySymbol = "�";

                    if (includePensionData && scheme.SchemeData_Id.HasValue)
                    {
                        var schemeRepo = (IPensionSchemeRepository)db.GetRepository<PensionScheme>();
                        var assetClassRepository = (IAssetClassRepository)db.GetRepository<AssetClass>();
                        var pensionData = schemeRepo.GetById(scheme.SchemeData_Id.Value, _financialIndexRepository.Value, assetClassRepository);
                        log.InfoFormat("Retrieved pension scheme {0}.", pensionData.Id);

                        var serializer = new Serializer();
                        var cachedBasisManager = new CachedBasisManager(serializer, _dbScopeFactory);

                        scheme.PensionScheme = new CachedPensionScheme(pensionData, cachedBasisManager, serializer, _dbScopeFactory);
                    }

                    return scheme;
                }
            }

            return null;
        }

        public IScheme GetSummary(int id)
        {
            var scheme = getScheme(id, false);

            if (scheme != null)
            {
                return filterAccessible(new IScheme[] { scheme }).SingleOrDefault();
            }
            else
            {
                return null;
            }
        }

        public IScheme GetSummaryByName(string schemeName)
        {
            using (var db = new DbInteractionScope())
            {
                var scheme = new Scheme();

                scheme.InjectFrom(((ISchemeDetailRepository)db.GetRepository<SchemeDetail>()).GetByName(schemeName));

                if (scheme != null)
                {
                    return filterAccessible(new IScheme[] { scheme }).SingleOrDefault();
                }
                else
                {
                    return null;
                }
            }
        }

        public IEnumerable<IScheme> GetAllSummaries()
        {
            using (var db = new DbInteractionScope())
            {
                var schemeRepo = (ISchemeDetailRepository)db.GetRepository<SchemeDetail>();

                var schemes = schemeRepo.GetAll()
                    .Select(x => (Scheme)new Scheme().InjectFrom(x));

                return filterAccessible(schemes);
            }
        }

        public IScheme GetSchemeWithDetails(int id)
        {
            var scheme = getScheme(id, true);

            if (scheme != null)
            {
                return filterAccessible(new IScheme[] { scheme }).SingleOrDefault();
            }
            else
            {
                return null;
            }
        }

        public XDocument GetSchemeSource(int id)
        {
            using (var db = _dbScopeFactory.GetScope())
            {
                var source = db.GetRepository<SchemeDetailSource>().GetById(id);

                if (source != null)
                {
                    return source.Source;
                }
            }

            return null;
        }

        private IEnumerable<IScheme> filterAccessible(IEnumerable<IScheme> schemeDetails)
        {
            var profile = getProfile();

            Func<IScheme, bool> hasDirectAccess =
                scheme =>
                    scheme.CreatedByUserId.GetValueOrDefault(0) == profile.UserId ||
                    (
                    profile.SchemeDetails.IsNullOrEmpty() == false &&
                    profile.SchemeDetails.Select(x => x.SchemeDetailId).Contains(scheme.SchemeDetailId)
                    );

            if (profile != null)
            {
                switch (profile.Role)
                {
                    case RoleType.Admin:
                        break;
                    case RoleType.Installer:
                    case RoleType.Consultant:
                        schemeDetails = schemeDetails.Where(x => !x.IsRestricted || hasDirectAccess(x));
                        break;
                    case RoleType.Client:
                    default:
                        schemeDetails = schemeDetails.Where(x => hasDirectAccess(x));
                        break;
                }
            }

            return schemeDetails;
        }

        public DetailedResult<ISchemeAdminServiceResult, IScheme> Save(string schemeNameKey, SchemeDetail scheme, IEnumerable<SchemeDocument> documents, string updateBy, int createdByUserId, XDocument schemeDataXml = null)
        {
            var editing = !string.IsNullOrEmpty(schemeNameKey);
            var result = new DetailedResult<ISchemeAdminServiceResult, IScheme>();
            var summary = new Scheme();

            if (ValidationResult == null)
                ValidateResults(schemeNameKey, scheme, schemeDataXml);

            if (ValidationResult.Success)
            {
                var audit = editing ? prepareHistoricRecord(schemeNameKey) : null;

                summary.InjectFrom(getSummaryToSave(schemeNameKey, scheme, ValidationResult.Get, documents, updateBy, createdByUserId, schemeDataXml));

                using (var db = new DbTransactionalScope())
                {
                    if (schemeDataXml != null)
                    {
                        var assetClassRepository = (IAssetClassRepository)db.GetRepository<AssetClass>();
                        var importResult = new DataImportService().ImportScheme(scheme.SchemeName, _principal.Identity.Name, schemeDataXml, _financialIndexRepository.Value, _assetClassRepository);
                        if (importResult.Completed)
                        {
                            summary.SchemeData_Id = importResult.Result;
                        }
                        else
                        {
                            result.ValidationErrors = new List<ISchemeAdminServiceResult> { ISchemeAdminServiceResult.XMLError };
                            result.DataImportProgress = importResult.Progress;
                            result.DataImportException = importResult.Exception;
                        }
                    }

                    if (result.ValidationErrors.IsNullOrEmpty())
                    {
                        var schemeRepo = (ISchemeDetailRepository)db.GetRepository<SchemeDetail>();

                        if (!editing)
                        {
                            schemeRepo.Insert(summary);

                            _createdListeners.ForEach(x => x.SchemeCreated(summary, db, _principal.Identity.Name));
                        }
                        else
                        {
                            schemeRepo.Update(summary);

                            _updatedListeners.ForEach(x => x.SchemeUpdated(summary, db, _principal.Identity.Name));

                            db.GetRepository<HistoricSchemeDetail>().Insert(audit);
                        }

                        if (schemeDataXml != null)
                        {
                            db.GetRepository<SchemeDetailSource>().InsertOrUpdate(new SchemeDetailSource(summary.SchemeDetailId, schemeDataXml));
                        }

                        db.Commit();

                        result.Success = true;
                        result.Result = summary;
                    }
                }
            }
            else
            {
                result.ValidationErrors = ValidationResult.Statuses;
            }

            return result;
        }

        public void ValidateResults(string schemeNameKey, SchemeDetail scheme, XDocument schemeDataXml = null)
        {
            ValidationResult = validate(schemeNameKey, scheme, schemeDataXml);
        }

        public IEnumerable<IScheme> GetImportHistory(string schemeName)
        {
            var history = new List<SchemeDetail>();
            using (var db = new DbInteractionScope())
            {
                var scheme = ((ISchemeDetailRepository)db.GetRepository<SchemeDetail>()).GetByName(schemeName);
                if (scheme != null)
                {
                    history.Add(scheme);
                    history.AddRange(
                        ((IHistoricSchemeDetailRepository)db.GetRepository<HistoricSchemeDetail>()).GetAll(scheme.SchemeDetailId)
                            .Select(x => (SchemeDetail)new SchemeDetail().InjectFrom(x)));
                }
            }
            return history.Select(x => (IScheme)new Scheme().InjectFrom(x));
        }

        public void Delete(string schemeName)
        {
            using (var db = new DbTransactionalScope())
            {
                var schemeRepo = (ISchemeDetailRepository)db.GetRepository<SchemeDetail>();

                var scheme = schemeRepo.GetByName(schemeName);

                schemeRepo.Delete(scheme);

                db.Commit();
            }
        }

        public void DeleteById(int id)
        {
            using (var db = new DbTransactionalScope())
            {
                var schemeRepo = (ISchemeDetailRepository)db.GetRepository<SchemeDetail>();

                var scheme = schemeRepo.GetById(id);

                if (scheme != null)
                {
                    schemeRepo.Delete(scheme);
                    db.Commit();
                }
                else
                    throw new Exception(string.Format("Delete operation failed. Cannot find scheme with id: {0}", id));
            }
        }

        public void DeleteDocument(int schemeId, int documentId)
        {
            using (var db = new DbTransactionalScope())
            {
                var schemeRepo = (ISchemeDetailRepository)db.GetRepository<SchemeDetail>();

                var scheme = schemeRepo.GetById(schemeId);

                if (scheme != null && scheme.Documents != null && scheme.Documents.Any())
                {
                    var doc = scheme.Documents.Where(x => x.SchemeDocumentId == documentId).Single();
                    if(doc != null)
                        schemeRepo.DeleteDocument(doc);
                }

                db.Commit();
            }
        }

        private GetResult<ISchemeAdminServiceResult, Dictionary<string, object>> validate(string schemeNameKey, SchemeDetail scheme, XDocument schemeDataXml = null)
        {
            var errors = new List<ISchemeAdminServiceResult>();
            var xmlValidationResult = new GetResult<ISchemeAdminServiceResult, Dictionary<string, object>>(true);

            var checkForDuplicateSchemeName = string.IsNullOrEmpty(schemeNameKey) || !string.Equals(schemeNameKey, scheme.SchemeName, StringComparison.CurrentCultureIgnoreCase);

            if (checkForDuplicateSchemeName)
            {
                // validate scheme name is available

                using (var db = new DbInteractionScope())
                {
                    var repo = (ISchemeDetailRepository)db.GetRepository<SchemeDetail>();

                    if (repo.GetByName(scheme.SchemeName) != null)
                    {
                        errors.Add(ISchemeAdminServiceResult.NameConflict);
                    }
                }
            }

            if (!errors.Any())
            {
                // if xml, then scheme name must match supplied
                if (schemeDataXml != null)
                {
                    xmlValidationResult = validateAndExtractSchemeXmlDetails(schemeDataXml);
                    if (xmlValidationResult.Success)
                    {
                        var schemeDataExtract = xmlValidationResult.Get;
                        if ((string)schemeDataExtract["SchemeName"] != scheme.SchemeName)
                        {
                            errors.Add(ISchemeAdminServiceResult.NameMismatch);
                        }
                        else
                        {
                            // is schemeref unique?
                            var schemeRef = schemeDataExtract["SchemeRef"] as string;
                            if (checkForDuplicateSchemeName)
                            {
                                using (var db = new DbInteractionScope())
                                {
                                    var repo = (ISchemeDetailRepository)db.GetRepository<SchemeDetail>();

                                    if (repo.GetAll().Any(s => s.SchemeData_SchemeRef == schemeRef))
                                    {
                                        errors.Add(ISchemeAdminServiceResult.ReferenceConflict);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        errors.AddRange(xmlValidationResult.Statuses);
                    }
                }
            }

            return new GetResult<ISchemeAdminServiceResult, Dictionary<string, object>>(xmlValidationResult.Get, errors);
        }

        private GetResult<ISchemeAdminServiceResult, Dictionary<string, object>> validateAndExtractSchemeXmlDetails(XDocument schemeDataXml)
        {
            var errors = new List<ISchemeAdminServiceResult>();
            var extract = new Dictionary<string, object>();

            // SchemeName
            var schemeNameElement = schemeDataXml.XPathSelectElement("/Data/Main/SchemeName");
            if (IsBlank(schemeNameElement)) errors.Add(ISchemeAdminServiceResult.XMLNameMissing);
            else extract["SchemeName"] = schemeNameElement.Value;

            // SchemeRef
            var schemeRefElement = schemeDataXml.XPathSelectElement("/Data/Main/SchemeRef");
            if (IsBlank(schemeRefElement)) errors.Add(ISchemeAdminServiceResult.XMLReferenceMissing);
            else extract["SchemeRef"] = schemeRefElement.Value;


            // ProducedOnDate
            try
            {
                var dateElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Date");
                var timeElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Time");

                if (IsBlank(dateElement) || IsBlank(timeElement)) errors.Add(ISchemeAdminServiceResult.XMLDateMissing);
                else extract["ProducedOnDate"] = DateTime.Parse(dateElement.Value + ' ' + timeElement.Value, new CultureInfo("en-GB").DateTimeFormat);
            }
            catch
            {
                errors.Add(ISchemeAdminServiceResult.XMLDateInvalid);
            }


            // ProducedByUser
            var userElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/UserName");
            if (IsBlank(userElement)) errors.Add(ISchemeAdminServiceResult.XMLUsernameMissing);
            else extract["ProducedByUser"] = userElement.Value;

            // DataComment
            var commentElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/DataComment");
            if (!IsBlank(commentElement)) extract["Comment"] = commentElement.Value;

            // Spreadsheet
            var spreadsheetElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Spreadsheet");
            if (IsBlank(spreadsheetElement)) errors.Add(ISchemeAdminServiceResult.XMLSpreadsheetPathMissing);
            else extract["Spreadsheet"] = spreadsheetElement.Value;

            // ExportFile
            var exportFileElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/ExportFile");
            if (!IsBlank(exportFileElement)) extract["ExportFile"] = exportFileElement.Value;

            // DataCaptureVersion
            var versionElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/DataCaptureVersion");
            if (IsBlank(versionElement)) errors.Add(ISchemeAdminServiceResult.XMLDataCaptureVersionMissing);
            else extract["DataCaptureVersion"] = versionElement.Value;

            return new GetResult<ISchemeAdminServiceResult, Dictionary<string, object>>(extract, errors);
        }

        private bool isValidDateFormat(string inputDate)
        {
            // check if date in required format and is valid
            Regex regex = new Regex(@"^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$");
            Match match = regex.Match(inputDate);
            DateTime tempDate;
            if (match.Success && DateTime.TryParse(inputDate, out tempDate))
                return true;
            else
                return false;
        }

        private bool IsDateInPast(DateTime inputDate)
        {
            if ((inputDate.Date - DateTime.Now.Date).Days < 0)
                return true;
            else
                return false;
        }

        private bool IsBlank(XElement element)
        {
            return element == null || element.Value.Trim() == String.Empty;
        }

        private SchemeDetail getSummaryToSave(string pk, SchemeDetail scheme, Dictionary<string, object> schemeDataExtract, IEnumerable<SchemeDocument> documents, string updateBy, int createdByUserId, XDocument schemeDataXml)
        {
            var record = new SchemeDetail();
            if (!string.IsNullOrEmpty(pk))
            {
                using (var db = new DbInteractionScope())
                {
                    record = ((ISchemeDetailRepository)db.GetRepository<SchemeDetail>()).GetByName(pk);
                }
            }
            else
            {
                record.CreatedByUserId = createdByUserId;
            }

            if (schemeDataXml != null)
            {
                Func<string, string> tryGetVal = x => schemeDataXml.XPathSelectElement(x) != null ? schemeDataXml.XPathSelectElement(x).Value : string.Empty;

                record.WelcomeMessage = tryGetVal("Data/Main/WelcomeMessage");
                record.ContactName = tryGetVal("Data/Main/ContactName");
                record.ContactNumber = tryGetVal("Data/Main/ContactTel");
                record.ContactEmail = tryGetVal("Data/Main/ContactEmail");
                record.CurrencySymbol = tryGetVal("Data/Main/CurrencySymbol");

                // VaRCalcs
                var varCalcsEl = schemeDataXml.XPathSelectElement("/Data/Main/VaRCalcs");
                Func<string, bool> hasVaRCalcsOverride = elName =>
                {
                    if (varCalcsEl != null)
                    {
                        if (varCalcsEl.Descendants(elName).Any())
                        {
                            var attr = varCalcsEl.Descendants(elName).First().Attribute("globalOverride");
                            bool x;
                            if (attr != null && bool.TryParse(attr.Value, out x))
                            {
                                return x;
                            }
                        }
                    }
                    return false;
                };
                record.HasVolatilities = hasVaRCalcsOverride("Volatility");
                record.HasVarCorrelations = hasVaRCalcsOverride("Correlation");
            }

            record.SchemeName = scheme.SchemeName;
            record.Logo = scheme.Logo ?? record.Logo;
            record.IsRestricted = scheme.IsRestricted;
            record.AccountingDownloadsPassword = string.IsNullOrEmpty(scheme.AccountingDownloadsPassword) ? record.AccountingDownloadsPassword : _encryptionService.Encrypt(scheme.AccountingDownloadsPassword);
            record.UpdatedOnDate = DateTime.Now;
            record.UpdatedByUser = updateBy;

            if (schemeDataExtract != null)
            {
                record.SchemeData_SchemeRef = (string)schemeDataExtract["SchemeRef"];
                record.SchemeData_ImportedOnDate = DateTime.Now;
                record.SchemeData_ImportedByUser = updateBy;
                record.SchemeData_ProducedOnDate = (DateTime)schemeDataExtract["ProducedOnDate"];
                record.SchemeData_ProducedByUser = (string)schemeDataExtract["ProducedByUser"];
                record.SchemeData_Spreadsheet = (string)schemeDataExtract["Spreadsheet"];
                record.SchemeData_Comment = schemeDataExtract.ContainsKey("Comment") ? schemeDataExtract["Comment"] as string : null;
                record.SchemeData_ExportFile = schemeDataExtract.ContainsKey("ExportFile") ? schemeDataExtract["ExportFile"] as string : null;
                record.DataCaptureVersion = schemeDataExtract.ContainsKey("DataCaptureVersion") ? schemeDataExtract["DataCaptureVersion"] as string : null;
            }

            record.Documents = documents != null ? documents.ToList() : null;

            return record;
        }

        private HistoricSchemeDetail prepareHistoricRecord(string pk)
        {
            SchemeDetail scheme;

            using (var db = new DbInteractionScope())
            {
                scheme = ((ISchemeDetailRepository)db.GetRepository<SchemeDetail>()).GetByName(pk);
            }

            var historicRecord = new HistoricSchemeDetail();

            historicRecord.SourceSchemeDetailId = scheme.SchemeDetailId;

            historicRecord.SchemeName = scheme.SchemeName;
            historicRecord.SchemeData_SchemeRef = scheme.SchemeData_SchemeRef;
            historicRecord.SchemeData_AnchorDate = scheme.SchemeData_AnchorDate;

            historicRecord.SchemeData_Id = scheme.SchemeData_Id;

            historicRecord.SchemeData_ImportedOnDate = scheme.SchemeData_ImportedOnDate;
            historicRecord.SchemeData_ImportedByUser = scheme.SchemeData_ImportedByUser;
            historicRecord.SchemeData_ProducedOnDate = scheme.SchemeData_ProducedOnDate;
            historicRecord.SchemeData_ProducedByUser = scheme.SchemeData_ProducedByUser;

            historicRecord.SchemeData_Spreadsheet = scheme.SchemeData_Spreadsheet;
            historicRecord.SchemeData_Comment = scheme.SchemeData_Comment;
            historicRecord.SchemeData_ExportFile = scheme.SchemeData_ExportFile;

            historicRecord.UpdatedOnDate = scheme.UpdatedOnDate;
            historicRecord.UpdatedByUser = scheme.UpdatedByUser;

            historicRecord.WelcomeMessage = scheme.WelcomeMessage;
            historicRecord.Logo = scheme.Logo;

            historicRecord.ContactName = scheme.ContactName;
            historicRecord.ContactNumber = scheme.ContactNumber;
            historicRecord.ContactEmail = scheme.ContactEmail;
            historicRecord.AccountingDownloadsPassword = scheme.AccountingDownloadsPassword;

            historicRecord.DataCaptureVersion = scheme.DataCaptureVersion;

            return historicRecord;
        }

        private UserProfile getProfile()
        {
            if (_profileCache.Key != _principal.Identity.Name)
            {
                using (var db = new DbInteractionScope())
                {
                    _profileCache = new KeyValuePair<string,UserProfile>(
                        _principal.Identity.Name,
                        ((IUserProfileRepository)db.GetRepository<UserProfile>()).GetByUsername(_principal.Identity.Name));
                }
            }
            return _profileCache.Value;
        }
    }
}