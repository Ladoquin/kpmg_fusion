﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    internal interface ICachedBasisManager
    {
        ICachedBasis CurrentBasis { get; }
        ICachedBasis DefaultBasis { get; }
        void SetBases(ICachedPensionScheme scheme);
        void SetCurrent(int masterbasisId);
        ICachedBasis GetBasis(int masterbasisId);
        ICachedBasis GetFirstBasisOfType(BasisType type);
        IEnumerable<ICachedBasis> GetAll();
    }
}
