﻿namespace FlightDeck.Services
{
    using FlightDeck.DataAccess;
    using FlightDeck.Domain;
    
    public class DbInteractionScopeFactory : IDbInteractionScopeFactory
    {
        public IDbInteractionScope GetScope()
        {
            return new DbInteractionScope();
        }
        public IDbTransactionalScope GetTransactionalScope()
        {
            return new DbTransactionalScope();
        }
    }
}
