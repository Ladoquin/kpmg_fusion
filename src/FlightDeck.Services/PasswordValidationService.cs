﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using log4net;
    using System.Linq;
    
    public class PasswordValidationService : IPasswordValidationService
    {
        private readonly IUserPasswordEncrypter passwordEncrypter;
        private readonly IDbInteractionScopeFactory dbInteractionScopeFactory;
        private readonly ILog logger;

        public PasswordValidationService(IUserPasswordEncrypter passwordEncrypter)
            : this(passwordEncrypter, new DbInteractionScopeFactory())
        {
        }

        public PasswordValidationService(IUserPasswordEncrypter passwordEncrypter, IDbInteractionScopeFactory dbInteractionScopeFactory)
        {
            this.passwordEncrypter = passwordEncrypter;
            this.dbInteractionScopeFactory = dbInteractionScopeFactory;
            this.logger = LogManager.GetLogger("account-login");
        }

        public bool IsValid(string username, string oldpassword, string password)
        {
            if (string.IsNullOrEmpty(password) || password.Trim().Length == 0)
                return false;

            var isPasswordSameAsExisting = string.Equals(oldpassword, password, System.StringComparison.CurrentCultureIgnoreCase);
            var isPasswordInHistory = IsPasswordInHistory(username, password);
            var isPasswordSameAsUsername = IsPasswordSameAsUsername(username, password);

            if (isPasswordSameAsExisting)
                logger.Warn("Password same as existing");
            if (isPasswordInHistory)
                logger.Warn("Password same as one in history");
            if (isPasswordSameAsUsername)
                logger.Warn("Password same as username");

            return !isPasswordSameAsExisting && !isPasswordInHistory && !isPasswordSameAsUsername;
        }

        private bool IsPasswordInHistory(string username, string password)
        {
            var isPasswordInHistory = true;            

            using (var db = dbInteractionScopeFactory.GetScope())
            {
                var repo = (IUserProfilePasswordRepository)db.GetRepository<PasswordData>();

                var salt = repo.GetSalt(username);

                if (salt != null)
                {
                    var history = repo.GetHistory(username);

                    var target = passwordEncrypter.GetEncrypted(password, salt).EncryptedPassword;

                    isPasswordInHistory = history.Any(x => x.SequenceEqual(target));
                }
                else
                {
                    isPasswordInHistory = false; // users already with accounts won't have a password history or salt yet.
                }
            }

            return isPasswordInHistory;
        }

        private bool IsPasswordSameAsUsername(string username, string password)
        {
            var isPasswordSameAsUsername =
                string.Equals(username, password, System.StringComparison.CurrentCultureIgnoreCase) ||
                password.ToLower().IndexOf(username.ToLower()) >= 0 ||
                username.ToLower().IndexOf(password.ToLower()) >= 0;

            return isPasswordSameAsUsername;
        }
    }
}
