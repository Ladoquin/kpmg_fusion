﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Accounting;
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.Evolution;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using FlightDeck.ServiceInterfaces;
    using log4net;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;

    public class CachedBasis : IBasis, ICachedBasis
    {
        private readonly MasterBasis mb;
        private readonly DateTime refreshDate;
        private readonly ICachedPensionScheme scheme;
        private readonly IBasesDataManager basesDataManager;
        private readonly IPortfolioManager portfolioManager;
        private readonly ISerializer serializer;
        private readonly IDbInteractionScopeFactory dbScopeFactory;
        private readonly IAssumptionIndexEvolutionService assumptionIndexEvolutionService;
        private readonly IPensionIncreaseIndexEvolutionService pensionIncreaseIndexEvolutionService;

        private BasisEvolutionData evolutionResults;
        private MutableBeforeAfter<BasisAnalysisResults> analysisResults;
        private BasisAnalysisResults refreshDateResults;
        private IDictionary<DateTime, EvolutionData> combinedEvolutionResults;
        private MutableBeforeAfter<USGAAPDisclosure.NetPeriodicPensionCostForecastData> usgaapForecast;
        private bool stale;
        private bool usgaapStale = true;
        private bool varWaterFallStale;

        public bool Initialised { get; private set; }
        public string Name { get; private set; }
        public BasisType BasisType { get; private set; }
        public int MasterBasisId { get; private set; }
        public DateTime AnchorDate { get; private set; }
        public DateTime AttributionStart { get; private set; }
        public DateTime AttributionEnd { get; private set; }
        public DateTime? EvolutionStartDate { get; set; }
        public DateTime? EvolutionEndDate { get; set; }
        public TotalCosts TotalCostsAtAnchor { get; private set; }

        public event InputChangeHandler OnBasisChange;

        public enum ToContextMode
        {
            AsIs,
            WithAnalysisResults
        }

        internal CachedBasis(MasterBasis mb, ICachedPensionScheme scheme, ISerializer serializer, IDbInteractionScopeFactory dbScopeFactory, DateTime refreshDate)
        {
            this.mb = mb;
            this.scheme = scheme;
            this.basesDataManager = new BasesDataManager(mb.Bases.Values);
            this.portfolioManager = new PortfolioManager(scheme.Data, basesDataManager);
            this.serializer = serializer;
            this.dbScopeFactory = dbScopeFactory;
            this.MasterBasisId = mb.MasterBasisId;
            this.BasisType = mb.Type;
            this.Name = mb.DisplayName;
            this.AnchorDate = mb.Bases.Min(x => x.Key);
            this.refreshDate = refreshDate;
            this.assumptionIndexEvolutionService = new AssumptionIndexEvolutionService();
            this.pensionIncreaseIndexEvolutionService = new PensionIncreaseIndexEvolutionService();

            this.AttributionStart = AnchorDate;
            this.AttributionEnd = refreshDate;

            var b = basesDataManager.GetBasis(AnchorDate);
            if (b != null)
                this.TotalCostsAtAnchor = b.TotalCosts;

            this.OnBasisChange += handleBasisChange;

            scheme.OnInputChange += handleSchemeInputChange;
            scheme.OnWaterfallChange += handleWaterfallChange;
        }

        private void checkInitialised()
        {
            if (evolutionResults == null)
                Initialise();
        }

        public void Initialise()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var perfLog = LogManager.GetLogger("performance");

            BasisCache cache = null;

            // Check for pre-calculated LDI
            using (var db = dbScopeFactory.GetScope())
            {
                cache = ((IBasisCacheRepository)db.GetRepository<BasisCache>()).GetByMasterBasisId(MasterBasisId);
                if (cache != null && cache.BasisEvolutionData != null)
                {
                    evolutionResults = serializer.DeserializeObject<BasisEvolutionData>(cache.BasisEvolutionData);
                }
                else
                {
                    evolutionResults = BasisEvolutionData.CreateEmpty();
                }
            }

            // Check for gap between our last saved calculation date (if any) and the required Refresh date
            // (All the evolution results should contain the same end date so use any, here the LiabilityEvolution, to get the last date)
            var lastKnownAt = evolutionResults.IsEmpty() ? (DateTime?)null : evolutionResults.LiabilityEvolution.Max(x => x.Key);
            var startFrom = evolutionResults.IsEmpty() ? AnchorDate : lastKnownAt.Value.AddDays(1);

            if (startFrom <= refreshDate)
            {
                var loadLiabilityAssumptionEvolutionTask = new Task<IDictionary<DateTime, IDictionary<AssumptionType, double>>>(() => new LiabilityAssumptionEvolutionService(basesDataManager).Calculate(startFrom, refreshDate));
                // Liability evolution requires pension increases from the past and we don't cache these
                // We could, but penion increase evolution is pretty fast so keeping it simple and just recalculating full evolution from anchor date
                var loadPensionIncreaseEvolutionTask = new Task<IDictionary<DateTime, IDictionary<int, double>>>(() => new PensionIncreaseEvolutionService(basesDataManager).Calculate(AnchorDate, refreshDate));

                var ldiEvolutionData = scheme.Calculated.LDIEvolutionData == null
                                        ? null
                                        : scheme.Calculated.LDIEvolutionData.OrderBy(x => x.Date).ToDictionary(x => x.Date);


                var assetEvolutionService = new AssetEvolutionFactory().GetAssetEvolutionService(scheme.Data);
                if (lastKnownAt.HasValue)
                {
                    assetEvolutionService.SetLastKnownPosition(lastKnownAt.Value, evolutionResults.AssetEvolution[lastKnownAt.Value]);
                }
                var assetEvolutionCalculateTask = new Task<IEnumerable<AssetEvolutionItem>>(() =>
                    assetEvolutionService.Calculate(startFrom, refreshDate, portfolioManager, ldiEvolutionData));

                loadLiabilityAssumptionEvolutionTask.Start();
                loadPensionIncreaseEvolutionTask.Start();

                assetEvolutionCalculateTask.Start();

                var liabilityAssumptionEvolution = loadLiabilityAssumptionEvolutionTask.Result;
                var pensionIncreasesEvolution = loadPensionIncreaseEvolutionTask.Result;

                var liabilityEvolution = new LiabilityEvolutionService()
                    .Calculate(startFrom, refreshDate, basesDataManager, liabilityAssumptionEvolution, pensionIncreasesEvolution)
                        .OrderBy(x => x.Date)
                        .ToDictionary(x => x.Date);

                var assetEvolution = assetEvolutionCalculateTask.Result
                                    .OrderBy(x => x.Date)
                                    .ToDictionary(x => x.Date);

                evolutionResults.LiabilityEvolution = evolutionResults.LiabilityEvolution.Concat(liabilityEvolution).ToDictionary(x => x.Key, x => x.Value);
                evolutionResults.AssetEvolution = evolutionResults.AssetEvolution.Concat(assetEvolution).ToDictionary(x => x.Key, x => x.Value);

                // The experience calcs work on data at previous effective dates
                // Rather than faffing about working out the correct data to pass in when running from a non-day-zero point just pass everything in and redo the calcs
                // from scratch - they are fast calcs so this shouldn't be a problem
                var experience = new LiabilityExperienceService().CalculateLiabilityStepChange(basesDataManager, evolutionResults.LiabilityEvolution);
                evolutionResults.LiabilityExperience = experience[LiabilityExperienceService.LiabilityExperienceType.Liab];
                evolutionResults.BuyinExperience = experience[LiabilityExperienceService.LiabilityExperienceType.Buyin];
                evolutionResults.AssetExperience = new AssetExperienceService().CalculateStepChange(portfolioManager, evolutionResults.AssetEvolution, ldiEvolutionData);
                
                //Expected deficit calcs must start from the anchor date every time, but are fast so no discernable performance loss from calculating from scratch every time
                evolutionResults.ExpectedDefit = new Lazy<IDictionary<DateTime, ExpectedDeficitData>>(() =>
                    new ExpectedDeficitService(scheme.Data)
                        .Calculate(
                            AnchorDate,
                            refreshDate,
                            basesDataManager.GetBasis(AnchorDate),
                            portfolioManager.GetPortfolio(AnchorDate),
                            evolutionResults));

                cache = cache ?? new BasisCache() { MasterBasisId = MasterBasisId };
                cache.BasisEvolutionData = serializer.SerializeObject<BasisEvolutionData>(evolutionResults);
                using (var db = dbScopeFactory.GetScope())
                {
                    db.GetRepository<BasisCache>().InsertOrUpdate(cache);
                }
            }

            ResetAssumptions();

            perfLog.InfoFormat("CachedBasis[{0}].Initialise(): {1} milliseconds", Name, stopwatch.ElapsedMilliseconds.ToString());
        }

        public void ResetAttributionPeriod()
        {
            SetAttributionPeriod(AnchorDate, refreshDate);
        }

        public void SetAttributionPeriod(DateTime start, DateTime end)
        {
            if (start < AnchorDate)
                start = AnchorDate;
            if (end < AnchorDate)
                end = refreshDate;

            AttributionStart = start;
            AttributionEnd = end;

            if (evolutionResults != null) // ie, initialised yet? 
            {
                cleanContext();

                ResetAssumptions();
            }
        }

        public void MarkAsStale()
        {
            stale = true;
            usgaapStale = true;
            varWaterFallStale = true;
        }

        #region master basis / basis queries

        public IEnumerable<DateTime> EffectiveDates
        {
            get
            {
                return basesDataManager.EffectiveDates;
            }
        }

        public IEnumerable<DateTime> GetAccountingPeriods(bool includeToAnalysisDate = true)
        {
            return new MasterBasisQueries().GetAccountingPeriods(refreshDate, mb, includeToAnalysisDate ? AttributionEnd : (DateTime?)null);
        }

        public double GetGiltYield()
        {
            return new GiltYieldService(basesDataManager.GetBasis(AttributionEnd)).GetGiltYield(AttributionEnd);
        }

        public IDictionary<AssumptionType, string> GetAssumptionLabels(DateTime date)
        {
            return basesDataManager.GetBasis(date).UnderlyingAssumptionIndices.Where(x => x.Value.IsVisible).ToDictionary(a => a.Key, a => a.Value.Name);
        }

        public IEnumerable<PensionIncreaseDefinition> GetPensionIncreaseDefinitions(DateTime date)
        {
            var b = basesDataManager.GetBasis(date);

            return b.UnderlyingPensionIncreaseIndices.Select(a => new PensionIncreaseDefinition(a.Key, a.Value.Label, b.PensionIncreases[a.Key], a.Value.IsFixed, a.Value.IsVisible));
        }

        public IDictionary<SimpleAssumptionType, KeyValuePair<string, double>> GetSimpleAssumptions()
        {
            return GetSimpleAssumptions(AttributionEnd);
        }

        public IDictionary<SimpleAssumptionType, KeyValuePair<string, double>> GetSimpleAssumptions(DateTime at)
        {
            return basesDataManager.GetBasis(at).SimpleAssumptions.Where(a => a.Value.IsVisible)
                .ToDictionary(a => a.Key, a => new KeyValuePair<string, double>(a.Value.Name, a.Value.Value));
        }

        public IEnumerable<AssetItem> GetAssetData()
        {
            var at = AttributionEnd;
            var pf = portfolioManager.GetPortfolio(at);

            var assetItems = pf.MarketAssets.Assets
                .Select(x => new AssetItem(x.Class.Type, x.Class.Category, x.Value));

            return assetItems;
        }

        public IEnumerable<AssetItem> GetAssetData(DateTime at)
        {
            var pf = portfolioManager.GetPortfolio(at);

            var assetItems = pf.AssetData.Assets
                .Select(x => new AssetItem(x.Class.Type, x.Class.Category, x.Value));

            return assetItems;
        }

        public BondYieldData GetBondYield()
        {
            var bondYield = basesDataManager.GetBasis(AttributionEnd).BondYield;

            if (bondYield == null)
                return null;

            bondYield.Value = new BondYieldService(basesDataManager.GetBasis(AttributionEnd)).GetGiltYield(AttributionEnd);

            return new BondYieldData
            {
                IndexName = bondYield.Index.Name,
                Label = bondYield.Label,
                Value = bondYield.Value
            };
        }

        #endregion

        #region client assumptions

        private MutableBeforeAfter<AssumptionData> ClientLiabilityAssumptions;
        private MutableBeforeAfter<IDictionary<AssetClassType, double>> ClientAssetAssumptions;
        private MutableBeforeAfter<IDictionary<int, double>> ClientPensionIncreases;
        private MutableBeforeAfter<RecoveryPlanAssumptions> ClientRecoveryAssumptions;

        #region getters

        public BeforeAfter<AssumptionData> GetClientLiabilityAssumptions()
        {
            return ClientLiabilityAssumptions;
        }
        public BeforeAfter<IDictionary<AssetClassType, double>> GetClientAssetAssumptions()
        {
            return ClientAssetAssumptions;
        }
        public BeforeAfter<IDictionary<int, double>> GetClientPensionIncreases()
        {
            return ClientPensionIncreases;
        }
        public BeforeAfter<RecoveryPlanAssumptions> GetClientRecoveryAssumptions()
        {
            return ClientRecoveryAssumptions;
        }

        #endregion

        #region setters

        public void SetClientLiabilityAssumptions(AssumptionData assumptions)
        {
            ClientLiabilityAssumptions.Set(assumptions);
            if (!ClientPensionIncreases.IsDirty)
            {
                // keep pension increases in line with assumptions
                ClientPensionIncreases = new MutableBeforeAfter<IDictionary<int, double>>(calculatePensionIncreases());
            }
            OnBasisChange(this, AssumptionChangeType.LiabilityAssumption);
        }
        public void SetClientAssetAssumptions(IDictionary<AssetClassType, double> assumptions)
        {
            ClientAssetAssumptions.Set(assumptions);
            OnBasisChange(this, AssumptionChangeType.AssetAssumption);
        }
        public void SetClientPensionIncreases(IDictionary<int, double> assumptions)
        {
            ClientPensionIncreases.Set(assumptions);
            OnBasisChange(this, AssumptionChangeType.PensionIncreasesAssumptions);
        }
        public void SetClientRecoveryAssumptions(RecoveryPlanAssumptions assumptions)
        {
            ClientRecoveryAssumptions.Set(assumptions);
            OnBasisChange(this, AssumptionChangeType.RecoveryPlan);
        }

        #endregion

        #region resetters

        public void ResetClientLiabilityAssumptions()
        {
            ClientLiabilityAssumptions.Reset();
            ClientPensionIncreases = new MutableBeforeAfter<IDictionary<int, double>>(calculatePensionIncreases());
            OnBasisChange(this, AssumptionChangeType.LiabilityAssumption);
        }
        public void ResetClientPensionIncreasesAssumptions()
        {
            //not same as other assumptions - a reset ties them back to the current state of the liability assumptions
            ClientPensionIncreases.Reset(); // reset so they are not dirty for the following calculate method
            ClientPensionIncreases = new MutableBeforeAfter<IDictionary<int, double>>(calculatePensionIncreases()); // now not dirty but linked
            OnBasisChange(this, AssumptionChangeType.PensionIncreasesAssumptions);
        }
        public void ResetClientAssetAssumptions()
        {
            ClientAssetAssumptions.Reset();
            OnBasisChange(this, AssumptionChangeType.AssetAssumption);
        }
        public void ResetClientRecoveryAssumptions()
        {
            ClientRecoveryAssumptions.Reset();
            OnBasisChange(this, AssumptionChangeType.RecoveryPlan);
        }

        #endregion

        public VaRInputs GetVaRInputs()
        {
            var VaRInputs = getResult().After.VaRInputs;

            return VaRInputs;
        }

        private void handleBasisChange(object sender, AssumptionChangeType e)
        {
            checkAnalysisResultsInitialised();

            // ie, re-run journey plan calcs
            analysisResults.Set(new BasisAnalysisService(scheme.ToContext()).Calculate(getContext(), BasisAnalysisService.CalculationMode.CalculateFromJourneyPlanBefore));

            usgaapStale = true;
        }

        private void handleSchemeInputChange(object sender, AssumptionChangeType e)
        {
            // defer re-running of journey plan calcs as this basis might not be active
            stale = true;
        }

        private void handleWaterfallChange(object sender, AssumptionChangeType e)
        {
            varWaterFallStale = true;
        }

        public void ResetAssumptions()
        {
            if (analysisResults != null)
                analysisResults.Reset();

            scheme.ResetAssumptions();

            resetAssumptions();
        }

        private void resetAssumptions()
        {
            var defaultAssumptions = getDefaultAssumptions(AttributionEnd);

            ClientPensionIncreases = new MutableBeforeAfter<IDictionary<int, double>>(defaultAssumptions.PensionIncreases);
            ClientRecoveryAssumptions = new MutableBeforeAfter<RecoveryPlanAssumptions>(defaultAssumptions.RecoveryPlanOptions);
            ClientAssetAssumptions = new MutableBeforeAfter<IDictionary<AssetClassType, double>>(defaultAssumptions.AssetAssumptions);
            ClientLiabilityAssumptions = new MutableBeforeAfter<AssumptionData>(new AssumptionData(
                defaultAssumptions.LiabilityAssumptions[AssumptionType.PreRetirementDiscountRate], defaultAssumptions.LiabilityAssumptions[AssumptionType.PostRetirementDiscountRate], defaultAssumptions.LiabilityAssumptions[AssumptionType.PensionDiscountRate], defaultAssumptions.LiabilityAssumptions[AssumptionType.SalaryIncrease], defaultAssumptions.LiabilityAssumptions[AssumptionType.InflationRetailPriceIndex], defaultAssumptions.LiabilityAssumptions[AssumptionType.InflationConsumerPriceIndex], defaultAssumptions.LifeExpectancy.PensionerLifeExpectancy, defaultAssumptions.LifeExpectancy.DeferredLifeExpectancy));
        }

        private IBasisAssumptions getDefaultAssumptions(DateTime at)
        {
            //liability assumptions            
            var b = basesDataManager.GetBasis(at);
            Func<AssumptionType, double> liabs = t => assumptionIndexEvolutionService.GetAssumptionIndexValue(t, at, b);
            var liabilityAssumptions = new AssumptionData(liabs(AssumptionType.PreRetirementDiscountRate), liabs(AssumptionType.PostRetirementDiscountRate), liabs(AssumptionType.PensionDiscountRate), liabs(AssumptionType.SalaryIncrease), liabs(AssumptionType.InflationRetailPriceIndex), liabs(AssumptionType.InflationConsumerPriceIndex), b.SimpleAssumptions[SimpleAssumptionType.LifeExpectancy65].Value, b.SimpleAssumptions[SimpleAssumptionType.LifeExpectancy65_45].Value);
            var lifeExpectancy = new LifeExpectancyAssumptions(liabilityAssumptions.PensionerLifeExpectancy, liabilityAssumptions.DeferredLifeExpectancy);
            var pensionIncreases = pensionIncreaseIndexEvolutionService.GetPensionIncreaseIndexValue(at, b);

            //asset assumptions
            var pf = portfolioManager.GetPortfolio(at);
            var assetAssumptions = new Dictionary<AssetClassType, double>(pf.AssetData.Assets.ToDictionary(x => x.Class.Type, x => x.DefaultReturn));
            foreach (var assetClass in AssetDefaults.DefaultAssetReturns.Keys.Where(key => !assetAssumptions.ContainsKey(key)))
                assetAssumptions.Add(assetClass, AssetDefaults.DefaultAssetReturns[assetClass]);

            //recovery plan
            var v = scheme.Data.ReturnOnAssets;
            var recoveryPlanAssumptions = new RecoveryPlanAssumptions(
                v,
                v,
                0,
                double.Parse(scheme.Data.NamedProperties[NamedPropertyGroupType.RecoveryPlan].Properties.Single(x => x.Key == NamedRecoveryPlanPropertyTypes.MaxLumpSum.ToString()).Value) * 1000000,
                double.Parse(scheme.Data.NamedProperties[NamedPropertyGroupType.RecoveryPlan].Properties.Single(x => x.Key == NamedRecoveryPlanPropertyTypes.AnnualIncs.ToString()).Value),
                scheme.Data.IsGrowthFixed,
                12,
                int.Parse(scheme.Data.NamedProperties[NamedPropertyGroupType.RecoveryPlan].Properties.Single(x => x.Key == NamedRecoveryPlanPropertyTypes.Length.ToString()).Value));

            return new BasisAssumptions(liabilityAssumptions.ToDictionary(), lifeExpectancy, pensionIncreases, assetAssumptions, recoveryPlanAssumptions);
        }

        private IDictionary<int, double> calculatePensionIncreases()
        {
            var b = basesDataManager.GetBasis(AttributionEnd);
            var pincs = new PensionIncreasesTrackerService().calculatePensionIncreases(b, AttributionEnd, ClientLiabilityAssumptions, ClientPensionIncreases);

            return pincs;
        }

        public void DecoupleClientPensionIncreasesFromLiabilityAssumptions()
        {
            var current = ClientPensionIncreases.After;
            SetClientPensionIncreases(current); // make it dirty to flag that it's decoupled
        }

        public void CoupleClientPensionIncreasesToLiabilityAssumptions()
        {
            ResetClientPensionIncreasesAssumptions(); // clean it so back end ignores client values - ie, coupled again
        }

        #endregion

        #region evolution data wrappers

        public IDictionary<DateTime, EvolutionData> Evolution
        {
            get
            {
                if (combinedEvolutionResults == null)
                {
                    checkInitialised();
                    combinedEvolutionResults =
                        evolutionResults.LiabilityEvolution.Values.Zip(
                            evolutionResults.AssetEvolution.Values,
                                (liab, asset) => new EvolutionData
                                    (
                                        liab.Date,
                                        liab.Act,
                                        liab.Def,
                                        liab.Pen,
                                        liab.Fut,
                                        liab.Liabs,
                                        liab.DefBuyIn,
                                        liab.PenBuyIn,
                                        liab.DiscPre,
                                        liab.DiscPost,
                                        liab.DiscPen,
                                        liab.SalInc,
                                        liab.RPI,
                                        liab.CPI,
                                        liab.Pinc1,
                                        liab.Pinc2,
                                        liab.Pinc3,
                                        liab.Pinc4,
                                        liab.Pinc5,
                                        liab.Pinc6,
                                        liab.ActDuration,
                                        liab.DefDuration,
                                        liab.PenDuration,
                                        liab.FutDuration,
                                        asset.EmployeeContributions,
                                        asset.EmployerContributions,
                                        asset.TotalBenefits,
                                        asset.NetBenefits,
                                        asset.NetIncome,
                                        asset.Growth,
                                        asset.AssetValue
                                    ))
                        .ToDictionary(x => x.Date);
                }
                return combinedEvolutionResults;
            }
        }

        public IDictionary<DateTime, ExpectedDeficitData> ExpectedDeficit
        {
            get
            {
                checkInitialised();
                return evolutionResults.ExpectedDefit.Value;
            }
        }

        #endregion

        #region analysis data getters

        public LiabilityData GetLiabilityData(AttributionEndType at)
        {
            if (at != AttributionEndType.RefreshDate)
                throw new NotImplementedException("No framework to currently allow results to be retrieved other that at Analysis date or Refresh date");

            var result = getRefreshDateResult();

            return new LiabilityData(result.UserActLiab, result.UserServiceCost, result.UserDefLiab, result.UserPenLiab);
        }

        //TODO must be a way to make this generic so each method isn't repeating the same pattern

        public BeforeAfter<AbfData> GetAbfData()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<AbfData>(cache.Before.ABF);
            if (cacheIsDirty())
            {
                result.Set(cache.After.ABF);
            }
            return result;
        }

        public BeforeAfter<BalanceData> GetBalanceSheetEstimate()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<BalanceData>(cache.Before.BalanceSheet);
            if (cacheIsDirty())
            {
                result.Set(cache.After.BalanceSheet);
            }
            return result;
        }

        public BeforeAfter<BuyinData> GetBuyinInfo()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<BuyinData>(cache.Before.Insurance);
            if (cacheIsDirty())
            {
                result.Set(cache.After.Insurance);
            }
            return result;
        }

        public BeforeAfter<double> GetInsurancePointChangeImpact()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<double>(cache.Before.InsurancePointChangeImpact);
            if (cacheIsDirty())
            {
                result.Set(cache.After.InsurancePointChangeImpact);
            }
            return result;
        }

        public BeforeAfter<List<LiabilityData>> GetCashFlowData()
        {
            Func<IDictionary<MemberStatus, double>, LiabilityData> convert = cf => new LiabilityData(cf[MemberStatus.ActivePast], cf[MemberStatus.ActiveFuture], cf[MemberStatus.Deferred], cf[MemberStatus.Pensioner]);
            var cache = getResult();
            var result = new MutableBeforeAfter<List<LiabilityData>>(cache.Before.Cashflow.Select(x => convert(x.Value)).ToList());
            if (cacheIsDirty())
            {
                result.Set(cache.After.Cashflow.Select(x => convert(x.Value)).ToList());
            }
            return result;
        }

        public List<LiabilityData> GetCashFlowData(AttributionEndType at)
        {
            if (at != AttributionEndType.RefreshDate)
                throw new NotImplementedException("No framework to currently allow results to be retrieved other that at Analysis date or Refresh date");

            return getRefreshDateResult().Cashflow.Select(cf => new LiabilityData(cf.Value[MemberStatus.ActivePast], cf.Value[MemberStatus.ActiveFuture], cf.Value[MemberStatus.Deferred], cf.Value[MemberStatus.Pensioner])).ToList();
        }

        public BeforeAfter<EtvData> GetEtvData()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<EtvData>(cache.Before.ETV);
            if (cacheIsDirty())
            {
                result.Set(cache.After.ETV);
            }
            return result;
        }
        public BeforeAfter<FroData> GetFroData()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<FroData>(cache.Before.FRO);
            if (cacheIsDirty())
            {
                result.Set(cache.After.FRO);
            }
            return result;
        }
        public BeforeAfter<EFroData> GetEFroData()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<EFroData>(cache.Before.EFRO);
            if (cacheIsDirty())
            {
                result.Set(cache.After.EFRO);
            }
            return result;
        }

        public BeforeAfter<FundingData> GetFundingLevelData()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<FundingData>(cache.Before.FundingLevel);
            if (cacheIsDirty())
            {
                result.Set(cache.After.FundingLevel);
            }
            return result;
        }

        public BeforeAfter<FutureBenefitsData> GetFutureBenefits()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<FutureBenefitsData>(cache.Before.FutureBenefits);
            if (cacheIsDirty())
            {
                result.Set(cache.After.FutureBenefits);
            }
            return result;
        }

        public BeforeAfter<InvestmentStrategyData> GetInvestmentStrategyData()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<InvestmentStrategyData>(cache.Before.InvestmentStrategy);
            if (cacheIsDirty())
            {
                result.Set(cache.After.InvestmentStrategy);
            }
            return result;
        }

        public BeforeAfter<HedgeBreakdownData> GetHedgingBreakdown()
        {
            var cache = getResult();
            var before = cache.Before.Hedging == null ? null : cache.Before.Hedging.Breakdown;
            var result = new MutableBeforeAfter<HedgeBreakdownData>(before);
            if (cacheIsDirty() && before != null)
            {
                result.Set(cache.After.Hedging.Breakdown);
            }
            return result;
        }

        public InvestmentStrategyData GetInvestmentStrategyData(AttributionEndType at)
        {
            if (at != AttributionEndType.RefreshDate)
                throw new NotImplementedException("No framework to currently allow results to be retrieved other that at Analysis date or Refresh date");

            return getRefreshDateResult().InvestmentStrategy;
        }

        public HedgeData GetHedgingDataAtRefreshDate()
        {
            return getRefreshDateResult().Hedging;
        }

        /// <summary>Get built-in hedge for assets at baseline.</summary>
        /// <remarks>Although this is Asset data which is normally at the scheme level, the hedge data is calculated against the currently active basis.</remarks>
        public HedgeBreakdownData GetHedgingDataAtAnchorDate()
        {
            var hedgeBreakdown =
                new AssetHedgeCalculator(scheme.ToContext())
                    .CalculateExistingHedge(AnchorDate, basesDataManager.GetBasis(AnchorDate), new PortfolioManager(scheme.Data, basesDataManager));

            return hedgeBreakdown;
        }

        public JourneyPlanResult GetJourneyPlan()
        {
            var cache = getResult();

            var before = new BeforeAfter<IList<LensData>>
                (
                cache.Before.JourneyPlanBefore.Values.Select(x => new LensData(x.Liabilities, x.Assets)).ToList(),
                cache.After.JourneyPlanBefore.Values.Select(x => new LensData(x.Liabilities, x.Assets)).ToList()
                );
            var after = new BeforeAfter<IList<LensData>>
                (
                cache.Before.JourneyPlanAfter.Values.Select(x => new LensData(x.Liabilities, x.Assets)).ToList(),
                cache.After.JourneyPlanAfter.Values.Select(x => new LensData(x.Liabilities, x.Assets)).ToList()
                );
            return new JourneyPlanResult { BeforePlan = before, AfterPlan = after };
        }

        public BeforeAfter<PieData> GetPieData()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<PieData>(cache.Before.PIE);
            if (cacheIsDirty())
            {
                result.Set(cache.After.PIE);
            }
            return result;
        }

        public BeforeAfter<IList<double>> GetRecoveryPlan()
        {
            var cache = getResult();
            var result = new BeforeAfter<IList<double>>
                (
                cache.After.RecoveryPlanPayments.Select(x => x.Value.Before).ToList(),
                cache.After.RecoveryPlanPayments.Select(x => x.Value.After).ToList()
                );
            return result;
        }

        public BeforeAfter<AttributionData> GetAttributionData()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<AttributionData>(cache.Before.Attribution);
            if (cacheIsDirty())
            {
                result.Set(cache.After.Attribution);
            }
            return result;
        }

        public BeforeAfter<SurplusAnalysisData> GetSurplusAnalysisData()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<SurplusAnalysisData>(cache.Before.SurplusAnalysis);
            if (cacheIsDirty())
            {
                result.Set(cache.After.SurplusAnalysis);
            }
            return result;
        }

        public BeforeAfter<IList<VarFunnelData>> GetVarFunnel()
        {
            var cache = getResult();
            var result = new MutableBeforeAfter<IList<VarFunnelData>>(cache.Before.VarFunnel);
            if (cacheIsDirty())
            {
                result.Set(cache.After.VarFunnel);
            }
            return result;
        }

        public BeforeAfter<WaterfallData> GetVarWaterfall()
        {
            var cache = getResult();

            var result = new MutableBeforeAfter<WaterfallData>(cache.Before.VarWaterfall);

            if (varWaterFallStale && cacheIsDirty()) //we're changing inputs with a dirty cache and should show before and after results
            {
                //We need to calculate with the defaults first in order to get the relevant 'before' value for the new waterfall input selection

                analysisResults.Set(new BasisAnalysisService(scheme.ToAdjustedDefaultContext()).Calculate(getDefaultContext(), BasisAnalysisService.CalculationMode.CalculateFromVarWaterFall));
                result = new MutableBeforeAfter<WaterfallData>(analysisResults.After.VarWaterfall);

                analysisResults.Set(new BasisAnalysisService(scheme.ToContext()).Calculate(getContext(), BasisAnalysisService.CalculationMode.CalculateFromVarWaterFall));
                result.Set(analysisResults.After.VarWaterfall);

                return result;
            }
            else if (cacheIsDirty())
            {
                result.Set(cache.After.VarWaterfall);
            }
            else if (varWaterFallStale) //we've only changed the waterfall inputs and the cache is clean so only show before results
            {
                analysisResults.Set(new BasisAnalysisService(scheme.ToContext()).Calculate(getContext(), BasisAnalysisService.CalculationMode.CalculateFromVarWaterFall));
                return new MutableBeforeAfter<WaterfallData>(analysisResults.After.VarWaterfall);
            }

            return result; //we're just getting the waterfall data which has potentially been updated by a CalculateEverything in getResult();
        }

        public BeforeAfter<IAS19Disclosure.IAS19PAndLForecastData> GetIASData()
        {
            if (BasisType != DomainShared.BasisType.Accounting)
                return null;

            var cache = getResult();
            var result = new MutableBeforeAfter<IAS19Disclosure.IAS19PAndLForecastData>(cache.Before.IAS19);
            if (cacheIsDirty())
            {
                result.Set(cache.After.IAS19);
            }
            return result;
        }

        public BeforeAfter<IAS19Disclosure.IAS19PAndLForecastData> GetFRSData()
        {
            if (BasisType != DomainShared.BasisType.Accounting)
                return null;

            var cache = getResult();
            var result = new MutableBeforeAfter<IAS19Disclosure.IAS19PAndLForecastData>(cache.Before.FRS17);
            if (cacheIsDirty())
            {
                result.Set(cache.After.FRS17);
            }
            return result;
        }

        public WeightedReturnParameters GetWeightedReturnParameters()
        {
            return getResult().After.WeightedReturns;
        }

        #endregion

        #region analysis period independent methods

        public BeforeAfter<IAS19Disclosure> GetIAS19Disclosure(DateTime start, DateTime end, bool useClientAssumptions)
        {
            if (BasisType != DomainShared.BasisType.Accounting)
                return null;

            var context = getContextAt(end, start);

            if (useClientAssumptions)
                context = context.AdjustAssumptions(
                    assumptions: ClientLiabilityAssumptions.After.ToDictionary(),
                    pensionIncreases: ClientPensionIncreases.After,
                    lifeExpectancy: new LifeExpectancyAssumptions(ClientLiabilityAssumptions.After.PensionerLifeExpectancy, ClientLiabilityAssumptions.After.DeferredLifeExpectancy),
                    assetAssumptions: ClientAssetAssumptions.After);

            var schemeCtx = scheme.ToContext();

            context.Results = new BasisAnalysisService(schemeCtx).Calculate(context, BasisAnalysisService.CalculationMode.CalculateEverything);

            var before = new IAS19Calculator(schemeCtx, context, start).Get(IAS19Calculator.CalculationMode.SummaryOnly);
            var after = new IAS19Calculator(schemeCtx, context, end, start).Get(IAS19Calculator.CalculationMode.Full, !useClientAssumptions);

            return new BeforeAfter<IAS19Disclosure>(before, after);
        }

        public BeforeAfter<FRS17Disclosure> GetFRS17Disclosure(DateTime start, DateTime end, bool useClientAssumptions)
        {
            if (BasisType != DomainShared.BasisType.Accounting)
                return null;

            var context = getContextAt(end, start);

            if (useClientAssumptions)
                context = context.AdjustAssumptions(
                    assumptions: ClientLiabilityAssumptions.After.ToDictionary(),
                    pensionIncreases: ClientPensionIncreases.After,
                    lifeExpectancy: new LifeExpectancyAssumptions(ClientLiabilityAssumptions.After.PensionerLifeExpectancy, ClientLiabilityAssumptions.After.DeferredLifeExpectancy),
                    assetAssumptions: ClientAssetAssumptions.After);

            var schemeCtx = scheme.ToContext();

            context.Results = new BasisAnalysisService(schemeCtx).Calculate(context, BasisAnalysisService.CalculationMode.CalculateEverything);

            var frs17calculator = new FRS17Calculator(schemeCtx, context, basesDataManager, portfolioManager, start, end);

            var before = frs17calculator.GetStartPositionSummary();
            var after = frs17calculator.Get(!useClientAssumptions);

            return new BeforeAfter<FRS17Disclosure>(before, after);
        }

        public BeforeAfter<USGAAPDisclosure.NetPeriodicPensionCostForecastData> GetUSGAAPData()
        {
            if (BasisType != DomainShared.BasisType.Accounting)
                return null;

            if (!scheme.Data.AccountingUSGAAPVisible)
                return null;

            if (!usgaapStale)
                return usgaapForecast;

            //TODO still using full calculator to get the P&L forecast - is this necessary?

            var ctx = getContext();

            var schemeCtx = scheme.ToContext();

            var usgaapInit = new USGAAPInitCalculator(schemeCtx, ctx, mb, portfolioManager).Calculate(ctx.StartDate, ctx.AnalysisDate);

            var f = new USGAAPCalculator(schemeCtx, usgaapInit, evolutionResults, basesDataManager, portfolioManager, ctx.Results, ClientAssetAssumptions.After).Get(getContext(), true)
                .NetPeriodPensionCostForecast;

            var forecast = new USGAAPDisclosure.NetPeriodicPensionCostForecastData(-f.ServiceCost, -f.InterestCost, -f.ExpectedReturn, -f.AmortisationOfPriorService, -f.AmortisationOfActuarial, -f.TotalNetPeriodicPension);

            if (usgaapForecast == null)
                usgaapForecast = new MutableBeforeAfter<USGAAPDisclosure.NetPeriodicPensionCostForecastData>(forecast);

            usgaapForecast.Set(forecast);

            usgaapStale = false;

            return usgaapForecast;
        }

        public BeforeAfter<USGAAPDisclosure> GetUSGAAPDisclosure(DateTime start, DateTime end, bool useClientAssumptions)
        {
            if (BasisType != DomainShared.BasisType.Accounting)
                return null;

            var context = getContextAt(end, start);

            var schemeCtx = scheme.ToContext();

            if (useClientAssumptions)
                context = context.AdjustAssumptions(
                    assumptions: ClientLiabilityAssumptions.After.ToDictionary(),
                    pensionIncreases: ClientPensionIncreases.After,
                    lifeExpectancy: new LifeExpectancyAssumptions(ClientLiabilityAssumptions.After.PensionerLifeExpectancy, ClientLiabilityAssumptions.After.DeferredLifeExpectancy),
                    assetAssumptions: ClientAssetAssumptions.After);

            context.Results = new BasisAnalysisService(schemeCtx).Calculate(context, BasisAnalysisService.CalculationMode.CalculateEverything);

            var usgaapInitAt = new USGAAPInitCalculator(schemeCtx, context, mb, portfolioManager).Calculate(start, end);

            var usgaapCalculator = new USGAAPCalculator(schemeCtx, usgaapInitAt, evolutionResults, basesDataManager, portfolioManager, analysisResults.After,
                useClientAssumptions ? ClientAssetAssumptions.After : null);

            var before = usgaapCalculator.GetStartPositionSummary(start, end);
            var after = usgaapCalculator.Get(context, useClientAssumptions);

            return new BeforeAfter<USGAAPDisclosure>(before, after);
        }

        #endregion

        #region context helpers

        private void cleanContext()
        {
            analysisResults = null;
            usgaapForecast = null;
            usgaapStale = true;
        }

        private BasisContext getContext()
        {
            return getContext(GetAssumptions());
        }

        private BasisContext getDefaultContext()
        {
            return getContext(getDefaultAssumptions(AttributionEnd));
        }

        private BasisContext getContext(IBasisAssumptions assumptions)
        {
            var currentBasis = basesDataManager.GetBasis(AttributionEnd);
            var pf = new PortfolioManager(scheme.Data, basesDataManager).GetPortfolio(AttributionEnd);

            return new BasisContext
                (
                    AttributionStart,
                    AttributionEnd,
                    currentBasis.EffectiveDate,
                    currentBasis,
                    pf,
                    analysisResults == null ? null : getAnalysisResultsClone(),
                    evolutionResults,
                    assumptions
                );
        }

        private IBasisContext getContextAt(DateTime at, DateTime periodStart)
        {
            var b = basesDataManager.GetBasis(at);
            var pf = new PortfolioManager(scheme.Data, basesDataManager).GetPortfolio(at);
            var assumptions = getDefaultAssumptions(at);

            return new BasisContext
                (
                    periodStart,
                    at,
                    b.EffectiveDate,
                    b,
                    pf,
                    null,
                    evolutionResults,
                    assumptions
                );
        }

        private BasisAnalysisResults getResultAt(DateTime at, DateTime periodStart)
        {
            var context = getContextAt(at, periodStart);

            var schemeCtx = scheme.ToContext(CachedPensionScheme.ToContextMode.Default);

            var results = new BasisAnalysisService(schemeCtx).Calculate(context, BasisAnalysisService.CalculationMode.CalculateEverything);

            return results;
        }

        public IBasisAssumptions GetAssumptions()
        {
            if (ClientLiabilityAssumptions == null) //ie, assumptions haven't been initialised yet
                resetAssumptions();

            return new BasisAssumptions
                (
                    ClientLiabilityAssumptions.After.ToDictionary(),
                    new LifeExpectancyAssumptions(ClientLiabilityAssumptions.After.PensionerLifeExpectancy, ClientLiabilityAssumptions.After.DeferredLifeExpectancy),
                    ClientPensionIncreases.After,
                    ClientAssetAssumptions.After,
                    ClientRecoveryAssumptions.After
                );
        }

        private void checkAnalysisResultsInitialised()
        {
            if (analysisResults == null)
            {
                checkInitialised();

                var defaultAssumptions = getDefaultAssumptions(AttributionEnd);

                var defaultSchemeContext = scheme.ToContext(CachedPensionScheme.ToContextMode.Default);

                // get the before state
                analysisResults = new MutableBeforeAfter<BasisAnalysisResults>(new BasisAnalysisService(defaultSchemeContext).Calculate(getContext(defaultAssumptions), BasisAnalysisService.CalculationMode.CalculateEverything));
            }
        }

        private BeforeAfter<BasisAnalysisResults> getResult()
        {
            checkAnalysisResultsInitialised();

            if (stale)
            {
                // ie, scheme input has changed while this basis was not active, so re-run recovery plan and journey plan after calcs
                analysisResults.Set(new BasisAnalysisService(scheme.ToContext()).Calculate(getContext(), BasisAnalysisService.CalculationMode.CalculateEverything));

                stale = false;
            }

            return analysisResults;
        }

        private BasisAnalysisResults getRefreshDateResult()
        {
            if (refreshDateResults == null)
            {
                checkInitialised();
                refreshDateResults = getResultAt(refreshDate, AnchorDate);
            }

            return refreshDateResults;
        }

        private bool cacheIsDirty()
        {
            //too complicated to check actually on the analysisResults instance because you would need a dependency matrix implemented
            //ie, if I reset liability assumptions then how do I know if Funding Level is dirty or not without checking all other inputs
            //therefore going to say the results are dirty if any of the assumptions are dirty
            return
                ClientLiabilityAssumptions.IsDirty ||
                ClientPensionIncreases.IsDirty ||
                ClientAssetAssumptions.IsDirty ||
                ClientRecoveryAssumptions.IsDirty ||
                scheme.IsAnalysisAssumptionsDirty;
        }

        private BasisAnalysisResults getAnalysisResultsClone()
        {
            //TODO yuk yuk YUK!! But there's a reference somewhere to the context results which means using the .Set() on analysisResults is also updating the Before. 
            //Fix this, else it's just going to cause another developer a wasted morning of annoyance!
            return new BasisAnalysisResults
            {
                ABF = analysisResults.After.ABF,
                AdjustedCashflow = analysisResults.After.AdjustedCashflow,
                AssetsAtSED = analysisResults.After.AssetsAtSED,
                Attribution = analysisResults.After.Attribution,
                BalanceSheet = analysisResults.After.BalanceSheet,
                BuyInAtAnalysisDate = analysisResults.After.BuyInAtAnalysisDate,
                BuyinCashflow = analysisResults.After.BuyinCashflow,
                Cashflow = analysisResults.After.Cashflow,
                ETV = analysisResults.After.ETV,
                FRO = analysisResults.After.FRO,
                FRS17 = analysisResults.After.FRS17,
                FundingLevel = analysisResults.After.FundingLevel,
                FutureBenefits = analysisResults.After.FutureBenefits,
                IAS19 = analysisResults.After.IAS19,
                Insurance = analysisResults.After.Insurance,
                InvestmentStrategy = analysisResults.After.InvestmentStrategy,
                Hedging = analysisResults.After.Hedging,
                JourneyPlanAfter = analysisResults.After.JourneyPlanAfter,
                JourneyPlanBefore = analysisResults.After.JourneyPlanBefore,
                JP2InvGrowth = analysisResults.After.JP2InvGrowth,
                JPInvGrowth = analysisResults.After.JPInvGrowth,
                PIEInit = analysisResults.After.PIEInit,
                PIE = analysisResults.After.PIE,
                ProfitLoss = analysisResults.After.ProfitLoss,
                RecoveryPlanPaymentRequired = analysisResults.After.RecoveryPlanPaymentRequired,
                RecoveryPlanPayments = analysisResults.After.RecoveryPlanPayments,
                StickyResults = analysisResults.After.StickyResults,
                SurplusAnalysis = analysisResults.After.SurplusAnalysis,
                UserActDuration = analysisResults.After.UserActDuration,
                UserActLiab = analysisResults.After.UserActLiab,
                UserDefBuyIn = analysisResults.After.UserDefBuyIn,
                UserDefDuration = analysisResults.After.UserDefDuration,
                UserDefLiab = analysisResults.After.UserDefLiab,
                UserPenBuyIn = analysisResults.After.UserPenBuyIn,
                UserPenDuration = analysisResults.After.UserPenDuration,
                UserPenLiab = analysisResults.After.UserPenLiab,
                UserServiceCost = analysisResults.After.UserServiceCost,
                UserActLiabAfter = analysisResults.After.UserActLiabAfter,
                UserDefLiabAfter = analysisResults.After.UserDefLiabAfter,
                UserPenLiabAfter = analysisResults.After.UserPenLiabAfter,
                VarFunnel = analysisResults.After.VarFunnel,
                VarWaterfall = analysisResults.After.VarWaterfall,
                WeightedReturns = analysisResults.After.WeightedReturns,
                VaRInputs = analysisResults.After.VaRInputs
            };
        }

        #endregion

        /// <summary>
        /// Produce an output of the complete state of this basis. 
        /// </summary>
        /// <returns></returns>
        public IBasisContext ToContext(ToContextMode mode = ToContextMode.AsIs)
        {
            var start = AttributionStart;
            var end = AttributionEnd;

            checkInitialised();

            var results = mode == ToContextMode.AsIs
                ? (analysisResults != null ? analysisResults.After : null)
                : getResult().After;

            var b = basesDataManager.GetBasis(end);
            var pf = portfolioManager.GetPortfolio(end);

            var context = new BasisContext(
                start,
                end,
                b.EffectiveDate,
                b,
                pf,
                results,
                evolutionResults,
                GetAssumptions());

            return context;
        }
    }
}
