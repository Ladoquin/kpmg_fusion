﻿using FlightDeck.DataAccess;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using log4net;
using System;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Security.Principal;
using System.Xml.Linq;
using System.Globalization;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.Services
{
    public class CurveImportDetailService : ICurveImportDetailService
    {
        public void ImportCurves(XDocument doc)
        {
            var dataImportService = new DataImportService();

            dataImportService.ImportCurveData(doc);
        }

        public CurveImportDetail GetLatest()
        {
            using (var db = new DbInteractionScope())
            {
                return ((ICurveImportDetailRepository)db.GetRepository<CurveImportDetail>()).GetLatest();
            }
        }

        public CurveImportDetail Save(XDocument xdoc, IPrincipal importedBy)
        {
            var extract = ExtractImportDetails(xdoc);

            var importDetail = new CurveImportDetail //todo: we only need one type of import detail? i.e. merge curve and index details.
            {
                ImportedByUser = importedBy.Identity.Name,
                ImportedOnDate = DateTime.Now,
                ProducedByUser = extract["ProducedByUser"] as string,
                ProducedOnDate = extract["ProducedOnDate"] as DateTime?,
                Spreadsheet = extract["Spreadsheet"] as string,
                Comment = extract.ContainsKey("Comment") ? extract["Comment"] as string : null
            };

            using (var db = new DbInteractionScope())
            {
                ((ICurveImportDetailRepository)db.GetRepository<CurveImportDetail>()).InsertOrUpdate(importDetail);
            }

            return importDetail;
        }

        private Dictionary<string, object> ExtractImportDetails(XDocument schemeDataXml)
        {
            var extract = new Dictionary<string, object>(10);

            Func<XElement, bool> IsBlank = (element) => element == null || element.Value.Trim() == String.Empty;

            // ProducedOnDate
            var dateElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Date");
            var timeElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Time");

            if (!IsBlank(dateElement) && IsBlank(timeElement))
            {
                DateTime d;
                if (DateTime.TryParse(dateElement.Value + ' ' + timeElement.Value, out d))
                    extract["ProducedOnDate"] = DateTime.Parse(dateElement.Value + ' ' + timeElement.Value, new CultureInfo("en-GB").DateTimeFormat);
            }
            if (!extract.ContainsKey("ProducedOnDate"))
                extract["ProducedOnDate"] = DateTime.Now.ToString(new CultureInfo("en-GB").DateTimeFormat);

            // ProducedByUser
            var userElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/UserName");
            if (!IsBlank(userElement)) extract["ProducedByUser"] = userElement.Value;

            // DataComment
            var commentElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/DataComment");
            if (!IsBlank(commentElement)) extract["Comment"] = commentElement.Value;

            // Spreadsheet
            var spreadsheetElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Spreadsheet");
            if (!IsBlank(spreadsheetElement)) extract["Spreadsheet"] = spreadsheetElement.Value;

            return extract;
        }
    }
}
