﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;

    internal interface ICachedPensionScheme : IPensionScheme
    {
        event InputChangeHandler OnInputChange;
        event InputChangeHandler OnWaterfallChange;
        ISchemeData Data { get; }
        ISchemeResults Calculated { get; }
        bool IsAnalysisAssumptionsDirty { get; }
        string DefaultBasisName { get; }
        ISchemeContext ToContext(CachedPensionScheme.ToContextMode contextMode = CachedPensionScheme.ToContextMode.WithClientAssumptions);
        ISchemeContext ToAdjustedDefaultContext();
        MasterBasis GetMasterBasis(int masterbasisId);
    }
}
