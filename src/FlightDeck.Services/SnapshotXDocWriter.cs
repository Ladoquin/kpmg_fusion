﻿namespace FlightDeck.Services
{
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    
    public class SnapshotXDocWriter : ISnapshotWriter
    {
        public MemoryStream WriteSnapshot(PropertyTreeItem snapshot)
        {
            var xdoc = new XDocument(
                outputTreeItem(snapshot)
                );

            var stream = new MemoryStream();
            xdoc.Save(stream);
            stream.Position = 0;

            return stream;
        }

        private XElement outputTreeItem(PropertyTreeItem item)
        {
            var elements = new Dictionary<string, Func<XElement>>(10);

            if (item.Properties != null)
                foreach (var property in item.Properties)
                    elements.Add(property.Key, () => new XElement(getSafeKey(property.Key), property.Value));

            if (item.Children != null)
                foreach (var child in item.Children)
                    elements.Add(child.Key, () => outputTreeItem(child));

            return new XElement(getSafeKey(item.Key), elements.OrderBy(x => x.Key).Select(x => x.Value.Invoke()));
        }

        private string getSafeKey(string key)
        {
            var newKey = string.Empty;
            for (int i = 0; i < key.Length; i++)
                if (char.IsLetterOrDigit(key, i))
                    newKey += key.Substring(i, 1);

            return newKey.Replace(" ", "");
        }
    }
}
