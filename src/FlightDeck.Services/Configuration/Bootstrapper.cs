﻿namespace FlightDeck.Services.Configuration
{
    using FlightDeck.DataAccess;
    using System.Configuration;
    
    public static class Bootstrapper
    {
        private static bool _dataAccessConfigured;

        public static void Init()
        {
            if (!_dataAccessConfigured)
            {
                DbInteractionScope.Configure(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                if (ConfigurationManager.ConnectionStrings["Log"] != null)
                    LogDbInteractionScope.Configure(ConfigurationManager.ConnectionStrings["Log"].ConnectionString);
                _dataAccessConfigured = true;
            }
        }
    }
}
