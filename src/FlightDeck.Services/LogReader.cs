﻿namespace FlightDeck.Services
{
    using FlightDeck.DataAccess;
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;

    public class LogReader : ILogReader
    {
        private ILogDbInteractionScope _logDbInteractionScope;

        public ILogDbInteractionScope LogDbInteractionScope
        {
            private get
            {
                if (_logDbInteractionScope == null)
                    _logDbInteractionScope = new LogDbInteractionScope();
                return _logDbInteractionScope;
            }
            set
            {
                _logDbInteractionScope = value;
            }
        }

        public IEnumerable<Log> GetAll(DateTime date, string level = null)
        {
            if (level != null && level.Trim().Length == 0)
                level = null;

            var repo = LogDbInteractionScope.GetRepository<Log>() as ILogRepository;

            var logs = repo.GetAll(date, level) ?? new List<Log>();

            LogDbInteractionScope.Dispose();

            _logDbInteractionScope = null;

            return logs;
        }
    }
}
