﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    using FlightDeck.ServiceInterfaces;
    using System;
    
    internal interface ICachedBasis : IBasis
    {
        void SetAttributionPeriod(DateTime start, DateTime end);
        void ResetAttributionPeriod();
        IBasisContext ToContext(CachedBasis.ToContextMode mode = CachedBasis.ToContextMode.AsIs);
        IBasisAssumptions GetAssumptions();
        event InputChangeHandler OnBasisChange;
        void MarkAsStale();
    }
}
