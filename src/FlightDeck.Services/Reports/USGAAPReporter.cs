﻿namespace FlightDeck.Services.Reports
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.ServiceInterfaces.Reports;
    using FlightDeck.Services.Reports.ExcelTransformations;
    using System;
    using System.Collections.Generic;

    public class USGAAPReporter : AccountingReporterBase, IExcelReporter
    {
        public USGAAPReporter(string templatesFolder)
            : base(templatesFolder, "USGAAP template.xlsx")
        {
        }

        protected override ExcelTransformations.IExcelTransformation GetTextTransformation()
        {
            return new USGAAPToTextTransformation();
        }

        protected override Dictionary<string, object> GetReportCellMappings(IScheme scheme, DateTime start, DateTime end, bool useClientAssumptions)
        {
            var report = scheme.PensionScheme.CurrentBasis.GetUSGAAPDisclosure(start, end, useClientAssumptions);

            var reportData = new AccountingReportData<USGAAPDisclosure>(scheme.SchemeName, scheme.CurrencySymbol, start, end, report.Before, report.After);

            var cellDataMappings = mapDisclosureToCellLookup(reportData);

            return cellDataMappings;
        }

        private Dictionary<string, object> mapDisclosureToCellLookup(AccountingReportData<USGAAPDisclosure> data)
        {
            var curr = data.AtEnd;
            var prev = data.AtStart;

            var cellDataMappings = new Dictionary<string, object>
                {
                    // title
                    { "B4", data.SchemeName },
                    { "B5", data.End },
                    // assumptions 1
                    { "C9", percent(curr.Assumptions1.DiscountRate) },
                    { "D9", percent(prev.Assumptions1.DiscountRate) },
                    { "C10", percent(curr.Assumptions1.FutureSalaryGrowth) },
                    { "D10", percent(prev.Assumptions1.FutureSalaryGrowth) },
                    { "C11", percent(curr.Assumptions1.RPIInflation) },
                    { "D11", percent(prev.Assumptions1.RPIInflation) },
                    { "C12", percent(curr.Assumptions1.CpiInflation) },
                    { "D12", percent(prev.Assumptions1.CpiInflation) },
                    { "C13", percent(curr.Assumptions1.ExpectedRateOfReturnOnAssets.Value) },
                    { "D13", percent(prev.Assumptions1.ExpectedRateOfReturnOnAssets.Value) },
                    { "C15", curr.Assumptions1.MaleLifeExpectancy65 + " years" },
                    { "D15", prev.Assumptions1.MaleLifeExpectancy65 + " years" },
                    { "C16", curr.Assumptions1.MaleLifeExpectancy65_45 + " years" },
                    { "D16", prev.Assumptions1.MaleLifeExpectancy65_45 + " years" },
                    // assumptions 2
                    { "C20", percent(curr.Assumptions2.DiscountRate) },
                    { "C21", percent(curr.Assumptions2.ExpectedRateOfReturnOnAssets.Value) },
                    { "C22", percent(curr.Assumptions2.FutureSalaryGrowth) },
                    { "C23", percent(curr.Assumptions2.RPIInflation) },
                    { "C24", percent(curr.Assumptions2.CpiInflation) },
                    { "C26", curr.Assumptions2.MaleLifeExpectancy65 + " years" },
                    { "C27", curr.Assumptions2.MaleLifeExpectancy65_45 + " years" },
                    // amounts recognised
                    { "C32", figure(curr.BalanceSheetAmounts.ProjectedBenefitObligation) },
                    { "D32", figure(prev.BalanceSheetAmounts.ProjectedBenefitObligation) },
                    { "C33", figure(curr.BalanceSheetAmounts.FairValueOfSchemeAssets) },
                    { "D33", figure(prev.BalanceSheetAmounts.FairValueOfSchemeAssets) },
                    { "C34", figure(curr.BalanceSheetAmounts.FundedStatus) },
                    { "D34", figure(prev.BalanceSheetAmounts.FundedStatus) },
                    //  present value
                    { "C40", figure(curr.ChangesInPresentValue.OpeningProjectedObligation) },
                    { "C41", figure(curr.ChangesInPresentValue.ServiceCost) },
                    { "C42", figure(curr.ChangesInPresentValue.InterestCost) },
                    { "C43", figure(curr.ChangesInPresentValue.ActuarialLosses) },
                    { "C44", figure(curr.ChangesInPresentValue.SchemeAmendments) },
                    { "C45", figure(curr.ChangesInPresentValue.Contributions) },
                    { "C46", figure(curr.ChangesInPresentValue.BenefitsPaid) },
                    { "C47", figure(curr.ChangesInPresentValue.ClosingProjectedObligation) },
                    // fair value
                    { "C52", figure(curr.ChangesInFairValue.OpeningFairValue) },
                    { "C53", figure(curr.ChangesInFairValue.ExpectedReturn) },
                    { "C54", figure(curr.ChangesInFairValue.ActuarialLosses) },
                    { "C55", figure(curr.ChangesInFairValue.EmployerContributions) },
                    { "C56", figure(curr.ChangesInFairValue.MemberContributions) },
                    { "C57", figure(curr.ChangesInFairValue.BenefitsPaid) },
                    { "C58", figure(curr.ChangesInFairValue.ClosingPairValue) },
                    // accumulated
                    { "C62", figure(curr.AccumulatedBenefitObligation.Value) },
                    // estimated future
                    { "C65", figure(curr.EstimatedPayments.Year1) },
                    { "C66", figure(curr.EstimatedPayments.Year2) },
                    { "C67", figure(curr.EstimatedPayments.Year3) },
                    { "C68", figure(curr.EstimatedPayments.Year4) },
                    { "C69", figure(curr.EstimatedPayments.Year5) },
                    { "C70", figure(curr.EstimatedPayments.Year6to10) },
                    // net periodic
                    { "C75", figure(curr.NetPeriodicPensionCost.ServiceCost) },
                    { "C76", figure(curr.NetPeriodicPensionCost.InterestCost) },
                    { "C77", figure(curr.NetPeriodicPensionCost.ExpectedReturn) },
                    { "C78", figure(curr.NetPeriodicPensionCost.AmortisationOfPriorService) },
                    { "C79", figure(curr.NetPeriodicPensionCost.AmortisationOfActuarialLosses) },
                    { "C80", figure(curr.NetPeriodicPensionCost.TotalPeriodicPensionCost) },
                    // comprehensive income
                    { "C85", figure(curr.ComprehensiveIncome.ActurialLoss) },
                    { "C86", figure(curr.ComprehensiveIncome.PriorServiceCost) },
                    { "C87", figure(curr.ComprehensiveIncome.ActurialGain) },
                    { "C88", figure(curr.ComprehensiveIncome.PriorServiceCredit) },
                    { "C89", figure(curr.ComprehensiveIncome.TotalAmount) },
                    // amount recognised
                    { "C94", figure(curr.ComprehensiveAccumulatedIncome.OpeningAOCI.Value) },
                    { "C95", figure(curr.ComprehensiveAccumulatedIncome.ActuarialLoss.Value) },
                    { "C96", figure(curr.ComprehensiveAccumulatedIncome.PriorServiceCost.Value) },
                    { "C97", figure(curr.ComprehensiveAccumulatedIncome.Total) },
                    // net periodic forecast
                    { "C102", figure(curr.NetPeriodPensionCostForecast.ServiceCost) },
                    { "C103", figure(curr.NetPeriodPensionCostForecast.InterestCost) },
                    { "C104", figure(curr.NetPeriodPensionCostForecast.ExpectedReturn) },
                    { "C105", figure(curr.NetPeriodPensionCostForecast.AmortisationOfPriorService) },
                    { "C106", figure(curr.NetPeriodPensionCostForecast.AmortisationOfActuarial) },
                    { "C107", figure(curr.NetPeriodPensionCostForecast.TotalNetPeriodicPension) },
                    // dates
                    { "C8", data.End },
                    { "D8", data.Start },
                    { "C19", data.End },
                    { "C30", data.End },
                    { "D30", data.Start },
                    { "C38", data.End },
                    { "C50", data.End },
                    { "C60", data.End },
                    { "C73", data.End },
                    { "C83", data.End },
                    { "C92", data.End },
                    { "C100", string.Format("Year from {0}", data.End.ToString("dd MMM yyyy")) },
                    // currency denominations
                    { "C31", data.CurrencySymbol + "000" },
                    { "D31", data.CurrencySymbol + "000" },
                    { "C39", data.CurrencySymbol + "000" },
                    { "C51", data.CurrencySymbol + "000" },
                    { "C61", data.CurrencySymbol + "000" },
                    { "C74", data.CurrencySymbol + "000" },
                    { "C84", data.CurrencySymbol + "000" },
                    { "C93", data.CurrencySymbol + "000" },
                    { "C101", data.CurrencySymbol + "000" }
                };

            return cellDataMappings;
        }
    }
}
