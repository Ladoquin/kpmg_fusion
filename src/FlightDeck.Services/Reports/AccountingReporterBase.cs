﻿namespace FlightDeck.Services.Reports
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.Services.Reports.ExcelTransformations;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public abstract class AccountingReporterBase
    {
        protected readonly string _templatePath;

        protected double percent(double d) { return Math.Round(d, 4); }
        protected double figure(double d) { return Math.Round(d / 1000); }

        protected abstract Dictionary<string, object> GetReportCellMappings(IScheme scheme, DateTime start, DateTime end, bool useClientAssumptions);
        protected abstract IExcelTransformation GetTextTransformation();

        public AccountingReporterBase(string templatesFolder, string templateName)
        {
            _templatePath = Path.Combine(templatesFolder, templateName);
        }

        public MemoryStream Create(IScheme scheme, DateTime start, DateTime end, bool useClientAssumptions, AccountingReportOutputType outputType)
        {
            var cellDataMappings = GetReportCellMappings(scheme, start, end, useClientAssumptions);

            var ms = mapDataToExcel(cellDataMappings, scheme.AccountingDownloadsPassword, outputType);

            return ms;
        }

        private MemoryStream mapDataToExcel(Dictionary<string, object> cellDataMappings, string excelPassword, AccountingReportOutputType outputType)
        {
            var xl = new ExcelTemplatePopulater();
            xl.EncryptedExcelEditPassword = excelPassword;

            List<IExcelTransformation> transformations = null;
            if (outputType == AccountingReportOutputType.ToText)
                transformations = new List<IExcelTransformation> { GetTextTransformation() };

            var ms = xl.Populate(_templatePath, cellDataMappings, transformations);

            return ms;
        }
    }
}
