﻿namespace FlightDeck.Services.Reports
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.ServiceInterfaces.Reports;
    using FlightDeck.Services.Reports.ExcelTransformations;
    using System;
    using System.Collections.Generic;

    public class FRS17Reporter : AccountingReporterBase, IExcelReporter
    {
        public FRS17Reporter(string templatesFolder)
            : base(templatesFolder, "FRS17 template.xlsx")
        {
        }

        protected override IExcelTransformation GetTextTransformation()
        {
            return new FRS17ToTextTransformation();
        }

        protected override Dictionary<string, object> GetReportCellMappings(IScheme scheme, DateTime start, DateTime end, bool useClientAssumptions)
        {
            var report = scheme.PensionScheme.CurrentBasis.GetFRS17Disclosure(start, end, useClientAssumptions);

            var reportData = new AccountingReportData<FRS17Disclosure>(scheme.SchemeName, scheme.CurrencySymbol, start, end, report.Before, report.After);

            var cellDataMappings = mapDisclosureToCellLookup(reportData);

            return cellDataMappings;
        }

        private Dictionary<string, object> mapDisclosureToCellLookup(AccountingReportData<FRS17Disclosure> data)
        {
            var curr = data.AtEnd;
            var prev = data.AtStart;

            var cellDataMappings = new Dictionary<string, object>
                {
                    // title
                    { "B4", data.SchemeName },
                    { "B5", data.End },
                    // actuarial assumptions
                    { "C9", percent(curr.Assumptions.DiscountRate) },
                    { "D9", percent(prev.Assumptions.DiscountRate) },
                    { "C10", percent(curr.Assumptions.FutureSalaryGrowth) },
                    { "D10", percent(prev.Assumptions.FutureSalaryGrowth) },
                    { "C11", percent(curr.Assumptions.RPIInflation) },
                    { "D11", percent(prev.Assumptions.RPIInflation) },
                    { "C12", percent(curr.Assumptions.CpiInflation) },
                    { "D12", percent(prev.Assumptions.CpiInflation) },
                    { "C13", percent(curr.Assumptions.ExpectedRateOfReturnOnAssets.Value) },
                    { "D13", percent(prev.Assumptions.ExpectedRateOfReturnOnAssets.Value) },
                    { "C15", curr.Assumptions.MaleLifeExpectancy65 + " years" },
                    { "D15", prev.Assumptions.MaleLifeExpectancy65 + " years" },
                    { "C16", curr.Assumptions.MaleLifeExpectancy65_45 + " years" },
                    { "D16", prev.Assumptions.MaleLifeExpectancy65_45 + " years" },
                    // amounts recognised
                    { "C21", figure(curr.BalanceSheetAmounts.PresentValueSchemeLiabilities) },
                    { "D21", figure(prev.BalanceSheetAmounts.PresentValueSchemeLiabilities) },
                    { "C22", figure(curr.BalanceSheetAmounts.FairValueOfSchemeAssets) },
                    { "D22", figure(prev.BalanceSheetAmounts.FairValueOfSchemeAssets) },
                    { "C23", figure(curr.BalanceSheetAmounts.SchemeSurplus) },
                    { "D23", figure(prev.BalanceSheetAmounts.SchemeSurplus) },
                    // present value of scheme liabilities
                    { "C29", figure(curr.PresentValueSchemeLiabilities.OpeningDefinedBenefitObligation) },
                    { "C30", figure(curr.PresentValueSchemeLiabilities.CurrentServiceCost) },
                    { "C31", figure(curr.PresentValueSchemeLiabilities.InterestExpense)},
                    { "C32", figure(curr.PresentValueSchemeLiabilities.ActuarialLosses)},
                    { "C33", figure(curr.PresentValueSchemeLiabilities.Contributions)},
                    { "C34", figure(curr.PresentValueSchemeLiabilities.BenefitsPaid)},
                    { "C35", figure(curr.PresentValueSchemeLiabilities.ClosingDefinedBenefitObligation)},
                    // fair value of scheme assets
                    { "C40", figure(curr.FairValueSchemeAssetChanges.OpeningFairValueSchemeAssets) },
                    { "C41", figure(curr.FairValueSchemeAssetChanges.ExpectedReturn) },
                    { "C42", figure(curr.FairValueSchemeAssetChanges.ActurialGains) },
                    { "C43", figure(curr.FairValueSchemeAssetChanges.ContributionsEmployer) },
                    { "C44", figure(curr.FairValueSchemeAssetChanges.ContributionsMembers) },
                    { "C45", figure(curr.FairValueSchemeAssetChanges.BenefitsPaid) },
                    { "C46", figure(curr.FairValueSchemeAssetChanges.ClosingFairValueSchemeAssets) },
                    // changes in surplus
                    { "C51", figure(curr.SchemeSurplusChanges.SchemeSurplusBeginning) },
                    { "C53", figure(curr.SchemeSurplusChanges.CurrentServiceCost) },
                    { "C54", figure(curr.SchemeSurplusChanges.InterestCost) },
                    { "C55", figure(curr.SchemeSurplusChanges.ExpectedReturnOnSchemeAssets) },
                    { "C56", figure(curr.SchemeSurplusChanges.PAndLTotal) },
                    { "C58", figure(curr.SchemeSurplusChanges.ActuarialLossesOnLiabilities) },
                    { "C59", figure(curr.SchemeSurplusChanges.ActuarialLossesOnAssets) },
                    { "C60", figure(curr.SchemeSurplusChanges.STRGLTotal) },
                    { "C62", figure(curr.SchemeSurplusChanges.Contributions) },
                    { "C64", figure(curr.SchemeSurplusChanges.SchemeSurplusEnd) },
                    // p & l forecast
                    { "C70", figure(curr.PAndLForecast.CurrentServiceCost) },
                    { "C71", figure(curr.PAndLForecast.InterestCost) },
                    { "C72", figure(curr.PAndLForecast.ExpectedReturnOnSchemeAssets) },
                    { "C73", figure(curr.PAndLForecast.Total) },
                    // dates
                    { "C8", data.End },
                    { "D8", data.Start },
                    { "C19", data.End },
                    { "D19", data.Start },
                    { "C27", data.End },
                    { "C38", data.End },
                    { "C49", data.End },
                    { "C68", "Year from " + data.End.ToString("dd MMM yyyy") },
                    // currency denominations
                    { "C20", data.CurrencySymbol + "000" },
                    { "D20", data.CurrencySymbol + "000" },
                    { "C28", data.CurrencySymbol + "000" },
                    { "C39", data.CurrencySymbol + "000" },
                    { "C50", data.CurrencySymbol + "000" },
                    { "C69", data.CurrencySymbol + "000" }
                };

            return cellDataMappings;
        }
    }

}