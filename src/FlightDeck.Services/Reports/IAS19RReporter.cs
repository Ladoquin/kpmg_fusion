﻿namespace FlightDeck.Services.Reports
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.ServiceInterfaces.Reports;
    using FlightDeck.Services.Reports.ExcelTransformations;
    using System;
    using System.Collections.Generic;

    public class IAS19RExcelReporter : AccountingReporterBase, IExcelReporter
    {
        public IAS19RExcelReporter(string templatesFolder)
            : base(templatesFolder, "IAS19R template.xlsx")
        {
        }

        public IAS19RExcelReporter(string templatesFolder, string excelTemplate)
                    : base(templatesFolder, excelTemplate)
        {
        }

        protected override IExcelTransformation GetTextTransformation()
        {
            return new IAS19ToTextTransformation();
        }

        protected override Dictionary<string, object> GetReportCellMappings(IScheme scheme, DateTime start, DateTime end, bool useClientAssumptions)
        {
            var report = scheme.PensionScheme.CurrentBasis.GetIAS19Disclosure(start, end, useClientAssumptions);

            var reportData = new AccountingReportData<IAS19Disclosure>(scheme.SchemeName, scheme.CurrencySymbol, start, end, report.Before, report.After);

            var cellDataMappings = mapDisclosureToCellLookup(reportData);

            return cellDataMappings;
        }

        private Dictionary<string, object> mapDisclosureToCellLookup(AccountingReportData<IAS19Disclosure> data)
        {
            var curr = data.AtEnd;
            var prev = data.AtStart;

            var cellDataMappings = new Dictionary<string, object>
                {
                    // title
                    { "B4", data.SchemeName },
                    { "B5", data.End },
                    // actuarial assumptions
                    { "C9", percent(curr.Assumptions.DiscountRate) },
                    { "D9", percent(prev.Assumptions.DiscountRate) },
                    { "C10", percent(curr.Assumptions.FutureSalaryGrowth) },
                    { "D10", percent(prev.Assumptions.FutureSalaryGrowth) },
                    { "C11", percent(curr.Assumptions.RPIInflation) },
                    { "D11", percent(prev.Assumptions.RPIInflation) },
                    { "C12", percent(curr.Assumptions.CpiInflation) },
                    { "D12", percent(prev.Assumptions.CpiInflation) },
                    { "C14", curr.Assumptions.MaleLifeExpectancy65 + " years" },
                    { "D14", prev.Assumptions.MaleLifeExpectancy65 + " years" },
                    { "C15", curr.Assumptions.MaleLifeExpectancy65_45 + " years" },
                    { "D15", prev.Assumptions.MaleLifeExpectancy65_45 + " years" },
                    // balance sheet
                    { "C20", figure(curr.BalanceSheetAmounts.DefinedBenefitObligation) },
                    { "D20", figure(prev.BalanceSheetAmounts.DefinedBenefitObligation) },
                    { "C21", figure(curr.BalanceSheetAmounts.FairValueOfSchemeAssets) },
                    { "D21", figure(prev.BalanceSheetAmounts.FairValueOfSchemeAssets) },
                    { "C22", figure(curr.BalanceSheetAmounts.SchemeSurplus) },
                    { "D22", figure(prev.BalanceSheetAmounts.SchemeSurplus) },
                    // defined benefit obligation
                    { "C28", figure(curr.DefinedBenefitObligation.OpeningDefinedBenefitObligation) },
                    { "C29", figure(curr.DefinedBenefitObligation.CurrentServiceCost) },
                    { "C30", figure(curr.DefinedBenefitObligation.InterestExpense)},
                    { "C31", figure(curr.DefinedBenefitObligation.ActuarialLosses)},
                    { "C32", figure(curr.DefinedBenefitObligation.Contributions)},
                    { "C33", figure(curr.DefinedBenefitObligation.BenefitsPaid)},
                    { "C34", figure(curr.DefinedBenefitObligation.ClosingDefinedBenefitObligation)},
                    // fair value of scheme assets
                    { "C39", figure(curr.FairValueSchemeAssetChanges.OpeningFairValueSchemeAssets)},
                    { "C40", figure(curr.FairValueSchemeAssetChanges.InterestIncome)},
                    { "C41", figure(curr.FairValueSchemeAssetChanges.ReturnOnSchemeAssets)},
                    { "C42", figure(curr.FairValueSchemeAssetChanges.SchemeAdminExpenses)},
                    { "C43", figure(curr.FairValueSchemeAssetChanges.ContributionsEmployer)},
                    { "C44", figure(curr.FairValueSchemeAssetChanges.ContributionsMembers)},
                    { "C45", figure(curr.FairValueSchemeAssetChanges.BenefitsPaid)},
                    { "C46", figure(curr.FairValueSchemeAssetChanges.ClosingFairValueSchemeAssets)},
                    // changes in surplus
                    { "C51", figure(curr.SchemeSurplusChanges.SchemeSurplusBeginning) },
                    { "C53", figure(curr.SchemeSurplusChanges.CurrentServiceCost) },
                    { "C54", figure(curr.SchemeSurplusChanges.SchemeAdminExpenses) },
                    { "C55", figure(curr.SchemeSurplusChanges.NetInterest) },
                    { "C56", figure(curr.SchemeSurplusChanges.PAndLTotal) },
                    { "C58", figure(curr.SchemeSurplusChanges.ActuarialLossesOnLiabilities) },
                    { "C59", figure(curr.SchemeSurplusChanges.ReturnOnSchemeAssets) },
                    { "C60", figure(curr.SchemeSurplusChanges.OCITotal) },
                    { "C62", figure(curr.SchemeSurplusChanges.Contributions) },
                    { "C64", figure(curr.SchemeSurplusChanges.SchemeSurplusEnd) },
                    // p & l forecast                        
                    { "C70", figure(curr.PAndLForecast.CurrentServiceCost) },
                    { "C71", figure(curr.PAndLForecast.SchemeAdminExpenses) },
                    { "C72", figure(curr.PAndLForecast.NetInterest) },
                    { "C73", figure(curr.PAndLForecast.Total) },
                    // dates
                    { "C8", data.End },
                    { "D8", data.Start },
                    { "C18", data.End },
                    { "D18", data.Start },
                    { "C26", data.End },
                    { "C37", data.End },
                    { "C49", data.End },
                    { "C68", "Year from " + data.End.ToString("dd MMM yyyy") },
                    // currency denominations
                    { "C19", data.CurrencySymbol + "000" },
                    { "D19", data.CurrencySymbol + "000" },
                    { "C27", data.CurrencySymbol + "000" },
                    { "C38", data.CurrencySymbol + "000" },
                    { "C50", data.CurrencySymbol + "000" },
                    { "C69", data.CurrencySymbol + "000" }
                };

            return cellDataMappings;
        }
    }
}
