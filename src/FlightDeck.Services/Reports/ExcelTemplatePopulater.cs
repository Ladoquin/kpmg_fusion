﻿namespace FlightDeck.Services.Reports
{
    using ClosedXML.Excel;
    using FlightDeck.Services.Reports.ExcelTransformations;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;

    class ExcelTemplatePopulater
    {
        const int maxLoadAttempts = 5;
        const int timeToWaitBeforeRetry = 500;

        private string _excelEditPassword = "KpMg2014";
        private string ExcelEditPassword
        {
            get
            {
                return _excelEditPassword;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    _excelEditPassword = "KpMg2014";
                else
                    _excelEditPassword = value;
            }
        }

        public string EncryptedExcelEditPassword
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    EncryptionService encryption = new EncryptionService();
                    ExcelEditPassword = encryption.Decrypt(value);
                }
            }
        }

        public MemoryStream Populate(string templatePath, Dictionary<string, object> cellDataMappings)
        {
            return Populate(templatePath, cellDataMappings, null);
        }

        public MemoryStream Populate(string templatePath, Dictionary<string, object> cellDataMappings, IEnumerable<IExcelTransformation> transformations)
        {
            XLWorkbook wb = null;
            var attempts = 0;
            do
            {
                try
                {
                    attempts++;
                    wb = new XLWorkbook(templatePath);
                }
                catch (IOException)
                {
                    // other process might be using template, give it a while before retrying
                    Thread.Sleep(timeToWaitBeforeRetry);
                }
            } while (wb == null && attempts < maxLoadAttempts);

            if (wb == null)
            {
                throw new IOException("Failed to load Excel template at: " + templatePath);
            }

            var ws = wb.Worksheets.First();

            foreach (var cell in cellDataMappings.Keys)
            {
                ws.Cell(cell).Value = cellDataMappings[cell];
            }

            if (transformations != null)
                foreach (var tranformation in transformations)
                    tranformation.Apply(ws);
            
            ws.PageSetup.PagesWide = 1;
            ws.PageSetup.PagesTall = 2;

            ws.Protection
                .SetObjects(false)
                .SetSelectLockedCells(false)
                .SetSelectUnlockedCells(false)
                .Protect(ExcelEditPassword);
            
            wb.Protect(true, true);

            var ms = new MemoryStream();
            wb.SaveAs(ms);
            ms.Seek(0, SeekOrigin.Begin);

            return ms;
        }
    }
}
