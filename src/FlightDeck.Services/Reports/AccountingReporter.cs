﻿namespace FlightDeck.Services.Reports
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.ServiceInterfaces.Reports;
    using System;
    using System.IO;
    
    public class AccountingReporter : IAccountingReporter
    {
        private readonly string _templatesFolder;

        public AccountingReporter(string templatesFolder)
        {
            _templatesFolder = templatesFolder;
        }

        public MemoryStream Create(IScheme scheme, AccountingStandardType type, DateTime start, DateTime end, AccountingReportOutputType outputType = AccountingReportOutputType.Default)
        {
            bool useClientAssumptions = end == scheme.PensionScheme.CurrentBasis.AttributionEnd;

            start = start < scheme.PensionScheme.CurrentBasis.AnchorDate || start > scheme.PensionScheme.RefreshDate ? scheme.PensionScheme.CurrentBasis.AnchorDate : start;
            end = end < scheme.PensionScheme.CurrentBasis.AnchorDate || end > scheme.PensionScheme.RefreshDate || end < start ? scheme.PensionScheme.RefreshDate : end;

            MemoryStream ms = null;

            switch (type)
            {
                case AccountingStandardType.IAS19:
                    ms = new IAS19RExcelReporter(_templatesFolder).Create(scheme, start, end, useClientAssumptions, outputType);
                    break;
                case AccountingStandardType.FRS102:
                    ms = new FRS102Reporter(_templatesFolder).Create(scheme, start, end, useClientAssumptions, outputType);
                    break;
                case AccountingStandardType.USGAAP:
                    ms = new USGAAPReporter(_templatesFolder).Create(scheme, start, end, useClientAssumptions, outputType);
                    break;
                default:
                    throw new Exception(string.Format("Unexpected AccountingStandardType value: '{0}'", type.ToString()));
            }

            return ms;
        }
    }
}
