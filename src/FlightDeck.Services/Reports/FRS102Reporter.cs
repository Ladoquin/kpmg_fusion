﻿namespace FlightDeck.Services.Reports
{
    public class FRS102Reporter : IAS19RExcelReporter
    {
        public FRS102Reporter(string templatesFolder)
            : base(templatesFolder, "FRS102 template.xlsx")
        { }
    }
}
