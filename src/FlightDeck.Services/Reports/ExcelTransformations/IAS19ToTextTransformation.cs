﻿namespace FlightDeck.Services.Reports.ExcelTransformations
{
    using System.Collections.Generic;

    class IAS19ToTextTransformation : DisclosureToTextTransformation
    {
        protected override IEnumerable<string> DateCellAddresses
        {
            get
            {
                return new List<string>
                    {
                        "B5",
                        "C8", 
                        "D8", 
                        "C18",
                        "D18",
                        "C26",
                        "C37",
                        "C49",
                    };
            }
        }

        protected override IEnumerable<string> PercentageCellAddresses
        {
            get
            {
                return new List<string>
                    {
                        "C9", 
                        "D9", 
                        "C10",
                        "D10",
                        "C11",
                        "D11",
                        "C12",
                        "D12"
                    };
            }
        }
    }
}
