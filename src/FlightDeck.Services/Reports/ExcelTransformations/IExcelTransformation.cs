﻿namespace FlightDeck.Services.Reports.ExcelTransformations
{
    using ClosedXML.Excel;
    
    public interface IExcelTransformation
    {
        void Apply(IXLWorksheet worksheet);
    }
}
