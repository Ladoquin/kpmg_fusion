﻿namespace FlightDeck.Services.Reports.ExcelTransformations
{
    using System.Collections.Generic;
    
    class USGAAPToTextTransformation : DisclosureToTextTransformation
    {
        protected override IEnumerable<string> DateCellAddresses
        {
            get 
            {
                return new List<string>
                    {
                        "B5",
                        "C8",
                        "D8",
                        "C19",
                        "C30",
                        "D30",
                        "C38",
                        "C50",
                        "C60",
                        "C73",
                        "C83",
                        "C92"
                    };
            }
        }

        protected override IEnumerable<string> PercentageCellAddresses
        {
            get
            {
                return new List<string>
                    {
                        "C9", 
                        "D9", 
                        "C10",
                        "D10",
                        "C11",
                        "D11",
                        "C12",
                        "D12",
                        "C13",
                        "D13",
                        "C20",
                        "C21",
                        "C22",
                        "C23",
                        "C24"
                    };
            }
        }
    }
}
