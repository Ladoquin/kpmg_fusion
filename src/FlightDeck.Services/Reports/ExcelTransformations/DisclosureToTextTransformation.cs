﻿namespace FlightDeck.Services.Reports.ExcelTransformations
{
    using ClosedXML.Excel;
    using System.Collections.Generic;
    using System.Globalization;

    abstract class DisclosureToTextTransformation : IExcelTransformation
    {
        protected abstract IEnumerable<string> DateCellAddresses { get; }
        protected abstract IEnumerable<string> PercentageCellAddresses { get; }        

        public void Apply(IXLWorksheet worksheet)
        {
            applyDateTransformations(worksheet);
            applyPercentageTransformations(worksheet);
        }

        void applyDateTransformations(IXLWorksheet worksheet)
        {
            foreach (var addr in DateCellAddresses)
            {
                var cell =  worksheet.Cell(addr);
                var d = cell.GetDateTime();
                cell.Clear(XLClearOptions.Contents);
                cell.SetDataType(XLCellValues.Text);
                cell.SetValue<string>(d.ToString("dd MMMM yyyy"));
            }
        }

        void applyPercentageTransformations(IXLWorksheet worksheet)
        {
            foreach (var addr in PercentageCellAddresses)
            {
                var cell = worksheet.Cell(addr);
                var value = (double)cell.Value;
                cell.DataType = XLCellValues.Text;
                cell.Value = value.ToString("P", CultureInfo.InvariantCulture);
            }
        }
    }
}
