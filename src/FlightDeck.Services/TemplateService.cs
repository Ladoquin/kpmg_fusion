﻿namespace FlightDeck.Services
{
    using FlightDeck.ServiceInterfaces;
    using System.Collections.Generic;
    using System.IO;

    public class TemplateService : ITemplateService
    {
        public string GenerateContent(string templatePath, Dictionary<string, string> tokenValues)
        {
            var template = File.ReadAllText(templatePath);

            foreach (var tokenValue in tokenValues)
            {
                template = template.Replace(tokenValue.Key, tokenValue.Value);
            }

            return template;
        }
    }
}
