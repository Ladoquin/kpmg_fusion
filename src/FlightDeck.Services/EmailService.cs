﻿using FlightDeck.ServiceInterfaces;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;

namespace FlightDeck.Services
{
    public class EmailService : IEmailService
    {
        private readonly SmtpSection _smtpSection;
        private readonly ILog _logger;

        public EmailService(SmtpSection smtpSection)
        {
            _smtpSection = smtpSection;
            _logger = LogManager.GetLogger("email");
        }

        public void SendEmail(IEnumerable<Tuple<string, string>> recipients, string subject, string html, string plain, string fromDisplayName = "KPMG Fusion")
        {
            recipients.ToList().ForEach(x => SendEmail(x.Item1, x.Item2, subject, html, plain, fromDisplayName));
        }

        public void SendEmail(string to, string toDisplay, string subject, string html, string plain, string fromDisplayName = "KPMG Fusion")
        {
            MailMessage email = new MailMessage();
            using (SmtpClient smtpClient = new SmtpClient())
            {

                string from = _smtpSection.From;

                email.To.Add(new MailAddress(to, toDisplay));
                email.From = new MailAddress(from, fromDisplayName);

                email.Subject = subject;
                email.IsBodyHtml = true;
                email.Body = plain;
                AlternateView htmlRepresentation = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);
                email.AlternateViews.Add(htmlRepresentation);

                try
                {
                    smtpClient.Send(email);
                    _logger.InfoFormat("Email sent. Recipient: {0}, subject: {1}", to, subject);
                }
                catch (Exception e)
                {
                    // We shouldn't stop normal execution but should at least log this has failed for support purposes
                    _logger.Error(String.Format("Failed to send email. Recipient: {0}, subject: {1}", to, subject), e);
                }
            }
        }

        public void SendEmailAsync(IEnumerable<Tuple<string, string>> recipients, string subject, string html, string plain, string fromDisplayName = "KPMG Fusion")
        {
            recipients.ToList().ForEach(x => SendEmailAsync(x.Item1, x.Item2, subject, html, plain, fromDisplayName));
        }

        public void SendEmailAsync(string to, string toDisplay, string subject, string html, string plain, string fromDisplayName = "KPMG Fusion")
        {
            ThreadPool.QueueUserWorkItem(i =>
            {
                try
                {
                    SendEmail(to, toDisplay, subject, html, plain, fromDisplayName);
                }
                catch(Exception e)
                {
                    LogManager.GetLogger(this.GetType()).ErrorFormat("Sending email error. {0}", e);
                }
            });

        }
    }
}
