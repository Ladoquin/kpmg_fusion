﻿namespace FlightDeck.Services
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    
    public class Scheme : SchemeDetail, IScheme
    {
        public IPensionScheme PensionScheme { get; set; }
    }
}
