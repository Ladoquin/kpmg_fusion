﻿namespace FlightDeck.Services
{
    using FlightDeck.DataAccess;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;

    public class ClientAssumptionService : IClientAssumptionService
    {
        private readonly ISerializer _serializer;

        private enum ClientAssumptionType : byte
        {
            Liabilities,
            PensionIncreases,
            Assets,
            RecoveryPlan,
            InvestmentStrategy,
            FROType,
            FRO,
            EFRO,
            ETV,
            PIE,
            FutureBenefits,
            ABF,
            Insurance,
            InvestmentStrategyOptions
        }

        public ClientAssumptionService(ISerializer serializer)
        {
            _serializer = serializer;
        }

        public IEnumerable<ClientAssumption> GetAll(int schemeDetailId)
        {
            using (var db = new DbInteractionScope())
            {
                return ((IClientAssumptionsRepository)db.GetRepository<ClientAssumption>()).GetAll(schemeDetailId);
            }
        }

        public int SaveCurrentOptions(string name, IScheme scheme, IPrincipal createdByUser)
        {
            var basis = scheme.PensionScheme.CurrentBasis;

            UserProfile user;
            using (var db = new DbInteractionScope())
            {
                user = ((IUserProfileRepository)db.GetRepository<UserProfile>()).GetByUsername(createdByUser.Identity.Name);
            }

            var d = new ClientAssumption
                (
                    0,
                    name,
                    scheme.SchemeDetailId,                    
                    basis.AttributionEnd,                                        
                    _serializer.SerializeObject<Dictionary<ClientAssumptionType, string>>(
                        new Dictionary<ClientAssumptionType, string>
                        {
                            { ClientAssumptionType.Liabilities, basis.GetClientLiabilityAssumptions().IsDirty ? _serializer.SerializeObject<AssumptionData>(basis.GetClientLiabilityAssumptions().After) : null },
                            { ClientAssumptionType.PensionIncreases, basis.GetClientPensionIncreases().IsDirty ? _serializer.SerializeObject<IDictionary<int, double>>(basis.GetClientPensionIncreases().After) : null },
                            { ClientAssumptionType.Assets, basis.GetClientAssetAssumptions().IsDirty ? _serializer.SerializeObject<IDictionary<AssetClassType, double>>(basis.GetClientAssetAssumptions().After) : null },
                            { ClientAssumptionType.RecoveryPlan, basis.GetClientRecoveryAssumptions().IsDirty ? _serializer.SerializeObject<RecoveryPlanAssumptions>(basis.GetClientRecoveryAssumptions().After) : null },
                            { ClientAssumptionType.InvestmentStrategy, scheme.PensionScheme.GetClientInvestmentStrategyAllocations().IsDirty ? _serializer.SerializeObject<IDictionary<AssetClassType, double>>(scheme.PensionScheme.GetClientInvestmentStrategyAllocations().After) : null },
                            { ClientAssumptionType.InvestmentStrategyOptions, scheme.PensionScheme.GetClientInvestmentStrategyOptions().IsDirty ? _serializer.SerializeObject<InvestmentStrategyOptions>(scheme.PensionScheme.GetClientInvestmentStrategyOptions().After) : null },
                            { ClientAssumptionType.FROType, scheme.PensionScheme.GetClientFroType().IsDirty ? _serializer.SerializeObject<FROType>(scheme.PensionScheme.GetClientFroType().After) : null },
                            { ClientAssumptionType.FRO, scheme.PensionScheme.GetClientFroOptions().IsDirty ? _serializer.SerializeObject<FlexibleReturnOptions>(scheme.PensionScheme.GetClientFroOptions().After) : null },
                            { ClientAssumptionType.EFRO, scheme.PensionScheme.GetClientEFroOptions().IsDirty ? _serializer.SerializeObject<EmbeddedFlexibleReturnOptions>(scheme.PensionScheme.GetClientEFroOptions().After) : null },
                            { ClientAssumptionType.ETV, scheme.PensionScheme.GetClientEtvOptions().IsDirty ? _serializer.SerializeObject<EnhancedTransferValueOptions>(scheme.PensionScheme.GetClientEtvOptions().After) : null },
                            { ClientAssumptionType.PIE, scheme.PensionScheme.GetClientPieOptions().IsDirty ? _serializer.SerializeObject<PieOptions>(scheme.PensionScheme.GetClientPieOptions().After) : null },
                            { ClientAssumptionType.FutureBenefits, scheme.PensionScheme.GetClientFutureBenefitsParameters().IsDirty ? _serializer.SerializeObject<FutureBenefitsParameters>(scheme.PensionScheme.GetClientFutureBenefitsParameters().After) : null },
                            { ClientAssumptionType.ABF, scheme.PensionScheme.GetClientAbfOptions().IsDirty ? _serializer.SerializeObject<AbfOptions>(scheme.PensionScheme.GetClientAbfOptions().After) : null },
                            { ClientAssumptionType.Insurance, scheme.PensionScheme.GetClientInsuranceParameters().IsDirty ? _serializer.SerializeObject<InsuranceParameters>(scheme.PensionScheme.GetClientInsuranceParameters().After) : null }
                        }),
                    user.UserId,
                    scheme.PensionScheme.CurrentBasis.MasterBasisId
                );

            using (var db = new DbInteractionScope())
            {
                db.GetRepository<ClientAssumption>().InsertOrUpdate(d);

                return d.Id;
            }
        }

        public void RestoreOptionsToScheme(int id, IScheme scheme)
        {
            ClientAssumption clientAssumption = null;

            using (var db = new DbInteractionScope())
            {
                clientAssumption = db.GetRepository<ClientAssumption>().GetById(id);
            }            

            var basis = scheme.PensionScheme.SwitchBasis(clientAssumption.MasterBasisId);

            scheme.PensionScheme.SetAttributionPeriod(basis.AttributionStart, clientAssumption.AnalysisDate);

            var data = _serializer.DeserializeObject<Dictionary<ClientAssumptionType, string>>(clientAssumption.Data);

            Func<ClientAssumptionType, bool> hasData = key => data.ContainsKey(key) && !string.IsNullOrEmpty(data[key]);

            if (hasData(ClientAssumptionType.Liabilities))
            {
                basis.SetClientLiabilityAssumptions(_serializer.DeserializeObject<AssumptionData>(data[ClientAssumptionType.Liabilities]));
            }
            if (hasData(ClientAssumptionType.PensionIncreases))
            {
                basis.SetClientPensionIncreases(_serializer.DeserializeObject<IDictionary<int, double>>(data[ClientAssumptionType.PensionIncreases]));
            }
            if (hasData(ClientAssumptionType.Assets))
            {
                basis.SetClientAssetAssumptions(_serializer.DeserializeObject<IDictionary<AssetClassType, double>>(data[ClientAssumptionType.Assets]));
            }
            if (hasData(ClientAssumptionType.RecoveryPlan))
            {
                basis.SetClientRecoveryAssumptions(_serializer.DeserializeObject<RecoveryPlanAssumptions>(data[ClientAssumptionType.RecoveryPlan]));
            }
            if (hasData(ClientAssumptionType.InvestmentStrategy))
            {
                scheme.PensionScheme.SetClientInvestmentStrategyAllocations(_serializer.DeserializeObject<IDictionary<AssetClassType, double>>(data[ClientAssumptionType.InvestmentStrategy]));
            }
            if (hasData(ClientAssumptionType.InvestmentStrategyOptions))
            {
                scheme.PensionScheme.SetClientInvestmentStrategyOptions(_serializer.DeserializeObject<InvestmentStrategyOptions>(data[ClientAssumptionType.InvestmentStrategyOptions]));
            }
            if (hasData(ClientAssumptionType.FROType))
            {
                scheme.PensionScheme.SetClientFroType(_serializer.DeserializeObject<FROType>(data[ClientAssumptionType.FROType]));
            }
            if (hasData(ClientAssumptionType.FRO))
            {
                scheme.PensionScheme.SetClientFroOptions(_serializer.DeserializeObject<FlexibleReturnOptions>(data[ClientAssumptionType.FRO]));
            }
            if (hasData(ClientAssumptionType.EFRO))
            {
                scheme.PensionScheme.SetClientEFroOptions(_serializer.DeserializeObject<EmbeddedFlexibleReturnOptions>(data[ClientAssumptionType.EFRO]));
            }
            if (hasData(ClientAssumptionType.ETV))
            {
                scheme.PensionScheme.SetClientEtvOptions(_serializer.DeserializeObject<EnhancedTransferValueOptions>(data[ClientAssumptionType.ETV]));
            }
            if (hasData(ClientAssumptionType.PIE))
            {
                scheme.PensionScheme.SetClientPieOptions(_serializer.DeserializeObject<PieOptions>(data[ClientAssumptionType.PIE]));
            }
            if (hasData(ClientAssumptionType.FutureBenefits))
            {
                scheme.PensionScheme.SetClientFutureBenefitsParameters(_serializer.DeserializeObject<FutureBenefitsParameters>(data[ClientAssumptionType.FutureBenefits]));
            }
            if (hasData(ClientAssumptionType.ABF))
            {
                scheme.PensionScheme.SetClientAbfOptions(_serializer.DeserializeObject<AbfOptions>(data[ClientAssumptionType.ABF]));
            }
            if (hasData(ClientAssumptionType.Insurance))
            {
                scheme.PensionScheme.SetClientInsuranceParameters(_serializer.DeserializeObject<InsuranceParameters>(data[ClientAssumptionType.Insurance]));
            }
        }

        public void DeleteSavedOptions(int id, IScheme scheme)
        {
            using (var db = new DbInteractionScope())
            {
                ((IClientAssumptionsRepository)db.GetRepository<ClientAssumption>()).Delete(id);
            }
        }
    }
}
