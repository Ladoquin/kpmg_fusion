﻿using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;

namespace FlightDeck.Services
{
    class CachedResultsService : ICachedResultsService
    {
        readonly IDbInteractionScopeFactory dbScopeFactory;

        public CachedResultsService()
            : this(new DbInteractionScopeFactory())
        {
        }

        public CachedResultsService(IDbInteractionScopeFactory dbScopeFactory)
        {
            this.dbScopeFactory = dbScopeFactory;
        }

        public void ClearAllForScheme(string schemeName)
        {
            using (var db = dbScopeFactory.GetScope())
            {
                ((IBasisCacheRepository)db.GetRepository<BasisCache>()).DeleteAllByScheme(schemeName);
                ((IPensionSchemeCacheRepository)db.GetRepository<PensionSchemeCache>()).DeleteAllByScheme(schemeName);
            }
        }
    }
}
