﻿namespace FlightDeck.Services
{
    using FlightDeck.DomainShared;
    
    /// <summary>
    /// The AssetClassType enum is being used by Fusion for the VaR classifications. This is fine for the calcs, but the names
    /// are slightly different (perhaps a new enum should have been created, but it would have been incredibly similar to AssetClassType
    /// and would probably have had to written something to map between the two anyway). 
    /// So this service just converts the AssetClassType to a VaR display name.
    /// </summary>
    class AssetClassTypeVaRDisplayNameService
    {
        public string GetVaRDisplayName(AssetClassType type, bool excludingSwaps)
        {
            switch (type)
            {
                case AssetClassType.LiabilityInterest:
                    return excludingSwaps ? "Liability Interest rate" : "Liability Interest rate";
                case AssetClassType.LiabilityInflation:
                    return excludingSwaps ? "Liability Inflation" : "Liability Inflation - with Swaps";
                case AssetClassType.LiabilityLongevity:
                    return excludingSwaps ? "Longevity" : "Longevity";
                case AssetClassType.FixedInterestGilts:
                    return "Fixed Interest Gilts - Rates";
                case AssetClassType.IndexLinkedGilts:
                    return "Index Linked Gilts - Rates";
                case AssetClassType.CorporateBonds:
                    return "Non gilt Credit - Duration";
                case AssetClassType.IndexLinkedGiltsInflation:
                    return "Index Linked Gilts - Inflation";
                case AssetClassType.Abf:
                    return "ABF";
                case AssetClassType.CreditSpread:
                    return "Non gilt Credit - Spread";
                case AssetClassType.DiversifiedCredit:
                    return "Diversified Credit";
                case AssetClassType.Equity:
                    return "Equity";
                case AssetClassType.DiversifiedGrowth:
                    return "Diversified Growth";
                case AssetClassType.Property:
                    return "Property";
                case AssetClassType.PrivateMarkets:
                    return "Private Markets";
                default:
                    return string.Empty;
            }
        }
    }
}
