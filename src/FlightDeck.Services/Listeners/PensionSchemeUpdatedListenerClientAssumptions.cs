﻿namespace FlightDeck.Services.Listeners
{
    using FlightDeck.DataAccess;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    
    internal class PensionSchemeUpdatedListenerClientAssumptions : IPensionSchemeUpdatedListener
    {
        public void SchemeUpdated(Scheme scheme, DbTransactionalScope ts, string activeusername)
        {
            ((IClientAssumptionsRepository)ts.GetRepository<ClientAssumption>()).DeleteAll(scheme.SchemeDetailId);
        }
    }
}
