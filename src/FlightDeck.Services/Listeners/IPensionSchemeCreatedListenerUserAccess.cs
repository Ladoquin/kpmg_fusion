﻿namespace FlightDeck.Services.Listeners
{
    using FlightDeck.DataAccess;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using System.Linq;

    internal class IPensionSchemeCreatedListenerUserAccess : IPensionSchemeCreatedListener
    {
        public void SchemeCreated(Scheme scheme, DbTransactionalScope ts, string activeusername)
        {
            var repo = (IUserProfileRepository)ts.GetRepository<UserProfile>();

            if (scheme.IsRestricted)
            {
                // If installer is creating a restricted scheme give them access to it
                var user = repo.GetByUsername(activeusername);
                if (user.Role == RoleType.Installer)
                {
                    user.SchemeDetails = user.SchemeDetails ?? new List<SchemeDetail>();
                    user.SchemeDetails = user.SchemeDetails.Union(new List<SchemeDetail> { new SchemeDetail { SchemeDetailId = scheme.SchemeDetailId } });
                    repo.Update(user);
                }
            }
        }
    }
}
