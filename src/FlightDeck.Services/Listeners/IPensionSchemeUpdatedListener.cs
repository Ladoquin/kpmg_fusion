﻿namespace FlightDeck.Services.Listeners
{
    using FlightDeck.DataAccess;

    internal interface IPensionSchemeUpdatedListener
    {
        void SchemeUpdated(Scheme scheme, DbTransactionalScope ts, string activeusername);
    }
}
