﻿namespace FlightDeck.Services.Listeners
{
    using FlightDeck.DataAccess;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using System.Linq;
    
    internal class PensionSchemeUpdatedListenerUserAcess : IPensionSchemeUpdatedListener
    {
        public void SchemeUpdated(Scheme scheme, DbTransactionalScope ts, string activeusername)
        {
            var repo = (IUserProfileRepository)ts.GetRepository<UserProfile>();

            if (scheme.IsRestricted)
            {
                // delete anyone's default scheme setting for this scheme that doesn't have explicit access already
                // ie, a consultant that might have set it as their default scheme when it was unrestricted will now lose access to it
                repo.PurgeRestrictedSchemeAccess(scheme.SchemeDetailId, new List<int> { (int)RoleType.Installer, (int)RoleType.Consultant });

                // if an installer is doing the update, give them access
                var user = repo.GetByUsername(activeusername);
                if (user.Role == RoleType.Installer)
                {
                    user.SchemeDetails = user.SchemeDetails ?? new List<SchemeDetail>();
                    if (!user.SchemeDetails.Any(x => x.SchemeDetailId == scheme.SchemeDetailId))
                    {
                        user.SchemeDetails = user.SchemeDetails.Union(new List<SchemeDetail> { new SchemeDetail { SchemeDetailId = scheme.SchemeDetailId } });
                        repo.Update(user);
                    }
                }
            }
            else
            {
                // clear any explicit access to this scheme
                repo.PurgeUnrestrictedSchemeAccess(scheme.SchemeDetailId, new List<int> { (int)RoleType.Installer, (int)RoleType.Consultant });
            }
        }
    }
}
