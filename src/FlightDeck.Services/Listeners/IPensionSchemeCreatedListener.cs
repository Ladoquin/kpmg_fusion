﻿namespace FlightDeck.Services.Listeners
{
    using FlightDeck.DataAccess;

    internal interface IPensionSchemeCreatedListener
    {
        void SchemeCreated(Scheme scheme, DbTransactionalScope ts, string activeusername);
    }
}
