﻿namespace FlightDeck.Services
{
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Schema;

    public class XmlSchemaValidator : IXmlSchemaValidator
    {
        public List<string> Messages { get; private set; }

        public bool Validate(Stream xml, Stream schema)
        {
            Messages = new List<string>();

            try
            {
                var schemaSet = new XmlSchemaSet();
                schemaSet.Add(null, XmlReader.Create(schema));
                var settings = new XmlReaderSettings();
                settings.ValidationType = ValidationType.Schema;
                settings.Schemas = schemaSet;
                settings.ValidationEventHandler +=
                    (o, e) =>
                    {
                        Messages.Add(string.Format("Line {0}: {1} ", e.Exception.LineNumber.ToString(), e.Message));
                    };

                var reader = XmlReader.Create(xml, settings);

                while (reader.Read()) ;
            }
            catch (XmlException ex)
            {
                Messages.Add(ex.Message);
            }

            return !Messages.Any();
        }
    }
}
