﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    using Domain.Evolution;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using log4net;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PreCalculationService : IPreCalculationService
    {
        readonly IDbInteractionScopeFactory dbScopeFactory;
        readonly Lazy<IFinancialIndexRepository> financialIndexRepository;
        readonly ISerializer serializer;

        public PreCalculationService(IDbInteractionScopeFactory dbScopeFactory, Lazy<IFinancialIndexRepository> financialIndexRepository, ISerializer serializer)
        {
            this.dbScopeFactory = dbScopeFactory;
            this.financialIndexRepository = financialIndexRepository;
            this.serializer = serializer;
        }

        public void ClearExisting()
        {
            using (var db = dbScopeFactory.GetScope())
            {
                ((IBasisCacheRepository)db.GetRepository<BasisCache>()).DeleteAll();
                ((IPensionSchemeCacheRepository)db.GetRepository<PensionSchemeCache>()).DeleteAll();
            }
        }

        /// <summary>
        /// Dictionary parameter is IndexName,EarliestChangeInIndex.
        /// Scheme specific cache deletion. For each scheme we only delete the cache down to a given date if it depends on the changed index.
        /// Any problems loading a scheme then we delete all the caches for that scheme.
        /// </summary>
        public void ClearExistingToEarliestUpdate(IDictionary<string, DateTime> indexEarliestUpdateDate)
        {
            var logger = LogManager.GetLogger("indices-import");

            //indexEarliestUpdateDate will only contain existing indices that have changed 
            //and not existing indices that have a new value on a new date
            if (indexEarliestUpdateDate.Count > 0)
                using (var db = dbScopeFactory.GetScope())
                {
                    //Get schemes with a cache
                    var pensionSchemeIds = ((IPensionSchemeCacheRepository)db.GetRepository<PensionSchemeCache>()).GetActiveSchemesWithCache();

                    if (pensionSchemeIds == null)
                        return;

                    foreach (var schemeId in pensionSchemeIds)
                    {
                        var assetClassRepository = (IAssetClassRepository)db.GetRepository<AssetClass>();

                        CacheDependencies cacheDependencies = null;

                        try
                        {
                            cacheDependencies = ((IPensionSchemeRepository)db.GetRepository<PensionScheme>()).GetCacheDependencies(schemeId);
                        }
                        catch (Exception e)
                        {                          
                            logger.Error("Cache dependencies for Scheme Id=" + schemeId + " could not be loaded for cache clean up. Any caches present for this scheme will be completely cleared.");
                        }

                        if (cacheDependencies == null)
                        {
                            ((IPensionSchemeCacheRepository)db.GetRepository<PensionSchemeCache>()).DeleteAllByScheme(schemeId);
                            ((IBasisCacheRepository)db.GetRepository<BasisCache>()).DeleteAllByScheme(schemeId);
                            continue;
                        }

                        //Get indices scheme dependant on
                        var dependeeIndices = cacheDependencies.Indices;

                        if (!indexEarliestUpdateDate.Any(i => dependeeIndices.Any(d => d == i.Key)))
                            continue; //scheme not dependant on any updated indices so move on to next scheme

                        //Get the earliest date of a depended on index that has changed
                        var minDate = indexEarliestUpdateDate.Where(i => dependeeIndices.Contains(i.Key)).Min(d => d.Value);

                        //Clear the PensionSchemeCache
                        var pensionSchemeCache = ((IPensionSchemeCacheRepository)db.GetRepository<PensionSchemeCache>()).GetBySchemeId(schemeId);

                        if (pensionSchemeCache != null)
                        {
                            var lDIEvolutionData = serializer.DeserializeObject<IEnumerable<Domain.LDI.LDIEvolutionItem>>(pensionSchemeCache.LDIEvolutionItems);

                            // Delete from the cache for date from (and including) the minDate
                            lDIEvolutionData = lDIEvolutionData.Where(l => l.Date < minDate).ToList();

                            // Save the cache
                            pensionSchemeCache.LDIEvolutionItems = serializer.SerializeObject(lDIEvolutionData);
                            db.GetRepository<PensionSchemeCache>().InsertOrUpdate(pensionSchemeCache);
                        }


                        //Clear the BasisCache
                        var masterBasisKeys = cacheDependencies.MasterBasisIds;

                        foreach (var masterBasisKey in masterBasisKeys)
                        {
                            var basisCache = ((IBasisCacheRepository)db.GetRepository<BasisCache>()).GetByMasterBasisId(masterBasisKey);

                            if (basisCache != null && basisCache.BasisEvolutionData != null)
                            {
                                var evolutionResults = serializer.DeserializeObject<BasisEvolutionData>(basisCache.BasisEvolutionData);

                                evolutionResults.LiabilityEvolution = evolutionResults.LiabilityEvolution.Where(l => l.Key < minDate).ToDictionary(mc => mc.Key, mc => mc.Value);
                                evolutionResults.AssetEvolution = evolutionResults.AssetEvolution.Where(l => l.Key < minDate).ToDictionary(mc => mc.Key, mc => mc.Value);
                                evolutionResults.LiabilityExperience = evolutionResults.LiabilityExperience.Where(l => l.Key < minDate).ToDictionary(mc => mc.Key, mc => mc.Value);
                                evolutionResults.BuyinExperience = evolutionResults.BuyinExperience.Where(l => l.Key < minDate).ToDictionary(mc => mc.Key, mc => mc.Value);
                                evolutionResults.AssetExperience = evolutionResults.AssetExperience.Where(l => l.Key < minDate).ToDictionary(mc => mc.Key, mc => mc.Value);

                                basisCache.BasisEvolutionData = serializer.SerializeObject(evolutionResults);
                                db.GetRepository<BasisCache>().InsertOrUpdate(basisCache);
                            }
                        }
                    }
                }
        }

        private void addIfNotExists(List<string> list, string item)
        {
            if (!list.Contains(item) && !string.IsNullOrEmpty(item))
                list.Add(item);          
        }

        public void Execute()
        {
            var pensionSchemeIds = new List<int>(500);

            using (var db = dbScopeFactory.GetScope())
            {
                pensionSchemeIds =
                    db.GetRepository<SchemeDetail>()
                        .GetAll()
                        .Where(x => x.SchemeData_Id.HasValue)
                        .Select(x => x.SchemeData_Id.Value)
                        .ToList();
            }

            foreach (var id in pensionSchemeIds)
            {
                PensionScheme pensionData = null;
                IBasis basis = null;

                try
                {
                    using (var db = dbScopeFactory.GetScope())
                    {
                        var assetClassRepository = (IAssetClassRepository)db.GetRepository<AssetClass>();
                        var schemeRepository = (IPensionSchemeRepository)db.GetRepository<PensionScheme>();
                        pensionData = schemeRepository.GetById(id, financialIndexRepository.Value, assetClassRepository);
                    }
                    var cachedBasisManager = new CachedBasisManager(serializer, dbScopeFactory);
                    var cachedPensionScheme = new CachedPensionScheme(pensionData, cachedBasisManager, serializer, dbScopeFactory); //forces initialisation of ldi calcs

                    foreach (var b in cachedPensionScheme.GetAllBases())
                    {
                        basis = b;
                        var e = basis.Evolution.First().Value; //force initialisation of evolution calcs
                    }
                }
                catch (Exception ex)
                {
                    var err = "Exception occurred while pre-calculating";
                    if (pensionData != null)
                        err += " scheme '" + pensionData.Name + "'";
                    if (basis != null)
                        err += ", basis '" + basis.Name + "'";
                    LogManager.GetLogger(this.GetType()).ErrorFormat(err + ": " + ex.ToString());
                }
            }
        }
    }
}
