﻿using FlightDeck.DataAccess;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using log4net;
using System;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Security.Principal;
using System.Xml.Linq;
using System.Globalization;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.Services
{
    public class IndexImportDetailService : IIndexImportDetailService
    {
        public Lazy<IFinancialIndexRepository> FinancialIndexRepository { get; private set; }

        public IndexImportDetailService(bool eager)
        {
            refreshIndexRepository();
            if (eager)
            {
                var x = FinancialIndexRepository.Value;
            }
        }

        private void refreshIndexRepository()
        {
            FinancialIndexRepository = new Lazy<IFinancialIndexRepository>(() =>
            {
                var log = LogManager.GetLogger(GetType());
                IList<FinancialIndex> indices;
                using (var db = new DbInteractionScope())
                {
                    var r = db.GetRepository<FinancialIndex>();
                    r.ClearCache();
                    indices = (IList<FinancialIndex>)r.GetAll();
                }
                var financialIndexRepository = new CachedFinancialIndexRepository(indices);
                log.InfoFormat("Restored index cache with {0} indices.", indices.Count);
                return financialIndexRepository;
            });
        }

        private List<int> insertedIndices;
        public List<int> InsertedIndices
        {
            get
            {
                return insertedIndices;
            }
        }

        private Dictionary<string, DateTime> indexEarliestUpdateDate = new Dictionary<string, DateTime>();
        /// <summary>
        /// This will only be populated after ImportIndices has been called
        /// </summary>
        public Dictionary<string, DateTime> IndexEarliestUpdateDate
        {
            get { return indexEarliestUpdateDate; }
        }

        public void ImportIndices(XDocument doc)
        {
            DataImportService dataImportService = null;

            using (var db = new DbInteractionScope())
            {
                dataImportService = new DataImportService();
            }

            dataImportService.ImportIndices(doc, FinancialIndexRepository.Value);

            insertedIndices = dataImportService.InsertedIndices;
            indexEarliestUpdateDate = dataImportService.IndexEarliestUpdateDate;

            refreshIndexRepository();
        }

        public IndexImportDetail GetLatest()
        {
            using (var db = new DbInteractionScope())
            {
                return ((IIndexImportDetailRepository)db.GetRepository<IndexImportDetail>()).GetLatest();
            }
        }

        public IndexImportDetail Save(XDocument xdoc, IPrincipal importedBy)
        {
            var extract = ExtractIndexImportDetails(xdoc);

            var importDetail = new IndexImportDetail
            {
                ImportedByUser = importedBy.Identity.Name,
                ImportedOnDate = DateTime.Now,
                ProducedByUser = extract["ProducedByUser"] as string,
                ProducedOnDate = extract["ProducedOnDate"] as DateTime?,
                Spreadsheet = extract["Spreadsheet"] as string,
                IndexStartDate = extract["IndexStartDate"] as DateTime?,
                IndexEndDate = extract["IndexEndDate"] as DateTime?,
                Comment = extract.ContainsKey("Comment") ? extract["Comment"] as string : null,
                ExportFile = extract.ContainsKey("ExportFile") ? extract["ExportFile"] as string : null,
                Indices = InsertedIndices
            };

            using (var db = new DbInteractionScope())
            {
                ((IIndexImportDetailRepository)db.GetRepository<IndexImportDetail>()).InsertOrUpdate(importDetail);
            }

            return importDetail;
        }


        public void FillEndGap(int IndexImportDetailId)
        {
            if (IndexImportDetailId <= 0)
                throw new ApplicationException("FillEndGap - Invalid IndexImportDetailId");

            // get list of all indices
            IndexImportDetail import;
            using (var db = new DbInteractionScope())
            {
                var repo = ((IIndexImportDetailRepository)db.GetRepository<IndexImportDetail>());
                import = repo.GetById(IndexImportDetailId);

                if (import.Indices != null && import.IndexEndDate != null && import.Indices.Count > 0)
                {
                    var financialIndexRepo = ((IFinancialIndexRepository)db.GetRepository<FinancialIndex>());
                    foreach (int i in import.Indices)
                    {
                        var index = FinancialIndexRepository.Value.GetById(i);
                        if (index != null)
                            financialIndexRepo.FillEndGap(index, import.IndexEndDate.GetValueOrDefault());
                    }
                    refreshIndexRepository();
                }
            }
        }

        private Dictionary<string, object> ExtractIndexImportDetails(XDocument schemeDataXml)
        {
            var extract = new Dictionary<string, object>(10);

            Func<XElement, bool> IsBlank = (element) => element == null || element.Value.Trim() == String.Empty;

            // ProducedOnDate
            var dateElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Date");
            var timeElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Time");

            if (!IsBlank(dateElement) && !IsBlank(timeElement))
            {
                DateTime d;
                if (DateTime.TryParse(dateElement.Value + ' ' + timeElement.Value, out d))
                    extract["ProducedOnDate"] = DateTime.Parse(dateElement.Value + ' ' + timeElement.Value, new CultureInfo("en-GB").DateTimeFormat);
            }
            if (!extract.ContainsKey("ProducedOnDate"))
                extract["ProducedOnDate"] = DateTime.Now.ToString(new CultureInfo("en-GB").DateTimeFormat);

            var indexStartDateElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Inputs/StartDate");
            if (!IsBlank(indexStartDateElement))
                extract["IndexStartDate"] = DateTime.Parse(indexStartDateElement.Value, new CultureInfo("en-GB").DateTimeFormat);
            else
                extract["IndexStartDate"] = null;

            var indexEndDateElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Inputs/EndDate");
            if (!IsBlank(indexEndDateElement))
                extract["IndexEndDate"] = DateTime.Parse(indexEndDateElement.Value, new CultureInfo("en-GB").DateTimeFormat);
            else
                extract["IndexEndDate"] = null;

            // ProducedByUser
            var userElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/UserName");
            if (!IsBlank(userElement)) extract["ProducedByUser"] = userElement.Value;

            // DataComment
            var commentElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/DataComment");
            if (!IsBlank(commentElement)) extract["Comment"] = commentElement.Value;

            // Spreadsheet
            var spreadsheetElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/Spreadsheet");
            if (!IsBlank(spreadsheetElement)) extract["Spreadsheet"] = spreadsheetElement.Value;

            // ExportFile
            var exportFileElement = schemeDataXml.XPathSelectElement("/Data/AuditTrail/ExportFile");
            if (!IsBlank(exportFileElement)) extract["ExportFile"] = exportFileElement.Value;

            return extract;
        }


    }
}
