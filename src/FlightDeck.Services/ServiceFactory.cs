﻿namespace FlightDeck.Services
{
    using FlightDeck.ServiceInterfaces;
    using System.Runtime.Caching;
    using System.Security.Principal;
    
    public class ServiceFactory
    {
        private IndexImportDetailService CachedIndexImportDetailService
        {
            get
            {
                if (!MemoryCache.Default.Contains("CachedIndexImportDetailService"))
                {
                    var policy = new CacheItemPolicy();
                    MemoryCache.Default.Add("CachedIndexImportDetailService", new IndexImportDetailService(true), policy);
                }
                return (IndexImportDetailService)MemoryCache.Default["CachedIndexImportDetailService"];
            }
        }


        public IIndexImportDetailService GetIndexImportDetailService()
        {
            return CachedIndexImportDetailService;
        }

        public ICurveImportDetailService GetCurveImportDetailService()
        {
            return new CurveImportDetailService();
        }

        public IPensionSchemeService GetPensionSchemeService(IPrincipal principal)
        {
            return new PensionSchemeService(principal, CachedIndexImportDetailService.FinancialIndexRepository);
        }

        public IPreCalculationService GetPreCalculationService()
        {
            return new PreCalculationService(new DbInteractionScopeFactory(), CachedIndexImportDetailService.FinancialIndexRepository, new Serializer());
        }

        public IGlobalParametersService GetGlobalParametersService()
        {
            return new GlobalParametersService();
        }

        public IIndicesService GetIndicesService()
        {
            return new IndicesService(CachedIndexImportDetailService.FinancialIndexRepository);
        }

        public void ClearCache()
        {
            MemoryCache.Default.Remove("CachedIndexImportDetailService");
        }
    }
}
