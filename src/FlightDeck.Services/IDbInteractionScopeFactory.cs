﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    
    public interface IDbInteractionScopeFactory
    {
        IDbInteractionScope GetScope();
        IDbTransactionalScope GetTransactionalScope();
    }
}
