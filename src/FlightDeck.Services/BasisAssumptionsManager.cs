﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.Domain.Evolution;
using FlightDeck.Domain.Assets;

namespace FlightDeck.Services
{
    class BasisAssumptionsManager
    {
        private MutableBeforeAfter<AssumptionData> ClientLiabilityAssumptions;
        private MutableBeforeAfter<IDictionary<int, double>> ClientPensionIncreases;
        private MutableBeforeAfter<IDictionary<AssetClassType, double>> ClientAssetAssumptions;
        private MutableBeforeAfter<RecoveryPlanAssumptions> ClientRecoveryAssumptions;

        private readonly ISchemeData scheme;
        private readonly BasisEvolutionData evolution;
        private readonly IBasesDataManager basesDataManager;
        private readonly IPortfolioManager portfolioManager;

        public BasisAssumptionsManager(ISchemeData scheme, BasisEvolutionData evolution, IBasesDataManager basesDataManager, IPortfolioManager portfolioManager)
        {
            this.scheme = scheme;
            this.evolution = evolution;
            this.basesDataManager = basesDataManager;
            this.portfolioManager = portfolioManager;
        }

        public void Reset(DateTime at)
        {
            //liability assumptions
            var liabs = evolution.LiabilityEvolution[at];
            var b = basesDataManager.GetBasis(at);
            ClientLiabilityAssumptions = new MutableBeforeAfter<AssumptionData>(new AssumptionData(
                liabs.DiscPre, liabs.DiscPost, liabs.DiscPen, liabs.SalInc, liabs.RPI, liabs.CPI, b.SimpleAssumptions[SimpleAssumptionType.LifeExpectancy65].Value, b.SimpleAssumptions[SimpleAssumptionType.LifeExpectancy65_45].Value));
            ClientPensionIncreases = new MutableBeforeAfter<IDictionary<int, double>>(new Dictionary<int, double> {
                        { 1, liabs.Pinc1 }, { 2, liabs.Pinc2 }, { 3, liabs.Pinc3 }, { 4, liabs.Pinc4 }, { 5, liabs.Pinc5 }, { 6, liabs.Pinc6 } });

            //asset assumptions
            var pf = portfolioManager.GetPortfolio(at);
            ClientAssetAssumptions = new MutableBeforeAfter<IDictionary<AssetClassType, double>>(pf.AssetData.Assets.ToDictionary(x => x.Class.Type, x => x.DefaultReturn));

            //recovery plan
            var v = scheme.ReturnOnAssets;
            var defaultRecoveryPlanAssumptions = new RecoveryPlanAssumptions(
                v,
                v,
                0,
                double.Parse(scheme.NamedProperties[NamedPropertyGroupType.RecoveryPlan].Properties.Single(x => x.Key == NamedRecoveryPlanPropertyTypes.MaxLumpSum.ToString()).Value),
                double.Parse(scheme.NamedProperties[NamedPropertyGroupType.RecoveryPlan].Properties.Single(x => x.Key == NamedRecoveryPlanPropertyTypes.AnnualIncs.ToString()).Value),
                scheme.IsGrowthFixed,
                12,
                int.Parse(scheme.NamedProperties[NamedPropertyGroupType.RecoveryPlan].Properties.Single(x => x.Key == NamedRecoveryPlanPropertyTypes.Length.ToString()).Value));

            ClientRecoveryAssumptions = new MutableBeforeAfter<RecoveryPlanAssumptions>(defaultRecoveryPlanAssumptions);
        }

        public IBasisAssumptions Before
        {
            get
            {
                return null;
            }
        }

        public IBasisAssumptions After
        {
            get
            {
                return null;
            }
        }
    }
}
