﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using log4net;
    using System;

    public class PasswordManager : IPasswordManager
    {
        private const int MaxSavedPassword = 5;

        private readonly IAccountSecurityService accountSecurity;
        private readonly IUserPasswordEncrypter passwordEncrypter;
        private readonly IPasswordValidationService passwordValidator;
        private readonly IDbInteractionScopeFactory dbInteractionScopeFactory;
        private readonly ILog logger;

        public PasswordManager(IAccountSecurityService accountSecurity)
            : this(accountSecurity, new UserPasswordEncrypter(), new PasswordValidationService(new UserPasswordEncrypter()), new DbInteractionScopeFactory())
        {
        }

        public PasswordManager(IAccountSecurityService accountSecurity, IUserPasswordEncrypter passwordEncrypter, IPasswordValidationService passwordValidationService, IDbInteractionScopeFactory dbInteractionScopeFactory)
        {
            this.accountSecurity = accountSecurity;
            this.passwordEncrypter = passwordEncrypter;
            this.passwordValidator = passwordValidationService;
            this.dbInteractionScopeFactory = dbInteractionScopeFactory;
            this.logger = LogManager.GetLogger("account-login");
        }

        public bool IsPasswordValid(string username, string oldpassword, string password)
        {
            return passwordValidator.IsValid(username, oldpassword, password);
        }

        public bool ChangePassword(string username, string oldpassword, string password)
        {
            // ChangePassword will throw an exception rather than return false in certain failure scenarios.
            bool changePasswordSucceeded;
            try
            {
                logger.Debug("Calling ChangePassword");
                changePasswordSucceeded = accountSecurity.ChangePassword(username, oldpassword, password);
                if (!changePasswordSucceeded)
                {
                    logger.Warn("Change password failure, unknown reason");
                }
                else
                {
                    savePasswordToHistory(username, password);
                }
            }
            catch (Exception ex)
            {
                changePasswordSucceeded = false;

                logger.Warn("Change password failure", ex);
            }
            return changePasswordSucceeded;
        }

        public bool ResetPassword(string username, string passwordResetToken, string password)
        {
            logger.Debug("Calling ResetPassword with Token");
            var changePasswordSucceeded = accountSecurity.ResetPassword(passwordResetToken, password);
            if (!changePasswordSucceeded)
            {
                logger.Warn("Change password failure, invalid token");
            }
            else
            {
                savePasswordToHistory(username, password);
            }
            return changePasswordSucceeded;
        }

        private void savePasswordToHistory(string username, string password)
        {
            using (var db = dbInteractionScopeFactory.GetScope())
            {
                var repo = (IUserProfilePasswordRepository)db.GetRepository<PasswordData>();

                var salt = repo.GetSalt(username);

                var passwordData = salt != null
                    ? passwordEncrypter.GetEncrypted(password, salt)
                    : passwordEncrypter.GetEncrypted(password);

                repo.Insert(username, passwordData.EncryptedPassword, passwordData.Salt);

                repo.TrimHistory(username, MaxSavedPassword);
            }
        }
    }
}
