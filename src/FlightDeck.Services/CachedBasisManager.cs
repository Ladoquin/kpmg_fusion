﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
    
    internal class CachedBasisManager : ICachedBasisManager
    {
        private readonly ISerializer serializer;
        private readonly IDbInteractionScopeFactory dbScopeFactory;
        private IEnumerable<MasterBasisKey> masterbasisKeys;
        private Dictionary<int, BasisType> cacheKeys; // easier to lookup types rather than messing about with lazy loaded CachedBasis
        private Dictionary<int, Lazy<ICachedBasis>> cache;

        public ICachedBasis CurrentBasis { get; private set; }
        public ICachedBasis DefaultBasis { get; private set; }

        public CachedBasisManager(ISerializer serializer, IDbInteractionScopeFactory dbScopeFactory)
        {
            this.serializer = serializer;
            this.dbScopeFactory = dbScopeFactory;
        }

        public void SetBases(ICachedPensionScheme scheme)
        {
            masterbasisKeys = scheme.GetMasterbasisKeys();
            cacheKeys = masterbasisKeys.ToDictionary(x => x.Id, x => x.BasisType);
            cache = masterbasisKeys.ToDictionary(
                x => x.Id,
                x => new Lazy<ICachedBasis>(() => new CachedBasis(scheme.GetMasterBasis(x.Id), scheme, serializer, dbScopeFactory, scheme.RefreshDate)));

            var mbkey = masterbasisKeys.Where(k => string.Equals(scheme.DefaultBasisName, k.Name));
            if (mbkey.Any())
            {
                CurrentBasis = cache[mbkey.First().Id].Value;
            }
            else
            {
                if (cacheKeys.Any(x => x.Value == BasisType.TechnicalProvision))
                    CurrentBasis = cache[cacheKeys.First(x => x.Value == BasisType.TechnicalProvision).Key].Value;
                else
                    CurrentBasis = cache.First().Value.Value;
            }

            DefaultBasis = cache[CurrentBasis.MasterBasisId].Value;
        }

        public void SetCurrent(int masterbasisId)
        {
            CurrentBasis = cache[masterbasisId].Value;
        }

        public IEnumerable<ICachedBasis> GetAll()
        {
            return cache.Values.Select(x => x.Value);
        }

        public ICachedBasis GetFirstBasisOfType(BasisType type)
        {
            if (cacheKeys.Any(x => x.Value == type))
                return cache[cacheKeys.First(x => x.Value == type).Key].Value;

            return null;
        }

        public ICachedBasis GetBasis(int masterbasisId)
        {
            if (cache.ContainsKey(masterbasisId))
                return cache[masterbasisId].Value;
            else
                return null;
        }
    }
}
