﻿namespace FlightDeck.Services
{
    using FlightDeck.DataAccess;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    public class GlobalParametersService : IGlobalParametersService
    {
        private const string DefaultParametersName = "DEFAULT";

        public IEnumerable<ParametersImportHistory> GetImportHistory()
        {
            using (var db = new DbInteractionScope())
            {
                var repo = db.GetRepository<ParametersImportHistory>();

                var data = repo.GetAll();

                return data ?? new List<ParametersImportHistory>();
            }
        }

        public IDictionary<AssetClassType, double> GetVolatilities()
        {
            using (var db = new DbInteractionScope())
            {
                var repo = (IAssetClassRepository)db.GetRepository<AssetClass>();

                var data = repo.GetVolatilitiesByName(DefaultParametersName);

                var dict = data.ToDictionary(x => x.Item1, x => x.Item2);

                return dict;
            }
        }

        public IDictionary<AssetClassType, IDictionary<AssetClassType, double>> GetVarCorrelations()
        {
            using (var db = new DbInteractionScope())
            {
                var repo = (IAssetClassRepository)db.GetRepository<AssetClass>();

                var data = repo.GetVarCorrelationsByName(DefaultParametersName);

                var dict = data.ToDictionary(x => x.Item1, x => (IDictionary<AssetClassType, double>)x.Item2.ToDictionary(y => y.Item1, y => y.Item2));

                return dict;
            }
        }

        public ActionResult<IGlobalParametersServiceResult> ImportParameters(XDocument xdoc, int userId)
        {
            var volatilitiesResult = GetVolatilitiesFromXDoc(xdoc.Root);
            var varCorrelationsResult = GetVarCorrelationsFromXDoc(xdoc.Root);

            if (volatilitiesResult.Success && 
                varCorrelationsResult.Success)
            {
                var volatilities = volatilitiesResult.Get;
                var correlations = varCorrelationsResult.Get;

                if (volatilities.Count + correlations.Count > 0)
                {
                    using (var db = new DbTransactionalScope())
                    {
                        var repo = (IAssetClassRepository)db.GetRepository<AssetClass>();

                        repo.UpdateVolatilities(DefaultParametersName, volatilities);
                        repo.UpdateVarCorrelations(DefaultParametersName, correlations);

                        db.GetRepository<ParametersImportHistory>().Insert(new ParametersImportHistory(userId, DateTime.Now));

                        db.Commit();
                    }
                }
                else
                {
                    return new ActionResult<IGlobalParametersServiceResult>(IGlobalParametersServiceResult.XMLStructureInvalid);
                }
            }
            else
            {
                return new ActionResult<IGlobalParametersServiceResult>(volatilitiesResult.Statuses.Union(varCorrelationsResult.Statuses));
            }

            return new ActionResult<IGlobalParametersServiceResult>(true);
        }

        public GetResult<IGlobalParametersServiceResult, IDictionary<AssetClassType, double>> GetVolatilitiesFromXDoc(XElement root)
        {
            var dic = new Dictionary<AssetClassType, double>();

            var error = (IGlobalParametersServiceResult?)null;

            try
            {
                var el = root.Descendants("Volatility");
                var vols = el != null ? el.Elements("Vol") : null;

                if (vols != null)
                {
                    foreach (var vol in vols)
                    {
                        var item = Utils.MapAssetClassNameToType(vol.Attribute("Item").Value);
                        double value = 0;

                        if (item == null)
                        {
                            error = IGlobalParametersServiceResult.UnrecognisedAssetClassType;
                        }
                        else if (!double.TryParse(vol.Value, out value))
                        {
                            error = IGlobalParametersServiceResult.ValueTypeInvalid;
                        }

                        if (error == null)
                        {
                            dic.Add(item.Value, value);
                        }
                    }
                }
            }
            catch
            {
                error = IGlobalParametersServiceResult.XMLStructureInvalid;
            }

            var result = error.HasValue
                ? new GetResult<IGlobalParametersServiceResult, IDictionary<AssetClassType, double>>(error.Value)
                : new GetResult<IGlobalParametersServiceResult, IDictionary<AssetClassType, double>>(dic);

            return result;
        }

        public GetResult<IGlobalParametersServiceResult, IDictionary<AssetClassType, IDictionary<AssetClassType, double>>> GetVarCorrelationsFromXDoc(XElement root)
        {
            var dic = new Dictionary<AssetClassType, IDictionary<AssetClassType, double>>();

            var error = (IGlobalParametersServiceResult?)null;

            try
            {
                var el = root.Descendants("Correlation");
                var corrs = el != null ? el.Elements("Corr") : null;

                if (corrs != null)
                {
                    foreach (var corr in corrs)
                    {
                        var item1 = Utils.MapAssetClassNameToType(corr.Attribute("Item1").Value);
                        var item2 = Utils.MapAssetClassNameToType(corr.Attribute("Item2").Value);
                        double value = 0;

                        if (item1 == null || item2 == null)
                        {
                            error = IGlobalParametersServiceResult.UnrecognisedAssetClassType;
                        }
                        else if (!double.TryParse(corr.Value, out value))
                        {
                            error = IGlobalParametersServiceResult.ValueTypeInvalid;
                        }

                        if (error == null)
                        {
                            if (!dic.ContainsKey(item1.Value))
                            {
                                dic.Add(item1.Value, new Dictionary<AssetClassType, double> { { item1.Value, 1 } });
                            }
                            // values in the import xml are only listed one way
                            // but in the database they're stored both ways
                            // so add the reverse mapping to the dictionary
                            if (!dic.ContainsKey(item2.Value))
                            {
                                dic.Add(item2.Value, new Dictionary<AssetClassType, double> { { item2.Value, 1 } });
                            }
                            if (!dic[item1.Value].ContainsKey(item2.Value))
                            {
                                dic[item1.Value].Add(item2.Value, value);
                            }
                            if (!dic[item2.Value].ContainsKey(item1.Value))
                            {
                                dic[item2.Value].Add(item1.Value, value);
                            }
                        }
                    }
                }
            }
            catch
            {
                error = IGlobalParametersServiceResult.XMLStructureInvalid;
            }

            var result = error.HasValue
                ? new GetResult<IGlobalParametersServiceResult, IDictionary<AssetClassType, IDictionary<AssetClassType, double>>>(error.Value)
                : new GetResult<IGlobalParametersServiceResult, IDictionary<AssetClassType, IDictionary<AssetClassType, double>>>(dic);

            return result;
        }
    }
}
