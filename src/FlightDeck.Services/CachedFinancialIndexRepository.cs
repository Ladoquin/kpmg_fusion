﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightDeck.DataAccess;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.Services
{
    public class CachedFinancialIndexRepository : IFinancialIndexRepository
    {
        private readonly Dictionary<int, FinancialIndex> indicesById;

        public CachedFinancialIndexRepository(IEnumerable<FinancialIndex> indices)
        {
            indicesById = indices.ToDictionary(x => x.Id);
        }

        public FinancialIndex GetById(int id)
        {
            return indicesById[id];
        }

        public IEnumerable<FinancialIndex> GetAll()
        {
            return indicesById.Values;
        }

        public int Insert(FinancialIndex item)
        {
            throw new System.NotSupportedException();
        }

        public bool Update(FinancialIndex item)
        {
            throw new System.NotSupportedException();
        }

        public void InsertOrUpdate(FinancialIndex item)
        {
            throw new System.NotSupportedException();
        }

        public void ClearCache()
        {
            throw new System.NotSupportedException();
        }
        public IEnumerable<dynamic> GetIndicesByImportId(int importDetailId)
        {
            throw new NotImplementedException();
        }

        public FinancialIndex GetByName(string name)
        {
            var result = indicesById.Values.FirstOrDefault(x => x.Name == name);
            if(result == null)
                throw new ApplicationException(string.Format("Index {0} could not be found.", name));
            return result;
        }

        public FinancialIndex GetByNameComplete(string name)
        {
            throw new System.NotSupportedException();
        }

        public FinancialIndex GiltsIndex 
        { 
            get
            { 
                return GetByName(FinancialIndexRepository.GiltsIndexName); 
            } 
        }
        public int FillEndGap(FinancialIndex item, DateTime EndDate)
        {
            throw new NotImplementedException();
        }
    }
}