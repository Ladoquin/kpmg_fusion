﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;

    public class IndicesService : IIndicesService
    {
        private readonly Lazy<IFinancialIndexRepository> financialIndexRepository;

        public IndicesService(Lazy<IFinancialIndexRepository> financialIndexRepository)
        {
            this.financialIndexRepository = financialIndexRepository;
        }

        public IEnumerable<IndexDataSummary> GetAllSummaries()
        {
            var summaries = financialIndexRepository.Value.GetAll().Select(x => new IndexDataSummary(x.Id, x.Name, x.BeginDate, x.EndDate));
        
            return summaries;
        }

        public IOrderedDictionary GetIndexValues(int id)
        {
            var values = new OrderedDictionary(1500);
            var index = financialIndexRepository.Value.GetById(id);
            var start = index.BeginDate;
            var i = 0;
            while (start.AddDays(i) <= index.EndDate)
            {
                var key = start.AddDays(i++);
                if (index.ContainsValue(key))
                    values.Add(key, index.GetValue(key));
            }
            return values;
        }
    }
}
