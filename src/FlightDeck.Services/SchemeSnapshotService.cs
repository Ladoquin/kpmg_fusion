﻿namespace FlightDeck.Services
{
    using FlightDeck.DomainShared;
    using FlightDeck.DomainShared.VaR;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class SchemeSnapshotService : ISchemeSnapshotService
    {
        private readonly ISnapshotWriter writer;

        public SchemeSnapshotService()
            : this(new SnapshotXDocWriter())
        {
        }
        public SchemeSnapshotService(ISnapshotWriter writer)
        {
            this.writer = writer;
        }

        public MemoryStream GetSnapshot(IPensionScheme scheme)
        {
            var snapshot = GetInstanceSnapshot(scheme);

            var ms = writer.WriteSnapshot(snapshot);

            return ms;
        }

        internal PropertyTreeItem GetInstanceSnapshot(IPensionScheme scheme)
        {
            var basis = scheme.CurrentBasis;

            /*****************
             * inputs
             **********/
            var propertyTree =
                new PropertyTreeItem("scheme",
                    null,
                    new List<PropertyTreeItem>
                    {
                        new PropertyTreeItem("inputs",
                            new Dictionary<string, object> 
                            {
                                { "Basis", basis.Name },
                                { "AttributionStart", basis.AttributionStart.ToString("dd/MM/yyyy") },
                                { "AttributionEnd", basis.AttributionEnd.ToString("dd/MM/yyyy") },
                                { "RecoveryPlanType", scheme.GetClientRecoveryPlanOptions().After },
                                { "FROType", scheme.GetClientFroType().After }
                            },
                            new List<PropertyTreeItem>
                            {
                                new PropertyTreeItem("LiabilityAssumptions", Utils.GetObjectPropertyValues<AssumptionData>(basis.GetClientLiabilityAssumptions().After)),
                                new PropertyTreeItem("PensionIncreases", basis.GetClientPensionIncreases().After.ToDictionary(x => "pinc" + x.Key.ToString(), x => (object)x.Value)),
                                new PropertyTreeItem("AssetAssumptions", basis.GetClientAssetAssumptions().After.ToDictionary(x => x.Key.ToString(), x => (object)x.Value)),
                                new PropertyTreeItem("RecoveryPlan", Utils.GetObjectPropertyValues<RecoveryPlanAssumptions>(basis.GetClientRecoveryAssumptions().After)),
                                new PropertyTreeItem("InvestmentStrategy",
                                    scheme.GetClientInvestmentStrategyAllocations().After.ToDictionary(x => x.Key.ToString(), x => (object)x.Value)
                                        .Union(new List<KeyValuePair<string, object>> { new KeyValuePair<string, object>("CashInjection", scheme.GetClientInvestmentStrategyOptions().After.CashInjection )})
                                        .ToDictionary(x => x.Key, x => x.Value)),
                                new PropertyTreeItem("FRO", Utils.GetObjectPropertyValues<FlexibleReturnOptions>(scheme.GetClientFroOptions().After)),
                                new PropertyTreeItem("EFRO", Utils.GetObjectPropertyValues<EmbeddedFlexibleReturnOptions>(scheme.GetClientEFroOptions().After)),
                                new PropertyTreeItem("ETV", Utils.GetObjectPropertyValues<EnhancedTransferValueOptions>(scheme.GetClientEtvOptions().After)),
                                new PropertyTreeItem("FutureBenefits", Utils.GetObjectPropertyValues<FutureBenefitsParameters>(scheme.GetClientFutureBenefitsParameters().After)),
                                new PropertyTreeItem("ABF", Utils.GetObjectPropertyValues<AbfOptions>(scheme.GetClientAbfOptions().After)),
                                new PropertyTreeItem("Insurance", Utils.GetObjectPropertyValues<InsuranceParameters>(scheme.GetClientInsuranceParameters().After)),
                                new PropertyTreeItem("VaR", null, getVaRInput(basis))
                            }
                        )
                    });

            /*****************
             * outputs
             **********/
            var output = new List<PropertyTreeItem>(20);

            //panels
            output.Add(new PropertyTreeItem("WeightedReturns", Utils.GetObjectPropertyValues<WeightedReturnParameters>(basis.GetWeightedReturnParameters())));
            output.Add(new PropertyTreeItem("FRO", Utils.GetObjectPropertyValues<FroData>(basis.GetFroData().After)));
            output.Add(new PropertyTreeItem("EFRO", Utils.GetObjectPropertyValues<EFroData>(basis.GetEFroData().After)));
            output.Add(new PropertyTreeItem("ETV", Utils.GetObjectPropertyValues<EtvData>(basis.GetEtvData().After)));
            output.Add(new PropertyTreeItem("PIE", Utils.GetObjectPropertyValues<PieData>(basis.GetPieData().After)));
            output.Add(new PropertyTreeItem("FutureBenefits", Utils.GetObjectPropertyValues<FutureBenefitsData>(basis.GetFutureBenefits().After)));
            output.Add(new PropertyTreeItem("ABF", Utils.GetObjectPropertyValues<AbfData>(basis.GetAbfData().After)));
            output.Add(getInsuranceOutput(scheme, basis));

            //lenses
            output.Add(getFundingProgression(basis));
            output.Add(getCashflows(basis));
            output.Add(new PropertyTreeItem("FundingPosition", Utils.GetObjectPropertyValues<FundingData>(basis.GetFundingLevelData().After)));
            output.Add(getRecoveryPlan(basis));
            output.Add(new PropertyTreeItem("VarWaterfall", Utils.GetObjectPropertyValues<WaterfallData>(basis.GetVarWaterfall().After)));
            output.Add(getVarFunnel(basis));

            if (basis.BasisType == BasisType.Accounting)
            {
                output.Add(new PropertyTreeItem("BalanceSheet", Utils.GetObjectPropertyValues<BalanceData>(basis.GetBalanceSheetEstimate().After)));
                if (scheme.AccountingSettings.IAS19Visible)
                    output.Add(new PropertyTreeItem("IASProfitLoss", Utils.GetObjectPropertyValues<IAS19Disclosure.IAS19PAndLForecastData>(basis.GetIASData().After)));
                if (scheme.AccountingSettings.FRS17Visible)
                    output.Add(new PropertyTreeItem("FRS102ProfitLoss", Utils.GetObjectPropertyValues<IAS19Disclosure.IAS19PAndLForecastData>(basis.GetFRSData().After)));
                if (scheme.AccountingSettings.USGAAPVisible)
                    output.Add(new PropertyTreeItem("USGAAPProfitLoss", Utils.GetObjectPropertyValues<USGAAPDisclosure.NetPeriodicPensionCostForecastData>(basis.GetUSGAAPData().After)));
            }

            propertyTree.Children.Add(new PropertyTreeItem("outputs", null, output));

            return propertyTree;
        }

        private List<PropertyTreeItem> getVaRInput(IBasis basis)
        {
            var varInputs = basis.GetVaRInputs();
            var volatilities = varInputs == null || varInputs.VolatilityAssumptions == null ? null : varInputs.VolatilityAssumptions.Volatilities;
            var noSwapsCorrelationMatrixInputs = varInputs == null ? null : varInputs.NoSwapsCorrelationMatrixInputs;
            var swapsCorrelationMatrixInputs = varInputs == null ? null : varInputs.SwapsCorrelationMatrixInputs;
            var varNaming = new AssetClassTypeVaRDisplayNameService();
            var volatilityProperties = volatilities == null ? null : volatilities.ToDictionary(x => varNaming.GetVaRDisplayName(x.Key, false), x => (object)x.Value);
            var correlationProperties =
                    new List<PropertyTreeItem>
                        {
                            new PropertyTreeItem("ExcludingSwapsAndBuyin", null,
                                noSwapsCorrelationMatrixInputs == null ? null :
                                noSwapsCorrelationMatrixInputs
                                    .Select(x => new PropertyTreeItem(varNaming.GetVaRDisplayName(x.Key, true),
                                            x.Value.ToDictionary(y => varNaming.GetVaRDisplayName(y.Key, true), y => (object)y.Value))).ToList()),
                            new PropertyTreeItem("IncludingSwapsAndBuyin", null,
                                swapsCorrelationMatrixInputs == null ? null :
                                swapsCorrelationMatrixInputs
                                    .Select(x => new PropertyTreeItem(varNaming.GetVaRDisplayName(x.Key, false),
                                            x.Value.ToDictionary(y => varNaming.GetVaRDisplayName(y.Key, false), y => (object)y.Value))).ToList())
                        };

            return new List<PropertyTreeItem> 
                { 
                    new PropertyTreeItem("Volatility", volatilityProperties),
                    new PropertyTreeItem("Correlations", null, correlationProperties)
                };
        }

        private PropertyTreeItem getInsuranceOutput(IPensionScheme scheme, IBasis basis)
        {
            var properties = Utils.GetObjectPropertyValues<BuyinData>(basis.GetBuyinInfo().After);
            properties.Add("BasisPointIncreaseImpact", basis.GetInsurancePointChangeImpact().After);
            var p = new PropertyTreeItem("Insurance",
                properties,
                new List<PropertyTreeItem>
                    {
                        new PropertyTreeItem("Strains", null,
                            scheme.GetStrains().Select(x => new PropertyTreeItem(x.Key.Replace(" ", ""),
                                new Dictionary<string, object>
                                    {
                                        { "StartDate", x.Value.Item2.ToString("dd/MM/yyyy") },
                                        { "StartStrain", x.Value.Item1[StrainType.Current] },
                                        { "MaxStrain", x.Value.Item1[StrainType.Max] },
                                        { "MinStrain", x.Value.Item1[StrainType.Min] }
                                    }))
                                    .ToList())
                    });

            return p;
        }

        private PropertyTreeItem getFundingProgression(IBasis basis)
        {
            Func<BeforeAfter<IList<LensData>>, List<PropertyTreeItem>> progressionToTree = progression =>
                {
                    var year = -1;
                    var yearTree = progression.After.Select(x => new PropertyTreeItem(getYearKey(++year), Utils.GetObjectPropertyValues<LensData>(x)));
                    return yearTree.ToList();
                };

            var data = basis.GetJourneyPlan();
            var p = new PropertyTreeItem("FundingProgression", null, new List<PropertyTreeItem>
                {
                    new PropertyTreeItem("Before", null, progressionToTree(data.BeforePlan)),
                    new PropertyTreeItem("After", null, progressionToTree(data.AfterPlan))
                });

            return p;
        }

        private PropertyTreeItem getCashflows(IBasis basis)
        {
            var data = basis.GetCashFlowData();
            var year = -1;
            var p = new PropertyTreeItem("Cashflows", null, data.After.Select(x => new PropertyTreeItem(getYearKey(++year), Utils.GetObjectPropertyValues<LiabilityData>(x))).ToList());

            return p;
        }

        private PropertyTreeItem getRecoveryPlan(IBasis basis)
        {
            var data = basis.GetRecoveryPlan();
            var year = -1;
            var p = new PropertyTreeItem("RecoveryPlan", data.After.ToDictionary(x => getYearKey(++year), x => (object)x));

            return p;
        }

        private PropertyTreeItem getVarFunnel(IBasis basis)
        {
            var data = basis.GetVarFunnel();
            var series = 0;
            var p = new PropertyTreeItem("VarFunnel", null, data.After.Select(
                x => new PropertyTreeItem("series" + (++series).ToString(), Utils.GetObjectPropertyValues<VarFunnelData>(x))).ToList());

            return p;   
        }

        private string getYearKey(int y)
        {
            return "year" + y.ToString("D2");
        }
    }
}
