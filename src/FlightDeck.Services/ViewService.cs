﻿namespace FlightDeck.Services
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ViewService : IViewService
    {
        /// <summary>
        /// Can't couple Buyin Asset Class with the Matched category in the enum definition because then it might start being used
        /// for generic asset calculations that are filtered by category. Buyin is added on the fly to asset collections for front end display purposes. 
        /// </summary>
        public const AssetCategory BuyinCategory = AssetCategory.Matched;

        public SchemeSummaryView GetSummary(IPensionScheme scheme, DateTime date)
        {
            var liabAssetEvolution = scheme.CurrentBasis.Evolution[date.Date];

            return new SchemeSummaryView
                {
                    Balance = liabAssetEvolution.TotalAssetsPlusBuyin - liabAssetEvolution.TotalLiability,
                    FundingTarget = -1 * (scheme.CurrentBasis.ExpectedDeficit[date].Deficit - (liabAssetEvolution.TotalAssetsPlusBuyin - liabAssetEvolution.TotalLiability)),
                };
        }

        /// <summary>
        /// Get Liabilities split at a specific point in time (client assumptions are ignored).
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public IEnumerable<BreakdownItem> GetLiabilitiesSplit(IPensionScheme scheme, DateTime date)
        {
            var current = scheme.CurrentBasis.Evolution[date.Date];

            var total = current.TotalLiability;

            var buyin = current.TotalExistingBuyin;

            var items = new List<BreakdownItem>
                {
                    new BreakdownItem 
                    { 
                        Label = "Pensioners", 
                        Value = current.Pensioner,
                        SubItems = new List<BreakdownItem>
                            {
                                new BreakdownItem("Insured", buyin), 
                                new BreakdownItem("Non Insured", current.Pensioner - buyin)
                            }
                    },
                    new BreakdownItem("Actives", current.Active),
                    new BreakdownItem("Deferreds", current.Deferred)
                };

            return items;
        }

        /// <summary>
        /// Get Liability split based on current basis' analysis date and client assumptions
        /// </summary>
        /// <param name="scheme"></param>
        /// <returns></returns>
        public IEnumerable<BreakdownItem> GetLiabilitiesCustomSplit(IPensionScheme scheme)
        {
            var current = scheme.CurrentBasis.GetFundingLevelData().After;

            var total = current.Liabilities;

            var buyin = current.Buyin;

            var items = new List<BreakdownItem>
                {
                    new BreakdownItem 
                    { 
                        Label = "Pensioners", 
                        Value = current.Pensioners,
                        SubItems = new List<BreakdownItem>
                            {
                                new BreakdownItem("Insured", buyin), 
                                new BreakdownItem("Non Insured", current.Pensioners - buyin)
                            }
                    },
                    new BreakdownItem("Actives", current.Actives),
                    new BreakdownItem("Deferreds", current.Deferreds)
                };

            return items;
        }

        public IDictionary<EvolutionElapsedPeriodType, EvolutionDataItem> GetEvolution(IPensionScheme scheme, DateTime date)
        {
            var tracker = scheme.CurrentBasis.Evolution.Values;

            var current = tracker.Single(t => t.Date == date.Date);

            Func<EvolutionData, EvolutionDataItem> toEvol = t => t == null ? null : new EvolutionDataItem
            {
                Asset = new ValueDifference { Value = t.TotalAssetsPlusBuyin, Difference = current.TotalAssetsPlusBuyin - t.TotalAssetsPlusBuyin },
                Liability = new ValueDifference { Value = t.TotalLiability, Difference = current.TotalLiability - t.TotalLiability },
                Balance = new ValueDifference { Value = t.TotalAssetsPlusBuyin - t.TotalLiability, Difference = (current.TotalAssetsPlusBuyin - current.TotalLiability) - (t.TotalAssetsPlusBuyin - t.TotalLiability) },
                Date = t.Date
            };

            var data = new Dictionary<EvolutionElapsedPeriodType, EvolutionDataItem>
                {
                    { EvolutionElapsedPeriodType.NoElapse, toEvol(tracker.SingleOrDefault(t => t.Date == date)) },
                    { EvolutionElapsedPeriodType.Day1, toEvol(tracker.SingleOrDefault(t => t.Date == date.AddDays(-1))) },
                    { EvolutionElapsedPeriodType.Month1, toEvol(tracker.SingleOrDefault(t => t.Date == date.AddMonths(-1))) },
                    { EvolutionElapsedPeriodType.Month3, toEvol(tracker.SingleOrDefault(t => t.Date == date.AddMonths(-3))) },
                    { EvolutionElapsedPeriodType.Month6, toEvol(tracker.SingleOrDefault(t => t.Date == date.AddMonths(-6))) },
                    { EvolutionElapsedPeriodType.Year1, toEvol(tracker.SingleOrDefault(t => t.Date == date.AddYears(-1))) }
                };

            return data;
        }

        public IEnumerable<KeyLabelValue<string, double>> GetLiabilityAssumptions(IPensionScheme scheme, DateTime date, IDictionary<string, string> defaultLabels)
        {
            var basis = scheme.CurrentBasis;

            var simpleAssumptions = basis.GetSimpleAssumptions(date);
            var labels = basis.GetAssumptionLabels(date);

            var tracker = scheme.CurrentBasis.Evolution[date.Date];

            var assumptions = tracker.ToAssumptionData(simpleAssumptions[SimpleAssumptionType.LifeExpectancy65].Value, simpleAssumptions[SimpleAssumptionType.LifeExpectancy65_45].Value);

            return getLiabilityAssumptions(assumptions, simpleAssumptions, labels, defaultLabels);
        }

        public IEnumerable<PensionIncreaseDefinition> GetPensionIncreases(IPensionScheme scheme, DateTime date)
        {
            var basis = scheme.CurrentBasis;

            var definitions = basis.GetPensionIncreaseDefinitions(date);

            var pensionIncreases = scheme.CurrentBasis.Evolution[date.Date].PensionIncreases;

            return definitions.Select(def => new PensionIncreaseDefinition(def.Key, def.Label, pensionIncreases[def.Key], def.IsFixed, def.IsVisible));
        }

        public IEnumerable<PensionIncreaseDefinition> GetPensionIncreasesAtAnchor(IPensionScheme scheme)
        {
            var basis = scheme.CurrentBasis;

            var definitions = basis.GetPensionIncreaseDefinitions(basis.AnchorDate);

            return definitions;
        }

        public IEnumerable<KeyLabelValue<string, BeforeAfter<double>>> GetLiabilityAssumptionsCustom(IPensionScheme scheme, IDictionary<string, string> defaultLabels)
        {
            var basis = scheme.CurrentBasis;

            var simpleAssumptions = basis.GetSimpleAssumptions();
            var labels = basis.GetAssumptionLabels(basis.AttributionEnd);

            var before = getLiabilityAssumptions(basis.GetClientLiabilityAssumptions().Before, simpleAssumptions, labels, defaultLabels);
            var after = getLiabilityAssumptions(basis.GetClientLiabilityAssumptions().After, simpleAssumptions, labels, defaultLabels);

            var items = before.Select(x => new KeyLabelValue<string, BeforeAfter<double>>
                (
                    x.Key,
                    x.Label,
                    new BeforeAfter<double>(x.Value, after.SingleOrDefault(a => a.Key == x.Key).Value)
                ));

            return items;
        }

        public IDictionary<PensionIncreaseDefinition, BeforeAfter<double>> GetPensionIncreasesCustom(IPensionScheme scheme)
        {
            var basis = scheme.CurrentBasis;

            var definitions = basis.GetPensionIncreaseDefinitions(basis.AttributionEnd);

            var pincs = basis.GetClientPensionIncreases();

            var increases =
                definitions.ToDictionary(
                   x => x,
                   x => new BeforeAfter<double>(pincs.Before[x.Key], pincs.After[x.Key]));

            return increases;
        }

        public IEnumerable<AssetReturnItem> GetAssetReturnsCustom(IPensionScheme scheme)
        {
            var items =
                scheme.CurrentBasis.GetAssetData()
                    .OrderBy(x => x.Category).ThenBy(x => x.Type.DisplayName())
                    .Select(x => new AssetReturnItem((int)x.Type, x.Type.DisplayName(), new BeforeAfter<double>(scheme.CurrentBasis.GetClientAssetAssumptions().Before[x.Type], scheme.CurrentBasis.GetClientAssetAssumptions().After[x.Type]))
                        {
                            UserMin = scheme.AssetDefinitions[x.Type].ReturnSpec.Min,
                            UserMax = scheme.AssetDefinitions[x.Type].ReturnSpec.Max,
                            UserIncrement = scheme.AssetDefinitions[x.Type].ReturnSpec.Increment
                        });

            return items;
        }

        public double GetWeightedAssetReturn(IPensionScheme scheme)
        {
            WeightedReturnParameters weightedAssetReturnParameters = scheme.CurrentBasis.GetWeightedReturnParameters(); 
            var weightedAssetReturn = weightedAssetReturnParameters.WeightedAssetReturn;
            return weightedAssetReturn;
        }

        public EvolutionSchemeData GetEvolutionSchemeData(IPensionScheme scheme)
        {
            var attribution = scheme.CurrentBasis.GetAttributionData().Before;
            var surplusAnalysisData = scheme.CurrentBasis.GetSurplusAnalysisData().Before;

            var item = new EvolutionSchemeData
                {
                    RegularEmployee = attribution.AttrEeeContsReceived,
                    RegularEmployer = attribution.AttrEerContsReceived,
                    Deficit = attribution.AttrRecoveryPlanConts,
                    Total = attribution.AttrEeeContsReceived + attribution.AttrEerContsReceived + attribution.AttrRecoveryPlanConts,
                    Payment = attribution.AttrTotBenefitOutgo,
                    NetCashflow = attribution.AttrEeeContsReceived + attribution.AttrEerContsReceived + attribution.AttrRecoveryPlanConts - attribution.AttrTotBenefitOutgo
                };

            return item;
        }

        #region privates
        
        private IEnumerable<BreakdownItem> getAssetBreakdown(IEnumerable<AssetData> assets, IDictionary<AssetClassType, double> classProportions, double a, IDictionary<ClientAssetType, double> newAssets = null)
        {
            var buyin = (double?)null;
            var newBuyin = (double?)null;
            var newAbf = (double?)null;
            if (newAssets != null)
            {
                if (newAssets.ContainsKey(ClientAssetType.BuyinExisting) && newAssets[ClientAssetType.BuyinExisting] > 0) buyin = newAssets[ClientAssetType.BuyinExisting];
                if (newAssets.ContainsKey(ClientAssetType.BuyinNew) && newAssets[ClientAssetType.BuyinNew] > 0)
                {
                    newBuyin = newAssets[ClientAssetType.BuyinNew];
                    buyin = buyin.GetValueOrDefault() + newBuyin;
                }
                
                if (newAssets.ContainsKey(ClientAssetType.ABF) && newAssets[ClientAssetType.ABF] > 0) newAbf = newAssets[ClientAssetType.ABF];
                a = a - newBuyin.GetValueOrDefault() - newAbf.GetValueOrDefault();
            }

            var subItems =
                assets
                    .OrderBy(x => x.Category).ThenBy(x => x.ClassName)
                    .Select(x => new KeyValuePair<AssetCategory, BreakdownItem>
                        (
                            x.Category,
                            new BreakdownItem
                                (
                                    x.ClassId,
                                    x.ClassName,
                                    a * classProportions[(AssetClassType)x.ClassId]
                                )
                        ))
                    .ToList();

            if (buyin.GetValueOrDefault() > 0)
            {
                subItems.Add(new KeyValuePair<AssetCategory, BreakdownItem>(BuyinCategory, new BreakdownItem("Buy in", buyin.Value)));
            }
            if (newAbf.HasValue)
            {
                if (!subItems.Any(x => x.Value.Key == (int)AssetClassType.Abf))
                {
                    subItems.Add(new KeyValuePair<AssetCategory, BreakdownItem>(Utils.GetEnumAttribute<AssetClassType, AssetCategory>(AssetClassType.Abf), new BreakdownItem((int)AssetClassType.Abf, AssetClassType.Abf.DisplayName(), newAbf.Value)));
                }
                else
                {
                    subItems.Single(x => x.Value.Key == (int)AssetClassType.Abf).Value.Value += newAbf.Value;
                }
            }

            var items =
                subItems
                    .GroupBy(x => x.Key)
                    .OrderBy(x => x.Key)
                    .Select(x => new BreakdownItem
                    {
                        Label = x.Key.ToString(),
                        Value = x.Sum(ac => ac.Value.Value),
                        SubItems = x.Select(ac => ac.Value).OrderBy(ac => ac.Label)
                    });

            return items;
        }

        private IEnumerable<KeyLabelValue<string, double>> getLiabilityAssumptions(AssumptionData assumptions, IDictionary<SimpleAssumptionType, KeyValuePair<string, double>> simpleAssumptions, IDictionary<AssumptionType, string> labels, IDictionary<string, string> defaultLabels)
        {
            var items = new List<KeyLabelValue<string, double>>(10);

            foreach (var label in labels)
            {
                double value = 0;

                switch (label.Key)
                {
                    case AssumptionType.PreRetirementDiscountRate:
                        value = assumptions.PreRetirementDiscountRate; break;
                    case AssumptionType.PostRetirementDiscountRate:
                        value = assumptions.PostRetirementDiscountRate; break;
                    case AssumptionType.PensionDiscountRate:
                        value = assumptions.PensionDiscountRate; break;
                    case AssumptionType.SalaryIncrease:
                        value = assumptions.SalaryIncrease; break;
                    case AssumptionType.InflationRetailPriceIndex:
                        value = assumptions.InflationRetailPriceIndex; break;
                    case AssumptionType.InflationConsumerPriceIndex:
                        value = assumptions.InflationConsumerPriceIndex; break;
                    default:
                        break;
                }

                items.Add(new KeyLabelValue<string, double>(label.Key.ToString(), label.Value ?? defaultLabels[label.Key.ToString()], value));
            }

            items.Add(new KeyLabelValue<string, double>(SimpleAssumptionType.LifeExpectancy65.ToString(), simpleAssumptions[SimpleAssumptionType.LifeExpectancy65].Key ?? defaultLabels[SimpleAssumptionType.LifeExpectancy65.ToString()], assumptions.PensionerLifeExpectancy));
            items.Add(new KeyLabelValue<string, double>(SimpleAssumptionType.LifeExpectancy65_45.ToString(), simpleAssumptions[SimpleAssumptionType.LifeExpectancy65_45].Key ?? defaultLabels[SimpleAssumptionType.LifeExpectancy65_45.ToString()], assumptions.DeferredLifeExpectancy));

            if (simpleAssumptions.ContainsKey(SimpleAssumptionType.SacrificeProportion))
            {
                items.Add(new KeyLabelValue<string, double>(SimpleAssumptionType.SacrificeProportion.ToString(), defaultLabels[SimpleAssumptionType.SacrificeProportion.ToString()], simpleAssumptions[SimpleAssumptionType.SacrificeProportion].Value));
            }

            return items;
        }

        private double getAssetAllocation(IPensionScheme scheme, AssetClassType ac, bool isAfter)
        {
            var r = .0;
            if (scheme.GetClientInvestmentStrategyAllocations().Before.ContainsKey(ac))
            {
                r = isAfter
                    ? scheme.GetClientInvestmentStrategyAllocations().After[ac]
                    : scheme.GetClientInvestmentStrategyAllocations().Before[ac];
            }
            return r;
        }

        #endregion

        /// <summary>
        /// Determine if current basis has buy in, if so, from when.
        /// </summary>
        /// <param name="scheme"></param>
        /// <returns>Null if no buy in, else the date of the buy in.</returns>
        public DateTime? GetBuyinStatus(IPensionScheme scheme)
        {
            var basis = scheme.CurrentBasis;

            foreach (var liabliityImportDate in basis.EffectiveDates.OrderBy(x => x))
            {
                if (basis.Evolution[liabliityImportDate].TotalExistingBuyin != 0)
                {
                    return liabliityImportDate;
                }
            }

            return (DateTime?)null;
        }

        public SchemeAssetData GetSchemeAssetData(IPensionScheme scheme, AssumptionsSourceType assumptionsSource)
        {
            var funds = GetAssetsBreakdown(scheme, assumptionsSource);
            var fundsTotal = funds.Sum(fund => fund.Value);
            var syntheticAssetInfo = assumptionsSource == AssumptionsSourceType.Default
                        ? scheme.GetSyntheticAssetInfo()
                        : new SyntheticAssetInfo(scheme.GetClientInvestmentStrategyOptions().After.SyntheticEquity, scheme.GetClientInvestmentStrategyOptions().After.SyntheticCredit);

            syntheticAssetInfo.SetAmounts(fundsTotal);

            return new SchemeAssetData
                {
                    Funds = funds,
                    SyntheticAssetInfo = syntheticAssetInfo
                };
        }

        public IEnumerable<AssetFundGrowthAndValue> GetSchemeAssetDataWithGrowth(IPensionScheme scheme)
        {
            var fundsGrowth = scheme.GetAssetsGrowthInfo().ToList();

            //we've already got code which works out funds current amount, so get that and merge it in to the results which have fund's growth

            var schemeAssetData = GetSchemeAssetData(scheme, AssumptionsSourceType.Default);

            var assets = new List<AssetFundGrowthAndValue>(20);

            Func<string, bool, double> findFundValue = (name, isSynthetic) =>
                {
                    if (!isSynthetic)
                    {
                        var f = schemeAssetData.Funds.SelectMany(x => x.SubItems).SelectMany(x => x.SubItems).FirstOrDefault(x => x.Label == name);
                        return f == null ? 0 : f.Value;
                    }
                    else
                    {
                        if (schemeAssetData.SyntheticAssetInfo == null)
                            return 0;
                        if (name == "Synthetic Equity")
                            return schemeAssetData.SyntheticAssetInfo.EquityAmount;
                        if (name == "Synthetic Credit")
                            return schemeAssetData.SyntheticAssetInfo.CreditAmount;
                        return 0;
                    }
                };

            foreach (var fund in fundsGrowth)
            {
                assets.Add(
                    new AssetFundGrowthAndValue(fund.Category, fund.Class, fund.Name)
                        {
                            Growth = fund.Growth,
                            IsSynthetic = fund.IsSynthetic,
                            Amount = findFundValue(fund.Name, fund.IsSynthetic)
                        });
            }

            //and also need to manually add any existing buyin because that won't come from asset funds automatically
            var existingBuyinAtStart = scheme.CurrentBasis.Evolution[scheme.CurrentBasis.AttributionStart].TotalExistingBuyin;
            var existingBuyinAtEnd = scheme.CurrentBasis.Evolution[scheme.CurrentBasis.AttributionEnd].TotalExistingBuyin;
            if (existingBuyinAtStart != 0 || existingBuyinAtEnd != 0)
            {
                assets.Add(new AssetFundGrowthAndValue(BuyinCategory, AssetClassType.Buyin, AssetClassType.Buyin.DisplayName())
                    {
                        Growth = (existingBuyinAtEnd - existingBuyinAtStart).MathSafeDiv(existingBuyinAtStart),
                        Amount = existingBuyinAtEnd
                    });
            }

            return assets;
        }

        public SchemeAssetData GetSchemeAssetDataAtRefreshDate(IPensionScheme scheme)
        {
            var funds = GetAssetsBreakdownAt(scheme, scheme.RefreshDate);
            var fundsTotal = funds.Sum(fund => fund.Value);
            var syntheticAssetInfo = scheme.GetSyntheticAssetInfo(scheme.RefreshDate);

            syntheticAssetInfo.SetAmounts(fundsTotal);

            return new SchemeAssetData
            {
                Funds = funds,
                SyntheticAssetInfo = syntheticAssetInfo
            };
        }

        public SchemeAssetData GetSchemeAssetDataAtAnchor(IPensionScheme scheme)
        {
            var funds = GetAssetsBreakdownAtAnchor(scheme);
            var fundsTotal = funds.Sum(fund => fund.Value);
            var syntheticAssetInfo = scheme.GetSyntheticAssetInfo(scheme.CurrentBasis.AnchorDate);
            
            syntheticAssetInfo.SetAmounts(fundsTotal);

            return new SchemeAssetData
            {
                Funds = funds,
                SyntheticAssetInfo = syntheticAssetInfo
            };
        }

        /// <summary>
        /// Get assets breakdown for a date other than the current attribution end (analysis) date.
        /// </summary>
        /// <remarks>Passing a date means data requested is for non-analysis date, therefore use default assumptions.</remarks>
        /// <param name="scheme"></param>
        /// <param name="at"></param>
        /// <returns></returns>
        public IEnumerable<BreakdownItem> GetAssetsBreakdownAt(IPensionScheme scheme, DateTime at)
        {
            var investment = scheme.CurrentBasis.GetInvestmentStrategyData(AttributionEndType.RefreshDate);
            var funds = scheme.GetAssetData(at);
            var fundTotal = funds.Sum(fund => fund.Amount);
            var allocations = funds
                .GroupBy(fund => fund.Asset.Type)
                .ToDictionary(
                    x => x.Key,
                    x => x.Sum(fund => fund.Amount) / fundTotal);

            return getAssetsBreakdown(scheme, allocations, at, investment);
        }

        /// <summary>
        /// Get assets breakdown for the current attribution end (analysis) date, with or without the current set of client assumptions. 
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="assumptionsSource"></param>
        /// <returns></returns>
        public IEnumerable<BreakdownItem> GetAssetsBreakdown(IPensionScheme scheme, AssumptionsSourceType assumptionsSource)
        {
            var investment = assumptionsSource == AssumptionsSourceType.Default ? scheme.CurrentBasis.GetInvestmentStrategyData().Before : scheme.CurrentBasis.GetInvestmentStrategyData().After;
            var allocations = assumptionsSource == AssumptionsSourceType.Default ? scheme.GetClientInvestmentStrategyAllocations().Before : scheme.GetClientInvestmentStrategyAllocations().After;

            return getAssetsBreakdown(scheme, allocations, scheme.CurrentBasis.AttributionEnd, investment);
        }

        /// <summary>
        /// Get assets breakdown for the anchor date of the current basis. 
        /// </summary>
        /// <remarks>No rolling forward / adjusting of asset values required, they are as they were imported.</remarks>
        /// <param name="scheme"></param>
        /// <returns></returns>
        private IEnumerable<BreakdownItem> GetAssetsBreakdownAtAnchor(IPensionScheme scheme)
        {
            var data = scheme.GetAssetData(scheme.CurrentBasis.AnchorDate).ToList();
            if (scheme.CurrentBasis.BasisType == BasisType.Accounting)
            {
                data.RemoveAll(x => x.Asset.Type == AssetClassType.Abf);
            }
            var existingBuyin = scheme.CurrentBasis.Evolution.First().Value.TotalExistingBuyin;                        

            if (existingBuyin > 0)
            {
                data.Add(new AssetFundItem(AssetClassType.Buyin.DisplayName(), new AssetItem(AssetClassType.Buyin, BuyinCategory, existingBuyin), existingBuyin));
            }

            var breakdown = categoriseAssetFunds(data);

            return breakdown;
        }

        private IEnumerable<BreakdownItem> getAssetsBreakdown(IPensionScheme scheme, IDictionary<AssetClassType, double> assetAllocations, DateTime at, InvestmentStrategyData investment)
        {
            var initialData = scheme.GetAssetData(at).ToList();

            //scale up allocations after removing non-editable abf proportion
            var abfProportion = assetAllocations.Any(x => x.Key == AssetClassType.Abf) ? assetAllocations.Single(x => x.Key == AssetClassType.Abf).Value : 0;
            var allocations = assetAllocations.Where(x => x.Key != AssetClassType.Abf).ToDictionary(x => x.Key, x => x.Value / (1 - abfProportion));

            //remove abf component - investment strategy will have worked that out and can be added back in whole
            initialData = initialData.Where(fund => fund.Asset.Type != AssetClassType.Abf).ToList();
            
            var initialAssetTotal = initialData.Sum(x => x.Amount);
            var initialAssetClassTotals = initialData.GroupBy(x => x.Asset.Type).ToDictionary(x => x.Key, x => x.Sum(fund => fund.Amount));

            //user may have given an allocation to a class with 0% allocation in scheme asset definition
            var clientCreatedFunds = allocations
                .Where(x =>
                    x.Value > 0 &&
                    (!initialData.Any(fund => fund.Asset.Type == x.Key) || initialData.Where(fund => fund.Asset.Type == x.Key).Sum(fund => fund.Amount) == 0))
                .Select(x => new KeyLabelValue<AssetClassType,double>(x.Key, x.Key.DisplayName() + " Fund", x.Value * investment.TotalCoreAssets));

            var fundProportions = new List<KeyLabelValue<AssetClassType,double>>();

            if (scheme.Funded)
            {
                foreach (var fund in initialData)
                    fundProportions.Add(new KeyLabelValue<AssetClassType, double>(fund.Asset.Type, fund.Name, fund.Amount.MathSafeDiv(initialAssetClassTotals[fund.Asset.Type])));
                foreach (var fund in clientCreatedFunds)
                    fundProportions.Add(new KeyLabelValue<AssetClassType, double>(fund.Key, fund.Label, 1));
            }

            var scaledFundAmounts = fundProportions.Select(x => new 
                { 
                    name = x.Label,
                    type = x.Key,
                    category = Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(x.Key).Category,
                    amount = x.Value * allocations[x.Key] * investment.TotalCoreAssets
                });

            var scaledAssets = scaledFundAmounts.GroupBy(x => x.type).Select(x => new { type = x.Key, total = x.Sum(fund => fund.amount) });

            var funds = scaledFundAmounts.Select(x => new AssetFundItem(x.name, new AssetItem(x.type, x.category, scaledAssets.Single(asset => asset.type == x.type).total), x.amount)).ToList();

            if (scheme.Funded)
            {
                if (scheme.CurrentBasis.BasisType != BasisType.Accounting)
                {
                    funds.Add(new AssetFundItem("ABF", new AssetItem(AssetClassType.Abf, Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(AssetClassType.Abf).Category, investment.ABF), investment.ABF));
                }
                funds.Add(new AssetFundItem(AssetClassType.Buyin.DisplayName(), new AssetItem(AssetClassType.Buyin, BuyinCategory, investment.Buyin), investment.Buyin));
            }
            var breakdown = categoriseAssetFunds(funds);

            return breakdown;
        }

        private IEnumerable<BreakdownItem> categoriseAssetFunds(IEnumerable<AssetFundItem> funds)
        {
            var categoryBreakdownItems = new List<BreakdownItem>();
            var fundsGroupedByCategory = funds.GroupBy(x => new { category = x.Asset.Category, type = x.Asset.Type });
            var categories = fundsGroupedByCategory.Select(x => x.Key.category).Distinct();

            foreach (var category in categories.OrderBy(x => x))
            {
                var fundsInCategory =
                    fundsGroupedByCategory
                        .Where(fund => fund.Key.category == category)
                        .SelectMany(fund => fund)
                        .ToList();

                var classes = fundsInCategory.Select(fund => fund.Asset.Type).Distinct();

                var assetBreakdownItems =
                    classes
                        .Select(type => new BreakdownItem(
                            fundsInCategory.First(fund => fund.Asset.Type == type).Asset.Type.DisplayName(),
                            fundsInCategory.First(fund => fund.Asset.Type == type).Asset.Value)
                        {
                            SubItems = fundsInCategory.Where(fund => fund.Asset.Type == type).OrderBy(fund => fund.Name).Select(fund => new BreakdownItem(fund.Name, fund.Amount))
                        });

                var categoryBreakdownItem = new BreakdownItem(
                    (int)category,
                    category.ToString(),
                    fundsInCategory.Sum(fund => fund.Amount))
                {
                    SubItems = assetBreakdownItems.OrderBy(asset => asset.Label)
                };

                categoryBreakdownItems.Add(categoryBreakdownItem);
            }

            return categoryBreakdownItems;
        }
    }
}
