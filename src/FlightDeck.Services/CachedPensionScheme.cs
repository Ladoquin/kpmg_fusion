﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using log4net;
using FlightDeck.Domain.Analysis;
using FlightDeck.Domain.Evolution;
using FlightDeck.Domain.Assets;
using FlightDeck.Domain.Repositories;
using FlightDeck.Domain.LDI;

namespace FlightDeck.Services
{
    public class CachedPensionScheme : ICachedPensionScheme, ISchemeResults, IBasesAssumptionsProvider
    {
        private readonly PensionScheme scheme;
        private readonly ICachedBasisManager cache;
        private readonly ISerializer serializer;
        private readonly IDbInteractionScopeFactory dbScopeFactory;
        private AnalysisParameters parameters;

        public event InputChangeHandler OnInputChange;
        public event InputChangeHandler OnWaterfallChange;

        public IBasis CurrentBasis { get { return cache.CurrentBasis; } }
        public int Id { get { return scheme.Id; } }
        public string Name { get { return scheme.Name; } }
        public string Reference { get { return scheme.Reference; } }
        public DateTime RefreshDate { get { return scheme.RefreshDate; } }
        public SchemeDataSource DataSource { get { return scheme.DataSource; } }
        public IDictionary<AssetClassType, double> Volatilities { get { return scheme.Volatilities; } }
        public IDictionary<AssetClassType, IDictionary<AssetClassType, double>> VarCorrelations { get { return scheme.VarCorrelations; } }
        public IDictionary<NamedPropertyGroupType, NamedPropertyGroup> NamedProperties { get { return scheme.NamedProperties; } }
        public IEnumerable<DateTime> AssetEffectiveDates { get { return scheme.AssetEffectiveDates; } }
        public IDictionary<AssetClassType, AssetClass> AssetDefinitions { get; private set; }
        public BuyinCost BuyinCost { get; set; }
        public DateTime? ContributionsEndDate { get { return !scheme.RecoveryPlanData.IsNullOrEmpty() ? scheme.RecoveryPlanData.Max(x => x.EndDate) : (DateTime?)null; } }
        public IEnumerable<Domain.LDI.LDIEvolutionItem> LDIEvolutionData { get; private set; }
        public bool Funded { get { return scheme.Funded; } }

        public IDictionary<string, IBasisAssumptions> BasisAssumptionsCollection
        {
            get
            {
                return cache.GetAll().ToDictionary(x => x.Name, x => x.GetAssumptions());
            }
        }

        public enum ToContextMode
        {
            Default,
            WithClientAssumptions
        }

        internal CachedPensionScheme(PensionScheme scheme, ICachedBasisManager cachedBasisManager, ISerializer serializer, IDbInteractionScopeFactory dbScopeFactory)
        {
            this.scheme = scheme;
            this.cache = cachedBasisManager;
            this.serializer = serializer;
            this.dbScopeFactory = dbScopeFactory;

            if (Data.SchemeAssets.Any(x => x.LDIInForce))
                SetLiabilityHedging();
            else
                LDIEvolutionData = new List<Domain.LDI.LDIEvolutionItem>();

            cache.SetBases(this);
            
            AssetDefinitions = scheme.GetAssetDefinitions(); //TODO this could be outside of the PensionScheme and cached in the weblayer

            ResetAssumptions();

            //any change on one basis will invalidate results on all other bases
            foreach (var c in cache.GetAll())
                c.OnBasisChange += (sender, assumptionType) =>
                    {
                        foreach (var other in cache.GetAll().Where(x => x.Name != ((ICachedBasis)sender).Name))
                            other.MarkAsStale();
                    };
        }

        //TODO not worked out where these should go yet, refactoring PensionScheme and CachedPensionScheme
        public MasterBasis GetMasterBasis(int masterbasisId)
        {
            return scheme.GetBasis(masterbasisId);
        }
        public IEnumerable<MasterBasisKey> GetMasterbasisKeys()
        {
            return scheme.GetMasterbasisKeys();
        }
        public string DefaultBasisName
        {
            get { return scheme.DefaultBasis; }
        }
        public ISchemeData Data
        {
            get { return scheme; } 
        }
        public ISchemeResults Calculated
        {
            get { return new SchemeResults { BuyinCost = BuyinCost, RefreshDate = RefreshDate, LDIEvolutionData = this.LDIEvolutionData }; }
        }

        #region client assumptions

        private MutableBeforeAfter<RecoveryPlanType> ClientRecoveryPlanOptions;
        private MutableBeforeAfter<WaterfallOptions> ClientWaterfallOptions;
        private MutableBeforeAfter<IDictionary<AssetClassType, double>> ClientInvestmentStrategyAllocations;
        private MutableBeforeAfter<FROType> ClientFroType;
        private MutableBeforeAfter<FlexibleReturnOptions> ClientFroOptions;
        private MutableBeforeAfter<EmbeddedFlexibleReturnOptions> ClientEFroOptions;
        private MutableBeforeAfter<EnhancedTransferValueOptions> ClientEtvOptions;
        private MutableBeforeAfter<PieOptions> ClientPieOptions;
        private MutableBeforeAfter<InsuranceParameters> ClientInsuranceParameters;
        private MutableBeforeAfter<FutureBenefitsParameters> ClientFutureBenefitsParameters;
        private MutableBeforeAfter<InvestmentStrategyOptions> ClientInvestmentStrategyOptions;
        private MutableBeforeAfter<AbfOptions> ClientAbfOptions;
        private Lazy<MutableBeforeAfter<JourneyPlanOptions>> ClientJourneyPlanOptions;

        #region getters

        public BeforeAfter<RecoveryPlanType> GetClientRecoveryPlanOptions()
        {
            return ClientRecoveryPlanOptions;
        }
        public BeforeAfter<IDictionary<AssetClassType, double>> GetClientInvestmentStrategyAllocations()
        {
            return ClientInvestmentStrategyAllocations;
        }
        public BeforeAfter<FROType> GetClientFroType()
        {
            return ClientFroType;
        }
        public BeforeAfter<EmbeddedFlexibleReturnOptions> GetClientEFroOptions()
        {
            return ClientEFroOptions;
        }
        public BeforeAfter<FlexibleReturnOptions> GetClientFroOptions()
        {
            return ClientFroOptions;
        }
        public BeforeAfter<EnhancedTransferValueOptions> GetClientEtvOptions()
        {
            return ClientEtvOptions;
        }
        public BeforeAfter<PieOptions> GetClientPieOptions()
        {
            return ClientPieOptions;
        }
        public BeforeAfter<InsuranceParameters> GetClientInsuranceParameters()
        {
            return ClientInsuranceParameters;
        }
        public BeforeAfter<FutureBenefitsParameters> GetClientFutureBenefitsParameters()
        {
            return ClientFutureBenefitsParameters;
        }
        public BeforeAfter<InvestmentStrategyOptions> GetClientInvestmentStrategyOptions()
        {
            return ClientInvestmentStrategyOptions;
        }
        public BeforeAfter<AbfOptions> GetClientAbfOptions()
        {
            return ClientAbfOptions;
        }
        public BeforeAfter<JourneyPlanOptions> GetClientJourneyPlanOptions()
        {
            return ClientJourneyPlanOptions.Value;
        }
        public BeforeAfter<WaterfallOptions> GetClientWaterfallOptions()
        {
            return ClientWaterfallOptions;
        }

        #endregion

        #region setters

        public void SetClientRecoveryPlanOptions(RecoveryPlanType type)
        {
            ClientRecoveryPlanOptions.Set(type);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.RecoveryPlan);
        }
        public void SetClientInvestmentStrategyAllocations(IDictionary<AssetClassType, double> allocations)
        {
            ClientInvestmentStrategyAllocations.Set(allocations);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.InvestmentStrategyAllocations);
        }
        public void SetClientFroType(FROType froType)
        {
            ClientFroType.Set(froType);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.FROType);
        }
        public void SetClientFroOptions(FlexibleReturnOptions froOptions)
        {
            ClientFroOptions.Set(froOptions);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.FRO);
        }
        public void SetClientEFroOptions(EmbeddedFlexibleReturnOptions efroOptions)
        {
            ClientEFroOptions.Set(efroOptions);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.EFRO);
        }
        public void SetClientEtvOptions(EnhancedTransferValueOptions etvOptions)
        {
            ClientEtvOptions.Set(etvOptions);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.ETV);
        }
        public void SetClientPieOptions(PieOptions pieOptions)
        {
            ClientPieOptions.Set(pieOptions);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.PIE);
        }
        public void SetClientInsuranceParameters(InsuranceParameters parameters)
        {
            ClientInsuranceParameters.Set(parameters);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.InsuredLiabilityPercentage);
        }
        public void SetClientFutureBenefitsParameters(FutureBenefitsParameters parameters)
        {
            ClientFutureBenefitsParameters.Set(parameters);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.FutureBenefits);
        }
        public void SetClientInvestmentStrategyOptions(InvestmentStrategyOptions options)
        {
            ClientInvestmentStrategyOptions.Set(options);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.InvestmentStrategyOptions);
        }
        public void SetClientAbfOptions(AbfOptions options)
        {
            ClientAbfOptions.Set(options);
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.ABF);
        }
        public void SetClientJourneyPlanOptions(JourneyPlanOptions options)
        {
            ClientJourneyPlanOptions.Value.Set(options);
        }
        public void SetClientWaterfallOptions(WaterfallOptions options)
        {
            ClientWaterfallOptions.Set(options);
            if (OnWaterfallChange != null)
                OnWaterfallChange(this, AssumptionChangeType.Waterfall);
        }

        #endregion

        #region resetters

        public void ResetClientRecoveryPlanOptions()
        {
            ClientRecoveryPlanOptions.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.RecoveryPlan);
        }
        public void ResetClientInvestmentStrategyAllocations()
        {
            ClientInvestmentStrategyAllocations.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.InvestmentStrategyAllocations);
        }
        public void ResetClientFroType()
        {
            ClientFroType.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.FROType);
        }
        public void ResetClientFroOptions()
        {
            ClientFroOptions.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.FRO);
        }
        public void ResetClientEFroOptions()
        {
            ClientEFroOptions.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.EFRO);
        }
        public void ResetClientEtvOptions()
        {
            ClientEtvOptions.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.ETV);
        }
        public void ResetClientPieOptions()
        {
            ClientPieOptions.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.PIE);
        }
        public void ResetClientInsuranceParameters()
        {
            ClientInsuranceParameters.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.InsuredLiabilityPercentage);
        }
        public void ResetClientFutureBenefitsParameters()
        {
            ClientFutureBenefitsParameters.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.FutureBenefits);
        }
        public void ResetClientInvestmentStrategyOptions()
        {
            ClientInvestmentStrategyOptions.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.InvestmentStrategyOptions);
        }
        public void ResetClientAbfOptions()
        {
            ClientAbfOptions.Reset();
            if (OnInputChange != null)
                OnInputChange(this, AssumptionChangeType.ABF);
        }
        public void ResetClientJourneyPlanOptions()
        {
            ClientJourneyPlanOptions.Value.Reset();
        }

        #endregion

        public void ResetAssumptions()
        {
            var defaultAssumptions = getDefaultAssumptions(CurrentBasis.AttributionEnd);

            ClientFroType = new MutableBeforeAfter<FROType>(defaultAssumptions.FROType);
            ClientFroOptions = new MutableBeforeAfter<FlexibleReturnOptions>(defaultAssumptions.FROOptions);
            ClientEFroOptions = new MutableBeforeAfter<EmbeddedFlexibleReturnOptions>(defaultAssumptions.EFROOptions);
            ClientEtvOptions = new MutableBeforeAfter<EnhancedTransferValueOptions>(defaultAssumptions.ETVOptions);
            ClientPieOptions = new MutableBeforeAfter<PieOptions>(defaultAssumptions.PIEOptions);
            ClientFutureBenefitsParameters = new MutableBeforeAfter<FutureBenefitsParameters>(defaultAssumptions.FutureBenefitOptions);
            ClientInvestmentStrategyAllocations = new MutableBeforeAfter<IDictionary<AssetClassType, double>>(defaultAssumptions.InvestmentStrategyAssetAllocation);
            ClientInvestmentStrategyOptions = new MutableBeforeAfter<InvestmentStrategyOptions>(defaultAssumptions.InvestmentStrategyOptions);
            ClientAbfOptions = new MutableBeforeAfter<AbfOptions>(defaultAssumptions.ABFOptions);
            ClientInsuranceParameters = new MutableBeforeAfter<InsuranceParameters>(defaultAssumptions.InsuranceOptions);
            ClientRecoveryPlanOptions = new MutableBeforeAfter<RecoveryPlanType>(defaultAssumptions.RecoveryPlanOptions);
            //Journey plan options don't fit into the standard client options pattern because they aren't used in
            //analysis calculations, and they require the current basis' context to work out their settings.
            //This makes it hard to initialise the scheme assumptions because it needs the basis context, but the basis context needs the scheme context - ie, circular reference problem
            //So have to manage client journey plan options separately
            ClientJourneyPlanOptions = new Lazy<MutableBeforeAfter<JourneyPlanOptions>>(() => new MutableBeforeAfter<JourneyPlanOptions>(getDefaultOtherAssumptions()));

            //Only initialise ClientWaterfallOptions with the defaults once as we don't want to reset them after client changes.
            if(ClientWaterfallOptions == null)
                ClientWaterfallOptions = new MutableBeforeAfter<WaterfallOptions>(defaultAssumptions.WaterfallOptions);

            BuyinCost = new BuyinCost(0, 0);
        }

        public bool IsAnalysisAssumptionsDirty
        {
            get
            {
                return
                    ClientAbfOptions.IsDirty ||
                    ClientEtvOptions.IsDirty ||
                    ClientFroType.IsDirty ||
                    ClientEFroOptions.IsDirty ||
                    ClientFroOptions.IsDirty ||
                    ClientFutureBenefitsParameters.IsDirty ||
                    ClientInsuranceParameters.IsDirty ||
                    ClientInvestmentStrategyAllocations.IsDirty ||
                    ClientInvestmentStrategyOptions.IsDirty ||
                    ClientPieOptions.IsDirty ||
                    ClientRecoveryPlanOptions.IsDirty;
            }
        }

        #endregion

        public AnalysisParameters AnalysisParameters
        {
            get
            {
                if (parameters == null)
                {
                    var analysisParameters = scheme.AnalysisParameters;
                    var applicationParameters = scheme.ApplicationParameters;

                    var defaultFRO = cache.DefaultBasis.GetFroData().Before;

                    var froMin = analysisParameters != null && analysisParameters.FROMinTVAmount.HasValue
                        ? analysisParameters.FROMinTVAmount.Value
                        : defaultFRO.Over55 * applicationParameters.FROMinTVPercentage;
                    var froMax = analysisParameters != null && analysisParameters.FROMaxTVAmount.HasValue
                        ? analysisParameters.FROMaxTVAmount.Value
                        : defaultFRO.Over55 * applicationParameters.FROMaxTVPercentage;
                    var etvMin = analysisParameters != null && analysisParameters.ETVMinTVAmount.HasValue
                        ? analysisParameters.ETVMinTVAmount.Value
                        : defaultFRO.Under55 * applicationParameters.ETVMinTVPercentage;
                    var etvMax = analysisParameters != null && analysisParameters.ETVMaxTVAmount.HasValue
                        ? analysisParameters.ETVMaxTVAmount.Value
                        : defaultFRO.Under55 * applicationParameters.ETVMaxTVPercentage;

                    parameters = new AnalysisParameters(froMin, froMax, etvMin, etvMax);
                }
                return parameters;
            }
        }

        public AccountingSettings AccountingSettings
        {
            get
            {
                return new AccountingSettings(scheme.AccountingDefaultStandard, scheme.AccountingIAS19Visible, scheme.AccountingFRS17Visible, scheme.AccountingUSGAAPVisible);
            }
        }

        public IDictionary<string, Tuple<IDictionary<StrainType, double>, DateTime>> GetStrains()
        {
            var strains = new InsuranceStrainCalculator(ToContext(), CurrentBasis.AttributionEnd).Calculate(
                cache.GetAll().ToDictionary(
                    x => new MasterBasisKey(x.MasterBasisId, x.BasisType, x.Name, Id),
                    x => x.Evolution));

            var result = strains.ToDictionary(
                x => cache.GetBasis(x.Key).Name,
                x => Tuple.Create(x.Value, cache.GetBasis(x.Key).AnchorDate));

            return result;
        }

        public ISchemeContext ToContext(CachedPensionScheme.ToContextMode contextMode = CachedPensionScheme.ToContextMode.WithClientAssumptions)
        {
            return new SchemeContext
                (
                    scheme,
                    contextMode == ToContextMode.WithClientAssumptions ? getAssumptions() : getDefaultAssumptions(CurrentBasis.AttributionEnd),
                    contextMode == ToContextMode.WithClientAssumptions ? (ISchemeResults)this : new SchemeResults { RefreshDate = RefreshDate, BuyinCost = new BuyinCost(0, 0), LDIEvolutionData = this.LDIEvolutionData },
                    this
                );
        }

        /// <summary>
        /// The idea of an AdjustedDefaultContext is that it contains sticky scheme assumptions that have a default but that should remain unaffected by a reset 
        /// The sticky assumptions are initialised in InitialiseStickyAssumptions()
        /// </summary>
        public ISchemeContext ToAdjustedDefaultContext()
        {
            var adjustedDefaultAssumptions = getDefaultAssumptions(CurrentBasis.AttributionEnd);

            adjustedDefaultAssumptions.WaterfallOptions = ClientWaterfallOptions.After;

            return new SchemeContext
                (
                    scheme,
                    adjustedDefaultAssumptions,
                    new SchemeResults { RefreshDate = RefreshDate, BuyinCost = new BuyinCost(0, 0), LDIEvolutionData = this.LDIEvolutionData },
                    this
                );
        }

        private ISchemeAssumptions getDefaultAssumptions(DateTime at)
        {
            var ldiHedgeData = GetLdiHedgingData(at);
            var ldiInterestHedge = ldiHedgeData != null ? ldiHedgeData.InterestRate : .0;
            var ldiInflationHedge = ldiHedgeData != null ? ldiHedgeData.Inflation : .0;

            var syntheticData = GetSyntheticAssetInfo(at);
            var syntheticCredit = syntheticData != null ? syntheticData.CreditProportion : .0;
            var syntheticEquity = syntheticData != null ? syntheticData.EquityProportion : .0;

            var assumptions = new SchemeAssumptions(
                FROType.Bulk,
                new FlexibleReturnOptions(0, 0),
                new EmbeddedFlexibleReturnOptions(0, 0),
                new EnhancedTransferValueOptions(0, 0, 0),
                new PieOptions(0, 0.2, 0),
                new FutureBenefitsParameters(0, BenefitAdjustmentType.None, 0),
                getDefaultInvestmentStrategyAllocations(at),
                new InvestmentStrategyOptions(0, syntheticCredit, syntheticEquity, ldiHedgeData != null, ldiInterestHedge, ldiInflationHedge),
                new AbfOptions(0),
                new InsuranceParameters(0, 0),
                RecoveryPlanType.Current,
                new WaterfallOptions(0.95, 3));

            return assumptions;
        }

        private IDictionary<AssetClassType, double> getDefaultInvestmentStrategyAllocations(DateTime at)
        {
            var assetEffectiveDate = scheme.SchemeAssets.Select(x => x.EffectiveDate).Distinct().Where(x => x <= at).OrderBy(x => x).Last();
            var schemeAssets = scheme.SchemeAssets.Where(x => x.EffectiveDate == assetEffectiveDate);
            var assets = schemeAssets.SelectMany(x => x.Assets);
            var assetTotal = assets.Sum(x => x.Value);
            var allocations = assets.ToDictionary(x => x.Class.Type, x => x.Value.MathSafeDiv(assetTotal));

            foreach (var assetClass in Utils.EnumToArray<AssetClassType>())
            {
                var attr = Utils.GetEnumAttribute<AssetClassType, AssetClassTypeClientVisibleAttribute>(assetClass);
                if (attr != null && attr.ClientVisible)
                {
                    if (assetClass != AssetClassType.Abf && assetClass != AssetClassType.Buyin)
                    {
                        if (!allocations.ContainsKey(assetClass))
                        {
                            allocations.Add(assetClass, 0);
                        }
                    }
                }
            }

            return allocations;
        }

        /// <summary>
        /// See note in ResetAssumptions about why journey plan options are being managed separately. 
        /// Have called this 'other' assumptions because it's possible other assumptions will come along that aren't relevant to analysis calculations and can go here too.
        /// </summary>
        /// <returns></returns>
        private JourneyPlanOptions getDefaultOtherAssumptions()
        {
            var context = cache.CurrentBasis.ToContext(CachedBasis.ToContextMode.WithAnalysisResults);

            var technicalProvisionsBasis = cache.GetFirstBasisOfType(BasisType.TechnicalProvision);
            var accountingBasis = cache.GetFirstBasisOfType(BasisType.Accounting); //assuming this would never be null in real life?
            var buyoutBasis = cache.GetFirstBasisOfType(BasisType.Buyout);

            if (technicalProvisionsBasis == null && accountingBasis == null) //Can't do JP without TP or Accounting basis.
            {
                LogManager.GetLogger(this.GetType()).Error(string.Format("Cannot load Journey Plan for '{0}' as there is no Technical Provision or Accounting basis.", Name));
                return null;
            }

            var fundingBasisKey = 0;

            if (technicalProvisionsBasis != null)
                fundingBasisKey = technicalProvisionsBasis.MasterBasisId;
            else if (accountingBasis != null)
                fundingBasisKey = accountingBasis.MasterBasisId;

            var journeyPlanOptions = new JourneyPlanOptions
                {
                    FundingBasisKey = fundingBasisKey,
                    SelfSufficiencyBasisKey = CurrentBasis.MasterBasisId,
                    BuyoutBasisKey = buyoutBasis != null ? buyoutBasis.MasterBasisId : 0,
                    RecoveryPlan = new NewJPExtraContributionOptions(1, 0, 1, 0, true, false),
                    SalaryInflationType = SalaryType.CPI,
                    TriggerSteps = 1,
                    FundingLevel = 0.8,
                    FinalReturn = Math.Round(context.Results.JP2InvGrowth - new GiltYieldService(context.Data).GetGiltYield(context.AnalysisDate), 4),
                    CurrentReturn = Math.Round(context.Results.JP2InvGrowth - new GiltYieldService(context.Data).GetGiltYield(context.AnalysisDate), 4),
                    InitialReturn = Math.Round(context.Results.JP2InvGrowth - new GiltYieldService(context.Data).GetGiltYield(context.AnalysisDate), 4),
                    SelfSufficiencyDiscountRate = 0.005,
                    FundingLevelTrigger = false,
                    TriggerTimeStartYear = 5,
                    TriggerTransitionPeriod = 20
                };
            return journeyPlanOptions;
        }

        private ISchemeAssumptions getAssumptions()
        {
            return new SchemeAssumptions
                (
                    CurrentBasis.BasisType == BasisType.Buyout ? FROType.Bulk : ClientFroType.After,
                    ClientFroOptions.After,
                    ClientEFroOptions.After,
                    ClientEtvOptions.After,
                    ClientPieOptions.After,
                    ClientFutureBenefitsParameters.After,
                    ClientInvestmentStrategyAllocations.After,
                    ClientInvestmentStrategyOptions.After,
                    ClientAbfOptions.After,
                    ClientInsuranceParameters.After,
                    ClientRecoveryPlanOptions.After,
                    ClientWaterfallOptions.After
                );
        }

        public IBasis SwitchBasis(int masterBasisId)
        {
            cache.SetCurrent(masterBasisId);

            return cache.CurrentBasis;
        }

        public IEnumerable<IBasis> GetAllBases()
        {
            return cache.GetAll();
        }

        public IEnumerable<BasisType> GetBasisTypes()
        {
            return scheme.GetMasterbasisKeys().Select(x => x.BasisType).Distinct();
        }

        public IBasis GetBasis(int masterBasisId)
        {
            return cache.GetBasis(masterBasisId);
        }

        public bool SetAttributionPeriod(DateTime start, DateTime end)
        {
            if (start == DateTime.MinValue ||
                start == DateTime.MaxValue ||
                end == DateTime.MinValue ||
                end == DateTime.MaxValue ||
                start > end ||
                start < CurrentBasis.AnchorDate ||
                end > scheme.RefreshDate)
            {
                return false;
            }

            foreach (var b in cache.GetAll())
            {
                b.SetAttributionPeriod(start, end);
            }

            return true;
        }

        public void ResetAttributionPeriod()
        {
            foreach (var b in cache.GetAll())
            {
                b.ResetAttributionPeriod();
            }
        }

        public IBasis GetDefaultBasis(BasisType type)
        {
            return cache.GetFirstBasisOfType(type);
        }

        public IEnumerable<BasisIdentifier> GetBasesIdentifiers()
        {
            return scheme.GetMasterbasisKeys().Select(x => new BasisIdentifier(x.Id, x.BasisType, x.Name));
        }

        public bool HasBasisOfType(BasisType type)
        {
            return scheme.GetMasterbasisKeys().Any(b => b.BasisType == type);
        }


        /// <summary>
        /// Synthetics at the attibution end date
        /// </summary>
        public SyntheticAssetInfo GetSyntheticAssetInfo()
        {
            var at = CurrentBasis.AttributionEnd;
            var schemeAsset = scheme.SchemeAssets.Where(a => a.EffectiveDate <= at).OrderBy(a => a.EffectiveDate).Last();
            var syntheticAssetInfo = new SyntheticAssetInfo(schemeAsset.PropSyntheticEquity, schemeAsset.PropSyntheticCredit);
            return syntheticAssetInfo;
        }

        /// <summary>
        /// Synthetics at a given date
        /// </summary>
        public SyntheticAssetInfo GetSyntheticAssetInfo(DateTime at)
        {
            var schemeAsset = scheme.SchemeAssets.Where(a => a.EffectiveDate <= at).OrderBy(a => a.EffectiveDate).Last();
            var syntheticAssetInfo = new SyntheticAssetInfo(schemeAsset.PropSyntheticEquity, schemeAsset.PropSyntheticCredit);
            return syntheticAssetInfo;
        }

        public IEnumerable<AssetFundItem> GetAssetData(DateTime at)
        {
            var schemeAsset = scheme.GetSchemeAsset(at);

            var assets = schemeAsset.Assets.ToDictionary(x => x.Id);

            var assetFunds = schemeAsset.Assets.SelectMany(x => x.AssetFunds)
                .Select(x => new AssetFundItem
                    (
                        x.Name,
                        new AssetItem(assets[x.AssetId].Class.Type, assets[x.AssetId].Class.Category, assets[x.AssetId].Value),
                        x.Amount
                    ));

            return assetFunds;
        }

        public MarketInfo GetMarketInfo()
        {
            return scheme.GetMarketInfo(CurrentBasis.AttributionStart, CurrentBasis.AttributionEnd);
        }

        public IEnumerable<AssetFundGrowth> GetAssetsGrowthInfo()
        {
            return scheme.GetGrowthOverPeriod(CurrentBasis.AttributionStart, CurrentBasis.AttributionEnd);
        }

        public NewJPData GetNewJP()
        {
            CurveDataProjection curveData = null;

            var opts = ClientJourneyPlanOptions.Value.After;

            if (opts == null)//we've failed to initialse JP assumtions for some reason (e.g. missing funding basis) and the JP won't work
                return null;

            //can't include bases that didn't exist at the analysis date
            Func<int, IBasisContext> tryGetBasisAtDate = id =>
                {
                    var b = cache.GetBasis(id);
                    if (b != null)
                    {
                        var ctx = b.ToContext();
                        if (ctx.EvolutionData.LiabilityEvolution.Keys.Min() <= cache.CurrentBasis.AttributionEnd)
                        {
                            return ctx;
                        }
                    }
                    return null;
                };

            var selfSufficiencyBasis = tryGetBasisAtDate(opts.SelfSufficiencyBasisKey);
            var buyoutBasis = tryGetBasisAtDate(opts.BuyoutBasisKey);
            var fundingBasis = tryGetBasisAtDate(opts.FundingBasisKey);

            if (fundingBasis == null)//calcs won't work
            {
                LogManager.GetLogger(this.GetType()).Error(string.Format("Cannot load Journey Plan for '{0}' as there is no Funding basis. A Technical Provision or Accounting Basis needs to be present and exist at the current analysis date.", Name));
                return null;
            }

            var parms = new NewJPParameters
                {
                    CashflowBasis = cache.GetBasis(CurrentBasis.MasterBasisId).ToContext(),
                    AnalysisDate = CurrentBasis.AttributionEnd,
                    FundingBasis = fundingBasis,
                    SelfSufficiencyBasis = selfSufficiencyBasis,
                    BuyoutBasis = buyoutBasis,
                    RecoveryPlan = opts.RecoveryPlan,
                    SalaryInflationType = SalaryType.CPI,
                    CurveDataProjection = curveData,
                    InvGrowthRateBefore = opts.CurrentReturn,
                    SelfSufficiencyDiscountRate = opts.SelfSufficiencyDiscountRate,
                    FundingLevel = opts.FundingLevel,
                    InitialGrowthRate = opts.InitialReturn,
                    TargetGrowthRate = opts.FinalReturn,
                    TriggerSteps = opts.TriggerSteps,
                    FundingLevelTrigger = opts.FundingLevelTrigger,
                    TriggerTimeStartYear = opts.TriggerTimeStartYear,
                    TriggerTransitionPeriod = opts.TriggerTransitionPeriod,
                    UseCurves = opts.UseCurves,
                    IncludeBuyIns = opts.IncludeBuyIns
                };

            var jp = new NewJP(ToContext(), cache.CurrentBasis.ToContext(CachedBasis.ToContextMode.WithAnalysisResults)).Calculate(parms);

            return jp;
        }

        /// <summary>
        /// Liability hedging data contains LDI inflation swaps and LDI interest swaps
        /// </summary>
        private void SetLiabilityHedging()
        {
            LDIEvolutionData = new List<LDIEvolutionItem>();

            // Check for pre-calculated LDI in the db cache
            using (var db = dbScopeFactory.GetScope())
            {
                var cache = ((IPensionSchemeCacheRepository)db.GetRepository<PensionSchemeCache>()).GetBySchemeId(scheme.Id);
                if (cache != null && cache.LDIEvolutionItems != null)
                {
                    LDIEvolutionData = serializer.DeserializeObject<IEnumerable<LDIEvolutionItem>>(cache.LDIEvolutionItems).ToList();
                }
            }

            // Check for gap between our last saved calculation date (if any) and the required Refresh date
            var anchor = scheme.LDIBasesDataManager.AnchorDate;
            var startFrom = LDIEvolutionData.Any() ? LDIEvolutionData.Max(x => x.Date).AddDays(1) : anchor;

            if (startFrom <= RefreshDate)
            {
                // Get the LDI Evolution data only for the days we don't already have cached results for
                var ldiEvolutionService = new LDIEvolutionService(scheme.LDIBasesDataManager, Data);
                var results = ldiEvolutionService.GetLiabilityHedgingData(startFrom, RefreshDate);
                // Results always gives a starting point, discard this if we're appending to existing values
                if (LDIEvolutionData.Any())
                {
                    results = results.OrderBy(d => d.Date).Skip(1);
                }
                LDIEvolutionData = LDIEvolutionData.Union(results).ToList();

                // Update the cache now with the full set of evolution data
                var cache = new PensionSchemeCache()
                {
                    PensionSchemeId = scheme.Id,
                    LDIEvolutionItems = serializer.SerializeObject<IEnumerable<LDIEvolutionItem>>(LDIEvolutionData)
                };
                using (var db = dbScopeFactory.GetScope())
                {
                    db.GetRepository<PensionSchemeCache>().InsertOrUpdate(cache);
                }
            }

            if (LDIEvolutionData.Count() != LDIEvolutionData.Select(x => x.Date).Distinct().Count())
            {
                var grouping = LDIEvolutionData.GroupBy(x => x.Date).Where(x => x.Count() > 1).Select(x => x.Key.ToString("dd/MMM/yyyy")).ToList();
                foreach (var g in grouping)
                {
                    System.Diagnostics.Debug.WriteLine(g);
                }
                foreach (var g in grouping)
                {
                    System.Diagnostics.Debug.WriteLine(g);
                }
            }
        }

        public LDIHedgingData GetLdiHedgingData(DateTime at)
        {
            var assetData = Data.GetSchemeAsset(at);

            if (assetData != null && assetData.LDIInForce)
                return new LDIHedgingData(assetData.InflationHedge, assetData.InterestHedge);
            else
                return null;
        }

    }
}