﻿namespace FlightDeck.Services
{
    using FlightDeck.DataAccess;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;

    public class UserProfileService : IUserProfileService
    {
        private readonly IRoleManager _roleManager;
        private readonly IPrincipal _principal;
        private readonly IDbInteractionScopeFactory _dbScopeFactory;

        private UserProfile __pp;
        private UserProfile getPrincipalProfile()
        {
            if (__pp == null)
            {
                using (var db = _dbScopeFactory.GetScope())
                {
                    __pp = ((IUserProfileRepository)db.GetRepository<UserProfile>()).GetByUsername(_principal.Identity.Name);
                }
            }
            return __pp;
        }

        public UserProfileService(IPrincipal principal, IRoleManager roleManager)
            : this(principal, roleManager, new DbInteractionScopeFactory())
        {
        }

        public UserProfileService(IPrincipal principal, IRoleManager roleManager, IDbInteractionScopeFactory dbScopeFactory)
        {
            _roleManager = roleManager;
            _principal = principal;
            _dbScopeFactory = dbScopeFactory;
        }

        private IEnumerable<UserProfileSearchKeys> getUsersInScope(IEnumerable<UserProfileSearchKeys> keys)
        {
            var requester = getPrincipalProfile();

            using (var db = _dbScopeFactory.GetScope())
            {
                var repo = (IUserProfileRepository)db.GetRepository<UserProfile>();                

                if (requester != null)
                {
                    // apply scope permissions
                    switch (requester.Role)
                    {
                        case RoleType.Admin:
                            break;
                        case RoleType.Installer:
                            // not sure of the exact requirements here yet so will allow installers to see: 
                            // themselves (obv!)
                            // all users they have created
                            // all installer and consultant users
                            // clients that are in at least one scheme the installer can see
                            var unrestrictedSchemeIds = ((ISchemeDetailRepository)db.GetRepository<SchemeDetail>()).GetAllUnrestricted();
                            keys = keys.Where(x =>
                                x.UserId == requester.UserId ||
                                x.CreatedByUserId == requester.UserId ||
                                x.Role == RoleType.Installer || x.Role == RoleType.Consultant ||
                                (
                                (x.Role == RoleType.Client || x.Role == RoleType.Adviser) &&
                                x.AccessibleSchemeIds.IsNullOrEmpty() == false &&
                                x.AccessibleSchemeIds.Intersect(unrestrictedSchemeIds.Union(requester.SchemeDetails.Select(sd => sd.SchemeDetailId))).Any()
                                ));
                            break;
                        case RoleType.Consultant:
                        case RoleType.Client:
                        default:
                            keys = keys.Where(x => x.UserId == requester.UserId);
                            break;
                    }

                    return keys;
                }
            }

            return new List<UserProfileSearchKeys>();
        }

        private UserProfile checkRequestInScope(UserProfile user)
        {
            if (user != null)
            {
                if (getUsersInScope(new List<UserProfileSearchKeys> { new UserProfileSearchKeys 
                    { 
                        UserId = user.UserId, 
                        AccessibleSchemeIds = user.SchemeDetails.Select(x => x.SchemeDetailId).ToList(),
                        LastName = user.LastName,
                        CreatedByUserId = user.CreatedByUserId,
                        Role = user.Role
                    }
                    }).Any())
                {
                    return filterRestrictedSchemes(user);
                }
            }
            return null;
        }

        public UserSearchResult Get(UserProfileSearchCriteria criteria, int pageIdx, int pageSize)
        {
            using (var db = new DbInteractionScope())
            {
                var userRepo = (IUserProfileRepository)db.GetRepository<UserProfile>();

                var requestor = getPrincipalProfile();

                var keys = getUsersInScope(userRepo.GetSearchKeys(new List<int> { requestor.UserId }));

                // apply search criteria
                if (!string.IsNullOrEmpty(criteria.SchemeFilter))
                {
                    var schemeId = ((ISchemeDetailRepository)db.GetRepository<SchemeDetail>()).GetByName(criteria.SchemeFilter).SchemeDetailId;
                    keys = keys.Where(x => x.AccessibleSchemeIds.Contains(schemeId));
                }
                if (criteria.RoleFilter.HasValue)
                {
                    keys = keys.Where(x => x.Role == criteria.RoleFilter.Value);
                }

                keys = criteria.NameDescending
                    ? keys.OrderByDescending(x => x.LastName)
                    : keys.OrderBy(x => x.LastName);

                var set = keys.Skip(pageIdx * pageSize).Take(pageSize).Select(x => x.UserId);

                var userPageSet = filterRestrictedSchemes(userRepo.GetMany(set));

                var result = new UserSearchResult
                    {
                        UserPageSet = userPageSet,
                        TotalMatches = keys.Count()
                    };

                return result;
            }
        }

        public UserProfile GetById(int id)
        {
            using (var db = _dbScopeFactory.GetScope())
            {
                var user = ((IUserProfileRepository)db.GetRepository<UserProfile>()).GetById(id);

                return checkRequestInScope(user);
            }
        }

        public UserProfile GetByUsername(string username, bool allowAnonymous = false)
        {
            using (var db = new DbInteractionScope())
            {
                var user = ((IUserProfileRepository)db.GetRepository<UserProfile>()).GetByUsername(username);

                if (!allowAnonymous)
                {
                    user = checkRequestInScope(user);
                }

                return user;
            }
        }

        public UserProfile GetByEmail(string email, bool allowAnonymous = false)
        {
            using (var db = new DbInteractionScope())
            {
                var user = ((IUserProfileRepository)db.GetRepository<UserProfile>()).GetByEmail(email);

                if (!allowAnonymous)
                {
                    user = checkRequestInScope(user);
                }

                return user;
            }
        }

        public Dictionary<string, int> GetSearchIndex()
        {
            return GetSearchIndex(null);
        }

        public Dictionary<string, int> GetSearchIndex(IEnumerable<int> excludeUserIds)
        {
            using (var db = new DbInteractionScope())
            {
                var userRepo = (IUserProfileRepository)db.GetRepository<UserProfile>();

                var userIds = getUsersInScope(userRepo.GetSearchKeys(excludeUserIds)).Select(k => k.UserId).ToList();                

                return 
                    userRepo.GetAll()
                        .Where(x => userIds.Contains(x.UserId))
                        .ToDictionary(
                            u => u.FirstName + ' ' + u.LastName + " (" + u.UserName + ") " + u.EmailAddress,
                            u => u.UserId,
                            StringComparer.OrdinalIgnoreCase);
            }
        }

        public ActionResult<IUserProfileServiceResult> Create(string username, RoleType role, IEnumerable<string> schemeNames = null, string firstname = null, string lastname = null, string email = null)
        {
            return save(true, username, role, schemeNames, firstname, lastname, email, null);
        }

        public ActionResult<IUserProfileServiceResult> Update(string username, RoleType? role = null, IEnumerable<string> schemeNames = null, string firstname = null, string lastname = null, string email = null, string updatedusername = null)
        {
            return save(false, username, role.GetValueOrDefault(_roleManager.GetRole(username)), schemeNames, firstname, lastname, email, updatedusername);
        }

        public ActionResult<IUserProfileServiceResult> ChangeActiveScheme(string username, string schemeName)
        {
            UserProfile user;

            using (var db = new DbInteractionScope())
            {
                var userRepo = (IUserProfileRepository)db.GetRepository<UserProfile>();

                user = userRepo.GetByUsername(username);

                var userHasAccess = user.SchemeDetails.Any(s => s.SchemeName == schemeName);

                if(userHasAccess == false && (user.Role == RoleType.Client || user.Role == RoleType.Adviser))
                    return new ActionResult<IUserProfileServiceResult>(true);

                if (string.IsNullOrEmpty(schemeName))
                {
                    user.ActiveSchemeDetailId = null;
                }
                else
                {
                    var scheme = ((ISchemeDetailRepository)db.GetRepository<SchemeDetail>()).GetByName(schemeName);

                    if (scheme == null)
                    {
                        return new ActionResult<IUserProfileServiceResult>(IUserProfileServiceResult.SchemeNotFound);
                    }

                    user.ActiveSchemeDetailId = scheme.SchemeDetailId;
                }

                userRepo.Update(user);
            }

            return new ActionResult<IUserProfileServiceResult>(true);
        }

        private ActionResult<IUserProfileServiceResult> save(bool @new, string username, RoleType role, IEnumerable<string> schemeNames = null, string firstname = null, string lastname = null, string email = null, string updatedusername = null)
        {
            UserProfile profile = null;
            IUserProfileServiceResult? error = null;
            IEnumerable<SchemeDetail> schemes = null;
            IUserProfileRepository userRepo = null;

            using (var db = new DbInteractionScope())
            {
                var schemeRepo = (ISchemeDetailRepository)db.GetRepository<SchemeDetail>();

                // do schemes exist?
                if (!schemeNames.IsNullOrEmpty())
                {
                    schemes = schemeRepo.GetByNames(schemeNames);
                    if (schemes == null || schemes.Count() != schemeNames.Count())
                    {
                        error = IUserProfileServiceResult.SchemeNotFound;
                    }
                }                

                if (error == null)
                {
                    // is username already in user?
                    userRepo = (IUserProfileRepository)db.GetRepository<UserProfile>();
                    if (@new || (!string.IsNullOrEmpty(updatedusername) && !string.Equals(username, updatedusername, System.StringComparison.CurrentCultureIgnoreCase)))
                    {
                        if (userRepo.GetByUsername(@new ? username : updatedusername) != null)
                        {
                            error = IUserProfileServiceResult.UsernameConflict;
                        }
                    }
                }

                if (error == null)
                {

                    if (@new) //never used?
                    {
                        userRepo.Insert(
                            new UserProfile(
                                0,
                                username,
                                firstname,
                                lastname,
                                email,
                                false,
                                true,
                                schemes,
                                (role == RoleType.Client || role == RoleType.Adviser) && schemes.Any() 
                                        ? schemes.First().SchemeDetailId 
                                        : (int?)null));
                    }
                    else
                    {
                        profile = userRepo.GetByUsername(username);

                        var activeSchemeId = profile.ActiveSchemeDetailId;
                        if (schemeNames != null)
                        {
                            if (role == RoleType.Client || role == RoleType.Adviser)
                            {
                                activeSchemeId = !schemeNames.IsNullOrEmpty() ? schemes.First().SchemeDetailId : (int?)null;
                            }

                            // schemes submitted may not be full set because updating user may not have access to all restricted schemes
                            // so make sure don't delete access to restricted schemes the updater is oblivious to
                            var requestingUser = getPrincipalProfile();
                            if (requestingUser.Role != RoleType.Admin)
                            {
                                var schemesHiddenToRequester = profile.SchemeDetails.Where(sd =>
                                    sd.IsRestricted &&
                                    profile.SchemeDetails.Select(x => x.SchemeDetailId)
                                        .Except(requestingUser.SchemeDetails.Select(x => x.SchemeDetailId))
                                            .Contains(sd.SchemeDetailId));

                                schemes = schemes ?? new List<SchemeDetail>();
                                schemes = schemes.Union(schemesHiddenToRequester);
                            }

                            if (role == RoleType.Installer || role == RoleType.Consultant)
                            {
                                // If the user has an active scheme that is restricted but no longer 
                                // has access to that scheme then remove it as the users active scheme.
                                if (activeSchemeId.HasValue && profile.ActiveSchemeDetail.IsRestricted && 
                                    (schemes == null || schemes.SingleOrDefault(s => s.IsRestricted && s.SchemeDetailId == activeSchemeId) == null ))
                                {
                                    activeSchemeId = null;
                                }
                            }
                        }

                        userRepo.Update(
                            new UserProfile(
                                profile.UserId,
                                updatedusername ?? profile.UserName,
                                firstname ?? profile.FirstName,
                                lastname ?? profile.LastName,
                                email ?? profile.EmailAddress,
                                profile.TermsAccepted,
                                profile.PasswordMustChange,
                                schemeNames != null ? schemes : profile.SchemeDetails,
                                activeSchemeId));
                    }

                    _roleManager.Delete(updatedusername ?? username);
                    _roleManager.AddUserToRole(updatedusername ?? username, role.ToString());
                }
            }

            return error == null
                ? new ActionResult<IUserProfileServiceResult>(true)
                : new ActionResult<IUserProfileServiceResult>(error.Value);
        }

        private UserProfile filterRestrictedSchemes(UserProfile user)
        {
            return filterRestrictedSchemes(new List<UserProfile> { user }).Single();
        }

        private IEnumerable<UserProfile> filterRestrictedSchemes(IEnumerable<UserProfile> users)
        {
            var requestingUser = getPrincipalProfile();

            var requesterRestrictedSchemeIds = requestingUser.SchemeDetails.Select(x => x.SchemeDetailId).ToList();

            foreach (var user in users)
            {
                if (requestingUser.Role != RoleType.Admin)
                {
                    user.SchemeDetails = user.SchemeDetails.Where(scheme => !scheme.IsRestricted || requesterRestrictedSchemeIds.Contains(scheme.SchemeDetailId));
                }

                yield return user;
            }
        }

        public void DeleteUser(string username)
        {
            using (var db = new DbInteractionScope())
            {
                ((IUserProfileRepository)db.GetRepository<UserProfile>()).Delete(username);
            }
        }

        public void DeleteUsers(IEnumerable<int> ids)
        {
            using (var db = new DbInteractionScope())
            {
                ((IUserProfileRepository)db.GetRepository<UserProfile>()).Delete(ids);
            }
        }

        public void DeleteSchemeAccess(int userId, int schemeId)
        {
            using (var db = new DbInteractionScope())
            {
                var repo = (IUserProfileRepository)db.GetRepository<UserProfile>();

                var user = repo.GetById(userId);

                if (user.ActiveSchemeDetailId.GetValueOrDefault() == schemeId)
                {
                    user.ActiveSchemeDetailId = null;
                }

                user.SchemeDetails = user.SchemeDetails.Except(user.SchemeDetails.Where(scheme => scheme.SchemeDetailId == schemeId));

                repo.Update(user);
            }
        }

        public bool AddFavouriteScheme(UserProfile user, int schemeDetailId)
        {
            var returnVal = false;
            if (!CanAddToFavourites(user, schemeDetailId))
                return false;

            using (var db = new DbTransactionalScope())
            {
                var repo = (IUserProfileRepository)db.GetRepository<UserProfile>();
                var schemeRepo = (ISchemeDetailRepository)db.GetRepository<SchemeDetail>();

                var schemeDetail = schemeRepo.GetById(schemeDetailId);

                if (schemeDetail != null)
                {
                    repo.AddFavouriteScheme(user.UserId, schemeDetailId); // update database table

                    db.Commit();

                    var favSchemes = (List<SchemeDetail>)user.FavouriteSchemes;
                    favSchemes.Add(schemeDetail);       // add to user's fav list                 
                    returnVal = true;
                }
            }
            return returnVal;
        }

        private bool CanAddToFavourites(UserProfile user, int schemeDetailId)
        {
            return user != null && !user.FavouriteSchemes.Any(scheme => scheme.SchemeDetailId == schemeDetailId);
        }

        public bool RemoveFavouriteScheme(UserProfile user, int schemeDetailId)
        {
            var returnVal = false;
            using (var db = new DbInteractionScope())
            {
                var repo = (IUserProfileRepository)db.GetRepository<UserProfile>();

                if (user != null && user.FavouriteSchemes.Any(x => x.SchemeDetailId == schemeDetailId))
                {
                    repo.RemoveFavouriteScheme(user.UserId, schemeDetailId); // update database table
                    
                    // remove from user's fav list 
                    var favSchemes =(List<SchemeDetail>) user.FavouriteSchemes;
                    favSchemes.Remove((user.FavouriteSchemes.Where(scheme => scheme.SchemeDetailId == schemeDetailId).First()));

                    returnVal = true;
                }
            }
            return returnVal;
        }
    }
}
