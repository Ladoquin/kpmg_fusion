﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightDeck.DataAccess;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.Services
{
    public class CachedCurveDataRepository : ICurveDataRepository
    {
        private readonly Dictionary<int, CurveDataProjection> projectionsById;

        public CachedCurveDataRepository(IEnumerable<CurveDataProjection> projections)
        {
            projectionsById = projections.ToDictionary(x => x.Id);
        }

        public CurveDataProjection GetById(int id)
        {
            return projectionsById[id];
        }

        public IEnumerable<CurveDataProjection> GetAll()
        {
            return projectionsById.Values;
        }

        public int Insert(CurveDataProjection item)
        {
            throw new System.NotSupportedException();
        }

        public bool Update(CurveDataProjection item)
        {
            throw new System.NotSupportedException();
        }

        public void InsertOrUpdate(CurveDataProjection item)
        {
            throw new System.NotSupportedException();
        }

        public void ClearCache()
        {
            throw new System.NotSupportedException();
        }

        public CurveDataProjection GetByName(string name)
        {
            var result = projectionsById.Values.FirstOrDefault(x => x.IndexName == name);

            return result;
        }
        
        public CurveDataProjection GetProjectionByDate(DateTime date)
        {
            throw new NotImplementedException();
        }
    }
}