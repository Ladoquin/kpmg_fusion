﻿using FlightDeck.DataAccess;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using System.Collections.Generic;
using System.Linq;

namespace FlightDeck.Services
{
    public class ApplicationSettings : IApplicationSettings
    {
        private const string ImportantMessageKey = "ImportantMessage";
        private const string ConsultantDefaultSchemeNameKey = "ConsultantDefaultSchemeName";
        private const string PensionIncreasesMaxValue = "PensionIncreasesMaxValue";

        private IDbInteractionScope _dbInteractionScope;

        public IDbInteractionScope DbInteractionScope
        {
            private get
            {
                if (_dbInteractionScope == null)
                    _dbInteractionScope = new DbInteractionScope();
                return _dbInteractionScope;
            }
            set
            {
                _dbInteractionScope = value;
            }
        }

        private IDbInteractionScopeFactory _dbScopeFactory;
        public IDbInteractionScopeFactory DbInteractionScopeFactory
        {
            private get
            {
                if (_dbScopeFactory == null)
                    _dbScopeFactory = new DbInteractionScopeFactory();
                return _dbScopeFactory;
            }
            set
            {
                _dbScopeFactory = value;
            }
        }

        private KeyValuePair<int, string> getAppSetting(ApplicationValue data)
        {
            var value = data.Value;
            var id = int.Parse(value.Substring(0, value.IndexOf("[::]")));
            var msg = value.Substring(value.IndexOf("[::]") + "[::]".Length);
            return new KeyValuePair<int, string>(id, msg);
        }

        public ImportantMessage GetImportantMessage()
        {
            ImportantMessage im = null;

            using (var db = new DbInteractionScope())
            {
                var data = ((IApplicationRepository)db.GetRepository<ApplicationValue>()).GetByKey(ImportantMessageKey);

                if (data != null)
                {
                    var val = getAppSetting(data);

                    im = new ImportantMessage { Key = val.Key, Message = val.Value };
                }
            }

            return im;
        }

        public void SaveImportantMessage(string message)
        {
            using (var db = new DbInteractionScope())
            {
                var record = new ApplicationValue { Key = ImportantMessageKey };

                var repo = (IApplicationRepository)db.GetRepository<ApplicationValue>();

                var data = repo.GetByKey(ImportantMessageKey);

                int id = 0;

                if (data != null)
                {
                    var val = getAppSetting(data);

                    id = val.Key + 1;
                }
                else
                {
                    id = 1;
                }

                record.Value = id.ToString() + "[::]" + message;

                repo.InsertOrUpdate(record);
            }
        }

        public string GetConsultantDefaultSchemeName()
        {
            string schemeName = null;

            var db = DbInteractionScope.GetRepository<ApplicationValue>() as IApplicationRepository;

            var data = db.GetByKey(ConsultantDefaultSchemeNameKey);

            if (data != null)
            {
                schemeName = data.Value;
            }

            DbInteractionScope.Dispose();

            _dbInteractionScope = null;

            return schemeName;
        }

        public double GetPensionIncreasesMaxValue()
        {
            var db = DbInteractionScope.GetRepository<ApplicationValue>() as IApplicationRepository;

            var val = double.Parse(db.GetByKey(PensionIncreasesMaxValue).Value);

            DbInteractionScope.Dispose();

            return val;
        }

        public string GetTrackerVersionSupported()
        {
            using (var dbInteractionScope = DbInteractionScopeFactory.GetScope())
            {
                var db = dbInteractionScope.GetRepository<ApplicationValue>() as IApplicationRepository;

                return db.GetTrackerVersionSupported();
            }
        }

        public bool IsSchemeSupported(string dataCaptureVersion)
        {
            if (dataCaptureVersion == null)
                return false;

            var trackerVersionSupported = GetTrackerVersionSupported();
            var trackerVersionArray = trackerVersionSupported.Split('.');
            
            var dataCaptureVersionArray = dataCaptureVersion.Split('.');
            if (trackerVersionArray.Length != 2 || dataCaptureVersionArray.Length != 2)
                return false;

            var trackerMajor = trackerVersionArray[0];
            var trackerMinor = trackerVersionArray[1];
            var schemeMajor = dataCaptureVersionArray[0];
            var schemeMinor = dataCaptureVersionArray[1];

            int val;
            var versionsValid = int.TryParse(trackerMajor, out val)
                                && int.TryParse(trackerMinor, out val)
                                && int.TryParse(schemeMajor, out val)
                                && int.TryParse(schemeMinor, out val);

            if (!versionsValid)
                return false;

            var schemeMajorDifferent = int.Parse(schemeMajor) != int.Parse(trackerMajor);

            if (int.Parse(schemeMajor) > int.Parse(trackerMajor))
                return true;
            else if (int.Parse(schemeMajor) == int.Parse(trackerMajor))
                return int.Parse(schemeMinor) >= int.Parse(trackerMinor);
            else
                return false;
        }

    }
}
