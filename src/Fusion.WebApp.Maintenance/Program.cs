﻿namespace Fusion.WebApp.Maintenance
{
    using System;
    
    class Program
    {
        static void Main(string[] args)
        {
            var userProfileManager = new UserProfileManager();

            Console.Write("Enter password: ");
            var password = Console.ReadLine();

            var encrypted = userProfileManager.GetPasswordEncrypted(password);

            Console.WriteLine(string.Format("Password encrypted: {0}", encrypted));
            Console.WriteLine("Press any key to exit.");

            Console.ReadKey();
        }
    }
}
