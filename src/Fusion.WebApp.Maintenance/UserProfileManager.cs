﻿namespace Fusion.WebApp.Maintenance
{
    using Fusion.Shared.Data;
    using System;
    using System.Collections.Generic;
    using System.Web.Security;
    using WebMatrix.WebData;
    
    class UserProfileManager
    {
        public string GetPasswordEncrypted(string password)
        {
            var membership = GetMembershipProvider();

            var username = Guid.NewGuid().ToString();
            
            membership.CreateUserAndAccount(username, password, new Dictionary<string, object> { { "TermsAccepted", false } });

            var encrypted = GetEncryptedPasswordFromDB(username);

            membership.DeleteAccount(username);

            DeleteUserRemnants(username);

            return encrypted;
        }

        private SimpleMembershipProvider GetMembershipProvider()
        {
            if (!WebSecurity.Initialized)
                WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: false);

            var membership = (SimpleMembershipProvider)Membership.Provider;

            return membership;
        }

        private string GetEncryptedPasswordFromDB(string username)
        {
            var data = new DbHelper().RunQuery(string.Format("select wm.* from UserProfile up inner join webpages_Membership wm on up.UserId = wm.UserId and up.UserName = '{0}'", username));

            for (var i = 0; i < data.Tables[0].Columns.Count; i++)
            {
                if (string.Equals("password", data.Tables[0].Columns[i].ColumnName, StringComparison.CurrentCultureIgnoreCase))
                {
                    return data.Tables[0].Rows[0][i] as string;
                }
            }

            return string.Empty;
        }

        private void DeleteUserRemnants(string username)
        {
            new DbHelper().RunQuery(string.Format("delete UserProfile where Username = '{0}'", username));
        }
    }
}
