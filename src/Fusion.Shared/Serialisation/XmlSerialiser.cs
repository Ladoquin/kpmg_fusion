﻿namespace Fusion.Shared.Serialisation
{
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;
    
    public class XmlSerialiser
    {
        public string Serialise<T>(T o) where T : new()
        {
            var xml = "";
            var xs = new XmlSerializer(typeof(T));
            var subReq = new T();
            using (var sww = new StringWriter())
            {
                using (var writer = XmlWriter.Create(sww))
                {
                    xs.Serialize(writer, subReq);
                }
                xml = sww.ToString();
            }
            return xml;
        }

        public T Deserialise<T>(string xml)
        {
            var xs = new XmlSerializer(typeof(T));
            using (var sr = new StringReader(xml))
            {
                return (T)xs.Deserialize(sr);
            }
        }
    }
}
