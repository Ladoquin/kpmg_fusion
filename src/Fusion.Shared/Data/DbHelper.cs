﻿namespace Fusion.Shared.Data
{
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;

    public class DbHelper
    {
        public enum Db
        {
            Default,
            Log,
            Master
        }

        private string getConnectionString(Db db)
        {
            var key = string.Empty;

            switch (db)
            {
                case Db.Log:
                    key = "Log";
                    break;
                case Db.Master:
                case Db.Default:
                default:
                    key = "DefaultConnection";
                    break;
            }

            var cnnString = ConfigurationManager.ConnectionStrings[key].ConnectionString;

            if (db == Db.Master)
            {
                cnnString = string.Format("{0}Master{1}",
                    cnnString.Substring(0, cnnString.ToLower().IndexOf("database=") + "database=".Length),
                    cnnString.Substring(cnnString.IndexOf(";", cnnString.ToLower().IndexOf("database="))));
            }

            return cnnString;
        }

        public void RunScript(string path, Db db = Db.Default)
        {
            var sql = File.ReadAllText(path);

            RunCommand(sql, db);
        }

        public DataSet RunScriptWithOutput(string path, Db db = Db.Default)
        {
            var sql = File.ReadAllText(path);

            return RunQuery(sql, db);
        }

        public void RunCommand(string sql, Db db = Db.Default)
        {
            var cnnString = getConnectionString(db);
            using (var cnn = new SqlConnection(cnnString))
            {
                try
                {
                    cnn.Open();
                    var cmd = new SqlCommand(sql, cnn);
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    if (cnn.State == System.Data.ConnectionState.Open)
                        cnn.Close();
                }
            }
        }

        public DataSet RunQuery(string sql, Db db = Db.Default)
        {
            var cnnString = getConnectionString(db);
            using (var cnn = new SqlConnection(cnnString))
            {
                try
                {
                    cnn.Open();
                    var cmd = new SqlCommand(sql, cnn);
                    var adapter = new SqlDataAdapter(cmd);
                    var ds = new DataSet();

                    adapter.Fill(ds);

                    return ds;
                }
                finally
                {
                    if (cnn.State == System.Data.ConnectionState.Open)
                        cnn.Close();
                }
            }
        }
    }
}
