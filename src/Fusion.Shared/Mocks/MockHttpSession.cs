﻿namespace Fusion.Shared.Mocks
{
    using System.Collections.Generic;
    using System.Web;

    public class MockHttpSession : HttpSessionStateBase
    {
        Dictionary<string, object> dic = new Dictionary<string, object>();

        public override object this[string name]
        {
            get
            {
                return dic.ContainsKey(name) ? dic[name] : null;
            }
            set
            {
                dic[name] = value;
            }
        }

        public override void Abandon()
        {
            dic = new Dictionary<string, object>();
        }
    }
}
