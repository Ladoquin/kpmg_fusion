﻿namespace Fusion.Shared.Mocks
{
    using System.Collections.Generic;
    using System.Web;

    public class MockHttpApplicationState : HttpApplicationStateBase
    {
        Dictionary<string, object> dic = new Dictionary<string, object>();

        public override object this[string key]
        {
            get
            {
                return dic.ContainsKey(key) ? dic[key] : null;
            }
            set
            {
                dic[key] = value;
            }
        }
    }
}