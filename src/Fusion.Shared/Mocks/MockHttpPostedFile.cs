﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Fusion.Shared.Mocks
{
    public class MockHttpPostedFile : HttpPostedFileBase
    {

        private byte[] _Bytes;

        public override Int32 ContentLength { get { return this._Bytes.Length; } }

        public override String ContentType { get { return this._ContentType; } }
        private String _ContentType;

        public override String FileName { get { return this._FileName; } }
        private String _FileName;

        public override Stream InputStream
        {
            get
            {
                if (this._Stream == null)
                {
                    this._Stream = new MemoryStream(this._Bytes);
                }
                return this._Stream;
            }
        }
        private MemoryStream _Stream;

        public MockHttpPostedFile(byte[] contentData, String contentType, String fileName)
        {
            this._ContentType = contentType;
            this._FileName = fileName;
            this._Bytes = contentData ?? new byte[0];
        }

        public override void SaveAs(String filename)
        {
            File.WriteAllBytes(filename, this._Bytes);
        }

    }
}
