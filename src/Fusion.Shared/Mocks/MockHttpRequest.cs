﻿namespace Fusion.Shared.Mocks
{
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Web;

    public class MockHttpRequest : HttpRequestBase
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();

        public override string this[string key]
        {
            get
            {
                return dic.ContainsKey(key) ? dic[key] : null;
            }
        }

        public void AddRequestItem(string key, string value)
        {
            if (!dic.ContainsKey(key))
                dic.Add(key, value);
            else
                dic[key] = value;
        }

        public override string UserAgent
        {
            get
            {
                return string.Empty;
            }
        }

    }
}
