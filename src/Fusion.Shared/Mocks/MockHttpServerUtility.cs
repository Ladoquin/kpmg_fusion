﻿namespace Fusion.Shared.Mocks
{
    using System.Configuration;
    using System.IO;
    using System.Reflection;
    using System.Web;

    public class MockHttpServerUtility : HttpServerUtilityBase
    {
        public override string MapPath(string path)
        {
            var root = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ServerMapRoot"])
                ? ConfigurationManager.AppSettings["ServerMapRoot"]
                : Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"..\..\..\Fusion.WebApp\");
            return path.Replace("~", root).Replace("/", "\\");
        }
    }
}
