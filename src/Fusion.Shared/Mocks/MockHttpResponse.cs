﻿namespace Fusion.Shared.Mocks
{
    using System.Web;
    
    public class MockHttpResponse : HttpResponseBase
    {

        HttpCookieCollection cookies = new HttpCookieCollection();

        public override void AppendHeader(string name, string value)
        {
            //whatevs
        }

        public override HttpCookieCollection Cookies
        {
            get
            {
                return cookies;
            }
        }
    }
}
