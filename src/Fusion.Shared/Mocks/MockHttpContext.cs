﻿namespace Fusion.Shared.Mocks
{
    using System.Collections.Generic;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Caching;

    public class MockHttpContext : HttpContextBase
    {
        private MockHttpSession httpSession = new MockHttpSession();
        private MockHttpServerUtility httpServerUtility = new MockHttpServerUtility();
        private MockHttpRequest httpRequest = new MockHttpRequest();
        private MockHttpResponse httpResponse = new MockHttpResponse();
        private MockHttpApplicationState httpApplication = new MockHttpApplicationState();

        private IPrincipal _user =
            new GenericPrincipal(
                new GenericIdentity("admin"), new[] { "Admin" });

        public override HttpApplicationStateBase Application
        {
            get
            {
                return httpApplication;
            }
        }

        public override IPrincipal User
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
            }
        }

        public override HttpSessionStateBase Session
        {
            get
            {
                return httpSession;
            }
        }

        public override HttpServerUtilityBase Server
        {
            get
            {
                return httpServerUtility;
            }
        }

        public override HttpRequestBase Request
        {
            get
            {
                return httpRequest;
            }
        }

        public override HttpResponseBase Response
        {
            get
            {
                return httpResponse;
            }
        }

        public override Cache Cache
        {
            get
            {
                return HttpRuntime.Cache;
            }
        }   
    }
}