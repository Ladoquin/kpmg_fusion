﻿namespace Fusion.Shared.Web
{
    public class JsonReader
    {
        public static T Get<T>(object obj, string propertyName) 
        {
            return (T)obj.GetType().GetProperty(propertyName).GetValue(obj);
        }
    }
}
