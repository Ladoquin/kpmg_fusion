﻿using FlightDeck.DomainShared;
using System;

public class FairValueSchemeAssetChangesFRS17Data : IEquatable<FairValueSchemeAssetChangesFRS17Data>
{
    public double OpeningFairValueSchemeAssets { get; private set; }
    public double ExpectedReturn { get; private set; }
    public double ActurialGains { get; private set; }
    public double ContributionsEmployer { get; private set; }
    public double ContributionsMembers { get; private set; }
    public double BenefitsPaid { get; private set; }
    public double ClosingFairValueSchemeAssets { get; private set; }

    public FairValueSchemeAssetChangesFRS17Data()
    {
    }
    public FairValueSchemeAssetChangesFRS17Data(double openingFairValueSchemeAssets, double expectedReturn, double acturialGains, double contributionsEmployer, double contributionsMembers, double benefitsPaid, double closingFairValueSchemeAssets)
    {
        OpeningFairValueSchemeAssets = openingFairValueSchemeAssets;
        ExpectedReturn = expectedReturn;
        ActurialGains = acturialGains;
        ContributionsEmployer = contributionsEmployer;
        ContributionsMembers = contributionsMembers;
        BenefitsPaid = benefitsPaid;
        ClosingFairValueSchemeAssets = closingFairValueSchemeAssets;
    }

    public bool Equals(FairValueSchemeAssetChangesFRS17Data other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Utils.EqualityCheck(OpeningFairValueSchemeAssets, other.OpeningFairValueSchemeAssets)
            && Utils.EqualityCheck(ExpectedReturn, other.ExpectedReturn)
            && Utils.EqualityCheck(ActurialGains, other.ActurialGains)
            && Utils.EqualityCheck(ContributionsEmployer, other.ContributionsEmployer)
            && Utils.EqualityCheck(ContributionsMembers, other.ContributionsMembers)
            && Utils.EqualityCheck(BenefitsPaid, other.BenefitsPaid)
            && Utils.EqualityCheck(ClosingFairValueSchemeAssets, other.ClosingFairValueSchemeAssets);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
        return Equals((FairValueSchemeAssetChangesFRS17Data)obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = OpeningFairValueSchemeAssets.GetHashCode();
            hashCode = (hashCode * 397) ^ ExpectedReturn.GetHashCode();
            hashCode = (hashCode * 397) ^ ActurialGains.GetHashCode();
            hashCode = (hashCode * 397) ^ ContributionsEmployer.GetHashCode();
            hashCode = (hashCode * 397) ^ ContributionsMembers.GetHashCode();
            hashCode = (hashCode * 397) ^ BenefitsPaid.GetHashCode();
            hashCode = (hashCode * 397) ^ ClosingFairValueSchemeAssets.GetHashCode();
            return hashCode;
        }
    }

    public override string ToString()
    {
        return string.Format("OpeningFairValueSchemeAssets: {0}, ExpectedReturn: {1}, ActurialGains: {2}, ContributionsEmployer: {3}, ContributionsMembers: {4}, BenefitsPaid: {5}, ClosingFairValueSchemeAssets: {6}", OpeningFairValueSchemeAssets, ExpectedReturn, ActurialGains, ContributionsEmployer, ContributionsMembers, BenefitsPaid, ClosingFairValueSchemeAssets);
    }
}