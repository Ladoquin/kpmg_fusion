﻿namespace FlightDeck.DomainShared
{
    using System;

    public class Log
    {
        public int Id { get; private set; }
        public DateTime Date { get; private set; }
        public string Thread { get; private set; }
        public string Level { get; private set; }
        public string Logger { get; private set; }
        public string Message { get; private set; }
        public string Exception { get; private set; }

        public Log(int id, DateTime date, string thread, string level, string logger, string message, string exception)
        {
            Id = id;
            Date = date;
            Thread = thread;
            Level = level;
            Logger = logger;
            Message = message;
            Exception = exception;
        }
    }
}
