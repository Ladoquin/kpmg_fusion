﻿namespace FlightDeck.DomainShared
{
    using System;
    
    [AttributeUsage(AttributeTargets.Field)]
    public class AssetClassTypeCategoryAttribute : Attribute
    {
        public AssetCategory Category { get; set; }

        public AssetClassTypeCategoryAttribute(AssetCategory category)
        {
            Category = category;
        }
    }
}
