﻿namespace FlightDeck.DomainShared
{
    public class PasswordData
    {
        public byte[] EncryptedPassword { get; private set; }
        public byte[] Salt { get; private set; }

        public PasswordData(byte[] encryptedPassword, byte[] salt)
        {
            EncryptedPassword = encryptedPassword;
            Salt = salt;
        }
    }
}
