﻿namespace FlightDeck.DomainShared
{
    using System;

    [Serializable]
    public class PieOptions
    {
        public double PortionEligible { get; private set; }
        public double ActualUplift { get; private set; }
        public double TakeUpRate { get; private set; }

        public PieOptions(double portionEligible, double actualUplift, double takeUpRate)
        {
            PortionEligible = portionEligible;
            ActualUplift = actualUplift;
            TakeUpRate = takeUpRate;
        }

        public override string ToString()
        {
            return string.Format("PortionEligible: {0}, ActualUplift: {1}, TakeUpRate: {2}", PortionEligible, ActualUplift, TakeUpRate);
        }
    }
}