﻿using System.ComponentModel.DataAnnotations;

namespace FlightDeck.DomainShared
{
    public class SchemeDocument
    {
        public int SchemeDocumentId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string FileName { get; set; }

        [Required]
        public byte[] Content { get; set; }

        [Required]
        public string ContentType { get; set; }
    }
}
