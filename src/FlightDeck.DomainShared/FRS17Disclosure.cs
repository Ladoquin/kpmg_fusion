﻿namespace FlightDeck.DomainShared
{
    using System;

    public class FRS17Disclosure : IEquatable<FRS17Disclosure>
    {
        public DateTime Date { get; private set; }
        public DisclosureAssumptions Assumptions { get; private set; }
        public FRS17BalanceSheetAmountData BalanceSheetAmounts { get; private set; }
        public PresentValueSchemeLiabilityData PresentValueSchemeLiabilities { get; set; }
        public FRS17FairValueSchemeAssetChangesData FairValueSchemeAssetChanges { get; set; }
        public FRS17SchemeSurplusChangesData SchemeSurplusChanges { get; set; }
        public FRS17PAndLForecastData PAndLForecast { get; set; }

        public FRS17Disclosure(DateTime date, DisclosureAssumptions assumptions, FRS17BalanceSheetAmountData balanceSheetAmounts, PresentValueSchemeLiabilityData schemeLiabilityChanges, FRS17FairValueSchemeAssetChangesData fairValueSchemeAssetChanges, FRS17SchemeSurplusChangesData schemeSurplusChanges, FRS17PAndLForecastData pAndLForecast)
        {
            Date = date;
            Assumptions = assumptions;
            BalanceSheetAmounts = balanceSheetAmounts;
            PresentValueSchemeLiabilities = schemeLiabilityChanges;
            FairValueSchemeAssetChanges = fairValueSchemeAssetChanges;
            SchemeSurplusChanges = schemeSurplusChanges;
            PAndLForecast = pAndLForecast;
        }

        public bool Equals(FRS17Disclosure other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date.Equals(other.Date) && Equals(Assumptions, other.Assumptions) && Equals(BalanceSheetAmounts, other.BalanceSheetAmounts) && Equals(PresentValueSchemeLiabilities, other.PresentValueSchemeLiabilities) && Equals(FairValueSchemeAssetChanges, other.FairValueSchemeAssetChanges) && Equals(SchemeSurplusChanges, other.SchemeSurplusChanges) && Equals(PAndLForecast, other.PAndLForecast);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((FRS17Disclosure)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ (Assumptions != null ? Assumptions.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (BalanceSheetAmounts != null ? BalanceSheetAmounts.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PresentValueSchemeLiabilities != null ? PresentValueSchemeLiabilities.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (FairValueSchemeAssetChanges != null ? FairValueSchemeAssetChanges.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (SchemeSurplusChanges != null ? SchemeSurplusChanges.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PAndLForecast != null ? PAndLForecast.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Date: {0}, Assumptions: {1}, BalanceSheetAmounts: {2}, PresentValueSchemeLiabilities: {3}, FairValueSchemeAssetChanges: {4}, SchemeSurplusChanges: {5}, PAndLForecast: {6}", Date, Assumptions, BalanceSheetAmounts, PresentValueSchemeLiabilities, FairValueSchemeAssetChanges, SchemeSurplusChanges, PAndLForecast);
        }

        #region Data classes

        public class FRS17BalanceSheetAmountData : IEquatable<FRS17BalanceSheetAmountData>
        {
            public double PresentValueSchemeLiabilities { get; private set; }
            public double FairValueOfSchemeAssets { get; private set; }
            public double SchemeSurplus { get; private set; }

            public FRS17BalanceSheetAmountData()
            {
            }
            public FRS17BalanceSheetAmountData(double presentValueSchemeLiabilities, double fairValueOfSchemeAssets, double schemeSurplus)
            {
                PresentValueSchemeLiabilities = presentValueSchemeLiabilities;
                FairValueOfSchemeAssets = fairValueOfSchemeAssets;
                SchemeSurplus = schemeSurplus;
            }

            public bool Equals(FRS17BalanceSheetAmountData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(PresentValueSchemeLiabilities, other.PresentValueSchemeLiabilities)
                    && Utils.EqualityCheck(FairValueOfSchemeAssets, other.FairValueOfSchemeAssets)
                    && Utils.EqualityCheck(SchemeSurplus, other.SchemeSurplus);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((FRS17BalanceSheetAmountData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = PresentValueSchemeLiabilities.GetHashCode();
                    hashCode = (hashCode * 397) ^ FairValueOfSchemeAssets.GetHashCode();
                    hashCode = (hashCode * 397) ^ SchemeSurplus.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("PresentValueOfSchemeAssets: {0}, FairValueOfSchemeAssets: {1}, SchemeSurplus: {2}", PresentValueSchemeLiabilities, FairValueOfSchemeAssets, SchemeSurplus);
            }
        }

        public class PresentValueSchemeLiabilityData : IEquatable<PresentValueSchemeLiabilityData>
        {
            public double OpeningDefinedBenefitObligation { get; private set; }
            public double CurrentServiceCost { get; private set; }
            public double InterestExpense { get; private set; }
            public double ActuarialLosses { get; private set; }
            public double Contributions { get; private set; }
            public double BenefitsPaid { get; private set; }
            public double ClosingDefinedBenefitObligation { get; private set; }

            public PresentValueSchemeLiabilityData()
            {
            }
            public PresentValueSchemeLiabilityData(double openingDefinedBenefitObligation, double currentServiceCost, double interestExpense, double actuarialLosses, double contributions, double benefitsPaid, double closingDefinedBenefitObligation)
            {
                OpeningDefinedBenefitObligation = openingDefinedBenefitObligation;
                CurrentServiceCost = currentServiceCost;
                InterestExpense = interestExpense;
                ActuarialLosses = actuarialLosses;
                Contributions = contributions;
                BenefitsPaid = benefitsPaid;
                ClosingDefinedBenefitObligation = closingDefinedBenefitObligation;
            }

            public bool Equals(PresentValueSchemeLiabilityData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(OpeningDefinedBenefitObligation, other.OpeningDefinedBenefitObligation)
                    && Utils.EqualityCheck(CurrentServiceCost, other.CurrentServiceCost)
                    && Utils.EqualityCheck(InterestExpense, other.InterestExpense)
                    && Utils.EqualityCheck(ActuarialLosses, other.ActuarialLosses)
                    && Utils.EqualityCheck(Contributions, other.Contributions)
                    && Utils.EqualityCheck(BenefitsPaid, other.BenefitsPaid)
                    && Utils.EqualityCheck(ClosingDefinedBenefitObligation, other.ClosingDefinedBenefitObligation);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((PresentValueSchemeLiabilityData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = OpeningDefinedBenefitObligation.GetHashCode();
                    hashCode = (hashCode * 397) ^ CurrentServiceCost.GetHashCode();
                    hashCode = (hashCode * 397) ^ InterestExpense.GetHashCode();
                    hashCode = (hashCode * 397) ^ ActuarialLosses.GetHashCode();
                    hashCode = (hashCode * 397) ^ Contributions.GetHashCode();
                    hashCode = (hashCode * 397) ^ BenefitsPaid.GetHashCode();
                    hashCode = (hashCode * 397) ^ ClosingDefinedBenefitObligation.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("OpeningDefinedBenefitObligation: {0}, CurrentServiceCost: {1}, InterestExpense: {2}, ActuarialLosses: {3}, Contributions: {4}, BenefitsPaid: {5}, ClosingDefinedBenefitObligation: {6}", OpeningDefinedBenefitObligation, CurrentServiceCost, InterestExpense, ActuarialLosses, Contributions, BenefitsPaid, ClosingDefinedBenefitObligation);
            }
        }

        public class FRS17FairValueSchemeAssetChangesData : IEquatable<FRS17FairValueSchemeAssetChangesData>
        {
            public double OpeningFairValueSchemeAssets { get; private set; }
            public double ExpectedReturn { get; private set; }
            public double ActurialGains { get; private set; }
            public double ContributionsEmployer { get; private set; }
            public double ContributionsMembers { get; private set; }
            public double BenefitsPaid { get; private set; }
            public double ClosingFairValueSchemeAssets { get; private set; }

            public FRS17FairValueSchemeAssetChangesData()
            {
            }
            public FRS17FairValueSchemeAssetChangesData(double openingFairValueSchemeAssets, double expectedReturn, double acturialGains, double contributionsEmployer, double contributionsMembers, double benefitsPaid, double closingFairValueSchemeAssets)
            {
                OpeningFairValueSchemeAssets = openingFairValueSchemeAssets;
                ExpectedReturn = expectedReturn;
                ActurialGains = acturialGains;
                ContributionsEmployer = contributionsEmployer;
                ContributionsMembers = contributionsMembers;
                BenefitsPaid = benefitsPaid;
                ClosingFairValueSchemeAssets = closingFairValueSchemeAssets;
            }

            public bool Equals(FRS17FairValueSchemeAssetChangesData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(OpeningFairValueSchemeAssets, other.OpeningFairValueSchemeAssets)
                    && Utils.EqualityCheck(ExpectedReturn, other.ExpectedReturn)
                    && Utils.EqualityCheck(ActurialGains, other.ActurialGains)
                    && Utils.EqualityCheck(ContributionsEmployer, other.ContributionsEmployer)
                    && Utils.EqualityCheck(ContributionsMembers, other.ContributionsMembers)
                    && Utils.EqualityCheck(BenefitsPaid, other.BenefitsPaid)
                    && Utils.EqualityCheck(ClosingFairValueSchemeAssets, other.ClosingFairValueSchemeAssets);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((FRS17FairValueSchemeAssetChangesData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = OpeningFairValueSchemeAssets.GetHashCode();
                    hashCode = (hashCode * 397) ^ ExpectedReturn.GetHashCode();
                    hashCode = (hashCode * 397) ^ ActurialGains.GetHashCode();
                    hashCode = (hashCode * 397) ^ ContributionsEmployer.GetHashCode();
                    hashCode = (hashCode * 397) ^ ContributionsMembers.GetHashCode();
                    hashCode = (hashCode * 397) ^ BenefitsPaid.GetHashCode();
                    hashCode = (hashCode * 397) ^ ClosingFairValueSchemeAssets.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("OpeningFairValueSchemeAssets: {0}, ExpectedReturn: {1}, ActurialGains: {2}, ContributionsEmployer: {3}, ContributionsMembers: {4}, BenefitsPaid: {5}, ClosingFairValueSchemeAssets: {6}", OpeningFairValueSchemeAssets, ExpectedReturn, ActurialGains, ContributionsEmployer, ContributionsMembers, BenefitsPaid, ClosingFairValueSchemeAssets);
            }
        }
        
        public class FRS17SchemeSurplusChangesData : IEquatable<FRS17SchemeSurplusChangesData>
        {
            public double SchemeSurplusBeginning { get; private set; }
            public double CurrentServiceCost { get; private set; }
            public double InterestCost { get; private set; }
            public double ExpectedReturnOnSchemeAssets { get; private set; }
            public double PAndLTotal { get; private set; }
            public double ActuarialLossesOnLiabilities { get; private set; }
            public double ActuarialLossesOnAssets { get; private set; }
            public double STRGLTotal { get; private set; }
            public double Contributions { get; private set; }
            public double SchemeSurplusEnd { get; private set; }

            public FRS17SchemeSurplusChangesData()
            {
            }
            public FRS17SchemeSurplusChangesData(double schemeSurplusBeginning, double currentServiceCost, double interestCost, double expectedReturnOnSchemeAssets, double pAndLTotal, double actuarialLossesOnLiabilities, double actuarialLossesOnAssets, double sTRGLTotal, double contributions, double schemeSurplusEnd)
            {
                SchemeSurplusBeginning = schemeSurplusBeginning;
                CurrentServiceCost = currentServiceCost;
                InterestCost = interestCost;
                ExpectedReturnOnSchemeAssets = expectedReturnOnSchemeAssets;
                PAndLTotal = pAndLTotal;
                ActuarialLossesOnLiabilities = actuarialLossesOnLiabilities;
                ActuarialLossesOnAssets = actuarialLossesOnAssets;
                STRGLTotal = sTRGLTotal;
                Contributions = contributions;
                SchemeSurplusEnd = schemeSurplusEnd;
            }

            public bool Equals(FRS17SchemeSurplusChangesData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(SchemeSurplusBeginning, other.SchemeSurplusBeginning)
                    && Utils.EqualityCheck(CurrentServiceCost, other.CurrentServiceCost)
                    && Utils.EqualityCheck(InterestCost, other.InterestCost)
                    && Utils.EqualityCheck(ExpectedReturnOnSchemeAssets, other.ExpectedReturnOnSchemeAssets)
                    && Utils.EqualityCheck(PAndLTotal, other.PAndLTotal)
                    && Utils.EqualityCheck(ActuarialLossesOnLiabilities, other.ActuarialLossesOnLiabilities)
                    && Utils.EqualityCheck(ActuarialLossesOnAssets, other.ActuarialLossesOnAssets)
                    && Utils.EqualityCheck(STRGLTotal, other.STRGLTotal)
                    && Utils.EqualityCheck(Contributions, other.Contributions)
                    && Utils.EqualityCheck(SchemeSurplusEnd, other.SchemeSurplusEnd);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((FRS17SchemeSurplusChangesData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = SchemeSurplusBeginning.GetHashCode();
                    hashCode = (hashCode * 397) ^ CurrentServiceCost.GetHashCode();
                    hashCode = (hashCode * 397) ^ InterestCost.GetHashCode();
                    hashCode = (hashCode * 397) ^ ExpectedReturnOnSchemeAssets.GetHashCode();
                    hashCode = (hashCode * 397) ^ PAndLTotal.GetHashCode();
                    hashCode = (hashCode * 397) ^ ActuarialLossesOnLiabilities.GetHashCode();
                    hashCode = (hashCode * 397) ^ ActuarialLossesOnAssets.GetHashCode();
                    hashCode = (hashCode * 397) ^ STRGLTotal.GetHashCode();
                    hashCode = (hashCode * 397) ^ Contributions.GetHashCode();
                    hashCode = (hashCode * 397) ^ SchemeSurplusEnd.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("SchemeSurplusBeginning: {0}, CurrentServiceCost: {1}, InterestCost: {2}, ExpectedReturnOnSchemeAssets: {3}, PAndLTotal: {4}, ActuarialLossesOnLiabilities: {5}, ActuarialLossesOnAssets: {6}, STRGLTotal: {7}, Contributions: {8}, SchemeSurplusEnd: {9}", SchemeSurplusBeginning, CurrentServiceCost, InterestCost, ExpectedReturnOnSchemeAssets, PAndLTotal, ActuarialLossesOnLiabilities, ActuarialLossesOnAssets, STRGLTotal, Contributions, SchemeSurplusEnd);
            }
        }

        public class FRS17PAndLForecastData : IEquatable<FRS17PAndLForecastData>
        {
            public double CurrentServiceCost { get; private set; }
            public double InterestCost { get; private set; }
            public double ExpectedReturnOnSchemeAssets { get; private set; }
            public double Total { get; private set; }

            public FRS17PAndLForecastData()
            {
            }
            public FRS17PAndLForecastData(double currentServiceCost, double interestCost, double expectedReturnOnSchemeAssets, double total)
            {
                CurrentServiceCost = currentServiceCost;
                InterestCost = interestCost;
                ExpectedReturnOnSchemeAssets = expectedReturnOnSchemeAssets;
                Total = total;
            }

            public bool Equals(FRS17PAndLForecastData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(CurrentServiceCost, other.CurrentServiceCost)
                    && Utils.EqualityCheck(InterestCost, other.InterestCost)
                    && Utils.EqualityCheck(ExpectedReturnOnSchemeAssets, other.ExpectedReturnOnSchemeAssets)
                    && Utils.EqualityCheck(Total, other.Total);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((FRS17PAndLForecastData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = CurrentServiceCost.GetHashCode();
                    hashCode = (hashCode * 397) ^ InterestCost.GetHashCode();
                    hashCode = (hashCode * 397) ^ ExpectedReturnOnSchemeAssets.GetHashCode();
                    hashCode = (hashCode * 397) ^ Total.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("CurrentServiceCost: {0}, InterestCost: {1}, ExpectedReturnOnSchemeAssets: {2}, Total: {3}", CurrentServiceCost, InterestCost, ExpectedReturnOnSchemeAssets, Total);
            }
        }

        #endregion
    }
}
