﻿namespace FlightDeck.DomainShared
{
    public class AssetReturnSpec
    {
        public double Min { get; private set; }
        public double Max { get; private set; }
        public double Increment { get; private set; }

        public AssetReturnSpec(double min, double max, double increment)
        {
            Increment = increment;
            Max = max;
            Min = min;
        }
    }
}