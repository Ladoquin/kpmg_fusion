﻿namespace FlightDeck.DomainShared
{
    public class BasisCache
    {
        public int MasterBasisId { get; set; }
        public string BasisEvolutionData { get; set; }
    }
}
