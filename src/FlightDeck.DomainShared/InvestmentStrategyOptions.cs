﻿namespace FlightDeck.DomainShared
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class InvestmentStrategyOptions
    {
        //public IDictionary<AssetClassType, double> ClientAssetAllocations { get; set; }  
        public double CashInjection { get; set; }
        public double SyntheticCredit { get; set; }
        public double SyntheticEquity { get; set; }
        public bool LDIInForce { get; set; }
        public double InterestHedge { get; set; }
        public double InflationHedge { get; set; }

        //todo: ClientAssetAllocations need to be in here
        public InvestmentStrategyOptions(double cashInjection, double syntheticCredit, double syntheticEquity, bool ldiInForce, double interestHedge, double inflationHedge)
        {
            CashInjection = cashInjection;
            SyntheticCredit = syntheticCredit;
            SyntheticEquity = syntheticEquity;
            LDIInForce = ldiInForce;
            InterestHedge = interestHedge;
            InflationHedge = inflationHedge;
        }
    }
}