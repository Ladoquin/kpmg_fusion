﻿namespace FlightDeck.DomainShared
{
    using System.Collections.Generic;
    using System.Linq;

    public class UserProfileSearchCriteria
    {
        public bool NameDescending { get; set; }
        public RoleType? RoleFilter { get; set; }
        public string SchemeFilter { get; set; }
    }
}
