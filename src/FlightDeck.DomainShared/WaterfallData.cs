﻿namespace FlightDeck.DomainShared
{
    using System;

    public class WaterfallData : IEquatable<WaterfallData>
    {
        public double LiabRiskInterestInflation { get; private set; }
        public double LiabRiskIncHedgingAssets { get; private set; }
        public double InterestInflationHedging { get; private set; }
        public double Longevity { get; private set; }
        public double LongevityIncBuyin { get; private set; }
        public double LongevityHedging { get; private set; }
        public double CreditRisk { get; private set; }
        public double EquityRisk { get; private set; }
        public double OtherGrowthRisk { get; private set; }
        public double TotalRisk { get; private set; }
        public double TotalStdDeviation { get; private set; }

        public WaterfallData(
            double liabRiskInterestInflation,
            double liabRiskIncHedgingAssets,
            double interestInflationHedging,
            double longevity,
            double longevityIncBuyin,
            double longevityHedging,
            double creditRisk,
            double equityRisk,
            double otherGrowthRisk,
            double totalRisk,
            double totalStdDeviation)
        {
            LiabRiskInterestInflation = liabRiskInterestInflation;
            LiabRiskIncHedgingAssets = liabRiskIncHedgingAssets;
            InterestInflationHedging = interestInflationHedging;
            Longevity = longevity;
            LongevityIncBuyin = longevityIncBuyin;
            LongevityHedging = longevityHedging;
            CreditRisk = creditRisk;
            EquityRisk = equityRisk;
            OtherGrowthRisk = otherGrowthRisk;
            TotalRisk = totalRisk;
            TotalStdDeviation = totalStdDeviation;
        }

        public bool Equals(WaterfallData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Math.Abs(LiabRiskInterestInflation - other.LiabRiskInterestInflation) < Utils.TestPrecision
                && Math.Abs(LiabRiskIncHedgingAssets - other.LiabRiskIncHedgingAssets) < Utils.TestPrecision
                && Math.Abs(InterestInflationHedging - other.InterestInflationHedging) < Utils.TestPrecision
                && Math.Abs(Longevity - other.Longevity) < Utils.TestPrecision
                && Math.Abs(LongevityIncBuyin - other.LongevityIncBuyin) < Utils.TestPrecision
                && Math.Abs(LongevityHedging - other.LongevityHedging) < Utils.TestPrecision
                && Math.Abs(CreditRisk - other.CreditRisk) < Utils.TestPrecision
                && Math.Abs(EquityRisk - other.EquityRisk) < Utils.TestPrecision
                && Math.Abs(OtherGrowthRisk - other.OtherGrowthRisk) < Utils.TestPrecision
                && Math.Abs(TotalRisk - other.TotalRisk) < Utils.TestPrecision
                && Math.Abs(TotalStdDeviation - other.TotalStdDeviation) < Utils.TestPrecision;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((WaterfallData)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = LiabRiskInterestInflation.GetHashCode();
                hashCode = (hashCode * 397) ^ LiabRiskIncHedgingAssets.GetHashCode();
                hashCode = (hashCode * 397) ^ InterestInflationHedging.GetHashCode();
                hashCode = (hashCode * 397) ^ Longevity.GetHashCode();
                hashCode = (hashCode * 397) ^ LongevityIncBuyin.GetHashCode();
                hashCode = (hashCode * 397) ^ LongevityHedging.GetHashCode();
                hashCode = (hashCode * 397) ^ CreditRisk.GetHashCode();
                hashCode = (hashCode * 397) ^ EquityRisk.GetHashCode();
                hashCode = (hashCode * 397) ^ OtherGrowthRisk.GetHashCode();
                hashCode = (hashCode * 397) ^ TotalRisk.GetHashCode();
                hashCode = (hashCode * 397) ^ TotalStdDeviation.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("LiabRiskInterestInflation: {0}, LiabRiskIncHedgingAssets: {1}, InterestInflationHedging: {2}, Longevity: {3}, LongevityIncBuyin: {4}, LongevityHedging: {5}, CreditRisk: {6}, EquityRisk: {7}, OtherGrowthRisk: {8}, TotalRisk: {9}, TotalStdDeviation: {10}", LiabRiskInterestInflation, LiabRiskIncHedgingAssets, InterestInflationHedging, Longevity, LongevityIncBuyin, LongevityHedging, CreditRisk, EquityRisk, OtherGrowthRisk, TotalRisk, TotalStdDeviation);
        }
    }
}