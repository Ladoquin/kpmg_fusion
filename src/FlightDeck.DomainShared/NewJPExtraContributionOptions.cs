﻿namespace FlightDeck.DomainShared
{
    public class NewJPExtraContributionOptions
    {
        public int StartYear { get; set; }
        public double AnnualContributions { get; set; }
        public int Term { get; set; }
        public double AnnualIncrements { get; set; }
        public bool AddCurrentRecoveryPlan { get; set; }
        public bool AddExtraContributions { get; set; }

        public NewJPExtraContributionOptions()
        {

        }

        public NewJPExtraContributionOptions(int startYear, double annualConts, int term, double annualIncs, bool addCurrentPlan, bool addExtraContributions)
        {
            StartYear = startYear;
            AnnualContributions = annualConts;
            Term = term;
            AnnualIncrements = annualIncs;
            AddCurrentRecoveryPlan = addCurrentPlan;
            AddExtraContributions = addExtraContributions;
        }
    }
}
