﻿using System;

namespace FlightDeck.DomainShared
{
    public class ProfitLossForecast : IEquatable<ProfitLossForecast>
    {
        public double ServiceCost { get; private set; }
        public double SchemeExpenses { get; private set; }
        public double InterestCharge { get; private set; }
        public double InterestCredit { get; private set; }
        public double Total { get { return ServiceCost + SchemeExpenses + InterestCharge + InterestCredit; } }

        public ProfitLossForecast(double serviceCost, double schemeExpenses, double interestCharge, double interestCredit)
        {
            InterestCredit = interestCredit;
            InterestCharge = interestCharge;
            SchemeExpenses = schemeExpenses;
            ServiceCost = serviceCost;
        }

        public bool Equals(ProfitLossForecast other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Math.Abs(ServiceCost - other.ServiceCost) < Utils.Precision 
                && Math.Abs(SchemeExpenses - other.SchemeExpenses) < Utils.Precision 
                && Math.Abs(InterestCharge - other.InterestCharge) < Utils.Precision 
                && Math.Abs(InterestCredit - other.InterestCredit) < Utils.Precision;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((ProfitLossForecast) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = ServiceCost.GetHashCode();
                hashCode = (hashCode*397) ^ SchemeExpenses.GetHashCode();
                hashCode = (hashCode*397) ^ InterestCharge.GetHashCode();
                hashCode = (hashCode*397) ^ InterestCredit.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("ServiceCost: {0}, SchemeExpenses: {1}, InterestCharge: {2}, InterestCredit: {3}", ServiceCost, SchemeExpenses, InterestCharge, InterestCredit);
        }
    }
}