﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FlightDeck.DomainShared
{
    public class SchemeDetail : IEquatable<SchemeDetail>, IEqualityComparer<SchemeDetail>
    {
        public int SchemeDetailId { get; set; }
        public virtual IList<SchemeDocument> Documents { get; set; }
        public string SchemeName { get; set; }
        public string CurrencySymbol { get; set; }
        public string WelcomeMessage { get; set; }
        public byte[] Logo { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public string SchemeData_SchemeRef { get; set; }
        public DateTime? SchemeData_AnchorDate { get; set; }
        public int? SchemeData_Id { get; set; }
        public DateTime? SchemeData_ImportedOnDate { get; set; }
        public string SchemeData_ImportedByUser { get; set; }
        public DateTime? SchemeData_ProducedOnDate { get; set; }
        public string SchemeData_ProducedByUser { get; set; }
        public string SchemeData_Spreadsheet { get; set; }
        public string SchemeData_Comment { get; set; }
        public string SchemeData_ExportFile { get; set; }
        public DateTime UpdatedOnDate { get; set; }
        public string UpdatedByUser { get; set; }
        public int? CreatedByUserId { get; set; }
        public bool HasVolatilities { get; set; }
        public bool HasVarCorrelations { get; set; }
        public bool IsRestricted { get; set; }
        public string AccountingDownloadsPassword { get; set; }
        public string DataCaptureVersion { get; set; }

        public SchemeDetail() { }
        public SchemeDetail(
            int id,
            string schemeName,
            string currency,
            string welcomeMessage,
            byte[] logo,
            string contactName,
            string contactNumber,
            string contactEmail,
            string schemeData_SchemeRef,
            DateTime? schemeData_AnchorDate,
            int? schemeData_Id,
            DateTime? schemeData_ImportedOnDate,
            string schemeData_ImportedByUser,
            DateTime? schemeData_ProducedOnDate,
            string schemeData_ProducedByUser,
            string schemeData_Spreadsheet,
            string schemeData_Comment,
            string schemeData_ExportFile,
            DateTime updatedOnDate,
            string updatedByUser,
            bool hasVolatilities,
            bool hasVarCorrelations,
            bool isRestricted,
            string accountingDownloadsPassword,
            string dataCaptureVersion)
        {
            SchemeDetailId = id;
            SchemeName = SchemeName;
            CurrencySymbol = currency;
            WelcomeMessage = welcomeMessage;
            Logo = logo;
            ContactName = contactName;
            ContactNumber = contactNumber;
            ContactEmail = contactEmail;
            SchemeData_SchemeRef = schemeData_SchemeRef;
            SchemeData_AnchorDate = schemeData_AnchorDate;
            SchemeData_Id = schemeData_Id;
            SchemeData_ImportedOnDate = schemeData_ImportedOnDate;
            SchemeData_ImportedByUser = schemeData_ImportedByUser;
            SchemeData_ProducedOnDate = schemeData_ProducedOnDate;
            SchemeData_ProducedByUser = schemeData_ProducedByUser;
            SchemeData_Spreadsheet = schemeData_Spreadsheet;
            SchemeData_Comment = schemeData_Comment;
            SchemeData_ExportFile = schemeData_ExportFile;
            UpdatedOnDate = updatedOnDate;
            UpdatedByUser = updatedByUser;
            HasVolatilities = hasVolatilities;
            HasVarCorrelations = hasVarCorrelations;
            IsRestricted = isRestricted;
            AccountingDownloadsPassword = accountingDownloadsPassword;
            DataCaptureVersion = dataCaptureVersion;
        }

        public override bool Equals(object obj)
        {
            return obj.GetType() == typeof(SchemeDetail) && this.SchemeDetailId == (obj as SchemeDetail).SchemeDetailId;
        }

        public bool Equals(SchemeDetail x, SchemeDetail y)
        {
            return x.SchemeDetailId == y.SchemeDetailId;
        }

        public int GetHashCode(SchemeDetail obj)
        {
            return (obj as SchemeDetail).SchemeDetailId.GetHashCode();
        }

        public override int GetHashCode()
        {
            return this.SchemeDetailId.GetHashCode();
        }

        public bool Equals(SchemeDetail other)
        {
            return !object.ReferenceEquals(other, null) && this.SchemeDetailId == other.SchemeDetailId;
        }

        public static bool operator ==(SchemeDetail scheme1, SchemeDetail scheme2)
        {
            if (object.ReferenceEquals(scheme1, scheme2)) return true;
            if (object.ReferenceEquals(scheme1, null)) return false;
            if (object.ReferenceEquals(scheme2, null)) return false;

            return scheme1.Equals(scheme2);
        }

        public static bool operator !=(SchemeDetail scheme1, SchemeDetail scheme2)
        {
            if (object.ReferenceEquals(scheme1, scheme2)) return false;
            if (object.ReferenceEquals(scheme1, null)) return true;
            if (object.ReferenceEquals(scheme2, null)) return true;

            return !scheme1.Equals(scheme2);
        }
    }
}
