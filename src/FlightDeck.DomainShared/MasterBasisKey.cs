﻿namespace FlightDeck.DomainShared
{
    using FlightDeck.DomainShared;
    using System;

    public class MasterBasisKey : IEquatable<MasterBasisKey>
    {
        public int Id { get; private set; }
        public BasisType BasisType { get; private set; }
        public string Name { get; private set; }
        public int PensionSchemeId { get; private set; }

        public MasterBasisKey(int id, BasisType basisType, string name, int pensionSchemeId)
        {
            Id = id;
            BasisType = basisType;
            Name = name;
            PensionSchemeId = pensionSchemeId;
        }

        public bool Equals(MasterBasisKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id
                && BasisType == other.BasisType
                && string.Equals(Name, other.Name)
                && PensionSchemeId == other.PensionSchemeId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((MasterBasisKey)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id.GetHashCode();
                hashCode = (hashCode * 397) ^ BasisType.GetHashCode();
                hashCode = (hashCode * 397) ^ Name.GetHashCode();
                hashCode = (hashCode * 397) ^ PensionSchemeId.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Id: {0}, BasisType: {1}, Name: {2}, PensionSchemeId: {3}", Id.ToString(), BasisType.ToString(), Name, PensionSchemeId.ToString());
        }
    }
}
