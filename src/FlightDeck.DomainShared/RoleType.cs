﻿namespace FlightDeck.DomainShared
{
    using System;
    using System.ComponentModel.DataAnnotations;
    
    public enum RoleType
    {
        [Display(Name = "Admin")]
        Admin = 1,
        [Display(Name = "Installer")]
        Installer = 2,
        [Display(Name = "Client")]
        Client = 3,
        [Display(Name = "KPMG Consultant")]
        Consultant = 4,
        [Display(Name = "Adviser")]
        Adviser = 5
    }
}
