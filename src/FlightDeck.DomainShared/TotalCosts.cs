﻿namespace FlightDeck.DomainShared
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TotalCosts : IEquatable<TotalCosts>
    {
        public double Assets { get; private set; }
        public double Active { get; private set; }
        public double Deferred { get; private set; }
        public double Pension { get; private set; }
        public double ServiceCost { get; private set; }

        public double Liabilities
        {
            get
            {
                return Active + Deferred + Pension;
            }
        }
        public double AssetsAll // poor name!
        {
            get
            {
                return Assets + AssetComponents.Values.Sum();
            }
        }

        public IDictionary<ClientAssetType, double> AssetComponents { get; private set; }

        public double Deficit { get { return Liabilities - Assets; } }

        public double GetAssetTotal(params ClientAssetType[] include)
        {
            return Assets + AssetComponents.Where(x => include.Contains(x.Key)).Sum(x => x.Value);
        }

        public TotalCosts(double assets, double active, double deferred, double pension, double serviceCost, IDictionary<ClientAssetType, double> newAssets = null)
        {
            Assets = assets;
            ServiceCost = serviceCost;
            Pension = pension;
            Deferred = deferred;
            Active = active;

            AssetComponents = newAssets ?? new Dictionary<ClientAssetType, double>();
            foreach (var v in Enum.GetValues(typeof(ClientAssetType)))
            {
                if (!AssetComponents.ContainsKey((ClientAssetType)v))
                    AssetComponents.Add((ClientAssetType)v, 0);
            }
        }

        public bool Equals(TotalCosts other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(Assets, other.Assets)
                && Utils.EqualityCheck(Active, other.Active)
                && Utils.EqualityCheck(Deferred, other.Deferred)
                && Utils.EqualityCheck(Pension, other.Pension)
                && Utils.EqualityCheck(ServiceCost, other.ServiceCost);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((TotalCosts) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Assets.GetHashCode();
                hashCode = (hashCode*397) ^ Active.GetHashCode();
                hashCode = (hashCode*397) ^ Deferred.GetHashCode();
                hashCode = (hashCode*397) ^ Pension.GetHashCode();
                hashCode = (hashCode*397) ^ ServiceCost.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Assets: {0}, Active: {1}, Deferred: {2}, Pension: {3}, ServiceCost: {4}, Liabilities: {5}, Deficit: {6}", Assets, Active, Deferred, Pension, ServiceCost, Liabilities, Deficit);
        }
    }
}