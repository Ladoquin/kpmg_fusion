﻿namespace FlightDeck.DomainShared
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class WaterfallOptions
    {
        public double CI { get; set; }
        public int Time { get; set; }

        public WaterfallOptions(double ci, int time)
        {
            CI = ci;
            Time = time;
        }
    }
}