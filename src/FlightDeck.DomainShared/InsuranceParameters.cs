﻿namespace FlightDeck.DomainShared
{
    using System;
    
    [Serializable]
    public class InsuranceParameters
    {
        public double PensionerLiabilityPercInsured { get; private set; }
        public double NonpensionerLiabilityPercInsured { get; private set; }

        public InsuranceParameters(double pensionerLiabilityInsured, double nonpensionerLiabilityInsured)
        {
            PensionerLiabilityPercInsured = pensionerLiabilityInsured;
            NonpensionerLiabilityPercInsured = nonpensionerLiabilityInsured;
        }
    }
}
