﻿using System;

namespace FlightDeck.DomainShared
{
    public class SurplusAnalysisData : IEquatable<SurplusAnalysisData>
    {
        public double InitialDeficit { get; private set; }
        public double InterestOnLiabilities { get; private set; }
        public double MarketChange { get; private set; }
        public double LiabExperience { get; private set; }
        public double Contributions { get; private set; }
        public double AssetReturn { get; private set; }
        public double AssetExperience { get; private set; }
        public double EndDeficit { get; private set; }        

        public SurplusAnalysisData(double initialDeficit, double interestOnLiabilities, double marketChange,
            double liabExperience, double contributions, double assetReturn, double assetExperience, double endDeficit)
        {
            AssetExperience = assetExperience;
            LiabExperience = liabExperience;
            MarketChange = marketChange;
            EndDeficit = endDeficit;
            AssetReturn = assetReturn;
            Contributions = contributions;
            InterestOnLiabilities = interestOnLiabilities;
            InitialDeficit = initialDeficit;
        }

        public bool Equals(SurplusAnalysisData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(InitialDeficit, other.InitialDeficit)
                && Utils.EqualityCheck(InterestOnLiabilities, other.InterestOnLiabilities)
                && Utils.EqualityCheck(MarketChange, other.MarketChange)
                && Utils.EqualityCheck(Contributions, other.Contributions)
                && Utils.EqualityCheck(AssetReturn, other.AssetReturn)
                && Utils.EqualityCheck(EndDeficit, other.EndDeficit)
                && Utils.EqualityCheck(LiabExperience, other.LiabExperience)
                && Utils.EqualityCheck(AssetExperience, other.AssetExperience);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((SurplusAnalysisData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = InitialDeficit.GetHashCode();
                hashCode = (hashCode * 397) ^ InterestOnLiabilities.GetHashCode();
                hashCode = (hashCode * 397) ^ Contributions.GetHashCode();
                hashCode = (hashCode * 397) ^ MarketChange.GetHashCode();
                hashCode = (hashCode * 397) ^ AssetReturn.GetHashCode();
                hashCode = (hashCode * 397) ^ EndDeficit.GetHashCode();
                hashCode = (hashCode * 397) ^ LiabExperience.GetHashCode();
                hashCode = (hashCode * 397) ^ AssetExperience.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("InitialDeficit: {0}, InterestOnLiabilities: {1}, MarketChange: {2}, Contributions: {3}, AssetReturn: {4}, EndDeficit: {5}, LiabExperience: {6}, AssetExperience: {7}", InitialDeficit, InterestOnLiabilities, MarketChange, Contributions, AssetReturn, EndDeficit, LiabExperience, AssetExperience);
        }
    }
}