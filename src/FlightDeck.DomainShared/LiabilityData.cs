using System;

namespace FlightDeck.DomainShared
{
    public class LiabilityData : IEquatable<LiabilityData>
    {
        public double ActivePast { get; private set; }
        public double ActiveFuture { get; private set; }
        public double Deferred { get; private set; }
        public double Pensioners { get; private set; }
         
        public LiabilityData(double activePast, double activeFuture, double deferred, double pensioners)
        {
            Pensioners = pensioners;
            Deferred = deferred;
            ActiveFuture = activeFuture;
            ActivePast = activePast;
        }

        public bool Equals(LiabilityData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(ActivePast, other.ActivePast) &&
                   Utils.EqualityCheck(ActiveFuture, other.ActiveFuture) &&
                   Utils.EqualityCheck(Deferred, other.Deferred) &&
                   Utils.EqualityCheck(Pensioners, other.Pensioners);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((LiabilityData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = ActivePast.GetHashCode();
                hashCode = (hashCode*397) ^ ActiveFuture.GetHashCode();
                hashCode = (hashCode*397) ^ Deferred.GetHashCode();
                hashCode = (hashCode*397) ^ Pensioners.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("ActivePast: {0}, ActiveFuture: {1}, Deferred: {2}, Pensioners: {3}", ActivePast, ActiveFuture, Deferred, Pensioners);
        }
    }
}