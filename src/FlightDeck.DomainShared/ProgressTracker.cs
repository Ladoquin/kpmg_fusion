﻿using System;

namespace FlightDeck.DomainShared
{
    public class ProgressTracker<TProgress, TResult>
    {
        public Exception Exception { get; private set; }
        public TProgress Progress { get; private set; }
        public bool Completed { get; private set; }
        public TResult Result { get; private set; }

        public void SetException(Exception ex) { Exception = ex; }
        public void SetProgress(TProgress progress) { Progress = progress; }
        public void SetComplete() { Completed = true; }
        public void SetComplete(TResult result) { Result = result; SetComplete(); }
    }
}
