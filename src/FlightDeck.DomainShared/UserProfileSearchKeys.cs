﻿namespace FlightDeck.DomainShared
{
    using System.Collections.Generic;

    public class UserProfileSearchKeys
    {
        public int UserId { get; set; }
        public RoleType Role { get; set; }
        public string LastName { get; set; }
        public int? CreatedByUserId { get; set; }
        public List<int> AccessibleSchemeIds { get; set; }
    }
}
