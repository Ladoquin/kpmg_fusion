﻿namespace FlightDeck.DomainShared
{
    using System;

    public class UserLiabilityData : LiabilityData, IEquatable<UserLiabilityData>
    {
        public double ActDuration { get; private set; }
        public double DefDuration { get; private set; }
        public double PenDuration { get; private set; }
        public double DeferredBuyin { get; private set; }
        public double PensionerBuyin { get; private set; }

        public UserLiabilityData(double activePast, double deferred, double pensioners, double activeFuture, double actDuration, double defDuration, double penDuration, double deferredBuyin, double pensionerBuyin)
            : base(activePast, activeFuture, deferred, pensioners)
        {
            ActDuration = actDuration;
            DefDuration = defDuration;
            PenDuration = penDuration;
            DeferredBuyin = deferredBuyin;
            PensionerBuyin = pensionerBuyin;
        }

        public bool Equals(UserLiabilityData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(ActivePast, other.ActivePast) &&
                   Utils.EqualityCheck(ActiveFuture, other.ActiveFuture) &&
                   Utils.EqualityCheck(Deferred, other.Deferred) &&
                   Utils.EqualityCheck(Pensioners, other.Pensioners) &&
                   Utils.EqualityCheck(ActDuration, other.ActDuration) &&
                   Utils.EqualityCheck(DefDuration, other.DefDuration) &&
                   Utils.EqualityCheck(PenDuration, other.PenDuration) &&
                   Utils.EqualityCheck(DeferredBuyin, other.DeferredBuyin) &&
                   Utils.EqualityCheck(PensionerBuyin, other.PensionerBuyin);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((UserLiabilityData)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = ActivePast.GetHashCode();
                hashCode = (hashCode * 397) ^ ActiveFuture.GetHashCode();
                hashCode = (hashCode * 397) ^ Deferred.GetHashCode();
                hashCode = (hashCode * 397) ^ Pensioners.GetHashCode();
                hashCode = (hashCode * 397) ^ ActDuration.GetHashCode();
                hashCode = (hashCode * 397) ^ DefDuration.GetHashCode();
                hashCode = (hashCode * 397) ^ PenDuration.GetHashCode();
                hashCode = (hashCode * 397) ^ DeferredBuyin.GetHashCode();
                hashCode = (hashCode * 397) ^ PensionerBuyin.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("ActivePast: {0}, Deferred: {1}, Pensioners: {2}; ActiveFuture: {3}; ActDuration: {4}; DefDuration: {5}; PenDuration: {6}; DeferredBuyin: {7}, PensionerBuyin: {8}", ActivePast, Deferred, Pensioners, ActiveFuture, ActDuration, DefDuration, PenDuration, DeferredBuyin, PensionerBuyin);
        }
    }
}
