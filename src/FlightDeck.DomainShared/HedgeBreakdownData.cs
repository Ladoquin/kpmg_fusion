﻿namespace FlightDeck.DomainShared
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    [Serializable]
    public class HedgeBreakdownData : IEquatable<HedgeBreakdownData>
    {
        public IDictionary<HedgeType, HedgeBreakdownItem> CurrentBasis { get; set; }
        public IDictionary<HedgeType, HedgeBreakdownItem> Cashflows { get; set; }

        public bool Equals(HedgeBreakdownData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            var equal = true;

            if (CurrentBasis == null || Cashflows == null || other.CurrentBasis == null || other.Cashflows == null)
            {
                equal = false;
            }
            else if (!CurrentBasis.Keys.OrderBy(key => key).SequenceEqual(other.CurrentBasis.Keys.OrderBy(key => key)) ||
                     !Cashflows.Keys.OrderBy(key => key).SequenceEqual(other.Cashflows.Keys.OrderBy(key => key)))
            {
                equal = false;
            }
            else
            {
                foreach (var key in CurrentBasis.Keys)
                {
                    if (!CurrentBasis[key].Equals(other.CurrentBasis[key]))
                        equal = false;
                }
                foreach (var key in Cashflows.Keys)
                {
                    if (!Cashflows[key].Equals(other.Cashflows[key]))
                        equal = false;
                }
            }

            return equal;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((HedgeBreakdownData)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = CurrentBasis.GetHashCode();
                hashCode = (hashCode * 397) ^ Cashflows.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            var s = new StringBuilder(500);
            if (CurrentBasis != null)
                foreach (var key in CurrentBasis.Keys)
                    s.AppendFormat("CurrentBasis[{0}]: {1}", key.ToString(), CurrentBasis[key].ToString());
            if (s.Length > 0)
                s.AppendLine();
            if (Cashflows != null)
                foreach (var key in Cashflows.Keys)
                    s.AppendFormat("Cashflows[{0}]: {1}", key.ToString(), Cashflows[key].ToString());
            return s.ToString();
        }
    }
}
