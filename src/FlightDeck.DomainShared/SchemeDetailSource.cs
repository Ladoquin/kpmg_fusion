﻿namespace FlightDeck.DomainShared
{
    using System.Xml.Linq;
    
    public class SchemeDetailSource
    {
        public int SchemeDetailId { get; private set; }
        public XDocument Source { get; private set; }

        public SchemeDetailSource(int schemeDetailId, string source)
            : this(schemeDetailId, string.IsNullOrEmpty(source) ? null : XDocument.Parse(source))
        {
        }

        public SchemeDetailSource(int schemeDetailId, XDocument source)
        {
            SchemeDetailId = schemeDetailId;
            Source = source;
        }
    }
}
