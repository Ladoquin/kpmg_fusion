﻿using System;

namespace FlightDeck.DomainShared
{
    public class SchemeDataSource
    {
        public DateTime? Ias19Disclosure { get; private set; }
        public DateTime? ValuationReport { get; private set; }
        public DateTime? AssetData { get; private set; }
        public DateTime? MemberData { get; private set; }
        public DateTime? BenefitData { get; private set; }

        public SchemeDataSource(DateTime? ias19Disclosure, DateTime? valuationReport,
            DateTime? assetData, DateTime? memberData, DateTime? benefitData)
        {
            BenefitData = benefitData;
            MemberData = memberData;
            AssetData = assetData;
            ValuationReport = valuationReport;
            Ias19Disclosure = ias19Disclosure;
        }
    }
}
