﻿using System;

namespace FlightDeck.DomainShared
{
    public class BeforeAfter<T> : IEquatable<BeforeAfter<T>>
    {
        public T Before { get; private set; }
        public T After { get; protected set; }
        public bool IsDirty { get; protected set; }

        public BeforeAfter()
        {
            Before = default(T);
            After = default(T);
        }

        public BeforeAfter(T before, T after)
        {
            Before = before;
            After = after;
        }

        public override string ToString()
        {
            return string.Format("Before: {0}, After: {1}", Before, After);
        }

        public bool Equals(BeforeAfter<T> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Before.Equals(other.Before) && After.Equals(other.After);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((BeforeAfter<T>) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return Before.GetHashCode()*397 ^ After.GetHashCode();
            }
        }
    }

    public static class BeforeAfterExtensions
    {
        public static bool DoubleEquals(this BeforeAfter<double> original, BeforeAfter<double> other)
        {
            return Utils.EqualityCheck(original.Before, other.Before)
                && Utils.EqualityCheck(original.After, other.After);
        }
    }

    public class MutableBeforeAfter<T> : BeforeAfter<T>
    {
        public MutableBeforeAfter(T before) : base(before, before)
        {
        }

        public void Set(T val)
        {
            After = val;
            IsDirty = true;
        }

        public void Reset()
        {
            After = Before;
            IsDirty = false;
        }
    }
}