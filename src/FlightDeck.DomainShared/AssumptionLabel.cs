﻿namespace FlightDeck.DomainShared
{
    public class AssumptionLabel
    {
        public string Label { get; private set; }
        public bool IsVisible { get; private set; }

        public AssumptionLabel(string label, bool visible)
        {
            Label = label;
            IsVisible = visible;
        }
    }
}
