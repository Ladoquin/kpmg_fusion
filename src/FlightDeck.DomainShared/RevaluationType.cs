﻿namespace FlightDeck.DomainShared
{
    public enum RevaluationType
    {
        None = 0,
        Cpi = 1,
        Rpi = 2,
        Fixed = 3
    }
}