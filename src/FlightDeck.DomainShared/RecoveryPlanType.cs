﻿namespace FlightDeck.DomainShared
{
    public enum RecoveryPlanType
    {
        Current = 1,
        NewFixed, 
        NewDynamic,
    }
}