﻿namespace FlightDeck.DomainShared
{
    public enum SimpleAssumptionType
    {
        ExpenseLoading = 1,
        MaleMarriedProportion,
        FemaleMarriedProportion,
        SacrificeProportion,
        LifeExpectancy65,
        LifeExpectancy65_45,
        CommutationAllowance
    }
}