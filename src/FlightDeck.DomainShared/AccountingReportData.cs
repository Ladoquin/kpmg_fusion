﻿namespace FlightDeck.DomainShared
{
    using System;
    
    public class AccountingReportData<T> where T : class
    {
        public string SchemeName { get; private set; }
        public string CurrencySymbol { get; private set; }
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }        
        public T AtStart { get; private set; }
        public T AtEnd { get; private set; }

        public AccountingReportData(string schemeName, string currencySymbol, DateTime start, DateTime end, T atStart, T atEnd)
        {
            SchemeName = schemeName;
            CurrencySymbol = currencySymbol;
            Start = start;
            End = end;
            AtStart = atStart;
            AtEnd = atEnd;
        }
    }
}
