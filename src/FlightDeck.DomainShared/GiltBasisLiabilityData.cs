﻿namespace FlightDeck.DomainShared
{
    public class GiltBasisLiabilityData
    {
        public double TotalLiab { get; private set; }
        public double LiabDuration { get; private set; }
        public double LiabPV01 { get; private set; }
        public double LiabIE01 { get; private set; }
        public double BuyInPV01 { get; private set; }
        public double BuyInIE01 { get; private set; }
        public HedgeData HedgeData { get; private set; }

        public GiltBasisLiabilityData(double totalLiab, double liabDuration, double liabPV01, double liabIE01, double buyinPV01, double buyinIE01, HedgeData hedgeData)
        {
            TotalLiab = totalLiab;
            LiabDuration = liabDuration;
            LiabPV01 = liabPV01;
            LiabIE01 = liabIE01;
            BuyInPV01 = buyinPV01;
            BuyInIE01 = buyinIE01;
            HedgeData = hedgeData;
        }
    }
}
