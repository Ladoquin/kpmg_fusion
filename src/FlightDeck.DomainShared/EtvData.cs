﻿using System;

namespace FlightDeck.DomainShared
{
    public class EtvData : IEquatable<EtvData>
    {
        public double DeferredLiabilities { get; private set; }
        public double CetvSize { get; private set; }
        public double EquivalentEtv { get; private set; }
        public double CetvPaid { get; private set; }
        public double EtvPaid { get; private set; }
        public double LiabilityDischarged { get; private set; }
        public double CostToCompany { get; private set; }
        public double ChangeInPosition { get; private set; }
        public double AssetsDisinvested { get; private set; }

        public EtvData(
            double deferredLiabilities,
            double cetvSize,
            double equivalentEtv,
            double cetvPaid,
            double etvPaid,
            double liabilityDischarged,
            double costToCompany,
            double changeInPosition, 
            double assetsDisinvested)
        {
            DeferredLiabilities = deferredLiabilities;
            CetvSize = cetvSize;
            EquivalentEtv = equivalentEtv;
            CetvPaid = cetvPaid;
            EtvPaid = etvPaid;
            LiabilityDischarged = liabilityDischarged;
            CostToCompany = costToCompany;
            ChangeInPosition = changeInPosition;
            AssetsDisinvested = assetsDisinvested;
        }

        public bool Equals(EtvData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(DeferredLiabilities, other.DeferredLiabilities)
                && Utils.EqualityCheck(CetvSize, other.CetvSize)
                && Utils.EqualityCheck(EquivalentEtv, other.EquivalentEtv)
                && Utils.EqualityCheck(CetvPaid, other.CetvPaid)
                && Utils.EqualityCheck(EtvPaid, other.EtvPaid)
                && Utils.EqualityCheck(LiabilityDischarged, other.LiabilityDischarged)
                && Utils.EqualityCheck(CostToCompany, other.CostToCompany)
                && Utils.EqualityCheck(ChangeInPosition, other.ChangeInPosition);
               // until refactor complete, don't check this value && Utils.EqualityCheck(AssetsDisinvested, other.AssetsDisinvested);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((EtvData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = DeferredLiabilities.GetHashCode();
                hashCode = (hashCode * 397) ^ CetvSize.GetHashCode();
                hashCode = (hashCode * 397) ^ EquivalentEtv.GetHashCode();
                hashCode = (hashCode * 397) ^ CetvPaid.GetHashCode();
                hashCode = (hashCode * 397) ^ EtvPaid.GetHashCode();
                hashCode = (hashCode * 397) ^ LiabilityDischarged.GetHashCode();
                hashCode = (hashCode * 397) ^ CostToCompany.GetHashCode();
                hashCode = (hashCode * 397) ^ ChangeInPosition.GetHashCode();
                //hashCode = (hashCode * 397) ^ AssetsDisinvested.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("DeferredLiabilities: {0}, CetvSize: {1}, EquivalentEtv: {2}, CetvPaid: {3}, EtvPaid: {4}, LiabilityDischarged: {5}, CostToCompany: {6}, ChangeInPosition: {7}, AssetsDisinvested: {8}", DeferredLiabilities, CetvSize, EquivalentEtv, CetvPaid, EtvPaid, LiabilityDischarged, CostToCompany, ChangeInPosition, AssetsDisinvested);
        }
    }
}
