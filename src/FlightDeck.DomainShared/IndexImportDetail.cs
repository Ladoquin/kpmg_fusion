﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.DomainShared
{
    public class IndexImportDetail
    {
        public int IndexImportDetailId { get; set; }

        public DateTime? ImportedOnDate { get; set; }

        public string ImportedByUser { get; set; }

        public DateTime? ProducedOnDate { get; set; }

        public string ProducedByUser { get; set; }

        public string Spreadsheet { get; set; }

        public string Comment { get; set; }

        public string ExportFile { get; set; }

        public DateTime? IndexStartDate { get; set; }

        public DateTime? IndexEndDate { get; set; }

        public virtual List<int> Indices { get; set; }
    }

    public class IndexImportIndices
    {
        public int IndexImportDetailId { get; set; }
        public int IndexId { get; set; }
    }
}
