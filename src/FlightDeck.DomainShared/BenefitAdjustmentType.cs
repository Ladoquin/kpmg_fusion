﻿namespace FlightDeck.DomainShared
{
    public enum BenefitAdjustmentType
    {
        None,
        BenefitReduction,
        SalaryLimit,
        CareRevaluation,
        DcImplementation
    }
}