﻿namespace FlightDeck.DomainShared
{
    using System;

    public class NewJPAssetOutput
    {
        public double AssetsAtStart { get; set; }
        public double EmployeeContributions { get; set; }
        public double EmployerContributions { get; set; }
        public double RecoveryPlanContributions { get; set; }
        public double ExtraContributions { get; set; }
        public double InsuranceIncome { get; set; }
        public double OutgoingBenefits { get; set; }
        public double NetIncome { get; set; }
        public double Growth { get; set; }
        public double AssetsAtEnd { get; set; }

        public NewJPAssetOutput(double assetsAtStart, double employeeConts, double employerConts, double recoveryPlanConts, double extraConts,
                                double insuranceIncome, double outgoingBenefits, double netIncome, double growth, double assetsAtEnd)
        {
            this.AssetsAtStart = assetsAtStart;
            this.EmployeeContributions = employeeConts;
            this.EmployerContributions = employerConts;
            this.RecoveryPlanContributions = recoveryPlanConts;
            this.ExtraContributions = extraConts;
            this.InsuranceIncome = insuranceIncome;
            this.OutgoingBenefits = outgoingBenefits;
            this.NetIncome = netIncome;
            this.Growth = growth;
            this.AssetsAtEnd = assetsAtEnd;
        }
    }
}
