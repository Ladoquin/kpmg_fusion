﻿namespace FlightDeck.DomainShared
{
    using System;

    [Serializable]
    public class EmbeddedFlexibleReturnOptions
    {
        public double AnnualTakeUpRate { get; private set; }
        public double EmbeddedFroBasisChange { get; set; }

        public EmbeddedFlexibleReturnOptions(double takeUpRate, double embeddedFroBasisChange)
        {
            AnnualTakeUpRate = takeUpRate;
            EmbeddedFroBasisChange = embeddedFroBasisChange;
        }
    }
}