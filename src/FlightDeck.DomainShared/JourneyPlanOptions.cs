﻿using System;
namespace FlightDeck.DomainShared
{
    public class JourneyPlanOptions
    {
        public bool IncludeBuyIns { get; set; }
        public int FundingBasisKey { get; set; }
        public int SelfSufficiencyBasisKey { get; set; }
        public int BuyoutBasisKey { get; set; }
        public SalaryType SalaryInflationType { get; set; }
        public double CurrentReturn { get; set; }
        public double SelfSufficiencyDiscountRate { get; set; }
        public int TriggerSteps { get; set; }
        public bool FundingLevelTrigger { get; set; }
        public double FundingLevel { get; set; }
        public double InitialReturn{ get; set; }
        public double FinalReturn{ get; set; }
        public int TriggerTimeStartYear { get; set; }
        public int TriggerTransitionPeriod { get; set; }
        public NewJPExtraContributionOptions RecoveryPlan { get; set; }
        public bool UseCurves { get; set; }

        public JourneyPlanOptions()
        {
            RecoveryPlan = new NewJPExtraContributionOptions();
        }
    }
}
