﻿namespace FlightDeck.DomainShared
{
    using System;

    [Serializable]
    public class FlexibleReturnOptions
    {
        public double EquivalentTansferValue { get; private set; }
        public double TakeUpRate { get; private set; }

        public FlexibleReturnOptions(double equivalentTransferValue, double takeUpRate)
        {
            TakeUpRate = takeUpRate;
            EquivalentTansferValue = equivalentTransferValue;
        }
    }
}