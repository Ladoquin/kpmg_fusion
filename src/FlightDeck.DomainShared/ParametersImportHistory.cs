﻿namespace FlightDeck.DomainShared
{
    using System;

    public class ParametersImportHistory
    {
        public int UserId { get; set; }
        public UserProfile User { get; set; }
        public DateTime DateCreated { get; set; }

        public ParametersImportHistory()
        {
        }
        public ParametersImportHistory(int userId, DateTime dateCreated)
        {
            UserId = userId;
            DateCreated = dateCreated;
        }
    }
}
