﻿namespace FlightDeck.DomainShared
{
    public class LDIHedgingData
    {
        public double Inflation { get; private set; }
        public double InterestRate { get; private set; }

        public LDIHedgingData(double inflation, double interestRate)
        {
            this.Inflation = inflation;
            this.InterestRate = interestRate;
        }
    }
}
