using System;
using System.Collections.Generic;

namespace FlightDeck.DomainShared
{
    [Serializable]
    public class AssumptionData : IEquatable<AssumptionData>
    {
        public double PreRetirementDiscountRate { get; private set; }
        public double PostRetirementDiscountRate { get; private set; }
        public double PensionDiscountRate { get; private set; }
        public double SalaryIncrease { get; private set; }
        public double InflationRetailPriceIndex { get; private set; }
        public double InflationConsumerPriceIndex { get; private set; }
        public double PensionerLifeExpectancy { get; private set; }
        public double DeferredLifeExpectancy { get; private set; }

        public AssumptionData(double preRetirementDiscountRate, double postRetirementDiscountRate,
            double pensionDiscountRate, double salaryIncrease, double inflationRetailPriceIndex,
            double inflationConsumerPriceIndex, double pensionerLifeExpectancy, double deferredLifeExpectancy)
        {
            DeferredLifeExpectancy = deferredLifeExpectancy;
            PensionerLifeExpectancy = pensionerLifeExpectancy;
            InflationConsumerPriceIndex = inflationConsumerPriceIndex;
            InflationRetailPriceIndex = inflationRetailPriceIndex;
            SalaryIncrease = salaryIncrease;
            PensionDiscountRate = pensionDiscountRate;
            PostRetirementDiscountRate = postRetirementDiscountRate;
            PreRetirementDiscountRate = preRetirementDiscountRate;
        }

        public Dictionary<AssumptionType, double> ToDictionary()
        {
           return new Dictionary<AssumptionType, double> 
            { 
                { AssumptionType.PreRetirementDiscountRate, PreRetirementDiscountRate }, 
                { AssumptionType.PostRetirementDiscountRate, PostRetirementDiscountRate }, 
                { AssumptionType.PensionDiscountRate, PensionDiscountRate }, 
                { AssumptionType.SalaryIncrease, SalaryIncrease }, 
                { AssumptionType.InflationRetailPriceIndex, InflationRetailPriceIndex }, 
                { AssumptionType.InflationConsumerPriceIndex, InflationConsumerPriceIndex } 
            };
        }
                
        public bool Equals(AssumptionData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(PreRetirementDiscountRate, other.PreRetirementDiscountRate) &&
                   Utils.EqualityCheck(PostRetirementDiscountRate, other.PostRetirementDiscountRate) &&
                   Utils.EqualityCheck(PensionDiscountRate, other.PensionDiscountRate) &&
                   Utils.EqualityCheck(SalaryIncrease, other.SalaryIncrease) &&
                   Utils.EqualityCheck(InflationRetailPriceIndex, other.InflationRetailPriceIndex) &&
                   Utils.EqualityCheck(InflationConsumerPriceIndex, other.InflationConsumerPriceIndex) &&
                   Utils.EqualityCheck(PensionerLifeExpectancy, other.PensionerLifeExpectancy) &&
                   Utils.EqualityCheck(DeferredLifeExpectancy, other.DeferredLifeExpectancy);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((AssumptionData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = PreRetirementDiscountRate.GetHashCode();
                hashCode = (hashCode*397) ^ PostRetirementDiscountRate.GetHashCode();
                hashCode = (hashCode*397) ^ PensionDiscountRate.GetHashCode();
                hashCode = (hashCode*397) ^ SalaryIncrease.GetHashCode();
                hashCode = (hashCode*397) ^ InflationRetailPriceIndex.GetHashCode();
                hashCode = (hashCode*397) ^ InflationConsumerPriceIndex.GetHashCode();
                hashCode = (hashCode*397) ^ PensionerLifeExpectancy.GetHashCode();
                hashCode = (hashCode*397) ^ DeferredLifeExpectancy.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("PreRetirementDiscountRate: {0}, PostRetirementDiscountRate: {1}, PensionDiscountRate: {2}, SalaryIncrease: {3}, InflationRetailPriceIndex: {4}, InflationConsumerPriceIndex: {5}, PensionerLifeExpectancy: {6}, DeferredLifeExpectancy: {7}", PreRetirementDiscountRate, PostRetirementDiscountRate, PensionDiscountRate, SalaryIncrease, InflationRetailPriceIndex, InflationConsumerPriceIndex, PensionerLifeExpectancy, DeferredLifeExpectancy);
        }
    }
}