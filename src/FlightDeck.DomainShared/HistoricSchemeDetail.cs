﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FlightDeck.DomainShared
{
    public class HistoricSchemeDetail : SchemeDetail
    {
        [Key]
        public int HistoricSchemeDetailId { get; set; }
        public int SourceSchemeDetailId { get; set; }

        public HistoricSchemeDetail() { }
        public HistoricSchemeDetail(
            int id,
            int sourceSchemeDetailId,
            string schemeName,
            string currencySymbol,
            string welcomeMessage,
            byte[] logo,
            string contactName,
            string contactNumber,
            string contactEmail,
            string schemeData_SchemeRef,
            DateTime? schemeData_AnchorDate,
            int? schemeData_Id,
            DateTime? schemeData_ImportedOnDate,
            string schemeData_ImportedByUser,
            DateTime? schemeData_ProducedOnDate,
            string schemeData_ProducedByUser,
            string schemeData_Spreadsheet,
            string schemeData_Comment,
            string schemeData_ExportFile,
            DateTime updatedOnDate,
            string updatedByUser,
            bool hasVolatilities,
            bool hasVarCorrelations,
            bool isRestricted,
            string accountingDownloadsPassword,
            string dataCaptureVersion)
        : base(
            sourceSchemeDetailId,
            schemeName,
            currencySymbol,
            welcomeMessage,
            logo,
            contactName,
            contactNumber,
            contactEmail,
            schemeData_SchemeRef,
            schemeData_AnchorDate,
            schemeData_Id,
            schemeData_ImportedOnDate,
            schemeData_ImportedByUser,
            schemeData_ProducedOnDate,
            schemeData_ProducedByUser,
            schemeData_Spreadsheet,
            schemeData_Comment,
            schemeData_ExportFile,
            updatedOnDate,
            updatedByUser,
            hasVolatilities,
            hasVarCorrelations,
            isRestricted,
            accountingDownloadsPassword,
            dataCaptureVersion)
        {
            HistoricSchemeDetailId = id;
            SourceSchemeDetailId = sourceSchemeDetailId;
        }

    }
}
