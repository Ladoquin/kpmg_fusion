﻿using System;

namespace FlightDeck.DomainShared
{
    [Serializable]
    public class RecoveryPlanAssumptions
    {
        public double InvestmentGrowth { get; private set; }
        public double DiscountIncrement { get; private set; }
        public bool IsGrowthFixed { get; private set; }
        public double LumpSumPayment { get; private set; }
        public double MaxLumpSumPayment { get; private set; }
        public double Increases { get; private set; }
        public int StartMonth { get; private set; }
        public int Term { get; private set; }
        public int StartYear { get { return 1 + StartMonth/12; } }

        public RecoveryPlanAssumptions(double investmentGrowth, double discountIncrement, double lumpSumPayment, double maxLumpSumPayment,
            double increases, bool isGrowthFixed, int startMonth, int term)
        {
            if (term < Utils.MinPlanTermInYears || term > Utils.MaxPlanTermInYears)
                throw new ArgumentException("term");
            if (startMonth < 0 || startMonth > 24)
                throw new ArgumentException("startMonth");

            Term = term;
            StartMonth = startMonth;
            IsGrowthFixed = isGrowthFixed;
            Increases = increases;
            LumpSumPayment = lumpSumPayment;
            MaxLumpSumPayment = maxLumpSumPayment;
            DiscountIncrement = discountIncrement;
            InvestmentGrowth = investmentGrowth;
            StartMonth = startMonth;
        }

        public RecoveryPlanAssumptions Clone()
        {
            return new RecoveryPlanAssumptions
                (
                    InvestmentGrowth,
                    DiscountIncrement, 
                    LumpSumPayment,
                    MaxLumpSumPayment, 
                    Increases,
                    IsGrowthFixed, 
                    StartMonth,
                    Term
                );
        }
    }
}