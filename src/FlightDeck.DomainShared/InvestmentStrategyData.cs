﻿namespace FlightDeck.DomainShared
{
    public class InvestmentStrategyData
    {
        public double TotalCoreAssets { get; private set; }

        /// <summary>
        /// BuyInAssetsAfter named range from Tracker
        /// </summary>
        public double Buyin { get; private set; }
        public double ABF { get; private set; }
        public double ABFAdjustment { get; private set; }

        public double TotalOtherAssets { get { return Buyin + ABF; } }
        public double TotalAssets { get { return TotalCoreAssets + TotalOtherAssets; } }

        public InvestmentStrategyData(double totalCoreAssets, double buyin, double abf, double abfAdjustment)
        {
            TotalCoreAssets = totalCoreAssets;
            Buyin = buyin;
            ABF = abf;
            ABFAdjustment = abfAdjustment;
        }

        public bool Equals(InvestmentStrategyData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(TotalCoreAssets, other.TotalCoreAssets)
                && Utils.EqualityCheck(Buyin, other.Buyin)
                && Utils.EqualityCheck(ABF, other.ABF)
                && Utils.EqualityCheck(ABFAdjustment, other.ABFAdjustment);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((InvestmentStrategyData)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = TotalCoreAssets.GetHashCode();
                hashCode = (hashCode * 397) ^ Buyin.GetHashCode();
                hashCode = (hashCode * 397) ^ ABF.GetHashCode();
                hashCode = (hashCode * 397) ^ ABFAdjustment.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("TotalCoreAssets: {0}, Buyin: {1}, ABF: {2}, ABFAdjustment: {3}", TotalCoreAssets, Buyin, ABF, ABFAdjustment);
        }
    }
}
