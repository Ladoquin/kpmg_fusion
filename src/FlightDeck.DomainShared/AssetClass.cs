﻿namespace FlightDeck.DomainShared
{
    using System.Collections.Generic;

    public class AssetClass
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public AssetCategory Category { get; private set; }
        public AssetReturnSpec ReturnSpec { get; private set; }
        public double Volatility { get; private set; }
        public IDictionary<AssetClassType, double> VarCorrelation { get; private set; }
        public AssetClassType Type { get; private set; }

        public AssetClass(int id, string name, AssetCategory category, AssetReturnSpec returnSpec, double volatility)
        {
            Id = id;
            Type = (AssetClassType)Id;
            Name = name;
            Category = category;
            ReturnSpec = returnSpec;
            Volatility = volatility;
        }
    }
}
