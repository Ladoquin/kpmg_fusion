﻿namespace FlightDeck.DomainShared
{
    public enum ClientAssetType
    {
        BuyinExisting,
        BuyinNew, 
        ABF
    }
}
