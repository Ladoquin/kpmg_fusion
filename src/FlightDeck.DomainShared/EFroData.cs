﻿using System;
using System.Collections.Generic;

namespace FlightDeck.DomainShared
{
    public class EFroData : IEquatable<EFroData>
    {
        public IDictionary<int, double> ActiveTransferValuesPayable { get; private set; }
        public IDictionary<int, double> DeferredTransferValuesPayable { get; private set; }
        public double CetvPaid { get; private set; }
        public double LiabilityDischarged { get; private set; }
        public double ChangeInActivesPastLiabilitiesDischarged { get; private set; }
        public double ChangeInDeferredsLiabilitiesDischarged { get; private set; }
        public double Improvement { get { return LiabilityDischarged - CetvPaid; } }
        public double NonPensionersLiabilities { get; private set; }

        public EFroData()
        {
            ActiveTransferValuesPayable = new Dictionary<int, double>();
            DeferredTransferValuesPayable = new Dictionary<int, double>();
        }
        public EFroData(double nonPensionersLiabilities)
        {
            ActiveTransferValuesPayable = new Dictionary<int, double>();
            DeferredTransferValuesPayable = new Dictionary<int, double>();
            NonPensionersLiabilities = nonPensionersLiabilities;
        }
        public EFroData(IDictionary<int, double> activeTransferValuesPayable, IDictionary<int, double> deferredTransferValuesPayable,
            double cetvPaid, double liabilityDischarged, double changeInActivesPastLiabilitiesDischarged, double changeInDeferredsLiabilitiesDischarged, double nonPensionersLiabilities)
        {
            CetvPaid = cetvPaid;
            LiabilityDischarged = liabilityDischarged;
            ActiveTransferValuesPayable = activeTransferValuesPayable;
            DeferredTransferValuesPayable = deferredTransferValuesPayable;
            ChangeInActivesPastLiabilitiesDischarged = changeInActivesPastLiabilitiesDischarged;
            ChangeInDeferredsLiabilitiesDischarged = changeInDeferredsLiabilitiesDischarged;
            NonPensionersLiabilities = nonPensionersLiabilities;
        }

        public bool Equals(EFroData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            var equals = Math.Abs(CetvPaid - other.CetvPaid) < Utils.TestPrecision
                && Math.Abs(LiabilityDischarged - other.LiabilityDischarged) < Utils.TestPrecision
                && Math.Abs(ChangeInActivesPastLiabilitiesDischarged - other.ChangeInActivesPastLiabilitiesDischarged) < Utils.TestPrecision
                && Math.Abs(ChangeInDeferredsLiabilitiesDischarged - other.ChangeInDeferredsLiabilitiesDischarged) < Utils.TestPrecision
                && Math.Abs(NonPensionersLiabilities - other.NonPensionersLiabilities) < Utils.TestPrecision;

            if (!equals)
                return false;

            if (!Utils.DictionariesRoughlyEquivalent(ActiveTransferValuesPayable, other.ActiveTransferValuesPayable, Utils.TestPrecision))
                return false;

            if (!Utils.DictionariesRoughlyEquivalent(DeferredTransferValuesPayable, other.DeferredTransferValuesPayable, Utils.TestPrecision))
                return false;

            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((EFroData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = CetvPaid.GetHashCode();
                hashCode = (hashCode * 397) ^ LiabilityDischarged.GetHashCode();
                hashCode = (hashCode * 397) ^ ActiveTransferValuesPayable.GetHashCode();
                hashCode = (hashCode * 397) ^ DeferredTransferValuesPayable.GetHashCode();
                hashCode = (hashCode * 397) ^ ChangeInActivesPastLiabilitiesDischarged.GetHashCode();
                hashCode = (hashCode * 397) ^ ChangeInDeferredsLiabilitiesDischarged.GetHashCode();
                hashCode = (hashCode * 397) ^ NonPensionersLiabilities.GetHashCode();
                return hashCode;
            }
        }
    }
}