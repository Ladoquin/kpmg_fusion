﻿namespace FlightDeck.DomainShared
{
    using System;

    [AttributeUsage(AttributeTargets.Field)]
    public class AssetClassTypeClientVisibleAttribute : Attribute
    {
        public bool ClientVisible { get; set; }

        public AssetClassTypeClientVisibleAttribute(bool clientVisible)
        {
            ClientVisible = clientVisible;
        }
    }
}
