﻿namespace FlightDeck.DomainShared
{
    public class HedgeData
    {
        public double GiltLDIOverlayInterestHedge { get; private set; }
        public double GiltLDIOverlayInflationHedge { get; private set; }
        public double CurrLDIOverlayInterestHedge { get; private set; }
        public double CurrLDIOverlayInflationHedge { get; private set; }
        public double BuyInInterestHedge { get; private set; }
        public double BuyInInflationHedge { get; private set; }
        public double BuyInLongevityHedge { get; private set; }

        public HedgeBreakdownData Breakdown { get; set; }

        public HedgeData(double giltLDIOverlayInterestHedge, double giltLDIOverlayInflationHedge, double currLDIOverlayInterestHedge, double currLDIOverlayInflationHedge, double buyInInterestHedge, double buyInInflationHedge, double buyInLongevityHedge)
        {
            GiltLDIOverlayInterestHedge = giltLDIOverlayInterestHedge;
            GiltLDIOverlayInflationHedge = giltLDIOverlayInflationHedge;
            CurrLDIOverlayInterestHedge = currLDIOverlayInterestHedge;
            CurrLDIOverlayInflationHedge = currLDIOverlayInflationHedge;
            BuyInInterestHedge = buyInInterestHedge;
            BuyInInflationHedge = buyInInflationHedge;
            BuyInLongevityHedge = buyInLongevityHedge;
        }
    }
}