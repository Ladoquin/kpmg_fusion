﻿namespace FlightDeck.DomainShared
{
    using System;

    [Serializable]
    public class AbfOptions
    {
        public double Value { get; private set; }
        public double RecoveryPlanLength { get; private set; }
        public double AbfLength { get; private set; }

        public AbfOptions(double value, double recoveryPlanLength = Utils.AbfRecoveryPlanLength, double abfLength = Utils.AbfLength)
        {
            AbfLength = abfLength;
            RecoveryPlanLength = recoveryPlanLength;
            Value = value;
        }
    }
}