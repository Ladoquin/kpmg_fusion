﻿namespace FlightDeck.DomainShared
{
    public enum CurveDataType
    {
        Gilt, 
        RPI,
        CPI
    }
}
