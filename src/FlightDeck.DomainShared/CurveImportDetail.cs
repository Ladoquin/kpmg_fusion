﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.DomainShared
{
    public class CurveImportDetail //todo: refactor... do we need seperate index / curve import details?
    {
        public int Id { get; set; }

        public DateTime? ImportedOnDate { get; set; }

        public string ImportedByUser { get; set; }

        public DateTime? ProducedOnDate { get; set; }

        public string ProducedByUser { get; set; }

        public string Spreadsheet { get; set; }

        public string Comment { get; set; }
    }
}
