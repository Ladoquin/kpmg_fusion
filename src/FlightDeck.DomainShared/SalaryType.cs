﻿namespace FlightDeck.DomainShared
{
    public enum SalaryType
    {
        RPI = 1, 
        CPI = 2,
        Zero = 3
    }
}
