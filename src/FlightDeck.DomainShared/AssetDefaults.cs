﻿namespace FlightDeck.DomainShared
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class AssetDefaults
    {
        /// <summary>
        /// Default asset returns, used for backward compatibility with schemes that didn't have all asset classes defined
        /// </summary>
        public static Dictionary<AssetClassType, double> DefaultAssetReturns = new Dictionary<AssetClassType, double>
            {
                { AssetClassType.Equity, 0.05 },
                { AssetClassType.Property, 0.02 },
                { AssetClassType.DiversifiedGrowth, 0.05 },
                { AssetClassType.DiversifiedCredit, 0.018 },
                { AssetClassType.PrivateMarkets, 0.08 },
                { AssetClassType.CorporateBonds, 0.012 },
                { AssetClassType.FixedInterestGilts, 0 },
                { AssetClassType.IndexLinkedGilts, 0 },
                { AssetClassType.Cash, 0 },
                { AssetClassType.Abf, 0.012 }
            };

        public static AssetReturnSpec DefaultAssetReturnSpec = new AssetReturnSpec(-0.025, 0.095, 0.0005);
    }
}
