﻿namespace FlightDeck.DomainShared
{
    using System.ComponentModel.DataAnnotations;

    public enum AssetClassType
    {
        [AssetClassTypeCategory(AssetCategory.Growth)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Equities")]
        Equity = 10,

        [AssetClassTypeCategory(AssetCategory.Matched)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Asset Backed Fund")]
        Abf = 9,

        [AssetClassTypeCategory(AssetCategory.Matched)]
        [Display(Name = "Gilts")]
        Gilts = 3,

        [AssetClassTypeCategory(AssetCategory.Matched)]
        [Display(Name = "Non Gilts")]
        NonGilts = 11,

        [AssetClassTypeCategory(AssetCategory.Growth)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Property")]
        Property = 4,

        [AssetClassTypeCategory(AssetCategory.Growth)]
        [Display(Name = "Hedge Fund")]
        HedgeFunds = 5,

        [AssetClassTypeCategory(AssetCategory.Matched)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Cash")]
        Cash = 8,

        [AssetClassTypeCategory(AssetCategory.Matched)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Fixed Interest Gilts")]
        FixedInterestGilts = 12,

        [AssetClassTypeCategory(AssetCategory.Matched)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Corporate Bonds")]
        CorporateBonds = 6,

        [AssetClassTypeCategory(AssetCategory.Growth)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Diversified Growth")]
        DiversifiedGrowth = 13,

        [AssetClassTypeCategory(AssetCategory.Growth)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Diversified Credit")]
        DiversifiedCredit = 14,

        [AssetClassTypeCategory(AssetCategory.Growth)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Private Markets")]
        PrivateMarkets = 15,

        [AssetClassTypeCategory(AssetCategory.Matched)]
        [AssetClassTypeClientVisible(true)]
        [Display(Name = "Index Linked Gilts")]
        IndexLinkedGilts = 7,

        [AssetClassTypeCategory(AssetCategory.None)]
        [Display(Name = "Liability Interest")]
        LiabilityInterest = 101,

        [AssetClassTypeCategory(AssetCategory.None)]
        [Display(Name = "Liability Longevity")]
        LiabilityLongevity = 102,

        [AssetClassTypeCategory(AssetCategory.None)]
        [Display(Name = "Credit - Spread")]
        CreditSpread = 104,

        [AssetClassTypeCategory(AssetCategory.None)]
        [Display(Name = "Liability Inflation")]
        LiabilityInflation = 105,

        [AssetClassTypeCategory(AssetCategory.None)]
        [Display(Name = "Index Linked Gilts - Inflation")]
        IndexLinkedGiltsInflation = 106,

        [AssetClassTypeCategory(AssetCategory.None)]
        [AssetClassTypeSynthetic(true)]
        SyntheticEquityOutgo,
        [AssetClassTypeCategory(AssetCategory.None)]
        [AssetClassTypeSynthetic(true)]
        SyntheticEquityIncome,
        [AssetClassTypeCategory(AssetCategory.None)]
        [AssetClassTypeSynthetic(true)]
        SyntheticCreditOutgo,
        [AssetClassTypeCategory(AssetCategory.None)]
        [AssetClassTypeSynthetic(true)]
        SyntheticCreditIncome,

        [AssetClassTypeCategory(AssetCategory.None)]
        [Display(Name = "Buy in")]
        Buyin = 999
    }
}
