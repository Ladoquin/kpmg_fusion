﻿namespace FlightDeck.DomainShared
{
    using System;

    [Serializable]
    public class FutureBenefitsParameters
    {
        public double MemberContributionsIncrease { get; private set; }
        public BenefitAdjustmentType AdjustmentType { get; private set; }
        public double AdjustmentRate { get; private set; }

        public bool AdjustBenefits
        {
            get { return AdjustmentType != BenefitAdjustmentType.None; }
        }

        public FutureBenefitsParameters(double memberContributionsIncrease, BenefitAdjustmentType adjustmentType, double adjustmentRate)
        {
            AdjustmentRate = adjustmentRate;
            AdjustmentType = adjustmentType;
            MemberContributionsIncrease = memberContributionsIncrease;
        }
    }
}