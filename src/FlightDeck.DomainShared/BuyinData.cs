﻿namespace FlightDeck.DomainShared
{
    using System;

    public class BuyinData : IEquatable<BuyinData>
    {
        public double UninsuredPensionerLiability { get; private set; }
        public double UninsuredNonPensionerLiability { get; private set; }
        public double PensionerLiabilityToBeInsured { get; private set; }
        public double NonPensionerLiabilityToBeInsured { get; private set; }

        public BuyinData(double uninsuredPenLiab, double uninsuredNonPenLiab, double penLiabToBeInsured, double nonPenLiabToBeInsured)
        {
            UninsuredPensionerLiability = uninsuredPenLiab;
            UninsuredNonPensionerLiability = uninsuredNonPenLiab;
            PensionerLiabilityToBeInsured = penLiabToBeInsured;
            NonPensionerLiabilityToBeInsured = nonPenLiabToBeInsured;
        }

        public bool Equals(BuyinData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(UninsuredPensionerLiability, other.UninsuredPensionerLiability)
                && Utils.EqualityCheck(UninsuredNonPensionerLiability, other.UninsuredNonPensionerLiability)
                && Utils.EqualityCheck(PensionerLiabilityToBeInsured, other.PensionerLiabilityToBeInsured)
                && Utils.EqualityCheck(NonPensionerLiabilityToBeInsured, other.NonPensionerLiabilityToBeInsured);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((BuyinData)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = UninsuredPensionerLiability.GetHashCode();
                hashCode = (hashCode * 397) ^ UninsuredNonPensionerLiability.GetHashCode();
                hashCode = (hashCode * 397) ^ PensionerLiabilityToBeInsured.GetHashCode();
                hashCode = (hashCode * 397) ^ NonPensionerLiabilityToBeInsured.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("UninsuredPensionerLiability: {0}, UninsuredNonPensionerLiability: {1}, PensionerLiabilityToBeInsured: {2}, NonPensionerLiabilityToBeInsured: {3}", UninsuredPensionerLiability, UninsuredNonPensionerLiability, PensionerLiabilityToBeInsured, NonPensionerLiabilityToBeInsured);
        }
    }
}
