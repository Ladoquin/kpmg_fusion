﻿namespace FlightDeck.DomainShared
{
    using System;
    using System.Collections.Generic;

    public class NewJPLiabilityOutput
    {
        public double ActivePastLiability { get; set; }
        public double DeferredLiability { get; set; }
        public double PensionerLiability { get; set; }
        public double ServiceCost { get; set; }
        public double AccrualLiability { get; set; }
        public double DeferredBuyin { get; set; }
        public double PensionerBuyin { get; set; }

        public NewJPLiabilityOutput(double activePastLiability, double deferredLiability, double pensionerLiability,
                                    double serviceCost, double accrualLiability, double deferredBuyin, double pensionerBuyin)
        {
            this.ActivePastLiability = activePastLiability;
            this.DeferredLiability = deferredLiability;
            this.PensionerLiability = pensionerLiability;
            this.ServiceCost = serviceCost;
            this.AccrualLiability = accrualLiability;
            this.DeferredBuyin = deferredBuyin;
            this.PensionerBuyin = pensionerBuyin;
        }
    }
}
