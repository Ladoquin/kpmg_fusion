﻿using System.Collections.Generic;

namespace FlightDeck.DomainShared
{
    public class NamedPropertyGroup
    {
        public NamedPropertyGroupType Type { get; private set; }
        public string Name { get; private set; }
        public IList<KeyValuePair<string, string>> Properties { get; private set; }

        public NamedPropertyGroup(NamedPropertyGroupType type, string name, IList<KeyValuePair<string, string>> properties)
        {
            Properties = properties;
            Name = name;
            Type = type;
        }
    }
}