﻿using System.Collections.Generic;

namespace FlightDeck.DomainShared
{
    public class UserProfile
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool TermsAccepted { get; set; }
        public bool PasswordMustChange { get; set; }
        public RoleType Role { get; set; }
        public IEnumerable<SchemeDetail> SchemeDetails { get; set; }
        public int? ActiveSchemeDetailId { get; set; }
        public int? CreatedByUserId { get; set; }
        public SchemeDetail ActiveSchemeDetail { get; set; }
        public int PasswordFailuresSinceLastSuccess { get; set; }
        public IEnumerable<SchemeDetail> FavouriteSchemes { get; set; }

        public UserProfile() { }
        public UserProfile(
            int id,
            string userName,
            string firstName,
            string lastName,
            string emailAddress,
            bool termsAccepted,
            bool passwordMustChange,
            IEnumerable<SchemeDetail> schemeDetails,
            int? activeSchemeDetailId
            )
        {
            UserId = id;
            UserName = userName;
            FirstName = firstName;
            LastName = lastName;
            EmailAddress = emailAddress;
            TermsAccepted = termsAccepted;
            PasswordMustChange = passwordMustChange;
            SchemeDetails = schemeDetails;
            ActiveSchemeDetailId = activeSchemeDetailId;
        }
    }
}