﻿using System;

namespace FlightDeck.DomainShared
{
    public class BalanceData : IEquatable<BalanceData>
    {
        public double Assets { get; private set; }
        public double Liabilities { get; private set; }
        public double SurplusDeficit { get { return Assets - Liabilities; } }

        public BalanceData(double assets, double liabilities)
        {
            Assets = assets;
            Liabilities = liabilities;
        }

        public bool Equals(BalanceData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(Assets, other.Assets, Utils.TestPrecisionType.ToPence)
                && Utils.EqualityCheck(Liabilities, other.Liabilities, Utils.TestPrecisionType.ToPence);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((BalanceData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Assets.GetHashCode()*397) ^ Liabilities.GetHashCode();
            }
        }

        public override string ToString()
        {
            return string.Format("Assets: {0}, Liabilities: {1}, SurplusDeficit: {2}", Assets, Liabilities, SurplusDeficit);
        }
    }
}