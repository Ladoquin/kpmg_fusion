﻿namespace FlightDeck.DomainShared
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    
    public enum AssetCategory
    {
        None = 0, //Used for asset classes that aren't in asset categories but are required for system asset calculations
        Growth = 1,
        Matched,
        Other
    }
}