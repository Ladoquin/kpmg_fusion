﻿namespace FlightDeck.DomainShared
{
    public enum VarStrategy
    {
        IncreaseHedging = 1,
        Diversification,
        DeRisk,
        ReRisk,
        Custom
    }
}