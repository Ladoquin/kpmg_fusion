﻿namespace FlightDeck.DomainShared
{
    public class SyntheticAssetInfo
    {
        public double EquityProportion { get; private set; }
        public double CreditProportion { get; private set; }
        public double EquityAmount { get; set; }
        public double CreditAmount { get; set; }
        public double TotalProportion
        {
            get
            {
                return EquityProportion + CreditProportion;
            }
        }
        public double TotalAmount
        {
            get
            {
                return EquityAmount + CreditAmount;
            }
        }

        public SyntheticAssetInfo(double equityProp, double creditProp)
        {
            EquityProportion = equityProp;
            CreditProportion = creditProp;
        }
    }

    public static class SyntheticAssetInfoExtensions
    {
        public static void SetAmounts(this SyntheticAssetInfo syntheticAssetInfo, double totalFunds)
        {
            syntheticAssetInfo.EquityAmount = syntheticAssetInfo.EquityProportion * totalFunds;
            syntheticAssetInfo.CreditAmount = syntheticAssetInfo.CreditProportion * totalFunds;
        }
    }
}
