﻿using System;

namespace FlightDeck.DomainShared
{
    public class FutureBenefitsData : IEquatable<FutureBenefitsData>
    {
        public double AnnualCost { get; private set; }
        public double ContributionRate { get; private set; }
        public double EmployeeRate { get; private set; }
        public double EmployerRate { get; private set; }
        public double AnnualCostChange { get; private set; }
        public double EmployerRateChange { get; private set; }
        public double LiabilitiesChange { get; private set; }
        public double ServiceCostEstimate { get; private set; }
        public double ContributionRateEstimate { get; private set; }
        public double EstimatedBenefitReduction { get; private set; }
        public double ChangeIn5Years { get; private set; }
        public double ChangeIn10Years { get; private set; }

        public FutureBenefitsData(double annualCost, double contributionRate,
            double employeeRate, double employerRate, double annualCostChange, 
            double employerRateChange, double liabilitiesChange, double serviceCostEstimate, 
            double contributionRateEstimate, double estimatedBenefitReduction, double changeIn5Years, double changeIn10Years)
        {
            ChangeIn10Years = changeIn10Years;
            ChangeIn5Years = changeIn5Years;
            EstimatedBenefitReduction = estimatedBenefitReduction;
            ContributionRateEstimate = contributionRateEstimate;
            ServiceCostEstimate = serviceCostEstimate;
            LiabilitiesChange = liabilitiesChange;
            EmployerRateChange = employerRateChange;
            AnnualCostChange = annualCostChange;
            EmployerRate = employerRate;
            EmployeeRate = employeeRate;
            ContributionRate = contributionRate;
            AnnualCost = annualCost;
        }

        public bool Equals(FutureBenefitsData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(AnnualCost, other.AnnualCost)
                && Utils.EqualityCheck(ContributionRate, other.ContributionRate)
                && Utils.EqualityCheck(EmployeeRate, other.EmployeeRate)
                && Utils.EqualityCheck(EmployerRate, other.EmployerRate)
                && Utils.EqualityCheck(AnnualCostChange, other.AnnualCostChange)
                && Utils.EqualityCheck(EmployerRateChange, other.EmployerRateChange)
                && Utils.EqualityCheck(LiabilitiesChange, other.LiabilitiesChange)
                && Utils.EqualityCheck(ServiceCostEstimate, other.ServiceCostEstimate)
                && Utils.EqualityCheck(ContributionRateEstimate, other.ContributionRateEstimate)
                && Utils.EqualityCheck(EstimatedBenefitReduction, other.EstimatedBenefitReduction)
                && Utils.EqualityCheck(ChangeIn5Years, other.ChangeIn5Years)
                && Utils.EqualityCheck(ChangeIn10Years, other.ChangeIn10Years);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((FutureBenefitsData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = AnnualCost.GetHashCode();
                hashCode = (hashCode*397) ^ ContributionRate.GetHashCode();
                hashCode = (hashCode*397) ^ EmployeeRate.GetHashCode();
                hashCode = (hashCode*397) ^ EmployerRate.GetHashCode();
                hashCode = (hashCode*397) ^ AnnualCostChange.GetHashCode();
                hashCode = (hashCode*397) ^ EmployerRateChange.GetHashCode();
                hashCode = (hashCode*397) ^ LiabilitiesChange.GetHashCode();
                hashCode = (hashCode*397) ^ ServiceCostEstimate.GetHashCode();
                hashCode = (hashCode*397) ^ ContributionRateEstimate.GetHashCode();
                hashCode = (hashCode*397) ^ EstimatedBenefitReduction.GetHashCode();
                hashCode = (hashCode*397) ^ ChangeIn5Years.GetHashCode();
                hashCode = (hashCode*397) ^ ChangeIn10Years.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("AnnualCost: {0}, ContributionRate: {1}, EmployeeRate: {2}, EmployerRate: {3}, AnnualCostChange: {4}, EmployerRateChange: {5}, LiabilitiesChange: {6}, ServiceCostEstimate: {7}, ContributionRateEstimate: {8}, EstimatedBenefitReduction: {9}, ChangeIn5years: {10}, ChangeIn10years: {11}", AnnualCost, ContributionRate, EmployeeRate, EmployerRate, AnnualCostChange, EmployerRateChange, LiabilitiesChange, ServiceCostEstimate, ContributionRateEstimate, EstimatedBenefitReduction, ChangeIn5Years, ChangeIn10Years);
        }
    }
}