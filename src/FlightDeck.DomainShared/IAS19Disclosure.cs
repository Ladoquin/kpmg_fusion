﻿namespace FlightDeck.DomainShared
{
    using System;

    public class IAS19Disclosure : IEquatable<IAS19Disclosure>
    {
        public DateTime Date { get; private set; }
        public DisclosureAssumptions Assumptions { get; private set; }
        public IAS19BalanceSheetAmountData BalanceSheetAmounts { get; private set; }
        public DefinedBenefitObligationData DefinedBenefitObligation { get; set; }
        public IAS19FairValueSchemeAssetChangesData FairValueSchemeAssetChanges { get; set; }
        public IAS19SchemeSurplusChangesData SchemeSurplusChanges { get; set; }
        public IAS19PAndLForecastData PAndLForecast { get; set; }

        public IAS19Disclosure(DateTime date, DisclosureAssumptions assumptions, IAS19BalanceSheetAmountData balanceSheetAmounts, DefinedBenefitObligationData definedBenefitObligation, IAS19FairValueSchemeAssetChangesData fairValueSchemeAssetChanges, IAS19SchemeSurplusChangesData schemeSurplusChanges, IAS19PAndLForecastData pAndLForecast)
        {
            Date = date;
            Assumptions = assumptions;
            BalanceSheetAmounts = balanceSheetAmounts;
            DefinedBenefitObligation = definedBenefitObligation;
            FairValueSchemeAssetChanges = fairValueSchemeAssetChanges;
            SchemeSurplusChanges = schemeSurplusChanges;
            PAndLForecast = pAndLForecast;
        }

        public bool Equals(IAS19Disclosure other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date.Equals(other.Date) && Equals(Assumptions, other.Assumptions) && Equals(BalanceSheetAmounts, other.BalanceSheetAmounts) && Equals(DefinedBenefitObligation, other.DefinedBenefitObligation) && Equals(FairValueSchemeAssetChanges, other.FairValueSchemeAssetChanges) && Equals(SchemeSurplusChanges, other.SchemeSurplusChanges) && Equals(PAndLForecast, other.PAndLForecast);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((IAS19Disclosure)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ (Assumptions != null ? Assumptions.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (BalanceSheetAmounts != null ? BalanceSheetAmounts.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (DefinedBenefitObligation != null ? DefinedBenefitObligation.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (FairValueSchemeAssetChanges != null ? FairValueSchemeAssetChanges.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (SchemeSurplusChanges != null ? SchemeSurplusChanges.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PAndLForecast != null ? PAndLForecast.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Date: {0}, Assumptions: {1}, BalanceSheetAmounts: {2}, DefinedBenefitObligation: {3}, FairValueSchemeAssetChanges: {4}, SchemeSurplusChanges: {5}, PAndLForecast: {6}", Date, Assumptions, BalanceSheetAmounts, DefinedBenefitObligation, FairValueSchemeAssetChanges, SchemeSurplusChanges, PAndLForecast);
        }

        #region Data classes

        public class IAS19BalanceSheetAmountData : IEquatable<IAS19BalanceSheetAmountData>
        {
            public double DefinedBenefitObligation { get; private set; }
            public double FairValueOfSchemeAssets { get; private set; }
            public double SchemeSurplus { get; private set; }

            public IAS19BalanceSheetAmountData()
            {
            }
            public IAS19BalanceSheetAmountData(double definedBenefitObligation, double fairValueOfSchemeAssets, double schemeSurplus)
            {
                DefinedBenefitObligation = definedBenefitObligation;
                FairValueOfSchemeAssets = fairValueOfSchemeAssets;
                SchemeSurplus = schemeSurplus;
            }

            public bool Equals(IAS19BalanceSheetAmountData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(DefinedBenefitObligation, other.DefinedBenefitObligation)
                    && Utils.EqualityCheck(FairValueOfSchemeAssets, other.FairValueOfSchemeAssets)
                    && Utils.EqualityCheck(SchemeSurplus, other.SchemeSurplus);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((IAS19BalanceSheetAmountData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = DefinedBenefitObligation.GetHashCode();
                    hashCode = (hashCode * 397) ^ FairValueOfSchemeAssets.GetHashCode();
                    hashCode = (hashCode * 397) ^ SchemeSurplus.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("DefinedBenefitObligation: {0}, FairValueOfSchemeAssets: {1}, SchemeSurplus: {2}", DefinedBenefitObligation, FairValueOfSchemeAssets, SchemeSurplus);
            }
        }

        public class DefinedBenefitObligationData : IEquatable<DefinedBenefitObligationData>
        {
            public double OpeningDefinedBenefitObligation { get; private set; }
            public double CurrentServiceCost { get; private set; }
            public double InterestExpense { get; private set; }
            public double ActuarialLosses { get; private set; }
            public double Contributions { get; private set; }
            public double BenefitsPaid { get; private set; }
            public double ClosingDefinedBenefitObligation { get; private set; }

            public DefinedBenefitObligationData()
            {
            }
            public DefinedBenefitObligationData(double openingDefinedBenefitObligation, double currentServiceCost, double interestExpense, double actuarialLosses, double contributions, double benefitsPaid, double closingDefinedBenefitObligation)
            {
                OpeningDefinedBenefitObligation = openingDefinedBenefitObligation;
                CurrentServiceCost = currentServiceCost;
                InterestExpense = interestExpense;
                ActuarialLosses = actuarialLosses;
                Contributions = contributions;
                BenefitsPaid = benefitsPaid;
                ClosingDefinedBenefitObligation = closingDefinedBenefitObligation;
            }

            public bool Equals(DefinedBenefitObligationData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(OpeningDefinedBenefitObligation, other.OpeningDefinedBenefitObligation)
                    && Utils.EqualityCheck(CurrentServiceCost, other.CurrentServiceCost)
                    && Utils.EqualityCheck(InterestExpense, other.InterestExpense)
                    && Utils.EqualityCheck(ActuarialLosses, other.ActuarialLosses)
                    && Utils.EqualityCheck(Contributions, other.Contributions)
                    && Utils.EqualityCheck(BenefitsPaid, other.BenefitsPaid)
                    && Utils.EqualityCheck(ClosingDefinedBenefitObligation, other.ClosingDefinedBenefitObligation);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((DefinedBenefitObligationData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = OpeningDefinedBenefitObligation.GetHashCode();
                    hashCode = (hashCode * 397) ^ CurrentServiceCost.GetHashCode();
                    hashCode = (hashCode * 397) ^ InterestExpense.GetHashCode();
                    hashCode = (hashCode * 397) ^ ActuarialLosses.GetHashCode();
                    hashCode = (hashCode * 397) ^ Contributions.GetHashCode();
                    hashCode = (hashCode * 397) ^ BenefitsPaid.GetHashCode();
                    hashCode = (hashCode * 397) ^ ClosingDefinedBenefitObligation.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("OpeningDefinedBenefitObligation: {0}, CurrentServiceCost: {1}, InterestExpense: {2}, ActuarialLosses: {3}, Contributions: {4}, BenefitsPaid: {5}, ClosingDefinedBenefitObligation: {6}", OpeningDefinedBenefitObligation, CurrentServiceCost, InterestExpense, ActuarialLosses, Contributions, BenefitsPaid, ClosingDefinedBenefitObligation);
            }
        }

        public class IAS19FairValueSchemeAssetChangesData : IEquatable<IAS19FairValueSchemeAssetChangesData>
        {
            public double OpeningFairValueSchemeAssets { get; private set; }
            public double InterestIncome { get; private set; }
            public double ReturnOnSchemeAssets { get; private set; }
            public double SchemeAdminExpenses { get; private set; }
            public double ContributionsEmployer { get; private set; }
            public double ContributionsMembers { get; private set; }
            public double BenefitsPaid { get; private set; }
            public double ClosingFairValueSchemeAssets { get; private set; }

            public IAS19FairValueSchemeAssetChangesData()
            {
            }
            public IAS19FairValueSchemeAssetChangesData(double openingFairValueSchemeAssets, double interestIncome, double returnOnSchemeAssets, double schemeAdminExpenses, double contributionsEmployer, double contributionsMembers, double benefitsPaid, double closingFairValueSchemeAssets)
            {
                OpeningFairValueSchemeAssets = openingFairValueSchemeAssets;
                InterestIncome = interestIncome;
                ReturnOnSchemeAssets = returnOnSchemeAssets;
                SchemeAdminExpenses = schemeAdminExpenses;
                ContributionsEmployer = contributionsEmployer;
                ContributionsMembers = contributionsMembers;
                BenefitsPaid = benefitsPaid;
                ClosingFairValueSchemeAssets = closingFairValueSchemeAssets;
            }

            public bool Equals(IAS19FairValueSchemeAssetChangesData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(OpeningFairValueSchemeAssets, other.OpeningFairValueSchemeAssets)
                    && Utils.EqualityCheck(InterestIncome, other.InterestIncome)
                    && Utils.EqualityCheck(ReturnOnSchemeAssets, other.ReturnOnSchemeAssets)
                    && Utils.EqualityCheck(SchemeAdminExpenses, other.SchemeAdminExpenses)
                    && Utils.EqualityCheck(ContributionsEmployer, other.ContributionsEmployer)
                    && Utils.EqualityCheck(ContributionsMembers, other.ContributionsMembers)
                    && Utils.EqualityCheck(BenefitsPaid, other.BenefitsPaid)
                    && Utils.EqualityCheck(ClosingFairValueSchemeAssets, other.ClosingFairValueSchemeAssets);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((IAS19FairValueSchemeAssetChangesData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = OpeningFairValueSchemeAssets.GetHashCode();
                    hashCode = (hashCode * 397) ^ InterestIncome.GetHashCode();
                    hashCode = (hashCode * 397) ^ ReturnOnSchemeAssets.GetHashCode();
                    hashCode = (hashCode * 397) ^ SchemeAdminExpenses.GetHashCode();
                    hashCode = (hashCode * 397) ^ ContributionsEmployer.GetHashCode();
                    hashCode = (hashCode * 397) ^ ContributionsMembers.GetHashCode();
                    hashCode = (hashCode * 397) ^ BenefitsPaid.GetHashCode();
                    hashCode = (hashCode * 397) ^ ClosingFairValueSchemeAssets.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("OpeningFairValueSchemeAssets: {0}, InterestIncome: {1}, ReturnOnSchemeAssets: {2}, SchemeAdminExpenses: {3}, ContributionsEmployer: {4}, ContributionsMembers: {5}, BenefitsPaid: {6}, ClosingFairValueSchemeAssets: {7}", OpeningFairValueSchemeAssets, InterestIncome, ReturnOnSchemeAssets, SchemeAdminExpenses, ContributionsEmployer, ContributionsMembers, BenefitsPaid, ClosingFairValueSchemeAssets);
            }
        }

        public class IAS19SchemeSurplusChangesData : IEquatable<IAS19SchemeSurplusChangesData>
        {
            public double SchemeSurplusBeginning { get; private set; }
            public double CurrentServiceCost { get; private set; }
            public double SchemeAdminExpenses { get; private set; }
            public double NetInterest { get; private set; }
            public double PAndLTotal { get; private set; }
            public double ActuarialLossesOnLiabilities { get; private set; }
            public double ReturnOnSchemeAssets { get; private set; }
            public double OCITotal { get; private set; }
            public double Contributions { get; private set; }
            public double SchemeSurplusEnd { get; private set; }

            public IAS19SchemeSurplusChangesData()
            {
            }
            public IAS19SchemeSurplusChangesData(double schemeSurplusBeginning, double currentServiceCost, double schemeAdminExpenses, double netInterest, double pAndLTotal, double actuarialLossesOnLiabilities, double returnOnSchemeAssets, double ociTotal, double contributions, double schemeSurplusEnd)
            {
                SchemeSurplusBeginning = schemeSurplusBeginning;
                CurrentServiceCost = currentServiceCost;
                SchemeAdminExpenses = schemeAdminExpenses;
                NetInterest = netInterest;
                PAndLTotal = pAndLTotal;
                ActuarialLossesOnLiabilities = actuarialLossesOnLiabilities;
                ReturnOnSchemeAssets = returnOnSchemeAssets;
                OCITotal = ociTotal;
                Contributions = contributions;
                SchemeSurplusEnd = schemeSurplusEnd;
            }

            public bool Equals(IAS19SchemeSurplusChangesData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(SchemeSurplusBeginning, other.SchemeSurplusBeginning)
                    && Utils.EqualityCheck(CurrentServiceCost, other.CurrentServiceCost)
                    && Utils.EqualityCheck(SchemeAdminExpenses, other.SchemeAdminExpenses)
                    && Utils.EqualityCheck(NetInterest, other.NetInterest)
                    && Utils.EqualityCheck(PAndLTotal, other.PAndLTotal)
                    && Utils.EqualityCheck(ActuarialLossesOnLiabilities, other.ActuarialLossesOnLiabilities)
                    && Utils.EqualityCheck(ReturnOnSchemeAssets, other.ReturnOnSchemeAssets)
                    && Utils.EqualityCheck(OCITotal, other.OCITotal)
                    && Utils.EqualityCheck(Contributions, other.Contributions)
                    && Utils.EqualityCheck(SchemeSurplusEnd, other.SchemeSurplusEnd);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((IAS19SchemeSurplusChangesData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = SchemeSurplusBeginning.GetHashCode();
                    hashCode = (hashCode * 397) ^ CurrentServiceCost.GetHashCode();
                    hashCode = (hashCode * 397) ^ SchemeAdminExpenses.GetHashCode();
                    hashCode = (hashCode * 397) ^ NetInterest.GetHashCode();
                    hashCode = (hashCode * 397) ^ PAndLTotal.GetHashCode();
                    hashCode = (hashCode * 397) ^ ActuarialLossesOnLiabilities.GetHashCode();
                    hashCode = (hashCode * 397) ^ ReturnOnSchemeAssets.GetHashCode();
                    hashCode = (hashCode * 397) ^ OCITotal.GetHashCode();
                    hashCode = (hashCode * 397) ^ Contributions.GetHashCode();
                    hashCode = (hashCode * 397) ^ SchemeSurplusEnd.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("SchemeSurplusBeginning: {0}, CurrentServiceCost: {1}, SchemeAdminExpenses: {2}, NetInterest: {3}, PAndLTotal: {4}, ActuarialLossesOnLiabilities: {5}, ReturnOnSchemeAssets: {6}, OCITotal: {7}, Contributions: {8}, SchemeSurplusEnd: {9}", SchemeSurplusBeginning, CurrentServiceCost, SchemeAdminExpenses, NetInterest, PAndLTotal, ActuarialLossesOnLiabilities, ReturnOnSchemeAssets, OCITotal, Contributions, SchemeSurplusEnd);
            }
        }

        public class IAS19PAndLForecastData : IEquatable<IAS19PAndLForecastData>
        {
            public double CurrentServiceCost { get; private set; }
            public double SchemeAdminExpenses { get; private set; }
            public double InterestCredit { get; private set; }
            public double InterestCharge { get; private set; }
            public double NetInterest { get; private set; }
            public double Total { get; private set; }

            public IAS19PAndLForecastData()
            {
            }
            public IAS19PAndLForecastData(double currentServiceCost, double schemeAdminExpenses, double interestCredit, double interestCharge, double netInterest, double total)
            {
                CurrentServiceCost = currentServiceCost;
                SchemeAdminExpenses = schemeAdminExpenses;
                InterestCredit = interestCredit;
                InterestCharge = interestCharge;
                NetInterest = netInterest;
                Total = total;
            }

            public bool Equals(IAS19PAndLForecastData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(CurrentServiceCost, other.CurrentServiceCost)
                    && Utils.EqualityCheck(SchemeAdminExpenses, other.SchemeAdminExpenses)
                    && Utils.EqualityCheck(InterestCredit, other.InterestCredit)
                    && Utils.EqualityCheck(InterestCharge, other.InterestCharge)
                    && Utils.EqualityCheck(NetInterest, other.NetInterest)
                    && Utils.EqualityCheck(Total, other.Total);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((IAS19PAndLForecastData)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = CurrentServiceCost.GetHashCode();
                    hashCode = (hashCode * 397) ^ SchemeAdminExpenses.GetHashCode();
                    hashCode = (hashCode * 397) ^ InterestCredit.GetHashCode();
                    hashCode = (hashCode * 397) ^ InterestCharge.GetHashCode();
                    hashCode = (hashCode * 397) ^ NetInterest.GetHashCode();
                    hashCode = (hashCode * 397) ^ Total.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("CurrentServiceCost: {0}, SchemeAdminExpenses: {1}, InterestCredit: {2}, InterestCharge: {3}, NetInterest: {4}, Total: {5}", CurrentServiceCost, SchemeAdminExpenses, InterestCredit, InterestCharge, NetInterest, Total);
            }
        }

        #endregion
    }
}
