﻿namespace FlightDeck.DomainShared
{
    public class AttributionData
    {
        public double AttrEeeContsReceived { get; private set; }
        public double AttrEerContsReceived { get; private set; }
        public double AttrTotalContsReceived { get; private set; }
        public double AttrInterestIncome { get; private set; }
        public double AttrRecoveryPlanConts { get; private set; }
        public double AttrSchemeAdminExpenses { get; private set; }
        public double AttrServiceCost { get; private set; }
        public double AttrTotBenefitOutgo { get; private set; }
        public double AttrNetBenefitOutgo { get; private set; }

        public AttributionData(
            double attrEeeContsReceived,
            double attrEerContsReceived,
            double attrTotalContsReceived,
            double attrInterestIncome,
            double attrRecoveryPlanConts,
            double attrSchemeAdminExpenses,
            double attrServiceCost,
            double attrTotBenefitOutgo,
            double attrNetBenefitOutgo)
        {
            AttrTotalContsReceived = attrTotalContsReceived;
            AttrEeeContsReceived = attrEeeContsReceived;
            AttrEerContsReceived = attrEerContsReceived;
            AttrInterestIncome = attrInterestIncome;
            AttrRecoveryPlanConts = attrRecoveryPlanConts;
            AttrSchemeAdminExpenses = attrSchemeAdminExpenses;
            AttrServiceCost = attrServiceCost;
            AttrTotBenefitOutgo = attrTotBenefitOutgo;
            AttrNetBenefitOutgo = attrNetBenefitOutgo;
        }
    }
}
