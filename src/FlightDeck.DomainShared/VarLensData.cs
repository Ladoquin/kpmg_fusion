﻿using System;

namespace FlightDeck.DomainShared
{
    public class VarLensData : IEquatable<VarLensData>
    {
        public double Interest { get; private set; }
        public double Longevity { get; private set; }
        public double Hedge { get; private set; }
        public double Credit { get; private set; }
        public double Equity { get; private set; }
        public double OtherGrowth { get; private set; }
        public double Diversification { get; private set; }
        public double TotalRisk { get; private set; }
        public double InterestInflationHedging { get; private set; }

        public VarLensData(double interest, double interestInflationHedging, double longevity, double hedge, double credit, double equity, double otherGrowth, double diversification, double totalRisk)
        {
            TotalRisk = totalRisk;
            Diversification = diversification;
            OtherGrowth = otherGrowth;
            Equity = equity;
            Credit = credit;
            Hedge = hedge;
            Longevity = longevity;
            Interest = interest;
            InterestInflationHedging = interestInflationHedging;
        }

        public override string ToString()
        {
            return string.Format("Interest: {0}, InterestInflationHedging: {1},  Longevity: {2}, Hedge: {3}, Credit: {4}, Equity: {5}, OtherGrowth: {6}, Diversification: {7}, TotalRisk: {8}", Interest, InterestInflationHedging, Longevity, Hedge, Credit, Equity, OtherGrowth, Diversification, TotalRisk);
        }

        public bool Equals(VarLensData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return
                Utils.EqualityCheck(Interest, other.Interest, precision: Utils.TestPrecisionType.ToPence) &&
                Utils.EqualityCheck(Longevity, other.Longevity, precision: Utils.TestPrecisionType.ToPence) &&
                Utils.EqualityCheck(Hedge, other.Hedge, precision: Utils.TestPrecisionType.ToPence) &&
                Utils.EqualityCheck(Credit, other.Credit, precision: Utils.TestPrecisionType.ToPence) &&
                Utils.EqualityCheck(Equity, other.Equity, precision: Utils.TestPrecisionType.ToPence) &&
                Utils.EqualityCheck(OtherGrowth, other.OtherGrowth, precision: Utils.TestPrecisionType.ToPence) &&
                Utils.EqualityCheck(Diversification, other.Diversification, precision: Utils.TestPrecisionType.ToPence) &&
                Utils.EqualityCheck(TotalRisk, other.TotalRisk, precision: Utils.TestPrecisionType.ToPence) &&
                Utils.EqualityCheck(InterestInflationHedging, other.InterestInflationHedging, precision: Utils.TestPrecisionType.ToPence);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((VarLensData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Interest.GetHashCode();
                hashCode = (hashCode*397) ^ Longevity.GetHashCode();
                hashCode = (hashCode*397) ^ Hedge.GetHashCode();
                hashCode = (hashCode*397) ^ Credit.GetHashCode();
                hashCode = (hashCode*397) ^ Equity.GetHashCode();
                hashCode = (hashCode*397) ^ OtherGrowth.GetHashCode();
                hashCode = (hashCode*397) ^ Diversification.GetHashCode();
                hashCode = (hashCode * 397) ^ TotalRisk.GetHashCode();
                hashCode = (hashCode * 397) ^ InterestInflationHedging.GetHashCode();
                return hashCode;
            }
        }
    }
}