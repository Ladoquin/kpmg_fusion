﻿namespace FlightDeck.DomainShared
{
    public enum AssumptionType
    {
        PreRetirementDiscountRate = 1,
        PostRetirementDiscountRate,
        PensionDiscountRate,
        SalaryIncrease,
        InflationRetailPriceIndex,
        InflationConsumerPriceIndex,
    }
}