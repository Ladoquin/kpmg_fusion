﻿using System;

namespace FlightDeck.DomainShared
{
    public class LensData : IEquatable<LensData>
    {
        public double Liabilities { get; private set; }
        public double Assets { get; private set; }
        public double Proportion { get; private set; }

        public LensData(double liabilities, double assets)
        {
            Liabilities = liabilities;
            Assets = assets;
            Proportion = Math.Round((Assets/Liabilities)*100, 2);
        }

        public bool Equals(LensData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(Liabilities, other.Liabilities)
                && Utils.EqualityCheck(Assets, other.Assets);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((LensData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Liabilities.GetHashCode();
                hashCode = (hashCode*397) ^ Assets.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Liabilities: {0:0.00}, Assets: {1:0.00}", Liabilities, Assets);
        }
    }
}