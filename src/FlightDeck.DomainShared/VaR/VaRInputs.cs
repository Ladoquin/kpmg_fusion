﻿namespace FlightDeck.DomainShared.VaR
{
    using System.Collections.Generic;
    
    public class VaRInputs
    {
        public VolatilityAssumptionResults VolatilityAssumptions { get; set; }
        public IDictionary<AssetClassType, IDictionary<AssetClassType, double>> NoSwapsCorrelationMatrixInputs { get; set; }
        public IDictionary<AssetClassType, IDictionary<AssetClassType, double>> SwapsCorrelationMatrixInputs { get; set; }        
    }
}
