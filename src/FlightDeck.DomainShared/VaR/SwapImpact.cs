﻿namespace FlightDeck.DomainShared.VaR
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public class SwapImpact
    {
        public double Allocation { get; set; }
        public double Notional { get; set; }
        public double Volatility { get; set; }
        public double VolatilityProduct { get; set; }
        public IDictionary<AssetClassType, double> CorrelationMatrix { get; set; }
    }
}
