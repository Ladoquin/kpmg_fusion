﻿namespace FlightDeck.DomainShared.VaR
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public class VolatilityAssumptionResults
    {
        public IDictionary<AssetClassType, double> Volatilities { get; private set; }
        public double RateInflationCorrelation { get; private set; }

        public VolatilityAssumptionResults(IDictionary<AssetClassType, double> volatilityAssumptions, double rateInflationCorrelation)
        {
            Volatilities = volatilityAssumptions;
            RateInflationCorrelation = rateInflationCorrelation;
        }
    }
}
