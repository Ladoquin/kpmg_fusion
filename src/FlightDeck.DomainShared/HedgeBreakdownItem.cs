﻿namespace FlightDeck.DomainShared
{
    using System;
    
    [Serializable]
    public class HedgeBreakdownItem : IEquatable<HedgeBreakdownItem>
    {
        public double PhysicalAssets { get; private set; }
        public double Buyin { get; private set; }
        public double LDIOverlay { get; set; }
        public double Total { get; set; }

        public HedgeBreakdownItem(double physicalAssets, double buyin, double ldiOverlay, double total)
        {
            PhysicalAssets = physicalAssets;
            Buyin = buyin;
            LDIOverlay = ldiOverlay;
            Total = total;
        }

        public bool Equals(HedgeBreakdownItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Math.Abs(PhysicalAssets - other.PhysicalAssets) < Utils.TestPrecision
                && Math.Abs(Buyin - other.Buyin) < Utils.TestPrecision
                && Math.Abs(LDIOverlay - other.LDIOverlay) < Utils.TestPrecision
                && Math.Abs(Total - other.Total) < Utils.TestPrecision;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((HedgeBreakdownItem)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = PhysicalAssets.GetHashCode();
                hashCode = (hashCode * 397) ^ Buyin.GetHashCode();
                hashCode = (hashCode * 397) ^ LDIOverlay.GetHashCode();
                hashCode = (hashCode * 397) ^ Total.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("PhysicalAssets: {0}, Buyin: {1}, LDIOverlay: {2}, Total: {3}", PhysicalAssets, Buyin, LDIOverlay, Total);
        }
    }
}
