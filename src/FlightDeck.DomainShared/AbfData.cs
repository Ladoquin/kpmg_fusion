﻿using System;

namespace FlightDeck.DomainShared
{
    public class AbfData : IEquatable<AbfData>
    {
        public double FundingDeficit { get; private set; }
        public double AnnualPayments { get; private set; }
        public double NetCashPosition1 { get; private set; }
        public double Proportion { get; private set; }
        public double AnnualContribution { get; private set; }
        public double NetCashPosition2 { get; private set; }
        public double CostSaving { get; private set; }
        public double Value { get; private set; }

        public AbfData(double fundingDeficit, double annualPayments, double netCashPosition1, double proportion, double annualContribution, double netCashPosition2, double costSaving, double value)
        {
            CostSaving = costSaving;
            NetCashPosition2 = netCashPosition2;
            AnnualContribution = annualContribution;
            Proportion = proportion;
            NetCashPosition1 = netCashPosition1;
            AnnualPayments = annualPayments;
            FundingDeficit = fundingDeficit;
            Value = value;
        }

        public bool Equals(AbfData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(FundingDeficit, other.FundingDeficit)
                && Utils.EqualityCheck(AnnualPayments, other.AnnualPayments)
                && Utils.EqualityCheck(NetCashPosition1, other.NetCashPosition1)
                && Utils.EqualityCheck(Proportion, other.Proportion)
                && Utils.EqualityCheck(AnnualContribution, other.AnnualContribution)
                && Utils.EqualityCheck(NetCashPosition2, other.NetCashPosition2)
                && Utils.EqualityCheck(CostSaving, other.CostSaving)
                && Utils.EqualityCheck(Value, other.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((AbfData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = FundingDeficit.GetHashCode();
                hashCode = (hashCode * 397) ^ AnnualPayments.GetHashCode();
                hashCode = (hashCode * 397) ^ NetCashPosition1.GetHashCode();
                hashCode = (hashCode * 397) ^ Proportion.GetHashCode();
                hashCode = (hashCode * 397) ^ AnnualContribution.GetHashCode();
                hashCode = (hashCode * 397) ^ NetCashPosition2.GetHashCode();
                hashCode = (hashCode * 397) ^ CostSaving.GetHashCode();
                hashCode = (hashCode * 397) ^ Value.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("FundingDeficit: {0}, AnnualPayments: {1}, NetCashPosition1: {2}, Proprotion: {3}, AnnualContribution: {4}, NetCashPosition2: {5}, CostSaving: {6}, Value: {7}", FundingDeficit, AnnualPayments, NetCashPosition1, Proportion, AnnualContribution, NetCashPosition2, CostSaving, Value);
        }
    }
}