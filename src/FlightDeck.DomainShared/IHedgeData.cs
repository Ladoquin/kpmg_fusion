﻿namespace FlightDeck.DomainShared
{
    public interface IHedgeData
    {
        double GiltLDIOverlayInterestHedge { get; }
        double GiltLDIOverlayInflationHedge { get; }
        double CurrLDIOverlayInterestHedge { get; }
        double CurrLDIOverlayInflationHedge { get; }
        double BuyInInterestHedge { get; }
        double BuyInInflationHedge { get; }
        double BuyInLongevityHedge { get; }
        HedgeBreakdownData Breakdown { get; }
    }
}
