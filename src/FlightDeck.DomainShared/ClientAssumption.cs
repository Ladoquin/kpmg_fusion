﻿namespace FlightDeck.DomainShared
{
    using System;

    public class ClientAssumption
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int SchemeDetailId { get; private set; }
        public int MasterBasisId { get; private set; }
        public DateTime AnalysisDate { get; private set; }
        public string Data { get; private set; }
        public int CreatedByUserId { get; private set; }

        public ClientAssumption(int id, string name, int schemeDetailId, DateTime analysisDate, string data, int createdByUserId, int masterBasisId)
        {
            Id = id;
            Name = name;
            SchemeDetailId = schemeDetailId;
            MasterBasisId = masterBasisId;
            AnalysisDate = analysisDate;
            CreatedByUserId = createdByUserId;
            Data = data;
        }
    }
}
