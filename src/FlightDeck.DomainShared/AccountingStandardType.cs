﻿namespace FlightDeck.DomainShared
{
    public enum AccountingStandardType : byte
    {
        IAS19 = 1,
        FRS17 = 2,
        USGAAP = 3,
        FRS102 = 4
    }
    public class AccountingStandardHelper
    {
        public bool TryParseAccountingStandardType(string value, out AccountingStandardType type)
        {
            var x = parseAccountingStandardType(value);

            type = x.GetValueOrDefault();

            return x.HasValue;
        }

        public AccountingStandardType ParseAccountingStandardType(string value)
        {
            return parseAccountingStandardType(value).GetValueOrDefault(AccountingStandardType.IAS19);
        }

        public AccountingStandardType? parseAccountingStandardType(string value)
        {
            if (string.Equals("IAS19", value, System.StringComparison.OrdinalIgnoreCase))
                return AccountingStandardType.IAS19;
            if (string.Equals("FRS102", value, System.StringComparison.OrdinalIgnoreCase) ||
                string.Equals("UKGAAP", value, System.StringComparison.OrdinalIgnoreCase) ||
                string.Equals("UK GAAP", value, System.StringComparison.OrdinalIgnoreCase))
                return AccountingStandardType.FRS102;
            if (string.Equals("USGAAP", value, System.StringComparison.OrdinalIgnoreCase) ||
                string.Equals("US GAAP", value, System.StringComparison.OrdinalIgnoreCase))
                return AccountingStandardType.USGAAP;

            return null;
        }
    }
}
