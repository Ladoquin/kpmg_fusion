﻿namespace FlightDeck.DomainShared
{
    using FlightDeck.DomainShared;
    using System.Collections.Generic;

    public class UserSearchResult
    {
        public IEnumerable<UserProfile> UserPageSet { get; set; }
        public int TotalMatches { get; set; }
    }
}
