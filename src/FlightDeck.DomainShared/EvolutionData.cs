﻿namespace FlightDeck.DomainShared
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// Combination of Domain.LiabilityEvolutionItem and Domain.AssetEvolutionItem, but with user friendly names and calcs.
    /// </summary>
    public class EvolutionData
    {
        public DateTime Date { get; private set; }
        //liabs
        public double Active { get; private set; }
        public double Deferred { get; private set; }
        public double Pensioner { get; private set; }
        public double Future { get; private set; }
        public double TotalLiability { get; private set; }
        public double DeferredBuyIn { get; private set; }
        public double PensionerBuyIn { get; private set; }
        public double PreRetirementDiscountRate { get; private set; }
        public double PostRetirementDiscountRate { get; private set; }
        public double PensionerDiscountRate { get; private set; }
        public double SalaryIncrease { get; private set; }
        public double RPI { get; private set; }
        public double CPI { get; private set; }
        public Dictionary<int, double> PensionIncreases { get; private set; }
        public double ActDuration { get; private set; }
        public double DefDuration { get; private set; }
        public double PenDuration { get; private set; }
        public double FutDuration { get; private set; }
        //assets
        public double EmployeeContributions { get; private set; }
        public double EmployerContributions { get; private set; }
        public double TotalBenefits { get; private set; }
        public double NetBenefits { get; private set; }
        public double NetIncome { get; private set; }
        public double Growth { get; private set; }
        public double TotalAssets { get; private set; }

        public double TotalExistingBuyin
        {
            get
            {
                return DeferredBuyIn + PensionerBuyIn;
            }
        }

        public double TotalAssetsPlusBuyin
        {
            get
            {
                return TotalAssets + TotalExistingBuyin;
            }
        }

        public EvolutionData(
            DateTime date,
            double act,
            double def,
            double pen,
            double fut,
            double liabs,
            double defBuyIn,
            double penBuyIn,
            double discPre,
            double discPost,
            double discPen,
            double salInc,
            double rpi,
            double cpi,
            double pinc1,
            double pinc2,
            double pinc3,
            double pinc4,
            double pinc5,
            double pinc6,
            double actDuration,
            double defDuration,
            double penDuration,
            double futDuration,
            double employeeContributions,
            double employerContributions,
            double totalBenefits,
            double netBenefits,
            double netIncome,
            double growth,
            double assetValue)
        {
            Date = date;
            Active = act;
            Deferred = def;
            Pensioner = pen;
            Future = fut;
            TotalLiability = liabs;
            DeferredBuyIn = defBuyIn;
            PensionerBuyIn = penBuyIn;
            PreRetirementDiscountRate = discPre;
            PostRetirementDiscountRate = discPost;
            PensionerDiscountRate = discPen;
            SalaryIncrease = salInc;
            RPI = rpi;
            CPI = cpi;
            PensionIncreases = new Dictionary<int, double>
                {
                    { 1, pinc1 },
                    { 2, pinc2 },
                    { 3, pinc3 },
                    { 4, pinc4 },
                    { 5, pinc5 }, 
                    { 6, pinc6 }
                };
            ActDuration = actDuration;
            DefDuration = defDuration;
            PenDuration = penDuration;
            FutDuration = futDuration;
            EmployeeContributions = employeeContributions;
            EmployerContributions = employerContributions;
            TotalBenefits = totalBenefits;
            NetBenefits = netBenefits;
            NetIncome = netIncome;
            Growth = growth;
            TotalAssets = assetValue;
        }
    }
}
