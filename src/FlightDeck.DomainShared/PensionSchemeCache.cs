﻿namespace FlightDeck.DomainShared
{
    public class PensionSchemeCache
    {
        public int PensionSchemeId { get; set; }
        public string LDIEvolutionItems { get; set; }
    }
}
