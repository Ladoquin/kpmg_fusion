﻿using System.ComponentModel.DataAnnotations;

namespace FlightDeck.DomainShared
{
    public enum NamedPropertyGroupType
    {
        BaselineInfo = 1,
        ScheduleOfConts,
        [Display(Name="Recovery Plan Defaults")]
        RecoveryPlan,
        [Display(Name="Ref Gilt Yield")]
        RefGiltYeild,
        [Display(Name="Market Data")]
        MarketData,
        [Display(Name="Asset Growth")]
        AssetGrowth
    }
}