﻿using System;

namespace FlightDeck.DomainShared
{
    public class USGAAPDisclosure : IEquatable<USGAAPDisclosure>
    {
        public DateTime Date { get; private set; }
        public DisclosureAssumptions Assumptions1 { get; private set; }
        public DisclosureAssumptions Assumptions2 { get; set; }
        public USGAAPBalanceSheetAmountData BalanceSheetAmounts { get; private set; }        
        public ChangesInPresentValueData ChangesInPresentValue { get; set; }        
        public ChangesInFairValueData ChangesInFairValue { get; set; }        
        public AccumulatedBenefitObligationData AccumulatedBenefitObligation { get; set; }        
        public EstimatedPaymentData EstimatedPayments { get; set; }        
        public NetPeriodicPensionCostData NetPeriodicPensionCost { get; set; }
        public ComprehensiveIncomeData ComprehensiveIncome { get; set; }        
        public ComprehensiveAccumulatedIncomeData ComprehensiveAccumulatedIncome { get; set; }        
        public NetPeriodicPensionCostForecastData NetPeriodPensionCostForecast { get; set; }

        public USGAAPDisclosure(DateTime date, DisclosureAssumptions assumptions1, DisclosureAssumptions assumptions2, USGAAPBalanceSheetAmountData balanceSheetAmounts, ChangesInPresentValueData changesInPresentValue, ChangesInFairValueData changesInFairValue, AccumulatedBenefitObligationData accumulatedBenefitObligation, EstimatedPaymentData estimatedPayments, NetPeriodicPensionCostData netPeriodicPensionCost, ComprehensiveIncomeData comprehensiveIncome, ComprehensiveAccumulatedIncomeData accumulatedComprehensiveIncome, NetPeriodicPensionCostForecastData netPeriodPensionCostForecast)
        {
            Date = date;
            Assumptions1 = assumptions1;
            Assumptions2 = assumptions2;
            BalanceSheetAmounts = balanceSheetAmounts;
            ChangesInPresentValue = changesInPresentValue;
            ChangesInFairValue = changesInFairValue;
            AccumulatedBenefitObligation = accumulatedBenefitObligation;
            EstimatedPayments = estimatedPayments;
            NetPeriodicPensionCost = netPeriodicPensionCost;
            ComprehensiveIncome = comprehensiveIncome;
            ComprehensiveAccumulatedIncome = accumulatedComprehensiveIncome;
            NetPeriodPensionCostForecast = netPeriodPensionCostForecast;
        }

        public bool Equals(USGAAPDisclosure other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date.Equals(other.Date) && Equals(Assumptions1, other.Assumptions1) && Equals(Assumptions2, other.Assumptions2) && Equals(BalanceSheetAmounts, other.BalanceSheetAmounts) && Equals(ChangesInPresentValue, other.ChangesInPresentValue) && Equals(ChangesInFairValue, other.ChangesInFairValue) && Equals(AccumulatedBenefitObligation, other.AccumulatedBenefitObligation) && Equals(EstimatedPayments, other.EstimatedPayments) && Equals(NetPeriodicPensionCost, other.NetPeriodicPensionCost) && Equals(ComprehensiveIncome, other.ComprehensiveIncome) && Equals(ComprehensiveAccumulatedIncome, other.ComprehensiveAccumulatedIncome) && Equals(NetPeriodPensionCostForecast, other.NetPeriodPensionCostForecast);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((USGAAPDisclosure) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode*397) ^ (Assumptions1 != null ? Assumptions1.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Assumptions2 != null ? Assumptions2.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (BalanceSheetAmounts != null ? BalanceSheetAmounts.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ChangesInPresentValue != null ? ChangesInPresentValue.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ChangesInFairValue != null ? ChangesInFairValue.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (AccumulatedBenefitObligation != null ? AccumulatedBenefitObligation.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (EstimatedPayments != null ? EstimatedPayments.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (NetPeriodicPensionCost != null ? NetPeriodicPensionCost.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ComprehensiveIncome != null ? ComprehensiveIncome.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ComprehensiveAccumulatedIncome != null ? ComprehensiveAccumulatedIncome.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (NetPeriodPensionCostForecast != null ? NetPeriodPensionCostForecast.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Date: {0}, Assumptions1: {1}, Assumptions2: {2}, BalanceSheetAmounts: {3}, ChangesInPresentValue: {4}, ChangesInFairValue: {5}, AccumulatedBenefitObligation: {6}, EstimatedPayments: {7}, NetPeriodicPensionCost: {8}, ComprehensiveIncome: {9}, ComprehensiveAccumulatedIncome: {10}, NetPeriodPensionCostForecast: {11}", Date, Assumptions1, Assumptions2, BalanceSheetAmounts, ChangesInPresentValue, ChangesInFairValue, AccumulatedBenefitObligation, EstimatedPayments, NetPeriodicPensionCost, ComprehensiveIncome, ComprehensiveAccumulatedIncome, NetPeriodPensionCostForecast);
        }

        #region Data classes

        /// <summary>
        /// The amounts recognised in the balance sheet are as follows:
        /// </summary>
        public class USGAAPBalanceSheetAmountData : IEquatable<USGAAPBalanceSheetAmountData>
        {
            public USGAAPBalanceSheetAmountData()
            {
            }
            public USGAAPBalanceSheetAmountData(double projectedBenefitObligation, double fairValueOfSchemeAssets, double fundedStatus)
            {
                FundedStatus = fundedStatus;
                FairValueOfSchemeAssets = fairValueOfSchemeAssets;
                ProjectedBenefitObligation = projectedBenefitObligation;
            }

            public double ProjectedBenefitObligation { get; private set; }
            public double FairValueOfSchemeAssets { get; private set; }
            public double FundedStatus { get; private set; }

            public bool Equals(USGAAPBalanceSheetAmountData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(ProjectedBenefitObligation, other.ProjectedBenefitObligation)
                    && Utils.EqualityCheck(FairValueOfSchemeAssets, other.FairValueOfSchemeAssets)
                    && Utils.EqualityCheck(FundedStatus, other.FundedStatus);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((USGAAPBalanceSheetAmountData) obj);
            }
           
            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = ProjectedBenefitObligation.GetHashCode();
                    hashCode = (hashCode*397) ^ FairValueOfSchemeAssets.GetHashCode();
                    hashCode = (hashCode*397) ^ FundedStatus.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("ProjectedBenefitObligation: {0}, FairValueOfSchemeAssets: {1}, FundedStatus: {2}", ProjectedBenefitObligation, FairValueOfSchemeAssets, FundedStatus);
            }
        }

        /// <summary>
        /// Changes in the present value of the projected benefit obligation are as follows:
        /// </summary>
        public class ChangesInPresentValueData : IEquatable<ChangesInPresentValueData>
        {
            public double OpeningProjectedObligation { get; private set; }
            public double ServiceCost { get; private set; }
            public double InterestCost { get; private set; }
            public double ActuarialLosses { get; private set; }
            public double SchemeAmendments { get; private set; }
            public double Contributions { get; private set; }
            public double BenefitsPaid { get; private set; }
            public double ClosingProjectedObligation { get; private set; }

            public ChangesInPresentValueData()
            {
            }
            public ChangesInPresentValueData(double openingProjectedObligation, double serviceCost, double interestCost, double actuarialLosses, double schemeAmendments, double contributions, double benefitsPaid, double closingProjectedObligation)
            {
                ActuarialLosses = actuarialLosses;
                BenefitsPaid = benefitsPaid;
                ClosingProjectedObligation = closingProjectedObligation;
                Contributions = contributions;
                InterestCost = interestCost;
                OpeningProjectedObligation = openingProjectedObligation;
                SchemeAmendments = schemeAmendments;
                ServiceCost = serviceCost;
            }

            public bool Equals(ChangesInPresentValueData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(OpeningProjectedObligation, other.OpeningProjectedObligation)
                    && Utils.EqualityCheck(ServiceCost, other.ServiceCost)
                    && Utils.EqualityCheck(InterestCost, other.InterestCost)
                    && Utils.EqualityCheck(ActuarialLosses, other.ActuarialLosses)
                    && Utils.EqualityCheck(SchemeAmendments, other.SchemeAmendments)
                    && Utils.EqualityCheck(Contributions, other.Contributions)
                    && Utils.EqualityCheck(BenefitsPaid, other.BenefitsPaid)
                    && Utils.EqualityCheck(ClosingProjectedObligation, other.ClosingProjectedObligation);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((ChangesInPresentValueData) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = OpeningProjectedObligation.GetHashCode();
                    hashCode = (hashCode*397) ^ ServiceCost.GetHashCode();
                    hashCode = (hashCode*397) ^ InterestCost.GetHashCode();
                    hashCode = (hashCode*397) ^ ActuarialLosses.GetHashCode();
                    hashCode = (hashCode*397) ^ SchemeAmendments.GetHashCode();
                    hashCode = (hashCode*397) ^ Contributions.GetHashCode();
                    hashCode = (hashCode*397) ^ BenefitsPaid.GetHashCode();
                    hashCode = (hashCode*397) ^ ClosingProjectedObligation.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("OpeningProjectedObligation: {0}, ServiceCost: {1}, InterestCost: {2}, ActuarialLosses: {3}, SchemeAmendments: {4}, Contributions: {5}, BenefitsPaid: {6}, ClosingProjectedObligation: {7}", OpeningProjectedObligation, ServiceCost, InterestCost, ActuarialLosses, SchemeAmendments, Contributions, BenefitsPaid, ClosingProjectedObligation);
            }
        }

        /// <summary>
        /// Changes in the fair value of scheme assets are as follows:
        /// </summary>
        public class ChangesInFairValueData : IEquatable<ChangesInFairValueData>
        {
            public double OpeningFairValue { get; private set; }
            public double ExpectedReturn { get; private set; }
            public double ActuarialLosses { get; private set; }
            public double EmployerContributions { get; private set; }
            public double MemberContributions { get; private set; }
            public double BenefitsPaid { get; private set; }
            public double ClosingPairValue { get; private set; }

            public ChangesInFairValueData()
            {
            }
            public ChangesInFairValueData(double openingFairValue, double expectedReturn, double actuarialLosses, double employerContributions, double memberContributions, double benefitsPaid, double closingPairValue)
            {
                OpeningFairValue = openingFairValue;
                ExpectedReturn = expectedReturn;
                ActuarialLosses = actuarialLosses;
                EmployerContributions = employerContributions;
                MemberContributions = memberContributions;
                BenefitsPaid = benefitsPaid;
                ClosingPairValue = closingPairValue;
            }

            public bool Equals(ChangesInFairValueData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(OpeningFairValue, other.OpeningFairValue) 
                    && Utils.EqualityCheck(ExpectedReturn, other.ExpectedReturn) 
                    && Utils.EqualityCheck(ActuarialLosses, other.ActuarialLosses) 
                    && Utils.EqualityCheck(EmployerContributions, other.EmployerContributions) 
                    && Utils.EqualityCheck(MemberContributions, other.MemberContributions) 
                    && Utils.EqualityCheck(BenefitsPaid, other.BenefitsPaid) 
                    && Utils.EqualityCheck(ClosingPairValue, other.ClosingPairValue);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((ChangesInFairValueData) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = OpeningFairValue.GetHashCode();
                    hashCode = (hashCode*397) ^ ExpectedReturn.GetHashCode();
                    hashCode = (hashCode*397) ^ ActuarialLosses.GetHashCode();
                    hashCode = (hashCode*397) ^ EmployerContributions.GetHashCode();
                    hashCode = (hashCode*397) ^ MemberContributions.GetHashCode();
                    hashCode = (hashCode*397) ^ BenefitsPaid.GetHashCode();
                    hashCode = (hashCode*397) ^ ClosingPairValue.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("OpeningFairValue: {0}, ExpectedReturn: {1}, ActuarialLosses: {2}, EmployerContributions: {3}, MemberContributions: {4}, BenefitsPaid: {5}, ClosingPairValue: {6}", OpeningFairValue, ExpectedReturn, ActuarialLosses, EmployerContributions, MemberContributions, BenefitsPaid, ClosingPairValue);
            }
        }

        /// <summary>
        /// Accumulated benefit obligation
        /// </summary>
        public class AccumulatedBenefitObligationData : IEquatable<AccumulatedBenefitObligationData>
        {
            public double Value { get; private set; }

            public AccumulatedBenefitObligationData()
            {
            }
            public AccumulatedBenefitObligationData(double value)
            {
                Value = value;
            }

            public bool Equals(AccumulatedBenefitObligationData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(Value, other.Value);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((AccumulatedBenefitObligationData) obj);
            }

            public override int GetHashCode()
            {
                return Value.GetHashCode();
            }

            public override string ToString()
            {
                return string.Format("Value: {0}", Value);
            }
        }

        /// <summary>
        /// Estimated future benefit payments:
        /// </summary>
        public class EstimatedPaymentData : IEquatable<EstimatedPaymentData>
        {
            public double Year1 { get; private set; }
            public double Year2 { get; private set; }
            public double Year3 { get; private set; }
            public double Year4 { get; private set; }
            public double Year5 { get; private set; }
            public double Year6to10 { get; private set; }

            public EstimatedPaymentData()
            {
            }
            public EstimatedPaymentData(double year1, double year2, double year3, double year4, double year5, double year6To10)
            {
                Year1 = year1;
                Year2 = year2;
                Year3 = year3;
                Year4 = year4;
                Year5 = year5;
                Year6to10 = year6To10;
            }

            public bool Equals(EstimatedPaymentData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(Year1, other.Year1) 
                    && Utils.EqualityCheck(Year2, other.Year2)
                    && Utils.EqualityCheck(Year3, other.Year3) 
                    && Utils.EqualityCheck(Year4, other.Year4) 
                    && Utils.EqualityCheck(Year5, other.Year5)
                    && Utils.EqualityCheck(Year6to10, other.Year6to10);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((EstimatedPaymentData) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = Year1.GetHashCode();
                    hashCode = (hashCode*397) ^ Year2.GetHashCode();
                    hashCode = (hashCode*397) ^ Year3.GetHashCode();
                    hashCode = (hashCode*397) ^ Year4.GetHashCode();
                    hashCode = (hashCode*397) ^ Year5.GetHashCode();
                    hashCode = (hashCode*397) ^ Year6to10.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("Year1: {0}, Year2: {1}, Year3: {2}, Year4: {3}, Year5: {4}, Year6to10: {5}", Year1, Year2, Year3, Year4, Year5, Year6to10);
            }
        }

        /// <summary>
        /// Net periodic pension cost for period ending:
        /// </summary>
        public class NetPeriodicPensionCostData : IEquatable<NetPeriodicPensionCostData>
        {
            public double ServiceCost { get; private set; }
            public double InterestCost { get; private set; }
            public double ExpectedReturn { get; private set; }
            public double AmortisationOfPriorService { get; private set; }
            public double AmortisationOfActuarialLosses { get; private set; }
            public double TotalPeriodicPensionCost { get; private set; }

            public NetPeriodicPensionCostData()
            {
            }
            public NetPeriodicPensionCostData(double serviceCost, double interestCost, double expectedReturn, double amortisationOfPriorService, double amortisationOfActuarialLosses, double totalPeriodicPensionCost)
            {
                ServiceCost = serviceCost;
                InterestCost = interestCost;
                ExpectedReturn = expectedReturn;
                AmortisationOfPriorService = amortisationOfPriorService;
                AmortisationOfActuarialLosses = amortisationOfActuarialLosses;
                TotalPeriodicPensionCost = totalPeriodicPensionCost;
            }

            public bool Equals(NetPeriodicPensionCostData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(ServiceCost, other.ServiceCost) 
                    && Utils.EqualityCheck(InterestCost, other.InterestCost) 
                    && Utils.EqualityCheck(ExpectedReturn, other.ExpectedReturn) 
                    && Utils.EqualityCheck(AmortisationOfPriorService, other.AmortisationOfPriorService)
                    && Utils.EqualityCheck(AmortisationOfActuarialLosses, other.AmortisationOfActuarialLosses)
                    && Utils.EqualityCheck(TotalPeriodicPensionCost, other.TotalPeriodicPensionCost);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((NetPeriodicPensionCostData) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = ServiceCost.GetHashCode();
                    hashCode = (hashCode*397) ^ InterestCost.GetHashCode();
                    hashCode = (hashCode*397) ^ ExpectedReturn.GetHashCode();
                    hashCode = (hashCode*397) ^ AmortisationOfPriorService.GetHashCode();
                    hashCode = (hashCode*397) ^ AmortisationOfActuarialLosses.GetHashCode();
                    hashCode = (hashCode*397) ^ TotalPeriodicPensionCost.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("ServiceCost: {0}, InterestCost: {1}, ExpectedReturn: {2}, AmortisationOfPriorService: {3}, AmortisationOfActuarialLosses: {4}, TotalPeriodicPensionCost: {5}", ServiceCost, InterestCost, ExpectedReturn, AmortisationOfPriorService, AmortisationOfActuarialLosses, TotalPeriodicPensionCost);
            }
        }

        /// <summary>
        /// Amount included in other comprehensive income in the period ending:
        /// </summary>
        public class ComprehensiveIncomeData : IEquatable<ComprehensiveIncomeData>
        {
            public double ActurialLoss { get; private set; }
            public double PriorServiceCost { get; private set; }
            public double ActurialGain { get; private set; }
            public double PriorServiceCredit { get; private set; }
            public double TotalAmount { get; private set; }

            public ComprehensiveIncomeData()
            {
            }
            public ComprehensiveIncomeData(double acturialLoss, double priorServiceCost, double acturialGain, double priorServiceCredit, double totalAmount)
            {
                ActurialLoss = acturialLoss;
                PriorServiceCost = priorServiceCost;
                ActurialGain = acturialGain;
                PriorServiceCredit = priorServiceCredit;
                TotalAmount = totalAmount;
            }

            public bool Equals(ComprehensiveIncomeData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(ActurialLoss, other.ActurialLoss)
                    && Utils.EqualityCheck(PriorServiceCost, other.PriorServiceCost)
                    && Utils.EqualityCheck(ActurialGain, other.ActurialGain) 
                    && Utils.EqualityCheck(PriorServiceCredit, other.PriorServiceCredit)
                    && Utils.EqualityCheck(TotalAmount, other.TotalAmount);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((ComprehensiveIncomeData) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = ActurialLoss.GetHashCode();
                    hashCode = (hashCode*397) ^ PriorServiceCost.GetHashCode();
                    hashCode = (hashCode*397) ^ ActurialGain.GetHashCode();
                    hashCode = (hashCode*397) ^ PriorServiceCredit.GetHashCode();
                    hashCode = (hashCode*397) ^ TotalAmount.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("ActurialLoss: {0}, PriorServiceCost: {1}, ActurialGain: {2}, PriorServiceCredit: {3}, TotalAmount: {4}", ActurialLoss, PriorServiceCost, ActurialGain, PriorServiceCredit, TotalAmount);
            }
        }

        /// <summary>
        /// Amount recognised in accumulated other comprehensive income at:
        /// </summary>
        public class ComprehensiveAccumulatedIncomeData : IEquatable<ComprehensiveAccumulatedIncomeData>
        {
            public double? OpeningAOCI { get; private set; }
            public double? ActuarialLoss { get; private set; }
            public double? PriorServiceCost { get; private set; }
            public double Total { get; private set; }

            public ComprehensiveAccumulatedIncomeData()
            {
            }
            public ComprehensiveAccumulatedIncomeData(double? openingAOCI, double? actuarialLoss, double? priorServiceCost, double total)
            {
                OpeningAOCI = openingAOCI;
                ActuarialLoss = actuarialLoss;
                PriorServiceCost = priorServiceCost;
                Total = total;
            }

            public bool Equals(ComprehensiveAccumulatedIncomeData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(OpeningAOCI.GetValueOrDefault(), other.OpeningAOCI.GetValueOrDefault()) 
                    && Utils.EqualityCheck(ActuarialLoss.GetValueOrDefault(), other.ActuarialLoss.GetValueOrDefault()) 
                    && Utils.EqualityCheck(PriorServiceCost.GetValueOrDefault(), other.PriorServiceCost.GetValueOrDefault()) 
                    && Utils.EqualityCheck(Total, other.Total);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((ComprehensiveAccumulatedIncomeData) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = OpeningAOCI.GetHashCode();
                    hashCode = (hashCode*397) ^ ActuarialLoss.GetHashCode();
                    hashCode = (hashCode*397) ^ PriorServiceCost.GetHashCode();
                    hashCode = (hashCode*397) ^ Total.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("OpeningAOCI: {0}, ActuarialLoss: {1}, PriorServiceCost: {2}, Total: {3}", OpeningAOCI, ActuarialLoss, PriorServiceCost, Total);
            }
        }

        /// <summary>
        /// Net periodic pension cost forecast 
        /// </summary>
        public class NetPeriodicPensionCostForecastData : IEquatable<NetPeriodicPensionCostForecastData>
        {
            public double ServiceCost { get; private set; }
            public double InterestCost { get; private set; }
            public double ExpectedReturn { get; private set; }
            public double AmortisationOfPriorService { get; private set; }
            public double AmortisationOfActuarial { get; private set; }
            public double TotalNetPeriodicPension { get; private set; }

            public NetPeriodicPensionCostForecastData()
            {
            }
            public NetPeriodicPensionCostForecastData(double serviceCost, double interestCost, double expectedReturn, double amortisationOfPriorService, double amortisationOfActuarial, double totalNetPeriodicPension)
            {
                ServiceCost = serviceCost;
                InterestCost = interestCost;
                ExpectedReturn = expectedReturn;
                AmortisationOfPriorService = amortisationOfPriorService;
                AmortisationOfActuarial = amortisationOfActuarial;
                TotalNetPeriodicPension = totalNetPeriodicPension;
            }

            public bool Equals(NetPeriodicPensionCostForecastData other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Utils.EqualityCheck(ServiceCost, other.ServiceCost) 
                    && Utils.EqualityCheck(InterestCost, other.InterestCost) 
                    && Utils.EqualityCheck(ExpectedReturn, other.ExpectedReturn)
                    && Utils.EqualityCheck(AmortisationOfPriorService, other.AmortisationOfPriorService)
                    && Utils.EqualityCheck(AmortisationOfActuarial, other.AmortisationOfActuarial)
                    && Utils.EqualityCheck(TotalNetPeriodicPension, other.TotalNetPeriodicPension);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
                return Equals((NetPeriodicPensionCostForecastData) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = ServiceCost.GetHashCode();
                    hashCode = (hashCode*397) ^ InterestCost.GetHashCode();
                    hashCode = (hashCode*397) ^ ExpectedReturn.GetHashCode();
                    hashCode = (hashCode*397) ^ AmortisationOfPriorService.GetHashCode();
                    hashCode = (hashCode*397) ^ AmortisationOfActuarial.GetHashCode();
                    hashCode = (hashCode*397) ^ TotalNetPeriodicPension.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("ServiceCost: {0}, InterestCost: {1}, ExpectedReturn: {2}, AmortisationOfPriorService: {3}, AmortisationOfActuarial: {4}, TotalNetPeriodicPension: {5}", ServiceCost, InterestCost, ExpectedReturn, AmortisationOfPriorService, AmortisationOfActuarial, TotalNetPeriodicPension);
            }
        }

        #endregion
    }
}
