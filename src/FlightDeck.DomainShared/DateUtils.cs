﻿namespace FlightDeck.DomainShared
{
    using System;
    
    public static class DateUtils
    {
        public static DateTime SubtractToDate(this DateTime d, DateTime d2)
        {
            var date = d.Subtract(d.Subtract(d2));

            return d;
        }
    }
}
