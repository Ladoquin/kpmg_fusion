﻿namespace FlightDeck.DomainShared
{
    public class AnalysisParameters
    {
        public double FROMinTransferValue { get; private set; }
        public double FROMaxTransferValue { get; private set; }
        public double ETVMinTransferValue { get; private set; }
        public double ETVMaxTransferValue { get; private set; }

        public AnalysisParameters(double froMin, double froMax, double etvMin, double etvMax)
        {
            FROMinTransferValue = froMin;
            FROMaxTransferValue = froMax;
            ETVMinTransferValue = etvMin;
            ETVMaxTransferValue = etvMax;
        }
    }
}
