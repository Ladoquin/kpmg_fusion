﻿using System.Collections.Generic;
namespace FlightDeck.DomainShared
{
    public class MarketInfo
    {
        public MarketYields Yields { get; private set; }
        public AssetReturns Returns { get; private set; }
        public List<MarketDataIndex> MarketDataIndices { get; private set; }

        public MarketInfo(MarketYields yields, AssetReturns returns, List<MarketDataIndex> marketDataIndices)
        {
            Returns = returns;
            Yields = yields;
            MarketDataIndices = marketDataIndices;
        }

        public MarketInfo()
        {
            Returns = new AssetReturns();
            Yields = new MarketYields();
        }

        public class AssetReturns
        {
            public double UkEquity { get; private set; }
            public double GlobalEquity { get; private set; }
            public double Property { get; private set; }
            public double Dgfs { get; private set; }
            public double Gilts { get; private set; }
            public double IndexLinkedGilts { get; private set; }
            public double CorporateBonds { get; private set; }

            public AssetReturns(double ukEquity, double globalEquity, double property,
                double dgfs, double gilts, double indexLinkedGilts, double corporateBonds)
            {
                CorporateBonds = corporateBonds;
                IndexLinkedGilts = indexLinkedGilts;
                Gilts = gilts;
                Dgfs = dgfs;
                Property = property;
                GlobalEquity = globalEquity;
                UkEquity = ukEquity;
            }

            public AssetReturns()
            {
                CorporateBonds = 0;
                IndexLinkedGilts = 0;
                Gilts = 0;
                Dgfs = 0;
                Property = 0;
                GlobalEquity = 0;
                UkEquity = 0;
            }
        }

        public class MarketYields
        {
            public BeforeAfter<double> Inflation { get; private set; }
            public BeforeAfter<double> Gilts { get; private set; }
            public BeforeAfter<double> IndexLinkedGilts { get; private set; }
            public BeforeAfter<double> CorporateBonds { get; private set; }

            public MarketYields(BeforeAfter<double> inflation, BeforeAfter<double> gilts,
                BeforeAfter<double> indexLinkedGilts, BeforeAfter<double> corporateBonds)
            {
                CorporateBonds = corporateBonds;
                IndexLinkedGilts = indexLinkedGilts;
                Gilts = gilts;
                Inflation = inflation;
            }

            public MarketYields()
            {
                CorporateBonds = IndexLinkedGilts = Gilts
                    = Inflation = new BeforeAfter<double>();
            }
        }

        public class MarketDataIndex
        {
            public string Label { get; set; }
            public string IndexName { get; set; }
            public BeforeAfter<double> IndexValue { get; set; }
        }
    }
}
