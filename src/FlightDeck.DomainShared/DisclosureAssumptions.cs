﻿namespace FlightDeck.DomainShared
{
    using System;

    public class DisclosureAssumptions : IEquatable<DisclosureAssumptions>
    {
        public double DiscountRate { get; set; }
        public double FutureSalaryGrowth { get; set; }
        public double RPIInflation { get; set; }
        public double CpiInflation { get; set; }
        public double MaleLifeExpectancy65 { get; set; }
        public double MaleLifeExpectancy65_45 { get; set; }
        public double? ExpectedRateOfReturnOnAssets { get; set; }

        public DisclosureAssumptions()
        {
        }
        public DisclosureAssumptions(double discountRate, double futureSalaryGrowth,
            double rpiInflation, double cpiInflation,
            double maleLifeExpectancy65, double maleLifeExpectancy6545)
            : this(discountRate, null, futureSalaryGrowth, rpiInflation, cpiInflation, maleLifeExpectancy65, maleLifeExpectancy6545)
        {
        }
        public DisclosureAssumptions(double discountRate, double? expectedRateOfReturnOnAssets, double futureSalaryGrowth,
            double rpiInflation, double cpiInflation,
            double maleLifeExpectancy65, double maleLifeExpectancy6545)
        {
            MaleLifeExpectancy65_45 = maleLifeExpectancy6545;
            MaleLifeExpectancy65 = maleLifeExpectancy65;
            CpiInflation = cpiInflation;
            RPIInflation = rpiInflation;
            FutureSalaryGrowth = futureSalaryGrowth;
            DiscountRate = discountRate;
            ExpectedRateOfReturnOnAssets = expectedRateOfReturnOnAssets;
        }

        public bool Equals(DisclosureAssumptions other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(DiscountRate, other.DiscountRate)
                && Utils.EqualityCheck(FutureSalaryGrowth, other.FutureSalaryGrowth)
                && Utils.EqualityCheck(RPIInflation, other.RPIInflation)
                && Utils.EqualityCheck(CpiInflation, other.CpiInflation)
                && Utils.EqualityCheck(MaleLifeExpectancy65, other.MaleLifeExpectancy65)
                && Utils.EqualityCheck(MaleLifeExpectancy65_45, other.MaleLifeExpectancy65_45)
                && Math.Round(ExpectedRateOfReturnOnAssets.GetValueOrDefault(), 6) == Math.Round(other.ExpectedRateOfReturnOnAssets.GetValueOrDefault(), 6);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((DisclosureAssumptions)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = DiscountRate.GetHashCode();
                hashCode = (hashCode * 397) ^ FutureSalaryGrowth.GetHashCode();
                hashCode = (hashCode * 397) ^ RPIInflation.GetHashCode();
                hashCode = (hashCode * 397) ^ CpiInflation.GetHashCode();
                hashCode = (hashCode * 397) ^ MaleLifeExpectancy65.GetHashCode();
                hashCode = (hashCode * 397) ^ MaleLifeExpectancy65_45.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("DiscountRate: {0}, FutureSalaryGrowth: {1}, RPIInflation: {2}, CpiInflation: {3}, MaleLifeExpectancy65: {4}, MaleLifeExpectancy65_45: {5}, ExpectedRateOfReturnOnAssets: {6}", DiscountRate, FutureSalaryGrowth, RPIInflation, CpiInflation, MaleLifeExpectancy65, MaleLifeExpectancy65_45, ExpectedRateOfReturnOnAssets);
        }
    }
}
