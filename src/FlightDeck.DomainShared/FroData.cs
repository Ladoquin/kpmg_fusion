﻿using System;

namespace FlightDeck.DomainShared
{
    public class FroData : IEquatable<FroData>
    {
        public double SizeOfTV { get; private set; }
        public double CetvPaid { get; private set; }
        public double LiabilityDischarged { get; private set; }
        public double Improvement { get { return LiabilityDischarged - CetvPaid; } }
        public double DefferedLiability { get; private set; }
        public double Over55 { get; private set; }
        public double Under55 { get { return DefferedLiability - Over55; } }

        public FroData(double sizeOfTV, double cetvPaid, double liabilityDischarged,
             double defferedLiability, double over55)
        {
            CetvPaid = cetvPaid;
            SizeOfTV = sizeOfTV;
            LiabilityDischarged = liabilityDischarged;
            DefferedLiability = defferedLiability;
            Over55 = over55;
        }

        public bool Equals(FroData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(SizeOfTV, other.SizeOfTV) 
                && Utils.EqualityCheck(CetvPaid, other.CetvPaid)
                && Utils.EqualityCheck(LiabilityDischarged, other.LiabilityDischarged)
                && Utils.EqualityCheck(DefferedLiability, other.DefferedLiability)
                && Utils.EqualityCheck(Over55, other.Over55);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((FroData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = SizeOfTV.GetHashCode();
                hashCode = (hashCode*397) ^ CetvPaid.GetHashCode();
                hashCode = (hashCode*397) ^ LiabilityDischarged.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("SizeOfTV: {0}, CetvPaid: {1}, LiabilityDischarged: {2}, DefferedLiability: {3}, Over55: {4}", 
                SizeOfTV, CetvPaid, LiabilityDischarged, DefferedLiability, Over55);
        }
    }
}