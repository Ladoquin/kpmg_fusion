﻿namespace FlightDeck.DomainShared
{
    public struct BasisIdentifier
    {
        public BasisType Type { get; private set; }
        public string Name { get; private set; }
        public int Id { get; private set; }

        public BasisIdentifier(int id, BasisType type, string name)
            : this()
        {
            Id = id;
            Type = type;
            Name = name;
        }
    }
}
