﻿namespace FlightDeck.DomainShared
{
    public static class EvolutionDataExtensions
    {
        public static AssumptionData ToAssumptionData(this EvolutionData ev, double pensionerLifeExpectancy, double deferredLifeExpectancy)
        {
            return new AssumptionData(
                ev.PreRetirementDiscountRate,
                ev.PostRetirementDiscountRate,
                ev.PensionerDiscountRate,
                ev.SalaryIncrease,
                ev.RPI,
                ev.CPI,
                pensionerLifeExpectancy,
                deferredLifeExpectancy);
        }
    }
}
