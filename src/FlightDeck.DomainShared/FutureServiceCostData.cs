﻿using System;

namespace FlightDeck.DomainShared
{
    public class FutureServiceCostData : IEquatable<FutureServiceCostData>
    {
        public double AnnualCost { get; private set; }
        public double ContributionRate { get; private set; }
        public double EmployeeRate { get; private set; }
        public double EmployerRate { get; private set; }
        public double ServiceCost { get; private set; }

        public FutureServiceCostData(double annualCost, double contributionRate, double employeeRate,
            double employerRate, double serviceCost)
        {
            ServiceCost = serviceCost;
            EmployerRate = employerRate;
            EmployeeRate = employeeRate;
            ContributionRate = contributionRate;
            AnnualCost = annualCost;
        }

        public bool Equals(FutureServiceCostData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(AnnualCost, other.AnnualCost)
                && Utils.EqualityCheck(ContributionRate, other.ContributionRate) 
                && Utils.EqualityCheck(EmployeeRate, other.EmployeeRate)
                && Utils.EqualityCheck(EmployerRate, other.EmployerRate)
                && Utils.EqualityCheck(ServiceCost, other.ServiceCost);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((FutureServiceCostData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = AnnualCost.GetHashCode();
                hashCode = (hashCode*397) ^ ContributionRate.GetHashCode();
                hashCode = (hashCode*397) ^ EmployeeRate.GetHashCode();
                hashCode = (hashCode*397) ^ EmployerRate.GetHashCode();
                hashCode = (hashCode*397) ^ ServiceCost.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("AnnualCost: {0}, ContributionRate: {1}, EmployeeRate: {2}, EmployerRate: {3}, ServiceCost: {4}", AnnualCost, ContributionRate, EmployeeRate, EmployerRate, ServiceCost);
        }
    }
}