﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightDeck.DomainShared
{
    public class BondYieldData
    {
        public string IndexName { get; set; }
        public string Label { get; set; }
        public double Value { get; set; }
    }
}
