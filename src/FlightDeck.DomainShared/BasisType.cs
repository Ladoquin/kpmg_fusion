﻿using System.ComponentModel.DataAnnotations;

namespace FlightDeck.DomainShared
{
    public enum BasisType
    {
        [Display(Name = "Accounting")]
        Accounting = 1,
        [Display(Name = "Buyout")]
        Buyout,
        [Display(Name = "Funding")]
        TechnicalProvision, 
        [Display(Name = "Gilts")]
        Gilts,
        [Display(Name = "Neutral")]
        Neutral,
        [Display(Name = "Self Sufficiency")]
        SelfSufficiency
    }
}