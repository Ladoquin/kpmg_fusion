﻿namespace FlightDeck.DomainShared
{
    public class WeightedReturnParameters
    {
        public double AbsoluteAssetReturn { get; private set; }
        public double WeightedAssetReturn { get; private set; }
        public double WeightedDiscountRate { get; private set; }
        public double WdrRelativeToGilts { get; private set; }
        public double WdrAssetMargin { get { return AbsoluteAssetReturn - WeightedDiscountRate; }}
        public double CurrentWarRelativeToGilts { get; private set; }
        public double ChangeInExpectedReturn { get { return WeightedAssetReturn - CurrentWarRelativeToGilts; } }
        public double MaxReturn { get; private set; }

        public WeightedReturnParameters(double absoluteAssetReturn, double weightedAssetReturn, double weightedDiscountRate, double wdrRelativeToGilts, double currentWarRelativeToGilts, double maxReturn)
        {
            CurrentWarRelativeToGilts = currentWarRelativeToGilts;
            WdrRelativeToGilts = wdrRelativeToGilts;
            WeightedDiscountRate = weightedDiscountRate;
            WeightedAssetReturn = weightedAssetReturn;
            AbsoluteAssetReturn = absoluteAssetReturn;
            MaxReturn = maxReturn;
        }
    }
}