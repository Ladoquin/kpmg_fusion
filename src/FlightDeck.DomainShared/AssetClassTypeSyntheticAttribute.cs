﻿namespace FlightDeck.DomainShared
{
    using System;
    
    [AttributeUsage(AttributeTargets.Field)]
    public class AssetClassTypeSyntheticAttribute : Attribute
    {
        public bool IsSynthetic { get; set; }

        public AssetClassTypeSyntheticAttribute(bool isSynthetic)
        {
            IsSynthetic = isSynthetic;
        }
    }
}
