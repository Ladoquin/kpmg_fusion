﻿using System.Collections.Generic;

namespace FlightDeck.DomainShared
{
    public class NewJPData
    {
        public IDictionary<int, double> Liab1 { get; private set; }
        public IDictionary<int, double> Liab2 { get; private set; }
        public IDictionary<int, double> Liab3 { get; private set; }
        public IDictionary<int, double> Assets1 { get; private set; }
        public IDictionary<int, double> Assets2 { get; private set; }
        public IDictionary<BasisType, BeforeAfter<double>> DefaultPremiums { get; private set; }
        public IDictionary<BasisType, int> CurrentAssetsCrossOverPoints { get; private set; }
        public IDictionary<BasisType, int> NewAssetsCrossOverPoints { get; private set; }
        public IEnumerable<int> TriggerYears { get; private set; }

        public NewJPData(
            IDictionary<int, double> liab1,
            IDictionary<int, double> liab2,
            IDictionary<int, double> liab3,
            IDictionary<int, double> assets1,
            IDictionary<int, double> assets2,
            IDictionary<BasisType, BeforeAfter<double>> defaultPremiums,
            IDictionary<BasisType, int> currentAssetsCrossOverPoints,
            IDictionary<BasisType, int> newAssetsCrossOverPoints,
            IEnumerable<int> triggerYears)
        {
            Liab1 = liab1;
            Liab2 = liab2;
            Liab3 = liab3;
            Assets1 = assets1;
            Assets2 = assets2;
            DefaultPremiums = defaultPremiums;
            CurrentAssetsCrossOverPoints = currentAssetsCrossOverPoints;
            NewAssetsCrossOverPoints = newAssetsCrossOverPoints;
            TriggerYears = triggerYears;
        }
    }
}
