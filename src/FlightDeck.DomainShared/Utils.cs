﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Reflection;

namespace FlightDeck.DomainShared
{
    public static class Utils
    {
        public enum TestPrecisionType
        {
            Exact, 
            WithinPrecision, 
            ToPence,
            ToPounds,
            ToWithinAPound, // ie, difference is less than a pound out, so 1.90 == 2.10 and 1.20 == 1.80
            RelativePrecision,
            ToSignificantFigures
        }

        private static TestPrecisionType TestPrecisionMode
        {
            get
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestPrecisionMode"]))
                    return (TestPrecisionType)Enum.Parse(typeof(TestPrecisionType), ConfigurationManager.AppSettings["TestPrecisionMode"]);
                return TestPrecisionType.RelativePrecision;
            }
        }
        
        private static double TestAccuracy = 0.985;
        private static int TestSignificantFigures = 4;

        public const double TempDouble = 0.5; //TODO: get rid of this ASAP
        public const double Precision = 0.00001;
        public const double TestPrecision = 0.01;
        public const int MaxYear = 100;
        public const int MaxRecoveryPlanYear = 50;
        public const int MaxJourneyPlanYear = 40;
        public const int MaxPlanTermInYears = 41;
        public const int MinPlanTermInYears = 0;
        public const double AbfRecoveryPlanLength = 10;
        public const double AbfLength = 20;
        public const double CorporationTax = 0.20;
        public const double DaysInAYear = 365.25;

        public static double ToSignificantFigures(double d, int digits)
        {
            double scale = Math.Pow(10, Math.Floor(Math.Log10(Math.Abs(d))) + 1);
            return scale * Math.Round(d / scale, digits);
        }

        public static bool EqualityCheck(double a, double b, TestPrecisionType? precision = null)
        {
            if (a == b)
                return true;

            var testPrecision = precision.GetValueOrDefault(TestPrecisionMode);

            switch (testPrecision)
            {
                case TestPrecisionType.Exact:
                    return false;
                case TestPrecisionType.WithinPrecision:
                    return Math.Abs(a - b) < Utils.TestPrecision;
                case TestPrecisionType.ToPence:
                    return Math.Abs(Math.Round((decimal)a, 2) - Math.Round((decimal)b, 2)) <= (decimal)0.01; // rounding with doubles not producing consistent results
                case TestPrecisionType.ToPounds:
                    return Math.Round(a) == Math.Round(b);
                case TestPrecisionType.ToWithinAPound:
                    return Math.Abs(a - b) <= 1;
                case TestPrecisionType.RelativePrecision:
                    if (Math.Abs(a - b) < Utils.TestPrecision)
                        return true;
                    var relativeError = Math.Abs(a) > Math.Abs(b)
                        ? Math.Abs((a - b) / b)
                        : Math.Abs((a - b) / a);
                    return relativeError <= 1 - TestAccuracy;
                case TestPrecisionType.ToSignificantFigures:
                    var length = Math.Round(Math.Abs(Math.Max(Math.Abs(a), Math.Abs(b)))).ToString().Length;
                    var sigFig = Math.Min(TestSignificantFigures, length);
                    return Math.Sign(a) == Math.Sign(b) && ToSignificantFigures(a, sigFig) == ToSignificantFigures(b, sigFig);
                default:
                    return false;
            }
        }

        public static readonly List<AssetClassType> LiabilityAssetClassTypes = new List<AssetClassType>
        {
                AssetClassType.LiabilityInterest,
                AssetClassType.LiabilityLongevity,
        };

        public static readonly List<AssetClassType> AllVarAssetClassTypes = new List<AssetClassType>
        {
                AssetClassType.LiabilityInterest,
                AssetClassType.LiabilityLongevity,
                AssetClassType.FixedInterestGilts,
                AssetClassType.IndexLinkedGilts,
                AssetClassType.Abf,
                //AssetClassType.Cash,
                AssetClassType.CorporateBonds,
                AssetClassType.Equity,
                AssetClassType.DiversifiedGrowth,
                AssetClassType.Property,
        };

        //This is a port of http://www.source-code.biz/snippets/vbasic/9.htm
        public static double NormSInv(double p)
        {
            if (p < 0 || p > 1)
                throw new ArgumentOutOfRangeException("p");

            const double a1 = -39.6968302866538,
                a2 = 220.946098424521,
                a3 = -275.928510446969,
                a4 = 138.357751867269,
                a5 = -30.6647980661472,
                a6 = 2.50662827745924,
                b1 = -54.4760987982241,
                b2 = 161.585836858041,
                b3 = -155.698979859887,
                b4 = 66.8013118877197,
                b5 = -13.2806815528857,
                c1 = -7.78489400243029E-03,
                c2 = -0.322396458041136,
                c3 = -2.40075827716184,
                c4 = -2.54973253934373,
                c5 = 4.37466414146497,
                c6 = 2.93816398269878,
                d1 = 7.78469570904146E-03,
                d2 = 0.32246712907004,
                d3 = 2.445134137143,
                d4 = 3.75440866190742,
                pLow = 0.02425,
                pHigh = 1 - pLow;
            double q;

            if (p < pLow)
            {
                q = Math.Sqrt(-2 * Math.Log(p));
                return (((((c1 * q + c2) * q + c3) * q + c4) * q + c5) * q + c6) / ((((d1 * q + d2) * q + d3) * q + d4) * q + 1);
            }

            if (p <= pHigh)
            {
                q = p - 0.5;
                var r = q * q;
                return (((((a1 * r + a2) * r + a3) * r + a4) * r + a5) * r + a6) * q / (((((b1 * r + b2) * r + b3) * r + b4) * r + b5) * r + 1);
            }

            q = Math.Sqrt(-2 * Math.Log(1 - p));
            return -(((((c1 * q + c2) * q + c3) * q + c4) * q + c5) * q + c6) / ((((d1 * q + d2) * q + d3) * q + d4) * q + 1);
        }

        private static double CumulativeNormalDistribution(double z, double mean = 0, double stdDev = 1)
        {
            z = (z - mean) / stdDev;

            if (z > 6)
                return 1;
            if (z < -6)
                return 0;

            const double b1 = 0.31938153;
            const double b2 = -0.356563782;
            const double b3 = 1.781477937;
            const double b4 = -1.821255978;
            const double b5 = 1.330274429;
            const double p = 0.2316419;
            const double c2 = 0.3989423;

            var a = Math.Abs(z);
            var t = 1 / (1 + a * p);
            var b = c2 * Math.Exp((-z) * (z / 2));
            var n = ((((b5 * t + b4) * t + b3) * t + b2) * t + b1) * t;
            n = 1 - b * n;

            if (z < 0)
                n = 1 - n;

            return n;
        }

        public static double GetBlackScholesValue(double inflation, double volatility, double multiple = 1, double floor = 1000, double cap = 999.99)
        {
            //if (Math.Abs(floor - 1000) < Precision)
            //    floor = 0.00001 - multiple;

            if (volatility <= 0)
                throw new ArgumentException("Volatility must be greater than zero");
            if (multiple <= 0)
                throw new ArgumentException("Multiple must be greater than zero");
            if (cap <= floor)
                throw new ArgumentException("Cap must be greater than floor");

            var d1 = Math.Log(multiple * (1 + inflation) / (multiple + cap)) / volatility + volatility / 2;
            var d2 = Math.Log((multiple + floor) / (multiple * (1 + inflation))) / volatility - volatility / 2;
            var d3 = Math.Log(multiple * (1 + inflation) / (multiple + cap)) / volatility - volatility / 2;
            var d4 = Math.Log((multiple + floor) / (multiple * (1 + inflation))) / volatility + volatility / 2;

            return inflation * multiple -
                   multiple * (1 + inflation) * (CumulativeNormalDistribution(d1) + CumulativeNormalDistribution(d2))
                   + (multiple + cap) * CumulativeNormalDistribution(d3) +
                   (multiple + floor) * CumulativeNormalDistribution(d4);
        }

        public static IEnumerable<T> PaddedTake<T>(this IEnumerable<T> source, int count)
        {
            return source
                .Concat(Enumerable.Repeat(default(T), count))
                .Take(count);
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || !source.Any();
        }

        public static IEnumerable<T> EnumToArray<T>() where T : struct, IConvertible
        {
            foreach(var val in Enum.GetValues(typeof(T)))
            {
                yield return (T)val;
            }
        }

        public static TAttr GetEnumAttribute<TEnum, TAttr>(TEnum value) where TEnum : struct, IConvertible
        {
            var type = typeof(TEnum);
            var member = type.GetMember(value.ToString());
            var attr = member[0].GetCustomAttributes(typeof(TAttr), false);
            return attr.Length > 0 ? (TAttr)attr[0] : default(TAttr);
        }

        public static string DisplayName(this Enum enumVal)
        {
            //TODO make this use GetEnumAttribute
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());

            if (memInfo.Length == 0)
                return String.Empty;

            var attributes = memInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false);
            return (attributes.Length > 0) ? ((DisplayAttribute)attributes[0]).Name : String.Empty;
        }

        public static int GetDiffInYears(DateTime d, DateTime dd)
        {
            for (var i = 0; i < MaxYear; i++)
                if (dd.AddYears(i) > d)
                    return i - 1;
            return MaxYear;
        }

        public static double MathSafeDiv(this double x, double y)
        {
            return MathSafeDiv(x, y, 0);
        }

        public static double MathSafeDiv(this double x, double y, double defaultVal)
        {
            if (x == 0 || y == 0) return defaultVal;
            return x / y;
        }

        public static IDictionary<string, object> GetObjectPropertyValues<T>(T obj) where T : class
        {
            if (obj == null)
                return new Dictionary<string, object>();

            Type t = typeof(T);
            var propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var propertyValues = new Dictionary<string, object>(propInfos.Length);
            foreach (var propInfo in propInfos)
            {
                propertyValues.Add(propInfo.Name, propInfo.GetValue(obj, null));
            }

            return propertyValues;
        }

        public static bool DictionariesRoughlyEquivalent(IDictionary<int, double> a, IDictionary<int, double> b, double precision)
        {
            foreach (var key in a.Keys)
            {
                if (a[key] != 0)
                {
                    if (!b.ContainsKey(key) || Math.Abs(a[key] - b[key]) > precision)
                        return false;
                }
                else
                {
                    if (b.ContainsKey(key) && b[key] != 0)
                        return false;
                }
            }

            return true;
        }

        public static AssetClassType? MapAssetClassNameToType(string name)
        {
            switch (name)
            {
                case "Liability Interest rate/Inflation":
                    return AssetClassType.LiabilityInterest;
                case "Liability Longevity":
                    return AssetClassType.LiabilityLongevity;
                case "Fixed Interest Gilts":
                    return AssetClassType.FixedInterestGilts;
                case "Index Linked Gilts":
                    return AssetClassType.IndexLinkedGilts;
                case "ABF":
                    return AssetClassType.Abf;
                case "Corporate Bonds":
                case "Corporates":
                    return AssetClassType.CorporateBonds;
                case "Equity":
                    return AssetClassType.Equity;
                case "DGF":
                case "Diversified Growth":
                    return AssetClassType.DiversifiedGrowth;
                case "Diversified Credit":
                    return AssetClassType.DiversifiedCredit;
                case "Private Markets":
                    return AssetClassType.PrivateMarkets;
                case "Property":
                    return AssetClassType.Property;
                case "Cash":
                    return AssetClassType.Cash;
                default:
                    return null;
            }
        }
    }
}