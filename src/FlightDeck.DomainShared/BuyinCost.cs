﻿namespace FlightDeck.DomainShared
{
    using System;
    
    public class BuyinCost : IEquatable<BuyinCost>
    {
        public double CostOfPensionerBuyin { get; private set; }
        public double CostOfNonpensionerBuyin { get; private set; }
        public double Total { get { return CostOfPensionerBuyin + CostOfNonpensionerBuyin; } }

        public BuyinCost(double costOfPensionerBuyin, double costOfNonpensionerBuyin)
        {
            CostOfPensionerBuyin = costOfPensionerBuyin;
            CostOfNonpensionerBuyin = costOfNonpensionerBuyin;
        }

        public bool Equals(BuyinCost other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(CostOfPensionerBuyin, other.CostOfPensionerBuyin)
                && Utils.EqualityCheck(CostOfNonpensionerBuyin, other.CostOfNonpensionerBuyin);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((BuyinCost)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = CostOfPensionerBuyin.GetHashCode();
                hashCode = (hashCode * 397) ^ CostOfNonpensionerBuyin.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("CostOfPensionerBuyin: {0}, CostOfNonpensionerBuyin: {1}", CostOfPensionerBuyin, CostOfNonpensionerBuyin);
        }
    }
}
