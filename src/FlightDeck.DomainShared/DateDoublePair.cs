﻿using System;

namespace FlightDeck.DomainShared
{
    public class DateDoublePair : IEquatable<DateDoublePair>
    {
        public DateTime Date { get; private set; }
        public double Value { get; private set; }

        public DateDoublePair(DateTime date, double value)
        {
            Date = date;
            Value = value;
        }

        public override string ToString()
        {
            return string.Format("{0:d} {1:0.0000}", Date, Value);
        }

        public bool Equals(DateDoublePair other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date == other.Date && Math.Abs(Value - other.Value) < 0.001;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((DateDoublePair)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Date.GetHashCode() * 397) ^ Value.GetHashCode();
            }
        }
    }
}