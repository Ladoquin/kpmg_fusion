﻿namespace FlightDeck.DomainShared
{
    using System;

    [Serializable]
    public class ExpectedDeficitData : IEquatable<ExpectedDeficitData>
    {
        public DateTime Date { get; private set; }
        public double Assets { get; private set; }
        public double Liabilities { get; private set; }
        public double Deficit { get; private set; }
        public double FundingLevel { get; private set; }

        public ExpectedDeficitData(DateTime date, double assets, double liabilities, double deficit, double fundingLevel)
        {
            Date = date;
            Assets = assets;
            Liabilities = liabilities;
            Deficit = deficit;
            FundingLevel = fundingLevel;
        }

        public bool Equals(ExpectedDeficitData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date == other.Date
                && Utils.EqualityCheck(Assets, other.Assets)
                && Utils.EqualityCheck(Liabilities, other.Liabilities)
                && Utils.EqualityCheck(Deficit, other.Deficit)
                && Utils.EqualityCheck(FundingLevel, other.FundingLevel);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((ExpectedDeficitData)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Date.GetHashCode();
                hashCode = (hashCode * 397) ^ Assets.GetHashCode();
                hashCode = (hashCode * 397) ^ Liabilities.GetHashCode();
                hashCode = (hashCode * 397) ^ Deficit.GetHashCode();
                hashCode = (hashCode * 397) ^ FundingLevel.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Date: {0}, Assets: {1}, Liabilities: {2}, Deficit: {3}, FundingLevel: {4}", Date.ToString(), Assets, Liabilities, Deficit, FundingLevel);
        }
    }
}
