﻿using System;

namespace FlightDeck.DomainShared
{
    public class VarFunnelData : IEquatable<VarFunnelData>
    {
        public double Percentile95 { get; private set; }
        public double Percentile80 { get; private set; }
        public double Median { get; private set; }
        public double Percentile20 { get; private set; }
        public double Percentile5 { get; private set; }

        public VarFunnelData(double percentile95, double percentile80, double median, double percentile20, double percentile5)
        {
            Percentile5 = percentile5;
            Percentile20 = percentile20;
            Median = median;
            Percentile80 = percentile80;
            Percentile95 = percentile95;
        }

        public bool Equals(VarFunnelData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Utils.EqualityCheck(Percentile95, other.Percentile95, precision: Utils.TestPrecisionType.ToPence)
                   && Utils.EqualityCheck(Percentile80, other.Percentile80, precision: Utils.TestPrecisionType.ToPence)
                   && Utils.EqualityCheck(Median, other.Median, precision: Utils.TestPrecisionType.ToPence)
                   && Utils.EqualityCheck(Percentile20, other.Percentile20, precision: Utils.TestPrecisionType.ToPence)
                   && Utils.EqualityCheck(Percentile5, other.Percentile5, precision: Utils.TestPrecisionType.ToPence);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((VarFunnelData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Percentile95.GetHashCode();
                hashCode = (hashCode*397) ^ Percentile80.GetHashCode();
                hashCode = (hashCode*397) ^ Median.GetHashCode();
                hashCode = (hashCode*397) ^ Percentile20.GetHashCode();
                hashCode = (hashCode*397) ^ Percentile5.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Percentile95: {0}, Percentile80: {1}, Median: {2}, Percentile20: {3}, Percentile5: {4}",
                Percentile95, Percentile80, Median, Percentile20, Percentile5);
        }
    }
}