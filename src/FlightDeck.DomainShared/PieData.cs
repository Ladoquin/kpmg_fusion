﻿using System;

namespace FlightDeck.DomainShared
{
    public class PieData : IEquatable<PieData>
    {
        public double LiabilityExcludingLevelPensions { get; private set; }
        public double ValueOfIncreases1 { get; private set; }
        public double ValueOfIncreases2 { get; private set; }
        public double EligibleForExchange { get; private set; }
        public double CostNeutralUplift { get; private set; }
        public double ValueShared { get; private set; }
        public BeforeAfter<double> Liability { get; private set; }
        public double LiabilityChange { get; private set; }
        public double ZeroIncreases { get; private set; }        
        public double PensionerLiabIncrease { get; private set; }
        public double ChangeInFundingPosition { get { return Liability.Before - Liability.After; } }

        public PieData(BeforeAfter<double> liability, double liabilityExcludingLevelPensions, double liabilityChange, double eligibleForExchange,
            double zeroIncreases, double valueOfIncreases1, double valueOfIncreases2, double pensionerLiabIncrease,
            double costNeutralUplift, double valueShared)
        {
            ValueOfIncreases1 = valueOfIncreases1;
            ValueOfIncreases2 = valueOfIncreases2;
            LiabilityExcludingLevelPensions = liabilityExcludingLevelPensions;
            PensionerLiabIncrease = pensionerLiabIncrease;
            CostNeutralUplift = costNeutralUplift;
            ValueShared = valueShared;
            ZeroIncreases = zeroIncreases;
            EligibleForExchange = eligibleForExchange;
            LiabilityChange = liabilityChange;
            Liability = liability;
        }

        public bool Equals(PieData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Liability.DoubleEquals(other.Liability)
                && Math.Abs(LiabilityExcludingLevelPensions - other.LiabilityExcludingLevelPensions) < Utils.Precision
                && Math.Abs(LiabilityChange - other.LiabilityChange) < Utils.Precision
                && Math.Abs(EligibleForExchange - other.EligibleForExchange) < Utils.Precision
                && Math.Abs(ZeroIncreases - other.ZeroIncreases) < Utils.Precision
                && Math.Abs(ValueOfIncreases1 - other.ValueOfIncreases1) < Utils.Precision
                && Math.Abs(PensionerLiabIncrease - other.PensionerLiabIncrease) < Utils.Precision
                && Math.Abs(CostNeutralUplift - other.CostNeutralUplift) < Utils.Precision
                && Math.Abs(ValueShared - other.ValueShared) < Utils.Precision;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((PieData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Liability != null ? Liability.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ LiabilityExcludingLevelPensions.GetHashCode();
                hashCode = (hashCode*397) ^ LiabilityChange.GetHashCode();
                hashCode = (hashCode*397) ^ EligibleForExchange.GetHashCode();
                hashCode = (hashCode*397) ^ ZeroIncreases.GetHashCode();
                hashCode = (hashCode*397) ^ ValueOfIncreases1.GetHashCode();
                hashCode = (hashCode*397) ^ PensionerLiabIncrease.GetHashCode();
                hashCode = (hashCode*397) ^ CostNeutralUplift.GetHashCode();
                hashCode = (hashCode*397) ^ ValueShared.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Liability: {0}, LiabilityExcludingLevelPensions: {1}, LiabilityChange: {2}, EligibleForExchange: {3}, ZeroIncreases: {4}, ValueOfIncreases: {5}, PensionerLiabIncrease: {6}, CostNeutralUplift: {7}, ValueShared: {8}", Liability, LiabilityExcludingLevelPensions, LiabilityChange, EligibleForExchange, ZeroIncreases, ValueOfIncreases1, PensionerLiabIncrease, CostNeutralUplift, ValueShared);
        }
    }
}