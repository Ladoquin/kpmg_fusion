﻿namespace FlightDeck.DomainShared
{
    using System;

    [Serializable]
    public class EnhancedTransferValueOptions
    {
        public double EquivalentTransferValue { get; private set; }
        public double EnhancementToCetv { get; private set; }
        public double TakeUpRate { get; private set; }

        public EnhancedTransferValueOptions(double equivalentTransferValue, double enhancementToCetv, double takeUpRate)
        {
            EquivalentTransferValue = equivalentTransferValue;
            EnhancementToCetv = enhancementToCetv;
            TakeUpRate = takeUpRate;
        }

        public override string ToString()
        {
            return string.Format("EquivalentTransferValue: {0}, EnhancementToCetv: {1}, TakeUpRate: {2}", EquivalentTransferValue, EnhancementToCetv, TakeUpRate);
        }
    }
}
