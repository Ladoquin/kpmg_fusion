﻿namespace FlightDeck.DomainShared
{
    using System;
    
    public class FundingData
    {
        
        public double Actives { get; private set; }
        public double Deferreds { get; private set; }
        public double Pensioners { get; private set; }
        public double Assets { get; private set; }
        public double Buyin { get; private set; }
        public double Deficit { get; private set; }

        public double Liabilities
        {
            get
            {
                return Actives + Deferreds + Pensioners;
            }
        }
        public double AssetsTotal
        {
            get
            {
                return Assets + Buyin;
            }
        }

        public FundingData(double actives, double deferreds, double pensioners, double assets, double buyin, double deficit)
        {
            Actives = actives;
            Deferreds = deferreds;
            Pensioners = pensioners;
            Assets = assets;
            Buyin = buyin;
            Deficit = deficit;
        }

        public bool Equals(FundingData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Math.Abs(Actives - other.Actives) < Utils.TestPrecision
                && Math.Abs(Deferreds - other.Deferreds) < Utils.TestPrecision
                && Math.Abs(Pensioners - other.Pensioners) < Utils.TestPrecision
                && Math.Abs(Assets - other.Assets) < Utils.TestPrecision
                && Math.Abs(Buyin - other.Buyin) < Utils.TestPrecision
                && Math.Abs(Deficit - other.Deficit) < Utils.TestPrecision;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
            return Equals((FundingData)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Actives.GetHashCode();
                hashCode = (hashCode * 397) ^ Deferreds.GetHashCode();
                hashCode = (hashCode * 397) ^ Pensioners.GetHashCode();
                hashCode = (hashCode * 397) ^ Assets.GetHashCode();
                hashCode = (hashCode * 397) ^ Buyin.GetHashCode();
                hashCode = (hashCode * 397) ^ Deficit.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Actives: {0}, Deferreds: {1}, Pensioners: {2}, Assets: {3}, Buyin: {4}, Deficit: {5}", Actives, Deferreds, Pensioners, Assets, Buyin, Deficit);
        }
    }
}
