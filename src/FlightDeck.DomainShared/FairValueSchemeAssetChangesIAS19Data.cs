﻿using FlightDeck.DomainShared;
using System;

public class FairValueSchemeAssetChangesIAS19Data : IEquatable<FairValueSchemeAssetChangesIAS19Data>
{
    public double OpeningFairValueSchemeAssets { get; private set; }
    public double InterestIncome { get; private set; }
    public double ReturnOnSchemeAssets { get; private set; }
    public double SchemeAdminExpenses { get; private set; }
    public double ContributionsEmployer { get; private set; }
    public double ContributionsMembers { get; private set; }
    public double BenefitsPaid { get; private set; }
    public double ClosingFairValueSchemeAssets { get; private set; }

    public FairValueSchemeAssetChangesIAS19Data()
    {
    }
    public FairValueSchemeAssetChangesIAS19Data(double openingFairValueSchemeAssets, double interestIncome, double returnOnSchemeAssets, double schemeAdminExpenses, double contributionsEmployer, double contributionsMembers, double benefitsPaid, double closingFairValueSchemeAssets)
    {
        OpeningFairValueSchemeAssets = openingFairValueSchemeAssets;
        InterestIncome = interestIncome;
        ReturnOnSchemeAssets = returnOnSchemeAssets;
        SchemeAdminExpenses = schemeAdminExpenses;
        ContributionsEmployer = contributionsEmployer;
        ContributionsMembers = contributionsMembers;
        BenefitsPaid = benefitsPaid;
        ClosingFairValueSchemeAssets = closingFairValueSchemeAssets;
    }

    public bool Equals(FairValueSchemeAssetChangesIAS19Data other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Utils.EqualityCheck(OpeningFairValueSchemeAssets, other.OpeningFairValueSchemeAssets)
            && Utils.EqualityCheck(InterestIncome, other.InterestIncome)
            && Utils.EqualityCheck(ReturnOnSchemeAssets, other.ReturnOnSchemeAssets)
            && Utils.EqualityCheck(SchemeAdminExpenses, other.SchemeAdminExpenses)
            && Utils.EqualityCheck(ContributionsEmployer, other.ContributionsEmployer)
            && Utils.EqualityCheck(ContributionsMembers, other.ContributionsMembers)
            && Utils.EqualityCheck(BenefitsPaid, other.BenefitsPaid)
            && Utils.EqualityCheck(ClosingFairValueSchemeAssets, other.ClosingFairValueSchemeAssets);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType() && !this.GetType().IsSubclassOf(obj.GetType())) return false;
        return Equals((FairValueSchemeAssetChangesIAS19Data)obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = OpeningFairValueSchemeAssets.GetHashCode();
            hashCode = (hashCode * 397) ^ InterestIncome.GetHashCode();
            hashCode = (hashCode * 397) ^ ReturnOnSchemeAssets.GetHashCode();
            hashCode = (hashCode * 397) ^ SchemeAdminExpenses.GetHashCode();
            hashCode = (hashCode * 397) ^ ContributionsEmployer.GetHashCode();
            hashCode = (hashCode * 397) ^ ContributionsMembers.GetHashCode();
            hashCode = (hashCode * 397) ^ BenefitsPaid.GetHashCode();
            hashCode = (hashCode * 397) ^ ClosingFairValueSchemeAssets.GetHashCode();
            return hashCode;
        }
    }

    public override string ToString()
    {
        return string.Format("OpeningFairValueSchemeAssets: {0}, InterestIncome: {1}, ReturnOnSchemeAssets: {2}, SchemeAdminExpenses: {3}, ContributionsEmployer: {4}, ContributionsMembers: {5}, BenefitsPaid: {6}, ClosingFairValueSchemeAssets: {7}", OpeningFairValueSchemeAssets, InterestIncome, ReturnOnSchemeAssets, SchemeAdminExpenses, ContributionsEmployer, ContributionsMembers, BenefitsPaid, ClosingFairValueSchemeAssets);
    }
}