﻿namespace FlightDeck.DomainShared
{
    public class ImportantMessage
    {
        public int Key { get; set; }
        public string Message { get; set; }
    }
}
