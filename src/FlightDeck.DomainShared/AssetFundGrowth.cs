﻿namespace FlightDeck.DomainShared
{
    public class AssetFundGrowth
    {
        public AssetCategory Category { get; private set; }
        public AssetClassType Class { get; private set; }
        public bool IsSynthetic { get; set; } //aghhck, ugly workaround for the fact that we don't have a 'Synthetic' AssetCategory and it's too late in the 1.6 day to add it now
        public string Name { get; private set; }        
        public double? Growth { get; set; }

        public AssetFundGrowth(string name)
        {
            Name = name;
        }
        public AssetFundGrowth(AssetCategory category, AssetClassType type, string name)
        {
            Category = category;
            Class = type;
            Name = name;
        }
    }
}
