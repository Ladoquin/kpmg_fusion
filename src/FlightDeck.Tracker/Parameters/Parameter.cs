﻿namespace FlightDeck.Tracker.Parameters
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Models;
    using System;
    using System.Collections.Generic;

    public class Parameter
    {
        public int Id { get; set; }
        public string SchemeName { get; set; }
        public string SchemeRef { get; set; }
        public string BasisName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public TrackerJourneyPlanOptions NewJPAssumptions { get; set; }
        public BasisAssumptions BasisAssumptions { get; set; }
        public SchemeAssumptions SchemeAssumptions { get; set; }
    }
}
