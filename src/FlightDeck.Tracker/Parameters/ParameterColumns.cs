﻿namespace FlightDeck.Tracker.Parameters
{
    public class ParameterColumns
    {
        public const string LiabilitiesCol = "E";
        public const string MortalityCol = "K";
        public const string AssetsCol = "M";
        public const string RecoveryPlanCol = "W";
        public const string InvestmentStrategyCol = "AD";
        public const string CashInjection = "AM";
        public const string FROCol = "AS";
        public const string ETVCol = "AW";
        public const string PIECol = "AZ";
        public const string FutureBenefitsCol = "BC";
        public const string ABFCol = "BF";
        public const string InsuranceCol = "BG";
        public const string NewJPCol = "BI";
        public const string VarWaterfallCol = "BX";
    }
}
