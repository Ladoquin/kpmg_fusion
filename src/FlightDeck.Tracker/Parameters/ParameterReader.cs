﻿namespace FlightDeck.Tracker.Parameters
{
    using FlexCel.XlsAdapter;
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Models;
    using FlightDeck.Tracker.Reader;
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    public class ParameterReader
    {
        private XlsFile xls;

        public ParameterReader()
            : this(null)
        {
        }
        public ParameterReader(string parametersFilepath)
        {
            parametersFilepath = parametersFilepath ?? ConfigurationManager.AppSettings["TestParametersPath"];

            xls = new XlsFile(parametersFilepath, false);
        }

        public IEnumerable<Parameter> ReadAll()
        {
            for (int sheetIdx = 1; sheetIdx <= xls.SheetCount; sheetIdx++)
            {
                xls.ActiveSheet = sheetIdx;

                var schemeName = getCellVal<string>("B", 1);

                var c = 1;
                var r = 4;

                while (xls.GetCellValue(r, c) != null)
                {
                    var id = (int)getCellVal<double>("A", r);
                    var basisName = getCellVal<string>("B", r);
                    var start = DateTime.FromOADate(getCellVal<double>("C", r));
                    var end = DateTime.FromOADate(getCellVal<double>("D", r));
                    var newJP = getNewJPParameters(basisName, r);
                    var schemeAssumptions = getSchemeAssumptions(r);
                    var basisAssumptions = getBasisAssumptions(r);
                    var schemeRef = xls.SheetName;

                    yield return new Parameter
                        {
                            Id = id,
                            SchemeName = schemeName,
                            SchemeRef = schemeRef,
                            BasisName = basisName,
                            Start = start,
                            End = end,
                            NewJPAssumptions = newJP,
                            SchemeAssumptions = schemeAssumptions,
                            BasisAssumptions = basisAssumptions
                        };

                    r++;
                }
            }
        }

        private T getRangeVal<T>(string namedRange, int sheetIdx)
        {
            var coords = xls.GetNamedRange(namedRange, sheetIdx);

            return (T)xls.GetCellValue(coords.Top, coords.Left);
        }

        private T getCellVal<T>(string col, int row)
        {
            return getCellVal<T>(XlHelper.GetCol(col), row);
        }

        private T getCellVal<T>(int col, int row)
        {
            return (T)xls.GetCellValue(row, col);
        }

        private TrackerJourneyPlanOptions getNewJPParameters(string currentBasisName, int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.NewJPCol);
            var trackerJPOptions = new TrackerJourneyPlanOptions
                {
                    FundingBasisName = getCellVal<string>(col, row),
                    SelfSufficiencyBasisName = currentBasisName,
                    BuyoutBasisName = getCellVal<string>(col + 1, row),
                    IncludeBuyIns = xls.GetCellValue(row, col + 2) == null ? false : getCellVal<bool>(col + 2, row)
                };
            if (xls.GetCellValue(row, col + 3) != null)
                trackerJPOptions.CurrentReturn = getCellVal<double>(col + 3, row);
            if (xls.GetCellValue(row, col + 4) != null)
                trackerJPOptions.SelfSufficiencyDiscountRate = getCellVal<double>(col + 4, row);
            if (xls.GetCellValue(row, col + 5) != null)
                trackerJPOptions.InitialReturn = getCellVal<double>(col + 5, row);
            if (xls.GetCellValue(row, col + 6) != null)
                trackerJPOptions.FinalReturn = getCellVal<double>(col + 6, row);
            if (xls.GetCellValue(row, col + 7) != null)
                trackerJPOptions.TriggerSteps = (int) getCellVal<double>(col + 7, row);
            if (xls.GetCellValue(row, col + 8) != null)
                trackerJPOptions.TriggerTimeStartYear = (int) getCellVal<double>(col + 8, row);
            if (xls.GetCellValue(row, col + 9) != null)
                trackerJPOptions.TriggerTransitionPeriod = (int) getCellVal<double>(col + 9, row);

            var extraContOptions = new NewJPExtraContributionOptions();
            if (xls.GetCellValue(row, col + 10) != null)
                extraContOptions.AddCurrentRecoveryPlan = getCellVal<bool>(col + 10, row);
            if (xls.GetCellValue(row, col + 11) != null)
                extraContOptions.AddExtraContributions = getCellVal<bool>(col + 11, row);
            if (xls.GetCellValue(row, col + 12) != null)
                extraContOptions.StartYear = (int) getCellVal<double>(col + 12, row);
            if (xls.GetCellValue(row, col + 13) != null)
                extraContOptions.AnnualContributions = getCellVal<double>(col + 13, row);
            if (xls.GetCellValue(row, col + 14) != null)
                extraContOptions.Term = (int) getCellVal<double>(col + 14, row);

            trackerJPOptions.RecoveryPlan = extraContOptions;

            return trackerJPOptions;
        }

        private SchemeAssumptions getSchemeAssumptions(int row)
        {
            return new SchemeAssumptions
                (
                    getFROType(row),
                    getFRO(row),
                    getEFRO(row),
                    getETV(row),
                    getPIE(row),
                    getFutureBenefits(row),
                    getInvestmentStrategyAssetAllocation(row),
                    getInvestmentStrategyOptions(row),
                    getABF(row),
                    getInsurance(row),
                    getRecoveryPlanType(row),
                    getWaterfallOptions(row)
                );
        }

        private BasisAssumptions getBasisAssumptions(int row)
        {
            return new BasisAssumptions
                (
                    getLiabilities(row),
                    getMortality(row),
                    null,
                    getAssets(row),
                    getRecoveryPlan(row)
                );
        }

        private FROType getFROType(int row)
        {
            return xls.GetCellValue(row, XlHelper.GetCol(ParameterColumns.FROCol) + 2) == null
                ? FROType.Bulk
                : FROType.Embedded;
        }

        private FlexibleReturnOptions getFRO(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.FROCol);
            return xls.GetCellValue(row, col) == null
                ? null
                : new FlexibleReturnOptions
                    (
                        getCellVal<double>(col, row),
                        getCellVal<double>(col + 1, row) / 100.0
                    );
        }

        private EmbeddedFlexibleReturnOptions getEFRO(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.FROCol) + 2;
            return xls.GetCellValue(row, col) == null
                ? null
                : new EmbeddedFlexibleReturnOptions
                    (
                        getCellVal<double>(col, row) / 100.0,
                        getCellVal<double>(col + 1, row) / 100.0
                    );
        }

        private EnhancedTransferValueOptions getETV(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.ETVCol);
            return xls.GetCellValue(row, col) == null
                ? null
                : new EnhancedTransferValueOptions
                    (
                        getCellVal<double>(col, row),
                        getCellVal<double>(col + 1, row) / 100.0,
                        getCellVal<double>(col + 2, row) / 100.0
                    );
        }

        private PieOptions getPIE(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.PIECol);
            return xls.GetCellValue(row, col) == null
                ? null
                : new PieOptions
                    (
                        getCellVal<double>(col, row) / 100.0,
                        getCellVal<double>(col + 1, row) / 100.0,
                        getCellVal<double>(col + 2, row) / 100.0
                    );
        }

        private FutureBenefitsParameters getFutureBenefits(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.FutureBenefitsCol);
            if (xls.GetCellValue(row, col) == null)
                return null;

            var adjustmentType = BenefitAdjustmentType.None;
            switch ((int)getCellVal<double>(col + 1, row))
            {
                case 1:
                    adjustmentType = BenefitAdjustmentType.BenefitReduction;
                    break;
                case 2:
                    adjustmentType = BenefitAdjustmentType.SalaryLimit;
                    break;
                case 3:
                    adjustmentType = BenefitAdjustmentType.CareRevaluation;
                    break;
                case 4:
                    adjustmentType = BenefitAdjustmentType.DcImplementation;
                    break;
                case 0:
                default:
                    adjustmentType = BenefitAdjustmentType.None;
                    break;
            };
            return new FutureBenefitsParameters
                    (
                        getCellVal<double>(col, row) / 100.0,
                        adjustmentType,
                        adjustmentType == BenefitAdjustmentType.None ? 0 : getCellVal<double>(col + 2, row) / 100.0
                    );
        }

        private Dictionary<AssetClassType, double> getInvestmentStrategyAssetAllocation(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.InvestmentStrategyCol);
            return xls.GetCellValue(row, col) == null
                ? null
                : new Dictionary<AssetClassType, double>
                    {
                        { AssetClassType.Equity, getCellVal<double>(col, row) / 100.0 },
                        { AssetClassType.Property, getCellVal<double>(col + 1, row) / 100.0 },
                        { AssetClassType.DiversifiedGrowth, getCellVal<double>(col + 2, row) / 100.0 },
                        { AssetClassType.DiversifiedCredit, getCellVal<double>(col + 3, row) / 100.0 },
                        { AssetClassType.PrivateMarkets, getCellVal<double>(col + 4, row) / 100.0 },
                        { AssetClassType.CorporateBonds, getCellVal<double>(col + 5, row) / 100.0 },
                        { AssetClassType.FixedInterestGilts, getCellVal<double>(col + 6, row) / 100.0 },
                        { AssetClassType.IndexLinkedGilts, getCellVal<double>(col + 7, row) / 100.0 },
                        { AssetClassType.Cash, getCellVal<double>(col + 8, row) / 100.0 }
                    };
        }

        private InvestmentStrategyOptions getInvestmentStrategyOptions(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.CashInjection);
            var cashInjectionEntered = xls.GetCellValue(row, col) != null;
            var advancedOptionsEntered = xls.GetCellValue(row, col + 1) != null;
            return !cashInjectionEntered && !advancedOptionsEntered
                ? null
                : new InvestmentStrategyOptions(
                    cashInjectionEntered ? getCellVal<double>(col, row) : 0,
                    advancedOptionsEntered ? getCellVal<double>(col + 1, row) : 0,
                    advancedOptionsEntered ? getCellVal<double>(col + 2, row) : 0,
                    advancedOptionsEntered ? getCellVal<bool>(col + 3, row) : true,
                    advancedOptionsEntered ? getCellVal<double>(col + 4, row) : 0,
                    advancedOptionsEntered ? getCellVal<double>(col + 5, row) : 0);
        }

        private AbfOptions getABF(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.ABFCol);
            return xls.GetCellValue(row, col) == null
                ? null
                : new AbfOptions(getCellVal<double>(col, row));
        }

        private InsuranceParameters getInsurance(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.InsuranceCol);
            return xls.GetCellValue(row, col) == null
                ? null
                : new InsuranceParameters
                    (
                        getCellVal<double>(col, row) / 100.0,
                        getCellVal<double>(col + 1, row) / 100.0
                    );
        }

        private RecoveryPlanType getRecoveryPlanType(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.RecoveryPlanCol) + 6;
            if (xls.GetCellValue(row, col) == null)
                return RecoveryPlanType.Current;

            switch(getCellVal<string>(col, row)) 
            { 
                case "New plan":
                    return RecoveryPlanType.NewDynamic;
                case "New plan (fixed)":
                    return RecoveryPlanType.NewFixed;
                case "Current plan":
                default: 
                    return RecoveryPlanType.Current;
            };
        }

        private WaterfallOptions getWaterfallOptions(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.VarWaterfallCol);
            return xls.GetCellValue(row, col) == null
                ? null
                : new WaterfallOptions(getCellVal<double>(col + 1, row), (int)getCellVal<double>(col, row));
        }

        private Dictionary<AssumptionType, double> getLiabilities(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.LiabilitiesCol);

            var liabs = new Dictionary<AssumptionType, double>();

            if (xls.GetCellValue(row, col) != null)
                liabs.Add(AssumptionType.PreRetirementDiscountRate, getCellVal<double>(col, row) / 100.0);

            if (xls.GetCellValue(row, col + 1) != null)
                liabs.Add(AssumptionType.PostRetirementDiscountRate, getCellVal<double>(col + 1, row) / 100.0);

            if (xls.GetCellValue(row, col + 2) != null)
                liabs.Add(AssumptionType.PensionDiscountRate, getCellVal<double>(col + 2, row) / 100.0);

            if (xls.GetCellValue(row, col + 3) != null)
                liabs.Add(AssumptionType.SalaryIncrease, getCellVal<double>(col + 3, row) / 100.0);

            if (xls.GetCellValue(row, col + 4) != null)
                liabs.Add(AssumptionType.InflationRetailPriceIndex, getCellVal<double>(col + 4, row) / 100.0);

            if (xls.GetCellValue(row, col + 5) != null)
                liabs.Add(AssumptionType.InflationConsumerPriceIndex, getCellVal<double>(col + 5, row) / 100.0);

            return liabs;
        }

        private LifeExpectancyAssumptions getMortality(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.MortalityCol);
            return xls.GetCellValue(row, col) == null
                ? null
                : new LifeExpectancyAssumptions
                    (
                        getCellVal<double>(col, row),
                        getCellVal<double>(col + 1, row)
                    );
        }

        private Dictionary<AssetClassType, double> getAssets(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.AssetsCol);

            var assets = new Dictionary<AssetClassType, double>();

            if (xls.GetCellValue(row, col) != null)
                assets.Add(AssetClassType.Equity, getCellVal<double>(col, row) / 100.0);

            if (xls.GetCellValue(row, col + 1) != null)
                assets.Add(AssetClassType.Property, getCellVal<double>(col + 1, row) / 100.0);

            if (xls.GetCellValue(row, col + 2) != null)
                assets.Add(AssetClassType.DiversifiedGrowth, getCellVal<double>(col + 2, row) / 100.0);

            if (xls.GetCellValue(row, col + 3) != null)
                assets.Add(AssetClassType.DiversifiedCredit, getCellVal<double>(col + 3, row) / 100.0);

            if (xls.GetCellValue(row, col + 4) != null)
                assets.Add(AssetClassType.PrivateMarkets, getCellVal<double>(col + 4, row) / 100.0);

            if (xls.GetCellValue(row, col + 5) != null)
                assets.Add(AssetClassType.CorporateBonds, getCellVal<double>(col + 5, row) / 100.0);

            if (xls.GetCellValue(row, col + 6) != null)
                assets.Add(AssetClassType.FixedInterestGilts, getCellVal<double>(col + 6, row) / 100.0);

            if (xls.GetCellValue(row, col + 7) != null)
                assets.Add(AssetClassType.IndexLinkedGilts, getCellVal<double>(col + 7, row) / 100.0);

            if (xls.GetCellValue(row, col + 8) != null)
                assets.Add(AssetClassType.Cash, getCellVal<double>(col + 8, row) / 100.0);

            if (xls.GetCellValue(row, col + 9) != null)
                assets.Add(AssetClassType.Abf, getCellVal<double>(col + 9, row) / 100.0);

            return assets;
        }

        private RecoveryPlanAssumptions getRecoveryPlan(int row)
        {
            var col = XlHelper.GetCol(ParameterColumns.RecoveryPlanCol);
            if (xls.GetCellValue(row, col) == null)
                return null;

            var isGrowthFixed = getCellVal<double>(col, row) != 0;
            return new RecoveryPlanAssumptions
                    (
                        isGrowthFixed ? getCellVal<double>(col, row) / 100.0 : 0,
                        isGrowthFixed ? 0 : getCellVal<double>(col + 1, row) / 100.0,
                        getCellVal<double>(col + 5, row),
                        0,
                        getCellVal<double>(col + 4, row) / 100.0,
                        isGrowthFixed,
                        (int)getCellVal<double>(col + 2, row),
                        (int)getCellVal<double>(col + 3, row)
                    );
        }
    }
}