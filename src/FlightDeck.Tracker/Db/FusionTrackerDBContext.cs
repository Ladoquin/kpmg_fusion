﻿namespace FlightDeck.Tracker.Db
{
    using FlightDeck.Domain;
    using FlightDeck.Tracker.Models;
    using System.Data.Entity;

    public class FusionTrackerDBContext : DbContext
    {
        public DbSet<TrackerData> TrackerData { get; set; }

        public static void SetDropCreateDatabaseAlways()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<FusionTrackerDBContext>());
        }
    }
}
