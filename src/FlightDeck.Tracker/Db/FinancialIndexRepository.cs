﻿namespace FlightDeck.Tracker.Db
{
    using FlightDeck.Domain;
    using FlightDeck.Tracker.Reader;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Saving almost a million index values using Entity Framework is ridiculously slow, and I mean 10+ minutes slow, so just use a static lookup. 
    /// Does the job just as well for reading. Only drawback is no persistance between test runs.
    /// </summary>
    public class FinancialIndexRepository : IIndexReader
    {
        private static IList<FinancialIndex> indices;

        public FinancialIndex Get(string indexName)
        {
            var fi = indices.SingleOrDefault(x => x.Name == indexName);

            return fi;
        }

        public void Save(IList<FinancialIndex> values)
        {
            indices = values;
        }
    }
}
