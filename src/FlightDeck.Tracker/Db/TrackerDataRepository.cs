﻿namespace FlightDeck.Tracker.Db
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Accounting;
    using FlightDeck.Tracker.Models;
    using FlightDeck.Tracker.Reader;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public class TrackerDataRepository
    {
        public void Save(TrackerData data)
        {
            using (var db = new FusionTrackerDBContext())
            {
                db.TrackerData.Add(data);
                db.SaveChanges();
            }
        }

        public IEnumerable<TrackerData> GetAllSummaries()
        {
            IEnumerable<TrackerData> summaries = null;

            using (var db = new FusionTrackerDBContext())
            {
                summaries = db.TrackerData.ToList();
            }

            return summaries;
        }

        public IEnumerable<TrackerData> GetSummaries(IDictionary<string, List<int>> scenarios)
        {
            var summaries = GetAllSummaries().ToList();
            if (scenarios != null && summaries != null) {
                // filter summaries with supplied scenarios for each scheme
                return summaries.Where(x => scenarios.Any(
                                        s => s.Key == x.SchemeRef
                                        && s.Value.Any(i => i == x.ScenarioId)
                                      )
                            ).ToList();
            }
            else
                return new List<TrackerData>();
        }

        public TrackerData Get(int scenarioId, string schemeName, DateTime dateCreated)
        {
            TrackerData d = null;

            using (var db = new FusionTrackerDBContext())
            {
                //lazy load the scenario (because eager loading the whole object graph takes FOREVER!)
                d = db.TrackerData.AsNoTracking().Single(x => x.ScenarioId == scenarioId && x.SchemeName == schemeName && x.DateCreated == dateCreated);

                //ensure parent objects are not null just so we don't have to perform null checks all through the instantiation block
                d.Assumptions = d.Assumptions ?? new TrackerAssumptions();
                d.Assumptions.BasisAssumptions = d.Assumptions.BasisAssumptions ?? new TrackerBasisAssumptions();
                d.Assumptions.SchemeAssumptions = d.Assumptions.SchemeAssumptions ?? new TrackerSchemeAssumptions();
                d.Assumptions.JourneyPlanAssumptions = d.Assumptions.JourneyPlanAssumptions ?? new TrackerJourneyPlanOptions();
                d.Assumptions.StoredUserAssumptions = d.Assumptions.StoredUserAssumptions ?? new List<BasisAssumptionsDictionary>();
                d.Results = d.Results ?? new TrackerOutputs();

                //instantiate all lazy loaded objects and lists
                //doing this is about a kadrillion times faster than attempting to eager load the object graph using .Include() ...

                //assumptions
                d.Assumptions.SchemeAssumptions.InvestmentStrategyAssetAllocation = Instantiate(d.Assumptions.SchemeAssumptions.InvestmentStrategyAssetAllocation);
                d.Assumptions.BasisAssumptions.LiabilityAssumptions = Instantiate(d.Assumptions.BasisAssumptions.LiabilityAssumptions);
                d.Assumptions.BasisAssumptions.PensionIncreases = Instantiate(d.Assumptions.BasisAssumptions.PensionIncreases);
                d.Assumptions.BasisAssumptions.AssetAssumptions = Instantiate(d.Assumptions.BasisAssumptions.AssetAssumptions);
                d.Assumptions.StoredUserAssumptions = d.Assumptions.StoredUserAssumptions == null ? null :
                    d.Assumptions.StoredUserAssumptions.Select(x => new BasisAssumptionsDictionary(
                        x.Key,
                        new TrackerBasisAssumptions
                            (
                                x.Value.LiabilityAssumptions == null ? null : x.Value.LiabilityAssumptions.ToDictionary(l => l.Key, l => l.Value),
                                x.Value.LifeExpectancy,
                                x.Value.PensionIncreases == null ? null : x.Value.PensionIncreases.ToDictionary(p => p.Key, p => p.Value),
                                x.Value.AssetAssumptions == null ? null : x.Value.AssetAssumptions.ToDictionary(a => a.Key, a => a.Value),
                                x.Value.RecoveryPlanOptions
                            ))
                        ).ToList();
                d.Assumptions.JourneyPlanAssumptions.RecoveryPlan.ToString();

                //evolution
                d.Results.LDIEvolutionData = Instantiate(d.Results.LDIEvolutionData);
                d.Results.LiabilityEvolution = Instantiate(d.Results.LiabilityEvolution);
                d.Results.AssetEvolution = Instantiate(d.Results.AssetEvolution);
                d.Results.LiabilityAssumptionEvolution = d.Results.LiabilityAssumptionEvolution == null ? null : d.Results.LiabilityAssumptionEvolution.Select(x =>
                    new TrackerLiabilityAssumptionEvolution
                    {
                        Date = x.Date,
                        Assumptions = x.Assumptions.ToList()
                    })
                        .ToList();
                d.Results.PensionIncreaseEvolution = d.Results.PensionIncreaseEvolution == null ? null : d.Results.PensionIncreaseEvolution.Select(x =>
                    new TrackerPensionIncreaseEvolutionItem
                    {
                        Date = x.Date,
                        Pincs = x.Pincs.ToList()
                    })
                        .ToList();
                d.Results.LiabilityExperience = Instantiate(d.Results.LiabilityExperience);
                d.Results.BuyinExperience = Instantiate(d.Results.BuyinExperience);
                d.Results.AssetExperience = Instantiate(d.Results.AssetExperience);
                d.Results.ExpectedDeficit = Instantiate(d.Results.ExpectedDeficit);

                //analysis
                d.Results.ABF.ToString();
                d.Results.BalanceSheetEstimate.ToString();
                d.Results.BuyIn.ToString();
                d.Results.EFRO.ToString();
                d.Results.ETV.ToString();
                d.Results.FRO.ToString();
                d.Results.FundingLevel.ToString();
                d.Results.FutureBenefits.ToString();
                d.Results.GiltBasisLiability.ToString();
                d.Results.HedgeBreakdown.ToString();
                d.Results.InvestmentStrategyResults.ToString();
                d.Results.PIE.ToString();
                d.Results.PIEInit.ToString();
                d.Results.ProfitLoss.ToString();
                d.Results.SurplusAnalysisData.ToString();
                d.Results.UserLiabs.ToString();
                d.Results.Waterfall.ToString();
                d.Results.CashflowsBefore = d.Results.CashflowsBefore == null ? null : d.Results.CashflowsBefore.Select(x => new TrackerCashflow(x.Year, x.MemberStatuses.ToList())).ToList();
                d.Results.CashflowsAfter = d.Results.CashflowsAfter == null ? null : d.Results.CashflowsAfter.Select(x => new TrackerCashflow(x.Year, x.MemberStatuses.ToList())).ToList();
                d.Results.JourneyPlanBefore = Instantiate(d.Results.JourneyPlanBefore);
                d.Results.JourneyPlanAfter = Instantiate(d.Results.JourneyPlanAfter);
                d.Results.RecoveryPlanBefore = Instantiate(d.Results.RecoveryPlanBefore);
                d.Results.RecoveryPlanAfter = Instantiate(d.Results.RecoveryPlanAfter);
                if (d.Results.EFRO != null) d.Results.EFRO.ActiveTransferValuesPayable = Instantiate(d.Results.EFRO.ActiveTransferValuesPayable);
                if (d.Results.EFRO != null) d.Results.EFRO.DeferredTransferValuesPayable = Instantiate(d.Results.EFRO.DeferredTransferValuesPayable);
                d.Results.ProfitLossForecast = Instantiate(d.Results.ProfitLossForecast);
                d.Results.VarFunnel = Instantiate(d.Results.VarFunnel);
                if (d.Results.AssetHedgeBreakdownData != null)
                {
                    d.Results.AssetHedgeBreakdownData = d.Results.AssetHedgeBreakdownData.Select(bd => new TrackerHedgeBreakdownData
                    {
                        Date = bd.Date,
                        CurrentBasis = bd.CurrentBasis.Select(x => new HedgeTypeDictionary(x.Key, new TrackerHedgeBreakdownItem(x.Value.PhysicalAssets, x.Value.Buyin, x.Value.LDIOverlay, x.Value.Total))).ToList(),
                        Cashflows = bd.Cashflows.Select(x => new HedgeTypeDictionary(x.Key, new TrackerHedgeBreakdownItem(x.Value.PhysicalAssets, x.Value.Buyin, x.Value.LDIOverlay, x.Value.Total))).ToList()
                    })
                        .ToList();
                }
                if (d.Results.HedgeBreakdown != null)
                {
                    d.Results.HedgeBreakdown = new TrackerHedgeBreakdownData
                    {
                        Date = d.Results.HedgeBreakdown.Date,
                        CurrentBasis = d.Results.HedgeBreakdown.CurrentBasis.Select(x => new HedgeTypeDictionary(x.Key, new TrackerHedgeBreakdownItem(x.Value.PhysicalAssets, x.Value.Buyin, x.Value.LDIOverlay, x.Value.Total))).ToList(),
                        Cashflows = d.Results.HedgeBreakdown.Cashflows.Select(x => new HedgeTypeDictionary(x.Key, new TrackerHedgeBreakdownItem(x.Value.PhysicalAssets, x.Value.Buyin, x.Value.LDIOverlay, x.Value.Total))).ToList()
                    };
                }
                d.Results.GrowthOverPeriodInfo = Instantiate(d.Results.GrowthOverPeriodInfo);
                d.Results.GiltSpotRates = Instantiate(d.Results.GiltSpotRates);

                //accounting
                if (d.Results.IAS19DisclosureBefore != null) d.Results.IAS19DisclosureBefore.ToString();
                if (d.Results.IAS19DisclosureAfter != null) d.Results.IAS19DisclosureAfter.ToString();
                if (d.Results.FRS17DisclosureBefore != null) d.Results.FRS17DisclosureBefore.ToString();
                if (d.Results.FRS17DisclosureAfter != null) d.Results.FRS17DisclosureAfter.ToString();
                if (d.Results.USGAAPDisclosureBefore != null) d.Results.USGAAPDisclosureBefore.ToString();
                if (d.Results.USGAAPDisclosureAfter != null) d.Results.USGAAPDisclosureAfter.ToString();
                d.Results.USGAAPExpectedReturns = Instantiate(d.Results.USGAAPExpectedReturns);
                d.Results.USGAAPTable = d.Results.USGAAPTable == null ? null :
                    d.Results.USGAAPTable.Select(x => new TrackerAOCIDataItem
                    {
                        Date = x.Date,
                        AOCIData = new AOCIData(x.AOCIData.AOCI, x.AOCIData.UnrecognisedPriorServiceCost, x.AOCIData.AmortisationOfPriorServiceCost, x.AOCIData.ActuarialGainAmortisation)
                    })
                        .ToList();

                //jp
                d.Results.NewJP = d.Results.NewJP == null ? null : new TrackerNewJPData
                {
                    Liab1 = Instantiate(d.Results.NewJP.Liab1),
                    Liab2 = Instantiate(d.Results.NewJP.Liab2),
                    Liab3 = Instantiate(d.Results.NewJP.Liab3),
                    Assets1 = Instantiate(d.Results.NewJP.Assets1),
                    Assets2 = Instantiate(d.Results.NewJP.Assets2),
                    FundingPremiumPre = d.Results.NewJP.FundingPremiumPre,
                    FundingPremiumPost = d.Results.NewJP.FundingPremiumPost,
                    SelfSufficiencyPremiumPre = d.Results.NewJP.SelfSufficiencyPremiumPre,
                    SelfSufficiencyPremiumPost = d.Results.NewJP.SelfSufficiencyPremiumPost,
                    BuyoutPremiumPost = d.Results.NewJP.BuyoutPremiumPost,
                    BuyoutPremiumPre = d.Results.NewJP.BuyoutPremiumPre
                };

                //scheme
                d.Results.CompositeIndexParameters = d.Results.CompositeIndexParameters == null ? null : d.Results.CompositeIndexParameters.Select(x => new TrackerCompositeIndexData
                {
                    CiName = x.CiName,
                    CustomIndexType = x.CustomIndexType,
                    Rounding = x.Rounding,
                    SourceIndexProportion = Instantiate(x.SourceIndexProportion)
                })
                    .ToList();
                d.Results.CustomIndicesEvolution = d.Results.CustomIndicesEvolution == null ? null : d.Results.CustomIndicesEvolution.Select(x => new TrackerIndexEvolution
                {
                    Name = x.Name,
                    Values = Instantiate(x.Values)
                })
                    .ToList();
            }

            return d;
        }

        private IList<T> Instantiate<T>(IList<T> list) where T : class
        {
            return list == null ? null : list.ToList();
        }
    }
}
