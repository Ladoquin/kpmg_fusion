﻿namespace FlightDeck.Tracker.Reader
{
    using FlexCel.XlsAdapter;
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Models;
    using System;
    using System.Collections.Generic;

    public class TrackerAssumptionsReader
    {
        private XlsFile xls;
        private XlHelper xl;

        public TrackerAssumptionsReader(string trackerPath)
            : this(new XlsFile(trackerPath, false))
        {
        }

        public TrackerAssumptionsReader(XlsFile xls)
        {
            this.xls = xls;
            this.xl = new XlHelper(xls);
        }

        public TrackerAssumptions Read()
        {
            xls.ActiveSheetByName = "Panels";

            const string c = "C";
            var r = 13;
            Func<double> getData = () => { r++; return xl.GetDouble(r, c); };
            var clientLiabilityAssumptions = new Dictionary<AssumptionType, double>
                {
                    { AssumptionType.PreRetirementDiscountRate, getData() },
                    { AssumptionType.PostRetirementDiscountRate, getData() },
                    { AssumptionType.PensionDiscountRate, getData() },
                    { AssumptionType.SalaryIncrease, getData() },
                    { AssumptionType.InflationRetailPriceIndex, getData() },
                    { AssumptionType.InflationConsumerPriceIndex, getData() }
                };
            var clientLifeExpectancyAssumptions = new LifeExpectancyAssumptions(getData(), getData());
            var userPinc1Coords = xl.GetRangeCoords("UserPinc1");
            var clientPensionIncreases = new Dictionary<int, double>
                {
                    { 1, xl.GetDouble(userPinc1Coords.Row, userPinc1Coords.Col) },
                    { 2, xl.GetDouble(userPinc1Coords.Row + 1, userPinc1Coords.Col) },
                    { 3, xl.GetDouble(userPinc1Coords.Row + 2, userPinc1Coords.Col) },
                    { 4, xl.GetDouble(userPinc1Coords.Row + 3, userPinc1Coords.Col) },
                    { 5, xl.GetDouble(userPinc1Coords.Row + 4, userPinc1Coords.Col) },
                    { 6, xl.GetDouble(userPinc1Coords.Row + 5, userPinc1Coords.Col) },
                };

            var investmentGrowth = xl.GetDouble(20, "S", 0);
            var discountRate = xl.GetDouble(21, "S", 0);
            var recoveryPlanAssumptions = new TrackerRecoveryPlanAssumptions(investmentGrowth, discountRate,
                xl.GetDouble(22, "W"), 100000000, xl.GetDouble(21, "W"), Math.Abs(investmentGrowth) > Utils.Precision, (int)xl.GetDouble(19, "W"), (int)xl.GetDouble(20, "W"));

            var allocations = GetUserInvestmentStrategyAllocations();
            var clientAssetAssumptions = GetUserAssetReturns();

            double fbrate;
            var fbtype = GetBenefitAdjustment((int)xl.GetDouble("FBOptionNo"), out fbrate);

            var schemeAssumptions = new TrackerSchemeAssumptions
                (
                    xl.GetString("FROType").ToLower() == "bulk" ? FROType.Bulk : FROType.Embedded, //TODO
                    new FlexibleReturnOptions(xl.GetDouble(14, "AJ", 0), xl.GetDouble(16, "AJ")),
                    new EmbeddedFlexibleReturnOptions(xl.GetDouble("EFROTakeUpRate"), xl.GetDouble("EFROBasisChange")),
                    new EnhancedTransferValueOptions(xl.GetDouble(14, "AR", 0), xl.GetDouble(16, "AR"), xl.GetDouble(18, "AR")),
                    new PieOptions(xl.GetDouble(19, "BA"), xl.GetDouble(27, "BA"), xl.GetDouble(30, "BA")),
                    new FutureBenefitsParameters(xl.GetDouble("FBExtraEeConts"), fbtype, fbrate),
                    allocations,
                    new InvestmentStrategyOptions(xl.GetDouble("CashInjection", 0), xl.GetDouble("UserPropSyntheticCredit"), xl.GetDouble("UserPropSyntheticEquity"), xl.GetBool("LDIInForceAtAnalysisDate", "Panels"), xl.GetDouble("UserInterestHedge"), xl.GetDouble("UserInflationHedge")),
                    new AbfOptions(xl.GetDouble("ABFValue", 0)),
                    new InsuranceParameters(xl.GetDouble("InsPenProp"), xl.GetDouble("InsNonPenProp")),
                    ParseRecoveryPlanType(),
                    new WaterfallOptions(xl.GetDouble("CI", "VaRCalcs"), (int)xl.GetDouble("Time", "VaRCalcs"))
                );

            var basisAssumptions = new TrackerBasisAssumptions
                (
                    clientLiabilityAssumptions,
                    clientLifeExpectancyAssumptions,
                    clientPensionIncreases,
                    clientAssetAssumptions,
                    recoveryPlanAssumptions
                );

            var journeyPlanAssumptions = GetClientJourneyPlanOptions();

            var storedUserAssumptions = GetStoredUserAssumptions();

            return new TrackerAssumptions(schemeAssumptions, basisAssumptions, journeyPlanAssumptions) { StoredUserAssumptions = storedUserAssumptions };
        }

        private Dictionary<AssetClassType, double> GetUserInvestmentStrategyAllocations()
        {
            var dic = new Dictionary<AssetClassType, double>(10);
            xls.ActiveSheetByName = "Panels";
            var coords = xl.GetRangeCoords("UserAssetAllocation");
            for (int i = coords.Row; i <= coords.EndRow; i++)
            {
                if (xls.GetCellValue(i, coords.Col) != null)
                {
                    var assetClassType = Utils.MapAssetClassNameToType(xl.GetString(i, coords.Col));

                    if (assetClassType.HasValue)
                    {
                        dic.Add(assetClassType.Value, xl.GetDouble(i, coords.Col + 1));
                    }
                }
            }
            return dic;
        }

        private Dictionary<AssetClassType, double> GetUserAssetReturns()
        {
            var dic = new Dictionary<AssetClassType, double>(10);
            xls.ActiveSheetByName = "Panels";
            var coords = xl.GetRangeCoords("JPExpReturns");
            for (int i = coords.Row; i <= coords.EndRow; i++)
            {
                dic.Add(Utils.MapAssetClassNameToType(xl.GetString(i, coords.Col)).Value, xl.GetDouble(i, coords.Col + 1));
            }

            return dic;
        }

        private RecoveryPlanType ParseRecoveryPlanType()
        {
            var rp = xl.GetString(24, 23);
            if (rp.Equals("Current plan", StringComparison.InvariantCultureIgnoreCase))
                return RecoveryPlanType.Current;
            return rp.Equals("New plan", StringComparison.InvariantCultureIgnoreCase) ?
                RecoveryPlanType.NewDynamic : RecoveryPlanType.NewFixed;
        }

        private BenefitAdjustmentType GetBenefitAdjustment(int t, out double rate)
        {
            const string column = "BJ";
            const int row = 20;
            Func<double> getRate = () => xl.GetDouble(row + t, column);
            switch (t)
            {
                case 1:
                    rate = 0;
                    return BenefitAdjustmentType.None;
                case 2:
                    rate = getRate();
                    return BenefitAdjustmentType.BenefitReduction;
                case 3:
                    rate = getRate();
                    return BenefitAdjustmentType.SalaryLimit;
                case 4:
                    rate = getRate();
                    return BenefitAdjustmentType.CareRevaluation;
                case 5:
                    rate = getRate();
                    return BenefitAdjustmentType.DcImplementation;
            }
            throw new ArgumentException("Uknown benefit adjustment type");
        }

        private TrackerJourneyPlanOptions GetClientJourneyPlanOptions()
        {
            xls.ActiveSheetByName = "NewJP";

            var cfbasis = xl.GetString("CashflowBasis");
            var tpbasis = xls.GetCellValue(9, XlHelper.GetCol("B")) != null ? xl.GetString(9, XlHelper.GetCol("B")) : string.Empty;
            var ssbasis = xls.GetCellValue(10, XlHelper.GetCol("B")) != null ? xl.GetString(10, XlHelper.GetCol("B")) : string.Empty;
            var bobasis = xls.GetCellValue(11, XlHelper.GetCol("B")) != null ? xl.GetString(11, XlHelper.GetCol("B")) : string.Empty;

            var SalaryInflation = xl.GetString("SalaryInflation");
            var InvGrowthBefore = Double.Parse(xl.GetString("InvGrowthBefore").Replace("%", ""));
            var SelfSuffDiscountRate = Double.Parse(xl.GetString("SelfSuffDiscountRate").Replace("%", ""));
            var XCStartYear = xl.GetDouble("XCStartYear");
            var XCInitialConts = xl.GetDouble("XCInitialConts");
            var XCTerm = xl.GetDouble("XCTerm");
            var XCIncs = xl.GetDouble("XCIncs");
            var NewJPUseCurves = xl.GetBool("NewJPUseCurves", "NewJP");
            var NewJPIncludeBuyIn = xl.GetBool("NewJPIncludeBuyIn", "NewJP");
            var NewJPAddRP = xl.GetBool("NewJPAddRP", "NewJP");
            var NewJPAddXC = xl.GetBool("NewJPAddXC", "NewJP");
            var TriggerType = xl.GetString("TriggerType");
            var TriggerSteps = xl.GetDouble("TriggerSteps");
            var Trigger1Time = xl.GetDouble("Trigger1Time");
            var TriggerTransitionPeriod = xl.GetDouble("TriggerTransitionPeriod");

            var InvGrowthAfterCoords = xl.GetRangeCoords("InvGrowthAfter");
            var initialGrowthAfter = xl.GetDouble(InvGrowthAfterCoords.Row, InvGrowthAfterCoords.Col);
            var targetGrowthAfter = xl.GetDouble(InvGrowthAfterCoords.Row + 1, InvGrowthAfterCoords.Col);

            var assumptions = new TrackerJourneyPlanOptions
            {
                FundingBasisName = tpbasis,
                SelfSufficiencyBasisName = ssbasis,
                BuyoutBasisName = bobasis,
                SalaryInflationType = (SalaryType)Enum.Parse(typeof(SalaryType), SalaryInflation),
                CurrentReturn = InvGrowthBefore,
                SelfSufficiencyDiscountRate = SelfSuffDiscountRate,
                RecoveryPlan = new NewJPExtraContributionOptions((int)XCStartYear, XCInitialConts, (int)XCTerm, XCIncs, NewJPAddRP, NewJPAddXC),
                TriggerSteps = (int)TriggerSteps,
                TriggerTimeStartYear = (int)Trigger1Time,
                TriggerTransitionPeriod = (int)TriggerTransitionPeriod,                
                UseCurves = NewJPUseCurves,
                IncludeBuyIns = NewJPIncludeBuyIn,
                InitialReturn = initialGrowthAfter,
                FinalReturn = targetGrowthAfter
            };

            return assumptions;
        }

        private IList<BasisAssumptionsDictionary> GetStoredUserAssumptions()
        {
            var userAssumptionsTable = new List<BasisAssumptionsDictionary>();
            xls.ActiveSheetByName = "CALCS";
            var userAssumptionsTableRange = xl.GetRangeCoords("UserAssumptionsTable");
            var row = userAssumptionsTableRange.Row;
            var col = userAssumptionsTableRange.Col;
            while (xls.GetCellValue(row, ++col) != null)
            {
                var basisName = xl.GetString(row, col);
                var assumptions = new TrackerBasisAssumptions
                    (
                        new Dictionary<AssumptionType, double>
                            {
                                { AssumptionType.PreRetirementDiscountRate, xl.GetDouble(row + 1, col) },
                                { AssumptionType.PostRetirementDiscountRate, xl.GetDouble(row + 2, col) },
                                { AssumptionType.PensionDiscountRate, xl.GetDouble(row + 3, col) },
                                { AssumptionType.SalaryIncrease, xl.GetDouble(row + 4, col) },
                                { AssumptionType.InflationRetailPriceIndex, xl.GetDouble(row + 5, col) },
                                { AssumptionType.InflationConsumerPriceIndex, xl.GetDouble(row + 6, col) }
                            },
                        new LifeExpectancyAssumptions(xl.GetDouble(row + 7, col), xl.GetDouble(row + 8, col)),
                        new Dictionary<int, double>
                            {
                                { 1, xl.GetDouble(row + 10, col) },
                                { 2, xl.GetDouble(row + 11, col) },
                                { 3, xl.GetDouble(row + 12, col) },
                                { 4, xl.GetDouble(row + 13, col) },
                                { 5, xl.GetDouble(row + 14, col) },
                                { 6, xl.GetDouble(row + 15, col) }
                            },
                        new Dictionary<AssetClassType, double>(),
                        new TrackerRecoveryPlanAssumptions()
                    );
                userAssumptionsTable.Add(new BasisAssumptionsDictionary(basisName, assumptions));
            };

            return userAssumptionsTable;
        }
    }
}
