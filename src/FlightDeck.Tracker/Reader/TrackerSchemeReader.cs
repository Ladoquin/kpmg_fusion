﻿namespace FlightDeck.Tracker.Reader
{
    using FlexCel.XlsAdapter;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Assets;
    using FlightDeck.Domain.LDI;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Db;
    using Moq;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TrackerSchemeReader
    {
        private XlsFile xls;
        private XlHelper xl;
        private IIndexReader indices;       
        private Dictionary<AssetClassType, double> DefaultVolatilities;
        private Dictionary<AssetClassType, double> Volatilities;
        private Dictionary<AssetClassType, IDictionary<AssetClassType, double>> CorrelationMatrix;
        private Dictionary<string, int> masterBasisMockIds = new Dictionary<string, int>();

        public XlsFile XlsInstance { get { return xls; } }

        public TrackerSchemeReader(string trackerPath, IIndexReader indexReader)
            : this(new XlsFile(trackerPath, false), indexReader)
        {
        }

        public TrackerSchemeReader(XlsFile xls, IIndexReader indexReader)
        {
            this.xls = xls;
            this.xl = new XlHelper(xls);
            this.indices = indexReader;
        }

        public PensionScheme ReadScheme()
        {
            GetDefaultVolatilities();
            GetVolatilities();
            GetCorrelationMatrix();

            var schemeName = GetSchemeName();

            var bases = GetBases();
            var schemeAssets = GetSchemeAssets();
            var contributions = GetContributions();
            var recoveryPlan = GetRecoveryPlan();
            var accountingParams = GetAccountingParams();

            xls.ActiveSheetByName = "Contributions";
            var ReturnOnAssets = xl.GetDouble(28, "B");
            var IsGrowthFixed = xl.GetString(25, 2).ToLower().StartsWith("fixed rate");

            var COutContRate = .0;
            xls.ActiveSheetByName = "Main";
            var IsFunded = xl.GetBool("IsFunded");
            double d;
            if (xl.TryGetDouble("COutContRate", out d, "Main"))
                COutContRate = d;
            else
                COutContRate = 0.03;

            var Ias19ExpenseLoading = xl.GetDouble("IAS19ExpenseLoading");

            var schemeDataSource = new SchemeDataSource(DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);

            var recoveryPlanAssumptions = GetRecoveryPlanAssumptions();

            var namedPropertyGroups = new Dictionary<NamedPropertyGroupType, NamedPropertyGroup>
            {
                {NamedPropertyGroupType.RecoveryPlan, new NamedPropertyGroup(NamedPropertyGroupType.RecoveryPlan, "RP", 
                    new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>(NamedRecoveryPlanPropertyTypes.MaxLumpSum.ToString(), recoveryPlanAssumptions.MaxLumpSumPayment.ToString()),
                        new KeyValuePair<string, string>(NamedRecoveryPlanPropertyTypes.AnnualIncs.ToString(), recoveryPlanAssumptions.Increases.ToString()),
                        new KeyValuePair<string, string>(NamedRecoveryPlanPropertyTypes.Length.ToString(), recoveryPlanAssumptions.Term.ToString())
                    })}
            };

            xls.ActiveSheetByName = "Main";
            var AssetGrowthIndices = xl.GetRangeCoords("AssetGrowthIndices");
            var i = 0;
            var assetsGrowthNamedProperties = new List<KeyValuePair<string, string>>();
            while (xls.GetCellValue(AssetGrowthIndices.Row + i, AssetGrowthIndices.Col) != null)
            {
                assetsGrowthNamedProperties.Add(new KeyValuePair<string, string>(xl.GetString(AssetGrowthIndices.Row + i, AssetGrowthIndices.Col), xl.GetString(AssetGrowthIndices.Row + i, AssetGrowthIndices.Col + 1)));
                i++;
            }
            namedPropertyGroups.Add(NamedPropertyGroupType.AssetGrowth, new NamedPropertyGroup(NamedPropertyGroupType.AssetGrowth, NamedPropertyGroupType.AssetGrowth.DisplayName(), assetsGrowthNamedProperties));

            xls.ActiveSheetByName = "VolatilityAssumptions";
            var giltDurationDefault = (int)xl.GetDouble("DefaultGiltDuration");
            var corpDurationDefault = (int)xl.GetDouble("DefaultCorpDuration");
            var applicationParameters = new PensionSchemeApplicationParameters(0.002, 0.5, 1.2, 0.5, 1.2, giltDurationDefault, corpDurationDefault);
            xls.ActiveSheetByName = "Main";
            var LDIDiscRateIndex = xl.GetString("LDIDiscRateIndex");
            var LDIInflationIndex = xl.GetString("LDIInflationIndex");
            var LDIOverrideAssumptions = xl.GetBool("LDIOverrideAssumptions", "Main");
            var LDIBasis = new LDICashflowBasis
                (
                    xl.GetString("LDIBasis"),
                    LDIOverrideAssumptions,
                    LDIOverrideAssumptions ? (string.IsNullOrEmpty(LDIDiscRateIndex) ? null : indices.Get(LDIDiscRateIndex)) : null,
                    LDIOverrideAssumptions ? xl.GetDouble("LDIDiscRatePremium", 0) : 0,
                    LDIOverrideAssumptions ? (string.IsNullOrEmpty(LDIInflationIndex) ? null : indices.Get(LDIInflationIndex)) : null,
                    LDIOverrideAssumptions ? xl.GetDouble("LDIInflationGap", 0) : 0
                );
            var SyntheticEquityIncomeIndex = xl.GetString("SyntheticEquityIncomeIndex");
            var SyntheticEquityOutgoIndex = xl.GetString("SyntheticEquityOutgoIndex");
            var SyntheticCreditIncomeIndex = xl.GetString("SyntheticCreditIncomeIndex");
            var SyntheticCreditOutgoIndex = xl.GetString("SyntheticCreditOutgoIndex");
            var syntheticAssets = new SyntheticAssetIndices
                (
                    string.IsNullOrEmpty(SyntheticEquityIncomeIndex) ? null : indices.Get(SyntheticEquityIncomeIndex),
                    string.IsNullOrEmpty(SyntheticEquityOutgoIndex) ? null : indices.Get(SyntheticEquityOutgoIndex),
                    string.IsNullOrEmpty(SyntheticCreditIncomeIndex) ? null : indices.Get(SyntheticCreditIncomeIndex),
                    string.IsNullOrEmpty(SyntheticCreditOutgoIndex) ? null : indices.Get(SyntheticCreditOutgoIndex)
                );

            var indexRepoMock = new Mock<IFinancialIndexRepository>();
            indexRepoMock.Setup(x => x.GetByName(It.IsAny<string>())).Returns((string name) => indices.Get(name));

            var assetsGrowthProvider = new AssetsGrowthProvider(indexRepoMock.Object);

            var pensionScheme = new PensionScheme(1, schemeName, "Ref",
                bases, schemeAssets,
                contributions, recoveryPlan, schemeDataSource,
                Volatilities, DefaultVolatilities, CorrelationMatrix, namedPropertyGroups, null, assetsGrowthProvider, ReturnOnAssets,
                IsGrowthFixed, Ias19ExpenseLoading,
                accountingParams.AccountingDefaultStandard, accountingParams.AccountingIAS19Visible, accountingParams.AccountingFRS17Visible, accountingParams.AccountingUSGAAPVisible,
                accountingParams.ABOSalaryType, applicationParameters, null, COutContRate, LDIBasis, syntheticAssets, IsFunded,
                null);

            return pensionScheme;
        }

        public string ReadSelectedBasisName()
        {
            xls.ActiveSheetByName = "CALCS";
            var basisName = xl.GetString("ChosenBasis");
            return basisName;
        }

        public BasisType ReadSelectedBasisType()
        {
            xls.ActiveSheetByName = "CALCS";
            var basisType = TrackerReaderUtils.ParseBasisType(xl.GetString("ChosenBasisType"));
            return basisType;
        }

        public DateTime ReadAttributionStartDate()
        {
            xls.ActiveSheetByName = "Tracker";
            return xl.GetDate("AttrStartDate");
        }

        public DateTime ReadAttributionEndDate()
        {
            xls.ActiveSheetByName = "Tracker";
            return xl.GetDate("AttrEndDate");
        }

        private string GetSchemeName()
        {
            xls.ActiveSheetByName = "Main";
            var coords = xl.GetRangeCoords("NewSchemeName");
            return xl.GetString(coords.Row, coords.Col);
        }

        private IList<Basis> GetBases()
        {
            xls.ActiveSheetByName = "JourneyPlan";
            var giltIndex = indices.Get(xl.GetString(49, 3));

            xls.ActiveSheetByName = "Main";
            var volatility = xl.GetDouble("InflationVol");

            var increaseDefinitions = GetIncreaseDefinitions();

            var result = new List<Basis>();
            for (var i = 1; i <= xls.SheetCount; i++)
            {
                var sheet = xls.GetSheetName(i);
                if (!sheet.ToLower().StartsWith("liab"))
                    continue;

                xls.ActiveSheetByName = sheet;
                var t = TrackerReaderUtils.ParseBasisType(xl.GetString(6, 2));

                var name = xl.GetString(7, XlHelper.GetCol("B"));
                var anchorDate = xl.GetDate(8, "B");
                var lastPensionIncrease = xl.GetDate(9, "B");
                var linkedToCorpBonds = xl.GetBool("LinkedToCorpBonds", sheet);

                var simpleAssumptions = GetSimpleAssumptions();
                var salaryProgressions = GetSalaryProgression();

                var liabIncreases = GetLiabIncreases();

                var cashflows = GetCashFlows(sheet);

                var assumptions = new List<FinancialAssumption>();
                var pensionIncreases = new List<PensionIncreaseAssumption>();
                GetAssumptions(assumptions, pensionIncreases, volatility, increaseDefinitions);

                var aoci = xl.GetDouble("USGAAP_AOCI", 0);
                var unrecognisedPriorServiceCost = xl.GetDouble("USGAAP_UnrecognisedPriorService", 0);
                var amortisationPriorServiceCost = xl.GetDouble("USGAAP_AmortisationOfPriorService", 0);
                var amortisationPeriodForActGain = xl.GetDouble("USGAAP_AmortisationPeriodForActGain", (int?)null);

                if (!masterBasisMockIds.ContainsKey(name))
                    masterBasisMockIds.Add(name, masterBasisMockIds.Any() ? masterBasisMockIds.Values.Max() + 1 : 1);
                var mockMasterBasisId = masterBasisMockIds[name];
                var bondYield = new BondYield();//todo: populate (from v1)

                var ab = new Basis(1, 1, mockMasterBasisId, name, t, anchorDate, lastPensionIncrease, linkedToCorpBonds, salaryProgressions, cashflows,
                    assumptions.ToDictionary(x => x.Type, x => x.InitialValue),
                    pensionIncreases.ToDictionary(x => x.Id, x => x.InitialValue),
                    liabIncreases, bondYield, aoci, unrecognisedPriorServiceCost, amortisationPriorServiceCost, (int)amortisationPeriodForActGain.GetValueOrDefault(), simpleAssumptions, giltIndex.Id,
                    assumptions.ToDictionary(x => x.Type),
                    pensionIncreases.ToDictionary(x => x.Id, x => x),
                    giltIndex);
                result.Add(ab);
            }
            return result;
        }

        private FinancialAssumption GetAssumption(int id, AssumptionType type)
        {
            var idx = xl.GetRangeCoords("AssumptionsTable").Row + id - 1;
            return new FinancialAssumption(id, 1, xl.GetDouble(idx, "D"), indices.Get(xl.GetString(idx, 5)), type, string.Empty);
        }

        private PensionIncreaseAssumption GetPensionIncrease(int id, IList<dynamic> definitions, double volatility)
        {
            var penIncsValuesRow = xl.GetRangeCoords("AssumptionsTable").Row + 6;

            var idx = penIncsValuesRow + id - 1;
            var category = xl.GetString(idx, 3).ToLower() == "fixed"
                ? AssumptionCategory.Fixed
                : AssumptionCategory.BlackScholes;
            var isVisible = xl.GetBool(idx, 9);
            var def = definitions[id - 1];

            InflationType inflationType;
            var inflationindex = def.Index.ToLower();
            if (inflationindex == "rpi")
                inflationType = InflationType.Rpi;
            else if (inflationindex == "cpi")
                inflationType = InflationType.Cpi;
            else inflationType = InflationType.None;

            var index = inflationType == InflationType.None ? null : indices.Get(xl.GetString(idx, 5));

            return new PensionIncreaseAssumption(id, 1, xl.GetDouble(idx, "D"), index, id, def.Label, def.Minimum,
                def.Maximum, def.Increase, category, volatility, inflationType, def.IsFixed, isVisible);
        }

        private List<dynamic> GetIncreaseDefinitions()
        {
            xls.ActiveSheetByName = "Main";
            var result = new List<dynamic>();
            const int offset = 40;
            const int labelIdx = 3;
            double d;
            for (var i = offset; i < offset + 6; i++)
            {
                var label = xl.GetString(i, labelIdx);
                result.Add(new
                {
                    Label = label,
                    Index = xl.GetString(i, 4),
                    Minimum = xl.GetDouble(i, "E", 0),
                    Maximum = xl.GetDouble(i, "F", 0),
                    Increase = xl.GetDouble(i, "G", 0),
                    IsFixed = xl.TryGetDouble(i, 7, out d)
                });
            }
            return result;
        }

        private IDictionary<SimpleAssumptionType, SimpleAssumption> GetSimpleAssumptions()
        {
            var simpleAssumptions = new Dictionary<SimpleAssumptionType, SimpleAssumption>();
            var t = SimpleAssumptionType.LifeExpectancy65;
            simpleAssumptions.Add(t, new SimpleAssumption(t, xl.GetDouble("LifeExp65"), "Pensioner", true));
            t = SimpleAssumptionType.LifeExpectancy65_45;
            simpleAssumptions.Add(t, new SimpleAssumption(t, xl.GetDouble("LifeExp65_45"), "Act/Def", true));

            return simpleAssumptions;
        }

        private List<SalaryProgression> GetSalaryProgression()
        {
            var salaryProgressions = new List<SalaryProgression>();

            var salaryProgressionRange = xl.GetRangeCoords("SalaryProgression");

            var rowOffset = salaryProgressionRange.Row - 1;

            var col = "A";
            for (var c = 2; xls.GetCellValue(rowOffset + 1, c) != null; c++)
            {
                col = XlHelper.IncrementCol(col);
                var salaryRoll = new Dictionary<int, double>();
                for (var r = 11; xls.GetCellValue(rowOffset + r, c) != null; r++)
                {
                    salaryRoll.Add(r - 10, xl.GetDouble(rowOffset + r, col));
                }
                salaryProgressions.Add(new SalaryProgression(c - 1, 1,
                    (int)xl.GetDouble(rowOffset + 1, col),
                    (int)xl.GetDouble(rowOffset + 2, col),
                    (int)xl.GetDouble(rowOffset + 3, col), salaryRoll));
            }
            return salaryProgressions;
        }

        private IDictionary<int, double> GetLiabIncreases()
        {
            var result = new Dictionary<int, double>();

            var mortAdjRange = xl.GetRangeCoords("MortAdj");

            for (var row = mortAdjRange.Row; row <= mortAdjRange.EndRow; row++)
            {
                result.Add((int)xl.GetDouble(row, "K"), xl.GetDouble(row, "L"));
            }
            return result;
        }

        private IEnumerable<CashflowData> GetCashFlows(string sheetName)
        {
            var cashflowData = new List<CashflowData>();

            var cashflowRange = xl.GetRangeCoords("LiabCashflows");

            var offset = cashflowRange.Row - 1;

            return TrackerReaderUtils.ParseCashflowRange(xls, sheetName, cashflowRange.Col, offset);
        }

        private void GetAssumptions(ICollection<FinancialAssumption> assumptions, ICollection<PensionIncreaseAssumption> pensionIncreases, double volatility, IList<dynamic> increaseDefinitions)
        {
            assumptions.Add(GetAssumption(1, AssumptionType.PreRetirementDiscountRate));
            assumptions.Add(GetAssumption(2, AssumptionType.PostRetirementDiscountRate));
            assumptions.Add(GetAssumption(3, AssumptionType.PensionDiscountRate));
            assumptions.Add(GetAssumption(4, AssumptionType.SalaryIncrease));
            assumptions.Add(GetAssumption(5, AssumptionType.InflationRetailPriceIndex));
            assumptions.Add(GetAssumption(6, AssumptionType.InflationConsumerPriceIndex));

            for (var i = 1; i < 7; i++)
            {
                pensionIncreases.Add(GetPensionIncrease(i, increaseDefinitions, volatility));
            }
        }

        private List<SchemeAsset> GetSchemeAssets()
        {
            var schemeAssets = new List<SchemeAsset>();            

            var defaultGiltDuration = (int)xl.GetDouble("DefaultGiltDuration", "VolatilityAssumptions");
            var defaultCorpDuration = (int)xl.GetDouble("DefaultCorpDuration", "VolatilityAssumptions");

            for (var i = 1; i <= xls.SheetCount; i++)
            {
                var sheet = xls.GetSheetName(i);
                if (!sheet.ToLower().StartsWith("assets"))
                    continue;

                xls.ActiveSheetByName = sheet;

                var assetClassData = GetAssetAllocationDataForAssetSheet(sheet);

                var assetFunds = GetAssetFunds(sheet);

                var assets = assetFunds
                    .GroupBy(x => x.AssetClass.Type)
                    .Select(x => new Asset(0, i, assetClassData[x.Key].Class, assetClassData[x.Key].IncludeInIas, assetClassData[x.Key].DefaultReturn, x));

                schemeAssets.Add(new SchemeAsset(i, 1, xl.GetDate("EffectiveDate"), xl.GetBool("LDIInForce"), xl.GetString("LDIFund"),
                        xl.GetDouble("InterestHedge", 0), xl.GetDouble("InflationHedge", 0), xl.GetDouble("PropSyntheticEquity", 0), xl.GetDouble("PropSyntheticCredit", 0),
                        (int)xl.GetDouble("GiltDuration", 0), (int)xl.GetDouble("CorpDuration", 0), assets));
            }

            return schemeAssets;
        }

        private Dictionary<AssetClassType, Asset> GetAssetAllocationDataForAssetSheet(string sheetName)
        {
            var assetClassData = new Dictionary<AssetClassType, Asset>();

            xls.ActiveSheetByName = sheetName;

            var coords = xl.GetRangeCoords("AssetClassData");

            for (int row = coords.Row; row <= coords.EndRow; row++)
            {
                var name = xl.GetString(row, coords.Col);
                var assetClassType = Utils.MapAssetClassNameToType(name).GetValueOrDefault();
                var assetCategory = ParseCategory(xl.GetString(row, coords.Col + 1));
                var defaultReturn = xl.GetDouble(row, coords.Col + 2);
                var includeInIAS = xl.GetBool(row, coords.Col + 3);
                var amount = xl.GetDouble(row, coords.Col + 4);

                assetClassData.Add(assetClassType, new Asset(0, 0, new AssetClass((int)assetClassType, name, assetCategory, null, 0), includeInIAS, defaultReturn, null));
            }

            return assetClassData;
        }

        private List<AssetFund> GetAssetFunds(string sheetName)
        {
            var assetFunds = new List<AssetFund>();
            xls.ActiveSheetByName = sheetName;
            var coords = xl.GetRangeCoords("AssetAllocation");
            var ldiFund = xl.GetString("LDIFund");

            Func<AssetClassType, AssetClass> getAssetClass = type =>
            {
                return new AssetClass((int)type, type.ToString(), Utils.GetEnumAttribute<AssetClassType, AssetClassTypeCategoryAttribute>(type).Category, new AssetReturnSpec(0, 0, 0), DefaultVolatilities[type]);
            };

            for (int row = coords.Row; row <= coords.EndRow; row++)
            {
                if (xls.GetCellValue(row, coords.Col + 1) != null)
                {
                    var assetClassType = Utils.MapAssetClassNameToType(xl.GetString(row, coords.Col + 1)).GetValueOrDefault();

                    assetFunds.Add(new AssetFund(row, xl.GetString(row, coords.Col), 0, getAssetClass(assetClassType),
                                    indices.Get(xl.GetString(row, coords.Col + 2)), xl.GetDouble(row, coords.Col + 3), xl.GetString(row, coords.Col).Equals(ldiFund, StringComparison.OrdinalIgnoreCase)));
                }
            }

            return assetFunds;
        }

        private AssetCategory ParseCategory(string category)
        {
            if (category.Equals("growth", StringComparison.InvariantCultureIgnoreCase))
                return AssetCategory.Growth;
            if (category.Equals("matching", StringComparison.InvariantCultureIgnoreCase))
                return AssetCategory.Matched;
            return AssetCategory.Other;
        }

        private Dictionary<AssetClassType, double> GetClientAssetAssumptions()
        {
            return readAssetAssumptions("L", 15);
        }

        private Dictionary<AssetClassType, double> GetAssetAllocations()
        {
            return readAssetAssumptions("AD", 14);
        }

        private void GetCorrelationMatrix()
        {
            CorrelationMatrix = new Dictionary<AssetClassType, IDictionary<AssetClassType, double>>
                {
                    {
                        AssetClassType.Equity, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.35},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts, 0.35},
                            {AssetClassType.IndexLinkedGilts, 0.25},
                            {AssetClassType.Abf, 0.5},
                            {AssetClassType.CorporateBonds, 0.5},
                            {AssetClassType.Equity, 1},
                            {AssetClassType.DiversifiedGrowth, 0.8},
                            {AssetClassType.Property, 0.25},
                        }
                    },
                    {
                        AssetClassType.FixedInterestGilts, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.9},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  1},
                            {AssetClassType.IndexLinkedGilts,   0.75},
                            {AssetClassType.Abf,   0.75},
                            {AssetClassType.CorporateBonds,   0.75},
                            {AssetClassType.Equity,  0.35},
                            {AssetClassType.DiversifiedGrowth,  0.25},
                            {AssetClassType.Property,   0.20}
                        }
                    },
                    {
                        AssetClassType.CorporateBonds, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.75},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.75},
                            {AssetClassType.IndexLinkedGilts,   0.55},
                            {AssetClassType.Abf,   1},
                            {AssetClassType.CorporateBonds,   1},
                            {AssetClassType.Equity,  0.5},
                            {AssetClassType.DiversifiedGrowth,  0.4},
                            {AssetClassType.Property,   0.2},
                        }
                    },
                    {
                        AssetClassType.Property, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.15},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.2},
                            {AssetClassType.IndexLinkedGilts,   0.2},
                            {AssetClassType.Abf,   0.2},
                            {AssetClassType.CorporateBonds,   0.2},
                            {AssetClassType.Equity,  0.25},
                            {AssetClassType.DiversifiedGrowth,  0.2},
                            {AssetClassType.Property,   1},
                        }
                    },
                    {
                        AssetClassType.DiversifiedGrowth, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.15},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.25},
                            {AssetClassType.IndexLinkedGilts,   0.2},
                            {AssetClassType.Abf,   0.4},
                            {AssetClassType.CorporateBonds,   0.4},
                            {AssetClassType.Equity,  0.8},
                            {AssetClassType.DiversifiedGrowth,  1},
                            {AssetClassType.Property,   0.2},
                        }
                    },
                    {
                        AssetClassType.IndexLinkedGilts, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.9},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.75},
                            {AssetClassType.IndexLinkedGilts,   1},
                            {AssetClassType.Abf,   0.55},
                            {AssetClassType.CorporateBonds,   0.55},
                            {AssetClassType.Equity,  0.25},
                            {AssetClassType.DiversifiedGrowth,  0.2},
                            {AssetClassType.Property,   0.2},
                        }
                    },
                    {
                        AssetClassType.Abf, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0.75},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.75},
                            {AssetClassType.IndexLinkedGilts,   0.55},
                            {AssetClassType.Abf,   1},
                            {AssetClassType.CorporateBonds,   1},
                            {AssetClassType.Equity,  0.5},
                            {AssetClassType.DiversifiedGrowth,  0.4},
                            {AssetClassType.Property,   0.2},
                        }
                    },
                    {
                        AssetClassType.LiabilityInterest, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 1},
                            {AssetClassType.LiabilityLongevity, 0},
                            {AssetClassType.FixedInterestGilts,  0.9},
                            {AssetClassType.IndexLinkedGilts,   0.9},
                            {AssetClassType.Abf,   0.75},
                            {AssetClassType.CorporateBonds,       0.75},
                            {AssetClassType.Equity,  0.35},
                            {AssetClassType.DiversifiedGrowth,  0.15},
                            {AssetClassType.Property,   0.15},
                        }
                    },
                    {
                        AssetClassType.LiabilityLongevity, new Dictionary<AssetClassType, double>
                        {
                            {AssetClassType.LiabilityInterest, 0},
                            {AssetClassType.LiabilityLongevity, 1},
                            {AssetClassType.FixedInterestGilts,  0},
                            {AssetClassType.IndexLinkedGilts,   0},
                            {AssetClassType.Abf,   0},
                            {AssetClassType.CorporateBonds,   0},
                            {AssetClassType.Equity,  0},
                            {AssetClassType.DiversifiedGrowth,  0},
                            {AssetClassType.Property,   0},
                        }
                    },
                    {AssetClassType.Cash, null}
                };
        }

        private void GetVolatilities()
        {
            Volatilities = new Dictionary<AssetClassType, double>
                {
                    {AssetClassType.Equity,             0.2},
                    {AssetClassType.FixedInterestGilts, 0.09},
                    {AssetClassType.CorporateBonds,     0.105},
                    {AssetClassType.Property,           0.13},
                    {AssetClassType.DiversifiedGrowth,  0.125},
                    {AssetClassType.Cash,               0.0},
                    {AssetClassType.IndexLinkedGilts,   0.07},
                    {AssetClassType.Abf,                0.105},
                    {AssetClassType.LiabilityInterest, 0.09},
                    {AssetClassType.LiabilityLongevity, 0.035}
                };
        }

        private void GetDefaultVolatilities()
        {
            DefaultVolatilities = new Dictionary<AssetClassType, double>();

            xls.ActiveSheetByName = "VolatilityAssumptions";
            var coords = xl.GetRangeCoords("DefaultVolatilities");
            var r = coords.Row;
            var c = coords.Col;
            DefaultVolatilities.Add(AssetClassType.Equity, xl.GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.Property, xl.GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.DiversifiedGrowth, xl.GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.DiversifiedCredit, xl.GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.PrivateMarkets, xl.GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.CorporateBonds, xl.GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.FixedInterestGilts, xl.GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.IndexLinkedGilts, xl.GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.Cash, xl.GetDouble(r++, c));
            DefaultVolatilities.Add(AssetClassType.Abf, xl.GetDouble(r++, c));
        }

        private Dictionary<AssetClassType, double> readAssetAssumptions(string col, int row)
        {
            xls.ActiveSheetByName = "Panels";
            var assetAssumptions = new Dictionary<AssetClassType, double>();
            Action<AssetClassType> getData = type =>
            {
                assetAssumptions.Add(type, xl.GetDouble(row++, col));
            };
            getData(AssetClassType.Equity);
            getData(AssetClassType.Property);
            getData(AssetClassType.DiversifiedGrowth);
            getData(AssetClassType.DiversifiedCredit);
            getData(AssetClassType.PrivateMarkets);
            getData(AssetClassType.CorporateBonds);
            getData(AssetClassType.FixedInterestGilts);
            getData(AssetClassType.IndexLinkedGilts);
            getData(AssetClassType.Cash);
            getData(AssetClassType.Abf);

            return assetAssumptions;
        }

        private List<Contribution> GetContributions()
        {
            var contributionRates = new List<Contribution>();
            const int rowOffset = 74;
            xls.ActiveSheetByName = "Contributions";

            for (var r = 1; xls.GetCellValue(rowOffset + r, 2) != null; r++)
            {
                contributionRates.Add(new Contribution(1, xl.GetDateSlow(rowOffset + r, "B"), xl.GetDouble(rowOffset + r, "C", 0), xl.GetDouble(rowOffset + r, "D", 0)));
            }

            return contributionRates;
        }

        private List<RecoveryPayment> GetRecoveryPlan()
        {
            xls.ActiveSheetByName = "Contributions";
            var recoveryPlan = new List<RecoveryPayment>();
            for (var row = 40; xls.GetCellValue(row, 2) != null; row++)
            {
                recoveryPlan.Add(new RecoveryPayment(1, xl.GetDate(row, "B"), xl.GetDate(row, "C"), xl.GetDouble(row, "D"), (int)xl.GetDouble(row, "E")));
            }
            return recoveryPlan;
        }


        private dynamic GetAccountingParams()
        {
            xls.ActiveSheetByName = "Main";
            var AccountingDefaultStandard = new FlightDeck.DomainShared.AccountingStandardHelper().ParseAccountingStandardType(xl.GetString("DefaultStandard"));
            var AccountingIAS19Visible = string.Equals(xl.GetString("IAS19Visible"), "Yes", StringComparison.CurrentCultureIgnoreCase);
            var AccountingFRS17Visible = string.Equals(xl.GetString("FRS17Visible"), "Yes", StringComparison.CurrentCultureIgnoreCase);
            var AccountingUSGAAPVisible = string.Equals(xl.GetString("USGAAPVisible"), "Yes", StringComparison.CurrentCultureIgnoreCase);
            SalaryType ABOSalaryType = SalaryType.CPI;
            switch (xl.GetString("USGAAPSalaryOption").ToLower())
            {
                case "rpi": ABOSalaryType = FlightDeck.DomainShared.SalaryType.RPI; break;
                case "cpi": ABOSalaryType = FlightDeck.DomainShared.SalaryType.CPI; break;
                case "zero": ABOSalaryType = FlightDeck.DomainShared.SalaryType.Zero; break;
                default:
                    break;
            }

            return new
                {
                    AccountingDefaultStandard,
                    AccountingIAS19Visible,
                    AccountingFRS17Visible,
                    AccountingUSGAAPVisible,
                    ABOSalaryType
                };
        }

        private RecoveryPlanAssumptions GetRecoveryPlanAssumptions()
        {
            xls.ActiveSheetByName = "Panels";
            var investmentGrowth = xl.GetDouble(20, "S", 0);
            var discountRate = xl.GetDouble(21, "S", 0);
            var recoveryPlanAssumptions = new RecoveryPlanAssumptions(investmentGrowth, discountRate,
                xl.GetDouble(22, "W"), 100000000, xl.GetDouble(21, "W"), Math.Abs(investmentGrowth) > Utils.Precision, (int)xl.GetDouble(19, "W"), (int)xl.GetDouble(20, "W"));

            return recoveryPlanAssumptions;
        }
    }
}
