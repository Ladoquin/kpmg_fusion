﻿namespace FlightDeck.Tracker.Reader
{
    using FlexCel.XlsAdapter;
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class TrackerReaderUtils
    {
        public static BasisType ParseBasisType(string b)
        {
            switch (b.ToLower())
            {
                case "accounting":
                    return BasisType.Accounting;
                case "buyout":
                    return BasisType.Buyout;
                case "technical provisions":
                    return BasisType.TechnicalProvision;
                case "gilts":
                    return BasisType.Gilts;
                default:
                    throw new ApplicationException(string.Format("Unknown basis {0}", b));
            }
        }

        private static MemberStatus ParseMemberStatus(string statusString)
        {
            switch (statusString.ToLower())
            {
                case "act":
                    return MemberStatus.ActivePast;
                case "def":
                case "pup":
                    return MemberStatus.Deferred;
                case "pen":
                    return MemberStatus.Pensioner;
                case "fut":
                    return MemberStatus.ActiveFuture;
                default:
                    throw new ArgumentException(string.Format("Unable to parse {0}.", statusString));
            }
        }

        private static RevaluationType ParseRevaluationType(string value)
        {
            if (String.IsNullOrEmpty(value))
                return RevaluationType.None;
            if (value.ToLower().StartsWith("c"))
                return RevaluationType.Cpi;
            if (value.ToLower().StartsWith("r"))
                return RevaluationType.Rpi;
            return RevaluationType.Fixed;
        }

        public static IEnumerable<TrackerCashflowData> ParseCashflowRange(XlsFile xls, string sheetName, int col, int startRow, bool allowZeros = false)
        {
            var xl = new XlHelper(xls);

            xls.ActiveSheetByName = sheetName;

            while (xls.GetCellValue(startRow + 1, col) != null)
            {
                var status = ParseMemberStatus(xl.GetString(startRow + 1, col));
                var minAge = xl.GetDouble(startRow + 2, col);
                var maxAge = xl.GetDouble(startRow + 3, col);
                var s = xls.GetCellValue(startRow + 8, col) ?? string.Empty;
                var revaluation = ParseRevaluationType(s.ToString());
                var retirementAge = (int)xl.GetDouble(startRow + 4, col);
                var benefit = (xl.GetString(startRow + 5, col)).ToLower() == "pension"
                    ? BenefitType.Pension
                    : BenefitType.LumpSum;
                var pensionIncreaseId = (int?)xl.GetDouble(startRow + 6, col);
                if (benefit == BenefitType.LumpSum)
                    pensionIncreaseId = null;
                var buyinEntry = xl.GetStringWithDefault(startRow + 9, col);
                var isBuyin = buyinEntry.Length > 0 && buyinEntry.ToLower().Trim() != "false";

                var yearData = new Dictionary<int, double>();
                var emptyYears = new List<int>();
                var i = 1;
                for (var r = 15; xls.GetCellValue(startRow + r, col) != null; r++)
                {
                    var cash = xl.GetDouble(startRow + r, col);
                    if (Math.Abs(cash) > Utils.Precision || allowZeros)
                        yearData[i] = cash;
                    else
                        emptyYears.Add(i);
                    i++;
                }
                foreach (var emptyYear in emptyYears.Where(x => x > yearData.Keys.Min() && x < yearData.Keys.Max()))
                    yearData[emptyYear] = 0;

                var cashflows = yearData;

                yield return new TrackerCashflowData(col - 1, 1, status, minAge, maxAge, revaluation, retirementAge, benefit, cashflows, cashflows.Keys.Min(), cashflows.Keys.Max(), isBuyin, pensionIncreaseId);

                col++;
            }
        }
    }
}
