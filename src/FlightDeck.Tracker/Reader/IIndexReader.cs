﻿namespace FlightDeck.Tracker.Reader
{
    using FlightDeck.Domain;
    
    public interface IIndexReader
    {
        FinancialIndex Get(string name);
    }
}
