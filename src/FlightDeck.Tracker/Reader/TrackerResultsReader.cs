﻿namespace FlightDeck.Tracker.Reader
{
    using FlexCel.Core;
    using FlexCel.XlsAdapter;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Accounting;
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Db;
    using FlightDeck.Tracker.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TrackerResultsReader
    {
        private XlsFile xls;
        private XlHelper xl;
        private TrackerOutputs t;
        private IIndexReader indices;
        private IList<TrackerBasisSummary> basisSummaries;

        public TrackerResultsReader(string trackerPath, IIndexReader indexReader)
            : this(new XlsFile(trackerPath, false), indexReader)
        {
        }

        public TrackerResultsReader(XlsFile xls, IIndexReader indexReader)
        {
            this.xls = xls;
            this.xl = new XlHelper(xls);
            this.indices = indexReader;
        }

        public TrackerOutputs Read()
        {
            t = new TrackerOutputs();

            GetDates();
            GetBasisSummaries();
            GetLiabilityEvolution();
            GetLiabilityAssumptionEvolutionResults();
            GetLiabilityPensionIncreaseEvolutionResults();
            GetAssetEvolutionResults();
            GetExperienceResults();
            GetEvolutionSatelliteResults();
            GetAssetHedgeBreakdownData();
            GetExpectedDeficit();
            GetCashFlowOutputs();
            GetFundingLevel();
            GetBalanceEstimate();
            GetProfitLoss();
            GetProfitLossForecastResults();
            GetUserLiabs();
            GetSurplusAnalysis();
            GetGrowthOverPeriodInfo();
            GetJourneyPlan();
            GetJourneyPlanSatelliteResults();
            GetRecoveryPlanResults();
            GetGiltLiabilityResults();
            GetVarWaterfallResults();
            GetVarFunnelResults();            
            GetIAS19Disclosure();
            GetFRS17Disclosure();
            GetUSGAAP();
            GetCompositeIndexComponents();
            GetCustomIndices();
            GetNewJP();

            return t;            
        }

        private void GetCustomIndices()
        {
            t.CustomIndicesEvolution = new List<TrackerIndexEvolution>(10);

            var customIndexNames = new List<string>();
            xls.ActiveSheetByName = "Main";
            var range = xl.GetRangeCoords("SSISetup");
            for (int r = range.Row; r <= range.EndRow; r++)
            {
                var name = xl.GetStringWithDefault(r, range.Col);
                if (!string.IsNullOrEmpty(name))
                {
                    customIndexNames.Add(name);
                }
            }

            xls.ActiveSheetByName = "Testing";
            var row = 5;
            var col = 1;
            while (xls.GetCellValue(row, col) != null)
            {
                var idxName = xl.GetString(row, col);
                if (customIndexNames.Contains(idxName))
                {
                    var evol = new List<DateDictionary>(1000);
                    var d = row + 1;
                    while (xls.GetCellValue(d, col) != null)
                    {
                        evol.Add(new DateDictionary(xl.GetDate(d, 1), xl.GetDouble(d, col)));
                        d++;
                    }
                    t.CustomIndicesEvolution.Add(new TrackerIndexEvolution { Name = idxName, Values = evol });
                }
                col++;
            }
        }

        private void GetDates()
        {
            xls.ActiveSheetByName = "Tracker";
            t.AttributionEnd = xl.GetDate("AnalysisDate");
            t.AttributionStart = xl.GetDate("AttrStartDate");
            t.RefreshDate = xl.GetDateSlow(5, "B");
        }

        private void GetLiabilityEvolution()
        {
            xls.ActiveSheetByName = "Tracker";

            t.LiabilityEvolution = new List<TrackerLiabilityEvolutionItem>(1000);

            var startCell = xl.GetRangeCoords("LiabTrackingA1");
            var col = startCell.Col;
            var row = startCell.Row;

            DateTime date;

            while (xl.TryGetDate(row, col, out date))
            {
                t.LiabilityEvolution.Add(
                    new TrackerLiabilityEvolutionItem
                        (
                            date,
                            xl.GetDouble(row, col + 1, 0),
                            xl.GetDouble(row, col + 2, 0),
                            xl.GetDouble(row, col + 3, 0),
                            xl.GetDouble(row, col + 4, 0),
                            xl.GetDouble(row, col + 5, 0),
                            xl.GetDouble(row, col + 6, 0),
                            xl.GetDouble(row, col + 7, 0),
                            xl.GetDouble(row, col + 8, 0),
                            xl.GetDouble(row, col + 9, 0),
                            xl.GetDouble(row, col + 10, 0),
                            xl.GetDouble(row, col + 11, 0),
                            xl.GetDouble(row, col + 12, 0),
                            xl.GetDouble(row, col + 13, 0),
                            xl.GetDouble(row, col + 14, 0),
                            xl.GetDouble(row, col + 15, 0),
                            xl.GetDouble(row, col + 16, 0),
                            xl.GetDouble(row, col + 17, 0),
                            xl.GetDouble(row, col + 18, 0),
                            xl.GetDouble(row, col + 19, 0),
                            xl.GetDouble(row, col + 20, 0),
                            xl.GetDouble(row, col + 21, 0),
                            xl.GetDouble(row, col + 22, 0),
                            xl.GetDouble(row, col + 23, 0)
                        ));

                row++;
            }
        }

        private void GetLiabilityAssumptionEvolutionResults()
        {
            t.LiabilityAssumptionEvolution =
                t.LiabilityEvolution.Select(x => new TrackerLiabilityAssumptionEvolution
                    {
                        Date = x.Date,
                        Assumptions = new List<AssumptionTypeDictionary>
                            {
                                new AssumptionTypeDictionary(AssumptionType.PreRetirementDiscountRate, x.DiscPre),
                                new AssumptionTypeDictionary(AssumptionType.PostRetirementDiscountRate, x.DiscPost),
                                new AssumptionTypeDictionary(AssumptionType.PensionDiscountRate, x.DiscPen),
                                new AssumptionTypeDictionary(AssumptionType.SalaryIncrease, x.SalInc),
                                new AssumptionTypeDictionary(AssumptionType.InflationRetailPriceIndex, x.RPI),
                                new AssumptionTypeDictionary(AssumptionType.InflationConsumerPriceIndex, x.CPI),
                            }
                    })
                    .ToList();
        }

        private void GetLiabilityPensionIncreaseEvolutionResults()
        {
            t.PensionIncreaseEvolution =
                t.LiabilityEvolution.Select(x => new TrackerPensionIncreaseEvolutionItem
                    {
                        Date = x.Date, 
                        Pincs = new List<TrackerPensionIncreaseValue>
                            {
                                new TrackerPensionIncreaseValue { PincReference = 1, Value = x.Pinc1 },
                                new TrackerPensionIncreaseValue { PincReference = 2, Value = x.Pinc2 },
                                new TrackerPensionIncreaseValue { PincReference = 3, Value = x.Pinc3 },
                                new TrackerPensionIncreaseValue { PincReference = 4, Value = x.Pinc4 },
                                new TrackerPensionIncreaseValue { PincReference = 5, Value = x.Pinc5 },
                                new TrackerPensionIncreaseValue { PincReference = 6, Value = x.Pinc6 }
                            }
                    })
                    .ToList();               
        }

        private void GetUserLiabs()
        {
            xls.ActiveSheetByName = "Panels";
            t.UserLiabs = new TrackerUserLiabilityData
                (
                xl.GetDouble("UserActLiab"),
                xl.GetDouble("UserDefLiab"),
                xl.GetDouble("UserPenLiab"),
                xl.GetDouble("UserServiceCost"),
                xl.GetDouble("UserActDuration"),
                xl.GetDouble("UserDefDuration"),
                xl.GetDouble("UserPenDuration"),
                xl.GetDouble("UserDefBuyIn"),
                xl.GetDouble("UserPenBuyIn")
                );

            xls.ActiveSheetByName = "FundingLevel";
            t.UserActLiabAfter = xl.GetDouble("UserActLiabAfter");
            t.UserDefLiabAfter = xl.GetDouble("UserDefLiabAfter");
            t.UserPenLiabAfter = xl.GetDouble("UserPenLiabAfter");
        }

        private void GetSurplusAnalysis()
        {
            xls.ActiveSheetByName = "Tracker";
            const string column = "BF";
            t.SurplusAnalysisData = new TrackerSurplusAnalysisData(
                xl.GetDouble(92, column),
                xl.GetDouble(93, column),
                xl.GetDouble(94, column),
                xl.GetDouble(95, column),
                xl.GetDouble(96, column),
                xl.GetDouble(97, column),
                xl.GetDouble(98, column),
                xl.GetDouble(99, column)
                );
            t.MarketChange = xl.GetDouble(94, column);
        }

        private void GetPIE()
        {
            xls.ActiveSheetByName = "Panels";
            t.PIEInit = new TrackerPIEInitData(xl.GetDouble("PIEPenLiab"), xl.GetDouble("PIEValueOfIncs"));

            t.PIE = new TrackerPIEData(new BeforeAfter<double>(xl.GetDouble("PIELiabBefore"), xl.GetDouble("PIELiabAfter")),
                xl.GetDouble("PIEPenLiab"), xl.GetDouble(15, "BD"), xl.GetDouble("PIEPenLiabEligible"), xl.GetDouble("PIEPenLiabNilIncs"), xl.GetDouble("PIEValueOfIncs"), xl.GetDouble(22, "BA"),
                0, xl.GetDouble("PIEMaxUplift"), xl.GetDouble("PIEValueShared"));
        }

        private void GetEFRO()
        {
            t.EFRO = new TrackerEFroData(new Dictionary<int, double>(), new Dictionary<int, double>(), 0, 0, 0, 0, 0);//todo: empty data objects.
            if (xl.GetString("FROType").ToLower() != "bulk")
            {
                var EFROTransferValuesCoords = xl.GetRangeCoords("EFROTransferValues");

                var ActiveTransferValuesPayable = new Dictionary<int, double>();
                var DeferredTransferValuesPayable = new Dictionary<int, double>();
                for (int i = 1; i <= 100; i++)
                {
                    ActiveTransferValuesPayable.Add(i, xl.GetDouble(EFROTransferValuesCoords.Row + i, EFROTransferValuesCoords.Col + 1, 0));
                    DeferredTransferValuesPayable.Add(i, xl.GetDouble(EFROTransferValuesCoords.Row + i, EFROTransferValuesCoords.Col + 2, 0));
                }

                t.EFRO = new TrackerEFroData(ActiveTransferValuesPayable, DeferredTransferValuesPayable, xl.GetDouble("EFROCETVsPaid"), xl.GetDouble("EFROLiabsDischarged"), xl.GetDouble("EFROChangeInActLiab"), xl.GetDouble("EFROChangeInDefLiab"), xl.GetDouble("UserActLiab") + xl.GetDouble("UserDefLiab"));
            }
        }

        private void GetFutureBenefits()
        {
            xls.ActiveSheetByName = "Panels";
            t.FutureBenefits = new TrackerFutureBenefitsData(
                xl.GetDouble("FBAnnualCost", 0),
                xl.GetDouble("FBContRate", 0),
                xl.GetDouble("FBEeRate", 0),
                xl.GetDouble("FBErRate", 0),
                xl.GetDouble("FBChangeInAnnualCost", 0),
                xl.GetDouble("FBChangeInErRate", 0),
                xl.GetDouble("FBChangeInFundingPosition", 0),
                xl.GetDouble("FBBestEstimateServiceCost", 0),
                xl.GetDouble("FBBestEstimateContRate", 0),
                xl.GetDouble(14, "BI", 0),
                xl.GetDouble(22, "BM", 0),
                xl.GetDouble(23, "BM", 0)
                );
        }

        public void GetInvestmentStrategy()
        {
            xls.ActiveSheetByName = "Panels";

            t.InvestmentStrategyResults = new TrackerInvestmentStrategyData
                (
                xl.GetDouble(132, "AE"),
                xl.GetDouble(137, "AE", 0),
                xl.GetDouble(138, "AE", 0),
                xl.GetDouble(117, "AD", 0)
                );

            t.OutPerformanceMaxReturn = xl.GetDouble(25, "S", .0);

            t.HedgeBreakdown = new TrackerHedgeBreakdownData
            {
                Date = t.AttributionEnd,
                CurrentBasis = new List<HedgeTypeDictionary>
                        {
                            new HedgeTypeDictionary(HedgeType.Interest, new HedgeBreakdownItem(xl.GetDouble(56, "AB"), xl.GetDouble(57, "AB"), xl.GetDouble(58, "AB"), xl.GetDouble(59, "AB"))),
                            new HedgeTypeDictionary(HedgeType.Inflation, new HedgeBreakdownItem(xl.GetDouble(56, "AC"), xl.GetDouble(57, "AC"), xl.GetDouble(58, "AC"), xl.GetDouble(59, "AC")))
                        },
                Cashflows = new List<HedgeTypeDictionary>
                        {
                            new HedgeTypeDictionary(HedgeType.Interest, new HedgeBreakdownItem(xl.GetDouble(56, "AD"), xl.GetDouble(57, "AD"), xl.GetDouble(58, "AD"), xl.GetDouble(59, "AD"))),
                            new HedgeTypeDictionary(HedgeType.Inflation, new HedgeBreakdownItem(xl.GetDouble(56, "AE"), xl.GetDouble(57, "AE"), xl.GetDouble(58, "AE"), xl.GetDouble(59, "AE")))
                        }
            };
        }

        private void GetAbf()
        {
            xls.ActiveSheetByName = "Panels";
            t.ABF = new TrackerAbfData(xl.GetDouble("ABFDeficit"), xl.GetDouble("ABFRPConts", 0), xl.GetDouble("ABFRPNetCash", 0), xl.GetDouble("ABFValueProp"),
                xl.GetDouble("ABFConts", 0), xl.GetDouble("ABFNetCash", 0), xl.GetDouble("ABFSaving", 0), xl.GetDouble("ABFValue", 0));
        }

        private void GetVarWaterfallResults()
        {
            xls.ActiveSheetByName = "VaRCalcs";
            var row = 81;
            var col = "K";
            t.Waterfall = new TrackerWaterfallData(
                xl.GetDouble(row++, col),
                xl.GetDouble(row++, col),
                xl.GetDouble(row++, col),
                xl.GetDouble(row++, col),
                xl.GetDouble(row++, col),
                xl.GetDouble(row++, col),
                xl.GetDouble(row++, col),
                xl.GetDouble(row++, col),
                xl.GetDouble(row++, col),
                xl.GetDouble(row + 1, col),
                xl.GetDouble(row + 2, col));
        }

        private void GetVarFunnelResults()
        {
            xls.ActiveSheetByName = "VaRCalcs";
            const int row = 122;
            t.VarFunnel = new List<TrackerVarFunnelData>();
            var year = 0;
            Func<int, string, TrackerVarFunnelData> getData = (r, c) =>
                new TrackerVarFunnelData(
                    year++,
                    xl.GetDouble(r++, c),
                    xl.GetDouble(r++, c), 
                    xl.GetDouble(r++, c),
                    xl.GetDouble(r++, c), 
                    xl.GetDouble(r, c));
            t.VarFunnel.Add(getData(row, "C"));
            t.VarFunnel.Add(getData(row, "D"));
            t.VarFunnel.Add(getData(row, "E"));
            t.VarFunnel.Add(getData(row, "F"));
            t.VarFunnel.Add(getData(row, "G"));
            t.VarFunnel.Add(getData(row, "H"));
        }

        private void GetBalanceEstimate()
        {
            xls.ActiveSheetByName = "Accounting";
            t.BalanceSheetEstimate = new TrackerBalanceData(xl.GetDouble(29, "B"), xl.GetDouble(30, "C"));
        }

        private void GetProfitLossForecastResults()
        {
            xls.ActiveSheetByName = "Accounting";
            t.ProfitLossForecast = new List<TrackerProfitLossForecast>
            {
                new TrackerProfitLossForecast(xl.GetDouble(32, "P", 0), xl.GetDouble(32, "O", 0), xl.GetDouble(32, "N", 0), xl.GetDouble(32, "M", 0))
                {
                    ValuationBasis = ValuationBasisType.Ias19
                },
                new TrackerProfitLossForecast(xl.GetDouble(32, "U", 0), xl.GetDouble(32, "R", 0), xl.GetDouble(32, "T", 0), xl.GetDouble(32, "S", 0))                
                {
                    ValuationBasis = ValuationBasisType.Frs17
                }
            };
        }

        private void GetIAS19Disclosure()
        {
            if (!xl.GetBool("IAS19Visible", "Main"))
                return;

            const int multi = 1000;

            Func<string, TrackerIAS19Disclosure> getreport = col =>
            {
                xls.ActiveSheetByName = "Accounting";
                var interestCredit = xl.GetDouble(32, "M");
                var interestCharge = xl.GetDouble(32, "N");

                xls.ActiveSheetByName = "Disclosures";

                int row = 11;
                Func<int, int> inc = x => { row += x; return row; };
                return new TrackerIAS19Disclosure(
                    xl.GetDateSlow(row, col),
                    new DisclosureAssumptions
                    {
                        DiscountRate = xl.GetDouble(inc(1), col, 0),
                        FutureSalaryGrowth = xl.GetDouble(inc(1), col, 0),
                        RPIInflation = xl.GetDouble(inc(1), col, 0),
                        CpiInflation = xl.GetDouble(inc(1), col, 0),
                        MaleLifeExpectancy65 = xl.GetDouble(inc(2), col, 0),
                        MaleLifeExpectancy65_45 = xl.GetDouble(inc(1), col, 0)
                    },
                    new IAS19Disclosure.IAS19BalanceSheetAmountData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new IAS19Disclosure.DefinedBenefitObligationData(
                        xl.GetDouble(inc(6), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new IAS19Disclosure.IAS19FairValueSchemeAssetChangesData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new IAS19Disclosure.IAS19SchemeSurplusChangesData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(2), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(2), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(2), col, 0) * multi,
                        xl.GetDouble(inc(2), col, 0) * multi),
                    new IAS19Disclosure.IAS19PAndLForecastData(
                        xl.GetDouble(inc(6), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        interestCredit,
                        interestCharge,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi)
                        );
            };

            var before = getreport("D");
            //E.F. can't handle complex types with a null value
            //before.DefinedBenefitObligation = null;
            //before.FairValueSchemeAssetChanges = null;
            //before.SchemeSurplusChanges = null;
            //before.PAndLForecast = null;

            var after = getreport("C");

            t.IAS19DisclosureBefore = before;
            t.IAS19DisclosureAfter = after;
        }

        private void GetFRS17Disclosure()
        {
            if (!xl.GetBool("FRS17Visible", "Main"))
                return;

            xls.ActiveSheetByName = "Disclosures";
            const int multi = 1000;

            Func<string, TrackerFRS17Disclosure> getreport = col =>
            {
                int row = 11;
                Func<int, int> inc = x => { row += x; return row; };
                return new TrackerFRS17Disclosure(
                    xl.GetDateSlow(row, col),
                    new DisclosureAssumptions
                    {
                        DiscountRate = xl.GetDouble(inc(1), col, 0),
                        FutureSalaryGrowth = xl.GetDouble(inc(1), col, 0),
                        RPIInflation = xl.GetDouble(inc(1), col, 0),
                        CpiInflation = xl.GetDouble(inc(1), col, 0),
                        ExpectedRateOfReturnOnAssets = xl.GetDouble(inc(1), col, 0),
                        MaleLifeExpectancy65 = xl.GetDouble(inc(2), col, 0),
                        MaleLifeExpectancy65_45 = xl.GetDouble(inc(1), col, 0)
                    },
                    new FRS17Disclosure.FRS17BalanceSheetAmountData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new FRS17Disclosure.PresentValueSchemeLiabilityData(
                        xl.GetDouble(inc(6), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new FRS17Disclosure.FRS17FairValueSchemeAssetChangesData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new FRS17Disclosure.FRS17SchemeSurplusChangesData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(2), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(2), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(2), col, 0) * multi,
                        xl.GetDouble(inc(2), col, 0) * multi),
                    new FRS17Disclosure.FRS17PAndLForecastData(
                        xl.GetDouble(inc(6), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi)
                    );
            };

            var before = getreport("J");
            //E.F. can't handle complex types with a null value
            //before.PresentValueSchemeLiabilities = null;
            //before.FairValueSchemeAssetChanges = null;
            //before.SchemeSurplusChanges = null;
            //before.PAndLForecast = null;

            var after = getreport("I");

            t.FRS17DisclosureBefore = before;
            t.FRS17DisclosureAfter = after;
        }

        private void GetUSGAAP()
        {
            GetUSGAAPTable();
            GetUSGAAPExpectedReturns();
            GetUSGAAPDisclosure();
        }

        private void GetUSGAAPTable()
        {
            t.USGAAPTable = new List<TrackerAOCIDataItem>();

            if (!xl.GetBool("USGAAPVisible", "Main"))
                return;

            xls.ActiveSheetByName = "Disclosures";

            var coords = xl.GetRangeCoords("USGAAPTable");
            var row = coords.Row;

            while (xls.GetCellValue(row, coords.Col) != null)
            {
                t.USGAAPTable.Add(new TrackerAOCIDataItem(xl.GetDate(row, coords.Col), new AOCIData(xl.GetDouble(row, coords.Col + 1), xl.GetDouble(row, coords.Col + 2), xl.GetDouble(row, coords.Col + 3), (int)xl.GetDouble(row, coords.Col + 4))));
                row++;
            }
        }

        private void GetUSGAAPExpectedReturns()
        {
            t.USGAAPExpectedReturns = new List<TrackerUSGAAPExpectedReturn>(3);

            if (!xl.GetBool("USGAAPVisible", "Main"))
                return;

            xls.ActiveSheetByName = "Disclosures";

            t.USGAAPExpectedReturns.Add(new TrackerUSGAAPExpectedReturn { USGAAPExpectedReturnsType = USGAAPExpectedReturnsType.AnalysisEnd, Value = xl.GetDouble(21, "S", 0) });
            t.USGAAPExpectedReturns.Add(new TrackerUSGAAPExpectedReturn { USGAAPExpectedReturnsType = USGAAPExpectedReturnsType.AnalysisStart, Value = xl.GetDouble(21, "T", 0) });
            t.USGAAPExpectedReturns.Add(new TrackerUSGAAPExpectedReturn { USGAAPExpectedReturnsType = USGAAPExpectedReturnsType.UsersInputs, Value = xl.GetDouble(21, "U", 0) });
        }

        private void GetUSGAAPDisclosure()
        {
            if (!xl.GetBool("USGAAPVisible", "Main"))
                return;

            xls.ActiveSheetByName = "Disclosures";
            const int multi = 1000;

            Func<string, TrackerUSGAAPDisclosure> getreport = col =>
            {
                int row = 11;
                Func<int, int> inc = x => { row += x; return row; };
                return new TrackerUSGAAPDisclosure(
                    xl.GetDateSlow(row, col),
                    new DisclosureAssumptions
                    {
                        DiscountRate = xl.GetDouble(inc(1), col, 0),
                        FutureSalaryGrowth = xl.GetDouble(inc(1), col, 0),
                        RPIInflation = xl.GetDouble(inc(1), col, 0),
                        CpiInflation = xl.GetDouble(inc(1), col, 0),
                        ExpectedRateOfReturnOnAssets = xl.GetDouble(inc(1), col, 0),
                        MaleLifeExpectancy65 = xl.GetDouble(inc(2), col, 0),
                        MaleLifeExpectancy65_45 = xl.GetDouble(inc(1), col, 0)
                    },
                    new DisclosureAssumptions
                    {
                        DiscountRate = xl.GetDouble(inc(4), col, 0),
                        ExpectedRateOfReturnOnAssets = xl.GetDouble(inc(1), col, 0),
                        FutureSalaryGrowth = xl.GetDouble(inc(1), col, 0),
                        RPIInflation = xl.GetDouble(inc(1), col, 0),
                        CpiInflation = xl.GetDouble(inc(1), col, 0),
                        MaleLifeExpectancy65 = xl.GetDouble(inc(2), col, 0),
                        MaleLifeExpectancy65_45 = xl.GetDouble(inc(1), col, 0)
                    },
                    new USGAAPDisclosure.USGAAPBalanceSheetAmountData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new USGAAPDisclosure.ChangesInPresentValueData(
                        xl.GetDouble(inc(6), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new USGAAPDisclosure.ChangesInFairValueData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new USGAAPDisclosure.AccumulatedBenefitObligationData(
                        xl.GetDouble(inc(4), col, 0) * multi),
                    new USGAAPDisclosure.EstimatedPaymentData(
                        xl.GetDouble(inc(3), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new USGAAPDisclosure.NetPeriodicPensionCostData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new USGAAPDisclosure.ComprehensiveIncomeData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi),
                    new USGAAPDisclosure.ComprehensiveAccumulatedIncomeData(
                        xl.GetDouble(inc(5), col, null) * multi,
                        xl.GetDouble(inc(1), col, null) * multi,
                        xl.GetDouble(inc(1), col, null) * multi,
                            xl.GetDouble(inc(1), col, 0) * multi),
                    new USGAAPDisclosure.NetPeriodicPensionCostForecastData(
                        xl.GetDouble(inc(5), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi,
                        xl.GetDouble(inc(1), col, 0) * multi));
            };

            var before = getreport("P");
            //E.F. can't handle complex types with a null value
            //before.Assumptions2 = null;
            //before.AccumulatedBenefitObligation = null;
            //before.ChangesInPresentValue = null;
            //before.ChangesInFairValue = null;
            //before.EstimatedPayments = null;
            //before.NetPeriodicPensionCost = null;
            //before.ComprehensiveIncome = null;
            //before.ComprehensiveAccumulatedIncome = null;
            //before.NetPeriodPensionCostForecast = null;

            var after = getreport("O");

            t.USGAAPDisclosureBefore = before;
            t.USGAAPDisclosureAfter = after;
        }

        private void GetProfitLoss()
        {
            xls.ActiveSheetByName = "Accounting";

            t.ProfitLoss = new TrackerProfitLossData
                (
                xl.GetDouble(28, "H"),
                xl.GetDouble(29, "H"),
                xl.GetDouble(30, "H"),
                xl.GetDouble(32, "H"),
                xl.GetDouble(33, "H"),
                xl.GetDouble(34, "H"),
                xl.GetDouble(41, "H"),
                xl.GetDouble(42, "H"),
                xl.GetDouble(43, "H"),
                xl.GetDouble(44, "H"),
                xl.GetDouble(45, "H"),
                xl.GetDouble(46, "H")
                );
        }

        private void GetEvolutionSatelliteResults()
        {
            xls.ActiveSheetByName = "Tracker";
            t.BuyInAtAnalysisDate = xl.GetDouble("BuyInAtAnalysisDate");
        }

        private void GetFundingLevel()
        {
            xls.ActiveSheetByName = "FundingLevel";
            double totalBuyin = 0;
            try
            {
                double.TryParse(((TFormula)xls.GetCellValue(33, 4)).Result.ToString(), out totalBuyin);
            }
            catch { }
            t.FundingLevel = new TrackerFundingData(
                xl.GetDouble(29, "C"), xl.GetDouble(30, "C"), xl.GetDouble(31, "C"),
                xl.GetDouble(32, "D"), xl.GetDouble(33, "D"), xl.GetDouble(36, "D") - xl.GetDouble(36, "C"));
        }

        private void GetJourneyPlanSatelliteResults()
        {
            xls.ActiveSheetByName = "JourneyPlan";
            t.AssetsAtSED = xl.GetDouble("AssetsAtSED");
            t.JPInvGrowth = xl.GetDouble("JPInvGrowth", 0);
            t.JP2InvGrowth = xl.GetDouble("JP2InvGrowth", 0);
            t.JPAfterLiabDuration = xl.GetDouble("JP2LiabDuration");

            GetAdjustedCashflows(); 
            GetInvestmentStrategy();
            GetFRO();
            GetEFRO();
            GetETV();
            GetPIE();
            GetFutureBenefits();
            GetAbf();
            GetInsurance();
        }

        private void GetAdjustedCashflows()
        {
            var cashflowRange = xl.GetRangeCoords("JP2CashflowsA1", "Cashflow");

            var offset = cashflowRange.Row + 2;

            t.AdjustedCashflows = 
                TrackerReaderUtils.ParseCashflowRange(xls, "Cashflow", cashflowRange.Col + 1, cashflowRange.Row + 2, true)
                    .ToList();
        }

        private void GetFRO()
        {
            xls.ActiveSheetByName = "Panels";
            t.FRO = new TrackerFroData(xl.GetDouble("FROBasisChange"), xl.GetDouble("FROTVsPaid", 0), xl.GetDouble("FROLiabsDischarged", 0), xl.GetDouble("UserDefLiab"), xl.GetDouble("PreFRODefLiab"));
        }

        private void GetETV()
        {
            xls.ActiveSheetByName = "Panels";

            var etvChanged = xl.GetDouble("ETVUnenhancedValue", 0) > 0 ? xl.GetDouble("ETVBasisChange") : 0;

            t.ETV = new TrackerEtvData(xl.GetDouble("PreETVDefLiab", 0), etvChanged, xl.GetDouble(17, "AR", 0),
                xl.GetDouble("ETVUnenhancedTVsPaid", 0), xl.GetDouble("ETVEnhancedTVsPaid", 0), xl.GetDouble("ETVLiabsDischarged", 0), xl.GetDouble(17, "AU", 0), xl.GetDouble(18, "AU", 0), xl.GetDouble("ETVUnenhancedTVsPaid", 0));
        }

        private void GetInsurance()
        {
            xls.ActiveSheetByName = "Panels";
            t.InsPenCost = xl.GetDouble("InsPenCost");
            t.InsNonPenCost = xl.GetDouble("InsNonPenCost");

            xls.ActiveSheetByName = "Panels";

            t.BuyIn = new TrackerBuyinData(
                xl.GetDouble(12, XlHelper.GetCol("BZ"), 0),
                xl.GetDouble(13, XlHelper.GetCol("BZ"), 0),
                xl.GetDouble("InsPenAsset", 0),
                xl.GetDouble("InsNonPenAsset", 0));

            t.TotalInsurancePremiumPaid = xl.GetDouble(23, XlHelper.GetCol("BZ"), 0);

            var strainsCoords = xl.GetRangeCoords("BuyoutStrains");

            var i = 0;
            while (xls.GetCellValue(strainsCoords.Row + i, XlHelper.GetCol("CB")) != null)
            {

                i++;
            }

            t.InsurancePointIncreaseAmount = xl.GetDouble("InsImpact50bps", 0, "Panels");
        }

        private void GetGiltLiabilityResults()
        {
            xls.ActiveSheetByName = "VolatilityAssumptions";
            var GiltLiabDuration = xl.GetDouble("GiltLiabDuration");
            var GiltLiabPV01 = xl.GetDouble("GiltLiabPV01");
            var GiltLiabIE01 = xl.GetDouble("GiltLiabIE01");
            var GiltBuyInPV01 = xl.GetDouble("GiltBuyInPV01");
            var GiltBuyInIE01 = xl.GetDouble("GiltBuyInIE01");
            xls.ActiveSheetByName = "LDIHedge";
            var GiltTotLiab = xl.GetDouble("GiltTotLiab");

            t.GiltBasisLiability = new TrackerGiltBasisLiabilityData(GiltTotLiab, GiltLiabDuration, GiltLiabPV01, GiltLiabIE01, GiltBuyInPV01, GiltBuyInIE01, null);
        }

        private void GetAssetHedgeBreakdownData()
        {
            t.AssetHedgeBreakdownData = new List<TrackerHedgeBreakdownData>();
            for (var i = 1; i <= xls.SheetCount; i++)
            {
                var sheet = xls.GetSheetName(i);
                if (!sheet.ToLower().StartsWith("assets"))
                    continue;

                xls.ActiveSheetByName = sheet;

                var effectiveDate = xl.GetDate("EffectiveDate");
                var builtInHedge = xl.GetRangeCoords("BuiltInHedge");
                var builtInHedgeRow = builtInHedge.Row;
                var builtInHedgeCol = builtInHedge.Col;
                var hedgeSummary = xl.GetRangeCoords("HedgeSummary");
                var hedgeSummaryRow = hedgeSummary.Row;
                var hedgeSummaryCol = hedgeSummary.Col;
                var hedgeBreakdownData = new TrackerHedgeBreakdownData
                {
                    Date = effectiveDate,
                    CurrentBasis = new List<HedgeTypeDictionary>
                        {
                            new HedgeTypeDictionary(HedgeType.Interest, new HedgeBreakdownItem(0, 0, 0, xl.GetDouble(hedgeSummaryRow, hedgeSummaryCol, 0))),
                            new HedgeTypeDictionary(HedgeType.Inflation, new HedgeBreakdownItem(0, 0, 0, xl.GetDouble(hedgeSummaryRow + 1, hedgeSummaryCol, 0)))
                        },
                    Cashflows = new List<HedgeTypeDictionary>
                        {
                            new HedgeTypeDictionary(HedgeType.Interest, new HedgeBreakdownItem(xl.GetDouble(builtInHedgeRow, builtInHedgeCol, 0), xl.GetDouble(builtInHedgeRow, builtInHedgeCol + 1, 0), 0, xl.GetDouble(hedgeSummaryRow, hedgeSummaryCol + 1, 0))),
                            new HedgeTypeDictionary(HedgeType.Inflation, new HedgeBreakdownItem(xl.GetDouble(builtInHedgeRow + 1, builtInHedgeCol, 0), xl.GetDouble(builtInHedgeRow + 1, builtInHedgeCol + 1, 0), 0, xl.GetDouble(hedgeSummaryRow + 1, hedgeSummaryCol + 1, 0)))
                        }
                };
                t.AssetHedgeBreakdownData.Add(hedgeBreakdownData);
            }
        }
        
        private void GetBasisSummaries()
        {
            basisSummaries = new List<TrackerBasisSummary>(10);

            for (var i = 1; i <= xls.SheetCount; i++)
            {
                var sheet = xls.GetSheetName(i);
                if (!sheet.ToLower().StartsWith("liab"))
                    continue;

                xls.ActiveSheetByName = sheet;
                var t = TrackerReaderUtils.ParseBasisType(xl.GetString(6, 2));

                var name = xl.GetString(7, XlHelper.GetCol("B"));
                var effectiveDate = xl.GetDate(8, "B");

                basisSummaries.Add(new TrackerBasisSummary(name, t, effectiveDate));
            }
        }

        private void GetCashFlowOutputs()
        {
            t.CashflowsBefore = new List<TrackerCashflow>(50);
            t.CashflowsAfter = new List<TrackerCashflow>(50);
            xls.ActiveSheetByName = "Cashflow";

            double year;
            var row = 30;
            var beforeCol = 1;
            var afterCol = 11;
            Func<int, int, Dictionary<MemberStatus, double>> getData = (r, c) =>
                new Dictionary<MemberStatus, double>
                    {
                        { MemberStatus.ActivePast, xl.GetDouble(r, c) },
                        { MemberStatus.Deferred, xl.GetDouble(r, c + 1) },
                        { MemberStatus.Pensioner, xl.GetDouble(r, c + 2) },
                        { MemberStatus.ActiveFuture, xl.GetDouble(r, c + 4) }
                    };
            while (xl.TryGetDouble(row, beforeCol, out year))
            {
                t.CashflowsBefore.Add(new TrackerCashflow((int)year, getData(row, beforeCol + 1).ToMemberStatusDictionary()));
                t.CashflowsAfter.Add(new TrackerCashflow((int)year, getData(row, afterCol).ToMemberStatusDictionary()));
                row++;
            }
        }

        private void GetAssetEvolutionResults()
        {
            t.LDIEvolutionData = new List<TrackerLDIEvolutionItem>(1000);

            xls.ActiveSheetByName = "Main";
            var LDIBasis = xl.GetString("LDIBasis");

            xls.ActiveSheetByName = "Tracker";

            t.AssetEvolution = new List<TrackerAssetEvolutionItem>(1000);

            var startCell = xl.GetRangeCoords("AssetA1");
            var col = startCell.Col;
            var row = startCell.Row;

            var assetVal = .0;

            //don't start adding LDI data until we reach the start date of the basis LDI is linked to
            var ldiStart =
                string.IsNullOrEmpty(LDIBasis)
                ? DateTime.MaxValue
                : basisSummaries.Where(b => b.Name == LDIBasis).Select(b => b.EffectiveDate).Min();

            while (xl.TryGetDouble(row, col + 9, out assetVal))
            {
                var date = xl.GetDate(row, col);

                t.AssetEvolution.Add(
                    new TrackerAssetEvolutionItem
                        (
                            date,
                            xl.GetDouble(row, col + 1, 0),
                            xl.GetDouble(row, col + 2, 0),
                            xl.GetDouble(row, col + 4, 0),
                            xl.GetDouble(row, col + 5, 0),
                            xl.GetDouble(row, col + 6, 0),
                            xl.GetDouble(row, col + 7, 0),
                            xl.GetDouble(row, col + 8, 0),
                            xl.GetDouble(row, col + 9, 0),
                            xl.GetDouble(row, col + 10, 0)
                        ));

                if (date >= ldiStart)
                {
                    t.LDIEvolutionData.Add(
                        new TrackerLDIEvolutionItem
                        (
                            date,
                                xl.GetDouble(row, col + 8, 0),
                                xl.GetDouble(row, col + 9, 0)
                        )
                    );
                }

                row++;
            }
        }

        private void GetExperienceResults()
        {
            xls.ActiveSheetByName = "Tracker";

            t.LiabilityExperience = new List<DateDictionary>();

            DateTime date;
            var startCell = xl.GetRangeCoords("LiabExperience");
            var col = startCell.Col;
            var row = startCell.Row;

            while (xl.TryGetDate(row, col, out date))
            {
                t.LiabilityExperience.Add(new DateDictionary(date, xl.GetDouble(row, col + 1, 0)));
                row++;
            }

            t.BuyinExperience = new List<DateDictionary>();

            startCell = xl.GetRangeCoords("BuyInExperience");
            col = startCell.Col;
            row = startCell.Row;

            while (xl.TryGetDate(row, col, out date))
            {
                t.BuyinExperience.Add(new DateDictionary(date, xl.GetDouble(row, col + 1, 0)));
                row++;
            }

            t.AssetExperience = new List<DateDictionary>();

            startCell = xl.GetRangeCoords("AssetExperience");
            col = startCell.Col;
            row = startCell.Row;

            while (xl.TryGetDate(row, col, out date))
            {
                t.AssetExperience.Add(new DateDictionary(date, xl.GetDouble(row, col + 1, 0)));
                row++;
            }
        }

        private void GetExpectedDeficit()
        {
            xls.ActiveSheetByName = "ExpectedDeficit";

            t.ExpectedDeficit = new List<TrackerExpectedDeficitData>(1000);

            var col = XlHelper.GetCol("O");
            var row = 6;

            DateTime date;

            var end = t.LiabilityEvolution.Max(x => x.Date);

            while (xl.TryGetDate(row, col, out date) && date <= end)
            {
                t.ExpectedDeficit.Add(
                    new TrackerExpectedDeficitData(
                        date,
                        xl.GetDouble(row, "AI"),
                        xl.GetDouble(row, "AH"),
                        xl.GetDouble(row, "S"),
                        xl.GetDouble(row, "AJ")));
                row++;
            }
        }

        private void GetJourneyPlan()
        {
            t.JourneyPlanBefore = new List<TrackerBalanceData>();
            t.JourneyPlanAfter = new List<TrackerBalanceData>();

            xls.ActiveSheetByName = "JourneyPlan";

            var beforeCoords = xl.GetRangeCoords("JPOutputA1");
            var afterCoords = xl.GetRangeCoords("JP2OutputA1");

            for (int i = 0; i <= 50; i++)
            {
                t.JourneyPlanBefore.Add(new TrackerBalanceData(xl.GetDouble(beforeCoords.Row + i, beforeCoords.Col + 2), xl.GetDouble(beforeCoords.Row + i, beforeCoords.Col + 1)) { Year = i });
                t.JourneyPlanAfter.Add(new TrackerBalanceData(xl.GetDouble(afterCoords.Row + i, afterCoords.Col + 2), xl.GetDouble(afterCoords.Row + i, afterCoords.Col + 1)) { Year = i });
            }
        }

        private void GetRecoveryPlanResults()
        {
            xls.ActiveSheetByName = "RecoveryPlan";

            var oldRecoveryPlan = new List<IntKeyDictionary>();
            var newRecoveryPlan = new List<IntKeyDictionary>();

            var rangeOld = new RangeCoords(XlHelper.GetCol("S"), 31);

            GetPartRecoveryPlan(oldRecoveryPlan, rangeOld.Row, rangeOld.Col);
            GetPartRecoveryPlan(newRecoveryPlan, rangeOld.Row, rangeOld.Col + 1);

            t.RecoveryPlanBefore = new List<IntKeyDictionary>(oldRecoveryPlan);
            t.RecoveryPlanAfter = new List<IntKeyDictionary>(newRecoveryPlan);
            t.RecoveryPlanPaymentRequired = xl.GetDouble("RPAmount");
        }

        private void GetPartRecoveryPlan(List<IntKeyDictionary> recoveryPlanPart, int startRow, int column)
        {
            for (int i = 0; i < 50; i++)
            {
                var row = startRow + i;
                var value = xl.GetDouble(row, column, 0);
                recoveryPlanPart.Add(new IntKeyDictionary(i, value));
            }
            // recovery plan might start with an empty and have values later in the timeline
            // but if you can't find any values at any point, then clear it out.
            if (recoveryPlanPart.All(x => Math.Abs(x.Value) < Utils.TestPrecision))
            {
                recoveryPlanPart.Clear();
            }
            // or clear any trailing zeros
            for (int i = recoveryPlanPart.Count - 1; i >= 0; i--)
            {
                if (recoveryPlanPart[i].Value < 0.00000001 && recoveryPlanPart[i].Value > -0.00000001)
                    recoveryPlanPart.RemoveAt(i);
                else
                    break;
            }
        }

        private void GetCompositeIndexComponents()
        {
            t.CompositeIndexParameters = new List<TrackerCompositeIndexData>();
            
            xls.ActiveSheetByName = "Main";
            var compositeIndexCoords = xl.GetRangeCoords("SSISetup", "Main");
            var sSIProportions = xl.GetRangeCoords("SSIProportions", "Main");

            var totalReturnCompositeIndexParams = new TrackerCompositeIndexData()
            {
                CiName = (string)xls.GetCellValue(compositeIndexCoords.Row, compositeIndexCoords.Col),
                CustomIndexType = (string)xls.GetCellValue(compositeIndexCoords.Row, compositeIndexCoords.Col + 1),
                Rounding = (double)xls.GetCellValue(compositeIndexCoords.Row, compositeIndexCoords.Col + 2),
                SourceIndexProportion = new List<TrackerSourceIndexProportion>
                {
                    new TrackerSourceIndexProportion 
                    {
                        IndexName = (string)xls.GetCellValue(sSIProportions.Row, sSIProportions.Col + 2),
                        Value = (double)xls.GetCellValue(sSIProportions.Row, sSIProportions.Col + 1)
                    },
                    new TrackerSourceIndexProportion 
                    {
                        IndexName = (string)xls.GetCellValue(sSIProportions.Row + 1, sSIProportions.Col + 2),
                        Value = (double)xls.GetCellValue(sSIProportions.Row + 1, sSIProportions.Col + 1)
                    }
                }
            };

            t.CompositeIndexParameters.Add(totalReturnCompositeIndexParams);

            var yieldCompositeIndexParams = new TrackerCompositeIndexData()
            {
                CiName = (string)xls.GetCellValue(compositeIndexCoords.Row + 1, compositeIndexCoords.Col),
                CustomIndexType = (string)xls.GetCellValue(compositeIndexCoords.Row + 1, compositeIndexCoords.Col + 1),
                Rounding = (double)xls.GetCellValue(compositeIndexCoords.Row + 1, compositeIndexCoords.Col + 2),
                SourceIndexProportion = new List<TrackerSourceIndexProportion>
                {
                    new TrackerSourceIndexProportion 
                    {
                        IndexName = (string)xls.GetCellValue(sSIProportions.Row + 2, sSIProportions.Col + 2),
                        Value = (double)xls.GetCellValue(sSIProportions.Row + 2, sSIProportions.Col + 1)
                    },
                    new TrackerSourceIndexProportion 
                    {
                        IndexName = (string)xls.GetCellValue(sSIProportions.Row + 3, sSIProportions.Col + 2),
                        Value = (double)xls.GetCellValue(sSIProportions.Row + 3, sSIProportions.Col + 1)
                    }
                }
            };

            t.CompositeIndexParameters.Add(yieldCompositeIndexParams);
        }

        private void GetGrowthOverPeriodInfo()
        {
            t.GrowthOverPeriodInfo = new List<TrackerKeyValue>();

            xls.ActiveSheetByName = "Misc";
            var GrowthOverPeriodCoords = xl.GetRangeCoords("GrowthOverPeriod");
            var i = 0;
            while (xls.GetCellValue(GrowthOverPeriodCoords.Row + i, GrowthOverPeriodCoords.Col) != null)
            {
                t.GrowthOverPeriodInfo.Add(new TrackerKeyValue(
                    xl.GetString(GrowthOverPeriodCoords.Row + i, GrowthOverPeriodCoords.Col),
                    xls.GetCellValue(GrowthOverPeriodCoords.Row + i, GrowthOverPeriodCoords.Col + 1).ToString() == "n/a"
                        ? (double?)null
                        : xl.GetDouble(GrowthOverPeriodCoords.Row + i, GrowthOverPeriodCoords.Col + 1)));
                i++;
            }
        }

        private void GetNewJP()
        {
            GetNewJPGiltRates();
            GetNewJPChartResults();
        }

        private void GetNewJPGiltRates()
        {
            xls.ActiveSheetByName = "NewJP";
            t.GiltSpotRates = new List<IntKeyDictionary>(100);
            var curveTableCoords = xl.GetRangeCoords("CurveTable");
            var year = 1.0;
            while (xl.TryGetDouble(curveTableCoords.Row + (int)year, curveTableCoords.Col, out year))
            {
                t.GiltSpotRates.Add(new IntKeyDictionary((int)year, xl.GetDouble(curveTableCoords.Row + (int)year, curveTableCoords.Col + 1)));
                year++;
            }
        }

        private void GetNewJPChartResults()
        {
            double? fundingPremPre = null;
            double? fundingPremPost = null;
            double? selfsufficiencyPre = null;
            double? selfsufficientyPost = null;
            double? buyoutPre = null;
            double? buyoutPost = null;

            xls.ActiveSheetByName = "NewJP";

            var range = xl.GetRangeCoords("NewJPBases");

            for (int r = 0; r <= 2; r++)
            {
                var row = range.Row + r;
                var basisName = xl.GetString(row, range.Col);
                if (!string.IsNullOrEmpty(basisName))
                {
                    double d;
                    if (xl.TryGetDouble(row, range.Col + 1, out d))
                    {
                        var pre = xl.GetDouble(row, range.Col + 1);
                        var post = xl.GetDouble(row, range.Col + 2);
                        switch(row)
                        {
                            case 0: 
                                fundingPremPre = pre;
                                fundingPremPost = post;
                                break;
                            case 1: 
                                selfsufficiencyPre = pre;
                                selfsufficientyPost = post;
                                break;
                            case 2: 
                                buyoutPre = pre;
                                buyoutPost = post;
                                break;
                        }
                    }
                }
            }

            var chartData = GetNewJPChartData();

            t.NewJP = new TrackerNewJPData
                {
                    Liab1 = chartData.Select(x => new TrackerNewJPYearlyResults(x.Key, x.Value.TechnicalProvision)).ToList(),
                    Liab2 = chartData.Select(x => new TrackerNewJPYearlyResults(x.Key, x.Value.SelfSufficiency)).ToList(),
                    Liab3 = chartData.Select(x => new TrackerNewJPYearlyResults(x.Key, x.Value.Buyout)).ToList(),
                    Assets1 = chartData.Select(x => new TrackerNewJPYearlyResults(x.Key, x.Value.Asset1)).ToList(),
                    Assets2 = chartData.Select(x => new TrackerNewJPYearlyResults(x.Key, x.Value.Asset2)).ToList(),
                    FundingPremiumPre = fundingPremPre,
                    FundingPremiumPost = fundingPremPost,
                    SelfSufficiencyPremiumPre = selfsufficiencyPre,
                    SelfSufficiencyPremiumPost = selfsufficientyPost,
                    BuyoutPremiumPre = buyoutPre,
                    BuyoutPremiumPost = buyoutPost
                };
        }

        private IDictionary<int, NewJPChartDataItem> GetNewJPChartData()
        {
            xls.ActiveSheetByName = "NewJP";

            var NewJPChartData = new Dictionary<int, NewJPChartDataItem>(50);

            var startCell = xl.GetRangeCoords("NewJPChartA1");
            var col = startCell.Col;
            var row = startCell.Row + 1;

            for (int i = 0; i <= 50; i++)
            {
                NewJPChartData.Add(i,
                    new NewJPChartDataItem
                        (
                            i,
                            xl.GetDouble(row, col + 1, 0),
                            xl.GetDouble(row, col + 2, 0),
                            xl.GetDouble(row, col + 3, 0),
                            xl.GetDouble(row, col + 4, 0),
                            xl.GetDouble(row, col + 5, 0)
                        ));
                row++;
            }

            return NewJPChartData;
        }
    }
}