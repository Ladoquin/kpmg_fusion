﻿namespace FlightDeck.Tracker.Reader
{
    using FlexCel.XlsAdapter;
    using FlightDeck.Domain;
    using FlightDeck.Tracker.Models;
    using System;
    using System.Collections.Generic;
    
    public class TrackerIndicesReader
    {
        private XlsFile xls;
        private XlHelper xl;

        public TrackerIndicesReader(XlsFile xls)
        {
            this.xls = xls;
            this.xl = new XlHelper(xls);
        }

        public IList<FinancialIndex> Read()
        {
            var indices = new List<FinancialIndex>(1000);

            xls.ActiveSheetByName = "Stats";
            var id = 1;
            var dates = new List<DateTime>();
            const int rowOffset = 5;

            for (var column = 2; column <= xls.ColCount; column++)
            {
                var values = new SortedList<DateTime, double>();
                for (var row = rowOffset; xls.GetCellValue(row, 1) != null; row++)
                {
                    if (dates.Count < xls.RowCount)
                        dates.Add(xl.GetDate(row, "A"));

                    var potentialValue = xls.GetCellValue(row, column) ?? .0;

                    var value = (double)potentialValue;
                    var date = dates[row - rowOffset];

                    values.Add(date, value);
                }
                var name = (string)xls.GetCellValue(4, column);
                if (!string.IsNullOrWhiteSpace(name))
                    indices.Add(new FinancialIndex(id++, name, values));
            }

            return indices;
        }
    }
}
