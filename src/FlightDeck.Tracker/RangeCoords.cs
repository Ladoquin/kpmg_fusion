﻿namespace FlightDeck.Tracker
{
    struct RangeCoords
    {
        public int Col { get; set; }
        public int Row { get; set; }
        public int EndCol { get; set; }
        public int EndRow { get; set; }
        public RangeCoords(int col, int row)
            : this(col, row, col, row)
        {
        }
        public RangeCoords(int col, int row, int endcol, int endrow)
            : this()
        {
            Col = col;
            Row = row;
            EndCol = endcol;
            EndRow = endrow;
        }
    }
}
