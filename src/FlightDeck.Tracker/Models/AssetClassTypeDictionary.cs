﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class AssetClassTypeDictionary
    {
        public Guid Id { get; set; }
        public AssetClassType Key { get; set; }
        public double Value { get; set; }

        public AssetClassTypeDictionary()
        {
            Id = Guid.NewGuid();
        }
        public AssetClassTypeDictionary(AssetClassType key, double value)
            : this()
        {
            Key = key;
            Value = value;
        }
    }
}
