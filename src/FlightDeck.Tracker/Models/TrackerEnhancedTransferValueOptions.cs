﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;

    public class TrackerEnhancedTransferValueOptions : EnhancedTransferValueOptions
    {
        public TrackerEnhancedTransferValueOptions()
            : this(0, 0, 0)
        {
        }

        public TrackerEnhancedTransferValueOptions(double equivalentTransferValue, double enhancementToCetv, double takeUpRate)
            : base(equivalentTransferValue, enhancementToCetv, takeUpRate)
        {
        }
    }
}
