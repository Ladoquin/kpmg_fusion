﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public class MemberStatusDictionary
    {
        public Guid Id { get; set; }
        public MemberStatus Key { get; set; }
        public double Value { get; set; }
        public virtual TrackerCashflow TrackerCashflow { get; set; }

        public MemberStatusDictionary()
        {
            Id = Guid.NewGuid();
        }
    }

    public static class MemberStatusDictionaryExtensions
    {
        public static IList<MemberStatusDictionary> ToMemberStatusDictionary(this IDictionary<MemberStatus, double> dic)
        {
            var l = new List<MemberStatusDictionary>(4);
            foreach (var key in dic.Keys)
                l.Add(new MemberStatusDictionary { Key = key, Value = dic[key] });
            return l;
        }
    }
}
