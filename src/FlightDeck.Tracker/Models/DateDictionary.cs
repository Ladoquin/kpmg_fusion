﻿namespace FlightDeck.Tracker.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class DateDictionary
    {
        public Guid Id { get; set; }
        public DateTime Key { get; set; }
        public double Value { get; set; }

        public DateDictionary()
        {
        }
        public DateDictionary(DateTime key, double value)
        {
            Id = Guid.NewGuid();
            Key = key;
            Value = value;
        }
    }

    public static class DateDictionaryExtensions
    {
        public static IDictionary<DateTime, double> ToDictionary(this IList<DateDictionary> values)
        {
            return values.ToDictionary(x => x.Key, x => x.Value);
        }
    }
}
