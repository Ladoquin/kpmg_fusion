﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class TrackerBasisAssumptions
    {
        public Guid Id { get; set; }
        public virtual IList<AssumptionTypeDictionary> LiabilityAssumptions { get; set; }
        public virtual TrackerLifeExpectancyAssumptions LifeExpectancy { get; set; }
        public virtual IList<IntKeyDictionary> PensionIncreases { get; set; }
        public virtual IList<AssetClassTypeDictionary> AssetAssumptions { get; set; }
        public virtual TrackerRecoveryPlanAssumptions RecoveryPlanOptions { get; set; }

        public TrackerBasisAssumptions()
            : this(null, null, null, null, null)
        {
        }

        public TrackerBasisAssumptions
            (
            IDictionary<AssumptionType, double> liabilityAssumptions,
            LifeExpectancyAssumptions lifeExpectancy,
            IDictionary<int, double> pensionIncreases,
            IDictionary<AssetClassType, double> assetAssumptions,
            TrackerRecoveryPlanAssumptions recoveryPlanOptions
            )
        {
            Id = Guid.NewGuid();
            LiabilityAssumptions = liabilityAssumptions == null ? null : liabilityAssumptions.Select(x => new AssumptionTypeDictionary(x.Key, x.Value)).ToList();
            LifeExpectancy = lifeExpectancy == null ? null : new TrackerLifeExpectancyAssumptions(lifeExpectancy.PensionerLifeExpectancy, lifeExpectancy.DeferredLifeExpectancy);
            PensionIncreases = pensionIncreases == null ? null : pensionIncreases.Select(x => new IntKeyDictionary(x.Key, x.Value)).ToList();
            AssetAssumptions = assetAssumptions == null ? null : assetAssumptions.Select(x => new AssetClassTypeDictionary(x.Key, x.Value)).ToList();
            RecoveryPlanOptions = recoveryPlanOptions;
        }

        public BasisAssumptions ToBasisAssumptions()
        {
            return new BasisAssumptions
                (
                    LiabilityAssumptions == null ? null : LiabilityAssumptions.ToDictionary(x => x.Key, x => x.Value),
                    LifeExpectancy,
                    PensionIncreases == null ? null : PensionIncreases.ToDictionary(x => x.Key, x => x.Value),
                    AssetAssumptions == null ? null : AssetAssumptions.ToDictionary(x => x.Key, x => x.Value),
                    RecoveryPlanOptions
                );
        }
    }
}
