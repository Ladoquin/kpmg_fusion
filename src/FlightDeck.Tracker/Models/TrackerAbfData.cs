﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class TrackerAbfData : AbfData
    {
        public Guid Id { get; set; }

        public TrackerAbfData()
            : this(0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public TrackerAbfData(double fundingDeficit, double annualPayments, double netCashPosition1, double proportion, double annualContribution, double netCashPosition2, double costSaving, double value)
            : base(fundingDeficit, annualPayments, netCashPosition1, proportion, annualContribution, netCashPosition2, costSaving, value)
        {
            Id = Guid.NewGuid();
        }
    }
}
