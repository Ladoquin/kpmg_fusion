﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class TrackerSurplusAnalysisData : SurplusAnalysisData
    {
        public Guid Id { get; set; }

        public TrackerSurplusAnalysisData()
            : this(0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public TrackerSurplusAnalysisData(double initialDeficit, double interestOnLiabilities, double marketChange,
            double liabExperience, double contributions, double assetReturn, double assetExperience, double endDeficit)
            : base(initialDeficit, interestOnLiabilities, marketChange, liabExperience, contributions, assetReturn, assetExperience, endDeficit)
        {
            Id = Guid.NewGuid();
        }
    }
}
