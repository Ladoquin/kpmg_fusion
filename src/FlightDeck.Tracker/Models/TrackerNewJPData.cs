﻿namespace FlightDeck.Tracker.Models
{
    using System;
    using System.Collections.Generic;
    
    public class TrackerNewJPData
    {
        public Guid Id { get; set; }
        public virtual IList<TrackerNewJPYearlyResults> Liab1 { get; set; }
        public virtual IList<TrackerNewJPYearlyResults> Liab2 { get; set; }
        public virtual IList<TrackerNewJPYearlyResults> Liab3 { get; set; }
        public virtual IList<TrackerNewJPYearlyResults> Assets1 { get; set; }
        public virtual IList<TrackerNewJPYearlyResults> Assets2 { get; set; }
        public double? FundingPremiumPre { get; set; }
        public double? FundingPremiumPost { get; set; }
        public double? SelfSufficiencyPremiumPre { get; set; }
        public double? SelfSufficiencyPremiumPost { get; set; }
        public double? BuyoutPremiumPre { get; set; }
        public double? BuyoutPremiumPost { get; set; }
        public virtual IList<BasisTypeDictionary> CurrentAssetsCrossOverPoints { get; set; }
        public virtual IList<BasisTypeDictionary> NewAssetsCrossOverPoints { get; set; }
        public virtual IList<int> TriggerYears { get; private set; }

        public TrackerNewJPData()
        {
            Id = Guid.NewGuid();
        }
    }
}
