﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class TrackerProfitLossForecast : ProfitLossForecast
    {
        public Guid Id { get; set; }
        public ValuationBasisType ValuationBasis { get; set; }

        public TrackerProfitLossForecast()
            : this(0, 0, 0, 0)
        {
        }

        public TrackerProfitLossForecast(double serviceCost, double schemeExpenses, double interestCharge, double interestCredit)
            : base(serviceCost, schemeExpenses, interestCharge, interestCredit)
        {
            Id = Guid.NewGuid();
        }
    }
}
