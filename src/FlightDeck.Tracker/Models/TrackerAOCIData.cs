﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain.Accounting;
    using System;

    public class TrackerAOCIDataItem
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public virtual AOCIData AOCIData { get; set; }

        public TrackerAOCIDataItem()
        {
        }

        public TrackerAOCIDataItem(DateTime date, AOCIData data)
        {
            Id = Guid.NewGuid();
            Date = date;
            AOCIData = data;
        }
    }
}
