﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Tracker.Reader;
    using System;
    using System.Collections.Generic;

    public class TrackerCashflow
    {
        public Guid Id { get; set; }
        public int Year { get; set; }
        public virtual ICollection<MemberStatusDictionary> MemberStatuses { get; set; }
        public virtual TrackerOutputs TrackerReader { get; set; }

        public TrackerCashflow()
            : this(0, null)
        {
        }
        public TrackerCashflow(int year, ICollection<MemberStatusDictionary> memberStatuses)
        {
            Id = Guid.NewGuid();
            Year = year;
            MemberStatuses = memberStatuses;
        }
    }
}
