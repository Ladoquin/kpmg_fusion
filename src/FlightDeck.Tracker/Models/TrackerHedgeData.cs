﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class TrackerHedgeData : HedgeData
    {
        public Guid Id { get; set; }
        public new TrackerHedgeBreakdownData Breakdown { get; set; }

        public TrackerHedgeData()
            : this(0, 0, 0, 0, 0, 0, 0)
        {
        }
        public TrackerHedgeData(double giltLDIOverlayInterestHedge, double giltLDIOverlayInflationHedge, double currLDIOverlayInterestHedge, double currLDIOverlayInflationHedge, double buyInInterestHedge, double buyInInflationHedge, double buyInLongevityHedge)
            : base(giltLDIOverlayInterestHedge, giltLDIOverlayInflationHedge, currLDIOverlayInterestHedge, currLDIOverlayInflationHedge, buyInInterestHedge, buyInInflationHedge, buyInLongevityHedge)
        {
            Id = Guid.NewGuid();
        }
    }
}
