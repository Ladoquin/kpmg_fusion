﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    
    public class TrackerBalanceData// : BalanceData
    {
        public Guid Id { get; set; }
        public int Year { get; set; }

        public TrackerBalanceData()
            : this(0, .0, .0)
        {
        }

        public TrackerBalanceData(double assets, double liabilities)
            : this(assets, liabilities, assets - liabilities)
        {
        }

        public TrackerBalanceData(double assets, double liabilities, double surplusDeficit)
            //: base(assets, liabilities, surplusDeficit)
        {
            Id = Guid.NewGuid();
            Assets = assets;
            Liabilities = liabilities;
            SurplusDeficit = surplusDeficit;
        }

        //TODO Arhghghghg, why is this needed, why won't this class just inherit off BalanceData and work with E.F. like all the other classes?!?!?!?
        public double Assets { get; set; }
        public double Liabilities { get; set; }
        public double SurplusDeficit { get; set; }

        public BalanceData ToBalanceData()
        {
            return new BalanceData(Assets, Liabilities);
        }
    }
}
