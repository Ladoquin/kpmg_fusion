﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;

    public class TrackerFlexibleReturnOptions : FlexibleReturnOptions
    {
        public TrackerFlexibleReturnOptions()
            : this(0, 0)
        {
        }

        public TrackerFlexibleReturnOptions(double equivalentTransferValue, double takeUpRate)
            : base(equivalentTransferValue, takeUpRate)
        {
        }
    }
}
