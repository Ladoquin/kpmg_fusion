﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class AssumptionTypeDictionary
    {
        public Guid Id { get; set; }
        public AssumptionType Key { get; set; }
        public double Value { get; set; }

        public AssumptionTypeDictionary()
        {
            Id = Guid.NewGuid();
        }
        public AssumptionTypeDictionary(AssumptionType key, double value)
            : this()
        {
            Key = key;
            Value = value;
        }
    }
}
