﻿namespace FlightDeck.Tracker.Models
{
    public class TrackerNewJPYearlyResults : IntKeyDictionary
    {
        public TrackerNewJPYearlyResults()
            : this(0, 0)
        {
        }

        public TrackerNewJPYearlyResults(int year, double value)
            : base(year, value)
        {
        }
    }
}
