﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain.Evolution;
    using System;

    public class TrackerAssetEvolutionItem : AssetEvolutionItem
    {
        public Guid Id { get; set; }

        public TrackerAssetEvolutionItem()
            : base(DateTime.MinValue, .0, .0, .0, .0, .0, .0, .0, .0, .0)
        {
        }

        public TrackerAssetEvolutionItem(
            DateTime date,
            double employeeContributions,
            double employerContributions,
            double totalBenefits,
            double netBenefits,
            double netIncome,
            double growth,
            double ldiInterestSwap,
            double ldiInflationSwap,
            double assetValue)
            : base(date, employeeContributions, employerContributions, totalBenefits, netBenefits, netIncome, growth, ldiInterestSwap, ldiInflationSwap, assetValue)
        {
            Id = Guid.NewGuid();
        }
    }
}
