﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    
    public class TrackerInsuranceParameters : InsuranceParameters
    {
        public TrackerInsuranceParameters()
            : this(0, 0)
        {
        }

        public TrackerInsuranceParameters(double pensionerLiabilityInsured, double nonpensionerLiabilityInsured)
            : base(pensionerLiabilityInsured, nonpensionerLiabilityInsured)
        {
        }
    }
}
