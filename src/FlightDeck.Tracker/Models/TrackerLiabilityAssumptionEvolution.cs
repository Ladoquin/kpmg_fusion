﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class TrackerLiabilityAssumptionEvolution
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public virtual IList<AssumptionTypeDictionary> Assumptions { get; set; }

        public TrackerLiabilityAssumptionEvolution()
        {
            Id = Guid.NewGuid();
        }
    }

    public static class TrackerLiabilityAssumptionEvolutionExtensions
    {
        public static IDictionary<DateTime, IDictionary<AssumptionType, double>> ToDictionary(this IList<TrackerLiabilityAssumptionEvolution> values)
        {
            return
                values
                    .ToDictionary(
                        x => x.Date,
                        x => (IDictionary<AssumptionType, double>)x.Assumptions.ToDictionary(
                            y => y.Key,
                            y => y.Value));
        }
    }
}
