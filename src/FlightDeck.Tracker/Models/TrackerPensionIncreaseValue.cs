﻿namespace FlightDeck.Tracker.Models
{
    using System;
    
    public class TrackerPensionIncreaseValue
    {
        public Guid Id { get; set; }
        public int PincReference { get; set; }
        public double Value { get; set; }

        public TrackerPensionIncreaseValue()
        {
            Id = Guid.NewGuid();
        }
    }
}
