﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    
    public class TrackerAbfOptions : AbfOptions
    {
        public TrackerAbfOptions()
            : this(0, Utils.AbfRecoveryPlanLength, Utils.AbfLength)
        {
        }

        public TrackerAbfOptions(double value, double recoveryPlanLength, double abfLength)
            : base(value, recoveryPlanLength, abfLength)
        {
        }
    }
}
