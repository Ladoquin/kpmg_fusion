﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class TrackerBuyinData : BuyinData
    {
        public Guid Id { get; set; }

        public TrackerBuyinData()
            : this(0, 0, 0, 0)
        {
        }

        public TrackerBuyinData(double uninsuredPenLiab, double uninsuredNonPenLiab, double penLiabToBeInsured, double nonPenLiabToBeInsured)
            : base(uninsuredPenLiab, uninsuredNonPenLiab, penLiabToBeInsured, nonPenLiabToBeInsured)
        {
            Id = Guid.NewGuid();
        }
    }
}
