﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Reader;
    using System;
    using System.ComponentModel.DataAnnotations;
    
    public class TrackerFundingData : FundingData
    {
        public Guid Id { get; set; }

        [Required]
        public virtual TrackerOutputs TrackerInstance { get; set; }

        public TrackerFundingData()
            : this(0, 0, 0, 0, 0, 0)
        {
        }

        public TrackerFundingData(double actives, double deferreds, double pensioners, double assets, double buyin, double deficit)
            : base(actives, deferreds, pensioners, assets, buyin, deficit)
        {
            Id = Guid.NewGuid();
        }
    }
}
