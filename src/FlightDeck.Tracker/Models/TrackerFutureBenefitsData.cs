﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerFutureBenefitsData : FutureBenefitsData
    {
        public Guid Id { get; set; }

        public TrackerFutureBenefitsData()
            : this(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public TrackerFutureBenefitsData(double annualCost, double contributionRate,
            double employeeRate, double employerRate, double annualCostChange,
            double employerRateChange, double liabilitiesChange, double serviceCostEstimate,
            double contributionRateEstimate, double estimatedBenefitReduction, double changeIn5Years, double changeIn10Years)
            : base(annualCost, contributionRate, employeeRate, employerRate, annualCostChange, employerRateChange, liabilitiesChange, serviceCostEstimate, contributionRateEstimate, estimatedBenefitReduction, changeIn5Years, changeIn10Years)
        {
            Id = Guid.NewGuid();
        }
    }
}
