﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain;
    
    public class TrackerLifeExpectancyAssumptions : LifeExpectancyAssumptions
    {
        public TrackerLifeExpectancyAssumptions()
            : this(0, 0)
        {
        }

        public TrackerLifeExpectancyAssumptions(double pensionerLifeExpectancy, double deferredLifeExpectancy)
            : base(pensionerLifeExpectancy, deferredLifeExpectancy)
        {
        }
    }
}
