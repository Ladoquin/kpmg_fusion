﻿namespace FlightDeck.Tracker.Models
{
    using System;
    using FlightDeck.Domain;
    
    public class TrackerSourceIndexProportion
    {
        public Guid Id { get; set; }
        public double Value { get; set; }
        public string IndexName { get; set; }

        public TrackerSourceIndexProportion()
        {
            Id = Guid.NewGuid();
        }
    }
}
