﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Reader;
    using System;

    public class TrackerData
    {       
        public Guid Id { get; set; }
        public string SchemeName { get; set; }
        public string SchemeRef { get; set; }
        public int ScenarioId { get; set; }
        public string BasisName { get; set; }
        public BasisType BasisType { get; set; }

        private DateTime? dateCreated = null;
        public DateTime DateCreated
        {
            get
            {
                return this.dateCreated.HasValue
                   ? this.dateCreated.Value
                   : DateTime.Now;
            }

            set { this.dateCreated = value; }
        }
        
        public virtual TrackerAssumptions Assumptions { get; set; }
        public virtual TrackerOutputs Results { get; set; }

        public TrackerData()
        {
            Id = Guid.NewGuid();
        }
    }
}
