﻿namespace FlightDeck.Tracker.Models
{
    using System;
    
    public class BasisAssumptionsDictionary
    {
        public Guid Id { get; set; }
        public string Key { get; set; }
        public virtual TrackerBasisAssumptions Value { get; set; }

        public BasisAssumptionsDictionary()
        {
            Id = Guid.NewGuid();
        }

        public BasisAssumptionsDictionary(string key, TrackerBasisAssumptions value)
            : this()
        {
            Key = key;
            Value = value;
        }
    }
}
