﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using FlightDeck.Tracker.Reader;
    using System;

    public class TrackerBasis
    {
        public int Id { get; set; }
        public int PensionSchemeId { get; set; }
        public int MasterBasisId { get; set; }
        public string DisplayName { get; set; }
        public BasisType Type { get; set; }
        public DateTime EffectiveDate { get; set; }

        public virtual TrackerOutputs TrackerInstance { get; set; }
    }
}
