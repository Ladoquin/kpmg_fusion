﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain;
    using System.Collections.Generic;

    public class CompositeIndexData
    {
        public string CiName { get; set; }
        public string CustomIndexType { get; set; }
        public double Rounding { get; set; }
        public Dictionary<FinancialIndex, double> SourceIndexProportion { get; set; }
    }
}
