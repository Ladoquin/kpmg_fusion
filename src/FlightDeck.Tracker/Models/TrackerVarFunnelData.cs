﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerVarFunnelData : VarFunnelData
    {
        public Guid Id { get; set; }
        public int Year { get; set; }

        public TrackerVarFunnelData()
            : this(0, 0, 0, 0, 0, 0)
        {
        }

        public TrackerVarFunnelData(int year, double percentile95, double percentile80, double median, double percentile20, double percentile5)
            : base(percentile95, percentile80, median, percentile20, percentile5)
        {
            Id = Guid.NewGuid();
            Year = year;
        }
    }
}
