﻿namespace FlightDeck.Tracker.Models
{
    using System;
    using FlightDeck.DomainShared;
    
    public class BasisTypeDictionary
    {
        public Guid Id { get; set; }
        public BasisType Key { get; set; }
        public double Value { get; set; }

        public BasisTypeDictionary()
            : this(BasisType.Accounting, 0)
        {
        }

        public BasisTypeDictionary(BasisType key, double value)
        {
            Id = Guid.NewGuid();
            Key = key;
            Value = value;
        }
    }
}
