﻿namespace FlightDeck.Tracker.Reader
{
    using FlightDeck.Domain;
using FlightDeck.Tracker.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

    public class TrackerOutputs
    {
        public Guid Id { get; private set; }

        [Required]
        public virtual TrackerData TrackerScenario { get; set; }

        public DateTime RefreshDate { get; set; }
        public DateTime AttributionStart { get; set; }
        public DateTime AttributionEnd { get; set; }        
        public virtual ICollection<TrackerCashflow> CashflowsBefore { get; set; }
        public virtual ICollection<TrackerCashflow> CashflowsAfter { get; set; }
        public virtual TrackerUserLiabilityData UserLiabs { get; set; }
        public virtual IList<TrackerLDIEvolutionItem> LDIEvolutionData { get; set; }
        public virtual IList<TrackerLiabilityAssumptionEvolution> LiabilityAssumptionEvolution { get; set; }
        public virtual IList<TrackerPensionIncreaseEvolutionItem> PensionIncreaseEvolution { get; set; }
        public virtual IList<TrackerLiabilityEvolutionItem> LiabilityEvolution { get; set; }
        public virtual IList<TrackerAssetEvolutionItem> AssetEvolution { get; set; }
        public virtual IList<DateDictionary> LiabilityExperience { get; set; }
        public virtual IList<DateDictionary> BuyinExperience { get; set; }
        public virtual IList<DateDictionary> AssetExperience { get; set; }
        public double BuyInAtAnalysisDate { get; set; }
        public virtual IList<TrackerHedgeBreakdownData> AssetHedgeBreakdownData { get; set; }
        public virtual IList<TrackerExpectedDeficitData> ExpectedDeficit { get; set; }
        public virtual TrackerSurplusAnalysisData SurplusAnalysisData { get; set; }
        public virtual IList<TrackerKeyValue> GrowthOverPeriodInfo { get; set; }
        public virtual IList<TrackerBalanceData> JourneyPlanBefore { get; set; }
        public virtual IList<TrackerBalanceData> JourneyPlanAfter { get; set; }
        public virtual IList<IntKeyDictionary> RecoveryPlanBefore { get; set; }
        public virtual IList<IntKeyDictionary> RecoveryPlanAfter { get; set; }
        public double RecoveryPlanPaymentRequired { get; set; }
        public virtual IList<TrackerCashflowData> AdjustedCashflows { get; set; }
        public virtual TrackerInvestmentStrategyData InvestmentStrategyResults { get; set; }
        public virtual TrackerFroData FRO { get; set; }
        public virtual TrackerEFroData EFRO { get; set; }
        public virtual TrackerEtvData ETV { get; set; }
        public virtual TrackerPIEInitData PIEInit { get; set; }
        public virtual TrackerPIEData PIE { get; set; }
        public virtual TrackerFutureBenefitsData FutureBenefits { get; set; }
        public virtual TrackerAbfData ABF { get; set; }
        public virtual TrackerHedgeBreakdownData HedgeBreakdown { get; set; }
        public virtual TrackerBuyinData BuyIn { get; set; }
        public double UserActLiabAfter { get; set; }
        public double UserDefLiabAfter { get; set; }
        public double UserPenLiabAfter { get; set; }
        public double MarketChange { get; set; }
        public double AssetsAtSED { get; set; }
        public double JPInvGrowth { get; set; }
        public double JP2InvGrowth { get; set; }
        public double JPAfterLiabDuration { get; set; }
        public double InsPenCost { get; set; }
        public double InsNonPenCost { get; set; }
        public double TotalInsurancePremiumPaid { get; set; }
        public double InsurancePointIncreaseAmount { get; set; }
        public double OutPerformanceMaxReturn { get; set; }
        public virtual TrackerGiltBasisLiabilityData GiltBasisLiability { get; set; }
        public virtual TrackerFundingData FundingLevel { get; set; }
        public virtual TrackerBalanceData BalanceSheetEstimate { get; set; }
        public virtual TrackerProfitLossData ProfitLoss { get; set; }
        public virtual IList<TrackerProfitLossForecast> ProfitLossForecast { get; set; }
        public virtual TrackerWaterfallData Waterfall { get; set; }
        public virtual IList<TrackerVarFunnelData> VarFunnel { get; set; }
        public virtual IList<TrackerAOCIDataItem> USGAAPTable { get; set; }
        public virtual IList<TrackerUSGAAPExpectedReturn> USGAAPExpectedReturns { get; set; }
        public virtual TrackerIAS19Disclosure IAS19DisclosureBefore { get; set; }
        public virtual TrackerIAS19Disclosure IAS19DisclosureAfter { get; set; }
        public virtual TrackerFRS17Disclosure FRS17DisclosureBefore { get; set; }
        public virtual TrackerFRS17Disclosure FRS17DisclosureAfter { get; set; }
        public virtual TrackerUSGAAPDisclosure USGAAPDisclosureBefore { get; set; }
        public virtual TrackerUSGAAPDisclosure USGAAPDisclosureAfter { get; set; }
        public virtual IList<TrackerIndexEvolution> CustomIndicesEvolution { get; set; }
        public virtual IList<TrackerCompositeIndexData> CompositeIndexParameters { get; set; }
        public virtual IList<IntKeyDictionary> GiltSpotRates { get; set; }
        public virtual TrackerNewJPData NewJP { get; set; }

        public TrackerOutputs() 
        {
            Id = Guid.NewGuid();
        }

        #region for debugging...

        /*
                        var foo = new TrackerData
            {
                BasisName = "Accounting",
                BasisType = DomainShared.BasisType.Accounting,
                Id = System.Guid.NewGuid(),
                ScenarioId = 1,
                SchemeName = "The Space01 Test Scheme",
                Results = new TrackerOutputs
                {
                    AttributionEnd = System.DateTime.Now,
                    AttributionStart = System.DateTime.Now,
                    ABF = new TrackerAbfData(),
                    AssetEvolution = new List<TrackerAssetEvolutionItem>(),
                    AssetExperience = new List<DateDictionary>(),
                    AssetHedgeBreakdownData = new List<TrackerHedgeBreakdownData> { new TrackerHedgeBreakdownData
                            {
                                Date = System.DateTime.Now,
                                Cashflows = new List<HedgeTypeDictionary>
                                    {
                                        new HedgeTypeDictionary(DomainShared.HedgeType.Inflation, new TrackerHedgeBreakdownItem(1, 2, 3, 4))
                                    },
                                CurrentBasis = new List<HedgeTypeDictionary>
                                    {
                                        new HedgeTypeDictionary(DomainShared.HedgeType.Inflation, new TrackerHedgeBreakdownItem(1, 2, 3, 4))
                                    }
                            }
                        },
                    BalanceSheetEstimate = new TrackerBalanceData(10000, 20000),
                    BuyIn = new TrackerBuyinData(),
                    BuyinExperience = new List<DateDictionary>(),
                    EFRO = new TrackerEFroData(),
                    ETV = new TrackerEtvData(),
                    FRO = new TrackerFroData(),
                    FRS17DisclosureAfter = new TrackerFRS17Disclosure(System.DateTime.Now, new DomainShared.DisclosureAssumptions(), new DomainShared.FRS17Disclosure.FRS17BalanceSheetAmountData(), new DomainShared.FRS17Disclosure.PresentValueSchemeLiabilityData(), new DomainShared.FRS17Disclosure.FRS17FairValueSchemeAssetChangesData(), new DomainShared.FRS17Disclosure.FRS17SchemeSurplusChangesData(), new DomainShared.FRS17Disclosure.FRS17PAndLForecastData()),
                    FRS17DisclosureBefore = new TrackerFRS17Disclosure(System.DateTime.Now, new DomainShared.DisclosureAssumptions(), new DomainShared.FRS17Disclosure.FRS17BalanceSheetAmountData(), new DomainShared.FRS17Disclosure.PresentValueSchemeLiabilityData(), new DomainShared.FRS17Disclosure.FRS17FairValueSchemeAssetChangesData(), new DomainShared.FRS17Disclosure.FRS17SchemeSurplusChangesData(), new DomainShared.FRS17Disclosure.FRS17PAndLForecastData()),
                    FundingLevel = new TrackerFundingData(),
                    FutureBenefits = new TrackerFutureBenefitsData(),
                    GiltBasisLiability = new TrackerGiltBasisLiabilityData(),
                    HedgeBreakdown = new TrackerHedgeBreakdownData
                            {
                                Date = System.DateTime.Now,
                                Cashflows = new List<HedgeTypeDictionary>
                                    {
                                        new HedgeTypeDictionary(DomainShared.HedgeType.Inflation, new TrackerHedgeBreakdownItem(1, 2, 3, 4))
                                    },
                                CurrentBasis = new List<HedgeTypeDictionary>
                                    {
                                        new HedgeTypeDictionary(DomainShared.HedgeType.Inflation, new TrackerHedgeBreakdownItem(1, 2, 3, 4))
                                    }
                            },
                    IAS19DisclosureAfter = new TrackerIAS19Disclosure(System.DateTime.Now, new DomainShared.DisclosureAssumptions(), new DomainShared.IAS19Disclosure.IAS19BalanceSheetAmountData(), new DomainShared.IAS19Disclosure.DefinedBenefitObligationData(), new DomainShared.IAS19Disclosure.IAS19FairValueSchemeAssetChangesData(), new DomainShared.IAS19Disclosure.IAS19SchemeSurplusChangesData(), new DomainShared.IAS19Disclosure.IAS19PAndLForecastData()),
                    IAS19DisclosureBefore = new TrackerIAS19Disclosure(System.DateTime.Now, new DomainShared.DisclosureAssumptions(), new DomainShared.IAS19Disclosure.IAS19BalanceSheetAmountData(), new DomainShared.IAS19Disclosure.DefinedBenefitObligationData(), new DomainShared.IAS19Disclosure.IAS19FairValueSchemeAssetChangesData(), new DomainShared.IAS19Disclosure.IAS19SchemeSurplusChangesData(), new DomainShared.IAS19Disclosure.IAS19PAndLForecastData()),
                    InvestmentStrategyResults = new TrackerInvestmentStrategyData(),
                    NewJP = new TrackerNewJPData(),
                    PIE = new TrackerPIEData(),
                    PIEInit = new TrackerPIEInitData(),
                    ProfitLoss = new TrackerProfitLossData(),
                    RefreshDate = System.DateTime.Now,
                    SurplusAnalysisData = new TrackerSurplusAnalysisData(),
                    UserLiabs = new TrackerUserLiabilityData(),
                    USGAAPDisclosureAfter = new TrackerUSGAAPDisclosure(System.DateTime.Now, new DomainShared.DisclosureAssumptions(), new DomainShared.DisclosureAssumptions(), new DomainShared.USGAAPDisclosure.USGAAPBalanceSheetAmountData(), new DomainShared.USGAAPDisclosure.ChangesInPresentValueData(), new DomainShared.USGAAPDisclosure.ChangesInFairValueData(), new DomainShared.USGAAPDisclosure.AccumulatedBenefitObligationData(), new DomainShared.USGAAPDisclosure.EstimatedPaymentData(), new DomainShared.USGAAPDisclosure.NetPeriodicPensionCostData(), new DomainShared.USGAAPDisclosure.ComprehensiveIncomeData(), new DomainShared.USGAAPDisclosure.ComprehensiveAccumulatedIncomeData(), new DomainShared.USGAAPDisclosure.NetPeriodicPensionCostForecastData()),
                    USGAAPDisclosureBefore = new TrackerUSGAAPDisclosure(System.DateTime.Now, new DomainShared.DisclosureAssumptions(), new DomainShared.DisclosureAssumptions(), new DomainShared.USGAAPDisclosure.USGAAPBalanceSheetAmountData(), new DomainShared.USGAAPDisclosure.ChangesInPresentValueData(), new DomainShared.USGAAPDisclosure.ChangesInFairValueData(), new DomainShared.USGAAPDisclosure.AccumulatedBenefitObligationData(), new DomainShared.USGAAPDisclosure.EstimatedPaymentData(), new DomainShared.USGAAPDisclosure.NetPeriodicPensionCostData(), new DomainShared.USGAAPDisclosure.ComprehensiveIncomeData(), new DomainShared.USGAAPDisclosure.ComprehensiveAccumulatedIncomeData(), new DomainShared.USGAAPDisclosure.NetPeriodicPensionCostForecastData()),
                    Waterfall = new TrackerWaterfallData(),
                    RecoveryPlanAfter = new List<IntKeyDictionary> { new IntKeyDictionary(1, 234.22), new IntKeyDictionary(2, 21243.51) },
                    RecoveryPlanBefore = new List<IntKeyDictionary> { new IntKeyDictionary(1, 3245235), new IntKeyDictionary(2, 1112.23) },
                    JourneyPlanAfter = new List<TrackerBalanceData> { new TrackerBalanceData(23.64, 98983.32), new TrackerBalanceData(98375.23, 235.23) },
                    JourneyPlanBefore = new List<TrackerBalanceData> { new TrackerBalanceData(1, 2, 3), new TrackerBalanceData(4, 5, 6), new TrackerBalanceData(7, 8, 9) }
                }
            };
            new TrackerDataRepository().Save(foo);

         */

        #endregion
    }
}