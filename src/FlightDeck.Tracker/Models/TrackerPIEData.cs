﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerPIEData : PieData
    {
        public Guid Id { get; set; }

        public TrackerPIEData()
            : this(null, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public double LiabilityBefore { get; set; }
        public double LiabilityAfter { get; set; }

        public TrackerPIEData(BeforeAfter<double> liability, double liabilityExcludingLevelPensions, double liabilityChange, double eligibleForExchange,
            double zeroIncreases, double valueOfIncreases1, double valueOfIncreases2, double pensionerLiabIncrease,
            double costNeutralUplift, double valueShared)
            : base(liability, liabilityExcludingLevelPensions, liabilityChange, eligibleForExchange, zeroIncreases, valueOfIncreases1, valueOfIncreases2, pensionerLiabIncrease, costNeutralUplift, valueShared)
        {
            Id = Guid.NewGuid();
            if (liability != null)
            {
                LiabilityBefore = liability.Before;
                LiabilityAfter = liability.After;
            }
        }

        public PieData ToPieData()
        {
            return new PieData
                (
                    new BeforeAfter<double>(LiabilityBefore, LiabilityAfter),
                    LiabilityExcludingLevelPensions, LiabilityChange, EligibleForExchange, ZeroIncreases, ValueOfIncreases1, ValueOfIncreases2, PensionerLiabIncrease, CostNeutralUplift, ValueShared
                );
        }
    }
}
