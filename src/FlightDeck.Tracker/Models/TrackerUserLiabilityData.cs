﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerUserLiabilityData : UserLiabilityData, IEquatable<UserLiabilityData>
    {
        public Guid Id { get; set; }

        public TrackerUserLiabilityData()
            : this(0, 0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public TrackerUserLiabilityData(double activePast, double deferred, double pensioners, double activeFuture, double actDuration, double defDuration, double penDuration, double deferredBuyin, double pensionerBuyin)
            : base(activePast, deferred, pensioners, activeFuture, actDuration, defDuration, penDuration, deferredBuyin, pensionerBuyin)
        {
            Id = Guid.NewGuid();
        }
    }
}
