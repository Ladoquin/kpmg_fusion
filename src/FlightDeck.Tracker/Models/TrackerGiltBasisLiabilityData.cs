﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class TrackerGiltBasisLiabilityData : GiltBasisLiabilityData
    {
        public Guid Id { get; set; }

        public TrackerGiltBasisLiabilityData()
            : this(0, 0, 0, 0, 0, 0, null)
        {
        }

        public new TrackerHedgeData HedgeData { get; private set; }

        public TrackerGiltBasisLiabilityData(double totalLiab, double liabDuration, double liabPV01, double liabIE01, double buyinPV01, double buyinIE01, TrackerHedgeData hedgeData)
            : base(totalLiab, liabDuration, liabPV01, liabIE01, buyinPV01, buyinIE01, hedgeData)
        {
            Id = Guid.NewGuid();
            this.HedgeData = hedgeData == null ? null : new TrackerHedgeData(hedgeData.GiltLDIOverlayInterestHedge, hedgeData.GiltLDIOverlayInflationHedge, hedgeData.CurrLDIOverlayInterestHedge, hedgeData.CurrLDIOverlayInflationHedge, hedgeData.BuyInInterestHedge, hedgeData.BuyInInflationHedge, hedgeData.BuyInLongevityHedge);
        }
    }
}
