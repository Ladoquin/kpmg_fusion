﻿namespace FlightDeck.Tracker.Models
{
    using System;
    using System.Collections.Generic;

    public class TrackerIndexEvolution
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual IList<DateDictionary> Values { get; set; }

        public TrackerIndexEvolution()
        {
            Id = Guid.NewGuid();
        }
    }
}
