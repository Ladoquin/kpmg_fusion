﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerFRS17Disclosure : FRS17Disclosure
    {
        public Guid Id { get; set; }

        public TrackerFRS17Disclosure()
            : this(DateTime.MinValue, null, null, null, null, null, null)
        {
        }

        public TrackerFRS17Disclosure(DateTime date, DisclosureAssumptions assumptions, FRS17Disclosure.FRS17BalanceSheetAmountData balanceSheetAmounts, PresentValueSchemeLiabilityData schemeLiabilityChanges, FRS17Disclosure.FRS17FairValueSchemeAssetChangesData fairValueSchemeAssetChanges, FRS17Disclosure.FRS17SchemeSurplusChangesData schemeSurplusChanges, FRS17Disclosure.FRS17PAndLForecastData pAndLForecast)
            : base(date, assumptions, balanceSheetAmounts, schemeLiabilityChanges, fairValueSchemeAssetChanges, schemeSurplusChanges, pAndLForecast)
        {
            Id = Guid.NewGuid();
        }
    }
}
