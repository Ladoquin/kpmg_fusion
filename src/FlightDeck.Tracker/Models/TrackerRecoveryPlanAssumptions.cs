﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    
    public class TrackerRecoveryPlanAssumptions : RecoveryPlanAssumptions
    {
        public TrackerRecoveryPlanAssumptions()
            : this(0, 0, 0, 0, 0, false, 0, 0)
        {
        }

        public TrackerRecoveryPlanAssumptions(double investmentGrowth, double discountIncrement, double lumpSumPayment, double maxLumpSumPayment, double increases, bool isGrowthFixed, int startMonth, int term)
            : base(investmentGrowth, discountIncrement, lumpSumPayment, maxLumpSumPayment, increases, isGrowthFixed, startMonth, term)
        {
        }
    }
}
