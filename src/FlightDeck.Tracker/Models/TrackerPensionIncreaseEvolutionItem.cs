﻿namespace FlightDeck.Tracker.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class TrackerPensionIncreaseEvolutionItem
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public virtual IList<TrackerPensionIncreaseValue> Pincs { get; set; }

        public TrackerPensionIncreaseEvolutionItem()
        {
            Id = Guid.NewGuid();
        }
    }

    public static class TrackerPensionIncreaseEvolutionItemExtensions
    {
        public static IDictionary<DateTime, IDictionary<int, double>> ToDictionary(this IList<TrackerPensionIncreaseEvolutionItem> values)
        {
            return values
                .ToDictionary(
                    x => x.Date,
                    x => (IDictionary<int, double>)x.Pincs.ToDictionary(
                        y => y.PincReference,
                        y => y.Value));
        }
    }
}
