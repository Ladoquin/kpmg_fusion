﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain;
    using System;

    public class TrackerPIEInitData : PIEInitData
    {
        public Guid Id { get; set; }

        public TrackerPIEInitData()
            : this(.0, .0)
        {
        }

        public TrackerPIEInitData(double penLiab, double valueOfIncs)
            : base(penLiab, valueOfIncs)
        {
            Id = Guid.NewGuid();
        }
    }
}
