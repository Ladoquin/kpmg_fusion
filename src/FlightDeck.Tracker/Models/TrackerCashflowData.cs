﻿using FlightDeck.Domain;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightDeck.Tracker.Models
{
    public class TrackerCashflowData : CashflowData
    {
        [Key]
        public Guid Id2 { get; set; }

        public TrackerCashflowData() 
            : this(0, 0, MemberStatus.ActiveFuture, 0, 0, RevaluationType.None, 0, BenefitType.LumpSum, null, 0, 0, false, null)
        {
        }

        public new IList<IntKeyDictionary> Cashflow
        {
            get
            {
                return base.Cashflow.Select(x => new IntKeyDictionary(x.Key, x.Value)).ToList();
            }
            set
            {
                base.Cashflow = value.ToDictionary(x => x.Key, x => x.Value);
            }
        }

        public TrackerCashflowData(
            int id,
            int basisId,
            MemberStatus memberStatus,
            double minAge,
            double maxAge,
            RevaluationType revaluationType,
            int retirementAge,
            BenefitType benefitType,
            Dictionary<int, double> cashflow,
            int beginYear,
            int endYear,
            bool isBuyin,
            int? pensionIncreaseReference)
            : base(id, basisId, memberStatus, minAge, maxAge, revaluationType, retirementAge, benefitType, cashflow, beginYear, endYear, isBuyin, pensionIncreaseReference)
        {
            Id2 = Guid.NewGuid();
        }

        //public CashflowData ToCashflowData()
        //{
        //    return new CashflowData(Id, BasisId, MemberStatus, MinAge, MaxAge, RevaluationType, RetirementAge, BenefitType, null, BeginYear, EndYear, IsBuyIn, PensionIncreaseReference);
        //}
    }
}
