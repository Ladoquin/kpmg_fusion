﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerEtvData : EtvData
    {
        public Guid Id { get; set; }

        public TrackerEtvData()
            : this(0, 0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public TrackerEtvData(double deferredLiabilities, double cetvSize, double equivalentEtv, double cetvPaid, double etvPaid, double liabilityDischarged, double costToCompany, double changeInPosition, double assetsDisinvested)
            : base(deferredLiabilities, cetvSize, equivalentEtv, cetvPaid, etvPaid, liabilityDischarged, costToCompany, changeInPosition, assetsDisinvested)
        {
            Id = Guid.NewGuid();
        }
    }
}
