﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TrackerEFroData : EFroData
    {
        public Guid Id { get; set; }

        public TrackerEFroData()
            : this(null, null, 0, 0, 0, 0, 0)
        {            
        }

        public virtual new IList<IntKeyDictionary> ActiveTransferValuesPayable { get; set; }
        public virtual new IList<IntKeyDictionary> DeferredTransferValuesPayable { get; set; }

        public TrackerEFroData(IDictionary<int, double> activeTransferValuesPayable, IDictionary<int, double> deferredTransferValuesPayable,
            double cetvPaid, double liabilityDischarged, double changeInActivesPastLiabilitiesDischarged, double changeInDeferredsLiabilitiesDischarged, double nonPensionersLiabilities)
            : base(activeTransferValuesPayable, deferredTransferValuesPayable, cetvPaid, liabilityDischarged, changeInActivesPastLiabilitiesDischarged, changeInDeferredsLiabilitiesDischarged, nonPensionersLiabilities)
        {
            Id = Guid.NewGuid();
            ActiveTransferValuesPayable = activeTransferValuesPayable == null ? null : activeTransferValuesPayable.Select(x => new IntKeyDictionary(x.Key, x.Value)).ToList();
            DeferredTransferValuesPayable = deferredTransferValuesPayable == null ? null : deferredTransferValuesPayable.Select(x => new IntKeyDictionary(x.Key, x.Value)).ToList();
        }

        public EFroData ToEfroData()
        {
            return new EFroData
               (
                   ActiveTransferValuesPayable != null ? ActiveTransferValuesPayable.ToDictionary(x => x.Key, x => x.Value) : new Dictionary<int, double>(),
                   DeferredTransferValuesPayable != null ? DeferredTransferValuesPayable.ToDictionary(x => x.Key, x => x.Value) : new Dictionary<int, double>(),
                   CetvPaid,
                   LiabilityDischarged,
                   ChangeInActivesPastLiabilitiesDischarged,
                   ChangeInDeferredsLiabilitiesDischarged,
                   NonPensionersLiabilities
               );
        }
    }
}
