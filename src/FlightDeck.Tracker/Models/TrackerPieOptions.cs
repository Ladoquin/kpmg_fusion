﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;

    public class TrackerPieOptions : PieOptions
    {
        public TrackerPieOptions()
            : this(0, 0, 0)
        {
        }

        public TrackerPieOptions(double portionEligible, double actualUplift, double takeUpRate)
            : base(portionEligible, actualUplift, takeUpRate)
        {
        }
    }
}
