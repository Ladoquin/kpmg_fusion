﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TrackerHedgeBreakdownData
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public virtual IList<HedgeTypeDictionary> CurrentBasis { get; set; }
        public virtual IList<HedgeTypeDictionary> Cashflows { get; set; }

        public TrackerHedgeBreakdownData()
        {
            Id = Guid.NewGuid();
        }

        public HedgeBreakdownData ToHedgeBreakdownData()
        {
            return new HedgeBreakdownData
                {
                    Cashflows = Cashflows != null ? Cashflows.ToDictionary(x => x.Key, x =>  (HedgeBreakdownItem)x.Value) : null,
                    CurrentBasis = CurrentBasis != null ? CurrentBasis.ToDictionary(x => x.Key, x => (HedgeBreakdownItem)x.Value) : null
                };
        }
    }
}
