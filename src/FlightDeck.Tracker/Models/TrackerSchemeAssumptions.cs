﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TrackerSchemeAssumptions
    {
        public Guid Id { get; set; }
        public FROType FroType { get; set; }
        public virtual TrackerFlexibleReturnOptions FroOptions { get; set; }
        public virtual TrackerEmbeddedFlexibleReturnOptions EfroOptions { get; set; }
        public virtual TrackerEnhancedTransferValueOptions EtvOptions { get; set; }
        public virtual TrackerPieOptions PieOptions { get; set; }
        public virtual TrackerFutureBenefitsParameters FutureBenefitOptions { get; set; }
        public virtual IList<AssetClassTypeDictionary> InvestmentStrategyAssetAllocation { get; set; }
        public virtual TrackerInvestmentStrategyOptions InvestmentStrategyOptions { get; set; }
        public virtual TrackerAbfOptions AbfOptions { get; set; }
        public virtual TrackerInsuranceParameters InsuranceOptions { get; set; }
        public RecoveryPlanType RecoveryPlanOptions { get; set; }
        public virtual TrackerWaterfallOptions WaterfallOptions { get; set; }

        public TrackerSchemeAssumptions()
            : this(FROType.Bulk, null, null, null, null, null, null, null, null, null, RecoveryPlanType.Current, null)
        {
        }

        public TrackerSchemeAssumptions
            (
            FROType froType,
            FlexibleReturnOptions froOptions,
            EmbeddedFlexibleReturnOptions efroOptions,
            EnhancedTransferValueOptions etvOptions,
            PieOptions pieOptions,
            FutureBenefitsParameters futureBenefitOptions,
            IDictionary<AssetClassType, double> investmentStrategyAssetAllocation,
            InvestmentStrategyOptions investmentStrategyOptions,
            AbfOptions abfOptions,
            InsuranceParameters insuranceOptions,
            RecoveryPlanType recoveryPlanOptions,
            WaterfallOptions waterfallOptions
            )
        {
            Id = Guid.NewGuid();
            FroType = froType;
            FroOptions = froOptions == null ? null : new TrackerFlexibleReturnOptions(froOptions.EquivalentTansferValue, froOptions.TakeUpRate);
            EfroOptions = efroOptions == null ? null : new TrackerEmbeddedFlexibleReturnOptions(efroOptions.AnnualTakeUpRate, efroOptions.EmbeddedFroBasisChange);
            EtvOptions = etvOptions == null ? null : new TrackerEnhancedTransferValueOptions(etvOptions.EquivalentTransferValue, etvOptions.EnhancementToCetv, etvOptions.TakeUpRate);
            PieOptions = pieOptions == null ? null : new TrackerPieOptions(pieOptions.PortionEligible, pieOptions.ActualUplift, pieOptions.TakeUpRate);
            FutureBenefitOptions = futureBenefitOptions == null ? null : new TrackerFutureBenefitsParameters(futureBenefitOptions.MemberContributionsIncrease, futureBenefitOptions.AdjustmentType, futureBenefitOptions.AdjustmentRate);
            InvestmentStrategyAssetAllocation = investmentStrategyAssetAllocation == null ? null : investmentStrategyAssetAllocation.Select(x => new AssetClassTypeDictionary(x.Key, x.Value)).ToList();
            InvestmentStrategyOptions = investmentStrategyOptions == null ? null : new TrackerInvestmentStrategyOptions(investmentStrategyOptions.CashInjection, investmentStrategyOptions.SyntheticCredit, investmentStrategyOptions.SyntheticEquity, investmentStrategyOptions.LDIInForce, investmentStrategyOptions.InterestHedge, investmentStrategyOptions.InflationHedge);
            AbfOptions = abfOptions == null ? null : new TrackerAbfOptions(abfOptions.Value, abfOptions.RecoveryPlanLength, abfOptions.AbfLength);
            InsuranceOptions = insuranceOptions == null ? null : new TrackerInsuranceParameters(insuranceOptions.PensionerLiabilityPercInsured, insuranceOptions.NonpensionerLiabilityPercInsured);
            RecoveryPlanOptions = recoveryPlanOptions;
            WaterfallOptions = waterfallOptions == null ? null : new TrackerWaterfallOptions(waterfallOptions.CI, waterfallOptions.Time);
        }

        public SchemeAssumptions ToSchemeAssumptions()
        {
            return new SchemeAssumptions
                (
                    FroType,
                    FroOptions,
                    EfroOptions,
                    EtvOptions,
                    PieOptions,
                    FutureBenefitOptions,
                    InvestmentStrategyAssetAllocation == null ? null : InvestmentStrategyAssetAllocation.ToDictionary(x => x.Key, x => x.Value),
                    InvestmentStrategyOptions,
                    AbfOptions,
                    InsuranceOptions,
                    RecoveryPlanOptions,
                    WaterfallOptions
                );
        }
    }
}
