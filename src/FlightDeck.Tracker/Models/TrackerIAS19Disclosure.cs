﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerIAS19Disclosure : IAS19Disclosure
    {
        public Guid Id { get; set; }

        public TrackerIAS19Disclosure()
            : this(DateTime.MinValue, null, null, null, null, null, null)
        {
        }

        public TrackerIAS19Disclosure(DateTime date, DisclosureAssumptions assumptions, IAS19Disclosure.IAS19BalanceSheetAmountData balanceSheetAmounts, DefinedBenefitObligationData definedBenefitObligation, IAS19Disclosure.IAS19FairValueSchemeAssetChangesData fairValueSchemeAssetChanges, IAS19Disclosure.IAS19SchemeSurplusChangesData schemeSurplusChanges, IAS19Disclosure.IAS19PAndLForecastData pAndLForecast)
            : base(date, assumptions, balanceSheetAmounts, definedBenefitObligation, fairValueSchemeAssetChanges, schemeSurplusChanges, pAndLForecast)
        {
            Id = Guid.NewGuid();
        }
    }
}
