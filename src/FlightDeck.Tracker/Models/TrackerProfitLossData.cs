﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain;
    using System;

    public class TrackerProfitLossData : ProfitLossData
    {
        public Guid Id { get; set; }

        public TrackerProfitLossData()
            : this(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public TrackerProfitLossData(
            double salaryRoll,
            double accrualCost,
            double totalRate,
            double employerCost,
            double serviceCostFRS,
            double serviceCostIAS,
            double benefitsPaid,
            double employerConts,
            double deficitConts,
            double memberConts,
            double benefitsPaidInsured,
            double netInvesments)
            : base(salaryRoll, accrualCost, totalRate, employerCost, serviceCostFRS, serviceCostIAS, benefitsPaid, employerConts, deficitConts, memberConts, benefitsPaidInsured, netInvesments)
        {
            Id = Guid.NewGuid();
        }
    }
}
