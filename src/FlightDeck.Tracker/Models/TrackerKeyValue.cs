﻿namespace FlightDeck.Tracker.Models
{
    using System;

    public class TrackerKeyValue
    {
        public Guid Id { get; set; }
        public string Key { get; set; }
        public double? Value { get; set; }

        public TrackerKeyValue()
        {
            Id = Guid.NewGuid();
        }
        public TrackerKeyValue(string key, double? value)
            : this()
        {
            Key = key;
            Value = value;
        }
    }
}
