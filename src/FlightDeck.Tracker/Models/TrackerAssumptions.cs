﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    
    public class TrackerAssumptions
    {
        public Guid Id { get; set; }
        public virtual TrackerSchemeAssumptions SchemeAssumptions { get; set; }
        public virtual TrackerBasisAssumptions BasisAssumptions { get; set; }
        public virtual TrackerJourneyPlanOptions JourneyPlanAssumptions { get; set; }
        public virtual IList<BasisAssumptionsDictionary> StoredUserAssumptions { get; set; }

        public TrackerAssumptions()
            : this(null, null, null)
        {
        }
        public TrackerAssumptions(TrackerSchemeAssumptions schemeAssumptions, TrackerBasisAssumptions basisAssumptions, TrackerJourneyPlanOptions journeyPlanAssumptions)
        {
            Id = Guid.NewGuid();
            StoredUserAssumptions = new List<BasisAssumptionsDictionary>();
            SchemeAssumptions = schemeAssumptions;
            BasisAssumptions = basisAssumptions;
            JourneyPlanAssumptions = journeyPlanAssumptions;
        }
    }
}
