﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain.LDI;
    using System;

    public class TrackerLDIEvolutionItem : LDIEvolutionItem
    {
        public Guid Id { get; set; }

        public TrackerLDIEvolutionItem()
            : this(DateTime.MinValue, .0, .0)
        {
        }

        public TrackerLDIEvolutionItem(DateTime date, double ldiInterestSwap, double ldiInflationSwap)
            : base(date, ldiInterestSwap, ldiInflationSwap)
        {
            Id = Guid.NewGuid();
        }
    }
}
