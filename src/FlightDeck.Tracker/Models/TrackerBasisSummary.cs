﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    class TrackerBasisSummary
    {
        public string Name { get; set; }
        public BasisType Type { get; set; }
        public DateTime EffectiveDate { get; set; }

        public TrackerBasisSummary(string name, BasisType type, DateTime effDate)
        {
            Name = name;
            Type = type;
            EffectiveDate = effDate;
        }
    }
}
