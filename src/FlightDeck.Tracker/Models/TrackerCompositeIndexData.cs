﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public class TrackerCompositeIndexData : CompositeIndexData
    {
        public Guid Id { get; set; }

        public TrackerCompositeIndexData()
        {
            Id = Guid.NewGuid();
        }

        public virtual new IList<TrackerSourceIndexProportion> SourceIndexProportion { get; set; }
    }
}
