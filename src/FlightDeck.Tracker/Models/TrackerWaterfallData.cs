﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerWaterfallData : WaterfallData
    {
        public Guid Id { get; set; }

        public TrackerWaterfallData()
            : this(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public TrackerWaterfallData(double liabRiskInterestInflation,
            double liabRiskIncHedgingAssets,
            double interestInflationHedging,
            double longevity,
            double longevityIncBuyin,
            double longevityHedging,
            double creditRisk,
            double equityRisk,
            double otherGrowthRisk,
            double totalRisk,
            double totalStdDeviation)
            : base(liabRiskInterestInflation, liabRiskIncHedgingAssets, interestInflationHedging, longevity, longevityIncBuyin, longevityHedging, creditRisk, equityRisk, otherGrowthRisk, totalRisk, totalStdDeviation)
        {
            Id = Guid.NewGuid();
        }
    }
}
