﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class TrackerExpectedDeficitData : ExpectedDeficitData
    {
        public Guid Id { get; set; }

        public TrackerExpectedDeficitData()
            : this(DateTime.MinValue, .0, .0, .0, .0)
        {
        }

        public TrackerExpectedDeficitData(DateTime date, double assets, double liabilities, double deficit, double fundingLevel)
            : base(date, assets, liabilities, deficit, fundingLevel)
        {
            Id = Guid.NewGuid();
        }
    }
}
