﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;

    public class TrackerEmbeddedFlexibleReturnOptions : EmbeddedFlexibleReturnOptions
    {
        public TrackerEmbeddedFlexibleReturnOptions()
            : this(0, 0)
        {
        }

        public TrackerEmbeddedFlexibleReturnOptions(double takeUpRate, double embeddedFroBasisChange)
            : base(takeUpRate, embeddedFroBasisChange)
        {
        }
    }
}
