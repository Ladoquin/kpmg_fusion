﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    
    public class TrackerFutureBenefitsParameters : FutureBenefitsParameters
    {
        public TrackerFutureBenefitsParameters()
            : this(0, BenefitAdjustmentType.None, 0)
        {
        }
        public TrackerFutureBenefitsParameters(double memberContributionsIncrease, BenefitAdjustmentType adjustmentType, double adjustmentRate)
            : base(memberContributionsIncrease, adjustmentType, adjustmentRate)
        {
        }
    }
}
