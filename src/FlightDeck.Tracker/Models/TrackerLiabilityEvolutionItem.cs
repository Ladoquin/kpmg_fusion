﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain.Evolution;
    using System;
    
    public class TrackerLiabilityEvolutionItem : LiabilityEvolutionItem
    {
        public Guid Id { get; set; }

        public TrackerLiabilityEvolutionItem()
            : this(DateTime.MinValue, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0, .0)
        {
        }

        public TrackerLiabilityEvolutionItem(
            DateTime date,
            double act,
            double def,
            double pen,
            double fut,
            double liabs,
            double defBuyIn,
            double penBuyIn,
            double discPre,
            double discPost,
            double discPen,
            double salInc,
            double rpi,
            double cpi,
            double pinc1,
            double pinc2,
            double pinc3,
            double pinc4,
            double pinc5,
            double pinc6,
            double actDuration,
            double defDuration,
            double penDuration,
            double futDuration)
            : base(date, act, def, pen, fut, liabs, defBuyIn, penBuyIn, discPre, discPost, discPen, salInc, rpi, cpi, pinc1, pinc2, pinc3, pinc4, pinc5, pinc6, actDuration, defDuration, penDuration, futDuration)
        {
            Id = Guid.NewGuid();
        }
    }
}
