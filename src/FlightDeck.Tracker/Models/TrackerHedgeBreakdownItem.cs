﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class TrackerHedgeBreakdownItem : HedgeBreakdownItem
    {
        public Guid Id { get; set; }

        public TrackerHedgeBreakdownItem()
            : this(0, 0, 0, 0)
        {
        }

        public TrackerHedgeBreakdownItem(double physicalAssets, double buyin, double ldiOverlay, double total)
            : base(physicalAssets, buyin, ldiOverlay, total)
        {
            Id = Guid.NewGuid();
        }
    }
}
