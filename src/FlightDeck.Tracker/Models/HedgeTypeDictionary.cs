﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class HedgeTypeDictionary
    {
        public Guid Id { get; set; }
        public HedgeType Key { get; set; }
        public virtual TrackerHedgeBreakdownItem Value { get; set; }

        public HedgeTypeDictionary()
            : this(HedgeType.Inflation, null)
        {
        }

        public HedgeTypeDictionary(HedgeType key, HedgeBreakdownItem value)
        {
            Id = Guid.NewGuid();
            Key = key;
            Value = value == null ? null : new TrackerHedgeBreakdownItem(value.PhysicalAssets, value.Buyin, value.LDIOverlay, value.Total);
        }
    }
}
