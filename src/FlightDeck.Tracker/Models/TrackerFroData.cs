﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerFroData : FroData
    {
        public Guid Id { get; set; }

        public TrackerFroData()
            : this(0, 0, 0, 0, 0)
        {
        }

        public TrackerFroData(double sizeOfTV, double cetvPaid, double liabilityDischarged, double defferedLiability, double over55)
            : base(sizeOfTV, cetvPaid, liabilityDischarged, defferedLiability, over55)
        {
            Id = Guid.NewGuid();
        }
    }
}
