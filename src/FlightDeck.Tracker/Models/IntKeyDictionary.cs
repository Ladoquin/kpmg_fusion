﻿namespace FlightDeck.Tracker.Models
{
    using System;

    public class IntKeyDictionary
    {
        public Guid Id { get; set; }
        public int Key { get; set; }
        public double Value { get; set; }

        public IntKeyDictionary()
            : this(0, 0)
        {
        }

        public IntKeyDictionary(int key, double value)
        {
            Id = Guid.NewGuid();
            Key = key;
            Value = value;
        }
    }
}
