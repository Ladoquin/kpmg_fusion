﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    
    public class TrackerInvestmentStrategyOptions : InvestmentStrategyOptions
    {
        public TrackerInvestmentStrategyOptions()
            : this(0, 0, 0, false, 0, 0)
        {
        }

        public TrackerInvestmentStrategyOptions(double cashInjection, double syntheticCredit, double syntheticEquity, bool ldiInForce, double interestHedge, double inflationHedge)
            : base(cashInjection, syntheticCredit, syntheticEquity, ldiInForce, interestHedge, inflationHedge)
        {
        }
    }
}
