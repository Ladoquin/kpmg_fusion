﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;
    
    public class TrackerUSGAAPDisclosure : USGAAPDisclosure
    {
        public Guid Id { get; set; }

        public TrackerUSGAAPDisclosure()
            : this(DateTime.MinValue, null, null, null, null, null, null, null, null, null, null, null)
        {
        }

        public TrackerUSGAAPDisclosure(DateTime date, DisclosureAssumptions assumptions1, DisclosureAssumptions assumptions2, USGAAPDisclosure.USGAAPBalanceSheetAmountData balanceSheetAmounts, ChangesInPresentValueData changesInPresentValue, ChangesInFairValueData changesInFairValue, AccumulatedBenefitObligationData accumulatedBenefitObligation, EstimatedPaymentData estimatedPayments, NetPeriodicPensionCostData netPeriodicPensionCost, ComprehensiveIncomeData comprehensiveIncome, ComprehensiveAccumulatedIncomeData accumulatedComprehensiveIncome, NetPeriodicPensionCostForecastData netPeriodPensionCostForecast)
            : base(date, assumptions1, assumptions2, balanceSheetAmounts, changesInPresentValue, changesInFairValue, accumulatedBenefitObligation, estimatedPayments, netPeriodicPensionCost, comprehensiveIncome, accumulatedComprehensiveIncome, netPeriodPensionCostForecast)
        {
            Id = Guid.NewGuid();
        }
    }
}
