﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.Domain.Accounting;
    using System;
    
    public class TrackerUSGAAPExpectedReturn
    {
        public Guid Id { get; set; }
        public USGAAPExpectedReturnsType USGAAPExpectedReturnsType { get; set; }
        public double Value { get; set; }

        public TrackerUSGAAPExpectedReturn()
        {
            Id = Guid.NewGuid();
        }
    }
}
