﻿namespace FlightDeck.Tracker.Models
{
    using System;
    using FlightDeck.DomainShared;
    
    public class TrackerJourneyPlanOptions
    {
        public Guid Id { get; set; }
        public string FundingBasisName { get; set; }
        public string SelfSufficiencyBasisName { get; set; }
        public string BuyoutBasisName { get; set; }
        public bool IncludeBuyIns { get; set; }
        public SalaryType SalaryInflationType { get; set; }
        public double? CurrentReturn { get; set; }
        public double? SelfSufficiencyDiscountRate { get; set; }
        public int? TriggerSteps { get; set; }
        public bool FundingLevelTrigger { get; set; }
        public double? FundingLevel { get; set; }
        public double? InitialReturn { get; set; }
        public double? FinalReturn { get; set; }
        public int? TriggerTimeStartYear { get; set; }
        public int? TriggerTransitionPeriod { get; set; }
        public NewJPExtraContributionOptions RecoveryPlan { get; set; }
        public bool? UseCurves { get; set; }

        public TrackerJourneyPlanOptions()
        {
            Id = Guid.NewGuid();
        }
    }
}
