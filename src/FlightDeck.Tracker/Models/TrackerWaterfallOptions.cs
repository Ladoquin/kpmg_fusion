﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    
    public class TrackerWaterfallOptions : WaterfallOptions
    {
        public TrackerWaterfallOptions()
            : this(0, 0)
        {
        }
        public TrackerWaterfallOptions(double ci, int time)
            : base(ci, time)
        {
        }
    }
}
