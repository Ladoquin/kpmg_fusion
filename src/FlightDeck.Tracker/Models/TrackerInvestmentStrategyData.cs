﻿namespace FlightDeck.Tracker.Models
{
    using FlightDeck.DomainShared;
    using System;

    public class TrackerInvestmentStrategyData : InvestmentStrategyData
    {
        public Guid Id { get; set; }

        public TrackerInvestmentStrategyData()
            : this(0, 0, 0, 0)
        {
        }

        public TrackerInvestmentStrategyData(double totalCoreAssets, double buyIn, double abf, double abfAdjustment)
            : base(totalCoreAssets, buyIn, abf, abfAdjustment)
        {
            Id = Guid.NewGuid();
        }
    }
}
