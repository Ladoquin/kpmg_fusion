﻿namespace FlightDeck.Tracker.Writer
{
    enum TrackerOperationLevel : byte
    {
        H1 = 0,
        H2,
        H3,
        H4,
        H5
    }
}
