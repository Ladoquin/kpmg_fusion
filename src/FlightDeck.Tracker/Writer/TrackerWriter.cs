﻿namespace FlightDeck.Tracker.Writer
{
    using FlightDeck.Tracker.Parameters;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using Excel = Microsoft.Office.Interop.Excel;
    using System.Linq;
    using System.Configuration;
    using FlightDeck.Domain;
    using FlightDeck.Tracker.Models;

    public class TrackerWriter
    {
        private readonly string originalTrackerPath;

        private Excel.Application xlApp;
        private Excel.Workbook xlWorkBook;
        private TrackerOperationHandler ExcelFn;
        private ITrackerOperationLogger logger;
        private IDictionary<int, string> xlSheetNames;
        private bool schemeFunded = true;

        public TrackerWriter(string originalTrackerPath)
        {
            this.logger = new TrackerOperationLogger();
            this.originalTrackerPath = originalTrackerPath;
        }

        /// <summary>
        /// Open a Tracker instance and load in the required scheme
        /// </summary>
        public void InitialiseScheme(string schemeXmlPath)
        {
            logger.Log("Initialise excel");

            xlApp = new Excel.Application { Visible = true };
            xlWorkBook = xlApp.Workbooks.Open(originalTrackerPath, false, false);            

            this.ExcelFn = new TrackerOperationHandler(xlApp, this.logger);

            ImportSchemeXml(schemeXmlPath);
            SetAdminOverrides();
            SetSheetNames();
            ClearCachedValues();
            GetSchemeProperties();
        }

        public void ImportSchemeXml(string schemeXmlPath)
        {
            //If your code is crashing on the next line make sure you've followed the 'clear cache' instructions in the new Tracker setup up doc
            ExcelFn.Call("ImportSchemeXml", schemeXmlPath, comment: "Import scheme xml from " + schemeXmlPath, level: TrackerOperationLevel.H1);
        }

        /// <summary>
        /// Initialise the Tracker for the selected basis.
        /// </summary>
        public void InitialiseBasis(string basisName)
        {
            ExcelFn.OperationLevel = TrackerOperationLevel.H3;

            logger.Log("Set basis '" + basisName + "'", TrackerOperationLevel.H2);
            TrackerWriterUtilities.SetBasis(basisName, xlWorkBook); //TODO this should be brought in to TrackerOperationHandler

            ExcelFn.Call("CalcIntroSheet.ClearAssumptionsBtn_Click");
            ExcelFn.Call("CalcIntroSheet.ClearAssumptionsBtn_Click");
            SetAttributionPeriod(new DateTime(2015, 7, 1), new DateTime(2015, 7, 31)); // dates which shouldn't cause the Update Tracker calc any problems
            ExcelFn.Call("TrackerSheet.TrackBtn_Click");
            ExcelFn.Call("ExpDeficitSheet.ExpDeficitCalcBtn_Click");
        }

        /// <summary>
        /// Read and saves sheet names fromt he provided XlWorkbook
        /// </summary>
        private void SetSheetNames()
        {
            logger.Log("Get sheet names", TrackerOperationLevel.H2);

            xlSheetNames = new Dictionary<int, string>();
            if (xlWorkBook != null && xlWorkBook.Sheets.Count > 0)
            {
                int i = 1;
                foreach(Excel.Worksheet worksheet in xlWorkBook.Worksheets)
                    xlSheetNames.Add(i++, worksheet.Name);
            }
        }

        /// <summary>
        /// Tracker does not delete or overwrite all values from previous schemes and calculations, so do that here.
        /// </summary>
        private void ClearCachedValues()
        {
            logger.Log("Clear cached values", TrackerOperationLevel.H2);

            var sheet = getWorksheet("JourneyPlan");
            sheet.Range["JPInvGrowth"].Value = "";
            sheet.Range["JP2InvGrowth"].Value = "";

            sheet = getWorksheet("VolatilityAssumptions");
            sheet.Range["GiltLiabDuration"].Value = "";
            sheet.Range["GiltLiabPV01"].Value = "";
            sheet.Range["GiltLiabIE01"].Value = "";
            sheet.Range["GiltBuyInPV01"].Value = "";
            sheet.Range["GiltBuyInIE01"].Value = "";

            sheet = getWorksheet("NewJP");
            for (int r = 9; r < 11; r++)
                for (int c = 3; c <= 4; c++)
                    ((Excel.Range)sheet.Cells[r, c]).Value = "";
        }

        /// <summary>
        /// Get scheme properties that determine how to run the set up
        /// </summary>
        private void GetSchemeProperties()
        {
            logger.Log("Get scheme properties", TrackerOperationLevel.H2);

            var sheet = getWorksheet("Main");
            var FundedYN = sheet.Range["FundedYN"].Value;
            if (FundedYN != null && string.Equals(FundedYN.ToString(), "no", StringComparison.CurrentCultureIgnoreCase))
                schemeFunded = false;
            else
                schemeFunded = true;
        }

        private void SetAdminOverrides()
        {
            logger.Log("Set admin overrides", TrackerOperationLevel.H2);

            var sheet = getWorksheet("Misc");
            ((Excel.Range)sheet.Cells[9, 7]).Value = "Y";            
            ((Excel.Range)sheet.Cells[12, 7]).Value = "Y";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TrackerTempFolder"]))
                ((Excel.Range)sheet.Cells[11, 7]).Value = ConfigurationManager.AppSettings["TrackerTempFolder"].Trim();
        }

        /// <summary>
        /// Write the parameters to the Tracker and run the necessary calculations. 
        /// </summary>
        public void LoadParameters(Parameter parameters)
        {
            SetAttributionPeriod(parameters.Start, parameters.End);

            // check if we need to use default asset assumptions
            // tracker is broken. it doesn't set asset default assumptions values on the Panels sheet correctly
            // we need to update the asset assumptions values on the Panels sheet in order to get our tests results pass 
            if (parameters.BasisAssumptions.AssetAssumptions == null)
                SetDefaultAssetAssumptions();

            ExcelFn.OperationLevel = TrackerOperationLevel.H3;

            ExcelFn.Call("PanelSheet.ResetAllPanels");

            if (parameters.SchemeAssumptions.RecoveryPlanOptions == RecoveryPlanType.NewFixed)
            {
                // tracker will not run a recovery plan calc when New plan (fixed) is selected
                // this is based on the assumption that the user will have already run at least one recovery plan calc (and therefore populated the recovery plan chart)
                // so run a default recovery plan calc now (before basis changes etc have taken place)...
                var sheet = getWorksheet("Panels");
                ExcelFn.Call("RecoveryPlanSheet.RecoveryPlanBtn_Click");
            }

            if (SetLiabilityAssumptions(parameters.BasisAssumptions.LiabilityAssumptions, parameters.BasisAssumptions.LifeExpectancy))
            {
                ExcelFn.Call("PanelSheet.ChangeBasisBtn_Click");
                ExcelFn.Call("PanelSheet.PreFRODefLiabBtn_Click");
                ExcelFn.Call("PanelSheet.PreETVDefLiabBtn_Click");
                ExcelFn.Call("PanelSheet.InitPIEBtn_Click");
            }

            if (schemeFunded)
            {
                SetAssetAssumptions(parameters.BasisAssumptions.AssetAssumptions, "L");

                SetRecoveryPlan(parameters.SchemeAssumptions.RecoveryPlanOptions, parameters.BasisAssumptions.RecoveryPlanOptions);
                SetInvestmentStrategy(parameters.SchemeAssumptions.InvestmentStrategyAssetAllocation, parameters.SchemeAssumptions.InvestmentStrategyOptions);
                SetFRO(parameters.SchemeAssumptions.FROType, parameters.SchemeAssumptions.FROOptions, parameters.SchemeAssumptions.EFROOptions);
                SetETV(parameters.SchemeAssumptions.ETVOptions);
                SetPIE(parameters.SchemeAssumptions.PIEOptions);
                SetFutureBenefits(parameters.SchemeAssumptions.FutureBenefitOptions);
                SetABF(parameters.SchemeAssumptions.ABFOptions);

                if (parameters.SchemeAssumptions.InsuranceOptions != null)
                {
                    SetInsurance(parameters.SchemeAssumptions.InsuranceOptions);
                    ExcelFn.Call("JourneyPlanSheet.JourneyPlan2Btn_Click");
                    ExcelFn.Call("PanelSheet.CalcInsuranceBtn_Click");
                }

                ExcelFn.Call("PanelSheet.CalcABFBtn_Click"); //not sure how tracker is meant to behave, but calculating abf before and after CalculateEverything seems to yield correct results
                ExcelFn.Call("JourneyPlanSheet.JourneyPlanBtn_Click");
                ExcelFn.Call("RecoveryPlanSheet.RecoveryPlanBtn_Click");
                ExcelFn.Call("JourneyPlanSheet.JourneyPlan2Btn_Click");
                ExcelFn.Call("PanelSheet.CalcABFBtn_Click");
                ExcelFn.Call("VolatilitySheet.UpdateGiltsBtn_Click");
                ExcelFn.Call("VaRSheet.VaRBtn_Click");

                ExcelFn.Call("MiscSheet.GrowthOverPeriodBtn_Click");

                SetJourneyPlan(parameters.NewJPAssumptions);
                ExcelFn.Call("NewJP");

                var assetSheetCount = xlSheetNames.Count(x => x.Value.ToLower().StartsWith("assets_"));
                for (var i = 1; i <= assetSheetCount; i++)
                {
                    var sheetSuffix = i > 1 ? (i - 1).ToString() : "";
                    if (xlSheetNames.Any(x => x.Value.ToLower() == "assets_" + i))
                    {
                        ExcelFn.Call(string.Format("AssetSheet{0}.HedgeBtn_Click", sheetSuffix));
                    }
                }
            }
            else
            {
                ExcelFn.Call("PanelSheet.CalcABFBtn_Click");
                ExcelFn.Call("JourneyPlanSheet.JourneyPlanBtn_Click");
                ExcelFn.Call("JourneyPlanSheet.JourneyPlan2Btn_Click");
                ExcelFn.Call("PanelSheet.CalcABFBtn_Click");
                ExcelFn.Call("VolatilitySheet.UpdateGiltsBtn_Click");
                ExcelFn.Call("VaRSheet.VaRBtn_Click");
            }
        }

        void SetAttributionPeriod(DateTime start, DateTime end)
        {
            logger.Log(string.Format("Set attribution period to {0} - {1}", start.ToString("dd/MM/yyyy"), end.ToString("dd/MM/yyyy")), TrackerOperationLevel.H3);

            var sheet = getWorksheet("Tracker");
            sheet.Range["AttrStartDate"].Value = start.ToOADate();
            sheet.Range["AnalysisDate"].Value = end.ToOADate();
        }

        bool SetLiabilityAssumptions(IDictionary<AssumptionType, double> liabilities, LifeExpectancyAssumptions mortality)
        {
            if (liabilities.IsNullOrEmpty() && mortality == null)
                return false;

            logger.Log("Set liability assumptions", TrackerOperationLevel.H3);

            var sheet = getWorksheet("Panels");
            if (!liabilities.IsNullOrEmpty())
            {
                if (liabilities.ContainsKey(AssumptionType.PreRetirementDiscountRate))
                    sheet.Range["UserDiscPre"].Value = liabilities[AssumptionType.PreRetirementDiscountRate];

                if (liabilities.ContainsKey(AssumptionType.PostRetirementDiscountRate))
                    sheet.Range["UserDiscPost"].Value = liabilities[AssumptionType.PostRetirementDiscountRate];

                if (liabilities.ContainsKey(AssumptionType.PensionDiscountRate))
                    sheet.Range["UserDiscPen"].Value = liabilities[AssumptionType.PensionDiscountRate];

                if (liabilities.ContainsKey(AssumptionType.SalaryIncrease))
                    sheet.Range["UserSalInc"].Value = liabilities[AssumptionType.SalaryIncrease];

                if (liabilities.ContainsKey(AssumptionType.InflationRetailPriceIndex))
                    sheet.Range["UserRPI"].Value = liabilities[AssumptionType.InflationRetailPriceIndex];

                if (liabilities.ContainsKey(AssumptionType.InflationConsumerPriceIndex))
                    sheet.Range["UserCPI"].Value = liabilities[AssumptionType.InflationConsumerPriceIndex];
            }
            if (mortality != null)
            {
                sheet.Range["UserLifeExpPen"].Value = mortality.PensionerLifeExpectancy;
                sheet.Range["UserLifeExpActDef"].Value = mortality.DeferredLifeExpectancy;
            }

            return true;
        }

        void SetDefaultAssetAssumptions()
        {
            if (xlSheetNames.Count == 0)
                return;

            // check if we have asset sheets, take the latest one
            var assetSheet = xlSheetNames.Where(x => x.Value.ToLower().StartsWith("assets")).OrderByDescending(x => x.Key).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(assetSheet.Value))
                return;
            else
            {
                // we found assets sheet, now set the value
                // as we have passed assets object as null,
                // we will create new object and set it's values from latest asset sheet (e.g. Assets_1)
                var sheet = getWorksheet(assetSheet.Value);
                var assets = new Dictionary<AssetClassType, double>();
                var row = 42;
                var col = XlHelper.GetCol("D");
                assets.Add(AssetClassType.Equity, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));
                assets.Add(AssetClassType.Property, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));
                assets.Add(AssetClassType.DiversifiedGrowth, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));
                assets.Add(AssetClassType.DiversifiedCredit, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));
                assets.Add(AssetClassType.PrivateMarkets, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));
                assets.Add(AssetClassType.CorporateBonds, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));
                assets.Add(AssetClassType.FixedInterestGilts, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));
                assets.Add(AssetClassType.IndexLinkedGilts, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));
                assets.Add(AssetClassType.Cash, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));
                assets.Add(AssetClassType.Abf, double.Parse(((Excel.Range)sheet.Cells[row++, col]).Value.ToString()));

                SetAssetAssumptions(assets, "M");
                ExcelFn.Call("ExpDeficitSheet.ExpDeficitCalcBtn_Click", level: TrackerOperationLevel.H3);
            }
        }

        void SetAssetAssumptions(IDictionary<AssetClassType, double> assets, string col)
        {
            if (assets.IsNullOrEmpty())
                return;

            var sheet = getWorksheet("Panels");

            logger.Log("Set asset assumptions", TrackerOperationLevel.H3);

            Action<int, int> setAssetData = (rowId, colId) =>
            {
                if (assets.ContainsKey(AssetClassType.Equity))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.Equity];

                if (assets.ContainsKey(AssetClassType.Property))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.Property];

                if (assets.ContainsKey(AssetClassType.DiversifiedGrowth))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.DiversifiedGrowth];

                if (assets.ContainsKey(AssetClassType.DiversifiedCredit))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.DiversifiedCredit];

                if (assets.ContainsKey(AssetClassType.PrivateMarkets))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.PrivateMarkets];

                if (assets.ContainsKey(AssetClassType.CorporateBonds))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.CorporateBonds];

                if (assets.ContainsKey(AssetClassType.FixedInterestGilts))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.FixedInterestGilts];

                if (assets.ContainsKey(AssetClassType.IndexLinkedGilts))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.IndexLinkedGilts];

                if (assets.ContainsKey(AssetClassType.Cash))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.Cash];

                if (assets.ContainsKey(AssetClassType.Abf))
                    ((Excel.Range)sheet.Cells[rowId++, col]).Value = assets[AssetClassType.Abf];
            };

            // set the values for assets panel 
            setAssetData(15, XlHelper.GetCol(col));
        }

        void SetRecoveryPlan(RecoveryPlanType type, RecoveryPlanAssumptions options)
        {
            if (options == null)
                return;
            
            logger.Log("Set recovery plan", TrackerOperationLevel.H3);
            
            if (type != RecoveryPlanType.NewFixed)
            {
                var sheet = getWorksheet("Panels");
                if (options.IsGrowthFixed)
                {
                    sheet.Range["RPInvGrowth"].Value = options.InvestmentGrowth;
                    sheet.Range["RPDiscountPlus"].Clear();
                }
                else
                {
                    sheet.Range["RPInvGrowth"].Clear();
                    sheet.Range["RPDiscountPlus"].Value = options.DiscountIncrement;
                }
                sheet.Range["RPStartMonth"].Value = options.StartMonth;
                sheet.Range["RPTerm"].Value = options.Term;
                sheet.Range["RPIncs"].Value = options.Increases;
                sheet.Range["RPLumpSum"].Value = options.LumpSumPayment;
                sheet.Range["RPOption"].Value = type == RecoveryPlanType.Current ? "Current plan" : "New plan";
            }
        }

        void SetInvestmentStrategy(IDictionary<AssetClassType, double> allocations, InvestmentStrategyOptions options)
        {
            var sheet = getWorksheet("Panels");
            if (options != null)
            {
                logger.Log("Set investment strategy advanced options", TrackerOperationLevel.H3);

                if (options.CashInjection > 0)
                {
                    ExcelFn.Call("ChangeCashInjectionSet", options.CashInjection, level: TrackerOperationLevel.H3);
                }
                sheet.Range["UserPropSyntheticCredit"].Value = options.SyntheticCredit;
                sheet.Range["UserPropSyntheticEquity"].Value = options.SyntheticEquity;
                sheet.Range["LDIInForceAtAnalysisDate"].Value = options.LDIInForce;
                sheet.Range["UserInterestHedge"].Value = options.InterestHedge;
                sheet.Range["UserInflationHedge"].Value = options.InflationHedge;
            }
            if (!allocations.IsNullOrEmpty())
            {
                //if there's existing abf then we need to scale down the allocations to prevent tracker from going over 100%
                var existingABFProportion = (double)sheet.Cells[23, XlHelper.GetCol("AD")].Value;
                var multiplier = 1 - existingABFProportion;
                logger.Log("Set investment strategy mix", TrackerOperationLevel.H3);

                var row = sheet.Range["UserAssetAllocation"].Row;
                var col = sheet.Range["UserAssetAllocation"].Column + 1;
                ((Excel.Range)sheet.Cells[row++, col]).Value = multiplier * allocations[AssetClassType.Equity];
                ((Excel.Range)sheet.Cells[row++, col]).Value = multiplier * allocations[AssetClassType.Property];
                ((Excel.Range)sheet.Cells[row++, col]).Value = multiplier * allocations[AssetClassType.DiversifiedGrowth];
                ((Excel.Range)sheet.Cells[row++, col]).Value = multiplier * allocations[AssetClassType.DiversifiedCredit];
                ((Excel.Range)sheet.Cells[row++, col]).Value = multiplier * allocations[AssetClassType.PrivateMarkets];
                ((Excel.Range)sheet.Cells[row++, col]).Value = multiplier * allocations[AssetClassType.CorporateBonds];
                ((Excel.Range)sheet.Cells[row++, col]).Value = multiplier * allocations[AssetClassType.FixedInterestGilts];
                ((Excel.Range)sheet.Cells[row++, col]).Value = multiplier * allocations[AssetClassType.IndexLinkedGilts];
                ((Excel.Range)sheet.Cells[row++, col]).Value = multiplier * allocations[AssetClassType.Cash];
            }
        }

        void SetFRO(FROType type, FlexibleReturnOptions fro, EmbeddedFlexibleReturnOptions efro)
        {
            var sheet = getWorksheet("Panels");
            sheet.Range["FROType"].Value = type == FROType.Bulk ? "Bulk" : "Embedded";
            if (type == FROType.Bulk && fro != null)
            {
                logger.Log("Set Bulk FRO", TrackerOperationLevel.H3);

                sheet.Range["PreFRODefTV"].Value = fro.EquivalentTansferValue;
                sheet.Range["FROTakeUpRate"].Value = fro.TakeUpRate;
            }
            else if (efro != null)
            {
                logger.Log("Set Embedded FRO", TrackerOperationLevel.H3);

                sheet.Range["EFROTakeUpRate"].Value = efro.AnnualTakeUpRate;
                sheet.Range["EFROBasisChange"].Value = efro.EmbeddedFroBasisChange;
            }
        }

        void SetETV(EnhancedTransferValueOptions etv)
        {
            if (etv == null)
                return;

            logger.Log("Set ETV", TrackerOperationLevel.H3);

            var sheet = getWorksheet("Panels");
            sheet.Range["ETVUnenhancedValue"].Value = etv.EquivalentTransferValue;
            sheet.Range["ETVUplift"].Value = etv.EnhancementToCetv;
            sheet.Range["ETVTakeUpRate"].Value = etv.TakeUpRate;
        }

        void SetPIE(PieOptions pie)
        {
            if (pie == null)
                return;

            logger.Log("Set PIE", TrackerOperationLevel.H3);

            var sheet = getWorksheet("Panels");
            sheet.Range["PIEPropEligible"].Value = pie.PortionEligible;
            sheet.Range["PIEActualUplift"].Value = pie.ActualUplift;
            sheet.Range["PIETakeUpRate"].Value = pie.TakeUpRate;
        }

        void SetFutureBenefits(FutureBenefitsParameters fb)
        {
            if (fb == null)
                return;

            logger.Log("Set future benefits", TrackerOperationLevel.H3);
            var sheet = getWorksheet("Panels");
            sheet.Range["FBExtraEeConts"].Value = fb.MemberContributionsIncrease;
            var row = sheet.Range["FBOptions"].Row;
            var col = sheet.Range["FBOptions"].Column;
            for (int r = 0; r <= 4; r++)
                ((Excel.Range)sheet.Cells[row + r, col]).Value = string.Empty;
            var x = 0;
            switch (fb.AdjustmentType)
            {
                case BenefitAdjustmentType.BenefitReduction:
                    x = 1;
                    break;
                case BenefitAdjustmentType.SalaryLimit:
                    x = 2;
                    break;
                case BenefitAdjustmentType.CareRevaluation:
                    x = 3;
                    break;
                case BenefitAdjustmentType.DcImplementation:
                    x = 4;
                    break;
                case BenefitAdjustmentType.None:
                default:
                    break;
            }
            ((Excel.Range)sheet.Cells[row + x, col]).Value = "x";
            if (fb.AdjustmentType != BenefitAdjustmentType.None)
            {
                ((Excel.Range)sheet.Cells[row + x, col + 1]).Value = fb.AdjustmentRate;
            }
        }

        void SetABF(AbfOptions abf)
        {
            if (abf == null)
                return;

            logger.Log("Set ABF", TrackerOperationLevel.H3);

            var sheet = getWorksheet("Panels");
            sheet.Range["ABFValue"].Value = abf.Value;
        }

        void SetInsurance(InsuranceParameters insurance)
        {
            if (insurance == null)
                return;

            logger.Log("Set insurance", TrackerOperationLevel.H3);

            var sheet = getWorksheet("Panels");
            sheet.Range["InsPenProp"].Value = insurance.PensionerLiabilityPercInsured;
            sheet.Range["InsNonPenProp"].Value = insurance.NonpensionerLiabilityPercInsured;            
        }

        void SetJourneyPlan(TrackerJourneyPlanOptions parameters)
        {
            logger.Log("Set Journey Plan", TrackerOperationLevel.H3);

            var sheet = getWorksheet("NewJP");
            var range = sheet.get_Range("NewJPBases");
            ((Excel.Range)sheet.Cells[range.Row, range.Column]).Value = parameters.FundingBasisName;
            ((Excel.Range)sheet.Cells[range.Row + 1, range.Column]).Value = parameters.SelfSufficiencyBasisName;
            if (!string.IsNullOrEmpty(parameters.BuyoutBasisName))
                ((Excel.Range)sheet.Cells[range.Row + 2, range.Column]).Value = parameters.BuyoutBasisName;

            var NewJPIncludeBuyIn = sheet.get_Range("NewJPIncludeBuyIn");
            ((Excel.Range)sheet.Cells[NewJPIncludeBuyIn.Row, NewJPIncludeBuyIn.Column]).Value = parameters.IncludeBuyIns ? "Yes" : "No";

            if (parameters.CurrentReturn.HasValue)
            {
                var InvGrowthBefore = sheet.get_Range("InvGrowthBefore");
                ((Excel.Range)sheet.Cells[InvGrowthBefore.Row, InvGrowthBefore.Column]).Value = parameters.CurrentReturn.Value / 100;
            }

            if (parameters.SelfSufficiencyDiscountRate.HasValue)
            {
                var SelfSuffDiscountRate = sheet.get_Range("SelfSuffDiscountRate");
                ((Excel.Range)sheet.Cells[SelfSuffDiscountRate.Row, SelfSuffDiscountRate.Column]).Value = parameters.SelfSufficiencyDiscountRate.Value / 100;
            }

            if (parameters.TriggerSteps.HasValue)
            {
                var TriggerSteps = sheet.get_Range("TriggerSteps");
                ((Excel.Range)sheet.Cells[TriggerSteps.Row, TriggerSteps.Column]).Value = parameters.TriggerSteps.Value;
            }

            if (parameters.TriggerTimeStartYear.HasValue)
            {
                var Trigger1Time = sheet.get_Range("Trigger1Time");
                ((Excel.Range)sheet.Cells[Trigger1Time.Row, Trigger1Time.Column]).Value = parameters.TriggerTimeStartYear.Value;
            }

            if (parameters.TriggerTransitionPeriod.HasValue)
            {
                var TriggerTransitionPeriod = sheet.get_Range("TriggerTransitionPeriod");
                ((Excel.Range)sheet.Cells[TriggerTransitionPeriod.Row, TriggerTransitionPeriod.Column]).Value = parameters.TriggerTransitionPeriod.Value;
            }

            var InvGrowthAfter = sheet.get_Range("InvGrowthAfter");
            if (parameters.InitialReturn.HasValue)
                ((Excel.Range)sheet.Cells[InvGrowthAfter.Row, InvGrowthAfter.Column]).Value = parameters.InitialReturn.Value / 100;
            if (parameters.FinalReturn.HasValue)
                ((Excel.Range)sheet.Cells[InvGrowthAfter.Row + 1, InvGrowthAfter.Column]).Value = parameters.FinalReturn.Value / 100;

            var extraContOptions = parameters.RecoveryPlan;
            if (extraContOptions != null)
            {
                var NewJPAddRP = sheet.get_Range("NewJPAddRP");
                ((Excel.Range)sheet.Cells[NewJPAddRP.Row, NewJPAddRP.Column]).Value = extraContOptions.AddCurrentRecoveryPlan ? "Yes" : "No";
                var NewJPAddXC = sheet.get_Range("NewJPAddXC");
                ((Excel.Range)sheet.Cells[NewJPAddXC.Row, NewJPAddXC.Column]).Value = extraContOptions.AddExtraContributions ? "Yes" : "No";

                if (extraContOptions.AddExtraContributions)
                {
                    var XCStartYear = sheet.get_Range("XCStartYear");
                    ((Excel.Range)sheet.Cells[XCStartYear.Row, XCStartYear.Column]).Value = extraContOptions.StartYear;

                    var XCInitialConts = sheet.get_Range("XCInitialConts");
                    ((Excel.Range)sheet.Cells[XCInitialConts.Row, XCInitialConts.Column]).Value = extraContOptions.AnnualContributions;

                    var XCTerm = sheet.get_Range("XCTerm");
                    ((Excel.Range)sheet.Cells[XCTerm.Row, XCTerm.Column]).Value = extraContOptions.Term;
                }
            }

        }

        /// <summary>
        /// Save the current Tracker instance.
        /// </summary>
        public void Save(string filepath)
        {
            logger.Log("Saving " + filepath, TrackerOperationLevel.H2);
            
            xlWorkBook.SaveAs(filepath);

            logger.Log("Saved", TrackerOperationLevel.H2);
        }

        public void Quit()
        {
            logger.Log("Kill excel", TrackerOperationLevel.H2);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            xlWorkBook.Close(false, Missing.Value, Missing.Value);
            xlApp.Quit();
            Marshal.FinalReleaseComObject(xlWorkBook);
            Marshal.FinalReleaseComObject(xlApp);

            try
            {
                while (Process.GetProcessesByName("excel").Length > 0)
                    Process.GetProcessesByName("excel")[0].Kill();
            }
            catch
            {
                //whatever
            }
        }

        private static void ReleaseXlObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            }
            finally
            {
                GC.Collect();
            }
        }

        private Excel.Worksheet getWorksheet(string name)
        {
            return (Excel.Worksheet)xlWorkBook.Worksheets.Item[name] as Excel.Worksheet;
        }
    }
}
