﻿namespace FlightDeck.Tracker.Writer
{
    using System;
    using System.Configuration;
    
    class TrackerOperationLogger : ITrackerOperationLogger
    {
        private const byte indentationSize = 3;
        private bool traceOn = true;

        public TrackerOperationLogger()
        {
            setState();
        }

        private void setState()
        {
            bool b;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TraceTrackerOperations"]) && bool.TryParse(ConfigurationManager.AppSettings["TraceTrackerOperations"], out b))
                traceOn = b;
        }

        public void Log(string comment)
        {
            Log(comment, (byte)0);
        }

        public void Log(string comment, TrackerOperationLevel level)
        {
            Log(comment, (byte)((byte)level * indentationSize));
        }

        private void Log(string comment, byte indentation = 0)
        {
            if (!traceOn)
                return; 

            Console.WriteLine(new string(' ', indentation) + comment);
        }
    }
}
