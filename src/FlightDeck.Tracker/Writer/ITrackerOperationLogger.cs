﻿namespace FlightDeck.Tracker.Writer
{
    interface ITrackerOperationLogger
    {
        void Log(string comment);
        void Log(string comment, TrackerOperationLevel level);
    }
}
