﻿namespace FlightDeck.Tracker.Writer
{
    using Excel = Microsoft.Office.Interop.Excel;

    class TrackerOperationHandler
    {
        readonly Excel.Application xlApp;
        readonly ITrackerOperationLogger logger;

        public TrackerOperationLevel OperationLevel { set; private get; }

        public TrackerOperationHandler(Excel.Application xlApp, ITrackerOperationLogger logger)
        {
            this.xlApp = xlApp;
            this.logger = logger;
        }

        public void Call(string functionName)
        {
            Call(functionName, args: null, comment: null, level: OperationLevel);
        }

        public void Call(string functionName, dynamic args = null, string comment = null, TrackerOperationLevel level = TrackerOperationLevel.H1)
        {
            logger.Log(comment ?? ("Call " + functionName), level);

            if (args == null)
                xlApp.Run(functionName);
            else
                xlApp.Run(functionName, args);
        }
    }
}
