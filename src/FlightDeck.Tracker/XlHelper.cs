﻿namespace FlightDeck.Tracker
{
    using FlexCel.Core;
    using FlexCel.XlsAdapter;
    using System;
    
    class XlHelper
    {
        XlsFile xls;

        public XlHelper(XlsFile xls)
        {
            this.xls = xls;
        }

        public static int GetCol(string colName)
        {
            var alphabet = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z".Split(',');

            if (colName.Length == 0) return -1;

            if (colName.Length == 1)
            {
                for (var i = 0; i < 26; i++)
                {
                    if (string.Equals(colName, alphabet[i], StringComparison.CurrentCultureIgnoreCase))
                        return i + 1;
                }
                return -1;
            }
            for (var i = 0; i < 26; i++)
            {
                for (var j = 0; j < 26; j++)
                {
                    if (string.Equals(colName, alphabet[i] + alphabet[j], StringComparison.CurrentCultureIgnoreCase))
                        return ((i + 1) * 26) + (j + 1);
                }
            }
            return -1;
        }

        public static string IncrementCol(string col, int inc = 1)
        {
            var alphabet = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z".Split(',');

            var candidate = col.Substring(col.Length - 1, 1);
            var first = col.Length == 1 ? string.Empty : col.Substring(0, 1);

            var idx = Array.IndexOf(alphabet, candidate);

            if (idx + inc >= alphabet.Length)
            {
                candidate = alphabet[alphabet.Length - idx - 1];
                first = alphabet[Array.IndexOf(alphabet, first) + 1];
            }
            else
            {
                candidate = alphabet[idx + inc];
            }
            return string.Format("{0}{1}", first, candidate);
        }

        public DateTime GetDate(string namedRange, string sheetName = null)
        {
            double d;
            if (TryGetDouble(namedRange, out d, sheetName))
            {
                return DateTime.FromOADate(d);
            }
            return DateTime.MinValue;
        }

        public DateTime GetDateSlow(int row, string column)
        {
            return DateTime.FromOADate(GetDouble(row, column));
        }

        public DateTime GetDate(int row, string column)
        {
            return DateTime.FromOADate(GetDouble(row, column));
        }

        public DateTime GetDate(int row, int column)
        {
            return DateTime.FromOADate(GetDouble(row, column));
        }

        public DateTime GetDateFormula(int row, string column)
        {
            return DateTime.FromOADate(GetDouble(row, column));
        }

        public double GetDouble(int row, string column, double dflt)
        {
            double? d = GetDouble(row, column, (double?)null);
            return d.GetValueOrDefault(dflt);
        }

        public double GetDouble(string rangeName, double dflt, string sheetName = null)
        {
            double? d = GetDouble(rangeName, (double?)null, sheetName);
            return d.GetValueOrDefault(dflt);
        }

        public double? GetDouble(int row, string column, double? dflt)
        {
            var d = .0;
            if (TryGetDouble(row, column, out d))
            {
                return d;
            }
            return dflt;
        }

        public double GetDouble(int row, int column, double dflt)
        {
            var d = .0;
            if (TryGetDouble(row, column, out d))
            {
                return d;
            }
            return dflt;
        }

        public double? GetDouble(string rangeName, double? dflt, string sheetName = null)
        {
            var d = .0;
            if (TryGetDouble(rangeName, out d, sheetName))
            {
                return d;
            }
            return dflt;
        }

        public double GetDouble(int row, int column)
        {
            var d = .0;
            if (TryGetDouble(row, column, out d))
            {
                return d;
            }
            throw new Exception(string.Format("Could not get double at {0} col {1}, row {2}, file {3}", xls.ActiveSheetByName, column.ToString(), row.ToString(), xls.ActiveFileName));
        }

        public double GetDouble(int row, string column)
        {
            var d = .0;
            if (TryGetDouble(row, column, out d))
            {
                return d;
            }
            throw new Exception(string.Format("Could not get double at {0}!{1}{2}, file {3}", xls.ActiveSheetByName, column, row.ToString(), xls.ActiveFileName));
        }

        public double GetDouble(string rangeName, string sheetName = null)
        {
            var d = .0;
            if (TryGetDouble(rangeName, out d, sheetName))
            {
                return d;
            }
            throw new Exception(string.Format("Could not get double at {0}!{1}, file {2}", xls.ActiveSheetByName, rangeName, xls.ActiveFileName));
        }

        public bool TryGetDouble(string rangeName, out double result, string sheetName = null)
        {
            return TryGetDouble(GetCellValue(rangeName, sheetName ?? xls.ActiveSheetByName), out result);
        }

        public bool TryGetDouble(int row, string column, out double result)
        {
            return TryGetDouble(xls.GetCellValue(row, XlHelper.GetCol(column)), out result);
        }

        public bool TryGetDouble(int row, int column, out double result)
        {
            return TryGetDouble(xls.GetCellValue(row, column), out result);
        }

        public bool TryGetDouble(object val, out double result)
        {
            var ok = false;
            result = .0;
            if (val != null)
            {
                if (val.GetType() == typeof(TFormula))
                {
                    ok = double.TryParse((val as TFormula).Result.ToString(), out result);
                }
                else
                {
                    ok = double.TryParse(val.ToString(), out result);
                }
            }
            return ok;
        }

        public bool TryGetDate(int row, int col, out DateTime d)
        {
            d = DateTime.MinValue;
            double dbl;
            if (xls.GetCellValue(row, col) != null && TryGetDouble(row, col, out dbl))
            {
                try
                {
                    d = DateTime.FromOADate(dbl);
                    return true;
                }
                catch
                {
                }
            }
            return false;
        }

        public bool GetBool(int row, string col)
        {
            return getBoolFromVal(xls.GetCellValue(row, XlHelper.GetCol(col)) as string);
        }

        public bool GetBool(int row, int col)
        {
            return getBoolFromVal(xls.GetCellValue(row, col) as string);
        }

        public bool GetBool(string namedRange, string sheetName = null)
        {
            var istrue = false;
            var val = GetCellValue(namedRange, sheetName ?? xls.ActiveSheetByName);
            if (val != null)
            {
                istrue = getBoolFromVal(val.ToString());
            }
            return istrue;
        }

        public object GetCellValue(string namedRange, string sheetName)
        {
            var currentlyActiveSheetName = xls.ActiveSheetByName;
            xls.ActiveSheetByName = sheetName;
            var cell = xls.GetNamedRange(namedRange, xls.GetSheetIndex(sheetName));
            var value = xls.GetCellValue(cell.Top, cell.Left);
            xls.ActiveSheetByName = currentlyActiveSheetName;
            return value;
        }

        public RangeCoords GetRangeCoords(string namedRange, string sheetName = null)
        {
            var cell = xls.GetNamedRange(namedRange, xls.GetSheetIndex(sheetName ?? xls.ActiveSheetByName));
            if (cell == null)
                throw new Exception(string.Format("NamedRange not found at: '{0}'" + namedRange));
            var range = new RangeCoords(cell.Left, cell.Top, cell.Right, cell.Bottom);
            return range;
        }

        private bool getBoolFromVal(string val)
        {
            var istrue = false;
            istrue = !string.IsNullOrEmpty(val) && (string.Equals(val, "yes", StringComparison.CurrentCultureIgnoreCase) || string.Equals(val, "true", StringComparison.CurrentCultureIgnoreCase));
            return istrue;
        }

        public string GetString(string namedRange)
        {
            var coords = GetRangeCoords(namedRange);

            return GetStringWithDefault(coords.Row, coords.Col);
        }

        public string GetString(int row, int column)
        {
            return xls.GetCellValue(row, column).ToString();
        }

        public string GetStringWithDefault(int row, int column)
        {
            var v = xls.GetCellValue(row, column);
            return v == null ? string.Empty : v.ToString();
        }
    }
}
