﻿namespace Fusion.WebApp.Specs
{
    using FlightDeck.DomainShared;
    using Fusion.Shared.Data;
    using System.Collections.Generic;
    using System.Web.Security;
    using WebMatrix.WebData;

    class MembershipHelper
    {
        private const string password = "Password1!";
        private readonly SimpleMembershipProvider membershipProvider;

        public MembershipHelper()
        {
            if (!WebSecurity.Initialized)
                WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);

            membershipProvider = (SimpleMembershipProvider)Membership.Provider;
        }

        public int CreateUser(string username)
        {
            membershipProvider.CreateUserAndAccount(username, password,
                new Dictionary<string, object>
                    {
                        { "TermsAccepted", true }
                    });

            var id = membershipProvider.GetUserId(username);

            return id;
        }

        public int GetUserId(string username)
        {
            return membershipProvider.GetUserId(username);
        }

        public void DeleteUser(string username, bool cascade = true)
        {
            if (membershipProvider.GetUserId(username) > -1)
            {
                var db = new DbHelper();
                if (cascade)
                {
                    db.RunQuery(string.Format("delete UserProfileSchemeDetail where UserId = (select UserId from UserProfile where Username = '{0}')", username));
                    Roles.RemoveUserFromRoles(username, Roles.GetRolesForUser(username));
                }
                membershipProvider.DeleteAccount(username);
                db.RunQuery(string.Format("delete UserProfile where Username = '{0}'", username));
            }
        }
    }
}
