﻿namespace Fusion.WebApp.Specs
{
    using Fusion.WebApp.Controllers;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using TechTalk.SpecFlow;

    internal class ControllerFactory
    {
        public static T Get<T>(ScenarioContext scenarioContext) where T : FusionBaseController, new()
        {
            var controller = new T();

            var httpContext = scenarioContext.Get<HttpContextBase>("httpcontext");
            var requestContext = new RequestContext(httpContext, new RouteData());

            controller.ControllerContext = new ControllerContext()
            {
                Controller = controller,
                RequestContext = requestContext
            };

            controller.Url = new UrlHelper(requestContext);

            return controller;
        }
    }
}
