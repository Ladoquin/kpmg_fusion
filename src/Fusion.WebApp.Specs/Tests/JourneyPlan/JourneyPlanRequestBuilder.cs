﻿using Fusion.WebApp.Controllers;
using Fusion.WebApp.RequestObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fusion.WebApp.Specs.Tests.JourneyPlan
{
    public class JourneyPlanRequestBuilder
    {
        public JourneyPlanRequest BuildJourneyPlanRequest(JourneyPlanController controller, dynamic jpData)
        {
            var request = new JourneyPlanRequest
                 {
                     EffectiveDate = jpData.newJourneyPlan.effectiveDate,
                     TechnicalProvisionsBasisId = jpData.newJourneyPlan.technicalProvisionsBasisId,
                     CurrentReturn = jpData.newJourneyPlan.assetGrowthData.currentReturn.val,
                     InitialReturn = jpData.newJourneyPlan.triggerData.initialReturnAssets.val,
                     FinalReturn = jpData.newJourneyPlan.triggerData.finalReturnAssets.val,
                     FundingLevel = jpData.newJourneyPlan.triggerData.fundingLevel.val,
                     NumberOfSteps = jpData.newJourneyPlan.triggerData.numberOfSteps.val,
                     DiscountRate = jpData.newJourneyPlan.selfSufficiencyData.discountRate.val,
                     StartYear = jpData.newJourneyPlan.recoveryPlanData.startYear.val,
                     AnnualContributions = jpData.newJourneyPlan.recoveryPlanData.annualCont.val,
                     Term = jpData.newJourneyPlan.recoveryPlanData.term.val,
                     AnnualIncrements = jpData.newJourneyPlan.recoveryPlanData.annualInc.val,
                     AddCurrentRecoveryPlan = jpData.newJourneyPlan.addCurrentRecoveryPlan ?? false,
                     IncludeBuyIns = jpData.includeBuyins ?? false
                 };

            return request;
        }
    }
}
