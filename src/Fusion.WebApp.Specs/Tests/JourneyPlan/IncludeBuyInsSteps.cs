﻿using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.JourneyPlan
{
    [Binding]
    public class IncludeBuyInsSteps
    {
        [When(@"I change include buy-ins from not checked to checked")]
        public void WhenIChangeIncludeBuy_InsFromNotCheckedToChecked()
        {
            var controller = ControllerFactory.Get<JourneyPlanController>(ScenarioContext.Current);

            var initialJPSettings = controller.NewJourneyPlanData() as JsonResult;

            var jpData = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(Newtonsoft.Json.JsonConvert.SerializeObject(initialJPSettings.Data));

            var jpRequest = new JourneyPlanRequestBuilder().BuildJourneyPlanRequest(controller, jpData);

            jpRequest.IncludeBuyIns = true;

            var updatedJPSettings = controller.SetJourneyPlanData(jpRequest);

            ScenarioContext.Current.Remove("currentJPSettings");
            ScenarioContext.Current.Add("currentJPSettings", updatedJPSettings);
        }

        [Then(@"The include buy-ins control is not checked")]
        public void ThenTheIncludeBuy_InsControlIsNotChecked()
        {
            Assert.IsFalse(getCurrentIncludeBuyInSetting());
        }

        [Then(@"The include buy-ins control is checked")]
        public void ThenTheIncludeBuy_InsControlIsChecked()
        {
            Assert.IsTrue(getCurrentIncludeBuyInSetting());
        }

        private bool getCurrentIncludeBuyInSetting()
        {
            var response = ScenarioContext.Current.Get<JsonResult>("currentJPSettings");

            var data = JsonReader.Get<object>(response, "Data");

            var newJourneyPlan = JsonReader.Get<object>(data, "newJourneyPlan");

            var includeBuyins = JsonReader.Get<bool>(newJourneyPlan, "includeBuyIns");

            return includeBuyins;
        }
    }
}
