﻿using Fusion.WebApp.Controllers;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Web.Mvc;
using TechTalk.SpecFlow;
using Fusion.Shared.Web;
using Fusion.Shared.Data;

namespace Fusion.WebApp.Specs.Tests.JourneyPlan
{
    [Binding]
    public class RecoveryPlanSteps
    {
        [When(@"I increase my annual contributions on the recovery plan")]
        public void WhenIIncreaseMyAnnualContributionsOnTheRecoveryPlan()
        {
            var controller = ControllerFactory.Get<JourneyPlanController>(ScenarioContext.Current);

            var initialJPSettings = controller.NewJourneyPlanData() as JsonResult;

            var jpData = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(initialJPSettings.Data));

            var jpRequest = new JourneyPlanRequestBuilder().BuildJourneyPlanRequest(controller, jpData);

            jpRequest.AddExtraContributions = true;
            jpRequest.AnnualContributions = 10000000; //£10M
            jpRequest.CurrentReturn = 0.05; //the max ...we need to increase the Current asset growth to 5% to get th chart lines to actually cross in the first place!
            jpRequest.Term = 2;//without a term the rp does nothing.

            var firstCrossingResult = controller.SetJourneyPlanData(jpRequest);
            var firstCrossingJpData = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(firstCrossingResult.Data));
            var firstCrossOver = firstCrossingJpData.newJourneyPlan.crossOverPoints.ssna;
            ScenarioContext.Current.Add("FirstCrossOver", firstCrossOver.Value);

            jpRequest.AnnualContributions = 100000000; //£100M
            var secondCrossingResult = controller.SetJourneyPlanData(jpRequest);
            var secondCrossingJpData = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(secondCrossingResult.Data));
            var secondCrossOver = secondCrossingJpData.newJourneyPlan.crossOverPoints.ssna;
            ScenarioContext.Current.Add("SecondCrossOver", secondCrossOver.Value);
        }

        [Then(@"My fund should become self sufficient sooner")]
        public void ThenMyFundShouldBecomeSelfSufficientSooner()
        {
            var firstCrossOver = ScenarioContext.Current.Get<long>("FirstCrossOver");
            var secondCrossOver = ScenarioContext.Current.Get<long>("SecondCrossOver");

            // We're self sufficient sooner if we add £100M per year over 2 years 
            // than if we only add £10M per year over 2 years.
            Assert.IsTrue(secondCrossOver < firstCrossOver);
        }

        [Then(@"The max contribution pa is equivalent to the recovery plan max lump sum for the scheme")]
        public void ThenTheMaxContributionPaIsEquivalentToTheRecoveryPlanMaxLumpSumForTheScheme()
        {
            var response = ScenarioContext.Current.Get<JsonResult>("currentJPSettings");

            var data = Shared.Web.JsonReader.Get<object>(response, "Data");

            var newJourneyPlan = Shared.Web.JsonReader.Get<object>(data, "newJourneyPlan");

            var recoveryPlanData = Shared.Web.JsonReader.Get<object>(newJourneyPlan, "recoveryPlanData");

            var annualCont = Shared.Web.JsonReader.Get<object>(recoveryPlanData, "annualCont");

            var max = Shared.Web.JsonReader.Get<double>(annualCont, "max");

            var maxFromScheme = new DbHelper().RunQuery(@"select np.value
                                                  from NamedProperties np
                                                  inner join NamedPropertyGroups npg on npg.id = np.NamedPropertyGroupId
                                                  inner join PensionSchemes ps on ps.Id  = npg.PensionSchemeId
                                                  inner join SchemeDetail sd on sd.SchemeData_Id = ps.id
                                                  where np.Name = 'MaxLumpSum'
                                                  and ps.Name = 'The Space01 Test Scheme'").Tables[0].Rows[0]["Value"];

            Assert.AreEqual(double.Parse((string)maxFromScheme) * 1000000, max);
        }


    }
}
