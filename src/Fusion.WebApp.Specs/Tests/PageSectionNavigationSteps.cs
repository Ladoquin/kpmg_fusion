﻿namespace Fusion.WebApp.Specs.Tests
{
    using Fusion.WebApp.Controllers;
    using TechTalk.SpecFlow;

    [Binding]
    public class PageSectionNavigationSteps
    {
        [When(@"I view the (.*) panel")]
        public void IViewTheXSection(string panel)
        {
            var pageSection = PageSectionType.None;

            switch (panel.ToLower())
            {
                case "asset assumptions": 
                    pageSection = PageSectionType.AssetAssumptions; 
                    break;
                case "investment strategy assumptions": 
                case "investment strategy advanced":
                    pageSection = PageSectionType.InvestmentStrategyAssumptions;
                    ScenarioContext.Current.SetActionResult(ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy());
                    break;
                default: 
                    break;
            }

            ScenarioContext.Current.SetCurrentPageSection(pageSection);
        }
    }
}