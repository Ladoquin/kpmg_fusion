﻿namespace Fusion.WebApp.Specs.Tests.LDI
{
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class LDISharedSteps
    {
        [Then(@"LDI data is returned")]
        public void ThenLDIDataIsReturned()
        {
            var json = getJson(ScenarioContext.Current.GetCurrentPage());
            
            var ldiData = JsonReader.Get<object>(json, "ldiData");

            Assert.NotNull(ldiData);
            Assert.Greater(JsonReader.Get<double>(ldiData, "interestRate"), -1);
            Assert.Greater(JsonReader.Get<double>(ldiData, "inflation"), -1);
            
        }

        [Then(@"LDI data is not returned")]
        public void ThenLDIDataIsNotReturned()
        {
            var json = getJson(ScenarioContext.Current.GetCurrentPage());

            Assert.Null(JsonReader.Get<object>(json, "ldiData"));
        }

        private object getJson(PageType page)
        {
            switch (page)
            {
                case PageType.Home:
                    return (ControllerFactory.Get<HomeController>(ScenarioContext.Current).Get() as JsonResult).Data;
                case PageType.Baseline:
                    return (ControllerFactory.Get<BaselineController>(ScenarioContext.Current).Get() as JsonResult).Data;
                case PageType.Evolution:
                    return (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;
                case PageType.Analysis:
                    return (ControllerFactory.Get<JsonController>(ScenarioContext.Current).Assets() as JsonResult).Data;
                default:
                    return null;
            }
        }
    }
}
