﻿namespace Fusion.WebApp.Specs.Tests
{
    using System;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    public static class ScenarioContextExtensions
    {
        public static string GetSchemeName(this ScenarioContext current)
        {
            if (current.ContainsKey("schemeName"))
                return current.Get<string>("schemeName");

            return string.Empty;
        }

        public static string GetBasisDisplayName(this ScenarioContext current)
        {
            if (current.ContainsKey("basisDisplayName"))
                return current.Get<string>("basisDisplayName");

            return string.Empty;
        }

        public static PageType GetCurrentPage(this ScenarioContext current)
        {
            if (current.ContainsKey("currentPage"))
                return current.Get<PageType>("currentPage");

            return PageType.Home;
        }

        public static DateTime GetAttributionEnd(this ScenarioContext current)
        {
            if (current.ContainsKey("attributionEnd"))
                return current.Get<DateTime>("attributionEnd");

            return DateTime.MinValue;
        }

        public static PageSectionType GetCurrentPageSection(this ScenarioContext current)
        {
            if (current.ContainsKey("currentPageSection"))
                return current.Get<PageSectionType>("currentPageSection");

            return PageSectionType.None;
        }

        public static ActionResult GetLastActionResult(this ScenarioContext current)
        {
            if (current.ContainsKey("lastActionResult"))
                return current.Get<ActionResult>("lastActionResult");

            return null;
        }

        public static void SetSchemeName(this ScenarioContext current, string schemeName)
        {
            current["schemeName"] = schemeName;
        }

        public static void SetBasisDisplayName(this ScenarioContext current, string basisDisplayName)
        {
            current["basisDisplayName"] = basisDisplayName;
        }

        public static void SetCurrentPage(this ScenarioContext current, PageType page)
        {
            current["currentPage"] = page;
        }

        public static void SetAttributionEnd(this ScenarioContext current, DateTime attributionEnd)
        {
            current["attributionEnd"] = attributionEnd;
        }

        public static void SetCurrentPageSection(this ScenarioContext current, PageSectionType pageSection)
        {
            current["currentPageSection"] = pageSection;
        }

        public static void SetActionResult(this ScenarioContext current, ActionResult result)
        {
            current["lastActionResult"] = result;
        }
    }
}
