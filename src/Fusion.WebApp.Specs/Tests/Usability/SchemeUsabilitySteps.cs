using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using Fusion.Shared.Data;
using Fusion.Shared.Mocks;
using System;
using System.IO;
using System.Collections;
using System.Security.Principal;
using System.Linq;
using TechTalk.SpecFlow;
using System.Web.Mvc;
using NUnit.Framework;
using FlightDeck.DomainShared;
using WebMatrix.WebData;
using Moq;
using Newtonsoft.Json;

namespace Fusion.WebApp.Specs.Tests.Usability
{
    [Binding]
    public class SchemeUsabilitySteps
    {
        [BeforeFeature("schemeUsability")]
        public static void BeforeFeature()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Shared", "SetUpTestUser.sql"));
        }

        [AfterFeature("schemeUsability")]
        public static void AfterFeature()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Shared", "TearDownTestUser.sql"));
        }
        
        [Given(@"I have access to at least one scheme")]
        public void GivenIHaveAccessToAtLeastOneScheme()
        {
            // set at least 1 scheme in the context
            ScenarioContext.Current.Set(Helper.GetSchemeService().GetAllSummaries().First(), "scheme");
        }

        [Given(@"I am not a Client user")]
        public void GivenIAmNotAClientUser()
        {
            // set user in the context
            ScenarioContext.Current.Set(Helper.GetMockTestUser(), "mockUser");
        }

        [When(@"I add a scheme to my favourites")]
        public void WhenIAddASchemeToMyFavourites()
        {
            ScenarioContext.Current.Set(GetFavouriteActionResult(false), "jsonResult");
        }

        [Then(@"I am able to see the scheme in my favourites list")]
        public void ThenIAmAbleToSeeTheSchemeInMyFavouritesList()
        {
            var scheme = ScenarioContext.Current.Get<SchemeDetail>("scheme");
            var jsonResult = ScenarioContext.Current.Get<JsonResult>("jsonResult");
            var obj = JsonConvert.DeserializeObject<JsonObject>(
                JsonConvert.SerializeObject(jsonResult.Data));
            Assert.IsTrue(obj.result);
            Assert.AreEqual(obj.SchemeId, scheme.SchemeDetailId);
        }

        [Given(@"I have at least one scheme in my favourites list")]
        public void GivenIHaveAtLeastOneSchemeInMyFavouritesList()
        {
            ScenarioContext.Current.Set(GetFavouriteActionResult(false), "jsonResultBefore");
        }

        [When(@"I remove a scheme from my favourites list")]
        public void WhenIRemoveASchemeFromMyFavouritesList()
        {
            ScenarioContext.Current.Set(GetFavouriteActionResult(true), "jsonResultAfter");
        }

        [Then(@"The scheme gets removed from my favourites list")]
        public void ThenTheSchemeGetsRemovedFromMyFavouritesList()
        {
            var scheme = ScenarioContext.Current.Get<SchemeDetail>("scheme");
            var jsonResult = ScenarioContext.Current.Get<JsonResult>("jsonResultAfter");
            var obj = JsonConvert.DeserializeObject<JsonObject>(
                JsonConvert.SerializeObject(jsonResult.Data));
            Assert.IsTrue(obj.result);
            Assert.AreEqual(obj.SchemeId, scheme.SchemeDetailId);
            Assert.IsTrue(string.IsNullOrWhiteSpace(obj.schemeName));
        }

        [When(@"I try to add an existing favourite scheme again")]
        public void WhenITryToAddAnExistingFavouriteSchemeAgain()
        {
            ScenarioContext.Current.Set(GetFavouriteActionResult(false), "jsonResultAfter");
        }

        [Then(@"The scheme will only appear once in my favourites list")]
        public void ThenTheSchemeWillOnlyAppearOnceInMyFavouritesList()
        {
            var scheme = ScenarioContext.Current.Get<SchemeDetail>("scheme");
            var jsonResult = ScenarioContext.Current.Get<JsonResult>("jsonResultAfter");
            var obj = JsonConvert.DeserializeObject<JsonObject>(
                JsonConvert.SerializeObject(jsonResult.Data));
            Assert.IsFalse(obj.result);
            Assert.IsTrue(string.IsNullOrWhiteSpace(obj.schemeName));
        }

        #region Private methods
        private ActionResult GetFavouriteActionResult(bool removeFavourite)
        {
            var scheme = ScenarioContext.Current.Get<SchemeDetail>("scheme");
            var controller = Helper.SetController<AccountController>(new MockHttpContext() { User = ScenarioContext.Current.Get<IPrincipal>("mockUser") });
            ActionResult result = controller.AddRemoveFavouriteScheme(scheme.SchemeDetailId, removeFavourite);
            return result;
        }

        private class JsonObject
        {
            public bool result { get; set; }
            public int SchemeId { get; set; }
            public string schemeName { get; set; }
        }
        #endregion
    }
}
