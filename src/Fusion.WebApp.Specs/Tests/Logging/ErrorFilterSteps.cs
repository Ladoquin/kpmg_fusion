﻿namespace Fusion.WebApp.Specs.Tests.Logging
{
    using Fusion.Shared.Data;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using NUnit.Framework;
    using System;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;

    [Binding]
    public class ErrorFilterSteps
    {
        [BeforeScenario("logging")]
        public static void SetUp()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Logging", "Setup.sql"), DbHelper.Db.Log);
        }

        [AfterScenario("logging")]
        public static void TearDown()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Logging", "TearDown.sql"), DbHelper.Db.Log);
        }

        [Given(@"There are entries in the logging table")]
        public void GivenThereAreEntriesInTheLoggingTable()
        {
        }

        [When(@"I select the Error filter on the log results")]
        public void WhenISelectTheErrorFilterOnTheLogResults()
        {
            var controller = ControllerFactory.Get<DeveloperController>(ScenarioContext.Current);
            var result = controller.Log(string.Empty, "ERROR");
            ScenarioContext.Current.Set(result);
        }

        [Then(@"I only see log entries of type Error")]
        public void ThenIOnlySeeLogEntriesOfTypeError()
        {
            var result = ScenarioContext.Current.Get<ActionResult>() as ViewResult;
            Assert.IsNotNull(result);
            var model = result.Model as LogViewModel;
            Assert.IsNotNull(model);
            Assert.IsTrue(model.Entries.Any(), "No log entries found");
            Assert.IsTrue(model.Entries.All(x => string.Equals(x.Level, "ERROR")), "Log entries not of type ERROR found");
        }
    }
}
