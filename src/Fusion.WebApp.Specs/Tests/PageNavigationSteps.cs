﻿namespace Fusion.WebApp.Specs.Tests
{
    using Fusion.WebApp.Controllers;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class PageNavigationSteps
    {
        [When(@"I view the Home page")]
        public void WhenIViewTheHomePage()
        {
            var controller = ControllerFactory.Get<HomeController>(ScenarioContext.Current);

            controller.Index();

            ScenarioContext.Current.SetCurrentPage(PageType.Home);
        }

        [When(@"I view the Baseline page")]
        public void WhenIViewTheBaselinePage()
        {
            var controller = ControllerFactory.Get<BaselineController>(ScenarioContext.Current);

            controller.Index();

            ScenarioContext.Current.SetCurrentPage(PageType.Baseline);
        }

        [When(@"I view the Evolution page")]
        public void WhenIViewTheEvolutionPage()
        {
            var controller = ControllerFactory.Get<EvolutionController>(ScenarioContext.Current);

            controller.Index();

            ScenarioContext.Current.SetCurrentPage(PageType.Evolution);
        }

        [When(@"I view the Analysis page")]
        public void WhenIViewTheAnalysisPage()
        {
            var controller = ControllerFactory.Get<AnalysisController>(ScenarioContext.Current);

            controller.Index();

            ScenarioContext.Current.SetCurrentPage(PageType.Analysis);
        }

        [When(@"I view the Journey Plan page")]
        public void WhenIViewTheJourneyPlanPage()
        {
            var currentSettingsKey = "currentJPSettings";
            var controller = ControllerFactory.Get<JourneyPlanController>(ScenarioContext.Current);

            controller.Index();
            var initialJPSettings = controller.NewJourneyPlanData();

            ScenarioContext.Current.SetCurrentPage(PageType.JourneyPlan);

            if (ScenarioContext.Current.ContainsKey(currentSettingsKey))
                ScenarioContext.Current.Remove(currentSettingsKey);

            ScenarioContext.Current.Add(currentSettingsKey, initialJPSettings);
        }
    }
}
