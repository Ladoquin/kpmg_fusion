﻿namespace Fusion.WebApp.Specs.Tests
{
    public enum PageType
    {
        Home, 
        Baseline,
        Evolution,
        Analysis,
        JourneyPlan
    }
}
