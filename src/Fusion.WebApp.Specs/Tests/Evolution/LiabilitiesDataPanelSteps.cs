﻿
namespace Fusion.WebApp.Specs.Tests.Evolution
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;

    [Binding]
    public class LiabilitiesDataPanelSteps
    {
        const string KeyCommutationOfAllowanceLabel = "CommutationOfAllowanceLabel";
        const string KeyPensionerLifeExpectancyLabel = "PensionerLifeExpectancyLabel";
        const string KeyNonPensionerLifeExpectancyLabel = "NonPensionerLifeExpectancyLabel";

        [Then(@"I do not see a change in the commutation of allowance value")]
        public void ThenIDoNotSeeAChangeInTheCommutationOfAllowanceValue()
        {
            var liabilityAssumptions = getInitialAndCurrentValues(ScenarioContext.Current);

            Assert.True(liabilityAssumptions.ContainsKey("SacrificeProportion"));
            Assert.AreEqual(liabilityAssumptions["SacrificeProportion"].Before.Value, liabilityAssumptions["SacrificeProportion"].After.Value);
        }

        [Then(@"I see a change in the commutation of allowance value")]        
        public void ThenISeeAChangeInTheCommutationOfAllowanceValue()
        {
            var liabilityAssumptions = getInitialAndCurrentValues(ScenarioContext.Current);

            Assert.True(liabilityAssumptions.ContainsKey("SacrificeProportion"));
            Assert.AreNotEqual(liabilityAssumptions["SacrificeProportion"].Before.Value, liabilityAssumptions["SacrificeProportion"].After.Value);
        }

        [Then(@"I do not see a change in the life expectancy values")]
        public void ThenIDoNotSeeAChangeInTheLifeExpectancyValues()
        {
            var liabilityAssumptions = getInitialAndCurrentValues(ScenarioContext.Current);

            Assert.True(liabilityAssumptions.ContainsKey("LifeExpectancy65"));
            Assert.True(liabilityAssumptions.ContainsKey("LifeExpectancy65_45"));
            Assert.AreEqual(liabilityAssumptions["LifeExpectancy65"].Before.Value, liabilityAssumptions["LifeExpectancy65"].After.Value);
            Assert.AreEqual(liabilityAssumptions["LifeExpectancy65_45"].Before.Value, liabilityAssumptions["LifeExpectancy65_45"].After.Value);
        }

        [Then(@"I see a change in the life expectancy values")]
        public void ThenISeeAChangeInTheLifeExpectancyValues()
        {
            var liabilityAssumptions = getInitialAndCurrentValues(ScenarioContext.Current);

            Assert.True(liabilityAssumptions.ContainsKey("LifeExpectancy65"));
            Assert.True(liabilityAssumptions.ContainsKey("LifeExpectancy65_45"));
            Assert.AreNotEqual(liabilityAssumptions["LifeExpectancy65"].Before.Value, liabilityAssumptions["LifeExpectancy65"].After.Value);
            Assert.AreNotEqual(liabilityAssumptions["LifeExpectancy65_45"].Before.Value, liabilityAssumptions["LifeExpectancy65_45"].After.Value);
        }

        [When(@"I note the commutation of allowance label")]
        public void WhenINoteTheCommutationOfAllowanceLabel()
        {
            var liabilityAssumptions = getInitialAndCurrentValues(ScenarioContext.Current);

            ScenarioContext.Current.Add(KeyCommutationOfAllowanceLabel, liabilityAssumptions["SacrificeProportion"].After.Label);
        }

        [When(@"I note the life expectancy labels")]
        public void WhenINoteTheLifeExpectancyLabels()
        {
            var liabilityAssumptions = getInitialAndCurrentValues(ScenarioContext.Current);

            ScenarioContext.Current.Add(KeyPensionerLifeExpectancyLabel, liabilityAssumptions["LifeExpectancy65"].After.Label);
            ScenarioContext.Current.Add(KeyNonPensionerLifeExpectancyLabel, liabilityAssumptions["LifeExpectancy65_45"].After.Label);
        }

        [Then(@"I see a change in the life expectancy labels")]
        public void ThenISeeAChangeInTheLifeExpectancyLabels()
        {
            var notedPenLabel = ScenarioContext.Current.Get<string>(KeyPensionerLifeExpectancyLabel);
            var notedNonPenLabel = ScenarioContext.Current.Get<string>(KeyNonPensionerLifeExpectancyLabel);

            var liabilityAssumptions = getInitialAndCurrentValues(ScenarioContext.Current);

            Assert.AreNotEqual(notedPenLabel, liabilityAssumptions["LifeExpectancy65"].After.Label);
            Assert.AreNotEqual(notedNonPenLabel, liabilityAssumptions["LifeExpectancy65_45"].After.Label);
        }

        private Dictionary<string, BeforeAfter<KeyLabelValue<string, double>>> getInitialAndCurrentValues(ScenarioContext context)
        {
            var controller = ControllerFactory.Get<EvolutionController>(context);

            var result = (controller.EvolutionAssumptionsPanel() as JsonResult).Data;

            var liabilityAssumptions = JsonReader.Get<object>(result, "liabilityAssumptions");

            var initial = JsonReader.Get<IEnumerable<KeyLabelValue<string, double>>>(liabilityAssumptions, "initial");
            var current = JsonReader.Get<IEnumerable<object>>(liabilityAssumptions, "current");

            var before = new Dictionary<string, KeyLabelValue<string, double>>();
            var after = new Dictionary<string, KeyLabelValue<string, double>>();

            foreach (var keyLabelValue in initial)
            {
                switch (keyLabelValue.Key)
                {
                    case "SacrificeProportion":
                    case "LifeExpectancy65":
                    case "LifeExpectancy65_45":
                        before.Add(keyLabelValue.Key, new KeyLabelValue<string, double>(keyLabelValue.Key, keyLabelValue.Label, keyLabelValue.Value));
                        break;
                }
            }
            foreach (var keyLabelValue in current)
            {
                var key = JsonReader.Get<string>(keyLabelValue, "Key");
                switch (key)
                {
                    case "SacrificeProportion":
                    case "LifeExpectancy65":
                    case "LifeExpectancy65_45":
                        after.Add(key, new KeyLabelValue<string, double>(key, JsonReader.Get<string>(keyLabelValue, "Label"), JsonReader.Get<double>(keyLabelValue, "Value")));
                        break;
                }
            }

            if (!before.Keys.OrderBy(key => key).SequenceEqual(after.Keys.OrderBy(key => key)))
                throw new Exception("Could not find all matching sets of initial and current liability assumption values");

            return before.Keys.ToDictionary(x => x, x => new BeforeAfter<KeyLabelValue<string, double>>(before[x], after[x]));
        }
    }
}
