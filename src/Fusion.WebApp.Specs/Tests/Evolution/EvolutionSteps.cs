﻿namespace Fusion.WebApp.Specs.Tests.Evolution
{
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    using System.Linq;

    [Binding]
    public class EvolutionSteps
    {
        [Then(@"I see the evolution of liabilities chart")]
        public void ThenISeeTheEvolutionOfLiabilitiesChart()
        {
            var controller = ControllerFactory.Get<EvolutionController>(ScenarioContext.Current);

            var result = controller.Tracker() as ActionResult;

            Assert.NotNull(result);

            var json = (result as JsonResult).Data;

            Assert.NotNull(json);

            var comparisonData = JsonReader.Get<IEnumerable>(json, "comparisonData");

            Assert.NotNull(comparisonData);

            var actual = new Dictionary<string, Dictionary<DateTime, double>>();

            foreach (var item in comparisonData)
            {
                var basisName = JsonReader.Get<string>(item, "basis");
                var points = new Dictionary<DateTime, double>();
                foreach (var point in JsonReader.Get<IEnumerable>(item, "points"))
                {
                    points.Add(JsonHelper.UnixEpoch.AddMilliseconds(JsonReader.Get<double>(point, "date")), JsonReader.Get<double>(point, "value"));
                }
                actual.Add(basisName, points);
            }

            var date1 = new DateTime(2014, 12, 31);

            Assert.AreEqual(4, actual.Count);
            Assert.Contains("Accounting", actual.Keys);
            Assert.Contains("Technical Provisions", actual.Keys);
            Assert.Contains("Buyout", actual.Keys);
            Assert.Contains("Gilts", actual.Keys);
            Assert.True(actual["Accounting"].ContainsKey(date1) && actual["Accounting"][date1] > 0);
            Assert.True(actual["Technical Provisions"].ContainsKey(date1) && actual["Technical Provisions"][date1] > 0);
            Assert.True(actual["Buyout"].ContainsKey(date1) && actual["Buyout"][date1] > 0);
            Assert.True(actual["Gilts"].ContainsKey(date1) && actual["Gilts"][date1] > 0);
        }

        [Then(@"I see the precision is correct on the evolution of funding level % chart")]
        public void ThenISeeThePrecisionIsCorrectOnTheEvolutionOfFundingLevelChart()
        {
            
        }

        [Then(@"I see that the tooltips on the funding level % chart displays the % values with the precision of (.*) decimal place\(s\)")]
        public void ThenISeeThatTheTooltipsOnTheFundingLevelChartDisplaysTheValuesWithThePrecisionOfDecimalPlaceS(int precision)
        {
            var controller = ControllerFactory.Get<EvolutionController>(ScenarioContext.Current);

            var result = controller.Tracker() as ActionResult;

            var json = (result as JsonResult).Data;

            Func<double, int> precisionCount = x =>
            {
                int[] bits = Decimal.GetBits((decimal)x);
                int exponent = bits[3] >> 16;
                int decimalCount = exponent;
                long lowDecimal = bits[0] | (bits[1] >> 8);
                while ((lowDecimal % 10) == 0)
                {
                    decimalCount--;
                    lowDecimal /= 10;
                }

                return decimalCount;
            };

            var fundingLevel = JsonReader.Get<IEnumerable<dynamic>>(json, "fundingLevelTracker");
            var expectedFundingLevel = JsonReader.Get<IEnumerable<dynamic>>(json, "expectedFundingLevelTracker");

            Assert.False(fundingLevel.Any(x => precisionCount(JsonReader.Get<double>(x, "value")) > precision));
            Assert.False(expectedFundingLevel.Any(x => precisionCount(JsonReader.Get<double>(x, "value")) > precision));
        }

    }
}
