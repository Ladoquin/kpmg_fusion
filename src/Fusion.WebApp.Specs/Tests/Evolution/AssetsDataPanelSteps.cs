﻿namespace Fusion.WebApp.Specs.Tests.Evolution
{
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class AssetsDataPanelSteps
    {
        const string KeyNotedClassValues = "KeyNotedClassValues";

        [When(@"I note the assets breakdown chart values")]
        public void WhenINoteTheAssetsBreakdownChartValues()
        {
            var notedClassValues = getCurrentAssetClassValues();

            ScenarioContext.Current.Add(KeyNotedClassValues, notedClassValues);
        }

        [Then(@"I see the assets breakdown chart displaying the noted values")]
        public void ThenISeeTheAssetsBreakdownChartDisplayingTheNotedValues()
        {
            var expected = ScenarioContext.Current.Get<Dictionary<string, double>>(KeyNotedClassValues);

            var actual = getCurrentAssetClassValues();

            Assert.That(actual, Is.EquivalentTo(expected));
        }

        [Then(@"I see the assets breakdown chart displaying synthetic assets")]
        public void ThenISeeTheAssetsBreakdownChartDisplayingSyntheticAssets()
        {
            var controller = ControllerFactory.Get<EvolutionController>(ScenarioContext.Current);

            var data = (controller.EvolutionAssumptionsPanel() as JsonResult).Data;

            var actual = JsonReader.Get<double>(JsonReader.Get<object>(data, "syntheticAssetInfo"), "TotalAmount");

            Assert.Greater(actual, 0);
        }

        private Dictionary<string, double> getCurrentAssetClassValues()
        {
            var controller = ControllerFactory.Get<EvolutionController>(ScenarioContext.Current);

            var data = (controller.EvolutionAssumptionsPanel() as JsonResult).Data;

            var notedClassValues =
                JsonReader.Get<IEnumerable<BreakdownItem>>(data, "assetsBreakdown").First().SubItems
                    .ToDictionary(x => x.Label, x => x.Value);

            return notedClassValues;
        }
    }
}
