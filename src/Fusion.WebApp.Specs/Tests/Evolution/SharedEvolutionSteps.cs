﻿namespace Fusion.WebApp.Specs.Tests.Evolution
{
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using System;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class SharedEvolutionSteps
    {
        [When(@"I set the Attribution Period Start Date to (.*)")]
        public void WhenISetTheAttributionPeriodStartDateTo(string date)
        {
            var evolutionController = ControllerFactory.Get<EvolutionController>(ScenarioContext.Current);

            var indexViewData = (evolutionController.Index() as ViewResult).Model;

            var currentAttributionEndDate = (indexViewData as SchemeViewModel).SchemeData.CurrentBasis.AttributionEnd;

            var attributionStartDate = new DateTime(int.Parse(date.Split('/')[2]), int.Parse(date.Split('/')[1]), int.Parse(date.Split('/')[0]));

            var jsonStartDate = DateConverter.ConvertToJsonDate(attributionStartDate);
            var jsonEndDate = DateConverter.ConvertToJsonDate(currentAttributionEndDate);

            var jsonController = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            jsonController.SetAnalysisPeriod(jsonStartDate, jsonEndDate, true);
        }

        [When(@"I set the Attribution Period End Date to (.*)")]
        public void WhenISetTheAttributionPeriodEndDateTo(string date)
        {
            var evolutionController = ControllerFactory.Get<EvolutionController>(ScenarioContext.Current);

            var indexViewData = (evolutionController.Index() as ViewResult).Model;

            var currentAttributionStartDate = (indexViewData as SchemeViewModel).SchemeData.CurrentBasis.AttributionStart;

            var attributionEndDate = new DateTime(int.Parse(date.Split('/')[2]), int.Parse(date.Split('/')[1]), int.Parse(date.Split('/')[0]));

            var jsonStartDate = new TimeSpan(currentAttributionStartDate.Ticks).Add(-new TimeSpan(JsonHelper.UnixEpoch.Ticks)).TotalMilliseconds.ToString();
            var jsonEndDate = new TimeSpan(attributionEndDate.Ticks).Add(-new TimeSpan(JsonHelper.UnixEpoch.Ticks)).TotalMilliseconds.ToString();

            var jsonController = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            jsonController.SetAnalysisPeriod(jsonStartDate, jsonEndDate, true);

            ScenarioContext.Current.SetAttributionEnd(attributionEndDate);
        }
    }
}
