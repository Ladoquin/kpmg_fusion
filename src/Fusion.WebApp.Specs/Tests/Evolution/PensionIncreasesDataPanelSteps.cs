﻿namespace Fusion.WebApp.Specs.Tests.Evolution
{
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class PensionIncreasesDataPanelSteps
    {
        [Then(@"I should see pension increases values for the attribution start and end dates")]
        public void ThenIShouldSeePensionIncreasesValuesForTheAttributionStartAndEndDates()
        {
            var json = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;
            var initial = JsonReader.Get<IEnumerable<PensionIncreaseDefinition>>(JsonReader.Get<object>(json, "pensionIncreases"), "initial");
            var current = JsonReader.Get<IEnumerable<PensionIncreaseDefinition>>(JsonReader.Get<object>(json, "pensionIncreases"), "current");

            Assert.NotNull(initial);
            Assert.NotNull(current);
            Assert.Greater(initial.Count(), 0);
            Assert.Greater(current.Count(), 0);
        }

        [Then(@"I should see a different number of pension increase values in the before and after columns")]
        public void ThenIShouldSeeADifferentNumberOfPensionIncreaseValuesInTheBeforeAndAfterColumns()
        {
            var json = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;
            var initial = JsonReader.Get<IEnumerable<PensionIncreaseDefinition>>(JsonReader.Get<object>(json, "pensionIncreases"), "initial");
            var current = JsonReader.Get<IEnumerable<PensionIncreaseDefinition>>(JsonReader.Get<object>(json, "pensionIncreases"), "current");

            Assert.AreNotEqual(initial.Where(x => x.IsVisible).Count(), current.Where(x => x.IsVisible).Count());
        }
    }
}
