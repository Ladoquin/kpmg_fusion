﻿namespace Fusion.WebApp.Specs.Tests.Evolution
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class AssetsGrowthDataPanelSteps
    {
        const string KeyAssetBreakdownTotal = "AssetBreakdownTotal";

        [Then(@"I see the asset growth values all with number values and unique labels")]
        public void ThenISeeTheAssetGrowthValuesAllWithNumberValuesAndUniqueLabels()
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;

            var values = JsonReader.Get<IEnumerable<object>>(data, "assetsGrowth").ToList();

            Assert.NotNull(values);
            Assert.Greater(values.Count, 0);

            var labels = new List<string>();
            foreach (var value in values)
            {
                var label = JsonReader.Get<string>(value, "Name");
                var growth = JsonReader.Get<double?>(value, "Growth");
                var amount = JsonReader.Get<double>(value, "Amount");
                Assert.IsNotNullOrEmpty(label);
                Assert.True(growth == null || growth.GetType() == typeof(double));
                Assert.True(amount.GetType() == typeof(double));
                Assert.False(labels.Contains(label));

                labels.Add(label);
            }
        }

        [When(@"I note the current asset growth values")]
        public void WhenINoteTheCurrentAssetGrowthValues()
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;

            ScenarioContext.Current.Add("SavedAssetGrowthValues", JsonReader.Get<IEnumerable<object>>(data, "assetsGrowth").ToList());
        }

        [Then(@"I see that the same asset growth labels are returned but the values have changed")]
        public void ThenISeeThatTheSameAssetGrowthLabelsAreReturnedButTheValuesHaveChanged()
        {
            Func<object, dynamic> convertFunc = x =>
                new { Key = JsonReader.Get<string>(x, "Name"), Growth = JsonReader.Get<double?>(x, "Growth"), Amount = JsonReader.Get<double>(x, "Amount") };

            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;
            var initial = ScenarioContext.Current.Get<IEnumerable<object>>("SavedAssetGrowthValues").Select(convertFunc).ToList();
            var current = JsonReader.Get<IEnumerable<object>>(data, "assetsGrowth").Select(convertFunc).ToList();

            Assert.That(current.Select(x => x.Key).OrderBy(x => x).ToList(), Is.EquivalentTo(initial.Select(x => x.Key).OrderBy(x => x).ToList()));
            foreach (var item in initial)
            {
                var curr = current.Single(x => x.Key == item.Key);
                if (item.Growth != null)
                {
                    Assert.AreNotEqual(item.Growth, curr.Growth);
                }
                if (curr.Amount != 0 || item.Amount != 0)
                {
                    Assert.AreNotEqual(item.Amount, curr.Amount);
                }
            }
        }

        [Then(@"I see that the following funds do not have a growth value")]
        public void ThenISeeThatTheFollowingFundsDoNotHaveAGrowthValue(Table funds)
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;

            var values = JsonReader.Get<IEnumerable<object>>(data, "assetsGrowth").ToList();

            foreach (var value in values)
            {
                var label = JsonReader.Get<string>(value, "Name");
                if (funds.Header.Contains(label))
                    Assert.AreEqual((double?)null, JsonReader.Get<double?>(value, "Growth"));
                else
                    Assert.AreNotEqual((double?)null, JsonReader.Get<double?>(value, "Growth"));
            }
        }

        [When(@"I note the current asset breakdown total")]
        public void WhenINoteTheCurrentAssetBreakdownTotal()
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;

            var assetsBreakdown = JsonReader.Get<IEnumerable<BreakdownItem>>(data, "assetsBreakdown");

            var assetBreakdownTotal = assetsBreakdown.SelectMany(x => x.SubItems).Sum(x => x.Value);

            ScenarioContext.Current.Add(KeyAssetBreakdownTotal, assetBreakdownTotal);
        }

        [Then(@"I see the asset growth values total is the same as the asset breakdown total")]
        public void ThenISeeTheAssetGrowthValuesTotalIsTheSameAsTheAssetBreakdownTotal()
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;

            var assetsGrowth = JsonReader.Get<IEnumerable<AssetFundGrowthAndValue>>(data, "assetsGrowth");

            var assetsGrowthTotal = assetsGrowth.Sum(x => x.Amount);

            Assert.That(assetsGrowthTotal, Is.EqualTo(ScenarioContext.Current[KeyAssetBreakdownTotal]).Within(Utils.TestPrecision));
        }

    }
}
