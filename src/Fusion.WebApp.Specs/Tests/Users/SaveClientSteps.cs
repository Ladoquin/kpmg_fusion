﻿namespace Fusion.WebApp.Specs.Tests.Users
{
    using FlightDeck.DomainShared;
    using Fusion.Shared.Data;
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using TechTalk.SpecFlow;

    [Binding]
    public class SaveClientSteps
    {
        const string clientusername = "saveclienttestuser";
        const string firstname = "foo";
        const string lastname = "bar";
        const string emailaddress = "foo@space01.co.uk";

        [Given(@"I have completed the user details form for a Client with no schemes assigned")]
        public void GivenIHaveCompletedTheUserDetailsFormForAClientWithNoSchemesAssigned()
        {
            var model = new AdditUserViewModel
            {
                UserName = clientusername,
                Role = RoleType.Client,
                EmailAddress = emailaddress, 
                FirstName = firstname,
                LastName = lastname,
                Schemes = new List<string>()
            };

            ScenarioContext.Current.Set(model, "model");
        }

        [Given(@"I have completed the user details form for a Client with schemes assigned")]
        public void GivenIHaveCompletedTheUserDetailsFormForAClientWithSchemesAssigned()
        {
            var model = new AdditUserViewModel
                {
                    UserName = clientusername,
                    Role = RoleType.Client,
                    EmailAddress = emailaddress,
                    FirstName = firstname,
                    LastName = lastname,
                    Schemes = new List<string> { "The Space01 Test Scheme" }
                };

            ScenarioContext.Current.Set(model, "model");
        }

        [Given(@"the Client I am about to save already exists")]
        public void GivenTheClientIAmAboutToSaveAlreadyExists()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Users", "SetUpExistingClientUser.sql"));
        }

        [When(@"I attempt to create the user")]
        public void WhenIAttemptToCreateTheUser()
        {
            var controller = ControllerFactory.Get<UsersController>(ScenarioContext.Current);
            var model = ScenarioContext.Current.Get<AdditUserViewModel>("model");

            var result = controller.Add(model);

            ScenarioContext.Current.Set(result);
        }

        [When(@"I attempt to save the user")]
        public void WhenIAttemptToSaveTheUser()
        {            
            var controller = ControllerFactory.Get<UsersController>(ScenarioContext.Current);
            var model = ScenarioContext.Current.Get<AdditUserViewModel>("model");

            model.CurrentUserName = clientusername;

            var result = controller.Edit(model);

            ScenarioContext.Current.Set(result);
        }

        [Then(@"save user is unsuccesful")]
        public void ThenSaveUserIsUnsuccesful()
        {
            var result = ScenarioContext.Current.Get<ActionResult>() as ViewResult;
            Assert.IsNotNull(result);
            var model = result.Model as ManageUserViewModel;
            Assert.IsNotNull(model);
            Assert.IsTrue(model.AdditUserViewModel.Result == AdditUserResult.AddUserFailure || model.AdditUserViewModel.Result == AdditUserResult.EditUserFailure);
        }

        [Then(@"I am informed that it is because there are no schemes assigned")]
        public void ThenIAmInformedThatItIsBecauseThereAreNoSchemesAssigned()
        {
            var result = ScenarioContext.Current.Get<ActionResult>() as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ModelState.Keys.Contains("Role"));
            Assert.AreEqual(1, result.ViewData.ModelState["Role"].Errors.Count);
            Assert.AreEqual("Must be assigned to a scheme", result.ViewData.ModelState["Role"].Errors[0].ErrorMessage);
        }

        [Then(@"the user is able to resubmit the form")]
        public void ThenTheUserIsAbleToResubmitTheForm()
        {
            var model = (ScenarioContext.Current.Get<ActionResult>() as ViewResult).Model as ManageUserViewModel;

            Assert.IsFalse(model.AdditUserViewModel.RolesAvailable.IsNullOrEmpty());
            Assert.AreEqual(clientusername, model.AdditUserViewModel.UserName);
            Assert.AreEqual(firstname, model.AdditUserViewModel.FirstName);
            Assert.AreEqual(lastname, model.AdditUserViewModel.LastName);
            Assert.AreEqual(emailaddress, model.AdditUserViewModel.EmailAddress);
            Assert.AreEqual(RoleType.Client, model.AdditUserViewModel.Role.GetValueOrDefault(RoleType.Admin));
        }

        [Then(@"save user is succesful")]
        public void ThenSaveUserIsSuccesful()
        {
            var result = ScenarioContext.Current.Get<ActionResult>() as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Manage", result.RouteValues["action"]);            
        }

        [AfterScenario("saveclient")]
        public void AfterScenario()
        {
            new MembershipHelper().DeleteUser(clientusername);
        }
    }
}
