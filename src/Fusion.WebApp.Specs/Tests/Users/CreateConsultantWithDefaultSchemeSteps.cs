﻿using FlightDeck.DomainShared;
using Fusion.WebApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TechTalk.SpecFlow;
using System.Linq;
using Fusion.WebApp.Controllers;
using FlightDeck.Services;
using FlightDeck.ServiceInterfaces;
using System.Web;
using Fusion.Shared.Data;
using System.Data;

namespace Fusion.WebApp.Specs
{
    [Binding]
    public class CreateConsultantWithDefaultSchemeSteps
    {
        const string consultantusername = "saveconsultanttestuser";
        const string firstname = "foo";
        const string lastname = "bar";
        const string emailaddress = "foo@space01.co.uk";

        [When(@"I have completed the user details form for a KPMG Consultant")]
        public void WhenIHaveCompletedTheUserDetailsFormForAKPMGConsultant()
        {

            var model = new AdditUserViewModel
            {
                UserName = consultantusername,
                Role = RoleType.Consultant,
                EmailAddress = emailaddress,
                FirstName = firstname,
                LastName = lastname,
                Schemes = new List<string>(),
                Password = "password"
            };

            ScenarioContext.Current.Set(model, "model");
        }

        [Then(@"when the new user logs in they should have the ABC scheme as their active scheme")]
        public void ThenWhenTheNewUserLogsInTheyShouldHaveTheABCSchemeAsTheirActiveScheme()
        {
            var db = new DbHelper();
            var ds = db.RunQuery(@"select sd.SchemeData_SchemeRef
                                    from UserProfile up 
                                    inner join SchemeDetail sd on up.ActiveSchemeDetailId = sd.SchemeDetailId
                                    where up.UserName = '" + consultantusername + "'");

            Assert.IsTrue((string)ds.Tables[0].Rows[0][0] == "ABC01");         
        }


        [AfterScenario("createconsultant")]
        public void AfterScenario()
        {
            new MembershipHelper().DeleteUser(consultantusername);
        }
    }
}
