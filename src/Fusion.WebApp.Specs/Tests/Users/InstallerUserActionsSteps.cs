﻿using Fusion.Shared.Data;
using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Users
{
    [Binding]
    public class InstallerUserActionsSteps
    {
        const string key_restrictedSchemes = "restrictedSchemes";
        const string key_clientUserId = "clientUserId";
        const string key_userManageResult = "userManageResult";
        const string key_userEditResult = "userEditResult";
        const string key_removedSchemeNames = "removedSchemeNames";

        [Given(@"More than one restricted scheme exists")]
        public void GivenMoreThanOneRestrictedSchemeExists()
        {
            // find the restricted schemes in the system

            var restrictedSchemes = new Dictionary<string, int>();
            foreach (DataRow row in new DbHelper().RunQuery("select SchemeDetailId, SchemeName from SchemeDetail where IsRestricted = 1").Tables[0].Rows)
            {
                restrictedSchemes.Add((string)row["SchemeName"], (int)row["SchemeDetailId"]);
            }
             
            if (restrictedSchemes.Count == 0)
                Assert.Fail("Test expects more than one restricted scheme to exist");

            ScenarioContext.Current.Add(key_restrictedSchemes, restrictedSchemes);
        }

        [Given(@"A client exists and has access to more than one restricted scheme")]
        public void GivenAClientExistsAndHasAccessToMoreThanOneRestrictedScheme()
        {
            var restrictedSchemes = ScenarioContext.Current.Get<Dictionary<string, int>>(key_restrictedSchemes);

            var db = new DbHelper();            

            // create a client

            var userId = (int)db.RunScriptWithOutput(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Users", "CreateTestClientUser.sql")).Tables[0].Rows[0]["UserId"];

            // give client user access to the first two restricted schemes found

            db.RunCommand(string.Format("insert into UserProfileSchemeDetail (UserId, SchemeDetailId) values ({0}, {1}), ({0}, {2})", userId, restrictedSchemes.First().Value, restrictedSchemes.Skip(1).First().Value));

            // also give client access to two unrestricted schemes

            db.RunCommand(string.Format("insert into UserProfileSchemeDetail (UserId, SchemeDetailId) values ({0}, (select SchemeDetailId from SchemeDetail where SchemeName = 'The Space01 Test Scheme'))", userId));
            db.RunCommand(string.Format("insert into UserProfileSchemeDetail (UserId, SchemeDetailId) values ({0}, (select SchemeDetailId from SchemeDetail where SchemeName = 'The ABC Pension Scheme'))", userId));

            ScenarioContext.Current.Add(key_clientUserId, userId);
        }

        [Given(@"I have access to at least one, but not all, of the restricted schemes the client has access to")]
        public void GivenIHaveAccessToAtLeastOneButNotAllOfTheRestrictedSchemesTheClientHasAccessTo()
        {
            var installerUsername = ScenarioContext.Current.Get<string>("usercreated");
            var restrictedSchemes = ScenarioContext.Current.Get<Dictionary<string, int>>(key_restrictedSchemes);

            // give installer access to the first restricted scheme the client has access to

            new DbHelper().RunCommand(
                string.Format("insert into UserProfileSchemeDetail (UserId, SchemeDetailId) values ((select UserId from UserProfile where UserName = '{0}'), {1})",
                    installerUsername,
                    restrictedSchemes.First().Value));
        }

        [When(@"I view the client")]
        public void WhenIViewTheClient()
        {
            var controller = ControllerFactory.Get<UsersController>(ScenarioContext.Current);

            var userId = ScenarioContext.Current.Get<int>(key_clientUserId);

            var result = controller.Manage(userId) as ViewResult;

            ScenarioContext.Current.Add(key_userManageResult, result);
        }

        [When(@"I remove access for the client to one or more restricted schemes")]
        public void WhenIRemoveAccessForTheClientToOneOrMoreRestrictedSchemes()
        {
            var restrictedSchemes = ScenarioContext.Current.Get<Dictionary<string, int>>(key_restrictedSchemes);
            var user = ScenarioContext.Current.Get<ViewResult>(key_userManageResult).Model as ManageUserViewModel;
            var controller = ControllerFactory.Get<UsersController>(ScenarioContext.Current);

            // remove the one restricted scheme that the installer had access to (the second restricted scheme should remain, but the installer can't see that)

            user.AdditUserViewModel.Schemes = new List<string> { "The Space01 Test Scheme", "The ABC Pension Scheme" };

            var result = controller.Edit(user.AdditUserViewModel) as ViewResult;

            ScenarioContext.Current.Add(key_userEditResult, result);
            ScenarioContext.Current.Add(key_removedSchemeNames, new List<string> { restrictedSchemes.First().Key });
        }

        [When(@"I remove access for the client to one or more unrestricted schemes")]
        public void WhenIRemoveAccessForTheClientToOneOrMoreUnrestrictedSchemes()
        {
            var restrictedSchemes = ScenarioContext.Current.Get<Dictionary<string, int>>(key_restrictedSchemes);
            var user = ScenarioContext.Current.Get<ViewResult>(key_userManageResult).Model as ManageUserViewModel;
            var controller = ControllerFactory.Get<UsersController>(ScenarioContext.Current);

            // remove one of the unrestricted schemes
            user.AdditUserViewModel.Schemes = new List<string> { "The Space01 Test Scheme", restrictedSchemes.First().Key };

            var result = controller.Edit(user.AdditUserViewModel) as ViewResult;

            ScenarioContext.Current.Add(key_userEditResult, result);
            ScenarioContext.Current.Add(key_removedSchemeNames, new List<string> { "The ABC Pension Scheme" });
        }

        [Then(@"I should see all the unrestricted schemes the client has access to")]
        public void ThenIShouldSeeAllTheUnrestrictedSchemesTheClientHasAccessTo()
        {
            var userId = ScenarioContext.Current.Get<int>(key_clientUserId);
            var userManageResult = ScenarioContext.Current.Get<ViewResult>(key_userManageResult).Model as ManageUserViewModel;

            var actual = userManageResult.AdditUserViewModel.Schemes.ToList();

            var expected = new List<string>();
            foreach (DataRow row in new DbHelper().RunQuery("select s.SchemeName from UserProfileSchemeDetail u inner join SchemeDetail s on u.SchemeDetailId = s.SchemeDetailId where s.IsRestricted = 0 and u.UserId = " + userId).Tables[0].Rows)
                expected.Add((string)row["SchemeName"]);

            Assert.True(expected.All(x => actual.Contains(x)));
        }

        [Then(@"I should see all the restricted schemes the client has access to which I have access to")]
        public void ThenIShouldSeeAllTheRestrictedSchemesTheClientHasAccessToWhichIHaveAccessTo()
        {
            var installerUsername = ScenarioContext.Current.Get<string>("usercreated");
            var userId = ScenarioContext.Current.Get<int>(key_clientUserId);
            var userManageResult = ScenarioContext.Current.Get<ViewResult>(key_userManageResult).Model as ManageUserViewModel;

            var actual = userManageResult.AdditUserViewModel.Schemes.ToList();

            var clientsRestrictedSchemes = new List<string>();
            foreach (DataRow row in new DbHelper().RunQuery("select s.SchemeName from UserProfileSchemeDetail u inner join SchemeDetail s on u.SchemeDetailId = s.SchemeDetailId where s.IsRestricted = 1 and u.UserId = " + userId).Tables[0].Rows)
                clientsRestrictedSchemes.Add((string)row["SchemeName"]);
            var installersRestrictedSchemes = new List<string>();
            foreach (DataRow row in new DbHelper().RunQuery("select s.SchemeName from UserProfileSchemeDetail u inner join SchemeDetail s on u.SchemeDetailId = s.SchemeDetailId where s.IsRestricted = 1 and u.UserId = (select UserId from UserProfile where UserName = '" + installerUsername + "')").Tables[0].Rows)
                installersRestrictedSchemes.Add((string)row["SchemeName"]);

            var schemesInstallerShouldSee = clientsRestrictedSchemes.Intersect(installersRestrictedSchemes);
            var schemesInstallerShouldNotSee = clientsRestrictedSchemes.Except(installersRestrictedSchemes);

            Assert.True(schemesInstallerShouldSee.All(x => actual.Contains(x)));
            Assert.False(schemesInstallerShouldNotSee.Any(x => actual.Contains(x)));
        }

        [Then(@"I should not see the restricted schemes the client has access to which I do not have access to")]
        public void ThenIShouldNotSeeTheRestrictedSchemesTheClientHasAccessToWhichIDoNotHaveAccessTo()
        {
            var installerUsername = ScenarioContext.Current.Get<string>("usercreated");
            var userId = ScenarioContext.Current.Get<int>(key_clientUserId);
            var userManageResult = ScenarioContext.Current.Get<ViewResult>(key_userManageResult).Model as ManageUserViewModel;

            var actual = userManageResult.AdditUserViewModel.Schemes.ToList();

            var clientsRestrictedSchemes = new List<string>();
            foreach (DataRow row in new DbHelper().RunQuery("select s.SchemeName from UserProfileSchemeDetail u inner join SchemeDetail s on u.SchemeDetailId = s.SchemeDetailId where s.IsRestricted = 1 and u.UserId = " + userId).Tables[0].Rows)
                clientsRestrictedSchemes.Add((string)row["SchemeName"]);
            var installersRestrictedSchemes = new List<string>();
            foreach (DataRow row in new DbHelper().RunQuery("select s.SchemeName from UserProfileSchemeDetail u inner join SchemeDetail s on u.SchemeDetailId = s.SchemeDetailId where s.IsRestricted = 1 and u.UserId = (select UserId from UserProfile where UserName = '" + installerUsername + "')").Tables[0].Rows)
                installersRestrictedSchemes.Add((string)row["SchemeName"]);

            var shouldBeHiddenToInstaller = clientsRestrictedSchemes.Except(installersRestrictedSchemes);

            Assert.False(actual.Any(x => shouldBeHiddenToInstaller.Contains(x)));
        }

        [Then(@"The restricted schemes I do not have access to should remain accessible to the client")]
        public void ThenTheRestrictedSchemesIDoNotHaveAccessToShouldRemainAccessibleToTheClient()
        {
            var userId = ScenarioContext.Current.Get<int>(key_clientUserId);
            var restrictedSchemes = ScenarioContext.Current.Get<Dictionary<string, int>>(key_restrictedSchemes);
            var removedSchemes = ScenarioContext.Current.Get<List<string>>(key_removedSchemeNames);

            var actual = new List<string>();
            foreach (DataRow row in new DbHelper().RunQuery("select s.SchemeName from UserProfileSchemeDetail u inner join SchemeDetail s on u.SchemeDetailId = s.SchemeDetailId where s.IsRestricted = 1 and u.UserId = " + userId).Tables[0].Rows)
                actual.Add((string)row["SchemeName"]);

            var expected = restrictedSchemes.Keys.Except(removedSchemes).ToList();

            Assert.That(actual, Is.EquivalentTo(expected));
        }

        [Then(@"The client should not have access to the removed schemes")]
        public void ThenTheClientShouldNotHaveAccessToTheRemovedSchemes()
        {
            var userId = ScenarioContext.Current.Get<int>(key_clientUserId);
            var removedSchemes = ScenarioContext.Current.Get<List<string>>(key_removedSchemeNames);

            var actual = new List<string>();
            foreach (DataRow row in new DbHelper().RunQuery("select s.SchemeName from UserProfileSchemeDetail u inner join SchemeDetail s on u.SchemeDetailId = s.SchemeDetailId where u.UserId = " + userId).Tables[0].Rows)
                actual.Add((string)row["SchemeName"]);

            Assert.False(removedSchemes.Any(x => actual.Contains(x)));
        }

        [AfterScenario("installerUserAction")]
        public static void TearDownInstallerUserActions()
        {
            var userId = ScenarioContext.Current.Get<int>(key_clientUserId);

            var username = (string)new DbHelper().RunQuery("select UserName from userprofile where userid = " + userId.ToString()).Tables[0].Rows[0]["UserName"];

            UserSetupSteps.TearDownUser(username);
        }
    }
}
