﻿namespace Fusion.WebApp.Specs.Tests
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Data;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;

    [Binding]
    public class AssetBreakdownSteps
    {
        private string analysisPlanParameters = "[\"liabilities\",\"assets\",\"recoveryPlan\",\"investmentStrategy\",\"fro\",\"etv\",\"pie\",\"futureBenefits\",\"abf\",\"insurance\"]";

        [Then(@"I see the assets breakdown chart #(.*)")]
        public void ThenISeeTheAssetsBreakdownChart(string tag)
        {
            var categories = GetAssetBreakdownFromTag(tag);

            Assert.NotNull(categories);
            Assert.Greater(categories.Count(), 0);

            foreach (var category in categories)
            {
                Assert.IsNotNullOrEmpty(category.Label);

                var classes = JsonReader.Get<IEnumerable<BreakdownItem>>(category, "SubItems");

                Assert.NotNull(classes);
                Assert.Greater(classes.Count(), 0);

                foreach (var cls in classes)
                {
                    Assert.IsNotNullOrEmpty(cls.Label);

                    var funds = JsonReader.Get<IEnumerable<BreakdownItem>>(cls, "SubItems");

                    Assert.NotNull(funds);
                    Assert.Greater(funds.Count(), 0);

                    foreach (var fund in funds)
                    {
                        Assert.IsNotNullOrEmpty(fund.Label);
                        Assert.GreaterOrEqual(fund.Value, 0);
                    }
                }
            }
        }

        [Then(@"Synthetic growth is the correct percentage of the total assets for '(.*)'")]
        public void ThenTheSyntheticGrowthValueIsTheCorrectPercentageOfTheTotalAssetsFor(string page)
        {
            var syntheticGrowth = GetSyntheticGrowthMonetaryValue(page);
            var totalAssets = GetTotalAssets(page);
            var syntheticGrowthPercentage = GetSyntheticGrowthPercentage(page);

            Assert.IsTrue(totalAssets * syntheticGrowthPercentage == syntheticGrowth);

        }

        private double GetTotalAssets(string page)
        {
            return GetAssetBreakdownFromTag(page).Sum(ab => ab.Value);
        }

        private double GetSyntheticGrowthMonetaryValue(string page)
        {
            return getSyntheticGrowthForPage(page);
        }

        private double GetSyntheticGrowthPercentage(string page)
        {
            double syntheticGrowthPercentage = 0.0;

            switch (page)
            {
                case "home":
                case "analysis":
                case "analysis-investmentStrategy":
                case "evolution":
                    var lastEffectiveDatedata = new DbHelper().RunQuery(string.Format(@"select top 1 sa.PropSyntheticEquity + sa.PropSyntheticCredit
                                                     from SchemeAssets sa
                                                     inner join PensionSchemes ps on ps.Id = sa.PensionSchemeId
													 inner join SchemeDetail sd on sd.SchemeData_Id = ps.Id
                                                     where sd.SchemeName = '{0}'
                                                     order by EffectiveDate desc ", ScenarioContext.Current.GetSchemeName()));// last effective date

                    syntheticGrowthPercentage = (double)lastEffectiveDatedata.Tables[0].Rows[0][0];
                    break;
                case "baseline":
                    var firstEffectiveDatedata = new DbHelper().RunQuery(string.Format(@"select top 1 sa.PropSyntheticEquity + sa.PropSyntheticCredit
                                                     from SchemeAssets sa
                                                     inner join PensionSchemes ps on ps.Id = sa.PensionSchemeId
													 inner join SchemeDetail sd on sd.SchemeData_Id = ps.Id
                                                     where sd.SchemeName = '{0}'
                                                     order by EffectiveDate asc ", ScenarioContext.Current.GetSchemeName()));// first effective date

                    syntheticGrowthPercentage = (double)firstEffectiveDatedata.Tables[0].Rows[0][0];
                    break;
            }

            return syntheticGrowthPercentage;
        }

        public IEnumerable<BreakdownItem> GetAssetBreakdownFromContext(ScenarioContext scenarioContext)
        {
            return getAssetBreakdown(scenarioContext.GetCurrentPage(), scenarioContext.GetCurrentPageSection());
        }

        private IEnumerable<BreakdownItem> getAssetBreakdown(PageType page, PageSectionType section = PageSectionType.None)
        {
            IEnumerable<BreakdownItem> categories = null;

            switch (page)
            {
                case PageType.Baseline:
                    categories = JsonReader.Get<IEnumerable<BreakdownItem>>(
                        JsonReader.Get<object>((
                            ControllerFactory.Get<BaselineController>(ScenarioContext.Current).Get() as JsonResult)
                                .Data,
                                "current"),
                                    "assetsBreakdown");
                    break;
                case PageType.Evolution:
                    categories = JsonReader.Get<IEnumerable<BreakdownItem>>((
                        ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult)
                            .Data,
                            "assetsBreakdown");
                    break;
                case PageType.Home:
                    categories = JsonReader.Get<IEnumerable<BreakdownItem>>((
                        ControllerFactory.Get<HomeController>(ScenarioContext.Current).Get() as JsonResult)
                            .Data,
                            "assetsBreakdown");
                    break;
                case PageType.Analysis:
                    if (section == PageSectionType.InvestmentStrategyAssumptions)
                        categories = JsonReader.Get<IEnumerable<BreakdownItem>>(
                            JsonReader.Get<object>((
                                ControllerFactory.Get<JsonController>(ScenarioContext.Current).AnalysisPlan(analysisPlanParameters) as JsonResult)
                                    .Data,
                                    "investmentStrategy"),
                                        "assetsBreakdown");
                    else
                        categories = JsonReader.Get<IEnumerable<BreakdownItem>>((
                            ControllerFactory.Get<JsonController>(ScenarioContext.Current).Assets() as JsonResult)
                                .Data,
                                "assetsBreakdown");
                    break;
            }

            return categories;
        }

        public IEnumerable<BreakdownItem> GetAssetBreakdownFromTag(string tag)
        {
            var page = PageType.Home;
            var section = PageSectionType.None;

            switch (tag)
            {
                case "baseline":
                    page = PageType.Baseline;
                    break;
                case "evolution":
                    page = PageType.Evolution;
                    break;
                case "home":
                    page = PageType.Home;
                    break;
                case "analysis":
                    page = PageType.Analysis;
                    section = PageSectionType.AssetAssumptions;
                    break;
                case "analysis-investmentStrategy":
                    page = PageType.Analysis;
                    section = PageSectionType.InvestmentStrategyAssumptions;
                    break;
            }

            return getAssetBreakdown(page, section);
        }

        private double getSyntheticGrowthForPage(string page)
        {
            object data = null;

            switch (page)
            {
                case "baseline":
                    data = JsonReader.Get<object>((ControllerFactory.Get<BaselineController>(ScenarioContext.Current).Get() as JsonResult).Data, "current");
                    break;
                case "evolution":
                    data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;
                    break;
                case "home":
                    data = (ControllerFactory.Get<HomeController>(ScenarioContext.Current).Get() as JsonResult).Data;
                    break;
                case "analysis":
                    data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).Assets() as JsonResult).Data;
                    break;
                case "analysis-investmentStrategy":
                    data = JsonReader.Get<object>((ControllerFactory.Get<JsonController>(ScenarioContext.Current).AnalysisPlan(analysisPlanParameters) as JsonResult).Data, "investmentStrategy");
                    break;
            }

            var syntheticAssetInfo = (dynamic)JsonReader.Get<object>(data, "syntheticAssetInfo");
            var syntheticGrowth = syntheticAssetInfo.TotalAmount;

            return syntheticGrowth;
        }

        [Then(@"I see the following asset donut categories")]
        public void ThenISeeTheFollowingAssetDonutCategories(Table table)
        {
            var assetsBreakdown = GetAssetBreakdownFromContext(ScenarioContext.Current);

            foreach (var h in table.Header)
            {
                var category = (AssetCategory)Enum.Parse(typeof(AssetCategory), h);
                var donutItem = assetsBreakdown.SingleOrDefault(x => x.Label == category.ToString());

                Assert.IsNotNull(donutItem);
                Assert.Greater(donutItem.Value, 0);
            }

            foreach (var a in assetsBreakdown)
            {
                Assert.True(table.Header.Contains(a.Label) || a.Value == 0, a.Label);
            }
        }
    }
}