﻿namespace Fusion.WebApp.Specs.Tests
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Data;
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class SchemeSteps
    {
        private static Dictionary<string, IScheme> cachedSchemes = new Dictionary<string, IScheme>();

        [Given(@"My default scheme is (.*)")]
        public void MyDefaultSchemeIs(string schemeName)
        {
            var data = new DbHelper().RunQuery("select * from SchemeDetail where SchemeName = '" + schemeName + "'");
            var schemeId = (int)data.Tables[0].Rows[0][0];
            var httpContext = ScenarioContext.Current.Get<MockHttpContext>("httpcontext");
            var user = httpContext.Session["UserProfile"] as UserProfile;
            user.ActiveSchemeDetailId = schemeId;
            httpContext.Session["UserProfile"] = user;
            new DbHelper().RunCommand(string.Format("update UserProfile set ActiveSchemeDetailId = {0} where UserName = '{1}'", schemeId.ToString(), user.UserName));

            if (cachedSchemes.ContainsKey(schemeName))
            {
                httpContext.Session["Scheme"] = cachedSchemes[schemeName];

                var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

                controller.ResetAllAssumptions();
            }
            else
            {
                // Force initialisation of the default scheme
                ControllerFactory.Get<HomeController>(ScenarioContext.Current).Get();

                cachedSchemes.Add(schemeName, (IScheme)httpContext.Session["Scheme"]);
            }

            ScenarioContext.Current.SetSchemeName(schemeName);
        }

        [Given(@"I have imported the (.*)")]
        public void GivenIHaveImportedTheTestScheme_OneAssetTypeWABF(string schemeName)
        {
            byte[] schemeXmlContent;

            using (var schemeXml = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "scheme_files", schemeName + ".xml"), FileMode.Open))
            {
                schemeXmlContent = new byte[schemeXml.Length];
                schemeXml.Read(schemeXmlContent, 0, Convert.ToInt32(schemeXml.Length));
            }

            var model = new AdditSchemeViewModel
            {
                IsEditScheme = false,
                CurrentSchemeName = schemeName,
                NewSchemeName = schemeName,
                SchemeData = new MockHttpPostedFile(schemeXmlContent, "text/xml", schemeName + ".xml")
            };

            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            (controller.ControllerContext.HttpContext.Request as MockHttpRequest).AddRequestItem("update", "true");

            var result = controller.Edit(model) as ViewResult;
            
            if (!ScenarioContext.Current.ContainsKey("schemesImported"))
                ScenarioContext.Current.Add("schemesImported", new List<Tuple<int,string>>());
            ScenarioContext.Current["schemesImported"] = ScenarioContext.Current.Get<List<Tuple<int, string>>>("schemesImported").Union(new List<Tuple<int, string>> { new Tuple<int,string> (model.Id, schemeName) }).ToList();
        }

        [AfterScenario("schemeImported")]
        public static void TearDownSchemeImported()
        {
            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);
            foreach (var scheme in ScenarioContext.Current.Get<List<Tuple<int, string>>>("schemesImported"))
            {
                controller.Edit(
                    new AdditSchemeViewModel
                    {
                        Id = scheme.Item1,
                        CurrentSchemeName = scheme.Item2
                    });
            }
        }
    }
}
