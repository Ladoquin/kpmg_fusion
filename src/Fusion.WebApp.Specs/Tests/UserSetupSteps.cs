﻿namespace Fusion.WebApp.Specs.Tests
{
    using FlightDeck.DomainShared;
    using Fusion.Shared.Data;
    using Fusion.Shared.Mocks;
    using System.Security.Principal;
    using System.Web.Security;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class UserSetupSteps
    {
        [Given(@"I am an Admin user")]
        public void GivenIAmAnAdminUser()
        {
            SetupUserContext(RoleType.Admin, "fusionapptestuseradmin");
        }

        [Given(@"I am an Installer user")]
        public void GivenIAmAnInstallerUser()
        {
            SetupUserContext(RoleType.Installer, "fusionapptestuserinstaller");
        }

        [Given(@"I am a Client user")]
        public void GivenIAmAClientUser()
        {
            SetupUserContext(RoleType.Client, "fusionapptestuserclient");
        }

        [Given(@"I am the Developer user")]
        public void GivenIAmTheDeveloperUser()
        {
            SetupUserContext(RoleType.Admin, Fusion.WebApp.App_Start.UserDbInitializer.DeveloperUser, false);
        }

        [AfterScenario]
        public void AfterScenario()
        {
            if (ScenarioContext.Current.ContainsKey("usercreated"))
            {
                var username = ScenarioContext.Current.Get<string>("usercreated");
                UserSetupSteps.TearDownUser(username);
            }
        }

        private void SetupUserContext(RoleType role, string username, bool create = true)
        {
            var userid = 0;
            if (create)
            {
                TearDownUser(username); //for reason not yet discovered, TearDownUser not always occurring after a failed test.
                userid = new MembershipHelper().CreateUser(username);
                Roles.AddUserToRole(username, role.ToString());
            }
            else
            {
                userid = new MembershipHelper().GetUserId(username);
            }

            var user = new GenericPrincipal(new GenericIdentity(username), new[] { role.ToString() });
            var userProfile = new UserProfile(userid, username, "fusionapp", "test", username + "@space01.co.uk", true, false, null, null);
            var httpContext = new MockHttpContext { User = user };

            httpContext.Session["UserProfile"] = userProfile;

            ScenarioContext.Current.Set(httpContext, "httpcontext");

            if (create)
            {
                ScenarioContext.Current.Set(username, "usercreated");
            }
        }

        public static void TearDownUser(string username)
        {
            new DbHelper().RunCommand(string.Format("if exists (select * from UserProfile where UserName = '{0}') delete UserProfileSchemeDetail where UserId = (select UserId from UserProfile where Username = '{0}')", username));
            new DbHelper().RunCommand(string.Format("if exists (select * from UserProfile where UserName = '{0}') delete UserProfilePasswordHistory where UserId = (select UserId from UserProfile where Username = '{0}')", username));
            new DbHelper().RunCommand(string.Format("if exists (select * from UserProfile where UserName = '{0}') delete UserProfilePassword where UserId = (select UserId from UserProfile where Username = '{0}')", username));
            new DbHelper().RunCommand(string.Format("if exists (select * from UserProfile where UserName = '{0}') delete UserProfileFavouriteSchemes where UserId = (select UserId from UserProfile where Username = '{0}')", username));
            new DbHelper().RunCommand(string.Format("if exists (select * from UserProfile where UserName = '{0}') delete webpages_UsersInRoles where UserId = (select UserId from UserProfile where Username = '{0}')", username));
            new DbHelper().RunCommand(string.Format("if exists (select * from UserProfile where UserName = '{0}') delete UserProfile where Username = '{0}'", username));
        }
    }
}
