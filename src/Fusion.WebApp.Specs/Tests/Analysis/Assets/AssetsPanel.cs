﻿namespace Fusion.WebApp.Specs.Tests.Analysis.Assets
{
    using FlightDeck.DomainShared;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy;
    using NUnit.Framework;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class AssetsPanel
    {
        [Then(@"I see hedged cashflow information on the asset assumptions tab")]
        public void ThenISeeHedgedCashflowInformationOnTheAssetAssumptionsTab()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var data = (controller.Assets() as JsonResult).Data;

            var ldiData = JsonReader.Get<object>(data, "ldiData");
            Assert.NotNull(ldiData);

            if (ScenarioContext.Current.ContainsKey(InvestmentStrategySharedSteps.KeyHedgingInterest))
            {
                //if testing that the client values are displayed perform a more stringent check
                Assert.That(JsonReader.Get<double>(ldiData, "cashflowInterestRate"), Is.EqualTo(ScenarioContext.Current[InvestmentStrategySharedSteps.KeyHedgingInterest]).Within(Utils.TestPrecision));
                Assert.That(JsonReader.Get<double>(ldiData, "cashflowInflation"), Is.EqualTo(ScenarioContext.Current[InvestmentStrategySharedSteps.KeyHedgingInflation]).Within(Utils.TestPrecision));
            }
            else
            {
                Assert.Greater(JsonReader.Get<double>(ldiData, "cashflowInterestRate"), 0);
                Assert.Greater(JsonReader.Get<double>(ldiData, "cashflowInflation"), 0);
            }
        }
    }
}
