﻿namespace Fusion.WebApp.Specs.Tests.Analysis.Liabilities
{
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class PensionIncreasesSteps
    {
        [Then(@"I see the pension increases at the analysis date")]
        public void ThenISeeThePensionIncreasesAtTheAnalysisDate()
        {
            var json = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).Liabilities() as JsonResult).Data;

            var pensionIncreases = JsonReader.Get<IEnumerable<object>>(json, "pensionIncreases");

            Assert.NotNull(pensionIncreases);
            Assert.Greater(pensionIncreases.Count(), 0);
            foreach (var pinc in pensionIncreases)
            {
                Assert.Greater(JsonReader.Get<int>(pinc, "Key"), 0);
                Assert.NotNull(JsonReader.Get<object>(pinc, "Value"));
                Assert.NotNull(JsonReader.Get<bool>(pinc, "IsVisible"));
            }
        }

        [Then(@"I do not see the hidden pension increases at the analysis date")]
        public void ThenIDoNotSeeTheHiddenPensionIncreasesAtTheAnalysisDate()
        {
            var json = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).Liabilities() as JsonResult).Data;

            var pensionIncreases = JsonReader.Get<IEnumerable<object>>(json, "pensionIncreases");

            Assert.Greater(pensionIncreases.Where(x => JsonReader.Get<bool>(x, "IsVisible") == false).Count(), 0);
        }
    }
}
