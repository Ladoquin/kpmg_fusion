﻿using Fusion.Shared.Data;
using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Analysis.Liabilities
{
    [Binding]
    public class UnderlyingBondYieldSteps
    {
        private string analysisPlanParameters = "[\"liabilities\"]";

        [Then(@"The gilt yield index '(.*)' value is as expected")]
        public void ThenTheGiltYieldIndexValueIsAsExpected(string indexName)
        {
            var analysisPlan = ControllerFactory.Get<JsonController>(ScenarioContext.Current).AnalysisPlan(analysisPlanParameters) as JsonResult;
            var liabilities = JsonReader.Get<object>(analysisPlan.Data, "liabilities");
            var basisBondYield = JsonReader.Get<object>(liabilities, "basisBondYield");
            var value = JsonReader.Get<object>(basisBondYield, "value");

            var attributionEnd = ScenarioContext.Current.GetAttributionEnd().ToString("yyyy-MM-dd");

            var ds = new DbHelper().RunQuery(@"select value / 100
                                                    from IndexValues iv
                                                    inner join FinancialIndices fi on iv.FinancialIndexId = fi.Id
                                                    where fi.Name = '" + indexName + @"'
                                                    and iv.Date = '" + attributionEnd + "'");

            var dbVlaue = (double)ds.Tables[0].Rows[0][0];

            Assert.IsTrue(dbVlaue == (double)value, "Expected LAPanelBondYield not correct at given date");
        }
    }
}
