﻿namespace Fusion.WebApp.Specs.Tests.Analysis.Liabilities
{
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class LiabilitiesPanelSteps
    {
        private double tolerance = 0.0000005;
        private const string KeyCurrentPenIncs = "KeyCurrentPenIncs";
        private const string KeySavedPenIncs = "KeySavedPenIncs";
        private const string KeyInflationChange = "KeyInflationChange";
        private const string KeyInflationTypeChange = "KeyInflationTypeChange";

        /// <summary>
        /// Liability assumptions as on 31/03/2014 for the Space01 Test Scheme Accounting basis
        /// </summary>
        private Dictionary<string, double> expectedDefaultLiabilityAssumptions = new Dictionary<string, double>
            {
                { "preRetirement", 0.047734 },
                { "postRetirement", 0.047734 },
                { "pensioner", 0.047734 },
                { "salaryIncreases", 0.0288679495301772 },
                { "RPI", 0.0308679495301772 },
                { "CPI", 0.0208679495301772 },
                { "assumptionLife65", 21.5 },
                { "assumptionLife45", 22.7 }
            };

        /// <summary>
        /// Pension Increase assumptions as on 31/03/2014 for the Space01 Test Scheme Accounting basis
        /// </summary>
        private Dictionary<int, double> expectedDefaultPensionIncreases = new Dictionary<int, double>
            {
                { 1, 0.0214435310066539 },
                { 2, 0.0309906150567998 },
                { 3, 0.0311154807001186 },
                { 4, 0.0211716994572031 },
                { 5, 0.0210162029253213 },
                { 6, 0 }
            };

        [When(@"I change a liability inflation assumption")]
        public void WhenIChangeALiabilityInflationAssumption()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var liabilityAssumptions = new Dictionary<string, double>(expectedDefaultLiabilityAssumptions);

            var inflationChange = 0.01;
            var inflationTypeChange = "RPI";

            liabilityAssumptions[inflationTypeChange] += inflationChange;

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(liabilityAssumptions);

            controller.SetLiabilityAssumptions(json);

            ScenarioContext.Current.Add(KeyInflationChange, inflationChange);
            ScenarioContext.Current.Add(KeyInflationTypeChange, inflationTypeChange);
        }

        [When(@"I change pension increase assumptions")]
        public void WhenIChangePensionIncreaseAssumptions()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var pensionIncreaseAssumptions = ScenarioContext.Current.ContainsKey(KeyCurrentPenIncs)
                ? ScenarioContext.Current.Get<Dictionary<int, double>>(KeyCurrentPenIncs)
                : new Dictionary<int, double>(expectedDefaultPensionIncreases);

            pensionIncreaseAssumptions[1] += 0.01;

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(pensionIncreaseAssumptions);

            controller.SetPensionIncreaseAssumptions(json);

            ScenarioContext.Current[KeyCurrentPenIncs] = pensionIncreaseAssumptions;
        }

        [When(@"I unlock pension increase assumptions")]
        public void WhenIUnlockPensionIncreaseAssumptions()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            controller.SetPensionIncreasesUnlocked();

            var unlockedPenIncs = getPensionIncreasesFromJson(controller.Liabilities() as JsonResult);

            ScenarioContext.Current.Add(KeySavedPenIncs, unlockedPenIncs);
            ScenarioContext.Current.Add(KeyCurrentPenIncs, unlockedPenIncs);
        }

        [When(@"I lock pension increase assumptions")]
        public void WhenILockPensionIncreaseAssumptions()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            controller.SetPensionIncreasesLocked();
        }

        [Then(@"pension increase assumptions are updated")]
        public void ThenPensionIncreaseAssumptionsAreUpdated()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var result = controller.Liabilities() as JsonResult;

            Assert.NotNull(result);

            var pensionIncreases = getPensionIncreasesFromJson(result);

            var expectedKeysChanges = ScenarioContext.Current.Get<string>(KeyInflationTypeChange) == "RPI"
                ? new List<int> { 2, 3, 4 }
                : new List<int> { 1, 5 };

            var actualKeys = new List<int>();

            foreach (var increase in pensionIncreases)
            {
                if (expectedKeysChanges.Contains(increase.Key))
                {
                    Assert.That(expectedDefaultPensionIncreases[increase.Key], Is.Not.EqualTo(increase.Value).Within(tolerance));
                }
                else
                {
                    Assert.That(expectedDefaultPensionIncreases[increase.Key], Is.EqualTo(increase.Value).Within(tolerance));
                }

                actualKeys.Add(increase.Key);
            }

            Assert.That(expectedDefaultPensionIncreases.Keys, Is.EquivalentTo(actualKeys));
        }

        [Then(@"pension increase assumptions equal those at time of unlocking")]
        public void ThenPensionIncreaseAssumptionsBeforeAndAfterValuesAreEqualToTheValueAtTimeOfUnlocking()
        {
            var expected = ScenarioContext.Current.Get<Dictionary<int, double>>(KeySavedPenIncs);

            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var actual = getPensionIncreasesFromJson(controller.Liabilities() as JsonResult);

            Assert.AreEqual(expected.Count, actual.Count);
            foreach (var exp in expected)
                Assert.That(exp.Value, Is.EqualTo(actual[exp.Key]).Within(tolerance));
        }

        [Then(@"pension increase assumptions are unaffected")]
        public void ThenPensionIncreaseAssumptionsAreUnaffected()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var result = controller.Liabilities() as JsonResult;

            Assert.NotNull(result);

            var expected = ScenarioContext.Current.Get<Dictionary<int, double>>(KeyCurrentPenIncs);

            var actual = getPensionIncreasesFromJson(result);

            Assert.AreEqual(expected.Count, actual.Count);
            foreach (var exp in expected)
                Assert.That(exp.Value, Is.EqualTo(actual[exp.Key]).Within(tolerance));
        }

        private Dictionary<int, double> getPensionIncreasesFromJson(JsonResult json)
        {
            var pensionIncreases = new Dictionary<int, double>();
            var pensionIncreasesData = JsonReader.Get<IEnumerable>(json.Data, "pensionIncreases");

            foreach (var increase in pensionIncreasesData)
            {
                var key = JsonReader.Get<int>(increase, "Key");
                var value = JsonReader.Get<object>(increase, "Value");
                var after = JsonReader.Get<double>(value, "After");

                pensionIncreases.Add(key, after);
            }

            return pensionIncreases;
        }
    }
}