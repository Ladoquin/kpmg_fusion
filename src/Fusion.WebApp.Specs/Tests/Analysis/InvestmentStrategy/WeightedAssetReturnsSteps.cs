﻿using FlightDeck.DomainShared;
using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy
{
    [Binding]
    public class WeightedAssetReturnsSteps
    {
        [Then(@"I see the correct weighted asset return values on the Investment Strategy panel (.*)")]
        public void ThenISeeTheCorrectWeightedAssetReturnValuesOnTheInvestmentStrategyPanel(string tag)
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var inv = controller.InvestmentStrategy() as JsonResult;

            var weightedReturnValues = JsonReader.Get<WeightedReturnParameters>(inv.Data, "weightedReturnParameters");

            Assert.NotNull(weightedReturnValues);

            if (string.Equals("#before", tag))
            {
                Assert.AreEqual(weightedReturnValues.CurrentWarRelativeToGilts, weightedReturnValues.WeightedAssetReturn);
                Assert.AreEqual(0, weightedReturnValues.ChangeInExpectedReturn);
            }
            else if (string.Equals("#after", tag))
            {
                Assert.AreNotEqual(weightedReturnValues.CurrentWarRelativeToGilts, weightedReturnValues.WeightedAssetReturn);
                Assert.AreNotEqual(0, weightedReturnValues.ChangeInExpectedReturn);
            }
        }
    }
}
