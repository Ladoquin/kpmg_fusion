﻿namespace Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy
{
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class SyntheticGrowthSteps
    {
        const string KeyClientSyntheticTotal = "clientSyntheticTotal";

        [Then(@"I see that synthetic growth components are present in the investment strategy options")]
        public void ThenISeeThatSyntheticGrowthComponentsArePresentInTheInvestmentStrategyOptions()
        {
            var allInvestmentStrategyData = ScenarioContext.Current.Get<dynamic>("result");

            var syntheticEquity = JsonReader.Get<object>(allInvestmentStrategyData, "syntheticEquity");
            var syntheticCredit = JsonReader.Get<object>(allInvestmentStrategyData, "syntheticCredit");

            Assert.IsNotNull(syntheticEquity);
            Assert.IsNotNull(syntheticCredit);
        }

        [When(@"I amend the synthetic equity and synthetic credit sliders")]
        public void WhenIAmendTheSyntheticEquityAndSyntheticCreditSliders()
        {
            ScenarioContext.Current.Add(InvestmentStrategySharedSteps.KeySyntheticEquity, 0.2);
            ScenarioContext.Current.Add(InvestmentStrategySharedSteps.KeySyntheticCredit, 0.3);

            InvestmentStrategySharedSteps.PostInvestmentStrategyAdvancedOptions();

            ScenarioContext.Current.Add(KeyClientSyntheticTotal, 0.5);
        }

        [Then(@"I see a corresponding synthetic component on the assets breakdown chart on the investment strategy panel")]
        public void ThenISeeACorrespondingSyntheticComponentOnTheAssetsBreakdownChartOnTheInvestmentStrategyPanel()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy() as JsonResult).Data;

            var expected = ScenarioContext.Current.Get<double>(KeyClientSyntheticTotal);
            var actual = JsonReader.Get<double>(data, "syntheticEquity") + JsonReader.Get<double>(data, "syntheticCredit");

            Assert.AreEqual(expected, actual);
        }

        [Then(@"I see a corresponding synthetic component on the assets breakdown chart on the assets panel")]
        public void ThenISeeACorrespondingSyntheticComponentOnTheAssetsBreakdownChartOnTheAssetsPanel()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).Assets() as JsonResult).Data;

            var totalAssets = JsonReader.Get<IEnumerable<BreakdownItem>>(data, "assetsBreakdown").Sum(ab => ab.Value);

            var syntheticTotalPercentage = ScenarioContext.Current.Get<double>(KeyClientSyntheticTotal);
            var actual = JsonReader.Get<double>(JsonReader.Get<object>(data, "syntheticAssetInfo"), "TotalAmount");

            Assert.AreEqual(syntheticTotalPercentage * totalAssets, actual);
        }

    }
}
