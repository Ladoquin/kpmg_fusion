﻿using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy
{
    [Binding]
    public class EnableLdiSteps
    {
        [When(@"I (.*) LDI")]
        public void WhenISetLDIState(string state)
        {
            ScenarioContext.Current[InvestmentStrategySharedSteps.KeyLDIState] = string.Equals("enable", state, StringComparison.CurrentCultureIgnoreCase);

            ScenarioContext.Current["result"] = InvestmentStrategySharedSteps.PostInvestmentStrategyAdvancedOptions();
        }

        [When(@"I set Hedging values")]
        public void WhenISetHedgingValuess()
        {
            ScenarioContext.Current[InvestmentStrategySharedSteps.KeyHedgingInterest] = 0.8;
            ScenarioContext.Current[InvestmentStrategySharedSteps.KeyHedgingInflation] = 0.9;

            ScenarioContext.Current["result"] = InvestmentStrategySharedSteps.PostInvestmentStrategyAdvancedOptions();
        }

        [Then(@"I see the that the default for LDI is enabled")]
        public void ThenISeeTheThatTheDefaultForLDIIsEnabled()
        {
            var json = ScenarioContext.Current.Get<dynamic>("result");

            var ldiEnabled = JsonReader.Get<bool>(json, "ldiEnabled");

            Assert.IsTrue(ldiEnabled);
        }

        [Then(@"I see that the Hedging panel is (.*)")]
        public void ThenISeeThatTheHedgingPanelIsInWhichState(string state)
        {
            var json = ScenarioContext.Current.Get<dynamic>("result");

            var hedgingInputsEnabled = JsonReader.Get<bool>(json, "hedgingInputsEnabled");

            Assert.AreEqual(string.Equals("enabled", state, StringComparison.CurrentCultureIgnoreCase), hedgingInputsEnabled);
        }

        [Then(@"I observe that LDI remains disabled")]
        public void ThenIObserveThatLDIRemainsDisabled()
        {
            var json = ScenarioContext.Current.Get<dynamic>("result");

            var ldiEnabled = JsonReader.Get<bool>(json, "ldiEnabled");

            Assert.IsFalse(ldiEnabled);
        }

        [Then(@"I see that the Hedging default values are set")]
        public void ThenISeeThatTheHedgingDefaultValuesAreSet()
        {
            var json = ScenarioContext.Current.Get<dynamic>("result");

            var hedgingInterest = JsonReader.Get<double>(json, "hedgingInterest");
            var hedgingInflation = JsonReader.Get<double>(json, "hedgingInflation");

            var expected = SchemeQueries.GetSchemeAssetDefinition(ScenarioContext.Current.GetSchemeName());

            Assert.AreEqual(expected.InterestHedge, hedgingInterest, "Hedging Interest");
            Assert.AreEqual(expected.InflationHedge, hedgingInflation, "Hedging Inflation");
        }

        [Then(@"I observe that the Hedging values remain")]
        public void ThenIObserveThatTheHedgingValuesRemain()
        {
            var json = ScenarioContext.Current.Get<dynamic>("result");

            var hedgingInterest = JsonReader.Get<double>(json, "hedgingInterest");
            var hedgingInflation = JsonReader.Get<double>(json, "hedgingInflation");

            Assert.AreEqual(ScenarioContext.Current.Get<double>(InvestmentStrategySharedSteps.KeyHedgingInterest), hedgingInterest, "Hedging Interest");
            Assert.AreEqual(ScenarioContext.Current.Get<double>(InvestmentStrategySharedSteps.KeyHedgingInflation), hedgingInflation, "Hedging Inflation");
        }

        [Then(@"I observe that the default hedging values have changed")]
        public void ThenIObserveThatTheDefaultHedgingValuesHaveChanged()
        {
            var json = ScenarioContext.Current.Get<dynamic>("result");

            var hedgingInterest = JsonReader.Get<double>(json, "hedgingInterest");
            var hedgingInflation = JsonReader.Get<double>(json, "hedgingInflation");

            Assert.AreNotEqual(ScenarioContext.Current.Get<double>(InvestmentStrategySharedSteps.KeyHedgingInterest), hedgingInterest, "Hedging Interest");
            Assert.AreNotEqual(ScenarioContext.Current.Get<double>(InvestmentStrategySharedSteps.KeyHedgingInflation), hedgingInflation, "Hedging Inflation");
        }

        [Then(@"I observe that the default hedging values are zero")]
        public void ThenIObserveThatTheDefaultHedgingValuesAreZero()
        {
            var json = ScenarioContext.Current.Get<dynamic>("result");

            var hedgingInterest = JsonReader.Get<double>(json, "hedgingInterest");
            var hedgingInflation = JsonReader.Get<double>(json, "hedgingInflation");

            Assert.AreEqual(.0, hedgingInterest, "Hedging Interest");
            Assert.AreEqual(.0, hedgingInflation, "Hedging Inflation");
        }

        [When(@"I reset the Investment Strategy panel")]
        public void WhenIResetTheInvestmentStrategyPanel()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var result = controller.ResetInvestmentStrategy();

            ScenarioContext.Current["result"] = (result as JsonResult).Data;
        }
    }
}
