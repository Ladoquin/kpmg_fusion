﻿namespace Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy
{
    using FlightDeck.DomainShared;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;

    [Binding]
    public class InvestmentStrategySharedSteps
    {
        public const string KeyLDIState = "ldiStateValue";
        public const string KeyHedgingInterest = "hedgingInterestValue";
        public const string KeyHedgingInflation = "hedgingInflationValue";
        public const string KeySyntheticEquity = "syntheticEquity";
        public const string KeySyntheticCredit = "syntheticCredit";
        public const string KeyCashInjection = "cashInjection";

        public static string BuildInvestmentStrategyAllocationsJson(IDictionary<AssetClassType, double> allocations)
        {
            var assetMixBuilder = new StringBuilder(500);
            assetMixBuilder.Append("[");
            foreach (var key in allocations.Keys)
            {
                assetMixBuilder.AppendFormat("{{\"id\":{0},\"proportion\":{1}}},", ((int)key).ToString(), allocations[key].ToString());
            }
            assetMixBuilder.Remove(assetMixBuilder.Length - 1, 1);
            assetMixBuilder.Append("]");
            return assetMixBuilder.ToString();
        }

        public static object PostInvestmentStrategyAdvancedOptions()
        {
            var defaultValues = SchemeQueries.GetSchemeAssetDefinition(ScenarioContext.Current.GetSchemeName());

            var syntheticEquity = ScenarioContext.Current.ContainsKey(KeySyntheticEquity) ? ScenarioContext.Current.Get<double>(KeySyntheticEquity) : defaultValues.PropSyntheticEquity;
            var syntheticCredit = ScenarioContext.Current.ContainsKey(KeySyntheticCredit) ? ScenarioContext.Current.Get<double>(KeySyntheticCredit) : defaultValues.PropSyntheticCredit;
            var enableLdi = ScenarioContext.Current.ContainsKey(KeyLDIState) ? ScenarioContext.Current.Get<bool>(KeyLDIState) : defaultValues.LDIInForce;
            var hedgingInterest = ScenarioContext.Current.ContainsKey(KeyHedgingInterest) ? ScenarioContext.Current.Get<double>(KeyHedgingInterest) : defaultValues.InterestHedge;
            var hedgingInflation = ScenarioContext.Current.ContainsKey(KeyHedgingInflation) ? ScenarioContext.Current.Get<double>(KeyHedgingInflation) : defaultValues.InflationHedge;

            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var result = controller.SetInvestmentStrategyAdvancedOptions(syntheticEquity, syntheticCredit, enableLdi, hedgingInterest, hedgingInflation) as ActionResult;

            var json = (result as JsonResult).Data;

            return json;
        }

        public static Dictionary<AssetClassType, double> GetAssetClassProportions()
        {
            var json = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy() as JsonResult).Data;

            var proportions = JsonReader.Get<IEnumerable<object>>(json, "assetsProportions");            

            var proportionsDictionary = proportions.ToDictionary(x => JsonReader.Get<AssetClassType>(x, "Key"), x => JsonReader.Get<double>(x, "Proportion"));

            return proportionsDictionary;
        }

        [Then(@"I should be able to edit the (.*) investment strategy slider")]
        public void ThenIShouldBeAbleToEditTheXInvestmentStrategySlider(string assetClassTypeName)
        {
            var assetMix = new Dictionary<AssetClassType, double>();

            foreach (var assetClassType in Utils.EnumToArray<AssetClassType>())
            {
                if (assetClassType != AssetClassType.Abf)
                {
                    var attr = Utils.GetEnumAttribute<AssetClassType, AssetClassTypeClientVisibleAttribute>(assetClassType);
                    if (attr != null && attr.ClientVisible)
                    {
                        assetMix.Add(assetClassType, 0);
                    }
                }
            }

            assetMix[(AssetClassType)Enum.Parse(typeof(AssetClassType), assetClassTypeName)] = 1.0;

            var json = BuildInvestmentStrategyAllocationsJson(assetMix);

            ControllerFactory.Get<JsonController>(ScenarioContext.Current).SetInvestmentStrategyAssetMix(json);
        }

        [When(@"I set cash injection to £(.*)M")]
        public void WhenISetCashInjectionToM(int p0)
        {
            ControllerFactory.Get<JsonController>(ScenarioContext.Current).SetInvestmentStrategyCashInjection(p0 * 1000000);
        }

        [When(@"I set the investment strategy proportions to")]
        public void WhenISetTheInvestmentStrategyProportionsTo(Table table)
        {
            var assetMix = 
                table.Header.ToDictionary(
                    x => (AssetClassType)Enum.Parse(typeof(AssetClassType), x),
                    x => double.Parse(table.Rows[0][x]) / 100);

            var json = BuildInvestmentStrategyAllocationsJson(assetMix);

            ControllerFactory.Get<JsonController>(ScenarioContext.Current).SetInvestmentStrategyAssetMix(json);
        }
    }
}
