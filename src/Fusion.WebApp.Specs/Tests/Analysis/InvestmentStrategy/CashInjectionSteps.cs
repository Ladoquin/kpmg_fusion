﻿using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using TechTalk.SpecFlow;
using FlightDeck.DomainShared;
using System;

namespace Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy
{
    [Binding]
    public class CashInjectionSteps
    {
        const string KeyTotalAssetAmount = "CashInjectionSteps.KeyTotalAssetAmount";

        [When(@"I note the current total asset value")]
        public void WhenINoteTheCurrentTotalAssetValue()
        {
            var json = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy() as JsonResult).Data;

            ScenarioContext.Current.Set(JsonReader.Get<double>(json, "totalAssets"), KeyTotalAssetAmount);
        }

        [Then(@"the total asset value increases by £(.*)M")]
        public void ThenTheTotalAssetValueIncreasesByM(int p0)
        {
            var previousTotal = ScenarioContext.Current.Get<double>(KeyTotalAssetAmount);

            var json = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy() as JsonResult).Data;

            var currentTotal = JsonReader.Get<double>(json, "totalAssets");

            var expectedTotal = previousTotal + p0 * 1000000;

            Assert.That(currentTotal, Is.EqualTo(expectedTotal).Within(Utils.Precision));
        }

        [Then(@"I see that cash injection is £(.*)M")]
        public void ThenISeeThatCashInjectionIsM(int p0)
        {
            var json = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy() as JsonResult).Data;

            var cashInjection = JsonReader.Get<double>(json, "cashInjection");

            Assert.AreEqual((double)p0 * 1000000, cashInjection);
        }

        [When(@"I note the percentage value of the following asset allocations")]
        public void WhenINoteThePercentageValueOfTheFollowingAssetAllocations(Table table)
        {
            var assetClassProportions = InvestmentStrategySharedSteps.GetAssetClassProportions();

            var noteworthyAssetClassTypes = table.Header.Select(x => (AssetClassType)Enum.Parse(typeof(AssetClassType), x)).ToList();

            var notedAssetClassProportions = assetClassProportions.Where(x => noteworthyAssetClassTypes.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value);

            ScenarioContext.Current.Set(notedAssetClassProportions, "notedAssetClassProportions");
        }

        [Then(@"I see that the percentage value has lowered for the noted asset allocations")]
        public void ThenISeeThatThePercentageValueHasLoweredForTheNotedAssetAllocations()
        {
            var previousAssetClassProportions = ScenarioContext.Current.Get<Dictionary<AssetClassType, double>>("notedAssetClassProportions");

            var currentAssetClassProportions = InvestmentStrategySharedSteps.GetAssetClassProportions()
                .Where(x => previousAssetClassProportions.Keys.Contains(x.Key))
                .ToDictionary(x => x.Key, x => x.Value);

            foreach (var key in previousAssetClassProportions.Keys)
                Assert.Less(currentAssetClassProportions[key], previousAssetClassProportions[key]);
        }
    }
}
