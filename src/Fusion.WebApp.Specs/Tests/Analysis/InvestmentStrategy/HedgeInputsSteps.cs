﻿using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy
{
    [Binding]
    public class HedgeInputsSteps
    {
        const string KeyHedgingInterestLowerLimit = "HedgingInterestLowerLimit";
        const string KeyHedgingInflationLowerLimit = "HedgingInflationLowerLimit";

        [When(@"I note the hedging interest lower limit")]
        public void WhenINoteTheHedgingInterestLowerLimit()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy(false) as JsonResult).Data;

            ScenarioContext.Current.Add(KeyHedgingInterestLowerLimit, JsonReader.Get<double>(data, "hedgingInterestLowerLimit"));
        }
        
        [When(@"I note the hedging inflation lower limit")]
        public void WhenINoteTheHedgingInflationLowerLimit()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy(false) as JsonResult).Data;

            ScenarioContext.Current.Add(KeyHedgingInflationLowerLimit, JsonReader.Get<double>(data, "hedgingInflationLowerLimit"));
        }
        
        [Then(@"the hedging interest lower limit has increased")]
        public void ThenTheHedgingInterestLowerLimitHasIncreased()
        {
            var original = ScenarioContext.Current.Get<double>(KeyHedgingInterestLowerLimit);
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy(false) as JsonResult).Data;
            var current = JsonReader.Get<double>(data, "hedgingInterestLowerLimit");

            Assert.Greater(current, original);
        }
        
        [Then(@"the hedging inflation lower limit has increased")]
        public void ThenTheHedgingInflationLowerLimitHasIncreased()
        {
            var original = ScenarioContext.Current.Get<double>(KeyHedgingInflationLowerLimit);
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy(false) as JsonResult).Data;
            var current = JsonReader.Get<double>(data, "hedgingInflationLowerLimit");

            Assert.Greater(current, original);
        }
    }
}
