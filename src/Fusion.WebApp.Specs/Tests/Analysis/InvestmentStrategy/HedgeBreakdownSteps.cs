﻿namespace Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy
{
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    using newton = Newtonsoft.Json;
    
    [Binding]
    public class HedgeBreakdownSteps
    {
        [Then(@"I see the hedge breakdown table")]
        public void ThenISeeTheHedgeBreakdownTable()
        {
            var json = (ScenarioContext.Current.GetLastActionResult() as JsonResult).Data;

            ScenarioContext.Current.Set<object>(json, "hedging-json");

            var hedgingComparison = (dynamic)newton.JsonConvert.DeserializeObject((json as JsonController.VarDataObject).hedgingBreakdown.ToString());

            var expectedType = Newtonsoft.Json.Linq.JTokenType.Float;

            Assert.True(hedgingComparison.CurrentBasis.Interest.PhysicalAssets.Type == expectedType);
            Assert.True(hedgingComparison.CurrentBasis.Interest.Buyin.Type == expectedType);
            Assert.True(hedgingComparison.CurrentBasis.Interest.LDIOverlay.Type == expectedType);
            Assert.True(hedgingComparison.CurrentBasis.Interest.Total.Type == expectedType);
            Assert.True(hedgingComparison.CurrentBasis.Inflation.PhysicalAssets.Type == expectedType);
            Assert.True(hedgingComparison.CurrentBasis.Inflation.Buyin.Type == expectedType);
            Assert.True(hedgingComparison.CurrentBasis.Inflation.LDIOverlay.Type == expectedType);
            Assert.True(hedgingComparison.CurrentBasis.Inflation.Total.Type == expectedType);
            Assert.True(hedgingComparison.Cashflows.Interest.PhysicalAssets.Type == expectedType);
            Assert.True(hedgingComparison.Cashflows.Interest.Buyin.Type == expectedType);
            Assert.True(hedgingComparison.Cashflows.Interest.LDIOverlay.Type == expectedType);
            Assert.True(hedgingComparison.Cashflows.Interest.Total.Type == expectedType);
            Assert.True(hedgingComparison.Cashflows.Inflation.PhysicalAssets.Type == expectedType);
            Assert.True(hedgingComparison.Cashflows.Inflation.Buyin.Type == expectedType);
            Assert.True(hedgingComparison.Cashflows.Inflation.LDIOverlay.Type == expectedType);
            Assert.True(hedgingComparison.Cashflows.Inflation.Total.Type == expectedType);
        }

        [Then(@"The current basis display name is shown as the current basis column header")]
        public void ThenTheCurrentBasisDisplayNameIsShownAsTheCurrentBasisColumnHeader()
        {
            var json = ScenarioContext.Current.Get<object>("hedging-json");
            Assert.AreEqual(ScenarioContext.Current.GetBasisDisplayName(), JsonReader.Get<string>(json, "basisName"));
        }
    }
}
