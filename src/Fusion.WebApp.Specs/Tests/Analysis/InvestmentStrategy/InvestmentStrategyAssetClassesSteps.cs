﻿using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;

using System.Web.Mvc;
using TechTalk.SpecFlow;
using System.Linq;
using System.Collections.Generic;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using System.Collections;
using Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy;

namespace Fusion.WebApp.Specs.Tests.InvestmentStrategy
{
    [Binding]
    public class InvestmentStrategyAssetClassesSteps
    {
        [When(@"I view the Investment Strategy tab")]
        public void WhenIViewTheInvestmentStrategyTab()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var result = controller.ResetInvestmentStrategy() as ActionResult;

            dynamic json = (result as JsonResult).Data;

            var assetsProportions = JsonReader.Get<IEnumerable>(json, "assetsProportions");

            // if we are calling this more than once in the same test
            // we need to remove from context first before adding again
            ScenarioContext.Current.Remove("assetsProportions");
            ScenarioContext.Current.Remove("result");

            ScenarioContext.Current.Add("assetsProportions", assetsProportions);
            ScenarioContext.Current.Add("result", json);
        }

        [Then(@"I see the that the relevant asset classes are included in the New Investment Strategy")]
        public void ThenISeeTheThatTheRelevantAssetClassesAreIncludedInTheNewInvestmentStrategy()
        {
            var assetsProportions = ScenarioContext.Current.Get<dynamic>("assetsProportions");

            List<AssetClassType> assetClassTypes = new List<AssetClassType>();

            foreach (var proportion in assetsProportions)
            {
                var key = JsonReader.Get<AssetClassType>(proportion, "Key");
                assetClassTypes.Add(key);
            }

            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.DiversifiedGrowth));
            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.Equity));
            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.Property));
            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.DiversifiedCredit));
            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.DiversifiedGrowth));
            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.PrivateMarkets));
            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.CorporateBonds));
            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.FixedInterestGilts));
            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.IndexLinkedGilts));
            Assert.IsTrue(assetClassTypes.Contains(AssetClassType.Cash));
        }

        [When(@"I note down the default hedging values")]
        public void WhenINoteDownTheDefaultHedgingValues()
        {
            var json = ScenarioContext.Current.Get<dynamic>("result");
            if (json != null)
            {
                var hedgingInterest = JsonReader.Get<double>(json, "hedgingInterest");
                var hedgingInflation = JsonReader.Get<double>(json, "hedgingInflation");

                ScenarioContext.Current[InvestmentStrategySharedSteps.KeyHedgingInterest] = hedgingInterest;
                ScenarioContext.Current[InvestmentStrategySharedSteps.KeyHedgingInflation] = hedgingInflation;
            }
        }

        [Then(@"I see that the asset class sliders are in correct order")]
        public void ThenISeeThatTheAssetClassSlidersAreInCorrectOrder()
        {
            var json = ScenarioContext.Current.Get<dynamic>("result");
            var setup = JsonReader.Get<object>(json, "setup");
            var assetClasses = ((IEnumerable<dynamic>) JsonReader.Get<IEnumerable<dynamic>>(setup, "assetClasses")).ToList();

            Assert.AreEqual(JsonReader.Get<dynamic>(assetClasses[0], "Label"), AssetClassType.DiversifiedCredit.DisplayName());
            Assert.AreEqual(JsonReader.Get<dynamic>(assetClasses[1], "Label"), AssetClassType.DiversifiedGrowth.DisplayName());
            Assert.AreEqual(JsonReader.Get<dynamic>(assetClasses[2], "Label"), AssetClassType.Equity.DisplayName());
            Assert.AreEqual(JsonReader.Get<dynamic>(assetClasses[3], "Label"), AssetClassType.PrivateMarkets.DisplayName());
            Assert.AreEqual(JsonReader.Get<dynamic>(assetClasses[4], "Label"), AssetClassType.Property.DisplayName());
            Assert.AreEqual(JsonReader.Get<dynamic>(assetClasses[5], "Label"), AssetClassType.CorporateBonds.DisplayName());
            Assert.AreEqual(JsonReader.Get<dynamic>(assetClasses[6], "Label"), AssetClassType.FixedInterestGilts.DisplayName());
            Assert.AreEqual(JsonReader.Get<dynamic>(assetClasses[7], "Label"), AssetClassType.IndexLinkedGilts.DisplayName());
            Assert.AreEqual(JsonReader.Get<dynamic>(assetClasses[8], "Label"), AssetClassType.Cash.DisplayName());
        }

        [Then(@"I see that the values in the assets breakdown on the Investment strategy tab match those on the Assets panel")]
        public void ThenISeeThatTheValuesInTheAssetsBreakdownOnTheInvestmentStrategyTabMatchThoseOnTheAssetsPanel()
        {
            var investmentStrategyData = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy() as JsonResult).Data;

            var investmentStrategyAssetsBreakdown = JsonReader.Get<IEnumerable<BreakdownItem>>(investmentStrategyData, "assetsBreakdown");

            var assetsData = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).Assets() as JsonResult).Data;

            var assetsAssetsBreakdown = JsonReader.Get<IEnumerable<BreakdownItem>>(assetsData, "assetsBreakdown");

            foreach (var category in investmentStrategyAssetsBreakdown)
            {
                var target = assetsAssetsBreakdown.SingleOrDefault(x => x.Label == category.Label);

                if (target != null)
                    Assert.That(category.Value,Is.EqualTo(target.Value).Within(Utils.Precision));
                else
                    Assert.True(category.Value == 0, "Assets breakdown category not found but it exists on the investment strategy panel");
            }
        }
    }
}
