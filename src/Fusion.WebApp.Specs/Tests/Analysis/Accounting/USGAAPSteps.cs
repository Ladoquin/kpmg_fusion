﻿namespace Fusion.WebApp.Specs.Tests.Analysis.Accounting
{
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;

    [Binding]
    public class USGAAPSteps
    {
        [Then(@"I see the USGAAP forecast chart")]
        [When(@"I see the USGAAP forecast chart")]
        public void ThenISeeTheUSGAAPForecastChart()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).AccountingData() as JsonResult).Data;

            var usgaap = JsonReader.Get<object>(JsonReader.Get<object>(data, "after"), "pnlUsgaap");

            Assert.NotNull(usgaap);
            Assert.NotNull(JsonReader.Get<double>(usgaap, "ServiceCost"));
            Assert.NotNull(JsonReader.Get<double>(usgaap, "InterestCost"));
            Assert.NotNull(JsonReader.Get<double>(usgaap, "ExpectedReturn"));
            Assert.NotNull(JsonReader.Get<double>(usgaap, "AmortisationOfPriorService"));
            Assert.NotNull(JsonReader.Get<double>(usgaap, "AmortisationOfActuarial"));
            Assert.NotNull(JsonReader.Get<double>(usgaap, "TotalNetPeriodicPension"));
        }

        [Then(@"The USGAAP forecast chart values have changed")]
        public void ThenTheUSGAAPForecastChartValuesHaveChanged()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).AccountingData() as JsonResult).Data;

            var before = JsonReader.Get<object>(JsonReader.Get<object>(data, "before"), "pnlUsgaap");
            var after = JsonReader.Get<object>(JsonReader.Get<object>(data, "after"), "pnlUsgaap");

            Assert.NotNull(before);
            Assert.NotNull(after);
            var somethingHasChanged = 
                JsonReader.Get<double>(before, "ServiceCost") != JsonReader.Get<double>(after, "ServiceCost") ||
                JsonReader.Get<double>(before, "InterestCost") != JsonReader.Get<double>(after, "InterestCost") ||
                JsonReader.Get<double>(before, "ExpectedReturn") != JsonReader.Get<double>(after, "ExpectedReturn") ||
                JsonReader.Get<double>(before, "AmortisationOfPriorService") != JsonReader.Get<double>(after, "AmortisationOfPriorService") ||
                JsonReader.Get<double>(before, "AmortisationOfActuarial") != JsonReader.Get<double>(after, "AmortisationOfActuarial") ||
                JsonReader.Get<double>(before, "TotalNetPeriodicPension") != JsonReader.Get<double>(after, "TotalNetPeriodicPension");
            Assert.True(somethingHasChanged);
        }
    }
}
