﻿namespace Fusion.WebApp.Specs.Tests.Analysis.Accounting
{
    using FlightDeck.DomainShared;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.Globalization;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class DisclosureSharedSteps
    {
        [When(@"I download the (.*) disclosure for date period (.*) to (.*)")]
        public void WhenIDownloadTheXDisclosureForDatePeriodTo(AccountingStandardType type, string d1, string d2)
        {
            var format = "d";
            var culture = new CultureInfo("en-GB");
            var start = DateConverter.ConvertToJsonDate(DateTime.ParseExact(d1, format, culture));
            var end = DateConverter.ConvertToJsonDate(DateTime.ParseExact(d2, format, culture));

            ScenarioContext.Current.Add("disclosureResult",
                ControllerFactory.Get<AnalysisController>(ScenarioContext.Current).Disclosure(type, start, end) as FileStreamResult);
        }

        [Then(@"I am served the (.*) disclosure")]
        public void ThenIAmServedTheXDisclosure(AccountingStandardType type)
        {
            var disclosureResult = ScenarioContext.Current.Get<FileStreamResult>("disclosureResult");

            Assert.NotNull(disclosureResult);
            Assert.Greater(disclosureResult.FileStream.Length, 0);
            Assert.IsNotNullOrEmpty(disclosureResult.FileDownloadName);
        }
    }
}
