﻿using FlightDeck.DomainShared;
using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Analysis.RecoveryPlan
{
    [Binding]
    public class RecoveryPlanSteps
    {
        const string KeyRecoveryPlanIncreases = "RecoveryPlanIncreases";
        const string KeyRecoveryPlanLumpSum = "RecoveryPlanLumpSum";

        [When(@"I set the Recovery Plan to")]
        public void WhenISetTheRecoveryPlanTo(Table table)
        {
            var length = int.Parse(table.Rows[0]["Length"]);
            var increases = double.Parse(table.Rows[0]["Increases"]);
            var lumpsum = double.Parse(table.Rows[0]["LumpSum"]);
            var start = int.Parse(table.Rows[0]["Start"]);
            var fixedAssetReturn = int.Parse(table.Rows[0]["FixedAssetReturn"]);
            var fixedGrowth = fixedAssetReturn > 0;
            var discIncrement = fixedGrowth ? 0 : double.Parse(table.Rows[0]["DiscountRate"]);
            var strategy = (RecoveryPlanType)Enum.Parse(typeof(RecoveryPlanType), table.Rows[0]["Strategy"]);

            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var rp = string.Format("{{\"recoveryPlanLength\":{0},\"recoveryPlanIncreases\":{1},\"recoveryPlanLumpSumPayment\":{2},\"recoveryPlanStartMonth\":{3},\"recoveryPlanInvestmentGrowth\":{4},\"recoveryPlanDiscountIncrement\":{5},\"recoveryPlanIsGrowthFixed\":{6},\"recoveryPlanStrategyType\":\"{7}\"}}",
                length,
                increases,
                lumpsum,
                start,
                fixedAssetReturn,
                discIncrement,
                fixedGrowth ? "true" : "false",
                strategy.ToString());

            controller.SetRecoveryPlan(rp);

            ScenarioContext.Current.Add(KeyRecoveryPlanIncreases, increases);
            ScenarioContext.Current.Add(KeyRecoveryPlanLumpSum, lumpsum);
        }
        
        [Then(@"My changes are accepted")]
        public void ThenMyChangesAreAccepted()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var rp = (controller.RecoveryPlan() as JsonResult).Data;

            var expected = ScenarioContext.Current.Get<double>(KeyRecoveryPlanLumpSum);
            var actual = JsonReader.Get<double>(rp, "LumpSumPayment");

            Assert.AreEqual(expected, actual);
        }
    }
}
