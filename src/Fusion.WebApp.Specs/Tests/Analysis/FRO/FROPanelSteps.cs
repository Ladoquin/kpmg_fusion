﻿namespace Fusion.WebApp.Specs.Tests.Analysis.FRO
{
    using Fusion.Shared.Data;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.IO;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;

    [Binding]
    public class FROPanelSteps
    {
        private struct FRODefaultValues
        {
            public double MinTV { get; private set; }
            public double MaxTV { get; private set; }

            public FRODefaultValues(double minTV, double maxTV)
                : this()
            {
                MinTV = minTV;
                MaxTV = maxTV;
            }
        }

        [BeforeScenario("fropanel_customvals")]
        public static void BeforeFeature()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "FRO", "Setup.sql"));
        }

        [AfterScenario("fropanel_customvals")]
        public static void AfterFeature()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "FRO", "TearDown.sql"));
        }

        [Then(@"I see the correct default parameters on the FRO panel")]
        public void ThenISeeTheCorrectDefaultParametersOnTheFROPanel()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);
            var result = controller.Fro() as ActionResult;

            Assert.NotNull(result);

            var json = (result as JsonResult).Data;

            Assert.NotNull(json);
            
            var froliability = JsonReader.Get<double>(json, "Over55");

            var definition = SchemeQueries.GetFROETVDefaults(ScenarioContext.Current.GetSchemeName());

            var minTv = definition.FROMinTVAmount.GetValueOrDefault(froliability * 0.5);
            var maxTv = definition.FROMaxTVAmount.GetValueOrDefault(froliability * 1.2);

            var setupData = JsonReader.Get<object>(json, "setup");

            Assert.AreEqual(0, JsonReader.Get<double>(setupData, "EquivalentTansferValue"));
            Assert.AreEqual(0, JsonReader.Get<double>(setupData, "TakeUpRate"));
            Assert.AreEqual(minTv, JsonReader.Get<double>(setupData, "FROMinTransferValue"));
            Assert.AreEqual(maxTv, JsonReader.Get<double>(setupData, "FROMaxTransferValue"));
        }
    }
}
