﻿namespace Fusion.WebApp.Specs.Tests.Analysis.FRO
{
    using FlightDeck.DomainShared;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class EFROPanelSteps
    {
        const string KeyETV = "EFROETVValue";
        const string KeyTakeUp = "EFROTakeUpValue";

        [When(@"I set EFRO as")]
        public void WhenISetEFROAs(Table table)
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            ScenarioContext.Current.Add(KeyETV, int.Parse(table.Rows[0]["SizeOfTransferValue"]) * 0.01);
            ScenarioContext.Current.Add(KeyTakeUp, int.Parse(table.Rows[0]["TakeUpRate"]) * 0.01);

            var efroData = string.Format("{{\"equivalentTansferValue\":{0},\"takeUpRate\":{1}}}", ScenarioContext.Current.Get<double>(KeyETV).ToString(), ScenarioContext.Current.Get<double>(KeyTakeUp).ToString());

            controller.SetEFro(efroData);
        }

        [Then(@"The EFRO assumptions are persisted")]
        public void ThenTheEFROAssumptionsArePersisted()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var data = (controller.Fro() as JsonResult).Data;

            var froType = JsonReader.Get<FROType>(data, "froType");
            var eFroData = JsonReader.Get<object>(data, "eFroData");
            var actualEmbeddedFroBasisChange = JsonReader.Get<double>(eFroData, "EmbeddedFroBasisChange");
            var actualAnnualTakeUpRate = JsonReader.Get<double>(eFroData, "AnnualTakeUpRate");

            Assert.AreEqual(ScenarioContext.Current.Get<double>(KeyETV), actualEmbeddedFroBasisChange, "EmbeddedFroBasisChange");
            Assert.AreEqual(ScenarioContext.Current.Get<double>(KeyTakeUp), actualAnnualTakeUpRate, "AnnualTakeUpRate");
        }
    }
}