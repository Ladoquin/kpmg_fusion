﻿namespace Fusion.WebApp.Specs.Tests.Analysis
{
    using FlightDeck.DomainShared;
    using Fusion.WebApp.Controllers;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Xml;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class ResultsDumpSteps
    {
        [When(@"I make a change to the assumptions")]
        public void WhenIMakeAChangeToTheAssumptions()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var recoveryPlanAssumptions = new Dictionary<string, object>
            {
                { "recoveryPlanInvestmentGrowth", "0.005" },
                { "recoveryPlanDiscountIncrement", "0.005" },
                { "recoveryPlanLumpSumPayment", "0" },
                { "recoveryPlanIncreases", "0" },
                { "recoveryPlanIsGrowthFixed", "false" },
                { "recoveryPlanStartMonth", "6" }, // <- changing the start month from default of 12 to 6
                { "recoveryPlanLength", "5" },
                { "recoveryPlanStrategyType", RecoveryPlanType.Current.ToString() }
            };

            controller.SetRecoveryPlan(JsonConvert.SerializeObject(recoveryPlanAssumptions));

            ScenarioContext.Current.Add("RecoveryPlanLengthChanged", true);
        }

        [When(@"I click the results dump button")]
        public void WhenIClickTheResultsDumpButton()
        {
            var controller = ControllerFactory.Get<AnalysisController>(ScenarioContext.Current);

            var result = controller.ResultsDump() as FileStreamResult;

            ScenarioContext.Current.Add("ResultsDump", result);
        }

        [Then(@"The results dump file is served")]
        public void ThenTheResultsDumpFileIsServed()
        {
            var result = ScenarioContext.Current.Get<FileStreamResult>("ResultsDump");

            var xml = new XmlDocument();

            xml.Load(result.FileStream);

            ScenarioContext.Current.Add("ResultsXML", xml);
        }

        [Then(@"The results file contains the expected results")]
        public void ThenTheResultsFileContainsTheExpectedResults()
        {
            var xml = ScenarioContext.Current.Get<XmlDocument>("ResultsXML");

            Assert.AreEqual("22.7", xml.SelectSingleNode("scheme/inputs/LiabilityAssumptions/DeferredLifeExpectancy").InnerText);
            Assert.AreEqual("0.04", xml.SelectSingleNode("scheme/inputs/AssetAssumptions/Equity").InnerText);
            Assert.AreEqual("Bulk", xml.SelectSingleNode("scheme/inputs/FROType").InnerText);

            var expectedRecoveryPlanYear0 = ScenarioContext.Current.ContainsKey("RecoveryPlanLengthChanged") && ScenarioContext.Current.Get<bool>("RecoveryPlanLengthChanged")
                ? 1750000
                : 1000000;
            var actualRecoveryPlanYear0 = double.Parse(xml.SelectSingleNode("scheme/outputs/RecoveryPlan/year00").InnerText);
            Assert.AreEqual(expectedRecoveryPlanYear0, Math.Round(actualRecoveryPlanYear0 / 10000d) * 10000);
        }
    }
}

