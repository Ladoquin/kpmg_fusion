﻿namespace Fusion.WebApp.Specs.Tests
{
    using FlightDeck.DomainShared;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using Fusion.WebApp.Specs.Tests.Analysis.InvestmentStrategy;
    using newton = Newtonsoft.Json;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    using FlightDeck.ServiceInterfaces;

    [Binding]
    public class SharedAnalysisSteps
    {
        [When(@"I set the Analysis Date to (.*)")]
        public void WhenISetTheAnalysisDateTo(string date)
        {
            var analysisDate = new DateTime(int.Parse(date.Split('/')[2]), int.Parse(date.Split('/')[1]), int.Parse(date.Split('/')[0]));

            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var end = new TimeSpan(analysisDate.Ticks).Add(-new TimeSpan(JsonHelper.UnixEpoch.Ticks)).TotalMilliseconds.ToString();

            controller.SetAnalysisPeriod("", end);

            ScenarioContext.Current.SetAttributionEnd(analysisDate);
        }

        [When(@"I change Investment Strategy allocation assumptions")]
        public void WhenIChangeInvestmentStrategyAllocationAssumptions()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);
            
            Func<double, double, List<Dictionary<string, object>>> getAllocations = (equityProportion, giltsProportion) =>
            {
                return new List<Dictionary<string, object>>
                    {
                        new Dictionary<string, object> { { "id", AssetClassType.Equity }, { "proportion", equityProportion }},
                        new Dictionary<string, object> { { "id", AssetClassType.PrivateMarkets }, { "proportion", 0 }},
                        new Dictionary<string, object> { { "id", AssetClassType.FixedInterestGilts }, { "proportion", giltsProportion }},
                        new Dictionary<string, object> { { "id", AssetClassType.CorporateBonds }, { "proportion", 0.10 }},
                        new Dictionary<string, object> { { "id", AssetClassType.Property }, { "proportion", 0 }},
                        new Dictionary<string, object> { { "id", AssetClassType.DiversifiedGrowth }, { "proportion", .15 }},
                        new Dictionary<string, object> { { "id", AssetClassType.DiversifiedCredit }, { "proportion", 0 }},
                        new Dictionary<string, object> { { "id", AssetClassType.Cash }, { "proportion", .05 }},
                        new Dictionary<string, object> { { "id", AssetClassType.IndexLinkedGilts }, { "proportion", 0 }},
                        new Dictionary<string, object> { { "id", AssetClassType.Abf }, { "proportion", 0 }}
                    };
            };

            var json = newton.JsonConvert.SerializeObject(getAllocations(0.65, 0.05));
            controller.SetInvestmentStrategyAssetMix(json);

            // change the allocations again - bug was reported where changing the slider more than once returns incorrect current weighted asset return
            json = newton.JsonConvert.SerializeObject(getAllocations(0.45, 0.25));

            controller.SetInvestmentStrategyAssetMix(json);
        }

        [When(@"I change Liability assumptions")]
        public void WhenIChangeLiabilityAssumptions()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var assumptions = new Dictionary<string, double>
                {
                    { "preRetirement", 0.05 },
                    { "postRetirement", 0.05 },
                    { "pensioner", 0.05 },
                    { "salaryIncreases", 0.03 },
                    { "RPI", 0.01 },
                    { "CPI", 0.01 },
                    { "assumptionLife65", 22 },
                    { "assumptionLife45", 24 }
                };

            var json = newton.JsonConvert.SerializeObject(assumptions);

            controller.SetLiabilityAssumptions(json);
        }

        [When(@"I change Asset assumptions")]
        public void WhenIChangeAssetAssumptions()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var assetReturns = new Dictionary<string, double>
                {
                    { ((int)AssetClassType.Abf).ToString(), 0.01 },
                    { ((int)AssetClassType.Cash).ToString(), 0.01 },
                    { ((int)AssetClassType.CorporateBonds).ToString(), -0.01 },
                    { ((int)AssetClassType.DiversifiedGrowth).ToString(), 0.02 },
                    { ((int)AssetClassType.Equity).ToString(), 0.01 },
                    { ((int)AssetClassType.FixedInterestGilts).ToString(), 0.02 },
                    { ((int)AssetClassType.Property).ToString(), -0.01 },
                    { ((int)AssetClassType.IndexLinkedGilts).ToString(), 0.01 }
                };

            var json = newton.JsonConvert.SerializeObject(assetReturns);

            controller.SetAssetAssumptions(json);
        }

        [When(@"I load the scenario (.*)")]
        public void WhenILoadTheScenario(string scenarioName)
        {
            var analysis = ControllerFactory.Get<AnalysisController>(ScenarioContext.Current);

            var indexViewData = analysis.Index() as ViewResult;

            var scenario = (indexViewData.Model as AnalysisViewModel).SavedScenarios.Single(x => x.Name == scenarioName);

            var json = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            json.SetBasisAnalysis(scenario.MasterBasisId);

            json.LoadAnalysisAssumptions(scenario.Id);
        }

        [Then(@"The Analysis page is ok")]
        public void ThenTheAnalysisPageIsOk()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var data = (controller.CashLens() as JsonResult).Data;

            var fundingLevelData = JsonReader.Get<object>(data, "fundingLevelData");
            var fundingLevelDataAfter = JsonReader.Get<object>(fundingLevelData, "after");

            Assert.NotNull(fundingLevelDataAfter);
            Assert.False(double.IsNaN(JsonReader.Get<double>(fundingLevelDataAfter, "Balance")));
            Assert.False(double.IsInfinity(JsonReader.Get<double>(fundingLevelDataAfter, "Balance")));
        }

        [Then(@"I should see the following asset classes")]
        public void TheIShouldSeeTheFollowingAssetClasses(Table table)
        {
            var expected = table.Header.Select(x => (int)(AssetClassType)Enum.Parse(typeof(AssetClassType), x))
                .OrderBy(x => (int)x)
                .ToList();

            object json = null;
            List<int> actual = null;

            switch (ScenarioContext.Current.GetCurrentPageSection())
            {
                case PageSectionType.AssetAssumptions:

                    json = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).Assets() as JsonResult).Data;

                    var assetReturns = JsonReader.Get<IEnumerable<AssetReturnItem>>(json, "assetReturns");

                    actual = assetReturns.Select(x => x.Key).OrderBy(x => x).ToList();

                    break;

                case PageSectionType.InvestmentStrategyAssumptions:

                    json = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy() as JsonResult).Data;

                    var assetClasses = JsonReader.Get<IEnumerable<object>>(JsonReader.Get<object>(json, "setup"), "assetClasses");

                    actual = assetClasses.Select(x => JsonReader.Get<int>(x, "Key")).OrderBy(x => x).ToList();
                    
                    break;

                default:

                    break;
            }

            Assert.That(actual, Is.EquivalentTo(expected));
        }

        [When(@"I press Reload Scheme Defaults")]
        public void WhenIPressReloadSchemeDefaults()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);
            controller.ResetAllAssumptions();
        }
    }
}
