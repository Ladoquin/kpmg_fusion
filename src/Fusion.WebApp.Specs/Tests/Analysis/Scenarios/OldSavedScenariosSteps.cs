﻿namespace Fusion.WebApp.Specs.Tests.Analysis.Scenarios
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    using System.Linq;

    [Binding]
    public class OldSavedScenariosSteps
    {
        [Then(@"the scenario is loaded")]
        public void ThenTheScenarioIsLoaded()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var panelstate = (controller.PanelState() as JsonResult).Data;

            var dirty = JsonReader.Get<bool>(panelstate, "liabilityAssumptions");

            Assert.True(dirty);
        }
    }
}

