﻿using Fusion.Shared.Mocks;
using Fusion.WebApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Scheme
{
    [Binding]
    public class SchemeSharedTests
    {
        public static AdditSchemeViewModel GetAdditSchemeViewModel(string schemeName, bool isEditScheme, string accountingDownloadsPassword = null)
        {
            byte[] schemeXmlContent;

            using (var schemeXml = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "scheme_files", schemeName + ".xml"), FileMode.Open))
            {
                schemeXmlContent = new byte[schemeXml.Length];
                schemeXml.Read(schemeXmlContent, 0, Convert.ToInt32(schemeXml.Length));
            }

            var model = new AdditSchemeViewModel
            {
                IsEditScheme = isEditScheme,
                AccountingDownloadsPassword = accountingDownloadsPassword,
                CurrentSchemeName = schemeName,
                NewSchemeName = schemeName,
                SchemeData = new MockHttpPostedFile(schemeXmlContent, "text/xml", schemeName + ".xml")
            };

            return model;
        }

        [Then(@"The scheme upload fails")]
        public void ThenTheSchemeUploadFails()
        {
            var result = ScenarioContext.Current.Get<ViewResult>("SaveSchemeResult").Model as ManageSchemeViewModel;

            Assert.NotNull(result);
            Assert.AreEqual(AdditSchemeResult.AddSchemeFailure, result.AdditSchemeViewModel.Result);
        }
    }
}
