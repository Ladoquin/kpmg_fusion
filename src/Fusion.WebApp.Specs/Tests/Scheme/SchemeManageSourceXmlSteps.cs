﻿namespace Fusion.WebApp.Specs.Tests.Scheme
{
    using Fusion.Shared.Data;
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using NUnit.Framework;
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Xml.Linq;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class SchemeManageSourceXmlSteps
    {
        const string KeySourceXml = "SourceXml";
        const string KeyUpdatedSourceXml = "UpdatedSourceXml";
        const string KeySchemeIdExported = "SchemeIdExported";

        [Given(@"(.*) has been imported")]
        public void GivenSchemeXHasBeenImported(string schemeName)
        {
            if (SchemeQueries.SelectSchemeDetail(schemeName).Rows.Count == 0)
            {
                WhenIImportSchemeX(schemeName);
            }
        }

        [When(@"I import scheme (.*)")]
        public void WhenIImportSchemeX(string schemeName)
        {
            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            var model = SchemeSharedTests.GetAdditSchemeViewModel(schemeName, false);

            var result = controller.Add(model) as ViewResult;

            ScenarioContext.Current.Add("SaveSchemeResult", result);
        }

        [When(@"I click the Export button for (.*)")]
        public void WhenIClickTheExportButtonForSchemeX(string schemeName)
        {
            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            var id = (int)SchemeQueries.SelectSchemeDetail(schemeName).Rows[0]["SchemeDetailId"];

            var source = controller.Download(id) as FileStreamResult;

            ScenarioContext.Current.Add(KeySourceXml, source);
            ScenarioContext.Current.Add(KeySchemeIdExported, id);
        }

        [When(@"I manage a scheme")]
        public void WhenIManageAScheme(Table table)
        {
            var schemeName = table.Rows[0]["SchemeName"];
            var IsNew = bool.Parse(table.Rows[0]["IsNew"]);
            var IsRestricted = bool.Parse(table.Rows[0]["IsRestricted"]);
            var sourceXmlFilename = table.Rows[0]["Xml"];
            var logoFilename = table.Rows[0]["Logo"];

            FileStream logofs = null;
            byte[] logoContent = null;
            if (!string.IsNullOrEmpty(logoFilename))
            {
                logofs = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "image_files", logoFilename), FileMode.Open);
                logoContent = new byte[logofs.Length];
                logofs.Read(logoContent, 0, Convert.ToInt32(logofs.Length));
            }

            FileStream xmlfs = null;
            byte[] xmlContent = null;
            if (!string.IsNullOrEmpty(sourceXmlFilename))
            {
                xmlfs = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "scheme_files", sourceXmlFilename), FileMode.Open);
                xmlContent = new byte[xmlfs.Length];
                xmlfs.Read(xmlContent, 0, Convert.ToInt32(xmlfs.Length));
            }

            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            var model = SchemeSharedTests.GetAdditSchemeViewModel(schemeName, !IsNew);
            model.IsRestricted = IsRestricted;
            model.Logo = logoContent == null ? null : new MockHttpPostedFile(logoContent, "image/png", logoFilename);
            model.SchemeData = xmlContent == null ? null : new MockHttpPostedFile(xmlContent, "text/xml", sourceXmlFilename);

            if (IsNew)
            {
                controller.Add(model);
            }
            else
            {
                (controller.ControllerContext.HttpContext.Request as MockHttpRequest).AddRequestItem("update", "true");
                controller.Edit(model);
            }

            if (logofs != null)
            {
                logofs.Close();
                logofs.Dispose();
            }

            if (xmlfs != null)
            {
                xmlfs.Close();
                xmlfs.Dispose();
            }
        }

        [When(@"I import an edited (.*) source xml")]
        public void WhenIImportAnEditedSchemeXml(string schemeName)
        {
            //get the source xml from resources
            var existingXmlFilepath = Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "scheme_files", schemeName + ".xml");
            var source = File.ReadAllText(existingXmlFilepath);
            var xdoc = XDocument.Parse(source);

            //update the contact tel field
            var updatedContactTel = DateTime.Now.Ticks.ToString();
            xdoc.Element("Data").Element("Main").Element("ContactTel").Value = updatedContactTel;

            //convert updated source in to postable file
            MockHttpPostedFile schemeData;
            byte[] xmlContent;
            using (var xmlfs = new FileStream(existingXmlFilepath, FileMode.Open))
            {
                xmlContent = new byte[xmlfs.Length];
                xmlfs.Read(xmlContent, 0, Convert.ToInt32(xmlfs.Length));
                schemeData = new MockHttpPostedFile(xmlContent, "text/xml", schemeName + ".xml");
            }

            //get the already imported scheme details
            var id = (int)SchemeQueries.SelectSchemeDetail(schemeName).Rows[0]["SchemeDetailId"];
            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);
            var model = (controller.Manage(id) as ViewResult).Model as ManageSchemeViewModel;

            //reimport the model with the updated xml source
            var update = SchemeSharedTests.GetAdditSchemeViewModel(schemeName, true);
            update.SchemeData = schemeData;
            (controller.ControllerContext.HttpContext.Request as MockHttpRequest).AddRequestItem("update", "true");
            controller.Edit(update);

            //save the updated source for future comparison
            ScenarioContext.Current.Add(KeyUpdatedSourceXml, source);
        }

        [Then(@"The scheme xml should be saved to the database for (.*)")]
        public void ThenTheSchemeXmlShouldBeSavedToTheDatabaseForSchemeX(string schemeName)
        {
            var expected = new DbHelper().RunQuery("select * from SchemeDetailSource sds inner join SchemeDetail sd on sd.SchemeDetailId = sds.SchemeDetailId where sd.SchemeName = '" + schemeName + "'").Tables[0];

            Assert.AreEqual(1, expected.Rows.Count);
        }

        [Then(@"I am served the source xml")]
        public void ThenIAmServedTheSourceXml()
        {
            //get xml that has been saved in the database
            var db = new DbHelper().RunQuery("select Source from SchemeDetailSource where SchemeDetailId = " + ScenarioContext.Current.Get<int>(KeySchemeIdExported)).Tables[0];
            var expected = XDocument.Parse((string)db.Rows[0]["Source"]);

            //get xml returned from the controller
            var fs = ScenarioContext.Current.Get<FileStreamResult>(KeySourceXml);
            var actual = XDocument.Load(fs.FileStream);

            Assert.AreEqual(expected.ToString(), actual.ToString());
        }
    }
}