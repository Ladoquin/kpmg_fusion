﻿namespace Fusion.WebApp.Specs.Tests.Scheme
{
    using Fusion.Shared.Data;
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using NUnit.Framework;
    using System;
    using System.IO;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class SchemeManageSteps
    {
        private const string createdSchemeName = "Test Scheme - 99";
        private const string existingEditableSchemeName = "Test Scheme - Accounting Password";

        [When(@"I add a scheme without providing an accounting downloads password")]
        public void WhenIAddASchemeWithoutProvidingAnAccountingDownloadsPassword()
        {
            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            var model = SchemeSharedTests.GetAdditSchemeViewModel(createdSchemeName, false);

            var result = controller.Add(model) as ViewResult;

            var schemeId = 0;
            var rows = new DbHelper().RunQuery("select SchemeDetailId from SchemeDetail where SchemeName = '" + createdSchemeName + "'").Tables[0].Rows;
            if (rows.Count > 0)
                schemeId = (int)rows[0][0];

            ScenarioContext.Current.Add("CreatedSchemeId", schemeId);
            ScenarioContext.Current.Add("TargetSchemeName", createdSchemeName);
            ScenarioContext.Current.Add("SaveSchemeResult", result);
        }

        [When(@"I add a scheme providing an accounting downloads password")]
        public void WhenIAddASchemeProvidingAnAccountingDownloadsPassword()
        {
            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            var model = SchemeSharedTests.GetAdditSchemeViewModel(createdSchemeName, false, "Password1");

            var result = controller.Add(model) as ViewResult;

            var schemeId = 0;
            var rows = new DbHelper().RunQuery("select SchemeDetailId from SchemeDetail where SchemeName = '" + createdSchemeName + "'").Tables[0].Rows;
            if (rows.Count > 0)
                schemeId = (int)rows[0][0];

            ScenarioContext.Current.Add("CreatedSchemeId", schemeId);
            ScenarioContext.Current.Add("TargetSchemeName", createdSchemeName);
            ScenarioContext.Current.Add("SaveSchemeResult", result);
        }

        [When(@"I edit an existing scheme with an accounting downloads password and do not provide a new password")]
        public void WhenIEditAnExistingSchemeWithAnAccountingDownloadsPasswordAndDoNotProvideANewPassword()
        {
            var rows = new DbHelper().RunQuery("select SchemeDetailId, AccountingDownloadsPassword from SchemeDetail where SchemeName = '" + existingEditableSchemeName + "'").Tables[0].Rows;
            if (rows.Count == 0 || rows[0]["AccountingDownloadsPassword"] == DBNull.Value)
                Assert.Fail("Test requires '" + existingEditableSchemeName + "' scheme to have been imported with an accounting password set.");
            var row = rows[0];
            var existingSchemeId = (int)row["SchemeDetailId"];
            var existingPassword = (string)row["AccountingDownloadsPassword"];

            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            var model = new AdditSchemeViewModel
                {
                    Id = existingSchemeId,
                    AccountingDownloadsPassword = null,
                    NewSchemeName = existingEditableSchemeName,
                    CurrentSchemeName = existingEditableSchemeName,
                    IsEditScheme = true
                };

            (controller.ControllerContext.HttpContext.Request as MockHttpRequest).AddRequestItem("update", "true");

            var result = controller.Edit(model) as ViewResult;

            ScenarioContext.Current.Add("TargetSchemeName", existingEditableSchemeName);
            ScenarioContext.Current.Add("ExistingAccountingDownloadsPassword", existingPassword);
            ScenarioContext.Current.Add("SaveSchemeResult", result);
        }

        [When(@"I edit an existing scheme with an accounting downloads password and provide a new password")]
        public void WhenIEditAnExistingSchemeWithAnAccountingDownloadsPasswordAndProvideANewPassword()
        {
            var rows = new DbHelper().RunQuery("select SchemeDetailId, AccountingDownloadsPassword from SchemeDetail where SchemeName = '" + existingEditableSchemeName + "'").Tables[0].Rows;
            if (rows.Count == 0 || rows[0]["AccountingDownloadsPassword"] == DBNull.Value)
                Assert.Fail("Test requires '" + existingEditableSchemeName + "' scheme to have been imported with an accounting password set.");
            var row = rows[0];
            var existingSchemeId = (int)row["SchemeDetailId"];
            var existingPassword = (string)row["AccountingDownloadsPassword"];

            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            var model = new AdditSchemeViewModel
            {
                Id = existingSchemeId,
                AccountingDownloadsPassword = "Password" + DateTime.Now.Ticks.ToString(),
                NewSchemeName = existingEditableSchemeName,
                CurrentSchemeName = existingEditableSchemeName,
                IsEditScheme = true
            };

            (controller.ControllerContext.HttpContext.Request as MockHttpRequest).AddRequestItem("update", "true");

            var result = controller.Edit(model) as ViewResult;

            ScenarioContext.Current.Add("TargetSchemeName", existingEditableSchemeName);
            ScenarioContext.Current.Add("ExistingAccountingDownloadsPassword", existingPassword);
            ScenarioContext.Current.Add("SaveSchemeResult", result);
        }

        [Then(@"The scheme should be saved")]
        public void ThenTheSchemeShouldBeSaved()
        {
            var result = ScenarioContext.Current.Get<ViewResult>("SaveSchemeResult");

            var model = result.Model as ManageSchemeViewModel;

            Assert.NotNull(model);
            Assert.NotNull(model.AdditSchemeViewModel);
            Assert.True(model.AdditSchemeViewModel.Result == AdditSchemeResult.AddSchemeSuccess || model.AdditSchemeViewModel.Result == AdditSchemeResult.EditSchemeSuccess);
        }

        [Then(@"The accounting downloads password for the scheme should be null")]
        public void ThenTheAccountingDownloadsPasswordForTheSchemeShouldBeNull()
        {
            var targetSchemeName = ScenarioContext.Current.Get<string>("TargetSchemeName");
            var result = new DbHelper().RunQuery("select AccountingDownloadsPassword from SchemeDetail where SchemeName = '" + targetSchemeName + "'").Tables[0].Rows[0]["AccountingDownloadsPassword"];

            Assert.IsTrue(result == DBNull.Value || result == null);
        }

        [Then(@"The accounting downloads password for the scheme should not be null")]
        public void ThenTheAccountingDownloadsPasswordForTheSchemeShouldNotBeNull()
        {
            var targetSchemeName = ScenarioContext.Current.Get<string>("TargetSchemeName");
            var result = new DbHelper().RunQuery("select AccountingDownloadsPassword from SchemeDetail where SchemeName = '" + targetSchemeName + "'").Tables[0].Rows[0]["AccountingDownloadsPassword"];

            Assert.IsTrue(result != DBNull.Value && !string.IsNullOrEmpty(result.ToString()));
        }

        [Then(@"The accounting downloads password for the scheme should be unchanged")]
        public void ThenTheAccountingDownloadsPasswordForTheSchemeShouldBeUnchanged()
        {
            var targetSchemeName = ScenarioContext.Current.Get<string>("TargetSchemeName");
            var previousPassword = ScenarioContext.Current.Get<string>("ExistingAccountingDownloadsPassword");
            var currentPassword = new DbHelper().RunQuery("select AccountingDownloadsPassword from SchemeDetail where SchemeName = '" + targetSchemeName + "'").Tables[0].Rows[0]["AccountingDownloadsPassword"].ToString();

            Assert.AreEqual(previousPassword, currentPassword);
        }

        [Then(@"The accounting downloads password for the scheme should be changed")]
        public void ThenTheAccountingDownloadsPasswordForTheSchemeShouldBeChanged()
        {
            var targetSchemeName = ScenarioContext.Current.Get<string>("TargetSchemeName");
            var previousPassword = ScenarioContext.Current.Get<string>("ExistingAccountingDownloadsPassword");
            var currentPassword = new DbHelper().RunQuery("select AccountingDownloadsPassword from SchemeDetail where SchemeName = '" + targetSchemeName + "'").Tables[0].Rows[0]["AccountingDownloadsPassword"].ToString();

            Assert.AreNotEqual(previousPassword, currentPassword);
        }

        [AfterScenario("schemeManageAdd")]
        public static void TeardownAddedScheme()
        {
            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);
            var createdSchemeId = ScenarioContext.Current.Get<int>("CreatedSchemeId");

            controller.Edit(
                new AdditSchemeViewModel
                    {
                        Id = createdSchemeId,
                        CurrentSchemeName = createdSchemeName
                    });
        }
    }
}
