﻿using Fusion.Shared.Mocks;
using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Scheme
{
    [Binding]
    public class LastImportDetailsSteps
    {
        [When(@"I upload a scheme")]
        public void WhenIUploadAScheme()
        {
            var model = SchemeSharedTests.GetAdditSchemeViewModel("Test Scheme - 01", true);

            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            (controller.ControllerContext.HttpContext.Request as MockHttpRequest).AddRequestItem("update", "true");

            var result = controller.Edit(model) as ViewResult;

            ScenarioContext.Current.Add("SaveSchemeResult", result);
        }

        [When(@"I upload a scheme with an invalid export file value")]
        public void WhenIUploadASchemeWithAnInvalidExportFileValue()
        {
            var model = SchemeSharedTests.GetAdditSchemeViewModel("Invalid AuditTrail Scheme", false);

            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            var result = controller.Add(model) as ViewResult;

            ScenarioContext.Current.Add("SaveSchemeResult", result);
        }

        [Then(@"the last import details should show details of the previous import")]
        public void ThenTheLastImportDetailsShouldShowDetailsOfThePreviousImport()
        {
            var result = ScenarioContext.Current.Get<ViewResult>("SaveSchemeResult").Model as ManageSchemeViewModel;

            Assert.AreEqual(AdditSchemeResult.EditSchemeSuccess, result.AdditSchemeViewModel.Result);

            var lastImportDetails = result.SchemeImportHistory.OrderBy(x => x.SchemeData_ImportedOnDate).Last();

            Assert.NotNull(lastImportDetails);
        }
    }
}
