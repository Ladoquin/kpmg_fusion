﻿namespace Fusion.WebApp.Specs.Tests
{
    using System;
    using System.Configuration;
    using System.IO;
    using TechTalk.SpecFlow;
    using WebMatrix.WebData;
    
    [Binding]
    public class InitialiseWebAppFeature
    {
        [BeforeFeature("web")]
        public static void InitialiseWebApp()
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Path.Combine(Environment.CurrentDirectory, ConfigurationManager.AppSettings["log4net.Config"])));
            FlightDeck.Services.Configuration.Bootstrapper.Init();
            if (!WebSecurity.Initialized)
                WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
        }
    }
}
