﻿using FlightDeck.DomainShared;
using Fusion.Shared.Data;
using Fusion.Shared.Mocks;
using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.StatsUpload
{
    [Binding]
    public class StatsFileValidationSteps
    {
        [Then(@"the file is rejected and the reasons why are displayed")]
        public void ThenTheFileIsRejectedAndTheReasonsWhyAreDisplayed()
        {
            var result = ScenarioContext.Current.Get<ActionResult>() as ViewResult;
            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Failure, model.Result);
            Assert.IsNotNull(model.ValidationMessages);
            Assert.IsTrue(model.ValidationMessages.Any());

            ScenarioContext.Current.Set(model);
        }

        [Then(@"one or more of the reasons relates to wrong data types")]
        public void ThenOneOrMoreOfTheReasonsRelatesToWrongDataTypes()
        {
            var model = ScenarioContext.Current.Get<IndexImportViewModel>();

            Assert.IsTrue(model.ValidationMessages.Any(m => m.Contains("The value 'foo' is invalid according to its datatype")));
        }

        [Then(@"one or more of the reasons relates to missing mandatory data")]
        public void ThenOneOrMoreOfTheReasonsRelatesToMissingMandatoryData()
        {
            var model = ScenarioContext.Current.Get<IndexImportViewModel>();

            Assert.IsTrue(model.ValidationMessages.Any(m => m.Contains("The actual length is less than the MinLength value")));
        }

        [Then(@"one or more of the reasons relates to missing mandatory elements")]
        public void ThenOneOrMoreOfTheReasonsRelatesToMissingMandatoryElements()
        {
            var model = ScenarioContext.Current.Get<IndexImportViewModel>();

            Assert.IsTrue(model.ValidationMessages.Any(m => m.Contains("List of possible elements expected")));
        }

        [Then(@"I am not able to proceed with the import")]
        public void ThenIAmNotAbleToProceedWithTheImport()
        {
            var model = ScenarioContext.Current.Get<IndexImportViewModel>();

            Assert.IsFalse(model.AllowInvalidSubmission);
        }


        [Then(@"I am able to proceed with the import")]
        public void ThenIAmAbleToProceedWithTheImport()
        {
            var model = ScenarioContext.Current.Get<IndexImportViewModel>();

            Assert.IsTrue(model.AllowInvalidSubmission);
        }

        [Then(@"validation messages are logged")]
        public void ThenValidationMessagesAreLogged()
        {
            var logs = new DbHelper().RunQuery("select * from [Log] where [Message] like 'Import file ''test-stats-file.xml''%'", DbHelper.Db.Log);

            Assert.IsNotNull(logs);
            Assert.IsTrue(logs.Tables[0].Rows.Count > 0);
        }

        [AfterFeature("stats")]
        public static void TearDown()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Stats", "TearDown.sql"));
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Stats", "TearDown2.sql"), DbHelper.Db.Log);

            new FlightDeck.Services.ServiceFactory().ClearCache();
        }

    }
}
