﻿using FlightDeck.DomainShared;
using Fusion.Shared.Data;
using Fusion.Shared.Mocks;
using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.StatsUpload
{
    [Binding]
    public class LastImportDetailsSteps
    {
        [When(@"I upload another stats file")]
        public void WhenIUploadAnotherStatsFile()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-valid-another.xml"), FileMode.Open);
            var contentData = new byte[statsfile.Length];
            statsfile.Read(contentData, 0, Convert.ToInt32(statsfile.Length));

            var model = new IndexImportViewModel
            {
                ImportDetail = new IndexImportDetail(),
                ImportFile = new MockHttpPostedFile(contentData, "text/xml", "test-stats-file.xml")
            };

            var controller = ControllerFactory.Get<IndicesController>(ScenarioContext.Current);
            var result = controller.Save(model);

            statsfile.Close();
            statsfile.Dispose();
            ScenarioContext.Current.Set(result);
        }

        [Then(@"The Last Import Details displays the details of that upload (.*)")]
        public void ThenTheLastImportDetailsDisplaysTheDetailsOfThatUpload(string tag)
        {
            var controller = ControllerFactory.Get<IndicesController>(ScenarioContext.Current);
            
            var result = controller.Manage() as ViewResult;

            Assert.NotNull(result);

            var v = tag == "#v1";
            var expected = new Dictionary<string, string>
                {
                    { "producedByUser", v ? "aperson" : "pbody" },
                    { "exportfile", v ? @"c:\Fusion stats export file.xml" : @"c:\Fusion stats export file v2.xml" }
                };
            var actual = result.Model as IndexImportViewModel;

            Assert.NotNull(actual);
            Assert.AreEqual(expected["producedByUser"], actual.ImportDetail.ProducedByUser);
            Assert.AreEqual(expected["exportfile"], actual.ImportDetail.ExportFile);
        }

        [Then(@"Both uploads are stored in the database history")]
        public void WhenTheBothUploadsAreStoredInTheDatabaseHistory()
        {
            var rows = new DbHelper().RunQuery("select top 2 * from IndexImportDetail where ImportedByUser = 'fusionapptestuseradmin' order by ImportedOnDate desc").Tables[0].Rows;

            Assert.NotNull(rows);

            var history = new List<IndexImportDetail>
                {
                    new IndexImportDetail() { ProducedByUser = (string)rows[0]["ProducedByUser"], ExportFile = (string)rows[0]["ExportFile"] },
                    new IndexImportDetail() { ProducedByUser = (string)rows[1]["ProducedByUser"], ExportFile = (string)rows[1]["ExportFile"] }
                };

            Assert.AreEqual(1, history.Count(x => x.ProducedByUser == "aperson"), "ProducedByUser: aperson");
            Assert.AreEqual(1, history.Count(x => x.ProducedByUser == "pbody"), "ProducedByUser: pbody");
            Assert.AreEqual(1, history.Count(x => x.ExportFile == @"c:\Fusion stats export file.xml"), "ExportFile: stats export file.xml");
            Assert.AreEqual(1, history.Count(x => x.ExportFile == @"c:\Fusion stats export file v2.xml"), "ExportFile: status export file v2.xml");
        }
    }
}
