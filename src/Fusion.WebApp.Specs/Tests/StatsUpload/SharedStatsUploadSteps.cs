﻿namespace Fusion.WebApp.Specs.Tests.StatsUpload
{
    using FlightDeck.DomainShared;
    using Fusion.Shared.Data;
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using NUnit.Framework;
    using System;
    using System.IO;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class SharedStatsUploadSteps
    {
        [Given(@"I have a stats file with a valid schema")]
        public void GivenIHaveAStatsFileWithAValidSchema()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-valid.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with an updated historic index")]
        public void GivenIHaveAStatsFileWithAnUpdatedHistoricIndex()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-valid-updated-index.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with an invalid schema due to wrong data types")]
        public void GivenIHaveAStatsFileWithAnInvalidSchemaDueToWrongDataTypes()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-invalid-wrongdatatypes.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with an invalid schema due to missing mandatory data")]
        public void GivenIHaveAStatsFileWithAnInvalidSchemaDueToMissingMandatoryData()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-invalid-missingdata.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with an invalid schema due to missing mandatory elements")]
        public void GivenIHaveAStatsFileWithAnInvalidSchemaDueToMissingMandatoryElements()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-invalid-missingelements.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with an invalid audit trail")]
        public void GivenIHaveAStatsFileWithAnInvalidAuditTrail()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-invalid-toolongexportfilevalue.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [When(@"I upload the stats file")]
        public void WhenIUploadTheStatsFile()
        {
            loadStatsFile();
        }

        [Given(@"This stats file is loaded")]
        public void ThisStatsFileIsLoaded()
        {
            loadStatsFile();
        }

        [Given(@"This scheme relies on the index being updated")]
        public void GivenThisSchemeReliesOnTheIndexBeingUpdated()
        {
            var schemeId = ScenarioContext.Current.Get<int>("schemeId");

            var originalIndexId = -1;
            var rows1 = new DbHelper().RunQuery("select LDIInflationIndex from PensionSchemes where Id = " + schemeId).Tables[0].Rows;
            if (rows1.Count > 0)
            {
                originalIndexId = (int)rows1[0][0];
                ScenarioContext.Current.Set(originalIndexId, "OriginalIndexForTearDownReinstatement");
            }

            var testIndexId = -1;
            var rows = new DbHelper().RunQuery("select Id from FinancialIndices where Name = 'TEST INDEX UK RPI'").Tables[0].Rows;
            if (rows.Count > 0)
            {
                testIndexId = (int)rows[0][0];
                new DbHelper().RunQuery("update PensionSchemes set LDIInflationIndex = " + testIndexId + " where Id = " + schemeId);
            }
        }

        private void loadStatsFile()
        {
            var statsfile = ScenarioContext.Current.Get<FileStream>("statsfile");
            var contentData = new byte[statsfile.Length];
            statsfile.Read(contentData, 0, Convert.ToInt32(statsfile.Length));

            var model = new IndexImportViewModel
            {
                ImportDetail = new IndexImportDetail(),
                ImportFile = new MockHttpPostedFile(contentData, "text/xml", "test-stats-file.xml")
            };

            var controller = ControllerFactory.Get<IndicesController>(ScenarioContext.Current);
            var result = controller.Save(model);

            statsfile.Close();
            statsfile.Dispose();
            ScenarioContext.Current.Set(result);
        }

        [Then(@"the file is accepted")]
        public void ThenTheFileIsAccepted()
        {
            var result = ScenarioContext.Current.Get<ActionResult>() as ViewResult;
            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Success, model.Result);
        }

        [Then(@"The file is not accepted")]
        public void ThenTheFileIsNotAccepted()
        {
            var result = ScenarioContext.Current.Get<ActionResult>() as ViewResult;
            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Failure, model.Result);
        }

        [AfterFeature("stats")]
        public static void TearDown()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Stats", "TearDown.sql"));
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Stats", "TearDown2.sql"), DbHelper.Db.Log);

            new FlightDeck.Services.ServiceFactory().ClearCache();
        }
    }
}
