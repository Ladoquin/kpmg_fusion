﻿using Fusion.Shared.Data;
using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using NUnit.Framework;
using System;
using System.IO;
using System.Web.Mvc;
using System.Linq;
using TechTalk.SpecFlow;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.Caching;
using System.Text;
using FlightDeck.ServiceInterfaces;
using System.Collections;

namespace Fusion.WebApp.Specs.Tests.StatsUpload
{
    [Binding]
    public class CustomIndicesSteps
    {
        [BeforeScenario("customStatsExist")]
        public static void SetUp()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Stats", "CreateCustomStats.sql"));
        }

        [Given(@"I have a stats file with a custom indices component")]
        public void GivenIHaveAStatsFileWithACustomIndicesComponent()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-valid-custom-indices.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with a custom indices component that is the same as tracker")]
        public void GivenIHaveAStatsFileWithACustomIndicesComponentThatIsTheSameAsTracker()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-valid-custom-indices-default-tracker-test.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with a custom indices component referencing non core indices")]
        public void GivenIHaveAStatsFileWithACustomIndicesComponentReferencingNonCoreIndices()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-valid-custom-indices-non-core-source.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with a custom indices component containing no component indices")]
        public void GivenIHaveAStatsFileWithACustomIndicesComponentContainingNoComponentIndices()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", 
                "custom-stats-no-components.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with a composite index that has one component")]
        public void GivenIHaveAStatsFileWithACompositeIndexThatHasOneComponent()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files",
                "custom-stats-one-component.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with a custom indices component containing duplicate component indices")]
        public void GivenIHaveAStatsFileWithACustomIndicesComponentContainingDuplicateComponentIndices()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files",
                "custom-stats-duplicate-components.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with a custom index with the same name as a core index")]
        public void GivenIHaveAStatsFileWithACustomIndexWithTheSameNameAsACoreIndex()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files",
                "custom-stats-name-clash-with-core-index.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I have a stats file with a custom indices component containing ten Source Indexes")]
        public void GivenIHaveAStatsFileWithACustomIndicesComponentContainingSourceIndexes()
        {
            var statsfile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "stats_files", "custom-stats-valid-ten-custom-indices.xml"), FileMode.Open);
            ScenarioContext.Current.Set(statsfile, "statsfile");
        }

        [Given(@"I take a snapshot of the indices prior to uploading new ones")]
        public void ITakeASnapshotOfTheIndicesPriorToUploadingNewOnes()
        {
            ScenarioContext.Current.Set(GetIndicesSnapshot(), "indicesSnapshot");
        }

        [When(@"I view the Manage Indices screen")]
        public void WhenIViewTheManageIndicesScreen()
        {
            var controller = ControllerFactory.Get<IndicesController>(ScenarioContext.Current);
            controller.Manage();
            
            ScenarioContext.Current.Set(controller, "controller");
        }

        [Then(@"the custom indices are created")]
        public void ThenTheCustomIndicesAreCreated()
        {
            var ds = new DbHelper().RunQuery("select [Date], [Value] from IndexValues where FinancialIndexId = (Select distinct Id from FinancialIndices where Name = 'Test Composite Index')");
            Assert.IsTrue(ds.Tables[0].Rows.Count > 0, "No custom indices created");
        }

        [Then(@"the (.*) custom indices are created")]
        public void ThenTheCustomIndicesAreCreated(int indicesCount)
        {
            var ds = new DbHelper().RunQuery("Select distinct Id from FinancialIndices where Name like '%Test Composite Index%'");
            Assert.IsTrue(ds.Tables[0].Rows.Count == indicesCount, "Incorrect number of custom indices created");
        }


        [Then(@"the file is rejected")]
        public void ThenTheFileIsRejected()
        {
            var result = ScenarioContext.Current.Get<ActionResult>() as ViewResult;
            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Failure, model.Result);

            ScenarioContext.Current.Set(model);
        }

        [Then(@"no indices are updated or created")]
        public void ThenNoIndicesAreUpdatedOrCreated()
        {
            bool indicesPrePostEqual = true;

            var indicesPreScenario = ScenarioContext.Current.Get<List<IOrderedDictionary>>("indicesSnapshot");
            var indicesPostScenario = GetIndicesSnapshot();

            if (indicesPostScenario.Count != indicesPreScenario.Count)
            {
                indicesPrePostEqual = false;
            }

            if (indicesPrePostEqual)
            {
                for (int i = 0; i < indicesPreScenario.Count; i++)
                {
                    foreach (var indexKey in indicesPreScenario[i].Keys)
                    {
                        if (!indicesPostScenario[i][indexKey].Equals(indicesPreScenario[i][indexKey]))
                        {
                            indicesPrePostEqual = false;
                            break;
                        }
                    }

                    if (!indicesPrePostEqual)
                        break;
                }
            }

            Assert.IsTrue(indicesPrePostEqual);
        }

        [Then(@"the indices the list contains includes composite indices")]
        public void ThenTheIndicesTheListContainsIncludesCompositeIndices()
        {
            var controller = ControllerFactory.Get<IndicesController>(ScenarioContext.Current);
            var result = controller.ViewAll() as ViewResultBase;
            var model = result.Model as IEnumerable<FlightDeck.ServiceInterfaces.IndexDataSummary>;

            Assert.IsTrue(model.Any(i => i.Name == "Test Composite Index Yield Type"));
        }

        [AfterScenario("customStats")]
        public static void TearDown()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Stats", "TearDown.sql"));
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Stats", "CustomIndicesTeardown.sql"));
            new FlightDeck.Services.ServiceFactory().ClearCache();
        }

        [AfterScenario("forcedSchemeLDIInflationIndexDependence")]
        public static void alteredLDIIndexReset()
        {
            var schemeId = ScenarioContext.Current.Get<int>("schemeId");
            var originalIndexId = ScenarioContext.Current.Get<int>("OriginalIndexForTearDownReinstatement");

            new DbHelper().RunQuery("update PensionSchemes set LDIInflationIndex=" + originalIndexId + " where Id = " + schemeId);
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Stats", "TearDown.sql"));

            new FlightDeck.Services.ServiceFactory().ClearCache();
        }

        private List<IOrderedDictionary> GetIndicesSnapshot()
        {
            MemoryCache.Default.Remove("CachedIndexImportDetailService");

            var controller = ControllerFactory.Get<IndicesController>(ScenarioContext.Current);
            var indicesService = ServiceFactory.GetService<IIndicesService>(controller);
            var allIndices = new List<IOrderedDictionary>();

            foreach (var index in indicesService.GetAllSummaries())
            {
                allIndices.Add(indicesService.GetIndexValues(index.Id));
            }

            return allIndices;
        }
    }
}
