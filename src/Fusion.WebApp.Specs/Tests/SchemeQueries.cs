﻿namespace Fusion.WebApp.Specs.Tests
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Assets;
    using FlightDeck.DomainShared;
    using Fusion.Shared.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    
    class SchemeQueries
    {
        public static DataTable SelectSchemeDetail(string schemeName)
        {
            var table = new DbHelper().RunQuery("select * from SchemeDetail where SchemeName = '" + schemeName + "'").Tables[0];

            return table;
        }

        /// <summary>
        /// Get asset class amounts from latest asset upload
        /// </summary>
        /// <param name="schemeName"></param>
        /// <returns></returns>
        public static IDictionary<AssetClassType, double> GetSchemeAssetClassAmounts(string schemeName)
        {
            var def = new Dictionary<AssetClassType, double>();

            var sql = string.Format(
                  @"select sum(af.Amount) [Amount], a.AssetClassId
                    from SchemeDetail sd
                    inner join PensionSchemes ps on sd.SchemeData_Id = ps.Id
                    inner join (select PensionSchemeId, max(EffectiveDate) [effectivedate] from SchemeAssets group by PensionSchemeId
                    ) x on ps.Id = x.PensionSchemeId
                    inner join SchemeAssets sa on x.effectivedate = sa.EffectiveDate and x.PensionSchemeId = sa.PensionSchemeId
                    inner join Assets a on sa.Id = a.SchemeAssetId
                    inner join AssetFunds af on a.Id = af.AssetId
                    where 
                    sd.SchemeName = '{0}'
                    group by a.AssetClassId", 
                schemeName);

            var data = new DbHelper().RunQuery(sql);

            foreach (DataRow row in data.Tables[0].Rows)
                def.Add((AssetClassType)row["AssetClassId"], (double)row["Amount"]);

            return def;
        }

        /// <summary>
        /// Get scheme asset definition from the latest asset upload
        /// </summary>
        /// <param name="schemeName"></param>
        /// <returns></returns>
        public static SchemeAsset GetSchemeAssetDefinition(string schemeName)
        {
            var sql = string.Format(
                  @"select sa.*
                    from SchemeDetail sd
                    inner join PensionSchemes ps on sd.SchemeData_Id = ps.Id
                    inner join (select PensionSchemeId, max(EffectiveDate) [effectivedate] from SchemeAssets group by PensionSchemeId
                    ) x on ps.Id = x.PensionSchemeId
                    inner join SchemeAssets sa on x.effectivedate = sa.EffectiveDate and x.PensionSchemeId = sa.PensionSchemeId
                    where 
                    sd.SchemeName = '{0}'", schemeName);

            var data = new DbHelper().RunQuery(sql).Tables[0].Rows[0];

            var schemeAsset = new SchemeAsset((int)data["Id"], (int)data["PensionSchemeId"], (DateTime)data["EffectiveDate"], (bool)data["LDIInForce"], string.Empty, (double)data["InterestHedge"], (double)data["InflationHedge"], (double)data["PropSyntheticEquity"], (double)data["PropSyntheticCredit"], (int)data["GiltDuration"], (int)data["CorpDuration"], null);

            return schemeAsset;
        }

        public static AnalysisParametersNullable GetFROETVDefaults(string schemeName)
        {
            var sql = string.Format(
                @"select ps.*
                  from SchemeDetail sd 
                  inner join PensionSchemes ps on sd.SchemeData_Id = ps.Id
                  where
                  sd.SchemeName = '{0}'",
                schemeName);

            var data = new DbHelper().RunQuery(sql).Tables[0].Rows[0];

            var def = new AnalysisParametersNullable
                (
                data["FROMinTVAmount"] == DBNull.Value ? (double?)null : (double)data["FROMinTVAmount"],
                data["FROMaxTVAmount"] == DBNull.Value ? (double?)null : (double)data["FROMaxTVAmount"],
                data["ETVMinTVAmount"] == DBNull.Value ? (double?)null : (double)data["ETVMinTVAmount"],
                data["ETVMaxTVAmount"] == DBNull.Value ? (double?)null : (double)data["ETVMaxTVAmount"]
                );

            return def;
        }

        public static double GetSchemeABFProportion(string schemeName)
        {
            var def = GetSchemeAssetClassAmounts(schemeName);

            if (def.ContainsKey(AssetClassType.Abf))
                return def[AssetClassType.Abf].MathSafeDiv(def.Sum(k => k.Value));

            return 0;
        }
    }
}
