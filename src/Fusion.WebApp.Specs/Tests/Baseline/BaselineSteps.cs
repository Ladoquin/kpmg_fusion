﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.Shared.Data;
using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Baseline
{
    [Binding]
    public class BaselineSteps
    {
        [Then(@"I am returned json relevant to the baseline page")]
        public void ThenIAmReturnedJsonRelevantToTheBaselinePage()
        {
            var controller = ControllerFactory.Get<BaselineController>(ScenarioContext.Current);

            var result = controller.Get() as ActionResult;

            Assert.NotNull(result);

            var json = (result as JsonResult).Data;

            Assert.NotNull(json);

            var bases = JsonReader.Get<IEnumerable<object>>(json, "bases");

            Assert.NotNull(bases);

            foreach (object basis in bases)
            {
                Assert.Greater(JsonReader.Get<int>(basis, "id"), 0);
                Assert.IsNotNullOrEmpty(JsonReader.Get<string>(basis, "type"));
                Assert.IsNotNullOrEmpty(JsonReader.Get<string>(basis, "name"));
                Assert.NotNull(JsonReader.Get<double>(basis, "balance"));
                Assert.NotNull(JsonReader.Get<AssumptionData>(basis, "assumptions"));
                Assert.NotNull(JsonReader.Get<bool>(basis, "active"));
                Assert.Greater(JsonReader.Get<double>(basis, "anchorDate"), 0);
            }

            var current = JsonReader.Get<object>(json, "current");

            Assert.NotNull(current);
            Assert.NotNull(JsonReader.Get<object>(current, "basis"));
            Assert.Greater(JsonReader.Get<double>(current, "anchorDate"), 0);
            Assert.NotNull(JsonReader.Get<double>(current, "balance"));
            Assert.NotNull(JsonReader.Get<double>(current, "liabilityTotal"));
            Assert.NotNull(JsonReader.Get<double>(current, "assetTotal"));
            Assert.NotNull(JsonReader.Get<object>(current, "syntheticAssetInfo"));
            Assert.NotNull(JsonReader.Get<IEnumerable<BreakdownItem>>(current, "assetsBreakdown"));
            Assert.NotNull(JsonReader.Get<IEnumerable<KeyLabelValue<string, double>>>(current, "assumptions"));
            Assert.NotNull(JsonReader.Get<IEnumerable<PensionIncreaseDefinition>>(current, "pensionIncreases"));
            Assert.NotNull(JsonReader.Get<NamedPropertyGroup>(current, "namedPropertyGroup"));

            var ldi = JsonReader.Get<object>(json, "ldiData");

            Assert.NotNull(ldi);
            Assert.Greater(JsonReader.Get<double>(ldi, "interestRate"), 0);
            Assert.Greater(JsonReader.Get<double>(ldi, "inflation"), 0);
        }

        [Then(@"I see pension increase assumptions")]
        public void ThenISeePensionIncreaseAssumptions()
        {
            var controller = ControllerFactory.Get<BaselineController>(ScenarioContext.Current);

            var result = controller.Get() as ActionResult;

            Assert.NotNull(result);

            var json = (result as JsonResult).Data;

            Assert.NotNull(json);

            var current = JsonReader.Get<object>(json, "current");
            var pensionIncreases = JsonReader.Get<object>(current, "pensionIncreases");

            ScenarioContext.Current.Set(current, "Basis");

            Assert.NotNull(pensionIncreases);
        }

        [Then(@"The pension increases are as specified in the underlying scheme")]
        public void ThePensionIncreasesAreAsSpecifiedInTheUnderlyingScheme()
        {
            var data = new DbHelper().RunQuery(string.Format(
                                      @"select pincs.Label, pincs.InitialValue
                                        from SchemeDetail sd
                                        inner join Bases b on b.PensionSchemeId = sd.SchemeData_Id
                                        and b.DisplayName = '{0}'
                                        inner join PensionIncreaseAssumptions pincs on pincs.BasisId = b.Id
                                        where sd.SchemeName = '{1}'", 
                                    ScenarioContext.Current.GetBasisDisplayName(),
                                    ScenarioContext.Current.GetSchemeName()));

            var pinc1 = new { Label = (string)data.Tables[0].Rows[0][0], Value = (double)data.Tables[0].Rows[0][1] };
            var pinc2 = new { Label = (string)data.Tables[0].Rows[1][0], Value = (double)data.Tables[0].Rows[1][1] };
            var pinc3 = new { Label = (string)data.Tables[0].Rows[2][0], Value = (double)data.Tables[0].Rows[2][1] };
            var pinc4 = new { Label = (string)data.Tables[0].Rows[3][0], Value = (double)data.Tables[0].Rows[3][1] };
            var pinc5 = new { Label = (string)data.Tables[0].Rows[4][0], Value = (double)data.Tables[0].Rows[4][1] };
            var pinc6 = new { Label = (string)data.Tables[0].Rows[5][0], Value = (double)data.Tables[0].Rows[5][1] };

            List<dynamic> pincsFromDb = new List<dynamic> { pinc1, pinc2, pinc3, pinc4, pinc5, pinc6 };

            var basis = ScenarioContext.Current.Get<dynamic>("Basis");
            var pincs = JsonReader.Get<IEnumerable>(basis, "pensionIncreases");

            List<dynamic> controllerSurfacedPincs = new List<dynamic>();

            foreach (var pinc in pincs)
            {
                var value = JsonReader.Get<double>(pinc, "Value");
                var label = JsonReader.Get<string>(pinc, "Label");

                controllerSurfacedPincs.Add(new { Label = label, Value = value });
            }

            for (int i = 0; i < pincsFromDb.Count; i++)
            {
                var a = Math.Round(pincsFromDb[i].Value, 4);
                var b = Math.Round(controllerSurfacedPincs[i].Value, 4);

                Assert.IsTrue(Math.Round(pincsFromDb[i].Value, 4) == Math.Round(controllerSurfacedPincs[i].Value, 4), string.Format("Pension increase {0} is incorrect", i + 1));
            }
        }
    }
}
