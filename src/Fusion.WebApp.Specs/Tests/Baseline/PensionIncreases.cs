﻿namespace Fusion.WebApp.Specs.Tests.Baseline
{
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Data;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class PensionIncreases
    {
        [Then(@"I should see the pension increase definitions for the current scheme anchor")]
        public void ThenIShouldSeeThePensionIncreaseDefinitionsForTheCurrentSchemeAnchor()
        {
            var sql = string.Format(@"
                select pincs.*
                from SchemeDetail sd
                inner join PensionSchemes ps on sd.SchemeData_Id = ps.Id
                inner join Bases b on ps.Id = b.PensionSchemeId
                inner join (
	                select Min(AnchorDate) [effectivedate], PensionSchemeId, DisplayName from Bases group by PensionSchemeId, DisplayName
                ) x on ps.Id = x.PensionSchemeId and b.DisplayName = x.DisplayName and b.AnchorDate = x.effectivedate
                inner join PensionIncreaseAssumptions pincs on b.Id = pincs.BasisId
                where
                sd.SchemeName = '{0}'
                and b.DisplayName = '{1}'",
                ScenarioContext.Current.GetSchemeName(),
                ScenarioContext.Current.GetBasisDisplayName());

            var data = new DbHelper().RunQuery(sql);

            var expecteds = new List<PensionIncreaseDefinition>();
            foreach(DataRow row in data.Tables[0].Rows)
                expecteds.Add(new PensionIncreaseDefinition
                    (
                    (int)row["ReferenceNumber"],
                    (string)row["Label"],
                    (double)row["InitialValue"],
                    (bool)row["IsFixed"],
                    (bool)row["IsVisible"]
                    ));

            var json = (ControllerFactory.Get<BaselineController>(ScenarioContext.Current).Get() as JsonResult).Data;
            var current = JsonReader.Get<object>(json, "current");
            var actuals = JsonReader.Get<IEnumerable<PensionIncreaseDefinition>>(current, "pensionIncreases");

            Assert.AreEqual(expecteds.Count, actuals.Count(), "Different number of pension increases");
            foreach (var expected in expecteds)
            {
                Assert.IsTrue(actuals.Where(x => x.Key == expected.Key).Count() == 1);

                var actual = actuals.Single(x => x.Key == expected.Key);

                Assert.AreEqual(expected.Label, actual.Label, "Label");
                Assert.AreEqual(expected.Value, actual.Value, "Value");
                Assert.AreEqual(expected.IsFixed, actual.IsFixed, "IsFixed");
                Assert.AreEqual(expected.IsVisible, actual.IsVisible, "IsVisible");
            }
        }
    }
}
