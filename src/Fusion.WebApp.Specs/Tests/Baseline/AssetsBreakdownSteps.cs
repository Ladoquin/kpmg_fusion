﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Baseline
{
    [Binding]
    public class BaselineAssetBreakdownSteps
    {   
        [Then(@"I see the assets breakdown chart displaying figures from the basis anchor date")]
        public void ThenISeeTheAssetsBreakdownChartDisplayingFiguresFromTheBasisAnchorDate()
        {
            //Don't currently know how to get the buyin at anchordate value from the databse. Awaiting help from Mike to see if
            //it's possible. If not, will have to use our own evolution calcs to get the value. 
//            var sql = string.Format(
//                      @"select af.*
//                        from SchemeDetail sd
//                        inner join PensionSchemes ps on sd.SchemeData_Id = ps.Id
//                        inner join (
//	                        select Min(AnchorDate) [effectivedate], PensionSchemeId, DisplayName from Bases group by PensionSchemeId, DisplayName
//                        ) x on ps.Id = x.PensionSchemeId
//                        inner join SchemeAssets sa on ps.Id = sa.PensionSchemeId and sa.EffectiveDate = x.effectivedate
//                        inner join Assets a on sa.Id = a.SchemeAssetId
//                        inner join AssetFunds af on a.Id = af.AssetId
//                        where
//                        ps.name = '{0}'
//                        and x.DisplayName = '{1}'",
//                      ScenarioContext.Current.GetSchemeName(),
//                      ScenarioContext.Current.GetBasisDisplayName());

//            var data = new DbHelper().RunQuery(sql);

//            var expectedAssetTotal = (double)data.Tables[0].Compute("sum(Amount)", "");

            var evolutionController = ControllerFactory.Get<EvolutionController>(ScenarioContext.Current);

            var tracker = JsonReader.Get<IEnumerable<object>>((evolutionController.Tracker() as JsonResult).Data, "tracker");
            var expectedAssetTotal = JsonReader.Get<double>(tracker.First(), "asset");

            var baselineController = ControllerFactory.Get<BaselineController>(ScenarioContext.Current);

            var json = JsonReader.Get<object>((baselineController.Get() as JsonResult).Data, "current");

            var actualAssetTotal = .0;

            foreach (var category in JsonReader.Get<IEnumerable<BreakdownItem>>(json, "assetsBreakdown"))
                foreach (var assetClass in category.SubItems)
                    foreach (var fund in assetClass.SubItems)
                        actualAssetTotal += fund.Value;

            //just testing the overall total as that is a good indication that the correct values are being shown
            //ie, it's pretty unlikely that another set of fund values are going to add up to the exact same amount

            Assert.That(actualAssetTotal, Is.EqualTo(expectedAssetTotal).Within(Utils.TestPrecision));
        }   
    }
}
