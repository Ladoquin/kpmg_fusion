﻿using Fusion.Shared.Data;
using Fusion.Shared.Mocks;
using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using Fusion.WebApp.Specs.Tests.Scheme;
using NUnit.Framework;
using System;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.Caching
{
    [Binding]
    public class SchemeCacheClearanceSteps
    {
        const string schemeName = "Test Scheme - 13";

        [When(@"I re-import and log in to the scheme multiple times")]
        public void WhenIRe_ImportAndLogInToTheSchemeMultipleTimes()
        {
            for (int i = 0; i < 3; i++)
            {
                importScheme();

                viewSchemeCalcResults();
            }
        }

        [Then(@"calculations are cached for the most recent scheme only")]
        public void ThenCalculationsAreCachedForTheMostRecentSchemeOnly()
        {
            var db = new DbHelper();

            var count = db.RunQuery(
                @"select psc.*
                  from PensionSchemes ps
                  inner join PensionSchemeCache psc on ps.Id = psc.PensionSchemeId
                  where ps.Name = '" + schemeName + "'").Tables[0].Rows.Count;

            Assert.AreEqual(1, count, "Pension scheme cache entries");

            count = db.RunQuery(
                @"select bc.*
                  from PensionSchemes ps
                  inner join Bases b on ps.Id = b.PensionSchemeId
                  inner join BasisCache bc on b.MasterBasisId = bc.MasterBasisId
                  where ps.Name = '" + schemeName + "'").Tables[0].Rows.Count;

            Assert.AreEqual(1, count, "Basis cache entries"); //we only view the Technical Provisions basis in this test
        }

        void importScheme()
        {
            var model = SchemeSharedTests.GetAdditSchemeViewModel(schemeName, true);

            var controller = ControllerFactory.Get<SchemeController>(ScenarioContext.Current);

            (controller.ControllerContext.HttpContext.Request as MockHttpRequest).AddRequestItem("update", "true");

            var result = controller.Edit(model) as ViewResult;

            if ((result.Model as ManageSchemeViewModel).AdditSchemeViewModel.Result != AdditSchemeResult.EditSchemeSuccess)
            {
                throw new Exception("Scheme import failed, test aborted");
            }
        }

        void viewSchemeCalcResults()
        {
            ControllerFactory.Get<AccountController>(ScenarioContext.Current).ChangeScheme(
                new SchemeNameUpdateModel
                {
                    SchemeName = schemeName
                });

            new BasisSteps().WhenIViewTheBasis("Technical Provisions");

            ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).Tracker();

            ControllerFactory.Get<JsonController>(ScenarioContext.Current).InvestmentStrategy();
        }
    }
}
