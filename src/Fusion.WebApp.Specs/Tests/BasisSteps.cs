﻿namespace Fusion.WebApp.Specs.Tests
{
    using Fusion.WebApp.Controllers;
    using System.Linq;
    using TechTalk.SpecFlow;

    [Binding]
    public class BasisSteps
    {
        [When(@"I view the (.*) basis")]
        public void WhenIViewTheBasis(string basisDisplayName)
        {
            var homeController = ControllerFactory.Get<HomeController>(ScenarioContext.Current);

            var masterbasisId = homeController.Scheme.PensionScheme.GetBasesIdentifiers().First(x => x.Name == basisDisplayName).Id;

            homeController.SetBasis(masterbasisId);

            ScenarioContext.Current.SetBasisDisplayName(basisDisplayName);
        }
    }
}
