﻿namespace Fusion.WebApp.Specs.Tests
{
    public enum PageSectionType
    {
        None,
        AssetAssumptions,
        InvestmentStrategyAssumptions
    }
}
