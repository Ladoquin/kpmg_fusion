﻿using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.CurveData
{
    [Binding]
    public class FillMissingCurveDataSteps
    {
        [Then(@"(.*) data points are present in the database")]
        public void ThenDataPointsArePresentInTheDatabase(int dataPointCount)
        {
            var curveDataPointCount = ScenarioContext.Current.Get<int>("CurveDataRowCount");
            Assert.IsTrue(curveDataPointCount == dataPointCount);
        }
    }
}
