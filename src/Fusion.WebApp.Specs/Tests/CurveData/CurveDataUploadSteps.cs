﻿using FlightDeck.DomainShared;
using Fusion.Shared.Data;
using Fusion.Shared.Mocks;
using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using NUnit.Framework;
using System;
using System.IO;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.CurveData
{
    [Binding]
    public class CurveDataUploadSteps
    {
        [Given(@"I have a curve data file with a valid schema")]
        public void GivenIHaveACurveDataFileWithAValidSchema()
        {
            var curveFile = new FileStream(Path.Combine(EnvironmentSettings.TestDataPath, "integration_tests_resources", "curve_files", "curves_valid.xml"), FileMode.Open);
            ScenarioContext.Current.Set(curveFile, "curveFile");
        }

        [When(@"I upload the curve data file")]
        public void WhenIUploadTheCurveDataFile()
        {
            var curveFile = ScenarioContext.Current.Get<FileStream>("curveFile");
            var contentData = new byte[curveFile.Length];
            curveFile.Read(contentData, 0, Convert.ToInt32(curveFile.Length));

            var model = new CurveImportViewModel
            {
                ImportDetail = new CurveImportDetail(),
                ImportFile = new MockHttpPostedFile(contentData, "text/xml", "test-curve-file.xml")
            };

            var controller = ControllerFactory.Get<CurveDataController>(ScenarioContext.Current);
            var result = controller.Save(model);

            curveFile.Close();
            curveFile.Dispose();
            ScenarioContext.Current.Set(result);
        }

        [When(@"Curve data has previously been uploaded")]
        public void WhenCurveDataHasPreviouslyBeenUploaded()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "CurveData", "SetUpPreviousUpload.sql"));
        }

        [Then(@"the curve data file is accepted")]
        public void ThenTheCurveDataFileIsAccepted()
        {
            var result = ScenarioContext.Current.Get<ActionResult>() as ViewResult;
            Assert.IsNotNull(result);
            var model = result.Model as CurveImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Success, model.Result);
        }

        [Then(@"The curve data is present on the database")]
        public void ThenTheCurveDataIsPresentOnTheDatabase()
        {
            var dataset = new DbHelper().RunQuery("select distinct Maturity from CurveDataSpotRates where CurveDataId = (select Id from CurveData where IndexName = 'TEST CURVE INDEX')");

            ScenarioContext.Current.Set(dataset.Tables[0].Rows.Count, "CurveDataRowCount");

            Assert.NotNull(dataset);
            Assert.True(dataset.Tables[0].Rows.Count > 0);
        }

        [Then(@"The new curve data overwrites the existing curve data")]
        public void ThenTheNewCurveDataOverwritesTheExisitingCurveData()
        {
            var dataset = new DbHelper().RunQuery("select * from CurveDataSpotRates where CurveDataId = (select Id from CurveData where IndexName = 'TEST CURVE INDEX') and Maturity = 1 and Date = '2014-10-01'");

            Assert.NotNull(dataset);
            Assert.AreEqual(1, dataset.Tables[0].Rows.Count);
            Assert.That(dataset.Tables[0].Rows[0]["Rate"], Is.Not.EqualTo(0.1).Within(Utils.Precision));
        }

        [Then(@"The new curve data doesn't delete existing curve data")]
        public void ThenTheNewCurveDataDoesnTDeleteExisitingCurveData()
        {
            var dataset = new DbHelper().RunQuery("select * from CurveDataSpotRates where CurveDataId = (select Id from CurveData where IndexName = 'TEST CURVE INDEX') and Maturity = 1 and Date = '2009-12-01'");

            Assert.NotNull(dataset);
            Assert.AreEqual(1, dataset.Tables[0].Rows.Count);
            Assert.That(dataset.Tables[0].Rows[0]["Rate"], Is.EqualTo(0.1).Within(Utils.Precision));
        }

        [AfterScenario("curveDataUpload")]
        public static void TearDown()
        {
            new DbHelper().RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "CurveData", "TearDown.sql"));
        }
    }
}
