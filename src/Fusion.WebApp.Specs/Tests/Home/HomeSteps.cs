﻿namespace Fusion.WebApp.Specs.Tests.Home
{
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class HomeSteps
    {
        [Then(@"I am returned the json relevant to the Home page")]
        public void ThenIAmReturnedTheJsonRelevantToTheHomePage()
        {
            var controller = ControllerFactory.Get<HomeController>(ScenarioContext.Current);

            var result = controller.Get() as ActionResult;

            Assert.NotNull(result);

            var json = (result as JsonResult).Data;

            Assert.NotNull(json);

            var basis = JsonReader.Get<object>(json, "basis");

            Assert.Greater(JsonReader.Get<double>(json, "date"), 0);
            Assert.NotNull(basis);
            Assert.Greater(JsonReader.Get<int>(basis, "masterBasisId"), 0);
            Assert.IsNotNullOrEmpty(JsonReader.Get<string>(basis, "name"));
            Assert.IsNotNullOrEmpty(JsonReader.Get<string>(basis, "type"));
            Assert.Greater(JsonReader.Get<int>(basis, "typeId"), 0);
            Assert.NotNull(JsonReader.Get<double>(json, "balance"));
            Assert.NotNull(JsonReader.Get<double>(json, "fundingTarget"));
            Assert.NotNull(JsonReader.Get<object>(json, "syntheticAssetInfo"));
            Assert.NotNull(JsonReader.Get<IEnumerable<BreakdownItem>>(json, "assetsBreakdown"));
            Assert.NotNull(JsonReader.Get<IEnumerable<BreakdownItem>>(json, "liabilitiesBreakdown"));

            var evolution = JsonReader.Get<object>(json, "evolution");

            Assert.NotNull(evolution);
            Assert.NotNull(JsonReader.Get<object>(evolution, "d0"));
            Assert.NotNull(JsonReader.Get<object>(evolution, "d1"));
            Assert.NotNull(JsonReader.Get<object>(evolution, "m1"));
            Assert.NotNull(JsonReader.Get<object>(evolution, "m3"));
            Assert.NotNull(JsonReader.Get<object>(evolution, "m6"));
            Assert.NotNull(JsonReader.Get<object>(evolution, "y1"));

            var ldi = JsonReader.Get<object>(json, "ldiData");

            Assert.NotNull(ldi);
            Assert.Greater(JsonReader.Get<double>(ldi, "interestRate"), 0);
            Assert.Greater(JsonReader.Get<double>(ldi, "inflation"), 0);
        }
    }
}
