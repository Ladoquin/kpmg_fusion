﻿namespace Fusion.WebApp.Specs.Tests.SchemeScenarios.Unfunded
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;

    [Binding]
    public class UnfundedSteps
    {
        [Then(@"No asset breakdown data is returned")]
        public void ThenNoAssetBreakdownDataIsReturned()
        {
            IEnumerable<BreakdownItem> assetsBreakdown = null;

            switch (ScenarioContext.Current.GetCurrentPage())
            {
                case PageType.Home:
                    assetsBreakdown = JsonReader.Get<IEnumerable<BreakdownItem>>(
                        (ControllerFactory.Get<HomeController>(ScenarioContext.Current).Get() as JsonResult).Data, "assetsBreakdown");
                    break;
                case PageType.Baseline:
                    assetsBreakdown = 
                        JsonReader.Get<IEnumerable<BreakdownItem>>(
                            JsonReader.Get<object>(
                                (ControllerFactory.Get<BaselineController>(ScenarioContext.Current).Get() as JsonResult).Data, "current"), "assetsBreakdown");
                    break;
                case PageType.Evolution:
                    assetsBreakdown = JsonReader.Get<IEnumerable<BreakdownItem>>(
                        (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data, "assetsBreakdown");
                    break;
                default:
                    Assert.Fail("Unexpected test case");
                    break;
            }

            Assert.True(assetsBreakdown.IsNullOrEmpty());
        }

        [Then(@"All asset values in the mini-evolution chart are zero")]
        public void ThenAllAssetValuesInTheMini_EvolutionChartAreZero()
        {
            var evolChart = 
                JsonReader.Get<object>(
                    (ControllerFactory.Get<HomeController>(ScenarioContext.Current).Get() as JsonResult).Data,
                        "evolution");

            foreach(var prop in new string[] { "d0", "d1", "m1", "m3", "m6", "y1" })
                Assert.AreEqual(0, JsonReader.Get<ValueDifference>(JsonReader.Get<object>(evolChart, prop), "Asset").Value);
        }

        [Then(@"The balance total equals the total liabilities")]
        public void ThenTheBalanceTotalEqualsTheTotalLiabilities()
        {
            var json = (ControllerFactory.Get<HomeController>(ScenarioContext.Current).Get() as JsonResult).Data;

            var balance = JsonReader.Get<double>(json, "balance");
            var liabilityTotal = JsonReader.Get<List<BreakdownItem>>(json, "liabilitiesBreakdown").Sum(x => x.Value);

            Assert.That(liabilityTotal, Is.EqualTo(-balance).Within(Utils.TestPrecision));
        }

        [Then(@"No asset growth data is returned")]
        public void ThenNoAssetGrowthDataIsReturned()
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;

            var assetGrowth = JsonReader.Get<IEnumerable<object>>(data, "assetsGrowth");

            Assert.True(assetGrowth.IsNullOrEmpty());
        }

        [Then(@"All asset values in the Surplus Analysis chart are zero")]
        public void ThenAllAssetValuesInTheSurplusAnalysisChartAreZero()
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).AnalysisOfSurplus() as JsonResult).Data;

            Assert.AreEqual(0, JsonReader.Get<double>(data, "Contributions"));
            Assert.AreEqual(0, JsonReader.Get<double>(data, "AssetReturn"));
            Assert.AreEqual(0, JsonReader.Get<double>(data, "AssetExperience"));
        }

        [Then(@"The showAssets flag is set to false")]
        public void ThenTheShowAssetsFlagIsSetToFalse()
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;

            var showAssets = JsonReader.Get<bool>(data, "showAssets");

            Assert.False(showAssets);
        }

        [Then(@"funded flag is set to false")]
        public void ThenFundedFlagIsSetToFalse()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).AnalysisPlan("[\"liabilities\"]") as JsonResult).Data;

            var funded = JsonReader.Get<bool>(data, "funded");

            Assert.False(funded);
        }

        [Then(@"The asset value in the Balance Sheet chart is zero")]
        public void ThenTheAssetValueInTheBalanceSheetChartIsZero()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).AccountingData() as JsonResult).Data;

            var after = JsonReader.Get<BalanceData>(JsonReader.Get<object>(data, "balance"), "after");

            Assert.AreEqual(0, after.Assets);
        }

        [Then(@"The asset value in the Funding Position chart is zero")]
        public void ThenTheAssetValueInTheFundingPositionChartIsZero()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).CashLens() as JsonResult).Data;

            var after = JsonReader.Get<object>(JsonReader.Get<object>(data, "fundingLevelData"), "after");

            Assert.AreEqual(0, JsonReader.Get<double>(after, "Assets"));
        }

        [Then(@"Employee rate is zero")]
        public void ThenEmployeeRateIsZero()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).FutureBenefits() as JsonResult).Data;

            Assert.AreEqual(0, JsonReader.Get<double>(data, "employeeRate"));
        }

        [Then(@"No recovery plan data is returned")]
        public void ThenNoRecoveryPlanDataIsReturned()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).CashLens() as JsonResult).Data;

            var before = JsonReader.Get<object>(JsonReader.Get<object>(data, "recoveryPlanData"), "before");

            Assert.Null(before);
        }

        [Then(@"All asset values in the VaRWaterfall chart are zero")]
        public void ThenAllAssetValuesInTheVaRWaterfallChartAreZero()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).GetVarWaterfallData() as JsonResult).Data;

            var after = JsonReader.Get<object>(data, "after");

            Assert.AreEqual(0, JsonReader.Get<double>(after, "InterestInflationHedging"));
            Assert.AreEqual(0, JsonReader.Get<double>(after, "Hedge"));
            Assert.AreEqual(0, JsonReader.Get<double>(after, "Credit"));
            Assert.AreEqual(0, JsonReader.Get<double>(after, "Equity"));            
            Assert.AreEqual(0, JsonReader.Get<double>(after, "OtherGrowth"));
        }

        [Then(@"Employee rate and Deficit recovery contributions are zero")]
        public void ThenEmployeeRateAndDeficitRecoveryContributionsAreZero()
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).EvolutionAssumptionsPanel() as JsonResult).Data;

            var evolutionSchemeData = JsonReader.Get<EvolutionSchemeData>(data, "schemeData");

            Assert.AreEqual(0, evolutionSchemeData.RegularEmployee);
            Assert.AreEqual(0, evolutionSchemeData.Payment);
        }

        [Then(@"The balance total and deficit values equal the total liabilities")]
        public void ThenTheBalanceTotalAndDeficitValuesEqualTheTotalLiabilities()
        {
            var data = (ControllerFactory.Get<EvolutionController>(ScenarioContext.Current).Tracker() as JsonResult).Data;

            //jquery currently doing a lot of work on this page, so can only really check that the tracker's last item contain zero assets

            var tracker = JsonReader.Get<IEnumerable<dynamic>>(data, "tracker");
            var balanceItem = tracker.OrderBy(x => DateConverter.GetJsonDate(JsonReader.Get<double>(x, "date"))).Last();

            Assert.AreEqual(0, JsonReader.Get<double>(balanceItem, "asset"));            
        }

        [Then(@"The asset values on the Funding Progression chart are zero")]
        public void ThenTheAssetValuesOnTheFundingProgressionChartAreZero()
        {
            var data = (ControllerFactory.Get<JsonController>(ScenarioContext.Current).CashLens() as JsonResult).Data;

            var fundingLevelData = JsonReader.Get<object>(JsonReader.Get<object>(data, "fundingLevelData"), "after");

            Assert.AreEqual(0, JsonReader.Get<double>(fundingLevelData, "Assets"));
        }
    }
}
