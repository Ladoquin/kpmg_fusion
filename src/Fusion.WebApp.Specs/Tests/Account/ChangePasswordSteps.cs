﻿namespace Fusion.WebApp.Specs.Tests.Account
{
    using Fusion.Shared.Data;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using NUnit.Framework;
    using System;
    using System.Threading;
    using System.Web.Mvc;
    using TechTalk.SpecFlow;

    [Binding]
    public class ChangePasswordSteps
    {
        //dummy ci change. 7
        private string salt = "0x93";
        private string password1 = "0xB9BC0837528B8FFFA6C97647F31D64346468BCD2"; //Password1! with the above salt
        private string password2 = "0x784FBAF48A24B1AD7924787ED58D6C88FCE8C900"; //Password2! with the above salt
        private DateTime createDate = new DateTime(2015, 1, 1);

        [Given(@"the password I am going to change to is not in my history")]
        public void GivenThePasswordIAmGoingToChangeToIsNotInMyHistory()
        {
            new DbHelper().RunCommand("insert into UserProfilePassword (UserId, Salt) " +
                "select UserId, convert(varbinary(128), " + salt + ") from UserProfile where UserName = 'fusionapptestuserclient'");
        }

        [Given(@"the password I am going to change to is already in my history")]
        public void GivenThePasswordIAmGoingToChangeToIsAlreadyInMyHistory()
        {
            new DbHelper().RunCommand("insert into UserProfilePassword (UserId, Salt) " +
                "select UserId, convert(varbinary(128), " + salt + ") from UserProfile where UserName = 'fusionapptestuserclient'");
            new DbHelper().RunCommand("insert into UserProfilePasswordHistory (UserId, Password, CreateDate) " +
                "select UserId, convert(varbinary(128), " + password2 + "), '" + createDate.ToString("yyyy-MM-dd") + "' from UserProfile where UserName = 'fusionapptestuserclient'");
        }

        [When(@"I change my password")]
        public void WhenIChangeMyPassword()
        {
            var controller = ControllerFactory.Get<AccountController>(ScenarioContext.Current);

            var model = new PasswordUpdateModel { OldPassword = "Password1!", NewPassword = "Password2!" };

            var result = controller.ChangePassword(model);

            ScenarioContext.Current.Set(result, "result");
        }

        [Given(@"I have changed my password five times")]
        public void GivenIHaveChangedMyPasswordFiveTimes()
        {
            var passwordAppendage = 2;
            for (int i = 0; i < 5; i++)
            {
                var controller = ControllerFactory.Get<AccountController>(ScenarioContext.Current);
                var model = new PasswordUpdateModel { OldPassword = string.Format("Password{0}!", (passwordAppendage - 1).ToString()), NewPassword = string.Format("Password{0}!", passwordAppendage.ToString()) };
                var result = controller.ChangePassword(model);
                passwordAppendage++;
            }
        }

        [Then(@"the new password should be saved")]
        public void ThenTheNewPasswordShouldBeSaved()
        {
            var result = ScenarioContext.Current["result"];

            Assert.IsTrue(result is RedirectToRouteResult);

            Assert.True(IsNewPasswordFound());
        }

        [Then(@"the new password should not be saved")]
        public void ThenTheNewPasswordShouldNotBeSaved()
        {
            var result = ScenarioContext.Current["result"] as ViewResult;

            Assert.NotNull(result);
            Assert.IsTrue((result.Model as AccountUpdateModel).PasswordUpdateModel.Result.HasFlag(ResultEnum.Failure));

            Assert.False(IsNewPasswordFound());
        }

        [When(@"I change my password back to my first password")]
        public void WhenIChangeMyPasswordBackToMyFirstPassword()
        {
            var controller = ControllerFactory.Get<AccountController>(ScenarioContext.Current);

            var model = new PasswordUpdateModel { OldPassword = "Password6!", NewPassword = "Password1!" };

            var result = controller.ChangePassword(model);

            ScenarioContext.Current.Set(result, "result");
        }

        [Then(@"the first password is saved as the most recent")]
        public void ThenTheFirstPasswordIsSavedAsTheMostRecent()
        {
            // requires it's own check because the step 'ThenTheNewPasswordShouldBeSaved' would always return true

            var data = new DbHelper().RunQuery(
                @"select h.* from UserProfilePasswordHistory h
                  inner join UserProfile u on h.UserId = u.UserId
                  where u.UserName = 'fusionapptestuserclient'
                  and h.CreateDate = (
                  select max(h2.CreateDate) from UserProfilePasswordHistory h2 where h2.UserId = (select u2.UserId from UserProfile u2 where u2.UserName = 'fusionapptestuserclient'))
                  and h.Password = convert(varbinary(128), " + password1 + ")");

            Assert.AreEqual(1, data.Tables[0].Rows.Count);
        }

        private bool IsNewPasswordFound()
        {
            var query = @"select h.*
                  from UserProfilePasswordHistory h
                  inner join UserProfile p on h.UserId = p.UserId 
                  where p.UserName = 'fusionapptestuserclient'
                  and h.CreateDate > '" + createDate.ToString("yyyy-MM-dd") + "'";

            var data = new DbHelper().RunQuery(query);

            log4net.LogManager.GetLogger(this.GetType()).Info("password search query: " + query);

            return data.Tables[0].Rows.Count > 0;
        }

    }
}