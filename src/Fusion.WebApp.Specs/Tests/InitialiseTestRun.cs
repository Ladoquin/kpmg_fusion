﻿namespace Fusion.WebApp.Specs.Tests
{
    using FlightDeck.Maintenance;
    using Fusion.Shared.Data;
    using System;
    using System.IO;
    using TechTalk.SpecFlow;

    [Binding]
    public class InitialiseTestRun
    {
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            var db = new DbHelper();

            // create function on db server that retrieves default backup location
            db.RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Shared", "fn_SQLServerBackupDir-DROP.sql"), DbHelper.Db.Master);
            db.RunScript(Path.Combine(Environment.CurrentDirectory, "DbScripts", "Shared", "fn_SQLServerBackupDir.sql"), DbHelper.Db.Master);
            // put database in single user mode to help prevent locking when restoring
            db.RunCommand("alter database FusionDBTest set single_user with rollback immediate", DbHelper.Db.Master);
            // get default backup folder
            var backupDir = (string)db.RunQuery("select dbo.fn_SQLServerBackupDir() [BackupDir]", DbHelper.Db.Master).Tables[0].Rows[0]["BackupDir"];
            // restore database
            var restoreSql = string.Format(
                "restore database FusionDBTest from disk = '{0}' with replace, move 'FusionDBTest' to '{1}', move 'FusionDBTest_Log' to '{2}'",
                    new FileInfo(Path.Combine(EnvironmentSettings.TestDBPath, "FusionDBTest.bak")).FullName,
                    Path.Combine(backupDir, "FusionDBTest_Data.mdf"),
                    Path.Combine(backupDir, "FusionDBTest_Log.ldf"));
            db.RunCommand(restoreSql, DbHelper.Db.Master);
            // put database back in to multi user mode
            db.RunCommand("alter database FusionDBTest set multi_user", DbHelper.Db.Master);
            // grant test user required access
            db.RunCommand("create user FusionTestRunner from login FusionTestRunner");
            db.RunCommand("exec sp_addrolemember 'db_datareader', FusionTestRunner");
            db.RunCommand("exec sp_addrolemember 'db_datawriter', FusionTestRunner");

            var vm = new DbVersionManager();
            vm.Initialize();
            vm.Update();
        }
    }
}
