﻿using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests.VaR
{
    [Binding]
    public class VaRChartDataSteps
    {
        [When(@"I Choose the cash lens")]
        public void WhenIChooseTheCashLens()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);

            var result = controller.GetVarWaterfallData() as ActionResult;

            Assert.NotNull(result);

            dynamic json = (result as JsonResult).Data;
      
          //  var varData = JsonReader.Get<dynamic>(json, "after");

            ScenarioContext.Current.Remove("jsonResult");
            ScenarioContext.Current.Add("jsonResult", json);
        }

        [Then(@"I see the VaR Waterfall chart with a Hedging of interest and inflation bar")]
        public void ThenISeeTheVaRWaterfallChartWithAHedgingOfInterestAndInflationBar()
        {
            var jsonResult = ScenarioContext.Current.Get<dynamic>("jsonResult"); 

            var varDataForChart = JsonReader.Get<dynamic>(jsonResult, "after");

            var interestInflationHedging = JsonReader.Get<double>(varDataForChart, "InterestInflationHedging");

            Assert.IsNotNull(interestInflationHedging);
        }
    }
}
