﻿using Fusion.Shared.Web;
using Fusion.WebApp.Controllers;
using NUnit.Framework;
using System;
using System.Web.Mvc;
using TechTalk.SpecFlow;

namespace Fusion.WebApp.Specs.Tests
{
    [Binding]
    public class VaRWaterfallInputsSteps
    {
        [When(@"I change the CI VaR Waterfall input")]
        public void WhenIChangeTheCIVaRWaterfallInput()
        {
            var ci = 0.995;//0.95 is the default so use 0.995

            setInitialInterestInflationHedging();

            ScenarioContext.Current.Add("nonDefaultCI", ci);

            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);
            var result = controller.SetVarWaterfallData(ci, 3) as ActionResult;

            setPostChangeInterestInflation(result);
        }

        [When(@"I change the Time VaR Waterfall input")]
        public void WhenIChangeTheTimeVaRWaterfallInput()
        {

            var preTimeChangeJson = ScenarioContext.Current.Get<dynamic>("jsonResult");
            ScenarioContext.Current.Add("preTimeChangeJson", preTimeChangeJson);

            var time = 4;//3 is the default so use 4 

            setInitialInterestInflationHedging();

            ScenarioContext.Current.Add("nonDefaultTime", time);

            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);
            var result = controller.SetVarWaterfallData(0.95, time) as ActionResult;

            setPostChangeInterestInflation(result);

            ScenarioContext.Current.Add("afterTimeChangeResult", result);
        }

        private void setInitialInterestInflationHedging()
        {
            //initial data
            var jsonResult = ScenarioContext.Current.Get<dynamic>("jsonResult");

            var varDataForChart = JsonReader.Get<dynamic>(jsonResult, "after");

            //picking InterestInflationHedging to compare rather than comparing all
            var preChange_InterestInflationHedging = JsonReader.Get<double>(varDataForChart, "InterestInflationHedging");
            ScenarioContext.Current.Add("preChange_InterestInflationHedging", preChange_InterestInflationHedging);
        }

        private void setPostChangeInterestInflation(ActionResult result)
        {
            dynamic json = (result as JsonResult).Data;
            var varData = JsonReader.Get<dynamic>(json, "after");

            var postChange_InterestInflationHedging = JsonReader.Get<double>(varData, "InterestInflationHedging");
            ScenarioContext.Current.Add("postChange_InterestInflationHedging", postChange_InterestInflationHedging);
        }

        [Then(@"I see new chart data rendered to the VaR Waterfall chart")]
        public void ThenISeeNewChartDataRenderedToTheVaRWaterfallChart()
        {
            var preVal = ScenarioContext.Current.Get<double>("preChange_InterestInflationHedging");
            var postVal = ScenarioContext.Current.Get<double>("postChange_InterestInflationHedging");

            Assert.AreNotEqual(preVal, postVal);
        }

        [Then(@"I see that the CI VaR Waterfall input is still showing the previous setting")]
        public void ThenISeeThatTheCIVaRWaterfallInputIsStillShowingThePreviousSetting()
        {
            var nonDefaultCI = ScenarioContext.Current.Get<double>("nonDefaultCI");

            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);
            var result = controller.GetVarWaterfallData() as ActionResult;

            dynamic json = (result as JsonResult).Data;
            var displayOptions = JsonReader.Get<dynamic>(json, "displayOptions");
            var ci = JsonReader.Get<double>(displayOptions, "ci");

            Assert.AreEqual(nonDefaultCI, ci);
        }

        [Then(@"I see that the Time VaR Waterfall input is still showing the previous setting")]
        public void ThenISeeThatTheTimeVaRWaterfallInputIsStillShowingThePreviousSetting()
        {
            var nonDefaultTime = ScenarioContext.Current.Get<int>("nonDefaultTime");

            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);
            var result = controller.GetVarWaterfallData() as ActionResult;

            dynamic json = (result as JsonResult).Data;
            var displayOptions = JsonReader.Get<dynamic>(json, "displayOptions");
            var time = JsonReader.Get<int>(displayOptions, "time");

            Assert.AreEqual(nonDefaultTime, time);
       }


        [Then(@"I see Before values only rendered on the chart tooltips")]
        public void ThenISeeBeforeValuesOnlyRenderedOnTheChartTooltips()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);
            var result = controller.GetVarWaterfallData() as ActionResult;

            //only after values are returned i.e. before any changes are made the after results are rendered as the before tool tip values
            dynamic json = (result as JsonResult).Data;
            var after = JsonReader.Get<dynamic>(json, "after");

            Assert.IsNotNull(after);

            var before = JsonReader.Get<object>(json, "before");

            Assert.IsNull(before);
        }

        [Then(@"I see Before and After values rendered on the chart tooltips")]
        public void ThenISeeBeforeAndAfterValuesRenderedOnTheChartTooltips()
        {
            var controller = ControllerFactory.Get<JsonController>(ScenarioContext.Current);
            var result = controller.GetVarWaterfallData() as ActionResult;

            //only after values are returned i.e. before any changes are made the after results are rendered as the before tool tip values
            dynamic json = (result as JsonResult).Data;
            var after = JsonReader.Get<dynamic>(json, "after");

            Assert.IsNotNull(after);

            var before = JsonReader.Get<object>(json, "before");

            Assert.IsNotNull(before);
        }

        [Then(@"Both the before and after values should change")]
        public void ThenBothTheBeforeAndAfterValuesShouldChange()
        {
            var afterTimeChangeResult = ScenarioContext.Current.Get<dynamic>("afterTimeChangeResult");
            dynamic afterJson = (afterTimeChangeResult as JsonResult).Data;
            var afterValues = JsonReader.Get<dynamic>(afterJson, "after");
            var beforeValues = JsonReader.Get<dynamic>(afterJson, "before");

            var preJson = ScenarioContext.Current.Get<dynamic>("preTimeChangeJson");
            var preAfterValues = JsonReader.Get<dynamic>(preJson, "after");
            var preBeforeValues = JsonReader.Get<dynamic>(preJson, "before");

            Assert.AreNotEqual(afterValues.ToString(), preAfterValues.ToString());
            Assert.AreNotEqual(beforeValues.ToString(), preBeforeValues.ToString());
        }
    }
}
