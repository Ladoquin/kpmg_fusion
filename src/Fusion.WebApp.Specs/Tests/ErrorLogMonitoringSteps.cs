﻿namespace Fusion.WebApp.Specs.Tests
{
    using Fusion.Shared.Data;
    using NUnit.Framework;
    using TechTalk.SpecFlow;
    
    [Binding]
    public class ErrorLogMonitoringSteps
    {
        /// <summary>
        /// Get the current error log count so that it can be comapred to the GetCurrentErrorLogCountAfter result.
        /// An increase in the count should fail the test.
        /// </summary>
        [BeforeScenario]
        public static void GetCurrentErrorLogCountBefore()
        {
            ScenarioContext.Current.Set(GetLoggedErrorCount(), "loggedErrorCount");
        }

        [Then(@"No errors are logged")]
        public static void NoErrorsAreLogged()
        {
            var initialErrorCount = ScenarioContext.Current.Get<int>("loggedErrorCount");
            Assert.IsTrue(initialErrorCount == GetLoggedErrorCount(), "Expected no error log entries.");
        }

        [AfterScenario("checkForErrors")]
        public static void NoErrorsAreLoggedX()
        {
            var initialErrorCount = ScenarioContext.Current.Get<int>("loggedErrorCount");
            Assert.IsTrue(initialErrorCount == GetLoggedErrorCount(), "Expected no error log entries.");
        }

        private static int GetLoggedErrorCount()
        {
            var ds = new DbHelper().RunQuery("select count(*) from Log where Level = 'ERROR'", DbHelper.Db.Log);
            int loggedErrorCount = (int)ds.Tables[0].Rows[0][0];
            return loggedErrorCount;
        }

        [Then(@"the exception '(.*)' is logged")]
        public void ThenTheExceptionIsLogged(string exceptionMessage)
        {
            var logs = new DbHelper().RunQuery("select * from [Log] where [Exception] like '%" + exceptionMessage + "%'", DbHelper.Db.Log);

            Assert.IsNotNull(logs);
            Assert.IsTrue(logs.Tables[0].Rows.Count > 0);
        }
    }
}
