﻿namespace Fusion.WebApp.Specs.Tests
{
    using System;
    
    class DateConverter
    {
        public static string ConvertToJsonDate(DateTime date)
        {
            var ts = new TimeSpan(date.Ticks).Add(-new TimeSpan(JsonHelper.UnixEpoch.Ticks)).TotalMilliseconds.ToString();

            return ts;
        }

        public static DateTime GetJsonDate(double date)
        {
            return JsonHelper.UnixEpoch.AddMilliseconds(date);
        }
    }
}
