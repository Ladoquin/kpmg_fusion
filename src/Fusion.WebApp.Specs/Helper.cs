﻿namespace Fusion.WebApp.Specs
{
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using FlightDeck.Services;
    using Fusion.WebApp.Controllers;
    using Moq;
    using System;
    using System.Linq;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    
    public class Helper
    {
        public const string testDataPath = @"..\..\..\..\test_data";

        public static T SetController<T>(HttpContextBase httpContext) where T : ControllerBase, new()
        {
            var controller = new T();

            controller.ControllerContext = new ControllerContext()
            {
                Controller = controller,
                RequestContext = new RequestContext(httpContext, new RouteData())
            };

            return controller;
        }

        public static void SetBasis(FusionBaseController controller, BasisType type)
        {
            controller.Scheme.PensionScheme.SwitchBasis(
                controller.Scheme.PensionScheme.GetBasesIdentifiers().First(b => b.Type == type).Id);
        }

        private static IPrincipal _mockUser;
        public static IPrincipal GetMockTestUser()
        {
            if(_mockUser == null)
            { 
                Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();
                var mockIdentity = new GenericIdentity(Helper.TestUsername);
                mockPrincipal.Setup(x => x.Identity).Returns(mockIdentity);
                _mockUser = mockPrincipal.Object;
            }
            return _mockUser;
        }

        private static PensionSchemeService _schemeService;
        public static PensionSchemeService GetSchemeService()
        {
            if(_schemeService == null)
            {
                Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();
                var mockIdentity = new GenericIdentity(Helper.TestUsername);
                mockPrincipal.Setup(x => x.Identity).Returns(mockIdentity);
                Mock<Lazy<IFinancialIndexRepository>> mockRepo = new Mock<Lazy<IFinancialIndexRepository>>();
                _schemeService = new PensionSchemeService(mockPrincipal.Object, mockRepo.Object);
            }
            return _schemeService;
        }

        public const string TestUsername = "fusionapptestuser";
        public const string TestPassword = "Password1!";
    }
}
