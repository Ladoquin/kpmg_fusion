﻿@web @schemeCache
Feature: SchemeCacheClearance
	In order to maintain performance
	As a KPMG Admin
	I want the scheme cache to cleared down when redundant

@checkForErrors @schemeImported
Scenario: Importing a new version of a scheme deletes the cache for all previous versions
	Given I am an Admin user
	And I have imported the Test Scheme - 13
	When I re-import and log in to the scheme multiple times
	Then calculations are cached for the most recent scheme only
