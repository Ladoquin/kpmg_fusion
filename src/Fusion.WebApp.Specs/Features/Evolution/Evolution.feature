﻿@evolution @web
Feature: Evolution
	In order to assess my scheme
	As any user
	I want to view the evolution of my scheme

Scenario: View the evolution of liabilities chart
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Evolution page
	Then I see the evolution of liabilities chart
	And No errors are logged

Scenario: View the correct precision on evolution of funding level % liabilities chart
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Evolution page
	Then I see that the tooltips on the funding level % chart displays the % values with the precision of 1 decimal place(s)