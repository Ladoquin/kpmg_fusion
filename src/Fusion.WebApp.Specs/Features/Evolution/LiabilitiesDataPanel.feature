﻿@evolution @web
Feature: LiabilitiesDataPanel
	In order to assess my scheme
	As any user
	I want to view the evolution of my scheme

@checkForErrors
Scenario: View the liability assumption information for an attribution period that does not span across a basis effective date
	Given I am a Client user
	And My default scheme is Test Scheme - 01
	When I view the Evolution page
	And I view the Funding basis
	And I set the Attribution Period Start Date to 31/03/2013
	And I set the Attribution Period End Date to 31/05/2013
	Then I do not see a change in the commutation of allowance value
	And I do not see a change in the life expectancy values

@checkForErrors
Scenario: View the liability assumption information for an attribution period that spans accross a basis effective date
	Given I am a Client user
	And My default scheme is Test Scheme - 01
	When I view the Evolution page
	And I view the Funding basis
	And I set the Attribution Period Start Date to 31/03/2013
	And I set the Attribution Period End Date to 31/03/2014
	Then I see a change in the commutation of allowance value
	And I see a change in the life expectancy values

@checkForErrors
Scenario: View the liability assumption information for a period either side of a basis effective date
	Given I am a Client user
	And My default scheme is Test Scheme - 01
	When I view the Evolution page
	And I view the Funding basis
	And I set the Attribution Period End Date to 31/05/2013
	And I note the commutation of allowance label
	And I note the life expectancy labels
	And I set the Attribution Period End Date to 31/03/2014
	Then I see a change in the life expectancy labels
