﻿@web @evolution
Feature: AssetsDataPanel
	In order to assess my scheme
	As any user
	I want to view the evolution of my scheme

@checkForErrors
Scenario: View the assets breakdown chart
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Evolution page
	Then I see the assets breakdown chart #evolution

@checkForErrors
Scenario: View the assets breakdown chart after making analysis asset allocation changes
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Evolution page
	And I note the assets breakdown chart values
	And I view the Analysis page
	And I set the Analysis Date to 02/03/2005
	And I change Investment Strategy allocation assumptions
	And I view the Evolution page
	Then I see the assets breakdown chart displaying the noted values

@checkForErrors
Scenario: View the assets breakdown chart for a scheme with synthetic asset
	Given I am a Client user
	And My default scheme is Test Scheme - Multi Fund with Synthetic Assets
	When I view the Evolution page
	And I set the Attribution Period End Date to 02/03/2005
	Then I see the assets breakdown chart displaying synthetic assets

@checkForErrors
Scenario: View the assets panel for a scheme with LDI turned on
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Evolution page
	Then LDI data is returned

@checkForErrors
Scenario: View the assets panel for a scheme with LDI turned off
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I disable LDI
	And I view the Evolution page
	Then LDI data is returned