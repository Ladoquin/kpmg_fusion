﻿@web @evolution @pensionIncreases
Feature: PensionIncreasesDataPanel
	In order to assess my scheme
	As any user
	I want to view the evolution of my scheme

@checkForErrors
Scenario: View the pension increases
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Evolution page
	And I view the Funding basis
	Then I should see pension increases values for the attribution start and end dates

@checkForErrors
Scenario: View the pension increases for a period spanning an effective date
	Given I am a Client user
	And My default scheme is Test Scheme - Non-Visible Pension Increases
	When I view the Evolution page
	And I view the Technical Provisions basis
	And I set the Attribution Period End Date to 31/12/2013
	Then I should see a different number of pension increase values in the before and after columns

