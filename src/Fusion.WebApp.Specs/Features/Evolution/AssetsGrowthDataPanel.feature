﻿@web @evolution
Feature: AssetsGrowthDataPanel
	In order to assess my scheme
	As any user
	I want to view the evolution of my scheme

@checkForErrors
Scenario: View the asset growth values
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Evolution page
	And I view the Asset growth panel
	Then I see the asset growth values all with number values and unique labels

@checkForErrors
Scenario: View that the asset growth values change with attribution period
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I set the Attribution Period End Date to 01/06/2013
	And I view the Evolution page
	And I view the Asset growth panel
	And I note the current asset growth values
	And I set the Attribution Period End Date to 01/12/2013
	Then I see that the same asset growth labels are returned but the values have changed

@checkForErrors
Scenario: View that the asset growth values contains n/a values for funds that do not exist both sides of the Attribution End Date
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Evolution page
	And I view the Asset growth panel
	And I note the current asset growth values
	And I set the Attribution Period End Date to 30/04/2014
	Then I see that the following funds do not have a growth value
	| Equity Fund | Domestic Equity | Overseas Equity | Buy in |

@checkForErrors
Scenario: View the asset growth values with an Attribution Period Start Date that does not match an Asset effective date
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Buyout basis
	And I view the Evolution page
	And I set the Attribution Period Start Date to 01/01/2014
	And I view the Asset growth panel
	Then I see the asset growth values all with number values and unique labels

@checkForErrors
Scenario: View that the asset growth values total matches that of the asset breakdown
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Evolution page
	And I view the Asset growth panel
	And I note the current asset breakdown total
	Then I see the asset growth values total is the same as the asset breakdown total

@checkForErrors
Scenario: View that the Buyin growth over period value is n/a
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Evolution page
	And I view the Asset growth panel
	Then I see that the following funds do not have a growth value
	| Equity Fund | Domestic Equity | Overseas Equity | Buy in |