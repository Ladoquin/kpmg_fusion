﻿@web @existingABF
Feature: ExistingABF
	In order to analyse a scheme with existing ABF
	As a Fusion user
	I want to see the scheme displayed correctly throughout the system

@checkForErrors
Scenario: View scheme's existing abf on the home page
	Given I am a Client user
	And My default scheme is Test Scheme - Existing ABF 1
	When I view the Funding basis
	And I view the Home page
	Then I see the following asset donut categories
	| Growth | Matched |

@checkForErrors
Scenario: View scheme's existing abf not being displayed for an accounting basis on the home page
	Given I am a Client user
	And My default scheme is Test Scheme - Existing ABF 1
	When I view the Accounting basis
	And I view the Home page
	Then I see the following asset donut categories
	| Growth |

@checkForErrors
Scenario: View scheme's existing abf on the baseline page
	Given I am a Client user
	And My default scheme is Test Scheme - Existing ABF 1
	When I view the Funding basis
	And I view the Baseline page
	Then I see the following asset donut categories
	| Growth | Matched |

@checkForErrors
Scenario: View scheme's existing abf not being displayed for an accounting basis on the baseline page
	Given I am a Client user
	And My default scheme is Test Scheme - Existing ABF 1
	When I view the Accounting basis
	And I view the Baseline page
	Then I see the following asset donut categories
	| Growth |

@checkForErrors
Scenario: View scheme's existing abf on the evolution page
	Given I am a Client user
	And My default scheme is Test Scheme - Existing ABF 1
	When I view the Funding basis
	And I view the Evolution page
	Then I see the following asset donut categories
	| Growth | Matched |

@checkForErrors
Scenario: View scheme's existing abf not being displayed for an accounting basis on the evolution page
	Given I am a Client user
	And My default scheme is Test Scheme - Existing ABF 1
	When I view the Accounting basis
	And I view the Evolution page
	Then I see the following asset donut categories
	| Growth |
