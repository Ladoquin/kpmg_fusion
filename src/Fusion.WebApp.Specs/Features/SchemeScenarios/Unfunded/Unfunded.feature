﻿@web @unfundedScheme
Feature: Unfunded
	In order to analyse unfunded schemes
	As a Fusion user
	I want to be able to use Fusion normally with an unfunded scheme

@checkForErrors
Scenario: View the Home page for an unfunded scheme
	Given I am a Client user
	And My default scheme is Test Scheme - Unfunded 1
	When I view the Home page
	Then No asset breakdown data is returned
	And All asset values in the mini-evolution chart are zero
	And LDI data is not returned
	And The balance total equals the total liabilities

@checkForErrors
Scenario: View the Baseline page for an unfunded scheme
	Given I am a Client user
	And My default scheme is Test Scheme - Unfunded 1
	When I view the Baseline page
	Then No asset breakdown data is returned
	And LDI data is not returned
	And The balance total equals the total liabilities

@checkForErrors
Scenario: View the Evolution page for an unfunded scheme
	Given I am a Client user
	And My default scheme is Test Scheme - Unfunded 1
	When I view the Evolution page
	Then No asset breakdown data is returned
	And No asset growth data is returned
	And LDI data is not returned
	And All asset values in the Surplus Analysis chart are zero
	And The showAssets flag is set to false
	And Employee rate and Deficit recovery contributions are zero
	And The balance total and deficit values equal the total liabilities

@checkForErrors
Scenario: View the Analysis page for an unfunded scheme
	Given I am a Client user
	And My default scheme is Test Scheme - Unfunded 1	
	When I view the Accounting basis
	And I view the Analysis page
	Then funded flag is set to false
	And The asset value in the Balance Sheet chart is zero
	And The balance total equals the total liabilities
	And The asset value in the Funding Position chart is zero
	And Employee rate is zero
	And No recovery plan data is returned
	And The asset values on the Funding Progression chart are zero
	And All asset values in the VaRWaterfall chart are zero
