﻿@web
Feature: VaRChartData
	As a Fusion user 
	So that I can understand my Value at Risk (VaR) in light of the Liability Driven Investment aspect of my portfolio 
	I want a new bar on the VaR Waterfall chart to illustrate hedging against interest and inflation 

Scenario: A new bar on the Waterfall chart will show hedging of interest and inflation
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I Choose the cash lens
	Then I see the VaR Waterfall chart with a Hedging of interest and inflation bar
	And No errors are logged
