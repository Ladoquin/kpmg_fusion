﻿@web
Feature: VaRWaterfallInputs
	As a Fusion user 
	I want CI and Time inputs on the VaR Waterfall chart 
	So that I can more accurately model my Value at Risk

@VaRWaterfall
Scenario: Changing the VaR Waterfall CI input results in new chart data
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I Choose the cash lens
	And I change the CI VaR Waterfall input
	Then I see new chart data rendered to the VaR Waterfall chart
	And No errors are logged

@VaRWaterfall
Scenario: Changing the VaR Waterfall Time input results in new chart data
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I Choose the cash lens
	And I change the Time VaR Waterfall input
	Then I see new chart data rendered to the VaR Waterfall chart
	And No errors are logged

@VaRWaterfall
Scenario: Navigation between pages leaves the VaR Waterfall CI input unchanged
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I Choose the cash lens
	And I change the CI VaR Waterfall input
	And I view the Evolution page
	When I view the Analysis page
	And I Choose the cash lens
	Then I see that the CI VaR Waterfall input is still showing the previous setting
	And No errors are logged

@VaRWaterfall
Scenario: Navigation between pages leaves the VaR Waterfall Time input unchanged
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I Choose the cash lens
	And I change the Time VaR Waterfall input
	And I view the Evolution page
	When I view the Analysis page
	And I Choose the cash lens
	Then I see that the Time VaR Waterfall input is still showing the previous setting
	And No errors are logged

@VaRWaterfall
Scenario: Changing a VaR input without changing scheme assumptions results in Before values only on the chart	
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I Choose the cash lens
	And I change the Time VaR Waterfall input
	Then I see Before values only rendered on the chart tooltips

@VaRWaterfall
Scenario: Changing a VaR input after changing scheme assumptions results in Before and After values on the chart
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I Choose the cash lens
	And I change Investment Strategy allocation assumptions
	And I change the Time VaR Waterfall input
	Then I see Before and After values rendered on the chart tooltips

@VaRWaterfall
Scenario: Reload Scheme Defaults has no effect on the VaR inputs selected
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I Choose the cash lens
	And I change Investment Strategy allocation assumptions
	And I change the Time VaR Waterfall input
	And I press Reload Scheme Defaults 
	Then I see that the Time VaR Waterfall input is still showing the previous setting

#Before and after need to be recalculated for every VaR unput change when the cache is dirty
@VaRWaterfall
Scenario: Before and After results are relevant to the VaR inputs selected 
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I Choose the cash lens
	And I change Investment Strategy allocation assumptions

	#force another get on the cash lense to replicate the .js behaviour of updating dependant panels on a scheme variable change
	And I Choose the cash lens 

	And I change the Time VaR Waterfall input
	Then Both the before and after values should change