﻿@web @schemeUsability
Feature: SchemeUsability
	In order to quickly navigate between schemes
	As a non-Client user
	I want a dropdown of my favourite schemes

Scenario: Add scheme to favourites list
	Given I have access to at least one scheme
	And I am not a Client user
	When I add a scheme to my favourites
	Then I am able to see the scheme in my favourites list

Scenario: Remove scheme from favourites list
	Given I have access to at least one scheme
	And I am not a Client user
	And I have at least one scheme in my favourites list
	When I remove a scheme from my favourites list
	Then The scheme gets removed from my favourites list

Scenario: Cannot add a scheme to favourites more than once
	Given  I have access to at least one scheme
	And I am not a Client user
	And I have at least one scheme in my favourites list
	When I try to add an existing favourite scheme again
	Then The scheme will only appear once in my favourites list