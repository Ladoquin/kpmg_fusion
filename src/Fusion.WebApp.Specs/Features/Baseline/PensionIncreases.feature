﻿@web @baseline @pensionIncreases
Feature: PensionIncreases
	In order to assess my scheme
	As a KPMG Partner 
	I want to view the starting position of my scheme

@checkForErrors
Scenario: View pension increase definitions at baseline
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Baseline page
	And I view the Technical Provisions basis
	Then I should see the pension increase definitions for the current scheme anchor

@checkForErrors
Scenario: View pension increase definitions for a scheme that has non-visible increases
	Given I am a Client user
	And My default scheme is Test Scheme - Non-Visible Pension Increases
	When I view the Baseline page
	And I view the Technical Provisions basis
	Then I should see the pension increase definitions for the current scheme anchor
