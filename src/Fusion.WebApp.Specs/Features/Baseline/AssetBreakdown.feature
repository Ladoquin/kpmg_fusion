﻿@web @baseline @assetBreakdown
Feature: Baseline asset breakdown
	In order to assess my scheme
	As a Fusion user
	I want to view the starting position of my scheme assets

@checkForErrors
Scenario: View the assets breakdown chart
	Given I am a Client user
	And My default scheme is The London Stock Exchange Retirement Plan updated
	When I view the Baseline page
	Then I see the assets breakdown chart #baseline

@checkForErrors
Scenario: Assets breakdown displays baseline figures
	Given I am a Client user
	And My default scheme is The London Stock Exchange Retirement Plan updated
	When I view the Baseline page
	And I view the Funding basis
	Then I see the assets breakdown chart displaying figures from the basis anchor date

@checkForErrors
Scenario: Assets breakdown shows baseline figures after date change and analysis changes
	Given I am a Client user
	And My default scheme is The London Stock Exchange Retirement Plan updated
	When I view the Analysis page
	And I view the Funding basis
	And I set the Analysis Date to 01/01/2015
	And I change Asset assumptions
	And I set cash injection to £10M
	And I change Investment Strategy allocation assumptions
	When I view the Baseline page
	Then I see the assets breakdown chart displaying figures from the basis anchor date

@checkForErrors
Scenario: Assets breakdown shows baseline figures from a later effective date when viewing a basis with later anchor date
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Baseline page
	And I view the Buyout basis
	Then I see the assets breakdown chart displaying figures from the basis anchor date
