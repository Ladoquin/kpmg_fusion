﻿@baseline @web
Feature: Baseline
	In order to assess my scheme
	As a KPMG Partner 
	I want to view the starting position of my scheme

@checkForErrors
Scenario: View the baseline page
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Baseline page
	Then I am returned json relevant to the baseline page

Scenario: View pension increases on baseline page
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Baseline page
	Then I see pension increase assumptions
	And No errors are logged


Scenario: Pension increases are correct as represented by the underlying scheme
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Baseline page
	Then I see pension increase assumptions
	And The pension increases are as specified in the underlying scheme
	And No errors are logged