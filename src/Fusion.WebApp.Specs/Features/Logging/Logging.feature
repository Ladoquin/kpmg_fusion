﻿@web
Feature: Logging
	In order to quickly identify issues with the Fusion application
	As a Space01 user
	I want to view log entries

@web @logging
Scenario: Filter log table by type Error
	Given There are entries in the logging table
	And I am the Developer user
	When I select the Error filter on the log results
	Then I only see log entries of type Error
