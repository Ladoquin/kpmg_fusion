﻿@web
Feature: LastImportDetails
	In order to help identify scheme upload problems
	As a user with admin privileges
	I want to view the details of the previous scheme xml upload

@checkForErrors
Scenario: View last import details
	Given I am an Admin user
	When I upload a scheme
	Then the last import details should show details of the previous import
