﻿@web @schemeXml
Feature: SchemeManageSourceXml
	In order to debug a problematic scheme in Fusion
	As a user with scheme management privileges
	I want access to the schemes source xml file

@checkForErrors
Scenario: Create new scheme and check that source xml is saved
	Given I am an Admin user
	When I import scheme Test Scheme - 02
	Then The scheme should be saved
	And The scheme xml should be saved to the database for Test Scheme - 02

@checkForErrors
Scenario: Download the source xml for a scheme
	Given I am an Admin user
	When I click the Export button for The Space01 Test Scheme
	Then I am served the source xml

@checkForErrors
Scenario: Update a scheme without uploading a new source xml ensuring the previous xml is not effected
	Given I am an Admin user
	And Test Scheme - 02 has been imported
	When I manage a scheme
	| SchemeName              | IsNew | Logo           | IsRestricted | AccountingPassword | Xml |
	| Test Scheme - 02        | False | TestLogo01.png | False        |                    |     |
	And I click the Export button for The Space01 Test Scheme
	Then I am served the source xml

@checkForErrors
Scenario: Update a scheme source xml
	Given I am an Admin user
	And Test Scheme - 02 has been imported
	When I import an edited Test Scheme - 02 source xml
	And I click the Export button for Test Scheme - 02
	Then I am served the source xml 
	#ie, I am served the *updated* source xml

