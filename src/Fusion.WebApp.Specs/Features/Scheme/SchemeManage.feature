﻿@web @schemeManage
Feature: SchemeManage
	In order to manage a scheme in Fusion
	As a user with scheme management privileges
	I want to be able to add and edit scheme details

@schemeManageAdd @checkForErrors
Scenario: Add Scheme without providing an accounting downloads password
	Given I am an Admin user
	When I add a scheme without providing an accounting downloads password
	Then The scheme should be saved
	And The accounting downloads password for the scheme should be null

@schemeManageAdd @checkForErrors
Scenario: Add Scheme providing an accounting downloads password
	Given I am an Admin user
	When I add a scheme providing an accounting downloads password
	Then The scheme should be saved
	And The accounting downloads password for the scheme should not be null

@checkForErrors
Scenario: Edit Scheme with existing accounting downloads password without providing a new password
	Given I am an Admin user
	When I edit an existing scheme with an accounting downloads password and do not provide a new password
	Then The scheme should be saved
	And The accounting downloads password for the scheme should be unchanged

@checkForErrors
Scenario: Edit Scheme with existing accounting downloads password providing a new password
	Given I am an Admin user
	When I edit an existing scheme with an accounting downloads password and provide a new password
	Then The scheme should be saved
	And The accounting downloads password for the scheme should be changed
	

