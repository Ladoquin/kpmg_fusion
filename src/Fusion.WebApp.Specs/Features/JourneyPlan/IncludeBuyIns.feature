﻿@web
Feature: IncludeBuyIns
	As a Fusion user 
	So that I can include/exclude buy-ins on my Journey Plan 
	I want a check box on the Journey Plan page to toggle the option

@JourneyPlan @web @checkForErrors
Scenario: The default state for include buy-ins is false
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Journey Plan page
	Then The include buy-ins control is not checked

@JourneyPlan @web @checkForErrors
Scenario: The include buy-ins state is maintain between Journey Plan calculations
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Journey Plan page
	And I change include buy-ins from not checked to checked
	Then The include buy-ins control is checked

@JourneyPlan @web @checkForErrors
Scenario: The include buy-ins state is maintained between page navigation
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Journey Plan page
	And I change include buy-ins from not checked to checked
	And I view the Analysis page
	And I view the Journey Plan page
	Then The include buy-ins control is checked



