﻿@web
Feature: RecoveryPlan
	As a Fusion user 
	When I change any of the Recovery Plan variables 
	I want to see the effects of my change on the chart illustrating how my journey to self sufficiency has been effected

@web
Scenario: Self sufficient sooner when annual contributions increased
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Journey Plan page
	And I increase my annual contributions on the recovery plan
	Then My fund should become self sufficient sooner
	And No errors are logged

	
@web
Scenario: The contribution maximum is the same as the max lump sum for the scheme
	Given I am an Admin user
	And My default scheme is The Space01 Test Scheme
	When I view the Journey Plan page
	Then The max contribution pa is equivalent to the recovery plan max lump sum for the scheme

