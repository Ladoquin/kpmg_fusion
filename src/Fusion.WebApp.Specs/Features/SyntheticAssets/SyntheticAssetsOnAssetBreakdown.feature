﻿@web @syntheticAssets
Feature: Synthetic assets on the asset breakdown
	As a Fusion User
	I want to see synthetic growth on the asset breakdown
	So that I understand what percenatge of my portfiolio is synthetic with a notional value

@checkForErrors @home
Scenario: Synthetic growth has the correct proportion and value on the home page
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Accounting basis
	And I view the Home page
	Then Synthetic growth is the correct percentage of the total assets for 'home'

@checkForErrors
Scenario: Synthetic growth has the correct proportion and value on the baseline page
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Accounting basis
	And I view the Baseline page
	Then Synthetic growth is the correct percentage of the total assets for 'baseline'

@checkForErrors
Scenario: Synthetic growth has the correct proportion and value on the analysis page
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Accounting basis
	And I view the Analysis page
	Then Synthetic growth is the correct percentage of the total assets for 'analysis'

@checkForErrors
Scenario: Synthetic growth has the correct proportion and value on the analysis page for the investment strategy panel
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Accounting basis
	And I view the Analysis page
	Then Synthetic growth is the correct percentage of the total assets for 'analysis-investmentStrategy'

@checkForErrors
Scenario: Synthetic growth has the correct proportion and value on the evolution page
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Accounting basis
	And I view the Evolution page
	Then Synthetic growth is the correct percentage of the total assets for 'evolution'

