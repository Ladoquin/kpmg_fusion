﻿@web @account @changepassword
Feature: ChangePassword
	In order to keep user's accounts secure
	As a Fusion user
	I must follow a set of rules pertaining to my new password selection

@web 
Scenario: Change password to unused value
	Given I am a Client user
	And the password I am going to change to is not in my history
	When I change my password
	Then the new password should be saved

#@web 
#Scenario: Change password to same as in recent history
#	Given I am a Client user
#	And the password I am going to change to is already in my history
#	When I change my password
#	Then the new password should not be saved

#@web 
#Scenario: Change password only prevents new password from being in history of last five
#	Given I am a Client user
#	# Because after calling the change password five times the password should no longer be in the history - this also sets up the salt
#	And the password I am going to change to is not in my history
#	And I have changed my password five times
#	When I change my password back to my first password
#	Then the new password should be saved
#	And the first password is saved as the most recent