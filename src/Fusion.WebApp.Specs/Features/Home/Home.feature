﻿@web @home
Feature: Home
	In order to assess my scheme
	As a Fusion
	I want to view the summary position of my scheme

@checkForErrors
Scenario: View the home page
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Home page
	Then I am returned the json relevant to the Home page

@checkForErrors
Scenario: View the home page for a scheme with ldi turned on
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Home page
	Then LDI data is returned

@checkForErrors
Scenario: View the home page for a scheme with ldi turned off
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I disable LDI
	And I view the Home page
	Then LDI data is returned

