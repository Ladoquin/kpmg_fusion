﻿@web @resultsdump
Feature: ResultsDump
	In order to assist regression testing
	As an Admin user
	I want to be able to quickly compare results between Fusion releases

@checkForErrors
Scenario: Download results dump file
	Given I am an Admin user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I click the results dump button
	Then The results dump file is served
	And The results file contains the expected results

@checkForErrors
Scenario: Download results dump file having made client side changes
	Given I am an Admin user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I make a change to the assumptions
	And I click the results dump button
	Then The results dump file is served
	And The results file contains the expected results
