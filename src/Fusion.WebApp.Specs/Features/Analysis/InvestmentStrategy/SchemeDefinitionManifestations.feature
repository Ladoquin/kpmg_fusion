﻿@web @investmentStrategy
Feature: SchemeDefinitionManifestations
	In order to see the effects of investment strategy changes on my scheme
	As a Fusion user
	I want to amend the investment strategy asset allocations

@checkForErrors
Scenario: Set investment strategy asset allocations on a scheme containing assets of only one class
	Given I am a Client user
	And My default scheme is Test Scheme - One Asset Type w ABF
	When I view the Funding basis
	And I view the Analysis page
	And I view the Investment Strategy Assumptions panel
	Then I should see the following asset classes 
	| Equity | Property | DiversifiedGrowth | DiversifiedCredit | PrivateMarkets | CorporateBonds | FixedInterestGilts | IndexLinkedGilts | Cash |
	And I should be able to edit the Equity investment strategy slider
	And I should be able to edit the PrivateMarkets investment strategy slider

