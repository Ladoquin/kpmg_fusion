﻿@web
Feature: WeightedAssetReturns
	In order to analyse my scheme
	As a Fusion user
	I want to be review the underlying values used in calculating my scheme's projections

Scenario: View the Weighted Asset Return values before analysis changes
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	Then I see the correct weighted asset return values on the Investment Strategy panel #before

Scenario: View the Weighted Asset Return values after analysis changes
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page	
	And I change Investment Strategy allocation assumptions
	Then I see the correct weighted asset return values on the Investment Strategy panel #after