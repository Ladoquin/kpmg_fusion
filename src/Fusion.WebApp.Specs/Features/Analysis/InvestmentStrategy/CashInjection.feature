﻿@web @investmentStrategy @cashInjection
Feature: CashInjection
	As a: Fusion user
	I want: To be able to simulate adding to my assets by making a cash injection to my scheme.
	So that: I can see the effect on my scheme value for such an injection.

@checkForErrors
Scenario: Cash injection increases the total asset value by increasing the amount in the cash asset class
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Analysis page
	And I note the current total asset value
	When I set cash injection to £2M
	Then the total asset value increases by £2M

@checkForErrors
Scenario: Cash injection change persists after allocation mix change
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Analysis page
	And I set cash injection to £12M
	And I set the investment strategy proportions to
	| DiversifiedCredit | DiversifiedGrowth | Equity | PrivateMarkets | Property | CorporateBonds | FixedInterestGilts | IndexLinkedGilts | Cash |
	| 50                | 10                | 20     | 0              | 5        | 5              | 0                  | 0                | 10   |
	Then I see that cash injection is £12M

@checkForErrors
Scenario: Cash injection change adjusts asset allocations for scheme with existing ABF component
	Given I am a Client user
	And My default scheme is Test Scheme - One Asset Type w ABF
	When I view the Funding basis
	And I view the Analysis page
	And I note the current total asset value
	And I note the percentage value of the following asset allocations
	| FixedInterestGilts |
	And I set cash injection to £10M
	Then I see that the percentage value has lowered for the noted asset allocations
	And the total asset value increases by £10M

