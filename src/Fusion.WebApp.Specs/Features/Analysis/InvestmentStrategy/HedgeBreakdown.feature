﻿@web
Feature: HedgeBreakdown
	In order to analysis my scheme
	As a Fusion user
	I want to see the hedge breakdown

@checkForErrors
Scenario: View hedge breakdown table for a scheme with LDI in force by default
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Analysis page 
	And I view the Investment Strategy Advanced panel
	Then I see the hedge breakdown table
	And The current basis display name is shown as the current basis column header
