﻿@web @investmentStrategy
Feature: InvestmentStrategyAssetClasses
	As a KPMG manager 
	I want to update the available asset classes 
	So that my clients can understand their pension funds via a more accurate representation of the available classes of asset

Scenario: The asset break down includes the relevant asset classes
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page 
	And I view the Investment Strategy tab
	Then I see the that the relevant asset classes are included in the New Investment Strategy
	And No errors are logged

Scenario: The asset classes sliders are in correct order
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I view the Investment Strategy tab
	Then I see that the asset class sliders are in correct order

Scenario: Investment strategy allocations change is reflected on the Assets panel
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Analysis page
	And I view the Investment Strategy tab
	And I set the investment strategy proportions to
	| DiversifiedCredit | DiversifiedGrowth | Equity | PrivateMarkets | Property | CorporateBonds | FixedInterestGilts | IndexLinkedGilts | Cash |
	| 50                | 20                | 20     | 0              | 5        | 5              | 0                  | 0                | 0    |
	Then I see that the values in the assets breakdown on the Investment strategy tab match those on the Assets panel

@existingABF
Scenario: Investment strategy allocations change is reflected on the Assets panel for a scheme with existing ABF
	Given I am a Client user
	And My default scheme is Test Scheme - One Asset Type w ABF
	When I view the Funding basis
	And I view the Analysis page
	And I view the Investment Strategy tab
	And I set the investment strategy proportions to
	| DiversifiedCredit | DiversifiedGrowth | Equity | PrivateMarkets | Property | CorporateBonds | FixedInterestGilts | IndexLinkedGilts | Cash |
	| 50                | 20                | 20     | 0              | 5        | 5              | 0                  | 0                | 0    |
	Then I see that the values in the assets breakdown on the Investment strategy tab match those on the Assets panel