﻿@web
Feature: EnableLdi
	As a Fusion user 
	I want to toggle the inclusion of LDI on my pension fund 
	So that I can include/exclude the concept as necessary

@LDI
Scenario: Default LDI setting is enabled
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page 
	And I view the Investment Strategy tab
	Then I see the that the default for LDI is enabled
	And I see that the Hedging panel is enabled
	And I see that the Hedging default values are set
	And No errors are logged

@LDI
Scenario: LDI can be disabled
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page 
	And I view the Investment Strategy tab
	And I disable LDI
	Then I observe that LDI remains disabled
	And I see that the Hedging panel is disabled
	And No errors are logged

@LDI @checkForErrors
Scenario: Hedging inputs are posted
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I view the Investment Strategy tab
	And I set Hedging values
	Then I observe that the Hedging values remain

@LDI @checkForErrors
Scenario: Hedging inputs are reset
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I view the Investment Strategy tab
	And I set Hedging values
	And I reset the Investment Strategy panel
	Then I see that the Hedging panel is enabled
	And I see that the Hedging default values are set

@LDI @checkForErrors
Scenario: Hedging inputs remain after disable / enable LDI toggle
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I view the Investment Strategy tab
	And I set Hedging values
	And I disable LDI
	And I enable LDI
	Then I observe that the Hedging values remain

@LDI @checkForErrors
Scenario: Default values for hedging inputs changes
	Given I am a Client user
	And My default scheme is Test Scheme - LDI Asset changes
	When I view the Analysis page
	And I view the Investment Strategy tab
	And I note down the default hedging values
	And I set the Analysis Date to 31/03/2013
	And I view the Investment Strategy tab
	Then I observe that the default hedging values have changed

@LDI @checkForErrors
Scenario: Default values for hedging inputs are zero when LDI is not in force
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Analysis page
	And I view the Investment Strategy tab
	Then I observe that LDI remains disabled
	And I observe that the default hedging values are zero
