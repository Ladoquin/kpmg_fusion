﻿@web @investmentStrategy @syntheticGrowth
Feature: SyntheticGrowth
	As a Fusion user 
	I want to set the proportions of Synthetic Equity and Credit in my portfolio 
	So that Fusion reflects my pension fund in a real world context where leveraged/notional products play a part
	
@checkForErrors
Scenario: Synthetic growth components are present in the investment strategy options
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page 
	And I view the Investment Strategy tab
	Then I see that synthetic growth components are present in the investment strategy options

@checkForErrors
Scenario: The asset breakdown chart includes synthetic asset total as set by user
	Given I am a Client user
	And My default scheme is Test Scheme - Multi Fund with Synthetic Assets
	When I view the Analysis page 
	And I view the Investment Strategy tab
	And I amend the synthetic equity and synthetic credit sliders
	Then I see a corresponding synthetic component on the assets breakdown chart on the investment strategy panel
	And I see a corresponding synthetic component on the assets breakdown chart on the assets panel

