﻿@web @ldi @hedging @analysis @investmentStrategy
Feature: HedgeInputs
	In order to analysis my scheme
	As a Fusion user
	I want to only be allowed to set relevant hedging inputs

@checkForErrors
Scenario: An investment strategy asset mix physical assets change udpates hedging lower limit
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Analysis page
	And I view the Investment Strategy Advanced panel
	And I note the hedging interest lower limit
	And I note the hedging inflation lower limit
	And I view the Investment Strategy Assumptions panel
	And I set the investment strategy proportions to
	| DiversifiedCredit | DiversifiedGrowth | Equity | PrivateMarkets | Property | CorporateBonds | FixedInterestGilts | IndexLinkedGilts | Cash |
	| 0                 | 0                 | 0      | 0              | 0        | 0              | 50                 | 50               | 0    |
	And I view the Investment Strategy Advanced panel
	Then the hedging interest lower limit has increased
	And the hedging inflation lower limit has increased
