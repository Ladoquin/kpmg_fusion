﻿@web @efro
Feature: EFROPanel
	In order to analyse the affect of an EFRO on my pension scheme
	As a Fusion user
	I want to set EFRO parameters

@checkForErrors
Scenario: Create an efro
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I set EFRO as
	| TakeUpRate | SizeOfTransferValue |
	| 20         | 60                  |
	Then The Analysis page is ok 
	And The EFRO assumptions are persisted

@checkForErrors
Scenario: Create an efro with no take up
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I set EFRO as
	| TakeUpRate | SizeOfTransferValue |
	| 0          | 0                   |
	Then The Analysis page is ok 
	And The EFRO assumptions are persisted

@checkForErrors
Scenario: Create an efro with max take up
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I set EFRO as
	| TakeUpRate | SizeOfTransferValue |
	| 99         | 120                 |
	Then The Analysis page is ok 
	And The EFRO assumptions are persisted

