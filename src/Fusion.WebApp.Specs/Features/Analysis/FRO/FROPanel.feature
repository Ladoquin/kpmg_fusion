﻿@web @fropanel
Feature: FROPanel
	In order to analyse the affect of a FRO on my pension scheme
	As a Fusion user
	I want to set FRO parameters

Scenario: View the FRO panel
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I view the Accounting basis
	Then I see the correct default parameters on the FRO panel
	And No errors are logged

@fropanel_customvals
Scenario: View the FRO panel with a scheme that has overriden min and max TV values
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Analysis page
	And I view the Funding basis
	Then I see the correct default parameters on the FRO panel
	And No errors are logged