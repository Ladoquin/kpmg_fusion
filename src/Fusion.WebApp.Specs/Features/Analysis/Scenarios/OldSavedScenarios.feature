﻿@web @scenarios
Feature: OldSavedScenarios
	In order to save time
	As any user
	I want my saved scenarios to be unaffected by a new Fusion release

#We don't support backward compatibility, so is this scenario still relevant?
#@checkForErrors
#Scenario: Load a scenario saved from version 1.5
#	Given I am a Client user
#	And My default scheme is The Space01 Test Scheme
#	When I view the Analysis page
#	And I load the scenario tp scenario#1
#	Then the scenario is loaded
