﻿@web @liabilitiespanel
Feature: LiabilitiesPanel
	In order to analyses the affect of liability assumptions on my scheme
	As a Fusion user
	I want to change liability assumptions

Scenario: Pension increases follow liability inflation assumption changes
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I set the Analysis Date to 31/3/2014
	And I change a liability inflation assumption
	Then pension increase assumptions are updated
	And No errors are logged

Scenario: Unlocking pension increases maintains their current values
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I set the Analysis Date to 31/3/2014
	And I change a liability inflation assumption
	And I unlock pension increase assumptions
	Then pension increase assumptions equal those at time of unlocking
	And No errors are logged

Scenario: Unlocked pension increase assumptions are unaffected by liablity inflation assumption changes
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I set the Analysis Date to 31/3/2014
	When I unlock pension increase assumptions
	And I change a liability inflation assumption
	Then pension increase assumptions are unaffected
	And No errors are logged
