﻿@web @pensionIncreases
Feature: PensionIncreases
	In order to analyses the affect of pension increase assumptions on my scheme
	As a Fusion user
	I want to change pension increases

@checkForErrors
Scenario: View pension increases
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Funding basis
	And I view the Analysis page
	Then I see the pension increases at the analysis date

@checkForErrors
Scenario: View pension increases for a scheme with hidden increases
	Given I am a Client user
	And My default scheme is Test Scheme - Non-Visible Pension Increases
	When I view the Technical Provisions basis
	And I view the Analysis page
	Then I do not see the hidden pension increases at the analysis date
