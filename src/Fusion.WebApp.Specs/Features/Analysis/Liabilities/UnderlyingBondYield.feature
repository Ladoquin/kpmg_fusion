﻿@web @liabilitiespanel
Feature: UnderlyingBondYield
	As a Fusion user 
	I want a customisable gilt yield index on the liability assumptions panel 
	So that I can see what underlying gilt yield I am achieving in my fund


Scenario: The gilt yield value is correct as of the selected analysis date
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I set the Analysis Date to 31/3/2014
	Then The gilt yield index 'FTSE BRIT.GOVT.FIXED ALL STOCKS - ANNUAL GRY' value is as expected
	And No errors are logged