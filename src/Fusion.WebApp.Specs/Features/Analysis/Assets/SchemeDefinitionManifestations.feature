﻿@web @assetAssumptions
Feature: SchemeDefinitionManifestations
	In order to see the effects of asset returny changes on my scheme
	As a Fusion user
	I want to amend the asset return assumptions

@checkForErrors @schemeImported
Scenario: Set asset returns on a scheme containing assets of only one class
	Given I am a Client user
	And I have imported the Test Scheme - One Asset Type w ABF
	And My default scheme is Test Scheme - One Asset Type w ABF
	When I view the Funding basis
	And I view the Analysis page
	And I view the Asset Assumptions panel
	Then I should see the following asset classes 
	| Equity | Property | DiversifiedGrowth | DiversifiedCredit | PrivateMarkets | CorporateBonds | FixedInterestGilts | IndexLinkedGilts | Cash | Abf |
	And I should be able to edit the Equity investment strategy slider
	And I should be able to edit the PrivateMarkets investment strategy slider