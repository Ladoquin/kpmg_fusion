﻿@web @assetsPanel
Feature: AssetsPanel
	In order to analyse the affect of asset assumptions on my scheme
	As a Fusion user
	I want to change asset returns

@checkForErrors
Scenario: View hedged cashflow data
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Analysis page
	Then I see hedged cashflow information on the asset assumptions tab

Scenario: View hedged cashflow data updated by client
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Technical Provisions basis
	And I view the Analysis page
	And I set Hedging values
	Then I see hedged cashflow information on the asset assumptions tab