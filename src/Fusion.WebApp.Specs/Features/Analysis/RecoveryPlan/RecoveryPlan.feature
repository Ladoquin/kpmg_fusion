﻿@web @recoveryPlan
Feature: RecoveryPlan
	In order to analyse the affect a different recover plan would have on my scheme
	As a Fusion user
	I want to change recovery plan parameters

@checkForErrors
Scenario: Make adjustments
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Analysis page
	And I set the Recovery Plan to
	| Length | Increases | LumpSum | Start | FixedAssetReturn | DiscountRate | Strategy |
	| 10     | 0.05      | 1500000 | 12    | 0                | 0.1          | Current  | 
	Then My changes are accepted
