﻿@web @accounting @usgaap
Feature: USGAAP
	In order to analysis my scheme
	As a KPMG user
	I want to view the USGAAP accounting information

@checkForErrors
Scenario: View USGAAP forecast summary
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Analysis page
	And I view the Accounting basis
	Then I see the USGAAP forecast chart

@checkForErrors
Scenario: View USGAAP forecast summary after liability changes
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Accounting basis
	And I view the Analysis page
	And I see the USGAAP forecast chart
	And I change Liability assumptions
	Then The USGAAP forecast chart values have changed

@checkForErrors
Scenario: View USGAAP forecast summary after asset return changes
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Accounting basis
	And I view the Analysis page
	And I see the USGAAP forecast chart
	And I change Asset assumptions
	Then The USGAAP forecast chart values have changed

@checkForErrors
Scenario: Download USGAAP disclosure for historic period
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Accounting basis
	And I view the Analysis page
	When I download the USGAAP disclosure for date period 31/03/2013 to 31/03/2014
	Then I am served the USGAAP disclosure

@checkForErrors
Scenario: Download USGAAP disclosure for current period with liability and asset assumption changes
	Given I am a Client user
	And My default scheme is The ABC Pension Scheme
	When I view the Accounting basis
	And I view the Analysis page
	And I change Liability assumptions
	And I change Asset assumptions
	When I download the USGAAP disclosure for date period 31/03/2014 to 03/02/2015
	Then I am served the USGAAP disclosure

@checkForErrors
Scenario: View USGAAP forecast summary for the start date
	Given I am a Client user
	And My default scheme is The Space01 Test Scheme
	When I view the Analysis page
	And I set the Analysis Date to 31/03/2013
	And I view the Accounting basis
	Then I see the USGAAP forecast chart