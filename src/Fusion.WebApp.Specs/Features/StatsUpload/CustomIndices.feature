﻿@stats @web @compositeIndices
Feature: CustomIndices
	So that my custom indices are more manageable 
	as Fusion administrator
	I want to be able to define a weighted average index derived from multiple other indices

@web @customStats
Scenario: Custom indices are created on stats import
	Given I have a stats file with a custom indices component
	And I am an Admin user
	When I upload the stats file
	Then the file is accepted
	And the custom indices are created
	And No errors are logged

@web @customStatsExist @customStats
Scenario: Only real indices can be specified as inputs
	Given I have a stats file with a custom indices component referencing non core indices
	And I am an Admin user
	When I upload the stats file
	Then the file is rejected
	And the exception 'A custom index cannot be used to create another custom index' is logged

@web @customStats
Scenario: Source index count greater than 0 in a composite index must be supported 
	Given I have a stats file with a composite index that has one component 
	And I am an Admin user
	When I upload the stats file
	Then the file is accepted
	And the custom indices are created
	And No errors are logged

@web @customStats
Scenario: Source index count equal to 0 in a composite index must NOT be supported 
	Given I have a stats file with a custom indices component containing no component indices
	And I am an Admin user
	And I take a snapshot of the indices prior to uploading new ones
	When I upload the stats file
	Then the file is rejected
	And the exception 'A composite index must have at least one component' is logged
	And no indices are updated or created

@web @customStatsExist @customStats
Scenario: Updating an existing composite index with a source index count = 0 the existing file must NOT be updated
	Given I have a stats file with a custom indices component containing no component indices
	And I am an Admin user
	And I take a snapshot of the indices prior to uploading new ones
	When I upload the stats file
	Then the file is rejected
	And the exception 'A composite index must have at least one component' is logged
	And no indices are updated or created

@web @customStats
Scenario: Source indexes on a composite index can only be used once in a composite index
	Given I have a stats file with a custom indices component containing duplicate component indices
	And I am an Admin user
	And I take a snapshot of the indices prior to uploading new ones
	When I upload the stats file
	Then the file is rejected
	And the exception 'Components of a composite index cannot be included more than once' is logged
	And no indices are updated or created

@web @customStats
Scenario: Composite indices cannot have the same name as normal indices
	Given I have a stats file with a custom index with the same name as a core index
	And I am an Admin user
	When I upload the stats file
	Then the file is rejected
	And the exception 'A custom index cannot have the same name as a core index' is logged
 
@web @customStats
Scenario: Composite indices are displayed in the indices list
	Given I have a stats file with a custom indices component
	And I am an Admin user
	When I upload the stats file 
	And I view the Manage Indices screen
	Then the indices the list contains includes composite indices
 
@web @customStats
Scenario: Source Index Count in a Composite Index Must be supported up to ten
	Given I have a stats file with a custom indices component containing ten Source Indexes
	And I am an Admin user
	When I upload the stats file
	Then the file is accepted
	And the 10 custom indices are created
	And No errors are logged