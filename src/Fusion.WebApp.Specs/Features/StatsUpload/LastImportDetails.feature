﻿@web @stats
Feature: LastImportDetails
	In order to identify potential stats file upload issues
	As a user with admin privileges
	I want to review the details of the previous stats file upload

@checkForErrors
Scenario: Import details are correctly recorded
	Given I have a stats file with a valid schema
	And I am an Admin user
	When I upload the stats file
	Then the file is accepted
	And The Last Import Details displays the details of that upload #v1

@checkForErrors
Scenario: History of Import details are kept
	Given I have a stats file with a valid schema
	And I am an Admin user
	When I upload the stats file
	And I upload another stats file
	Then The Last Import Details displays the details of that upload #v2
	And Both uploads are stored in the database history
