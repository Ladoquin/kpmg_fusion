﻿@web @stats
Feature: StatsUploadEffects
	In order to manage the Fusion system
	As a user with admin privileges
	I want necessary system tasks to be performed after uploading a new stats file

@checkForErrors @forcedSchemeLDIInflationIndexDependence 
Scenario: Uploading a new stats file clears pre-calculated values only down to the last index change date
	Given I am an Admin user
	And I have a stats file with a valid schema
	And This stats file is loaded
	And Pre-calculated values exist for at least one scheme and basis
	And I have a stats file with an updated historic index
	And This scheme relies on the index being updated 
	When I upload the stats file
	Then The pre-calculated values cache for the scheme should be cleared down to the last change date of any index depended on

