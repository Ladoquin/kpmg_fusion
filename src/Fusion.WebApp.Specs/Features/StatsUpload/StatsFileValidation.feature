﻿@stats @web
Feature: StatsFileValidation
	So that I can correct any structure and data type errors in the stats file XML
	As an Admin or Installer
	I want meaningful validation feedback when loading a stats file

@web
Scenario: Stats file schema is valid
	Given I have a stats file with a valid schema
	And I am an Admin user
	When I upload the stats file
	Then the file is accepted
	And No errors are logged

@web
Scenario: Stats file schema is invalid due to wrong data types
	Given I have a stats file with an invalid schema due to wrong data types
	And I am an Installer user
	When I upload the stats file
	Then the file is rejected and the reasons why are displayed
	And one or more of the reasons relates to wrong data types
	And I am not able to proceed with the import
	And No errors are logged

@web
Scenario: Stats file schema is invalid due to missing mandatory data
	Given I have a stats file with an invalid schema due to missing mandatory data
	And I am an Installer user
	When I upload the stats file
	Then the file is rejected and the reasons why are displayed
	And one or more of the reasons relates to missing mandatory data
	And I am not able to proceed with the import
	And No errors are logged

@web
Scenario: Stats file schema is invalid due to missing mandatory elements
	Given I have a stats file with an invalid schema due to missing mandatory elements
	And I am an Installer user
	When I upload the stats file
	Then the file is rejected and the reasons why are displayed
	And one or more of the reasons relates to missing mandatory elements
	And I am not able to proceed with the import

@web
Scenario: Allow import of an invalid stats file
	Given I have a stats file with an invalid schema due to wrong data types
	And I am an Admin user
	When I upload the stats file
	Then the file is rejected and the reasons why are displayed
	And I am able to proceed with the import

@web
Scenario: Upload of invalid stats file logs validation messages
	Given I have a stats file with an invalid schema due to wrong data types
	And I am an Installer user
	When I upload the stats file
	Then validation messages are logged
	And No errors are logged

