﻿@installer @userManagement @installerUserActions @web
Feature: InstallerUserActions
	In order to assit with user management
	As an Installer user
	I want to manage a subset of the schemes available

@checkForErrors @installerUserAction
Scenario: View client with overlapping restricted scheme access
	Given I am an Installer user
	And More than one restricted scheme exists
	And A client exists and has access to more than one restricted scheme
	And I have access to at least one, but not all, of the restricted schemes the client has access to
	When I view the client
	Then I should see all the unrestricted schemes the client has access to
	And I should see all the restricted schemes the client has access to which I have access to
	And I should not see the restricted schemes the client has access to which I do not have access to

@checkForErrors @installerUserAction
Scenario: Edit client restricted scheme list with overlapping restricted scheme access
	Given I am an Installer user
	And More than one restricted scheme exists
	And A client exists and has access to more than one restricted scheme
	And I have access to at least one, but not all, of the restricted schemes the client has access to
	When I view the client
	And I remove access for the client to one or more restricted schemes
	Then The client should not have access to the removed schemes
	And The restricted schemes I do not have access to should remain accessible to the client

@checkForErrors @installerUserAction
Scenario: Edit client unrestricted scheme list with overlapping restricted scheme access
	Given I am an Installer user
	And More than one restricted scheme exists
	And A client exists and has access to more than one restricted scheme
	And I have access to at least one, but not all, of the restricted schemes the client has access to
	When I view the client
	And I remove access for the client to one or more unrestricted schemes
	Then The client should not have access to the removed schemes
	And The restricted schemes I do not have access to should remain accessible to the client
