﻿@web @saveclient
Feature: SaveClient
	In order to effectively manage Fusion's user accounts
	As an Admin or Installer
	I want to save Client user account details

Scenario: Save a new Client user with no schemes assigned
	Given I am an Installer user
	And I have completed the user details form for a Client with no schemes assigned
	When I attempt to create the user
	Then save user is unsuccesful
	And I am informed that it is because there are no schemes assigned
	And the user is able to resubmit the form
	And No errors are logged

Scenario: Save an existing Client user with no schemes assigned
	Given I am an Installer user	
	And I have completed the user details form for a Client with no schemes assigned
	And the Client I am about to save already exists
	When I attempt to save the user
	Then save user is unsuccesful
	And I am informed that it is because there are no schemes assigned
	And the user is able to resubmit the form
	And No errors are logged

Scenario: Save a new Client user with schemes assigned
	Given I am an Installer user
	And I have completed the user details form for a Client with schemes assigned
	When I attempt to create the user
	Then save user is succesful
	And No errors are logged

Scenario: Save an existing Client user with schemes assigned
	Given I am an Installer user
	And I have completed the user details form for a Client with schemes assigned
	And the Client I am about to save already exists
	When I attempt to save the user
	Then save user is succesful
	And No errors are logged
