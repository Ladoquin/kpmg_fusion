﻿@web @createconsultant
Feature: CreateConsultantWithDefaultScheme
	As a: KPMG consultant
	I want: To already be assigned to the ABC sheme when I first log in to Fusion.
	So that: I don't see the (technically incorrect message) "You don't have any Schemes"

Scenario: The ABC scheme is assigned by default to new users in the role 'KPMG Consultant'
	Given I am an Admin user
	When I have completed the user details form for a KPMG Consultant
	And I attempt to create the user
	Then when the new user logs in they should have the ABC scheme as their active scheme
	And No errors are logged
