﻿@web @curveDataUpload
Feature: FillMissingCurveData
	As a KMPG consultant. 
	I want the last 25 years of curve data to be flat as per the last (25th) data point. 
	So that I can make rough 50yr projections. 

@curveDataUpload
Scenario: Fifty data points for a curve are observed in the database even though only twenty five are uploaded
	Given I have a curve data file with a valid schema
	And I am an Admin user
	When I upload the curve data file
	Then The curve data is present on the database
	And 50 data points are present in the database
	And No errors are logged
