﻿@web @curveDataUpload
Feature: CurveDataUpload
	As a: KPMG Admin 
	I want: a web UI to load forward curves 
	So that: I can control the curves that Fusion uses for projections.

@curveDataUpload
Scenario: Curve data file can be uploaded by an admin user
	Given I have a curve data file with a valid schema
	And I am an Admin user
	When I upload the curve data file
	Then the curve data file is accepted
	And No errors are logged

@curveDataUpload
Scenario: Curve data is persisted to the database after an upload
	Given I have a curve data file with a valid schema
	And I am an Admin user
	When I upload the curve data file
	Then The curve data is present on the database
	And No errors are logged

#where the new data exists for the same data points as the existing data
@curveDataUpload
Scenario: New curve data overwrites existing curve data
	Given I have a curve data file with a valid schema
	And I am an Admin user
	When Curve data has previously been uploaded
	And I upload the curve data file
	Then The new curve data overwrites the existing curve data
	And No errors are logged

#for data points not present in the new upload
@curveDataUpload
Scenario: Data from the latest upload doesn't delete old data
	Given I have a curve data file with a valid schema
	And I am an Admin user
	When Curve data has previously been uploaded
	And I upload the curve data file
	Then The new curve data doesn't delete existing curve data 
	And No errors are logged