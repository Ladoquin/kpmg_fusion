﻿declare @userid int
select @userid = UserId from UserProfile where username = 'fusionapptestuser'

delete webpages_UsersInRoles where UserId = @userid
delete webpages_Membership where UserId = @userid
delete UserProfilePasswordHistory where UserId = @userid
delete UserProfilePassword where UserId = @userid
delete UserProfileFavouriteSchemes where UserId = @userid
delete UserProfile where UserId = @userid
