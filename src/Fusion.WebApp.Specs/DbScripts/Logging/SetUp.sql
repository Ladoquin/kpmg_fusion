﻿insert into Log ([Date], [Thread], [Level], [Logger], [Message], [Exception]) 
values 
(getdate(), 'TestRunnerThread', 'DEBUG', 'Fusion.WebApp.Specs.Tests.Logging', 'Test DEBUG log entry', ''),
(getdate(), 'TestRunnerThread', 'ERROR', 'Fusion.WebApp.Specs.Tests.Logging', 'Test ERROR log entry', 'Test Exception')
