﻿declare @userid int

insert into UserProfile (UserName, FirstName, LastName, EmailAddress, TermsAccepted, PasswordMustChange, CreatedByUserId, IsSystemUser) 
values ('saveclienttestuser', 'foo', 'bar', 'saveclienttestuser@space01.co.uk', 1, 0, 1, 0)

set @userid = SCOPE_IDENTITY()

insert into webpages_Membership (UserId, [CreateDate], [IsConfirmed], [PasswordFailuresSinceLastSuccess], [Password], [PasswordSalt]) 
values (@userid, getdate(), 1, 0, 'ABTzBjxiVJq8e/hAsg4Y+nD64xWb86LybXjtKuj6jXHjTA5Rb9sDfS02rs9Dd4yXAQ==', '') -- Password1!

insert into webpages_UsersInRoles (UserId, RoleId) values (@userid, (select RoleId from webpages_Roles where RoleName = 'Client'))

insert into UserProfileSchemeDetail (UserId, SchemeDetailId) values (@userid, (select SchemeDetailId from SchemeDetail where SchemeName = 'The Space01 Test Scheme'))


