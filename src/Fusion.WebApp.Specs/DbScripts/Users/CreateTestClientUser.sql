﻿declare @userid int
declare @username varchar(20)

select @username = 'testuser' + cast(max(UserId) + 1 as varchar(5)) from UserProfile

insert into UserProfile (UserName, FirstName, LastName, EmailAddress, TermsAccepted, PasswordMustChange, CreatedByUserId, IsSystemUser) 
values (@username, 'test', 'user', 'clienttestuser@space01.co.uk', 1, 0, 1, 0)

set @userid = SCOPE_IDENTITY()

insert into webpages_Membership (UserId, [CreateDate], [IsConfirmed], [PasswordFailuresSinceLastSuccess], [Password], [PasswordSalt]) 
values (@userid, getdate(), 1, 0, 'ABTzBjxiVJq8e/hAsg4Y+nD64xWb86LybXjtKuj6jXHjTA5Rb9sDfS02rs9Dd4yXAQ==', '') -- Password1!

insert into webpages_UsersInRoles (UserId, RoleId) values (@userid, (select RoleId from webpages_Roles where RoleName = 'Client'))

select @userid [UserId]



