﻿if not exists (select * from CurveData where IndexName = 'TEST CURVE INDEX')
	insert into CurveData(IndexName) values ('TEST CURVE INDEX')
insert into CurveDataSpotRates (Maturity, CurveDataId, Rate, [Date]) values
(1, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.1, '2009-12-01'),
(2, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.2, '2009-12-01'),
(3, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.3, '2009-12-01'),
(4, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.4, '2009-12-01'),
(5, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.5, '2009-12-01'),
(6, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.6, '2009-12-01'),
(7, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.7, '2009-12-01'),
(8, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.8, '2009-12-01'),
(9, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.9, '2009-12-01'),
(1, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.1, '2014-10-01'),
(2, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.2, '2014-10-01'),
(3, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.3, '2014-10-01'),
(4, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.4, '2014-10-01'),
(5, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.5, '2014-10-01'),
(6, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.6, '2014-10-01'),
(7, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.7, '2014-10-01'),
(8, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.8, '2014-10-01'),
(9, (select Id from CurveData where IndexName = 'TEST CURVE INDEX'), 0.9, '2014-10-01')
insert into CurveImportDetail(ImportedOnDate, ImportedByUser) values (GetDate(), 'fusionapptestuseradmin')
