﻿declare @indexId int

select Id into #indexIds from FinancialIndices where Name like 'Test Composite Index%'

delete  IndexImportIndices where IndexId in (select Id from #indexIds)
delete  IndexValues where FinancialIndexId in (select Id from #indexIds)
delete  FinancialIndices where id in (select Id from #indexIds)
delete  IndexImportDetail where ImportedByUser = 'fusionapptestuseradmin'

drop table #indexIds