﻿delete ii
from IndexImportIndices ii
inner join FinancialIndices fi on ii.IndexId = fi.Id
where fi.Name like 'TEST INDEX%'
or IndexImportDetailId in (select IndexImportDetailId from IndexImportDetail where ImportedByUser = 'fusionapptestuseradmin')

delete iv
from FinancialIndices fi
inner join IndexValues iv on fi.Id = iv.FinancialIndexId
where fi.Name like 'TEST INDEX%'

delete FinancialIndices where Name like 'TEST INDEX%'

delete from BasisCache
delete from PensionSchemeCache

delete IndexImportDetail where ImportedByUser = 'fusionapptestuseradmin'





