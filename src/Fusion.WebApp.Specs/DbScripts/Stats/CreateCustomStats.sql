﻿declare @indexId int,
		@indexImportDetailId int

insert into FinancialIndices (name, Custom) values ('Test Composite Index', 1)
select @indexId = SCOPE_IDENTITY()

insert into IndexValues (FinancialIndexId, Value, Date) values (@indexId, 3098.707, '2010-03-24' )
insert into IndexValues (FinancialIndexId, Value, Date) values (@indexId, 3117.5292, '2010-03-25')

insert into IndexImportDetail (ImportedByUser) values ('fusionapptestuseradmin')
select @indexImportDetailId = SCOPE_IDENTITY()

insert into IndexImportIndices (IndexImportDetailId, IndexId) values (@indexImportDetailId, @indexId)