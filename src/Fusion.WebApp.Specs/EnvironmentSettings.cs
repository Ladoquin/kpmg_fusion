﻿namespace Fusion.WebApp.Specs
{
    using System.Configuration;
    
    class EnvironmentSettings
    {
        public static string TestDataPath
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestDataPath"]))
                    return @"..\..\..\..\test_data";
                return ConfigurationManager.AppSettings["TestDataPath"].Trim();
            }
        }

        public static string TestDBPath
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestDBPath"]))
                    return @"..\..\..\..\test_data\integration_tests_resources\db_files";
                return ConfigurationManager.AppSettings["TestDBPath"].Trim();
            }
        }
    }
}
