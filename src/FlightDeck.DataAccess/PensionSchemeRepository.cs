﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.Domain.Assets;
using FlightDeck.Domain.LDI;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.DataAccess
{
    public class PensionSchemeRepository : Repository<PensionScheme>, IPensionSchemeRepository
    {
        private string dataSourceTable = "SchemeDataSources";
        public PensionSchemeRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "PensionSchemes") { }

        //NB: indexRepository must be cached repository or it should belong to a different db scope to allow running two readers
        public PensionScheme GetById(int id, IFinancialIndexRepository indexRepository, IAssetClassRepository assetClassRepository)
        {
            var applicationRepository = new ApplicationRepository(connection, transaction);
            var applicationSettings = applicationRepository.GetAll();
            var insuranceBasisPointImpact = double.Parse(applicationSettings.Single(x => string.Equals("InsuranceBasisPointImpact", x.Key)).Value);
            var froMinTV = double.Parse(applicationSettings.Single(x => string.Equals("FROMinTVPercentage", x.Key)).Value);
            var froMaxTV = double.Parse(applicationSettings.Single(x => string.Equals("FROMaxTVPercentage", x.Key)).Value);
            var etvMinTV = double.Parse(applicationSettings.Single(x => string.Equals("ETVMinTVPercentage", x.Key)).Value);
            var etvMaxTV = double.Parse(applicationSettings.Single(x => string.Equals("ETVMaxTVPercentage", x.Key)).Value);
            var giltDurationDefault = int.Parse(applicationSettings.Single(x => string.Equals("DefaultGiltDuration", x.Key)).Value);
            var corpDurationDefault = int.Parse(applicationSettings.Single(x => string.Equals("DefaultCorpDuration", x.Key)).Value);
            var applicationParameters = new PensionSchemeApplicationParameters(insuranceBasisPointImpact, froMinTV, froMaxTV, etvMinTV, etvMaxTV, giltDurationDefault, corpDurationDefault);

            var basisRepository = new BasisRepository(connection, transaction);
            var bases = basisRepository.GetBySchemeId(id, indexRepository, 
                new LiabilityCashFlowRepository(connection, transaction), 
                new FinancialAssumptionRepository(connection, transaction),
                new PensionIncreaseAssumptionRepository(connection, transaction),
                new SalaryProgressionRepository(connection, transaction));

            var schemeAssetRepository = new SchemeAssetRepository(connection, transaction);
            var assetRepository = new AssetRepository(connection, transaction);
            var assetFundRepository = new AssetFundRepository(connection, transaction);
            var schemeAssets = schemeAssetRepository.GetByPensionSchemeId(id, assetRepository, assetClassRepository, indexRepository, assetFundRepository).ToList();
            var contributions = connection.Query<Contribution>(
                "select * from Contributions where PensionSchemeId = @PensionSchemeId", 
                new { PensionSchemeId = id }, transaction).ToList();

            var recovery = connection.Query<RecoveryPayment>(
                "select PensionSchemeId, Date, EndDate, Amount, Gap from RecoveryPayments where PensionSchemeId = @PensionSchemeId", //messed up the parameter order
                new {PensionSchemeId = id}, transaction);
            
            var namedProperties = GetNamedPropertyGroups(id);
            var marketInfoProvider = new MarketInfoProvider(indexRepository);
            var assetsGrowthProvider = new AssetsGrowthProvider(indexRepository);

            var volatilities = assetClassRepository.GetVolatilitiesByScheme(id).ToDictionary(x => x.Item1, x => x.Item2);
            var defaultVolatilityAssumptions = assetClassRepository.GetAll().ToDictionary(x => x.Type, x => x.Volatility);
            var varCorrelations = assetClassRepository.GetVarCorrelationsByScheme(id).ToDictionary(x => x.Item1, x => (IDictionary<AssetClassType, double>)x.Item2.ToDictionary(y => y.Item1, y => y.Item2));

            var sql = string.Format(GetSelectAll() + "s join {0} d on s.Id = d.PensionSchemeId where s.Id = @Id", dataSourceTable);
            return connection.Query<dynamic, dynamic, PensionScheme>(sql,
                (s, d) => new PensionScheme(id, s.Name, s.Reference, bases, schemeAssets, contributions, recovery,
                    new SchemeDataSource(d.Ias19Disclosure, d.ValuationReport, d.AssetData, d.MemberData, d.BenefitData),
                    volatilities, defaultVolatilityAssumptions, varCorrelations, namedProperties, marketInfoProvider, assetsGrowthProvider, s.ReturnOnAssets, s.IsGrowthFixed, s.Ias19ExpenseLoading,
                    (AccountingStandardType)s.AccountingDefaultStandard, s.AccountingIAS19Visible, s.AccountingFRS17Visible, s.AccountingUSGAAPVisible,
                    (SalaryType?)s.ABOSalaryType, applicationParameters,
                    new AnalysisParametersNullable(s.FROMinTVAmount, s.FROMaxTVAmount, s.ETVMinTVAmount, s.ETVMaxTVAmount), s.ContractingOutContributionRate, 
                    new LDICashflowBasis
                        (
                            s.LDIBasisName,
                            s.LDIOverrideAssumptions == null ? false : s.LDIOverrideAssumptions,
                            s.LDIDiscRateIndex == null ? null : indexRepository.GetById((int)s.LDIDiscRateIndex),
                            s.LDIDiscRatePremium == null ? 0 : s.LDIDiscRatePremium,
                            s.LDIInflationIndex == null ? null : indexRepository.GetById((int)s.LDIInflationIndex),
                            s.LDIInflationGap == null ? 0 : s.LDIInflationGap
                        ),
                    new SyntheticAssetIndices
                        (
                            s.SyntheticEquityIncomeIndexId == null ? null : indexRepository.GetById((int)s.SyntheticEquityIncomeIndexId),
                            s.SyntheticEquityOutgoIndexId == null ? null : indexRepository.GetById((int)s.SyntheticEquityOutgoIndexId),
                            s.SyntheticCreditIncomeIndexId == null ? null : indexRepository.GetById((int)s.SyntheticCreditIncomeIndexId),
                            s.SyntheticCreditOutgoIndexId == null ? null : indexRepository.GetById((int)s.SyntheticCreditOutgoIndexId)
                        ),
                    s.Funded,
                    s.DefaultBasis),
                    new { Id = id }, transaction, splitOn: "PensionSchemeId").Single();
        }

        private Dictionary<NamedPropertyGroupType, NamedPropertyGroup> GetNamedPropertyGroups(int id)
        {
            var namedProperties = new Dictionary<NamedPropertyGroupType, NamedPropertyGroup>();
            var q = connection.Query(GetSelectAll("NamedPropertyGroups") + "where PensionSchemeId = @Id",
                new {Id = id}, transaction).ToList();

            foreach (var g in q)
            {
                var type = (NamedPropertyGroupType) g.NamedPropertyGroupType;
                var props =
                    connection.Query(GetSelectAll("NamedProperties") + "where NamedPropertyGroupId = @Id", new {g.Id},
                        transaction).Select(x => new KeyValuePair<string, string>(x.Name, x.Value)).ToList();
                namedProperties.Add(type, new NamedPropertyGroup(type, g.Name, props));
            }
            return namedProperties;
        }

        public override int Insert(PensionScheme item)
        {
            return Insert((PensionSchemeBase)item);
        }

        public int Insert(PensionSchemeBase item)
        {
            var volatilityId = 0;
            if (item.Volatilities.Any())
            {
                volatilityId = (int)connection.Insert(new { Name = "Scheme" }, transaction, "VolatilitiesLookup", getId: true);
                foreach (var v in item.Volatilities.Keys)
                    connection.Insert(new { VolatilityId = volatilityId, AssetClassId = (int)v, Volatility = item.Volatilities[v] }, transaction, "Volatilities", getId: false);
            }
            else
            {
                volatilityId = connection.Query<int>("select Id from VolatilitiesLookup where Name = @name", new { name = "DEFAULT" }, transaction).Single();
            }

            var varCorrelationId = 0;
            if (item.VarCorrelations.Any())
            {
                varCorrelationId = (int)connection.Insert(new { Name = "Scheme" }, transaction, "VarCorrelationsLookup", getId: true);
                foreach (var ac in item.VarCorrelations.Keys)
                    foreach (var rac in item.VarCorrelations[ac].Keys)
                        connection.Insert(new { VarCorrelationId = varCorrelationId, AssetClassId = (int)ac, RelatedClassId = (int)rac, Variance = item.VarCorrelations[ac][rac] }, transaction, "VarCorrelations", getId: false);
            }
            else
            {
                varCorrelationId = connection.Query<int>("select Id from VarCorrelationsLookup where Name = @name", new { name = "DEFAULT" }, transaction).Single();
            }

            var ldiCashflowBasis = item.LDICashflowBasis ?? new LDICashflowBasis(null, false, null, 0, null, 0);
            var LDIDiscRateIndex = ldiCashflowBasis.DiscountRateIndex != null ? (int?)ldiCashflowBasis.DiscountRateIndex.Id : null;
            var LDIInflationIndex = ldiCashflowBasis.InflationIndex != null ? (int?)ldiCashflowBasis.InflationIndex.Id : null;
            var SyntheticEquityIncomeIndexId = item.SyntheticAssets != null && item.SyntheticAssets.EquityIncomeIndex != null ? (int?)item.SyntheticAssets.EquityIncomeIndex.Id : null;
            var SyntheticEquityOutgoIndexId = item.SyntheticAssets != null && item.SyntheticAssets.EquityOutgoIndex != null ? (int?)item.SyntheticAssets.EquityOutgoIndex.Id : null;
            var SyntheticCreditIncomeIndexId = item.SyntheticAssets != null && item.SyntheticAssets.CreditIncomeIndex != null ? (int?)item.SyntheticAssets.CreditIncomeIndex.Id : null;
            var SyntheticCreditOutgoIndexId = item.SyntheticAssets != null && item.SyntheticAssets.CreditOutgoIndex != null ? (int?)item.SyntheticAssets.CreditOutgoIndex.Id : null;

            var id = (int)connection.Insert(
                new {
                    item.Name, 
                    item.Reference,
                    item.ReturnOnAssets,
                    item.IsGrowthFixed,
                    item.Ias19ExpenseLoading,
                    varCorrelationId,
                    volatilityId,
                    item.ABOSalaryType, 
                    item.AccountingIAS19Visible,
                    item.AccountingFRS17Visible,
                    item.AccountingUSGAAPVisible,
                    item.AccountingDefaultStandard,
                    item.DefaultBasis,
                    item.AnalysisParameters.FROMinTVAmount,
                    item.AnalysisParameters.FROMaxTVAmount,
                    item.AnalysisParameters.ETVMinTVAmount,
                    item.AnalysisParameters.ETVMaxTVAmount,
                    item.ContractingOutContributionRate,
                    SyntheticEquityIncomeIndexId,
                    SyntheticEquityOutgoIndexId,
                    SyntheticCreditIncomeIndexId,
                    SyntheticCreditOutgoIndexId,
                    LDIBasisName = ldiCashflowBasis.BasisName,
                    LDIOverrideAssumptions = ldiCashflowBasis.OverrideAssumptions,
                    LDIDiscRateIndex,
                    LDIDiscRatePremium = ldiCashflowBasis.DiscountRatePremium,
                    LDIInflationIndex,
                    LDIInflationGap = ldiCashflowBasis.InflationGap,
                    item.Funded
                    }, 
                    transaction, TableName);
            var ds = item.DataSource;
            connection.Insert(new { PensionSchemeId = id, ds.Ias19Disclosure, ds.ValuationReport,
                ds.AssetData, ds.MemberData, ds.BenefitData }, transaction, dataSourceTable, getId: false);
            var basisRepository = new BasisRepository(connection, transaction);
            var schemeAssetRepository = new SchemeAssetRepository(connection, transaction);
            var assetRepository = new AssetRepository(connection, transaction);
            var assetFundRepository = new AssetFundRepository(connection, transaction);
            var masterBasisRepository = new MasterBasisRepository(connection, transaction);

            var masterBasisIdLookup = new Dictionary<string, int>();
            foreach (var masterBasisKey in item.UnderlyingBases.GroupBy(x => new { x.DisplayName, x.Type }))
            {
                var masterBasisId = masterBasisRepository.Insert(new MasterBasisKey(0, masterBasisKey.Key.Type, masterBasisKey.Key.DisplayName, id));
                masterBasisIdLookup.Add(masterBasisKey.Key.DisplayName, masterBasisId);
            }

            foreach (var b in item.UnderlyingBases)
            {
                b.PensionSchemeId = id; //TODO directly setting Ids here breaks the pattern of private ids, but otherwise we'd have 20 lines of new basis constructor args
                b.MasterBasisId = masterBasisIdLookup[b.DisplayName];
                basisRepository.Insert(b);
            }

            foreach (var schemeAsset in item.SchemeAssets)
            {
                schemeAsset.PensionSchemeId = id;

                // insert scheme
                var schemeAssetId = schemeAssetRepository.Insert(schemeAsset);
                foreach (var asset in schemeAsset.Assets)
                {
                    var assetId = assetRepository.Insert(new Asset(0, schemeAssetId, asset.Class, asset.IncludeInIas, asset.DefaultReturn, null));
                    foreach (var assetFund in asset.AssetFunds)
                    {
                        var isLdiFund = schemeAsset.LDIFund.ToLower() == assetFund.Name.ToLower();
                        assetFundRepository.Insert(new AssetFund(0, assetFund.Name, assetId, assetFund.AssetClass, assetFund.Index, assetFund.Amount, isLdiFund));
                    }
                }
                //assetRepository.Insert(new Asset(0, id, a.AssetClass, a.Index, a.Amount, a.IncludeInIas, a.EffectiveDate, a.DefaultReturn));
            }

            foreach (var c in item.ContributionRates)
            {
                connection.Insert(new { PensionSchemeId = id, c.Date, c.Employer, c.Employee }, transaction, "Contributions", getId: false);
            }

            foreach (var r in item.RecoveryPlanRaw)
            {
                connection.Insert(new { PensionSchemeId = id, r.Date, r.Amount, r.EndDate, r.Gap }, transaction, "RecoveryPayments", getId: false);
            }

            foreach (var namedPropertyGroup in item.NamedProperties.Values)
            {
                var groupId = (int)connection.Insert(new {NamedPropertyGroupType = (int)namedPropertyGroup.Type, PensionSchemeId = id, namedPropertyGroup.Name}, transaction, "NamedPropertyGroups");
                foreach (var valuePair in namedPropertyGroup.Properties)
                {
                    connection.Insert(new { NamedPropertyGroupId = groupId, Name = valuePair.Key, valuePair.Value }, transaction, "NamedProperties", getId: false);
                }
            }
            
            return id;
        }

        public CacheDependencies GetCacheDependencies(int schemeId)
        {
            var cacheDependencies = new CacheDependencies();

            cacheDependencies.Indices = connection.Query<string>(@"
                Create table #dependeeIndices(id int, name nvarchar(128))

                insert into #dependeeIndices select ps.LdiDiscRateIndex, f.Name 
                from PensionSchemes ps inner join FinancialIndices f on f.Id = ps.LDIDiscRateIndex  
                where ps.Id = @schemeId

                insert into #dependeeIndices select ps.LdiInflationIndex, f.Name 
                from PensionSchemes ps inner join FinancialIndices f on f.Id = ps.LdiInflationIndex 
                where ps.Id = @schemeId

                insert into #dependeeIndices select ps.SyntheticEquityIncomeIndexId, f.Name 
                from PensionSchemes ps inner join FinancialIndices f on f.Id = ps.SyntheticEquityIncomeIndexId 
                where ps.Id = @schemeId

                insert into #dependeeIndices select ps.SyntheticEquityOutgoIndexId, f.Name 
                from PensionSchemes ps inner join FinancialIndices f on f.Id = ps.SyntheticEquityOutgoIndexId
                where ps.Id = @schemeId

                insert into #dependeeIndices select ps.SyntheticCreditIncomeIndexId, f.Name 
                from PensionSchemes ps inner join FinancialIndices f on f.Id = ps.SyntheticCreditIncomeIndexId
                where ps.Id = @schemeId

                insert into #dependeeIndices select ps.SyntheticCreditOutgoIndexId, f.Name 
                from PensionSchemes ps inner join FinancialIndices f on f.Id = ps.SyntheticCreditOutgoIndexId 
                where ps.Id = @schemeId

                insert into #dependeeIndices select af.FinancialIndexId, f.Name
                from SchemeAssets sa inner join Assets a on sa.Id = a.SchemeAssetId
                inner join AssetFunds af on af.AssetId = a.Id
                inner join FinancialIndices f on f.Id = af.FinancialIndexId
                where sa.PensionSchemeId = @schemeId

                insert into #dependeeIndices select FinancialIndexId, f.Name
                from FinancialAssumptions fa inner join Bases b on fa.BasisId = b.Id
                inner join FinancialIndices f on f.Id = fa.FinancialIndexId
                where b.PensionSchemeId = @schemeId

                insert into #dependeeIndices select distinct pia.FinancialIndexId, f.Name
                from PensionIncreaseAssumptions pia inner join Bases b on pia.BasisId = b.Id
                inner join FinancialIndices f on f.Id = pia.FinancialIndexId
                where b.PensionSchemeId = @schemeId

                insert into #dependeeIndices Select f.Id, f.Name
                from Bases b inner join BasisBondYields bby on b.id = bby.BasisId
                inner join FinancialIndices f on f.Name = bby.[Index]
                where b.PensionSchemeId = @schemeId

                insert into #dependeeIndices select distinct b.RefGiltYieldId, f.Name
                from Bases b inner join FinancialIndices f on f.Id = b.RefGiltYieldId
                where b.PensionSchemeId = @schemeId

                select distinct name from #dependeeIndices
                drop table #dependeeIndices", new { schemeId }, transaction).ToList();

            cacheDependencies.MasterBasisIds = connection.Query<int>(@"
                select distinct MasterBasisId
                from Bases
                where PensionSchemeId = @schemeId", new { schemeId }, transaction).ToList();

            return cacheDependencies;
        }
    }
}