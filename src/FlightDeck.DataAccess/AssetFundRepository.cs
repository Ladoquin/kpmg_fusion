﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.Domain.Assets;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.DataAccess
{
    public class AssetFundRepository : Repository<AssetFund>, IAssetFundRepository 
    {
        public AssetFundRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "AssetFunds") {}

        public IEnumerable<AssetFund> GetByAssetId(int assetId, IFinancialIndexRepository indexRepository, AssetClass assetClass)
        {
            var assetFunds = connection.Query(GetSelectAll() + "where AssetId = @AssetId",
                new { AssetId = assetId }, transaction).Select(a =>
                    new AssetFund((int)a.Id, a.Name, (int)a.AssetId, assetClass, indexRepository.GetById((int)a.FinancialIndexId), (double)a.Amount, (bool)a.IsLDIFund)
                    );
            return assetFunds;
        }
        public override int Insert(AssetFund item)
        {
            return (int)connection.Insert(new
            {
                item.AssetId,
                FinancialIndexId = item.Index.Id,
                item.Name,
                item.Amount,
                item.IsLDIFund
            }, transaction, TableName);
        }
    }
}
