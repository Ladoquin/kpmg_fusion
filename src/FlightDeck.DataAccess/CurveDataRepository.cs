﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.DataAccess
{
    public class CurveDataRepository: Repository<CurveDataProjection>, ICurveDataRepository
    {
        private string spotRatesTable = "CurveDataSpotRates";
        private const int cacheSize = 5;
        public const string GiltsIndexName = "BOE NOMINAL GOVT CURVE";

        public CurveDataRepository(IDbConnection connection, IDbTransaction transaction) : 
            base(connection, transaction, "CurveData"){}

        public override CurveDataProjection GetById(int id)
        {
            //var curve = connection.Query(GetSelectAllById(), new {Id = id}, transaction).Single();
            //todo: factor spotrate population out of GetCurrentCurveProjectionsFromDB();
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetAll will mean get inflation curve and get gilt interest rate curve... so only 2 curves for now.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CurveDataProjection> GetAll()
        {
            List<CurveDataProjection> result = new List<CurveDataProjection>();

            var currentProjections = GetCurrentCurveProjectionsFromDB();

            return currentProjections;
        }

        private IEnumerable<CurveDataProjection> GetCurrentCurveProjectionsFromDB()
        {
            List<CurveDataProjection> curves = new List<CurveDataProjection>();

            var allCurves = connection.Query(GetSelectAll(), new { Date = DateTime.UtcNow.Date.AddYears(-cacheSize) }, transaction);

            foreach (var curve in allCurves)
            {
                var spots = connection.Query("select Maturity, CurveDataId, Rate, Date from CurveDataSpotRates where CurveDataId = @CurveDataId", new { CurveDataId = curve.Id }, transaction);
                
                List<SpotRate> spotRates = new List<SpotRate>();
                foreach(var spotRate in spots){
                    spotRates.Add(new SpotRate(spotRate.Maturity, (double)spotRate.Rate, spotRate.Date));
                }

                curves.Add(new CurveDataProjection(curve.Id, curve.IndexName, spotRates));
            }

            return curves;
        }

        public CurveDataProjection GetProjectionByDate(DateTime date)
        {
            var spots = connection.Query(@"select * 
                                            from CurveData d
                                            inner join CurveDataSpotRates c on d.Id = c.CurveDataId
                                            where 
                                            Date = (
                                                select top 1 Date from CurveDataSpotRates group by Date having Date <= @date order by Date DESC
                                            )",
                                new { date }, transaction);

            var spotRates = new List<SpotRate>(50);
            foreach (var spot in spots)
                spotRates.Add(new SpotRate(spot.Maturity, (double)spot.Rate, spot.Date));

            return new CurveDataProjection(spots.First().Id, spots.First().IndexName, spotRates);
        }

        public override int Insert(CurveDataProjection item)
        {
            var result = (int)connection.Insert(new { item.IndexName }, transaction, TableName);
            InsertSpotRates(item, result);
            return result;
        }

        private void InsertSpotRates(CurveDataProjection item, int curveId)
        {
            var spotRates = new List<dynamic>();
            foreach (var value in item.SpotRates)
            {
                spotRates.Add(new { Maturity = value.Maturity, CurveDataId = curveId, Rate = value.Rate, Date = value.Date });
            }
            connection.Execute(
                string.Format(@"insert {0}({1}, {2}, {3}, {4}) values (@{1}, @{2}, @{3}, @{4})", spotRatesTable, "Maturity", "CurveDataId", "Rate", "Date"), spotRates, transaction);
        }

        public override bool Update(CurveDataProjection item)
        {
            var result = connection.Update(new {item.Id, item.IndexName }, transaction, TableName);
            UpdateSpotRates(item);
            return result;
        }

        private void UpdateSpotRates(CurveDataProjection item)
        {
            foreach (var value in item.SpotRates)
            {
                var spotRate = new { Maturity = value.Maturity, CurveDataId = item.Id, Rate = value.Rate, Date = value.Date };
                if (connection.Execute(
                        string.Format(@"update {0} set {1} = @{1} where {2} = @{2} and {3} = @{3} and {4} = @{4}", spotRatesTable, "Rate", "CurveDataId", "Maturity", "Date"), spotRate, transaction) == 0)
                {
                    connection.Execute(
                        string.Format(@"insert {0}({1}, {2}, {3}, {4}) values(@{1}, @{2}, @{3}, @{4})", spotRatesTable, "CurveDataId", "Maturity", "Date", "Rate"), spotRate, transaction);
                }
            }
        }

        public CurveDataProjection GetByName(string name)
        {
            var curves = GetCurrentCurveProjectionsFromDB();// At time of writing there are only 2 i.e. inflation and gilt rates... any more and this will become too inefficient.

            if (curves == null || curves.Count() == 0)
                return null;

            var curve = curves.Where(c => c.IndexName == name).SingleOrDefault();

            if (curve == null)
                return null;

            return new CurveDataProjection(curve.Id, curve.IndexName, curve.SpotRates);
        }
    }
}
