﻿namespace FlightDeck.DataAccess
{
    using Dapper;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    
    public class UserProfilePasswordRepository : Repository<PasswordData>, IUserProfilePasswordRepository
    {
        public UserProfilePasswordRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "UserProfilePassword") { }

        public byte[] GetSalt(string username)
        {
            var record =
                connection.Query<byte[]>(@"select Salt " +
                    "from " + TableName + " p " +
                    "inner join UserProfile u on p.UserId = u.UserId " +
                    "where u.UserName = @username",
                    new { username },
                    transaction)
                .SingleOrDefault();

            return record;
        }

        public IEnumerable<byte[]> GetHistory(string username)
        {
            var history = 
                connection.Query(@"
                    select Password from UserProfilePasswordHistory h inner join UserProfile u on h.UserId = u.UserId where u.UserName = @username",
                    new { username },
                    transaction)
                .Select(x => (byte[])x.Password);

            return history;
        }

        public void Insert(string username, byte[] encryptedPassword, byte[] salt)
        {
            if (!connection.Query(
                   @"select * from " + TableName + " p " +
                    "inner join UserProfile u on p.UserId = u.UserId " +
                    "where u.UserName = @username", 
                    new { username },
                    transaction)
                .Any())
            {
                connection.Execute(
                   @"insert " + TableName + " (UserId, Salt) " +
                    "select u.UserId, @salt " +
                    "from UserProfile u " +
                    "where u.UserName = @username",
                    new { username, salt },
                    transaction);
            }

            connection.Execute(
               @"insert UserProfilePasswordHistory (UserId, Password, CreateDate) " +
                "select u.UserId, @encryptedPassword, @createDate " +
                "from UserProfile u " +
                "where u.UserName = @username",
                new { username, encryptedPassword, createDate = DateTime.Now },
                transaction);
        }

        public void TrimHistory(string username, int maxCount)
        {
            var savedCount = connection.Query(
                    @"select h.* from UserProfilePasswordHistory h " +
                        "inner join UserProfile u on h.UserId = u.UserId " +
                        "where u.UserName = @username",
                    new { username },
                    transaction)
                .Count();

            if (savedCount > maxCount)
            {
                var trim = (savedCount - maxCount).ToString();
                connection.Execute(
                    @"delete UserProfilePasswordHistory where Id in (" +
                        "select top " + trim + " h.Id " +
                        "from UserProfile u " +
                        "inner join UserProfilePasswordHistory h on u.UserId = h.UserId " +
                        "where u.UserName = @username " +
                        "order by h.CreateDate ASC)",
                    new { username },
                    transaction);
            }
        }
    }
}
