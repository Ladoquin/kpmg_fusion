﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.Domain.Assets;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.DataAccess
{
    //TODO introduce GetByParentId and find a way to inject cached repositories
    public class AssetRepository : Repository<Asset>, IAssetRepository
    {
        public AssetRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "Assets") {}

        //Note: indexRepository & assetFundRepository must be cached repository or it should belong to a different db scope to allow running two readers
        public IEnumerable<Asset> GetBySchemeAssetId(int schemeAssetId, IAssetClassRepository assetClassRepository, IFinancialIndexRepository indexRepository, IAssetFundRepository assetFundRepository)
        {
            var assets = connection.Query(GetSelectAll() + "where SchemeAssetId = @SchemeAssetId",
                new {SchemeAssetId = schemeAssetId}, transaction).Select(a =>
                    new Asset(a.Id, schemeAssetId, assetClassRepository.GetById((int)a.AssetClassId), a.IncludeInIas, a.DefaultReturn, assetFundRepository.GetByAssetId((int)a.Id, indexRepository, assetClassRepository.GetById((int)a.AssetClassId)).ToList()));
            return assets;
        }

        public override int Insert(Asset item)
        {
            return (int)connection.Insert(
                new
                {
                    AssetClassId = item.Class.Id,
                    item.IncludeInIas,
                    item.DefaultReturn,
                    item.SchemeAssetId
                }
                , transaction, TableName);
        }
    }

    public class CachedAssetClassRepository : Repository<AssetClass>, IAssetClassRepository
    {
        private readonly IDictionary<int, AssetClass> assetClasses;

        public CachedAssetClassRepository(IDbConnection connection, IDbTransaction transaction = null) : 
            base(connection, transaction, "AssetClasses")
        {
            assetClasses = connection.Query(GetSelectAll(), null, transaction).Select(a => 
                new AssetClass(a.Id, a.Name, (AssetCategory)a.Category, 
                    new AssetReturnSpec(a.MinReturn, a.MaxReturn, a.ReturnIncrement),
                    a.Volatility))
                        .ToDictionary(a => a.Id, a=>a);
        }

        public override AssetClass GetById(int id)
        {
            return assetClasses[id];
        }

        public override IEnumerable<AssetClass> GetAll()
        {
            return assetClasses.Values;
        }

        private Func<dynamic, Tuple<AssetClassType, double>> mapVolatilities = x => new Tuple<AssetClassType, double>
            (
                (AssetClassType)x.AssetClassId,
                (double)x.Volatility
            );

        public IEnumerable<Tuple<AssetClassType, double>> GetVolatilitiesByName(string name)
        {
            var volatilities =
                connection.Query(GetSelectAll("Volatilities") + " where VolatilityId = (select Id from VolatilitiesLookup where Name like @name)", new { name }, transaction)
                    .Select(mapVolatilities);

            return volatilities;        
        }

        public IEnumerable<Tuple<AssetClassType, double>> GetVolatilitiesByScheme(int schemeId)
        {
            var volatilities =
                connection.Query(GetSelectAll("Volatilities") + " where VolatilityId = (select VolatilityId from PensionSchemes where Id = @schemeId)", new { schemeId }, transaction)
                    .Select(mapVolatilities);

            return volatilities;        
        }

        private IEnumerable<Tuple<AssetClassType, IEnumerable<Tuple<AssetClassType, double>>>> mapVarCorrelations(IEnumerable<dynamic> records)
        {
            return records.Select(x => new
                    {
                        AssetClass = (AssetClassType)x.AssetClassId,
                        RelatedClass = (AssetClassType)x.RelatedClassId,
                        x.Variance
                    })
                    .GroupBy(x => x.AssetClass)
                    .Select(x => new Tuple<AssetClassType, IEnumerable<Tuple<AssetClassType, double>>>
                        (
                            x.Key,
                            x.Select(y => new Tuple<AssetClassType, double>(y.RelatedClass, y.Variance))
                        ));
        }

        public IEnumerable<Tuple<AssetClassType, IEnumerable<Tuple<AssetClassType, double>>>> GetVarCorrelationsByName(string name)
        {
            var varCorrelations =
                mapVarCorrelations(
                    connection.Query(GetSelectAll("VarCorrelations") + " where VarCorrelationId = (Select Id from VarCorrelationsLookup where Name = @name)", new { name }, transaction));

            return varCorrelations;
        }

        public IEnumerable<Tuple<AssetClassType, IEnumerable<Tuple<AssetClassType, double>>>> GetVarCorrelationsByScheme(int schemeId)
        {
            var varCorrelations =
                mapVarCorrelations(
                    connection.Query(GetSelectAll("VarCorrelations") + " where VarCorrelationId = (select VarCorrelationId from PensionSchemes where Id = @schemeId)", new { schemeId }, transaction));

            return varCorrelations;
        }

        public void UpdateVolatilities(string name, IDictionary<AssetClassType, double> volatilities)
        {
            foreach (var key in volatilities.Keys)
            {
                if (0 == connection.Execute("Update Volatilities Set Volatility = @vol where VolatilityId = (select Id from VolatilitiesLookup where Name = @name) and AssetClassId = @assetClassId", new { name, assetClassId = (int)key, vol = volatilities[key] }, transaction))
                {
                    connection.Execute("Insert Into Volatilities (VolatilityId, AssetClassId, Volatility) values ((select Id from VolatilitiesLookup where Name = @name), @assetClassId, @vol)", new { name, assetClassId = (int)key, vol = volatilities[key] }, transaction);
                }
            }
        }

        public void UpdateVarCorrelations(string name, IDictionary<AssetClassType, IDictionary<AssetClassType, double>> correlations)
        {
            foreach (var item1 in correlations.Keys)
            {
                foreach (var item2 in correlations[item1].Keys)
                {
                    var values = new { name, item1 = (int)item1, item2 = (int)item2, variance = correlations[item1][item2] };

                    if (0 == connection.Execute("Update VarCorrelations Set Variance = @variance Where VarCorrelationId = (select Id from VarCorrelationsLookup where Name = @name) and AssetClassId = @item1 And RelatedClassId = @item2", values, transaction))
                    {
                        connection.Execute("Insert Into VarCorrelations (VarCorrelationId, AssetClassId, RelatedClassId, Variance) values ((select Id from VarCorrelationsLookup where Name = @name), @item1, @item2, @variance)", values, transaction);
                    }
                }
            }
        }
    }
}