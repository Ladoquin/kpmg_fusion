﻿namespace FlightDeck.DataAccess
{
    using Dapper;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using System.Data;
    using System.Linq;
    using System;
    using System.Collections.Generic;

    public class PensionSchemeCacheRepository : Repository<PensionSchemeCache>, IPensionSchemeCacheRepository
    {
        public PensionSchemeCacheRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "PensionSchemeCache") { }

        public PensionSchemeCache GetBySchemeId(int pensionSchemeId)
        {
            var pensionSchemeCache = connection.Query<PensionSchemeCache>(
                GetSelectAll() + " where PensionSchemeId = @pensionSchemeId",
                new { pensionSchemeId },
                transaction)
                    .SingleOrDefault();

            return pensionSchemeCache;
        }

        public override void InsertOrUpdate(FlightDeck.DomainShared.PensionSchemeCache item)
        {
            if (connection.Execute("update PensionSchemeCache set LDIEvolutionItems = @LDIEvolutionItems where PensionSchemeId = @PensionSchemeId", item, transaction) == 0)
            {
                connection.Execute("insert into PensionSchemeCache (PensionSchemeId, LDIEvolutionItems) values (@PensionSchemeId, @LDIEvolutionItems)", item, transaction);
            }
        }

        public void DeleteAll()
        {
            connection.Execute("delete " + TableName, null, transaction);
        }

        public void DeleteAllByScheme(int schemeId)
        {
            connection.Execute("delete from " + TableName + " where PensionSchemeId = {schemeId}", null, transaction);
        }

        /// <summary>
        /// Note, this is different from DeleteAllByScheme(int) because this will delete the basis cache for all (historical) versions of a scheme.
        /// </summary>
        /// <param name="schemeName">Scheme name</param>
        public void DeleteAllByScheme(string schemeName)
        {
            var sql = @"
                delete psc
                from PensionSchemeCache psc
                inner join PensionSchemes ps on psc.PensionSchemeId = ps.Id
                where ps.Name = @schemeName";

            connection.Execute(sql, new { schemeName }, transaction);
        }

        public IEnumerable<int> GetActiveSchemesWithCache()
        {
            var schemeIds = connection.Query<int>("select psc.PensionSchemeId from " + TableName + " psc inner join SchemeDetail sd on psc.PensionSchemeId = sd.SchemeData_Id", transaction);

            return schemeIds;
        }
    }
}
