﻿namespace FlightDeck.DataAccess
{
    using Dapper;
    using FlightDeck.DomainShared;
    using System.Data;
    using System.Linq;

    public class SchemeDetailSourceRepository : Repository<SchemeDetailSource>
    {
        public SchemeDetailSourceRepository(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction, "SchemeDetailSource")
        {
        }

        public override SchemeDetailSource GetById(int id)
        {
            var schemeSource = connection.Query<SchemeDetailSource>(GetSelectAll() + " where SchemeDetailId = @id", new { id }, transaction).SingleOrDefault();

            return schemeSource;
        }

        public override void InsertOrUpdate(SchemeDetailSource item)
        {
            var data = new { item.SchemeDetailId, Source = item.Source != null ? item.Source.ToString() : null };

            if (connection.Execute("update " + TableName + " set SchemeDetailId = @SchemeDetailId, Source = @Source where SchemeDetailId = @SchemeDetailId", data, transaction) == 0)
            {
                connection.Execute("insert into " + TableName + " (SchemeDetailId, Source) values (@SchemeDetailId, @Source)", data, transaction);
            }
        }
    }
}
