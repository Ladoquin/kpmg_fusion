﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.Domain.Assets;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.DataAccess
{
    public class SchemeAssetRepository : Repository<SchemeAsset>, ISchemeAssetRepository
    {
        public SchemeAssetRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "SchemeAssets") {}

        public IEnumerable<SchemeAsset> GetByPensionSchemeId(int pensionSchemeId, IAssetRepository assetRepository, IAssetClassRepository assetClassRepository, IFinancialIndexRepository indexRepository, IAssetFundRepository assetFundRepository)
        {
            Func<double?, double> getDouble = inputVal =>
                {
                    return inputVal.HasValue ? inputVal.Value : 0;
                };

            return connection.Query(GetSelectAll() + "where PensionSchemeId = @PensionSchemeId",
                new { PensionSchemeId = pensionSchemeId }, transaction).Select(a =>
                    new SchemeAsset(a.Id, a.PensionSchemeId, a.EffectiveDate,
                                    a.LDIInForce == null ? false : (bool)a.LDIInForce, 
                                    string.Empty,
                                    getDouble((double?)a.InterestHedge), 
                                    getDouble((double?)a.InflationHedge), 
                                    getDouble((double?)a.PropSyntheticEquity), 
                                    getDouble((double?)a.PropSyntheticCredit),
                                    a.GiltDuration == null ? 0 : (int) a.GiltDuration,
                                    a.CorpDuration == null ? 0 : (int)a.CorpDuration,
                                    assetRepository.GetBySchemeAssetId((int)a.Id, assetClassRepository, indexRepository, assetFundRepository).ToList())
                                    );
        }

        public override int Insert(SchemeAsset item)
        {
            return (int)connection.Insert(new
            {
                item.PensionSchemeId,
                item.EffectiveDate,
                item.LDIInForce,
                item.InterestHedge,
                item.InflationHedge,
                item.PropSyntheticEquity,
                item.PropSyntheticCredit,
                item.GiltDuration,
                item.CorpDuration,
            }, transaction, TableName);
        }
    }
}
