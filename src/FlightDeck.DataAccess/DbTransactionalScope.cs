using System.Data;
namespace FlightDeck.DataAccess
{
    public class DbTransactionalScope : DbInteractionScope, IDbTransactionalScope
    {
        public DbTransactionalScope() : base(true)
        {
        }

        public DbTransactionalScope(IsolationLevel isolationLevel) : base(true, isolationLevel)
        {
        }

        public void Commit()
        {
            Transaction.Commit();
        }
    }
}