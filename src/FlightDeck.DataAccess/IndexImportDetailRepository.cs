﻿using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace FlightDeck.DataAccess
{
    public class IndexImportDetailRepository : Repository<IndexImportDetail>, IIndexImportDetailRepository
    {
        public IndexImportDetailRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "IndexImportDetail") { }

        public IndexImportDetail GetLatest()
        {
            var id = GetLatestId();

            return id > 0 ? GetById(id) : null;
        }

        public override IndexImportDetail GetById(int id)
        {
            IndexImportDetail result = connection.Query<IndexImportDetail>(GetSelectAll() + " where IndexImportDetailId = @id", new { id }).SingleOrDefault();
            List<int> indices = connection.Query<int>("SELECT IndexId FROM IndexImportIndices WHERE IndexImportDetailId = @id", new { id }).ToList();
            if (result != null && indices != null)
                result.Indices = indices;
            return result;
        }

        public override int Insert(IndexImportDetail item)
        {
            var result = connection.Insert(new { item.ImportedOnDate, item.ImportedByUser, item.ProducedOnDate, item.ProducedByUser, item.Spreadsheet, item.Comment, item.IndexStartDate, item.IndexEndDate, item.ExportFile }, transaction, TableName);
            item.IndexImportDetailId = (int)result;
            SaveImportedIndices(item);
            return item.IndexImportDetailId;
        }

        public override bool Update(IndexImportDetail item)
        {
            var cols = new string[] { "ImportedOnDate", "ImportedByUser", "ProducedOnDate", "ProducedByUser", "Spreadsheet", "Comment", "IndexStartDate", "IndexEndDate", "ExportFile" };
            return connection.Execute(
                string.Format(@"update {0} set {1} where IndexImportDetailId = @IndexImportDetailId", TableName, GetUpdateCols(cols)),
                new { item.ImportedOnDate, item.ImportedByUser, item.ProducedOnDate, item.ProducedByUser, item.Spreadsheet, item.Comment, item.IndexStartDate, item.IndexEndDate, item.IndexImportDetailId, item.ExportFile })
                > 0;
        }

        private void SaveImportedIndices(IndexImportDetail item)
        {
            if (item == null || item.IndexImportDetailId == 0)
                throw new ApplicationException("Invalid index import details");

            using (var db = new DbInteractionScope())
            {
                if (item.Indices != null && item.Indices.Any())
                {
                    var repo = ((IIndexImportIndicesRepository)db.GetRepository<IndexImportIndices>());

                    // delete existing mapping
                    repo.DeleteByImportId(item.IndexImportDetailId);

                    foreach (int i in item.Indices)
                        repo.Insert(new IndexImportIndices() { IndexId = i, IndexImportDetailId = item.IndexImportDetailId });
                }
            }
        }
    }

    public class IndexImportIndicesRepository : Repository<IndexImportIndices>, IIndexImportIndicesRepository
    {
        public IndexImportIndicesRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "IndexImportIndices") { }

        public override int Insert(IndexImportIndices item)
        {
            return connection.Execute(@"INSERT INTO IndexImportIndices VALUES (@IndexImportDetailId, @IndexId)", 
                        new { item.IndexImportDetailId, item.IndexId });
        }

        public void DeleteByImportId(int IndexImportDetailId)
        {
            connection.Execute(@"DELETE FROM IndexImportIndices WHERE IndexImportDetailId = @IndexImportDetailId", new { IndexImportDetailId });
        }
    }
}
