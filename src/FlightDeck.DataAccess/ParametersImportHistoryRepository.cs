﻿namespace FlightDeck.DataAccess
{
    using Dapper;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using System.Data;
    
    public class ParametersImportHistoryRepository : Repository<ParametersImportHistory>, IParametersImportHistoryRepository
    {
        public ParametersImportHistoryRepository(IDbConnection connection, IDbTransaction transaction = null)
            : base(connection, transaction, "ParametersImportHistory")
        {
        }

        public override IEnumerable<ParametersImportHistory> GetAll()
        {
            var results =
                connection.Query<ParametersImportHistory, UserProfile, ParametersImportHistory>(
                    "select * from " + TableName + " pih inner join UserProfile up on pih.UserId = up.UserId",
                    (pih, up) =>
                    {
                        pih.User = up;
                        return pih;
                    },
                    transaction,
                    splitOn: "UserId");

            return results;
        }

        public override int Insert(ParametersImportHistory item)
        {
            return connection.Execute(GetInsert() + " values (@UserId, @DateCreated)", new { item.UserId, item.DateCreated }, transaction);
        }
    }
}
