﻿using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace FlightDeck.DataAccess
{
    public class UserProfileRepository : Repository<UserProfile>, IUserProfileRepository
    {
        public UserProfileRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "UserProfile") { }

        public override UserProfile GetById(int id)
        {
            return get("where up.UserId = @UserId", new { UserId = id }).SingleOrDefault();
        }

        public UserProfile GetByUsername(string username)
        {
            return get("where up.Username like @username", new { username }).SingleOrDefault();
        }

        public UserProfile GetByEmail(string email)
        {
            return get("where up.EmailAddress like @email", new { email }).SingleOrDefault();
        }

        private IEnumerable<UserProfile> get(string where, object values, bool includeDocs = true)
        {
            var select =
                      "select * from "
                    + "UserProfile up "
                    + "inner join webpages_UsersInRoles r on up.UserId = r.UserId "
                    + "left join webpages_Membership m on up.UserId = m.UserId "
                    + "left join SchemeDetail sd on up.ActiveSchemeDetailId = sd.SchemeDetailId ";                    

            if (!string.IsNullOrEmpty(where))
                select += where;
            else
                select += "where up.IsSystemUser = 0";

            var users = connection.Query<UserProfile, UserRoleAssociation, UserProfileMembership, SchemeDetail, UserProfile>
                    (select,
                    (up, ura, upm, sd) =>
                    {
                        up.Role = (RoleType)(int)ura.RoleId;
                        up.PasswordFailuresSinceLastSuccess = upm != null ? upm.PasswordFailuresSinceLastSuccess : 0;
                        up.ActiveSchemeDetail = sd;
                        return up;
                    },
                    values,
                    transaction,
                    splitOn: "UserId,UserId,SchemeDetailId");

            foreach(var user in users)
            {
                //TODO apply some caching here so don't hit db for data we've already retrieved once
                user.SchemeDetails =
                    connection.Query<SchemeDetail>("select sd.* from SchemeDetail sd inner join UserProfileSchemeDetail upsd on sd.SchemeDetailId = upsd.SchemeDetailId where upsd.UserId = @UserId", new { user.UserId }, transaction);

                if (includeDocs && user.ActiveSchemeDetail != null)
                {
                    user.ActiveSchemeDetail.Documents =
                        connection.Query<SchemeDocument>("select * from SchemeDocuments where SchemeDetail_SchemeDetailId = @schemeDetailId", new { schemeDetailId = user.ActiveSchemeDetailId }, transaction)
                            .ToList();
                }

                user.FavouriteSchemes =
                    connection.Query<SchemeDetail>("select sd.* from SchemeDetail sd inner join UserProfileFavouriteSchemes upfs on sd.SchemeDetailId = upfs.SchemeDetailId where upfs.UserId = @UserId", new { user.UserId }, transaction);
            }

            return users;
        }

        public IEnumerable<UserProfile> GetMany(IEnumerable<int> userIds)
        {
            if (userIds.Count() > 2100)
            {
                throw new Exception("Dapper argument length exceeded");
            }
            return get("where up.UserId in @userIds", new { userIds }, false);
        }

        public IEnumerable<UserProfileSearchKeys> GetSearchKeys(IEnumerable<int> excludeUserIds)
        {
            var exclusions = !excludeUserIds.IsNullOrEmpty()
                ? "and " + (excludeUserIds.Count() == 1
                    ? "up.UserId <> " + excludeUserIds.First().ToString()
                    :  excludeUserIds.Select(x => x.ToString()).Aggregate((x, y) => "up.UserId <> " + x + " and up.UserId <> " + y))
                : string.Empty;

            return
                connection.Query(
                    "  select "
                    + "up.UserId, up.LastName, up.CreatedByUserId, r.RoleId, upsd.SchemeDetailId "
                    + "from  "
                    + "UserProfile up "
                    + "left join UserProfileSchemeDetail upsd on up.UserId = upsd.UserId "
                    + "left join webpages_UsersInRoles r on up.UserId = r.UserId "
                    + "where up.IsSystemuser = 0 "
                    + exclusions,
                    null,
                    transaction)
                .GroupBy(x => new { x.UserId, x.LastName, x.CreatedByUserId, x.RoleId })
                .Select(x => new UserProfileSearchKeys
                {
                    UserId = x.Key.UserId,
                    LastName = x.Key.LastName,
                    CreatedByUserId = x.Key.CreatedByUserId,
                    Role = (RoleType)x.Key.RoleId,
                    AccessibleSchemeIds = x.Where(val => val.SchemeDetailId != null).Select(val => (int)val.SchemeDetailId).ToList()
                });
        }

        public override int Insert(UserProfile item)
        {
            var createTransaction = transaction == null;

            var tx = transaction ?? connection.BeginTransaction();

            item.UserId = (int)connection.Insert(new { item.UserName, item.FirstName, item.LastName, item.EmailAddress, item.TermsAccepted, item.PasswordMustChange, item.ActiveSchemeDetailId }, tx, TableName);

            updateSchemes(item, tx);

            if (createTransaction)
            {
                tx.Commit();
            }

            return item.UserId;
        }

        public override bool Update(UserProfile item)
        {
            var createTransaction = transaction == null;

            var tx = transaction ?? connection.BeginTransaction();

            var cols = new string[] { "UserName", "FirstName", "LastName", "EmailAddress", "TermsAccepted", "PasswordMustChange", "ActiveSchemeDetailId" };
            if (connection.Execute(
                string.Format(@"update {0} set {1} where UserId = @UserId", TableName, GetUpdateCols(cols)),
                new { item.UserName, item.FirstName, item.LastName, item.EmailAddress, item.TermsAccepted, item.PasswordMustChange, item.UserId, item.ActiveSchemeDetailId },
                tx)
                > 0)
            {
                updateSchemes(item, tx);

                if (createTransaction)
                {
                    tx.Commit();
                }

                return true;
            }

            return false;
        }

        public void PurgeRestrictedSchemeAccess(int schemeId, IEnumerable<int> roleIds)
        {
            connection.Execute(@"
                update      up
                set         up.ActiveSchemeDetailId = null
                from        UserProfile up
                inner join  webpages_UsersInRoles r on up.UserId = r.UserId
                left join   UserProfileSchemeDetail upsd on up.UserId = upsd.UserId and up.ActiveSchemeDetailId = upsd.SchemeDetailId
                where       r.RoleId in @roleIds
                and         up.ActiveSchemeDetailId = @schemeid
                and         upsd.SchemeDetailId is null",
                new { schemeId, roleIds },
                transaction);
        }

        public void PurgeUnrestrictedSchemeAccess(int schemeId, IEnumerable<int> roleIds)
        {
            connection.Execute(@"
                delete		upsd
                from		UserProfile up
                inner join	webpages_UsersInRoles r on up.UserId = r.UserId
                inner join	UserProfileSchemeDetail upsd on up.UserId = upsd.UserId
                where		r.RoleId in @roleIds
                and			upsd.SchemeDetailId = @schemeid",
                new { schemeId, roleIds },
                transaction);
        }

        private void updateSchemes(UserProfile item, IDbTransaction tx)
        {
            connection.Execute("delete UserProfileSchemeDetail where userId = @UserId", new { item.UserId }, tx);

            if (!item.SchemeDetails.IsNullOrEmpty())
            {
                var schemeIds = "";
                foreach (var scheme in item.SchemeDetails)
                {
                    if (schemeIds.Length > 0) schemeIds += ", ";
                    schemeIds += "(@UserId, " + scheme.SchemeDetailId.ToString() + ")";
                }

                connection.Execute("insert into UserProfileSchemeDetail (UserId, SchemeDetailId) values " + schemeIds, new { item.UserId }, tx);
            }
        }

        public void Delete(string username)
        {
            var createTransaction = transaction == null;

            var tx = transaction ?? connection.BeginTransaction();

            connection.Execute("delete ClientAssumptions where CreatedByUserId = (select UserId from " + this.TableName + " where Username = @Username)", new { username }, tx);

            connection.Execute("delete UserProfilePasswordHistory where UserId = (select UserId from " + this.TableName + " where Username = @Username)", new { username }, tx);

            connection.Execute("delete UserProfilePassword where UserId = (select UserId from " + this.TableName + " where Username = @Username)", new { username }, tx);

            connection.Execute("delete webpages_UsersInRoles where UserId = (select UserId from " + this.TableName + " where Username = @Username)", new { username }, tx);

            connection.Execute("delete UserProfileSchemeDetail where UserId = (select UserId from " + this.TableName + " where Username = @Username)", new { username }, tx);

            connection.Execute("delete UserProfileFavouriteSchemes where UserId = (select UserId from " + this.TableName + " where Username = @Username)", new { username }, tx);

            connection.Execute("delete " + this.TableName + " where Username = @username", new { username }, tx);

            if (createTransaction)
            {
                tx.Commit();
            }
        }

        public void Delete(IEnumerable<int> userIds)
        {
            var createTransaction = transaction == null;

            var tx = transaction ?? connection.BeginTransaction();

            connection.Execute("delete ClientAssumptions where CreatedByUserId in @userIds", new { userIds }, tx);

            connection.Execute("delete UserProfilePasswordHistory where UserId in @userIds", new { userIds }, tx);

            connection.Execute("delete UserProfilePassword where UserId in @userIds", new { userIds }, tx);

            connection.Execute("delete webpages_UsersInRoles where UserId in @userIds", new { userIds }, tx);

            connection.Execute("delete UserProfileSchemeDetail where UserId in @userIds", new { userIds }, tx);

            connection.Execute("delete UserProfileFavouriteSchemes where UserId in @userIds", new { userIds }, tx);

            connection.Execute("delete " + this.TableName + " where UserId in @userIds", new { userIds }, tx);

            if (createTransaction)
            {
                tx.Commit();
            }
        }

        public void AddFavouriteScheme(int userId, int schemeDetailId)
        {
            var createTransaction = transaction == null;

            var tx = transaction ?? connection.BeginTransaction(); 
            
            connection.Execute("insert into UserProfileFavouriteSchemes (UserId, SchemeDetailId) values (@userId, @schemeDetailId)", new { userId, schemeDetailId }, tx);

            if (createTransaction)
                tx.Commit();
        }

        public void RemoveFavouriteScheme(int userId, int schemeDetailId)
        {
            var createTransaction = transaction == null;

            var tx = transaction ?? connection.BeginTransaction();

            connection.Execute("delete from UserProfileFavouriteSchemes where UserId = @userId and SchemeDetailId = @schemeDetailId", new { userId, schemeDetailId }, tx);

            if (createTransaction)
                tx.Commit();
        }
    }
}