﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.DataAccess
{
    public class SalaryProgressionRepository : Repository<SalaryProgression>, ISalaryProgressionRepository
    {
        public SalaryProgressionRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "SalaryProgressions") {}

        public IEnumerable<SalaryProgression> GetByBasisId(int basisId)
        {
            var sp = connection.Query(GetSelectAll() + "where BasisId = @BasisId", new {BasisId = basisId}, transaction);
            return sp.Select(s => new SalaryProgression(s.Id, s.BasisId, s.MinAge, s.MaxAge, s.RetirementAge, 
                connection.Query<dynamic>("select * from SalaryRolls where SalaryProgressionId = @ProgressionId", 
                    new {ProgressionId = s.Id}, transaction).ToDictionary(x => (int) x.Year, x => (double) x.Salary))).ToList();
        }

        public override int Insert(SalaryProgression item)
        {
            var id =  (int)connection.Insert(new {item.BasisId, item.MinAge, item.MaxAge, item.RetirementAge}, transaction, TableName);
            foreach (var r in item.SalaryRoll)
            {
                connection.Insert(new { SalaryProgressionId = id, Year = r.Key, Salary = r.Value }, transaction, "SalaryRolls", getId: false);
            }
            return id;
        }
    }
}