﻿namespace FlightDeck.DataAccess
{
    using Dapper;
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using System.Data;

    internal class MasterBasisRepository : Repository<MasterBasisKey>
    {
        public MasterBasisRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "MasterBases")
        {
        }
    }
}
