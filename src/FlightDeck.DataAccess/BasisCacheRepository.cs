﻿namespace FlightDeck.DataAccess
{
    using Dapper;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using System.Data;
    using System.Linq;
    using System;

    public class BasisCacheRepository : Repository<BasisCache>, IBasisCacheRepository
    {
        public BasisCacheRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "BasisCache") { }

        public BasisCache GetByMasterBasisId(int masterbasisId)
        {
            var basisCache = connection.Query<BasisCache>(
                GetSelectAll() + " where MasterBasisId = @masterbasisId",
                new { masterbasisId },
                transaction)
                    .SingleOrDefault();

            return basisCache;
        }

        public override void InsertOrUpdate(BasisCache item)
        {
            if (connection.Execute("update BasisCache set BasisEvolutionData = @BasisEvolutionData where MasterBasisId = @MasterBasisId", item, transaction) == 0)
            {
                connection.Execute("insert into BasisCache (MasterBasisId, BasisEvolutionData) values (@MasterBasisId, @BasisEvolutionData)", item, transaction);
            }
        }

        public void DeleteAll()
        {
            connection.Execute("delete " + TableName, null, transaction);
        }

        public void DeleteAllByScheme(int schemeId)
        {
            connection.Execute("delete from "+ TableName + " where MasterBasisId in (select id from MasterBases where PensionSchemeId = " + schemeId + " )", null, transaction);
        }

        /// <summary>
        /// Note, this is different from DeleteAllByScheme(int) because this will delete the basis cache for all (historical) versions of a scheme.
        /// </summary>
        /// <param name="schemeName">Scheme name</param>
        public void DeleteAllByScheme(string schemeName)
        {
            var sql = @"
                delete bc
                from BasisCache bc
                inner join MasterBases mb on bc.MasterBasisId = mb.Id
                inner join PensionSchemes ps on mb.PensionSchemeId = ps.Id
                where ps.Name = @schemeName";

            connection.Execute(sql, new { schemeName }, transaction);
        }
    }
}
