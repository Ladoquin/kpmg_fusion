﻿using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using System.Collections.Generic;
using System.Data;

namespace FlightDeck.DataAccess
{
    public class HistoricSchemeDetailRepository : Repository<HistoricSchemeDetail>, IHistoricSchemeDetailRepository
    {
        public HistoricSchemeDetailRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "HistoricSchemeDetail") { }

        public IEnumerable<HistoricSchemeDetail> GetAll(int schemeId)
        {
            return connection.Query<HistoricSchemeDetail>(GetSelectAll() + " where [SourceSchemeDetailId] = @schemeId", new { schemeId }, transaction);
        }

        public override int Insert(HistoricSchemeDetail item)
        {
            return
                (int)connection.Insert(new
                {
                    item.SourceSchemeDetailId,
                    item.SchemeName,
                    item.WelcomeMessage,
                    item.Logo,
                    item.ContactName,
                    item.ContactNumber,
                    item.ContactEmail,
                    item.SchemeData_SchemeRef,
                    item.SchemeData_AnchorDate,
                    item.SchemeData_Id,
                    item.SchemeData_ImportedOnDate,
                    item.SchemeData_ImportedByUser,
                    item.SchemeData_ProducedOnDate,
                    item.SchemeData_ProducedByUser,
                    item.SchemeData_Spreadsheet,
                    item.SchemeData_Comment,
                    item.UpdatedOnDate,
                    item.UpdatedByUser,
                    item.HasVolatilities,
                    item.HasVarCorrelations,
                    item.IsRestricted,
                    item.SchemeData_ExportFile,
                    item.AccountingDownloadsPassword,
                    item.DataCaptureVersion
                },
                    transaction, TableName);
        }
    }
}
