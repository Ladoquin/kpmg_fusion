﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.DataAccess
{
    public class FinancialAssumptionRepository : Repository<FinancialAssumption>, IFinancialAssumptionRepository
    {
        public FinancialAssumptionRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "FinancialAssumptions")
        {
        }

        public IEnumerable<FinancialAssumption> GetByBasisId(int basisId, IFinancialIndexRepository indexRepository)
        {
            var assumptionData = connection.Query(GetSelectAll() + " where BasisId = @BasisId", new {BasisId = basisId}, transaction);
            return assumptionData.Select(a => 
                new FinancialAssumption(a.Id, a.BasisId, a.InitialValue, 
                    indexRepository.GetById((int)a.FinancialIndexId), (AssumptionType) a.AssumptionType,
                    a.Name, a.IsVisible)).ToList();
        }

        public override int Insert(FinancialAssumption item)
        {
            return (int)connection.Insert(new { item.BasisId, item.InitialValue, AssumptionType = (int)item.Type, 
                FinancialIndexId = item.Index.Id, item.Name, item.IsVisible }, transaction, TableName);
        }
    }
}