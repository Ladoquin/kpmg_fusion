﻿using Dapper;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FlightDeck.DataAccess
{
    public class ApplicationRepository : Repository<ApplicationValue>, IApplicationRepository
    {
        public ApplicationRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "Application") {}

        public ApplicationValue GetByKey(string key)
        {
            var obj = connection.Query<ApplicationValue>(GetSelectAll() + " where [Key] = @key", new { key }, transaction).FirstOrDefault();

            return obj;
        }

        public override void InsertOrUpdate(ApplicationValue item)
        {
            if (connection.Execute("update Application set Value = @Value where [Key] = @Key", new { item.Key, item.Value }, transaction) == 0)
            {
                connection.Execute("insert into Application ([Key], Value) values (@Key, @Value)", new { item.Key, item.Value }, transaction);
            }
        }

        public string GetTrackerVersionSupported()
        {
            var v = connection.Query("select TrackerVersionSupported from [Version]").First();
            return v.TrackerVersionSupported;
        }
    }
}
