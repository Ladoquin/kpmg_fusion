namespace FlightDeck.DataAccess
{
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using System;
    using System.Data;
    using System.Data.SqlClient;

    public class LogDbInteractionScope : ILogDbInteractionScope, IDisposable
    {
        private static string connectionString;

        private readonly IDbConnection connection;
        protected readonly IDbTransaction Transaction;

        public static void Configure(string connString)
        {
            connectionString = connString;
        }

        internal IDbConnection Connection {get { return connection; }}

        public LogDbInteractionScope() : this(false) { }

        protected LogDbInteractionScope(bool isTransactional)
        {
            connection = new SqlConnection(connectionString);
            connection.Open();
            Transaction = isTransactional ? connection.BeginTransaction(IsolationLevel.ReadCommitted) : null;
        }

        public IRepository<T> GetRepository<T>() where T : class
        {
            var paramType = typeof (T);

            if (paramType == typeof(Log))
                return (IRepository<T>)new LogRepository(connection, Transaction);

            throw new ArgumentException(string.Format("{0} has no repository.", paramType.Name));
        }

        public void Dispose()
        {
            if(Transaction != null)
                Transaction.Dispose();
            connection.Dispose();
        }
    }
}