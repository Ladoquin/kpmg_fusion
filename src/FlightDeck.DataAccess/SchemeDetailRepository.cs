﻿using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FlightDeck.DataAccess
{
    public class SchemeDetailRepository : Repository<SchemeDetail>, ISchemeDetailRepository
    {
        public SchemeDetailRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "SchemeDetail") {}

        public override SchemeDetail GetById(int id)
        {
            var scheme = connection.Query<SchemeDetail>(GetSelectAll() + " where SchemeDetailId = @id", new { id }, transaction).SingleOrDefault();

            addProperties(scheme);

            return scheme;
        }

        public SchemeDetail GetByName(string schemeName)
        {
            var scheme = connection.Query<SchemeDetail>(GetSelectAll() + " where SchemeName like @schemeName", new { schemeName = schemeName }, transaction).SingleOrDefault();

            addProperties(scheme);

            return scheme;
        }        

        private SchemeDetail addProperties(SchemeDetail scheme)
        {
            if (scheme != null)
            {
                scheme.Documents =
                    connection.Query<SchemeDocument>(GetSelectAll("SchemeDocuments") + " where SchemeDetail_SchemeDetailId = @SchemeDetailId", new { scheme.SchemeDetailId }, transaction)
                        .ToList();
            }

            return scheme;
        }

        public IEnumerable<SchemeDetail> GetByNames(IEnumerable<string> schemeNames)
        {
            var schemes = connection.Query<SchemeDetail>(GetSelectAll() + " where SchemeName in @schemeNames", new { schemeNames = schemeNames }, transaction);

            return schemes;
        }

        public IEnumerable<int> GetAllUnrestricted()
        {
            var ids = connection.Query<int>("select SchemeDetailId from " + TableName + " where IsRestricted = 0", transaction);

            return ids;
        }

        public IEnumerable<SchemeDetail> GetByCreator(string username)
        {
            var schemes = connection.Query<SchemeDetail>("select sd.* from " + TableName + " sd inner join UserProfile up on sd.CreatedByUserId = up.UserId where up.Username = @username", new { username }, transaction);

            return schemes;
        }

        public override int Insert(SchemeDetail item)
        {
            item.SchemeDetailId =
                (int)connection.Insert(new
                    {
                        item.SchemeName,
                        item.CurrencySymbol,
                        item.WelcomeMessage,
                        item.Logo,
                        item.ContactName,
                        item.ContactNumber,
                        item.ContactEmail,
                        item.SchemeData_SchemeRef,
                        item.SchemeData_AnchorDate,
                        item.SchemeData_Id,
                        item.SchemeData_ImportedOnDate,
                        item.SchemeData_ImportedByUser,
                        item.SchemeData_ProducedOnDate,
                        item.SchemeData_Spreadsheet,
                        item.SchemeData_Comment,
                        item.UpdatedOnDate,
                        item.UpdatedByUser,
                        item.CreatedByUserId,
                        item.HasVolatilities,
                        item.HasVarCorrelations,
                        item.IsRestricted,
                        item.SchemeData_ExportFile,
                        item.AccountingDownloadsPassword,
                        item.DataCaptureVersion
                    },
                    transaction, TableName);

            updateDocuments(item.SchemeDetailId, item.Documents);

            return item.SchemeDetailId;
        }

        public override bool Update(SchemeDetail item)
        {
            var cols = new string[] { "SchemeName", "CurrencySymbol", "WelcomeMessage", "Logo", "ContactName", "ContactNumber", "ContactEmail", "SchemeData_SchemeRef", "SchemeData_AnchorDate", "SchemeData_Id", "SchemeData_ImportedOnDate", "SchemeData_ImportedByUser", "SchemeData_ProducedOnDate", "SchemeData_ProducedByUser", "SchemeData_Spreadsheet", "SchemeData_Comment", "UpdatedOnDate", "UpdatedByUser", "HasVolatilities", "HasVarCorrelations", "IsRestricted", "SchemeData_ExportFile", "AccountingDownloadsPassword", "DataCaptureVersion" };
            var update = connection.Execute(
                string.Format(@"update {0} set {1} where SchemeDetailId = @SchemeDetailId", TableName, GetUpdateCols(cols)),
                new { item.SchemeName, item.CurrencySymbol, item.WelcomeMessage, item.Logo, item.ContactName, item.ContactNumber, item.ContactEmail, item.SchemeData_SchemeRef, item.SchemeData_AnchorDate, item.SchemeData_Id, item.SchemeData_ImportedOnDate, item.SchemeData_ImportedByUser, item.SchemeData_ProducedOnDate, item.SchemeData_ProducedByUser, item.SchemeData_Spreadsheet, item.SchemeData_Comment, item.UpdatedOnDate, item.UpdatedByUser, item.SchemeDetailId, item.HasVolatilities, item.HasVarCorrelations, item.IsRestricted, item.SchemeData_ExportFile, item.AccountingDownloadsPassword, item.DataCaptureVersion },
                transaction)
                > 0;
            if (update)
            {
                item.Documents = updateDocuments(item.SchemeDetailId, item.Documents);
            }
            return update;
        }

        private IList<SchemeDocument> updateDocuments(int schemeDetailId, IList<SchemeDocument> documents)
        {
            if (documents == null || documents.Count == 0)
            {
                connection.Execute(@"delete SchemeDocuments where SchemeDetail_SchemeDetailId = @schemeDetailId", new { schemeDetailId }, transaction);
            }
            if (documents != null)
            {                
                foreach (var doc in documents)
                {
                    if (doc.SchemeDocumentId == 0)
                    {
                        doc.SchemeDocumentId = (int)connection.Insert(new { doc.Name, doc.FileName, doc.Content, doc.ContentType, SchemeDetail_SchemeDetailId = schemeDetailId }, transaction, "SchemeDocuments");
                    }
                    else
                    {
                        var cols = doc.Content != null
                            ? new string[] { "Name", "FileName", "Content", "ContentType" }
                            : new string[] { "Name", "FileName" };
                        connection.Execute(
                            string.Format(@"update SchemeDocuments set {0} where SchemeDocumentId = @SchemeDocumentId", GetUpdateCols(cols)),
                            new { doc.Name, doc.FileName, doc.Content, doc.ContentType, doc.SchemeDocumentId },
                            transaction);
                    }
                }
            }
            return documents;
        }

        public void Delete(SchemeDetail scheme)
        {
            var createTransaction = transaction == null;

            var tx = transaction ?? connection.BeginTransaction();

            connection.Execute(@"delete ClientAssumptions where SchemeDetailId = @SchemeId", new { SchemeId = scheme.SchemeDetailId }, tx);

            connection.Execute(@"update UserProfile set ActiveSchemeDetailId = null where ActiveSchemeDetailId = @SchemeId", new { SchemeId = scheme.SchemeDetailId }, tx);

            connection.Execute(@"delete UserProfileSchemeDetail where SchemeDetailId = @SchemeId", new { SchemeId = scheme.SchemeDetailId }, tx);

            connection.Execute(@"delete UserProfileFavouriteSchemes where SchemeDetailId = @SchemeId", new { SchemeId = scheme.SchemeDetailId }, tx);

            connection.Execute(@"delete SchemeDocuments where SchemeDetail_SchemeDetailId = @SchemeId", new { SchemeId = scheme.SchemeDetailId }, tx);

            connection.Execute(@"delete SchemeDetailSource where SchemeDetailId = @SchemeDetailId", new { scheme.SchemeDetailId }, tx);

            connection.Execute(@"delete SchemeDetail where SchemeDetailId = @SchemeId", new { SchemeId = scheme.SchemeDetailId }, tx);

            if (createTransaction)
            {
                tx.Commit();
            }
        }

        public void DeleteDocument(SchemeDocument doc)
        {
            connection.Execute(@"delete SchemeDocuments where SchemeDocumentId = @SchemeDocumentId", new { doc.SchemeDocumentId }, transaction);
        }
    }
}