﻿namespace FlightDeck.DataAccess
{
    using Dapper;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using System.Data;

    public class UserProfileMembershipRepository : Repository<UserProfileMembership>, IUserProfileMembershipRepository
    {
        public UserProfileMembershipRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "webpages_Membership") { }

        public void Unlock(int userId)
        {
            connection.Execute("update " + TableName + " set PasswordFailuresSinceLastSuccess = 0 where UserId = @userId", new { userId }, transaction);
        }
    }
}
