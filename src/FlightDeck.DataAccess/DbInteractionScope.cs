using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.Domain.Assets;

namespace FlightDeck.DataAccess
{
    public class DbInteractionScope : IDisposable, IDbInteractionScope
    {
        private static string connectionString;

        private readonly IDbConnection connection;
        protected readonly IDbTransaction Transaction;

        public static void Configure(string connString)
        {
            connectionString = connString;
        }

        internal IDbConnection Connection {get { return connection; }}

        public DbInteractionScope() : this(false) { }

        public DbInteractionScope(IsolationLevel isolationLevel) : this(true, isolationLevel) { }

        protected DbInteractionScope(bool isTransactional, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            connection = new SqlConnection(connectionString);
            connection.Open();
            Transaction = isTransactional ? connection.BeginTransaction(isolationLevel) : null;
        }

        public int GetVersionNumber()
        {
            var v = connection.Query("select Major from [Version]").First();
            return v.Major;
        }

        public IRepository<T> GetRepository<T>() where T : class
        {
            var paramType = typeof (T);

            if (paramType == typeof(PensionScheme))
                return (IRepository<T>)new PensionSchemeRepository(connection, Transaction);

            if (paramType == typeof (FinancialIndex))
                return (IRepository<T>) new FinancialIndexRepository(connection, Transaction);

            if (paramType == typeof(CurveDataProjection))
                return (IRepository<T>)new CurveDataRepository(connection, Transaction);

            if (paramType == typeof(CashflowData))
                return (IRepository<T>)new LiabilityCashFlowRepository(connection, Transaction);

            if (paramType == typeof(FinancialAssumption))
                return (IRepository<T>)new FinancialAssumptionRepository(connection, Transaction);

            if (paramType == typeof(PensionIncreaseAssumption))
                return (IRepository<T>)new PensionIncreaseAssumptionRepository(connection, Transaction);

            if (paramType == typeof(Basis))
                return (IRepository<T>)new BasisRepository(connection, Transaction);

            if (paramType == typeof(SchemeAsset))
                return (IRepository<T>)new SchemeAssetRepository(connection, Transaction);

            if (paramType == typeof(Asset))
                return (IRepository<T>)new AssetRepository(connection, Transaction);

            if (paramType == typeof(AssetFund))
                return (IRepository<T>)new AssetFundRepository(connection, Transaction);

            if (paramType == typeof(SalaryProgression))
                return (IRepository<T>)new SalaryProgressionRepository(connection, Transaction);

            if (paramType == typeof(AssetClass))
                return (IRepository<T>)new CachedAssetClassRepository(connection, Transaction);

            if (paramType == typeof(UserProfile))
                return (IRepository<T>)new UserProfileRepository(connection, Transaction);

            if (paramType == typeof(SchemeDetail))
                return (IRepository<T>)new SchemeDetailRepository(connection, Transaction);

            if (paramType == typeof(HistoricSchemeDetail))
                return (IRepository<T>)new HistoricSchemeDetailRepository(connection, Transaction);

            if (paramType == typeof(IndexImportDetail))
                return (IRepository<T>)new IndexImportDetailRepository(connection, Transaction);

            if (paramType == typeof(CurveImportDetail))
                return (IRepository<T>)new CurveImportDetailRepository(connection, Transaction);

            if (paramType == typeof(IndexImportIndices))
                return (IRepository<T>)new IndexImportIndicesRepository(connection, Transaction);

            if (paramType == typeof(ApplicationValue))
                return (IRepository<T>)new ApplicationRepository(connection, Transaction);

            if (paramType == typeof(UserProfileMembership))
                return (IRepository<T>)new UserProfileMembershipRepository(connection, Transaction);
       
            if (paramType == typeof(ClientAssumption))
                return (IRepository<T>)new ClientAssumptionsRepository(connection, Transaction);

            if (paramType == typeof(ParametersImportHistory))
                return (IRepository<T>)new ParametersImportHistoryRepository(connection, Transaction);

            if (paramType == typeof(PasswordData))
                return (IRepository<T>)new UserProfilePasswordRepository(connection, Transaction);

            if (paramType == typeof(PensionSchemeCache))
                return (IRepository<T>)new PensionSchemeCacheRepository(connection, Transaction);

            if (paramType == typeof(BasisCache))
                return (IRepository<T>)new BasisCacheRepository(connection, Transaction);

            if (paramType == typeof(SchemeDetailSource))
                return (IRepository<T>)new SchemeDetailSourceRepository(connection, Transaction);

            throw new ArgumentException(string.Format("{0} has no repository.", paramType.Name));
        }

        public void Dispose()
        {
            if(Transaction != null)
                Transaction.Dispose();
            connection.Dispose();
        }
    }
}