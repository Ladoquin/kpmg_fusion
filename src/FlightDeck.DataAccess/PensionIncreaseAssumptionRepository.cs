﻿using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FlightDeck.DataAccess
{
    public class PensionIncreaseAssumptionRepository : Repository<PensionIncreaseAssumption>, IPensionIncreaseAssumptionRepository
    {
        public PensionIncreaseAssumptionRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "PensionIncreaseAssumptions")
        {
        }

        public IEnumerable<PensionIncreaseAssumption> GetByBasisId(int basisId, IFinancialIndexRepository indexRepository)
        {
            var assumptionData = connection.Query(GetSelectAll() + " where BasisId = @BasisId", new { BasisId = basisId }, transaction);
            return assumptionData.Select(assumption =>
                new PensionIncreaseAssumption(assumption.Id, assumption.BasisId, assumption.InitialValue,
                    assumption.FinancialIndexId == null ? null : indexRepository.GetById((int)assumption.FinancialIndexId),
                    assumption.ReferenceNumber, assumption.Label, assumption.Minimum, assumption.Maximum, assumption.FixedIncrease,
                    (AssumptionCategory)assumption.Category, assumption.InflationVolatility,
                    assumption.InflationType == null ? InflationType.None : (InflationType)assumption.InflationType,
                    assumption.IsFixed, assumption.IsVisible)).ToList();
        }

        public override int Insert(PensionIncreaseAssumption item)
        {
            return (int)connection.Insert(new
            {
                item.BasisId,
                item.InitialValue,
                item.ReferenceNumber,
                item.Label,
                FinancialIndexId = item.Index == null ? (int?)null : item.Index.Id,
                item.Minimum,
                item.Maximum,
                item.FixedIncrease,
                item.Category,
                item.InflationVolatility,
                InflationType = item.InflationType == InflationType.None ? (int?)null : (int)item.InflationType,
                item.IsFixed,
                item.IsVisible
            }, transaction, TableName);
        }
    }
}