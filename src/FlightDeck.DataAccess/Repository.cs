using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using System.Text;

namespace FlightDeck.DataAccess
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected readonly IDbConnection connection;
        protected readonly IDbTransaction transaction;
        protected readonly string TableName;

        protected Repository(IDbConnection connection, IDbTransaction transaction = null, string tableName = null)
        {
            this.transaction = transaction;
            this.connection = connection;
            TableName = tableName;
        }

        protected string GetInsert()
        {
            return GetInsert(TableName);
        }

        protected string GetInsert(string table)
        {
            return string.Format("insert into {0} ", table);
        }

        protected int GetLatestId()
        {
            return GetLatestId(TableName);
        }

        protected int GetLatestId(string table)
        {
            return (int)connection.Query(string.Format("select IDENT_CURRENT('{0}') Id", table),
                transaction: transaction).First().Id;
        }

        protected string GetSelectAll()
        {
            return GetSelectAll(TableName);
        }

        protected string GetSelectAll(string table)
        {
            return string.Format("select * from {0} ", table);
        }

        protected string GetSelectAllById()
        {
            return string.Format("select * from {0} where id = @Id", TableName);
        }

        public virtual T GetById(int id)
        {
            return connection.Get<T>(id, transaction, TableName);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return connection.Query<T>(GetSelectAll(), transaction: transaction);
        }

        public virtual int Insert(T item)
        {
            return (int)connection.Insert(item, transaction, TableName);
        }

        public virtual bool Update(T item)
        {
            return connection.Update(item, transaction, TableName);
        }

        public virtual void InsertOrUpdate(T item)
        {
            if (!Update(item))
                Insert(item);
        }

        public virtual void ClearCache()
        {
            SqlMapper.PurgeQueryCache();
        }

        public virtual string GetUpdateCols(IEnumerable<string> cols)
        {
            var s = new StringBuilder(300);
            foreach (var col in cols)
            {
                if (s.Length > 0) s.Append(", ");
                s.AppendFormat("{0} = @{0}", col);
            }
            return s.ToString();
        }
    }
}