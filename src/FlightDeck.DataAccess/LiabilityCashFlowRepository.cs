﻿using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FlightDeck.DataAccess
{
    public class LiabilityCashFlowRepository : Repository<CashflowData>, ILiabilityCashFlowRepository
    {
        public LiabilityCashFlowRepository(IDbConnection connection, IDbTransaction transaction = null) : 
            base(connection, transaction, "LiabilityCashFlows") {}

        public IEnumerable<CashflowData> GetByBasisId(int basisId)
        {
            var liabilities = connection.Query(GetSelectAll() + " where BasisId = @BasisId", new {BasisId = basisId}, transaction);
            var cashFlows = connection.Query("select c.* from LiabilityCashFlows l join CashFlows c on l.Id = c.LiabilityCashFlowId where l.BasisId = @BasisId", new { BasisId = basisId }, transaction).ToLookup(c => (int)c.LiabilityCashFlowId, c => new Tuple<int, double>(c.Year, c.Cash));

            var result = new List<CashflowData>();
            foreach (var liability in liabilities)
            {
                var cashFlowData = ((IEnumerable<Tuple<int, double>>)cashFlows[liability.Id]).ToDictionary(cashFlow => cashFlow.Item1, cashFlow => cashFlow.Item2);
                result.Add(new CashflowData(liability.Id, liability.BasisId, (MemberStatus)liability.MemberStatus, liability.Age, liability.MaxAge,
                    (RevaluationType)liability.RevaluationType,
                    liability.RetirementAge,
                    (BenefitType)liability.BenefitType,
                    cashFlowData.ToDictionary(x => x.Key, x => x.Value), 
                    cashFlowData.Keys.Min(), 
                    cashFlowData.Keys.Max(),
                    liability.IsBuyIn, liability.PensionIncreaseReference));
            }
            return result;
        }

        public override int Insert(CashflowData item)
        {
            var id = (int)connection.Insert(new {item.BasisId, MemberStatus = (int)item.MemberStatus, Age = item.MinAge, item.MaxAge, item.RetirementAge, item.PensionIncreaseReference, RevaluationType = (int)item.RevaluationType, item.IsBuyIn, BenefitType = (int)item.BenefitType }, transaction, TableName);
            var cashFlows = new List<dynamic>();

            foreach (var c in item.Cashflow)
                cashFlows.Add(new {LiabilityCashFlowId = id, Year = c.Key, Cash = c.Value});

            connection.Execute(string.Format("insert {0}({1}, {2}, {3}) values(@{1}, @{2}, @{3})", "CashFlows", "LiabilityCashFlowId", "Year", "Cash"), cashFlows, transaction);
            return id;
        }
    }
}