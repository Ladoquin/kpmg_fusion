﻿namespace FlightDeck.DataAccess
{
    using Dapper;
    using FlightDeck.Domain;
    using FlightDeck.Domain.Repositories;
    using FlightDeck.DomainShared;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    class ClientAssumptionsRepository : Repository<ClientAssumption>, IClientAssumptionsRepository
    {
        public ClientAssumptionsRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "ClientAssumptions") { }

        public IEnumerable<ClientAssumption> GetAll(int schemeDetailId)
        {
            var data = connection.Query("select * from " + TableName + " where SchemeDetailId = @schemeDetailId", new { schemeDetailId }, transaction);

            return data.Select(x => new ClientAssumption(x.Id, x.Name, x.SchemeDetailId, x.AnalysisDate, x.Data, x.CreatedByUserId, x.MasterBasisId));
        }

        public void Delete(int id)
        {
            connection.Execute("delete " + TableName + " where Id = @id", new { id }, transaction);
        }

        public void DeleteAll(int schemeDetailId)
        {
            connection.Execute("delete " + TableName + " where SchemeDetailId = @schemeDetailId", new { schemeDetailId }, transaction);
        }
    }
}
