﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Authentication;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.DomainShared;
using FlightDeck.Domain.Repositories;
using System.Threading;

namespace FlightDeck.DataAccess
{
    public class BasisRepository : Repository<Basis>
    {
        private string masterBasesTable = "MasterBases";
        private string costsTable = "TotalCosts";        
        private string simpleAssumptionsTable = "SimpleAssumptions";
        private string liabIncreasesTable = "LiabilityIncreases";
        private string bondYieldTable = "BasisBondYields";

        public BasisRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "Bases")
        {
        }

        public IEnumerable<Basis> GetBySchemeId(int schemeId, IFinancialIndexRepository indexRepository,
            ILiabilityCashFlowRepository cashFlowRepository, IFinancialAssumptionRepository assumptionRepository, 
            IPensionIncreaseAssumptionRepository increaseAssumptionRepository, ISalaryProgressionRepository salaryRepository)
        {
            var sql = string.Format("Select b.*, m.Name [MasterBasisName], c.*, bby.* " +
                "from {0} b " +
                "join {1} m on b.MasterBasisId = m.Id " +
                "join {2} c on b.Id = c.BasisId " +
                "left outer join {3} bby on b.Id = bby.BasisId " +
                "where b.PensionSchemeId = @SchemeId", 
                TableName,
                masterBasesTable,
                costsTable,
                bondYieldTable);
            var basisData = connection.Query(sql, new { SchemeId = schemeId }, transaction);

            return basisData.Select(basis =>
                new Basis(
                    basis.Id, basis.PensionSchemeId, basis.MasterBasisId, basis.DisplayName, (BasisType)basis.BasisType,
                    basis.AnchorDate, basis.LastPensionIncrease, basis.LinkedToCorpBonds,
                    salaryRepository.GetByBasisId((int)basis.Id).ToList(),
                    cashFlowRepository.GetByBasisId((int)basis.Id),
                    assumptionRepository.GetByBasisId((int)basis.Id, indexRepository).ToDictionary(x => x.Type, x => x.InitialValue),
                    increaseAssumptionRepository.GetByBasisId((int)basis.Id, indexRepository).ToDictionary(x => x.ReferenceNumber, x => x.InitialValue),
                    connection.Query(GetSelectAll(liabIncreasesTable) + "where BasisId = @BasisId",
                        new { BasisId = basis.Id }, transaction).ToDictionary(x => (int)x.Age, x => (double)x.Increase),
                    basis.Index == null ? null : new BondYield 
                    { 
                        Index = indexRepository.GetByName(basis.Index), 
                        Label = basis.Label 
                    },
                    basis.AOCI, basis.UnrecognisedPriorServiceCost, basis.AmortisationOfPriorServiceCost, ((int?)basis.AmortisationPeriodForActGain).GetValueOrDefault(),
                    connection.Query(GetSelectAll(simpleAssumptionsTable) + "where BasisId = @BasisId",
                        new { BasisId = basis.Id }, transaction).ToDictionary(x => (SimpleAssumptionType)x.Type, x => new SimpleAssumption((SimpleAssumptionType)x.Type, x.Value, x.Name, x.IsVisible)),
                    basis.RefGiltYieldId,
                    assumptionRepository.GetByBasisId((int)basis.Id, indexRepository).ToDictionary(x => x.Type),
                    increaseAssumptionRepository.GetByBasisId((int)basis.Id, indexRepository).ToDictionary(x => x.ReferenceNumber),
                    basis.RefGiltYieldId != null ? ((IRepository<FinancialIndex>)indexRepository).GetById(basis.RefGiltYieldId) : indexRepository.GiltsIndex
                    ) { TotalCosts = new TotalCosts(0, basis.Active, basis.Deferred, basis.Pension, basis.ServiceCost) }
                 ).ToList();
        }

        public override int Insert(Basis item)
        {
            var id = (int)connection.Insert(new { item.PensionSchemeId, BasisType = (int)item.Type, item.DisplayName, AnchorDate = item.EffectiveDate, LastPensionIncrease = item.LastPensionIncreaseDate, item.LinkedToCorpBonds, item.RefGiltYieldId, AOCI = item.ActuarialBalanceInAOCI, UnrecognisedPriorServiceCost = item.PriorServiceBalanceInAOCI, AmortisationOfPriorServiceCost = item.AmortisationOfPriorService, item.MasterBasisId, AmortisationPeriodForActGain = item.AmortisationPeriod }, transaction, TableName);
            var costs = item.TotalCosts;
            connection.Insert(new { BasisId = id, costs.Active, costs.Deferred, costs.Pension, costs.ServiceCost}, 
                transaction, costsTable, getId: false);
            
            var assumptionRepository = new FinancialAssumptionRepository(connection, transaction);
            var pensionIncreaseRepository = new PensionIncreaseAssumptionRepository(connection, transaction);
            var cashFlowRepository = new LiabilityCashFlowRepository(connection, transaction);
            var salaryRepository = new SalaryProgressionRepository(connection, transaction);

            foreach (var a in item.UnderlyingAssumptionIndices.Values)
            {
                assumptionRepository.Insert(new FinancialAssumption(0, id, a.InitialValue, a.Index, a.Type, a.Name, a.IsVisible));
            }

            foreach (var p in item.UnderlyingPensionIncreaseIndices.Values)
            {
                pensionIncreaseRepository.Insert(new PensionIncreaseAssumption(0, id, p.InitialValue, p.Index,
                    p.ReferenceNumber, p.Label, p.Minimum, p.Maximum, p.FixedIncrease, p.Category, p.InflationVolatility, p.InflationType, p.IsFixed, p.IsVisible));
            }

            int cfcount = 0;
            foreach (var c in item.Cashflows)
            {
                c.BasisId = id;
                cashFlowRepository.Insert(c);
                cfcount++;
            }

            foreach (var s in item.SalaryProgression)
            {
                salaryRepository.Insert(new SalaryProgression(0, id, s.MinAge, s.MaxAge, s.RetirementAge, s.SalaryRoll));
            }

            foreach (var simpleAssumption in item.SimpleAssumptions.Values)
            {
                connection.Insert(new { BasisId = id, Type = (int)simpleAssumption.Type, simpleAssumption.Value, simpleAssumption.Name, simpleAssumption.IsVisible },
                transaction, simpleAssumptionsTable, getId: false);
            }

            foreach (var increase in item.MortalityAdjustments)
            {
                connection.Insert(new { BasisId = id, Age = increase.Key, Increase = increase.Value },
                transaction, liabIncreasesTable, getId: false);
            }

            connection.Insert(new { BasisId = id, Index = item.BondYield.Index.Name, Label = item.BondYield.Label },
                transaction, bondYieldTable, getId: false);


            return id;
        }
    }
}