﻿namespace FlightDeck.DataAccess
{
    using Dapper;
    using FlightDeck.Domain;
    using FlightDeck.DomainShared;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class LogRepository : Repository<Log>, ILogRepository
    {
        public LogRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "Log")
        {
        }

        public IEnumerable<Log> GetAll(DateTime date, string level = null)
        {
            return connection.Query<Log>(GetSelectAll()
                + " where DATEADD(dd, 0, DATEDIFF(dd, 0, Date)) = @date"
                + " and ISNULL(@level, Level) = Level" , 
                new { date = date.Date, level }, transaction);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
