﻿using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;
using FlightDeck.DomainShared;
using System.Data;

namespace FlightDeck.DataAccess
{
    public class CurveImportDetailRepository : Repository<CurveImportDetail>, ICurveImportDetailRepository
    {
        public CurveImportDetailRepository(IDbConnection connection, IDbTransaction transaction = null) :
            base(connection, transaction, "CurveImportDetail") { }

        public CurveImportDetail GetLatest()
        {
            var id = GetLatestId();

            return id > 0 ? GetById(id) : null;
        }
    }
}
