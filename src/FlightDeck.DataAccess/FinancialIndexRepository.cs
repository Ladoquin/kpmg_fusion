﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using FlightDeck.Domain;
using FlightDeck.Domain.Repositories;

namespace FlightDeck.DataAccess
{
    public class FinancialIndexRepository: Repository<FinancialIndex>, IFinancialIndexRepository
    {
        private string indexValuesTable = "IndexValues";
        private const int cacheSize = 10;
        public const string GiltsIndexName = "FTSE BRIT GOVT FIXED OVER 15 YEARS - ANNUAL GRY";

        public FinancialIndexRepository(IDbConnection connection, IDbTransaction transaction) : 
            base(connection, transaction, "FinancialIndices"){}

        public override FinancialIndex GetById(int id)
        {
            var index = connection.Query(GetSelectAllById(), new {Id = id}, transaction).Single();
            var indexValues =
                new SortedList<DateTime, double>(
                    connection.Query(GetSelectAll(indexValuesTable) + " where FinancialIndexId = @Id and Date >= @date",
                        new {Id = id, Date = DateTime.UtcNow.Date.AddYears(-cacheSize)}, transaction).
                        ToDictionary(x=>(DateTime)x.Date, x=> (double)x.Value));
            return new FinancialIndex(index.Id, index.Name, indexValues);
        }

        public override IEnumerable<FinancialIndex> GetAll()
        {

            List<FinancialIndex> result = new List<FinancialIndex>();

            var allIndices = GetAllIndicesFromDB();

            int currentId = -1;
            bool customIndex = false;
            string indexName = string.Empty;
            FinancialIndex newIndex = null;
            Dictionary<DateTime, double> dict = null;
            bool pendingAdd = (allIndices != null && allIndices.Any());

            foreach (var index in allIndices)
            {
                if (index.Id != currentId)
                {
                    // create new Financial index, and add to the list
                    if (currentId != -1)
                    {
                        newIndex = new FinancialIndex(currentId, indexName, new SortedList<DateTime, double>(dict), index.Custom);
                        result.Add(newIndex);
                    }

                    // new index
                    currentId = index.Id;
                    indexName = index.Name;
                    customIndex = index.Custom;
                    dict = new Dictionary<DateTime, double>();
                }
                dict.Add((DateTime)index.Date, (double)index.Value);
            }

            // add last index  OR  in case there was only 1 index type
            if (pendingAdd && dict != null)
            {
                newIndex = new FinancialIndex(currentId, indexName, new SortedList<DateTime, double>(dict), customIndex);
                result.Add(newIndex);
            }

            // clear
            allIndices = null;

            return result;
        }

        private IEnumerable<dynamic> GetAllIndicesFromDB()
        {
            var sqlQuery = "select F.Id, F.Name, IV.Value, IV.Date, F.Custom FROM FinancialIndices F " +
                           "LEFT OUTER JOIN IndexValues IV ON F.Id = IV.FinancialIndexId " +
                           "WHERE IV.Date >= @date ORDER BY F.Id, IV.Date";
            var allIndices = connection.Query(sqlQuery,
                                            new { Date = DateTime.UtcNow.Date.AddYears(-cacheSize) }, transaction);

            return allIndices;
        }

        public IEnumerable<dynamic> GetIndicesByImportId(int importDetailId)
        {
            if (importDetailId < 0)
                return null;

            List<int> importedIndices = new List<int>();
            var sqlQuery = "SELECT IndexId FROM IndexImportIndices WHERE IndexImportDetailId = @importDetailId";
            var allIndices = connection.Query(sqlQuery, importDetailId, transaction);

            return allIndices;
        }

        public override int Insert(FinancialIndex item)
        {
            var result = (int)connection.Insert(new {item.Name, item.Custom}, transaction, TableName);
            InsertIndexValues(item, result);
            return result;
        }

        public int FillEndGap(FinancialIndex item, DateTime EndDate)
        {
            if (item.EndDate < EndDate && !item.ContainsValue(EndDate))
            {
                var startingFrom = item.EndDate.AddDays(1);
                var dict = new Dictionary<DateTime, double>();
                var indexValue = item.GetValue(item.EndDate);

                for (var day = startingFrom.Date; day.Date <= EndDate.Date; day = day.AddDays(1))
                    dict.Add(day, indexValue);

                SortedList<DateTime, double> newValues = new SortedList<DateTime,double>(dict);

                if(newValues.Any())
                {
                    FinancialIndex newIndex = new FinancialIndex(item.Id, item.Name, newValues);
                    InsertIndexValues(newIndex, newIndex.Id);
                }
                return newValues.Count();
            }
            return 0;
        }

        private void InsertIndexValues(FinancialIndex item, int indexId)
        {
            var indexValues = new List<dynamic>();
            foreach (var value in item.RawValues)
            {
                indexValues.Add(new {FinancialIndexId = indexId, Date = value.Key, value.Value});
            }
            connection.Execute(
                string.Format(@"insert {0}({1}, {2}, {3}) values (@{1}, @{2}, @{3})", indexValuesTable, "FinancialIndexId",
                    "Date", "Value"), indexValues, transaction);
        }

        private void UpdateOrInsertIndexValues(FinancialIndex item)
        {
            foreach (var value in item.RawValues)
            {
                var indexValue = new { FinancialIndexId = item.Id, Date = value.Key, value.Value };
                if (connection.Execute(
                string.Format(@"update {0} set {3} = @{3} where {1} = @{1} and {2} = @{2}", indexValuesTable, "FinancialIndexId",
                    "Date", "Value"), indexValue, transaction) == 0)
                connection.Execute(
                string.Format(@"insert {0}({1}, {2}, {3}) values (@{1}, @{2}, @{3})", indexValuesTable, "FinancialIndexId",
                    "Date", "Value"), indexValue, transaction);
            }
            
        }

        public override bool Update(FinancialIndex item)
        {
            UpdateOrInsertIndexValues(item);
            return item.RawValues.Count > 0;
        }

        public FinancialIndex GetByName(string name)
        {
            var index = connection.Query(GetSelectAll() + "where Name = @Name", new { Name = name }, transaction).FirstOrDefault();
            if (index == null)
                return null;
            var indexValues =
                new SortedList<DateTime, double>(
                    connection.Query(GetSelectAll(indexValuesTable) + " where FinancialIndexId = @Id and Date >= @date",
                        new { Id = (int)index.Id, Date = DateTime.UtcNow.Date.AddYears(-cacheSize) }, transaction).
                        ToDictionary(x => (DateTime)x.Date, x => (double)x.Value));
            return new FinancialIndex(index.Id, index.Name, indexValues, index.Custom);
        }

        /// <summary>
        /// The same as GetByName but gets ALL Financial indices for the requested index.
        /// i.e. result set is not constrained by rolling 5 year period as GetByName is.
        /// </summary>
        public FinancialIndex GetByNameComplete(string name)
        {
            var index = connection.Query(GetSelectAll() + "where Name = @Name", new { Name = name }, transaction).FirstOrDefault();
            if (index == null)
                return null;
            var indexValues =
                new SortedList<DateTime, double>(
                    connection.Query(GetSelectAll(indexValuesTable) + " where FinancialIndexId = @Id",
                        new { Id = (int)index.Id }, transaction).
                        ToDictionary(x => (DateTime)x.Date, x => (double)x.Value));
            return new FinancialIndex(index.Id, index.Name, indexValues, index.Custom);
        }

        public FinancialIndex GiltsIndex { get { return GetByName(GiltsIndexName); } }
    }
}
