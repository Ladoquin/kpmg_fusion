﻿namespace Fusion.WebApp.Tests
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.ServiceInterfaces.Reports;
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Services;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.IO;
    using System.Web.Mvc;

    [TestFixture]
    public class AnalysisControllerTests
    {
        [TestCase(AccountingStandardType.IAS19, "1364688000000", "1412121600000")] // 31/3/2013 - 01/10/2014
        [TestCase(AccountingStandardType.IAS19, "1365897600000", "1412812800000")] // 14/04/2013 - 09/10/2014
        public void TestDisclosureConvertsDateArgsCorrectly(AccountingStandardType type, string start, string end)
        {
            var scheme = new MockSchemeService().GetEmptyScheme(new DateTime(2013, 3, 31), new DateTime(2013, 3, 31), new DateTime(2014, 11, 1), new DateTime(2014, 11, 1));
            var moqSessionManager = new Mock<ISessionManager>();
            moqSessionManager.Setup(x => x.GetScheme()).Returns(scheme);
            var moqAccountingReporter = new Mock<IAccountingReporter>();
            DateTime actualStartDate = DateTime.MinValue;
            DateTime actualEndDate = DateTime.MinValue;
            moqAccountingReporter.Setup(x => x.Create(It.IsAny<IScheme>(), It.IsAny<AccountingStandardType>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), AccountingReportOutputType.Default))
                .Callback((IScheme ps, AccountingStandardType ast, DateTime s, DateTime e, AccountingReportOutputType ot) =>
                    {
                        actualStartDate = s;
                        actualEndDate = e;
                    })
                .Returns(new MemoryStream());
            var controller = ControllerFactory.Get<AnalysisController>(new MockHttpContext());
            controller.SessionManager = moqSessionManager.Object;
            controller.AccountingReporter = moqAccountingReporter.Object;

            var result = controller.Disclosure(type, start, end) as FileStreamResult;

            Assert.NotNull(result);
            Assert.AreEqual(JsonHelper.UnixEpoch.AddMilliseconds(double.Parse(start)), actualStartDate);
            Assert.AreEqual(JsonHelper.UnixEpoch.AddMilliseconds(double.Parse(end)), actualEndDate);
        }
    }
}
