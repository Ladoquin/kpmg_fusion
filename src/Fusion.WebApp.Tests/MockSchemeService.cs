﻿namespace Fusion.WebApp.Tests
{
    using FlightDeck.ServiceInterfaces;
    using Moq;
    using System;

    class MockSchemeService
    {
        public IScheme GetEmptyScheme()
        {
            return GetEmptyScheme(null, null, null, null);
        }
        
        public IScheme GetEmptyScheme(
            DateTime? anchorDate,
            DateTime? attributionStartDate,
            DateTime? attributionEndDate,
            DateTime? refreshDate)
        {
            var currentBasis = new Mock<IBasis>();
            var pensionScheme = new Mock<IPensionScheme>();
            var schemeDetail = new Mock<IScheme>();

            if (anchorDate.HasValue)
                currentBasis.SetupGet(x => x.AnchorDate).Returns(anchorDate.Value);
            if (attributionStartDate.HasValue)
                currentBasis.SetupGet(x => x.AttributionStart).Returns(attributionStartDate.Value);
            if (attributionEndDate.HasValue)
                currentBasis.SetupGet(x => x.AttributionEnd).Returns(attributionEndDate.Value);
            if (refreshDate.HasValue)
                pensionScheme.SetupGet(x => x.RefreshDate).Returns(refreshDate.Value);

            pensionScheme.SetupGet(x => x.CurrentBasis).Returns(currentBasis.Object);
            schemeDetail.SetupGet(x => x.PensionScheme).Returns(pensionScheme.Object);

            return schemeDetail.Object;
        }
    }
}
