﻿namespace Fusion.WebApp.Tests
{
    using FlightDeck.DomainShared;
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Services;
    using Moq;
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    
    class ControllerFactory
    {
        public static T Get<T>(HttpContextBase httpContext) where T : FusionBaseController, new()
        {
            var controller = new T();

            var requestContext = new RequestContext(httpContext, new RouteData());

            controller.ControllerContext = new ControllerContext()
            {
                Controller = controller,
                RequestContext = requestContext
            };

            controller.Url = new UrlHelper(requestContext);

            return controller;
        }

        public static T GetWithScheme<T>(DateTime anchorDate, DateTime refreshDate, DateTime? attributionStart = null, DateTime? attributionEnd = null) where T : FusionBaseController, new()
        {
            var scheme = new MockSchemeService().GetEmptyScheme(anchorDate, attributionStart.GetValueOrDefault(anchorDate), attributionEnd.GetValueOrDefault(refreshDate), refreshDate);
            var moqSessionManager = new Mock<ISessionManager>();
            moqSessionManager.Setup(x => x.GetScheme()).Returns(scheme);
            var controller = ControllerFactory.Get<T>(new MockHttpContext());
            controller.SessionManager = moqSessionManager.Object;
            return controller;
        }
    }
}
