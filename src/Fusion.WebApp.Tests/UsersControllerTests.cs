﻿namespace Fusion.WebApp.Tests
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using Fusion.WebApp.Services;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Web.Mvc;

    [TestFixture]
    public class UsersControllerTests
    {
        [Test]
        public void TestAddReturnsRedirectWhenSavingNewClient()
        {
            var controller = SetupControllerForUserPost(true);
            var model = GetUserViewModel();
            model.Schemes = new string[] { "The Space01 Test Scheme" };
            
            var result = controller.Add(model);

            Assert.NotNull(result);
            Assert.NotNull(result as RedirectToRouteResult);
            Assert.AreEqual("Manage", (result as RedirectToRouteResult).RouteValues["action"]);
        }

        [Test]
        public void TestAddReturnsErrorsWhenSavingNewClientWithNoSchemes()
        {
            var controller = SetupControllerForUserPost(true);
            var model = GetUserViewModel();

            var result = controller.Add(model) as ViewResult;

            AssertFailedViewData(result);
        }

        [Test]
        public void TestEditReturnsRedirectWhenSavingExistingClient()
        {
            var controller = SetupControllerForUserPost(true);
            var model = GetUserViewModel();
            model.Schemes = new string[] { "The Space01 Test Scheme" };

            var result = controller.Edit(model);

            Assert.NotNull(result);
            Assert.NotNull(result as RedirectToRouteResult);
            Assert.AreEqual("Manage", (result as RedirectToRouteResult).RouteValues["action"]);
        }

        [Test]
        public void TestEditReturnsErrorsWhenSavingClientWithNoSchemes()
        {
            var controller = SetupControllerForUserPost(true);
            var model = GetUserViewModel();

            var result = controller.Edit(model) as ViewResult;

            AssertFailedViewData(result);
        }

        #region privates

        private UsersController SetupController()
        {
            var moqSessionManager = new Mock<ISessionManager>();
            moqSessionManager.Setup(x => x.GetUserProfile()).Returns(new UserProfile(1, "admin", "mr", "administrator", "admin@space01.co.uk", true, false, null, null));

            var moqRoleManager = new Mock<IRoleManager>();
            var moqUserProfileService = new Mock<IUserProfileService>();

            var controller = ControllerFactory.Get<UsersController>(new MockHttpContext());
            controller.SessionManager = moqSessionManager.Object;
            controller.RoleManager = moqRoleManager.Object;
            controller.UserProfileService = moqUserProfileService.Object;

            return controller;
        }

        private UsersController SetupControllerForUserPost(bool updateIsSuccessful)
        {
            var controller = SetupController();

            var moqMembershipProvider = new Mock<IMembershipProvider>();
            var moqUserProfileService = new Mock<IUserProfileService>();
            moqUserProfileService
                .Setup(x => x.Update(It.IsAny<string>(), It.IsAny<RoleType?>(), It.IsAny<IEnumerable<string>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new ActionResult<IUserProfileServiceResult>(updateIsSuccessful));

            controller.MembershipProvider = moqMembershipProvider.Object;
            controller.UserProfileService = moqUserProfileService.Object;

            return controller;
        }

        private AdditUserViewModel GetUserViewModel()
        {
            return new AdditUserViewModel
            {
                UserName = "foo",
                FirstName = "Kylie",
                LastName = "Minogue",
                EmailAddress = "kylieminogue@space01.co.uk",
                Role = RoleType.Client,
                Schemes = new string[0]
            };
        }

        private void AssertFailedViewData(ViewResult result)
        {
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ModelState.Keys.Contains("Role"));
            Assert.AreEqual(1, result.ViewData.ModelState["Role"].Errors.Count);
            Assert.AreEqual("Must be assigned to a scheme", result.ViewData.ModelState["Role"].Errors[0].ErrorMessage);

            var viewmodel = result.Model as ManageUserViewModel;

            Assert.IsNotNull(viewmodel);
            Assert.IsTrue(viewmodel.AdditUserViewModel.Result == AdditUserResult.AddUserFailure || viewmodel.AdditUserViewModel.Result == AdditUserResult.EditUserFailure);
            Assert.IsFalse(viewmodel.AdditUserViewModel.RolesAvailable.IsNullOrEmpty());
            Assert.AreEqual("foo", viewmodel.AdditUserViewModel.UserName);
            Assert.AreEqual("Kylie", viewmodel.AdditUserViewModel.FirstName);
            Assert.AreEqual("Minogue", viewmodel.AdditUserViewModel.LastName);
            Assert.AreEqual("kylieminogue@space01.co.uk", viewmodel.AdditUserViewModel.EmailAddress);
            Assert.AreEqual(RoleType.Client, viewmodel.AdditUserViewModel.Role.GetValueOrDefault(RoleType.Admin));
        }

        #endregion
    }
}
