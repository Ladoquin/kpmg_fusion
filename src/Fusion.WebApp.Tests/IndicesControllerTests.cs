﻿namespace Fusion.WebApp.Tests
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using Fusion.WebApp.Services;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Security.Principal;
    using System.Text;
    using System.Web.Mvc;

    [TestFixture]
    public class IndicesControllerTests
    {
        [Test]
        public void TestSaveReturnsSuccessfulViewResultWithAValidXmlFile()
        {
            var controller = GetMockController(RoleType.Admin, true);
            var input = GetMockInput();

            var result = controller.Save(input) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.IsTrue(model.Submitted);
            Assert.AreEqual(ResultEnum.Success, model.Result);
            Assert.AreEqual(0, model.ValidationMessages.Count());
        }

        [Test]
        public void TestSaveReturnsFailedViewResultWithAnInvalidXmlFile()
        {
            var controller = GetMockController(RoleType.Admin, false);
            var input = GetMockInput();

            var result = controller.Save(input) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.IsTrue(model.Submitted);
            Assert.AreEqual(ResultEnum.Failure, model.Result);
            Assert.AreNotEqual(0, model.ValidationMessages.Count());
        }

        [Test]
        public void TestSaveReturnsFailureWithAnInvalidXmlFileAndAllowsOverride()
        {
            var controller = GetMockController(RoleType.Admin, false);
            var input = GetMockInput();

            var result = controller.Save(input) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Failure, model.Result);
            Assert.IsTrue(model.AllowInvalidSubmission);
        }

        [Test]
        public void TestSaveReturnsFailureWithAnInvalidXmlFileAndDoesNotAllowOverride()
        {
            var controller = GetMockController(RoleType.Installer, false);
            var input = GetMockInput();

            var result = controller.Save(input) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Failure, model.Result);
            Assert.IsFalse(model.AllowInvalidSubmission);
        }

        [Test]
        public void TestForceSaveReturnsSuccessWithValidXmlFile()
        {
            var controller = GetMockController(RoleType.Admin, true);
            var input = GetMockInput();
            var tempData = new TempDataDictionary();
            tempData.Add("stats", new BinaryReader(input.ImportFile.InputStream).ReadBytes((int)input.ImportFile.InputStream.Length));
            tempData.Add("model", input);
            controller.TempData = tempData;

            var result = controller.ForceSave(null) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Success, model.Result);
        }

        [Test]
        public void TestForceSaveReturnsFailureWithInvalidXmlFile()
        {
            var controller = GetMockController(RoleType.Admin, false);
            var input = GetMockInput();
            var tempData = new TempDataDictionary();
            tempData.Add("stats", new BinaryReader(input.ImportFile.InputStream).ReadBytes((int)input.ImportFile.InputStream.Length));
            tempData.Add("model", input);
            controller.TempData = tempData;

            var result = controller.ForceSave(null) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as IndexImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Success, model.Result);
        }

        [Test]
        public void TestViewAllReturnsSummaries()
        {
            var moqIndicesService = new Mock<IIndicesService>();
            moqIndicesService.Setup(x => x.GetAllSummaries()).Returns(new List<IndexDataSummary> { new IndexDataSummary(1, "foo", DateTime.MinValue, DateTime.MaxValue) });
            var controller = GetMockController(RoleType.Admin, indicesService: moqIndicesService);

            var result = controller.ViewAll() as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as IEnumerable<IndexDataSummary>;
            Assert.IsNotNull(model);
            Assert.True(model.Any());
            Assert.AreEqual("foo", model.First().Name);
        }

        [Test]
        public void TestViewAllIndexValuesReturnsPartialView()
        {
            var moqIndicesService = new Mock<IIndicesService>();
            var dic = new OrderedDictionary();
            dic.Add(DateTime.Now.Date, 999);
            moqIndicesService.Setup(x => x.GetIndexValues(1)).Returns(dic);
            var controller = GetMockController(RoleType.Admin, indicesService: moqIndicesService);

            var result = controller.ViewAllIndexValues(1) as ContentResult;

            Assert.IsNotNull(result);
            var cnt = result.Content.ToLower();
            Assert.AreEqual("<table>", cnt.Substring(0, 7));
            Assert.AreEqual("</table>", cnt.Substring(cnt.Length - 8));
            Assert.AreEqual(cnt.Select((c, i) => cnt.Substring(i)).Count(sub => sub.StartsWith("<tr>")), cnt.Select((c, i) => cnt.Substring(i)).Count(sub => sub.StartsWith("</tr>")));
            Assert.AreEqual(cnt.Select((c, i) => cnt.Substring(i)).Count(sub => sub.StartsWith("<td>")), cnt.Select((c, i) => cnt.Substring(i)).Count(sub => sub.StartsWith("</td>")));
        }

        private IndicesController GetMockController(RoleType role, bool xmlValidates = true, Mock<IIndicesService> indicesService = null)
        {
            var mockHttpContext = new MockHttpContext { User = new GenericPrincipal(new GenericIdentity(role.ToString().ToLower()), new[] { role.ToString() }) };
            var controller = ControllerFactory.Get<IndicesController>(mockHttpContext);
            var moqSessionManager = new Mock<ISessionManager>();
            var moqIndexImportDetailService = new Mock<IIndexImportDetailService>();
            var moqXmlSchemaValidator = new Mock<IXmlSchemaValidator>();
            var moqIndicesService = indicesService ?? new Mock<IIndicesService>();
            var moqPreCalculationService = new Mock<IPreCalculationService>();
            moqXmlSchemaValidator.Setup(x => x.Validate(It.IsAny<Stream>(), It.IsAny<Stream>())).Returns(xmlValidates);
            moqXmlSchemaValidator.SetupGet(x => x.Messages).Returns(xmlValidates ? new List<string>() : new List<string> { "An xml validation error" });
            controller.SessionManager = moqSessionManager.Object;
            controller.IndexService = moqIndexImportDetailService.Object;
            controller.XmlSchemaValidator = moqXmlSchemaValidator.Object;
            controller.IndicesService = moqIndicesService.Object;
            controller.PreCalculationService = moqPreCalculationService.Object;
            return controller;            
        }

        private IndexImportViewModel GetMockInput()
        {
            var moqHttpPostedFile = new MockHttpPostedFile(Encoding.ASCII.GetBytes("<root/>"), "text/xml", "");
            var input = new IndexImportViewModel { ImportFile = moqHttpPostedFile };
            return input;
        }
    }
}
