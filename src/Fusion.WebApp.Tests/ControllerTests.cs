﻿namespace Fusion.WebApp.Tests
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web.Mvc;
    
    [TestFixture]
    public class ControllerTests
    {
        [Test]
        public void TestGetRequestToPostActionReturnsView()
        {
            var ignoreControllers = new List<string> { "AccountController", "JsonController" };

            foreach (var type in Assembly.LoadFile(System.IO.Path.Combine(Environment.CurrentDirectory, "Fusion.WebApp.dll")).GetTypes())
            {
                if (type.IsPublic &&
                    type.Name.EndsWith("Controller", System.StringComparison.OrdinalIgnoreCase) &&
                    type.IsAbstract == false &&
                    typeof(IController).IsAssignableFrom(type) &&
                    !ignoreControllers.Contains(type.Name))
                {
                    var controllerName = type.Name;

                    var actions = new Dictionary<string, List<string>>(10);

                    foreach (var methodInfo in type.GetMethods().Where(a => a.IsPublic))
                    {
                        if (!actions.ContainsKey(methodInfo.Name))
                            actions.Add(methodInfo.Name, new List<string>(2));

                        var attrs = (ActionMethodSelectorAttribute[])methodInfo.GetCustomAttributes(typeof(ActionMethodSelectorAttribute), true);
                        foreach (var attr in attrs)
                        {
                            if (attr.GetType().Equals(typeof(HttpGetAttribute)))
                            {
                                actions[methodInfo.Name].Add("GET");
                            }
                            else if (attr.GetType().Equals(typeof(HttpPostAttribute)))
                            {
                                actions[methodInfo.Name].Add("POST");
                            }
                        }
                    }

                    var invalidActions = actions.Where(x => x.Value.Contains("POST") && !x.Value.Contains("GET"));

                    if (invalidActions.Any())
                        Assert.Fail(string.Format("POST actions found with no corresponding GET on controller '{0}', actions: {1}", controllerName, invalidActions.Select(x => x.Key).Aggregate((x, y) => x + ", " + y)));
                }
            }
        }
    }
}
