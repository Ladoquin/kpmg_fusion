﻿namespace Fusion.WebApp.Tests
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using Fusion.Shared.Mocks;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Services;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Web.Mvc;

    [TestFixture]
    public class JsonControllerTests
    {
        [Test]
        public void TestEvolutionPageReturnsComparisonData()
        {
            var moqScheme = new Mock<IScheme>();
            var moqPensionScheme = new Mock<IPensionScheme>();
            var moqBasis = new Mock<IBasis>();
            var moqSessionManager = new Mock<ISessionManager>();

            moqBasis.SetupGet(x => x.Evolution).Returns(new Dictionary<DateTime, EvolutionData>());
            moqBasis.SetupGet(x => x.ExpectedDeficit).Returns(new Dictionary<DateTime, ExpectedDeficitData>());
            moqScheme.SetupGet(x => x.PensionScheme).Returns(moqPensionScheme.Object);
            moqSessionManager.Setup(x => x.GetScheme()).Returns(moqScheme.Object);
            moqPensionScheme.SetupGet(x => x.CurrentBasis).Returns(moqBasis.Object);

            var moqAccBasis = new Mock<IBasis>();
            moqAccBasis.SetupGet(x => x.Name).Returns("Acc");
            moqAccBasis.SetupGet(x => x.Evolution)
                .Returns(new Dictionary<DateTime, EvolutionData> 
                { 
                    { new DateTime(2013, 3, 31), new EvolutionData(new DateTime(2013, 3, 31), 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) },
                    { new DateTime(2014, 3, 31), new EvolutionData(new DateTime(2014, 3, 31), 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) }
                });
            var moqBuyBasis = new Mock<IBasis>();
            moqBuyBasis.SetupGet(x => x.Name).Returns("Buy");
            moqBuyBasis.SetupGet(x => x.Evolution)
                .Returns(new Dictionary<DateTime, EvolutionData> 
                { 
                    { new DateTime(2013, 3, 31), new EvolutionData(new DateTime(2013, 3, 31), 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) },
                    { new DateTime(2014, 3, 31), new EvolutionData(new DateTime(2014, 3, 31), 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) }
                });

            moqPensionScheme.Setup(x => x.GetAllBases()).Returns(new List<IBasis> { moqAccBasis.Object, moqBuyBasis.Object });

            var controller = ControllerFactory.Get<EvolutionController>(new MockHttpContext());

            controller.SessionManager = moqSessionManager.Object;

            var result = controller.Tracker() as JsonResult;

            Assert.NotNull(result);
            Assert.NotNull(result.Data);

            var comparisonData = JsonReader.Get<IEnumerable>(result.Data, "comparisonData");

            Assert.NotNull(comparisonData);

            var actual = new Dictionary<string, Dictionary<DateTime, double>>();

            foreach (var item in comparisonData)
            {
                var basisName = JsonReader.Get<string>(item, "basis");
                var points = new Dictionary<DateTime, double>();
                foreach (var point in JsonReader.Get<IEnumerable>(item, "points"))
                {
                    points.Add(JsonHelper.UnixEpoch.AddMilliseconds(JsonReader.Get<double>(point, "date")), JsonReader.Get<double>(point, "value"));
                }
                actual.Add(basisName, points);
            }

            Assert.AreEqual(2, actual.Keys.Count);
            Assert.Contains("Acc", actual.Keys);
            Assert.Contains("Buy", actual.Keys);
            Assert.AreEqual(1, actual["Acc"][new DateTime(2013, 3, 31)]);
            Assert.AreEqual(2, actual["Acc"][new DateTime(2014, 3, 31)]);
            Assert.AreEqual(3, actual["Buy"][new DateTime(2013, 3, 31)]);
            Assert.AreEqual(4, actual["Buy"][new DateTime(2014, 3, 31)]);
            
        }
    }
}
