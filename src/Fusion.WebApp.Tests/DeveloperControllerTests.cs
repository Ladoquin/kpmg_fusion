﻿namespace Fusion.WebApp.Tests
{
    using FlightDeck.ServiceInterfaces;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using Fusion.Shared.Mocks;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Web.Mvc;

    [TestFixture]
    public class DeveloperControllerTests
    {
        [Test]
        public void LogActionReturnsLogViewModel()
        {
            var controller = ControllerFactory.Get<DeveloperController>(new MockHttpContext());
            var moqLogReader = new Mock<ILogReader>();
            controller.LogReader = moqLogReader.Object;

            var result = controller.Log(DateTime.Now.ToString("dd/MM/yyyy"), string.Empty) as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model as LogViewModel);
        }

        [Test]
        public void LogActionCallsLogReaderGetAll()
        {
            var controller = ControllerFactory.Get<DeveloperController>(new MockHttpContext());
            var moqLogReader = new Mock<ILogReader>();
            controller.LogReader = moqLogReader.Object;

            var result = controller.Log(DateTime.Now.ToString("dd/MM/yyyy"), string.Empty) as ViewResult;

            moqLogReader.Verify(x => x.GetAll(It.IsAny<DateTime>(), It.IsAny<string>()));
        }

        [Test]
        public void LogActionUsesTodayDateForInvalidDate()
        {
            var controller = ControllerFactory.Get<DeveloperController>(new MockHttpContext());
            var moqLogReader = new Mock<ILogReader>();
            controller.LogReader = moqLogReader.Object;

            var result = controller.Log(string.Empty, string.Empty) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as LogViewModel;
            Assert.IsNotNull(model);
            Assert.IsTrue(model.Date.Date == DateTime.Now.Date);
        }
    }
}
