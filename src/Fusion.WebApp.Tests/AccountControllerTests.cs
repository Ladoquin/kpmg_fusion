﻿namespace Fusion.WebApp.Tests
{
    using FlightDeck.DomainShared;
    using FlightDeck.ServiceInterfaces;
    using FlightDeck.Services;
    using Fusion.Shared.Mocks;
    using Fusion.Shared.Web;
    using Fusion.WebApp.Controllers;
    using Fusion.WebApp.Models;
    using Fusion.WebApp.Services;
    using Moq;
    using NUnit.Framework;
    using System.Web.Mvc;

    [TestFixture]
    public class AccountControllerTests
    {

        AccountController controller;
        Mock<IMembershipProvider> mockMembershipProvider;
        Mock<ISessionManager> mockSessionManager;
        UserProfile userProfile;
        Mock<IAccountSecurityService> mockAccountSecurityService;

        [SetUp]
        public void SetUp()
        {

            controller = ControllerFactory.Get<AccountController>(new MockHttpContext());

            mockMembershipProvider = new Mock<IMembershipProvider>();
            mockMembershipProvider.Setup(m => m.ValidateUser(It.IsAny<string>(), It.IsAny<string>())).Returns(false);

            mockSessionManager = new Mock<ISessionManager>();
            userProfile = new UserProfile
            {
                PasswordFailuresSinceLastSuccess = AppSettings.AllowedFailedLoginAttempts //Set to max for test. Next attempt would have to be correct to succeed.
            };
            mockSessionManager.Setup(s => s.GetUserProfile()).Returns(userProfile);

            mockAccountSecurityService = new Mock<IAccountSecurityService>();
            mockAccountSecurityService.Setup(a => a.IsAuthenticated).Returns(true);

            controller.MembershipProvider = mockMembershipProvider.Object;
            controller.SessionManager = mockSessionManager.Object;
            controller.WebSecurity = mockAccountSecurityService.Object;

        }

        [TestCase(true)]
        [TestCase(false)]
        public void TestLoginReturnsAppropriateResponse(bool credentialsValid)
        {
            var moqIAccountSecurityService = new Mock<IAccountSecurityService>();
            moqIAccountSecurityService.Setup(x => x.UserExists(It.IsAny<string>())).Returns(true);
            moqIAccountSecurityService.Setup(x => x.Login(It.IsAny<string>(), It.IsAny<string>())).Returns(credentialsValid);
            var moqIRazorViewRenderer = new Mock<IRazorViewRenderer>();
            moqIRazorViewRenderer.Setup(x => x.RenderRazorViewToString(It.IsAny<string>(), It.IsAny<object>())).Returns("<div id='login-panel'>please login again</div>");
            var controller = ControllerFactory.Get<AccountController>(new MockHttpContext());
            controller.WebSecurity = moqIAccountSecurityService.Object;
            controller.RazorViewRenderer = moqIRazorViewRenderer.Object;
            var model = new LoginViewModel { UserName = "foo", Password = "bar" };

            var result = (controller.Login(model) as JsonResult).Data;

            Assert.NotNull(result);
            Assert.AreEqual(credentialsValid, JsonReader.Get<bool>(result, "response"));
            if (!credentialsValid)
            {
                Assert.NotNull(JsonReader.Get<string>(result, "partialView"));
                Assert.Greater(JsonReader.Get<string>(result, "partialView").IndexOf("login-panel"), 0);
            }
        }

        [Test]
        public void PasswordFailuresOnChangePasswordCanLockAccount()
        {
            var passwordUpdateModel = new PasswordUpdateModel
            {
                OldPassword = "Db9Fassword_1",
                NewPassword = "Db9Fassword_2",
                ConfirmPassword = "Db9Fassword_2"
            };

            var result = controller.ChangePassword(passwordUpdateModel);

            assertPasswordChangeFailure(result);
        }

        [Test]
        public void PasswordFailuresOnResetPasswordCanLockAccount()
        {
            var passwordUpdateModel = new PasswordResetViewModel
            {
                OldPassword = "Db9Fassword_1",
                NewPassword = "Db9Fassword_2",
                ConfirmPassword = "Db9Fassword_2"
            };

            var result = controller.ResetPassword(passwordUpdateModel);

            assertPasswordChangeFailure(result);
        }

        private void assertPasswordChangeFailure(ActionResult result)
        {
            Assert.AreEqual(AppSettings.AllowedFailedLoginAttempts + 1, userProfile.PasswordFailuresSinceLastSuccess, "Password failures need to be incremented past the AppSettings limit to lock the account");
            Assert.IsTrue(result.GetType() == typeof(RedirectToRouteResult), "Redirect expected on password failures exceeding limit");
            Assert.IsTrue((string)((RedirectToRouteResult)result).RouteValues["action"] == "Login", "Redirect to login page expected after password failures exceed limit");
        }
    }
}