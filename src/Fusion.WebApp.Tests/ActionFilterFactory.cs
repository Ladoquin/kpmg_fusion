﻿namespace Fusion.WebApp.Tests
{
    using Fusion.Shared.Mocks;
    using Fusion.WebApp.Controllers;
    using System.Collections.Generic;
    using System.Web.Mvc;

    class ActionFilterFactory
    {
        public static ActionExecutingContext GetActionExecutingContext(IDictionary<string, object> actionParameters = null)
        {
            var context = new ActionExecutingContext();
            context.HttpContext = new MockHttpContext();
            context.Result = new ViewResult();
            context.Controller = new JsonController(); // just any for now
            context.ActionParameters = actionParameters;

            return context;
        }
    }
}
