﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using FlightDeck.Services;
using Fusion.Shared.Mocks;
using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using Fusion.WebApp.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Fusion.WebApp.Tests
{
    [TestFixture]
    public class SchemeControllerTests
    {
        Mock<ISessionManager> mockSessionManager;
        Mock<IPensionScheme> pensionSchemeMock;
        Mock<IPensionSchemeService> pensionSchemeServicMock;
        UserProfile userProfile;
        MockHttpPostedFile mockHttpPostedFile;
        MockHttpContext context;
        SchemeController controller;
        Mock<IScheme> mockScheme;

        [SetUp]
        public void Setup()
        {
            mockSessionManager = new Mock<ISessionManager>();
            pensionSchemeMock = new Mock<IPensionScheme>();
            pensionSchemeServicMock = new Mock<IPensionSchemeService>();
            userProfile = new UserProfile { UserName = "admin" };
            mockHttpPostedFile = new MockHttpPostedFile(Encoding.ASCII.GetBytes("test"), "text", "text.notAccepted");

            mockSessionManager.Setup(s => s.GetScheme()).Returns(new Scheme { PensionScheme = pensionSchemeMock.Object });
            mockSessionManager.Setup(s => s.GetUserProfile()).Returns(userProfile);

            context = new MockHttpContext();
            (context.Request as MockHttpRequest).AddRequestItem("update", "Update");

            controller = ControllerFactory.Get<SchemeController>(context);
            controller.SessionManager = mockSessionManager.Object;
            controller.PensionSchemeService = pensionSchemeServicMock.Object;

            ConfigurationManager.AppSettings["AcceptedSchemeDocumentTypes"] = ".docx, .pdf, .pptx, .xlsx";
        }

        [Test]
        public void AcceptableDocumentTypeUploadSucceeds()
        {
            var acceptableMockHttpPostedFile = new MockHttpPostedFile(Encoding.ASCII.GetBytes("test"), "text", "text.docx");
            mockScheme = new Mock<IScheme>();

            var saveResult = new DetailedResult<ISchemeAdminServiceResult, IScheme>
            {
                Success = true, 
                Result = mockScheme.Object
            };

            pensionSchemeServicMock.Setup(p => p.Save(It.IsAny<string>(), It.IsAny<SchemeDetail>(), It.IsAny<IEnumerable<SchemeDocument>>(), It.IsAny<string>(), It.IsAny<int>(), null)).Returns(saveResult);

            var model = new AdditSchemeViewModel
            {
                DocumentFileNames = new List<string> { "test.docx" }, //acceptable types are defined in the App.Config file for this test assembly
                DocumentIds = new List<int> { -1 },
                DocumentNames = new List<string> { "test" },
                Documents = new List<HttpPostedFileBase> { acceptableMockHttpPostedFile }
            };

            var result = controller.Edit(model);

            var errors = new StringBuilder();

            if(((System.Web.Mvc.ViewResult)result).ViewData.ModelState.Count() > 0)
            {
                foreach(var value in ((System.Web.Mvc.ViewResult)result).ViewData.ModelState.Values.ToList())
                {
                    foreach (var msError in value.Errors)
                    {
                        errors.AppendLine(msError.ErrorMessage);
                    }
                }
            }

            Assert.IsTrue(((System.Web.Mvc.ViewResult)result).ViewData.ModelState.Count() == 0, $"There should be no errors on the model state for a succesfull upload. Errors: {errors.ToString()}");
            Assert.IsTrue(((ManageSchemeViewModel)((System.Web.Mvc.ViewResult)result).Model).SchemeImportHistory.Count() == 1, "There should be an import history count of 1 i.e. the succesfull upload");
            Assert.IsTrue(((ManageSchemeViewModel)((System.Web.Mvc.ViewResult)result).Model).AdditSchemeViewModel.Result == AdditSchemeResult.AddSchemeSuccess, "Upload result should be marked as successfull");
        }

        [Test]
        public void OnlyAcceptableDocumentTypesCanBeUploaded()
        {
            var model = new AdditSchemeViewModel
            {
                DocumentFileNames = new List<string> { "test.notAccepted" }, //acceptable types are defined in the App.Config file for this test assembly
                DocumentIds = new List<int> { -1 },
                DocumentNames = new List<string> { "test" },
                Documents = new List<HttpPostedFileBase> { mockHttpPostedFile }
            };

            var result = controller.Edit(model);

            assertDocUploadFail(result);
        }

        [Test]
        public void OnlyAcceptableDocumentTypesCanBeEdited()
        {
            var model = new AdditSchemeViewModel
            {
                DocumentFileNames = new List<string> { "test.notAccepted" }, //acceptable types are defined in the App.Config file for this test assembly
                DocumentIds = new List<int> { 1 },
                DeleteDocuments = new List<int> { 0 },
                DocumentNames = new List<string> { "test" },
                Documents = new List<HttpPostedFileBase> { mockHttpPostedFile }
            };

            var result = controller.Edit(model);

            assertDocUploadFail(result);
        }

        private void assertDocUploadFail(System.Web.Mvc.ActionResult result)
        {
            Assert.IsTrue(((System.Web.Mvc.ViewResult)result).ViewData.ModelState["Documents"].Errors.Count() == 1, "Documents on the ModelState should have an error for the unacceptable file type");
            Assert.IsTrue(((System.Web.Mvc.ViewResult)result).ViewData.ModelState["Documents"].Errors[0].ErrorMessage.StartsWith("Documents accepted"), "The error message should relate to the types that are acceptable");
        }
    }
}
