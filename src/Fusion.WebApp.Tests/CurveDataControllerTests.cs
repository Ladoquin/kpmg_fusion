﻿using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using Fusion.Shared.Mocks;
using Fusion.WebApp.Controllers;
using Fusion.WebApp.Models;
using Fusion.WebApp.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Fusion.WebApp.Tests
{
    [TestFixture]
    public class CurveDataControllerTests
    {
        [Test]
        public void TestSaveReturnsSuccessfulViewResultWithAValidXmlFile()
        {
            var controller = GetMockController(RoleType.Admin, true);
            var input = GetMockInput();

            var result = controller.Save(input) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as CurveImportViewModel;
            Assert.IsNotNull(model);
            Assert.IsTrue(model.Submitted);
            Assert.AreEqual(ResultEnum.Success, model.Result);
            Assert.AreEqual(0, model.ValidationMessages.Count());
        }

        [Test]
        public void TestSaveReturnsFailedViewResultWithAnInvalidXmlFile()
        {
            var controller = GetMockController(RoleType.Admin, false);
            var input = GetMockInput();

            var result = controller.Save(input) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as CurveImportViewModel;
            Assert.IsNotNull(model);
            Assert.IsTrue(model.Submitted);
            Assert.AreEqual(ResultEnum.Failure, model.Result);
            Assert.AreNotEqual(0, model.ValidationMessages.Count());
        }

        [Test]
        public void TestSaveReturnsFailureWithAnInvalidXmlFileAndAllowsOverride()
        {
            var controller = GetMockController(RoleType.Admin, false);
            var input = GetMockInput();

            var result = controller.Save(input) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as CurveImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Failure, model.Result);
            Assert.IsTrue(model.AllowInvalidSubmission);
        }

        [Test]
        public void TestSaveReturnsFailureWithAnInvalidXmlFileAndDoesNotAllowOverride()
        {
            var controller = GetMockController(RoleType.Installer, false);
            var input = GetMockInput();

            var result = controller.Save(input) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as CurveImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Failure, model.Result);
            Assert.IsFalse(model.AllowInvalidSubmission);
        }

        [Test]
        public void TestForceSaveReturnsSuccessWithValidXmlFile()
        {
            var controller = GetMockController(RoleType.Admin, true);
            var input = GetMockInput();
            var tempData = new TempDataDictionary();
            tempData.Add("curves", new BinaryReader(input.ImportFile.InputStream).ReadBytes((int)input.ImportFile.InputStream.Length));
            tempData.Add("model", input);
            controller.TempData = tempData;

            var result = controller.ForceSave(null) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as CurveImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Success, model.Result);
        }

        [Test]
        public void TestForceSaveReturnsFailureWithInvalidXmlFile()
        {
            var controller = GetMockController(RoleType.Admin, false);
            var input = GetMockInput();
            var tempData = new TempDataDictionary();
            tempData.Add("curves", new BinaryReader(input.ImportFile.InputStream).ReadBytes((int)input.ImportFile.InputStream.Length));
            tempData.Add("model", input);
            controller.TempData = tempData;

            var result = controller.ForceSave(null) as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as CurveImportViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(ResultEnum.Success, model.Result);
        }

        private CurveDataController GetMockController(RoleType role, bool xmlValidates)
        {
            var mockHttpContext = new MockHttpContext { User = new GenericPrincipal(new GenericIdentity(role.ToString().ToLower()), new[] { role.ToString() }) };
            var controller = ControllerFactory.Get<CurveDataController>(mockHttpContext);
            var moqSessionManager = new Mock<ISessionManager>();
            var moqCurveImportDetailService = new Mock<ICurveImportDetailService>();
            var moqXmlSchemaValidator = new Mock<IXmlSchemaValidator>();
            moqXmlSchemaValidator.Setup(x => x.Validate(It.IsAny<Stream>(), It.IsAny<Stream>())).Returns(xmlValidates);
            moqXmlSchemaValidator.SetupGet(x => x.Messages).Returns(xmlValidates ? new List<string>() : new List<string> { "An xml validation error" });
            controller.SessionManager = moqSessionManager.Object;
            controller.CurveService = moqCurveImportDetailService.Object;
            controller.XmlSchemaValidator = moqXmlSchemaValidator.Object;
            return controller;
        }

        private CurveImportViewModel GetMockInput()
        {
            var moqHttpPostedFile = new MockHttpPostedFile(Encoding.ASCII.GetBytes("<root/>"), "text/xml", "");
            var input = new CurveImportViewModel { ImportFile = moqHttpPostedFile };
            return input;
        }
    }
}
