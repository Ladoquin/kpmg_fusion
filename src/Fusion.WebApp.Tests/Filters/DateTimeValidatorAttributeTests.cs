﻿namespace Fusion.WebApp.Tests.Filters
{
    using Fusion.WebApp.Filters;
    using NUnit.Framework;
    using System.Collections.Generic;

    [TestFixture]
    class DateTimeValidatorAttributeTests
    {
        [TestCase("9999999999999999999999999999999999999", "9999999999999999999999999999999999999")]
        [TestCase("-999", "-999")]
        [TestCase("-9999999999999999999999999999999999999", "-9999999999999999999999999999999999999")]
        [TestCase("foo", "bar")]
        [TestCase("NaN", "NaN")]
        public void TestDateTimeValidatorHandlesUnexpectedDates(string start, string end)
        {
            var context = ActionFilterFactory.GetActionExecutingContext(
                new Dictionary<string, object>
                {
                    { "start", start },
                    { "end", end }
                });
            var filter = new DateTimeValidatorAttribute();            
            filter.OnActionExecuting(context);

            Assert.False(context.Controller.ViewData.ModelState.IsValid);
        }

        [TestCase(null, null)]
        [TestCase("", "")]
        [TestCase("0", "0")]
        [TestCase("1364688000000", "1364688000000")] // 31/03/2013 - 01/10/2014
        public void TestDateTimeValidatorHandlesOkDates(string start, string end)
        {
            var context = ActionFilterFactory.GetActionExecutingContext(
                new Dictionary<string, object>
                {
                    { "start", start },
                    { "end", end }
                });
            var filter = new DateTimeValidatorAttribute();
            filter.OnActionExecuting(context);

            Assert.True(context.Controller.ViewData.ModelState.IsValid);
        }
    }
}
