﻿namespace FlightDeck.Tracker.Runner
{
    using FlexCel.XlsAdapter;
    using FlightDeck.Tracker.Db;
    using FlightDeck.Tracker.Models;
    using FlightDeck.Tracker.Parameters;
    using FlightDeck.Tracker.Reader;
    using FlightDeck.Tracker.Writer;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    
    class Program
    {
        static void Main(string[] args)
        {
            //todo: always pass one arg so we don't do a full refresh if user forgets param from Jenkins.

            var stopwatch = new Stopwatch();
            var singleSchemeUpdate = false;
            var schemeRef = string.Empty; 

            stopwatch.Start();
            //string.IsNullOrEmpty(args[1])

            if (args.Length > 0 && !string.IsNullOrEmpty(args[0]))
            {
                if (args[0].ToLower() == "singleschemeupdate" && args.Length == 1)
                {
                    Console.WriteLine("Error: SchemeRef expected for single scheme update.");
                    System.Environment.Exit(1);
                }
                else if (args[0].ToLower() != "singleschemeupdate" && args.Length == 1)
                {
                    Console.WriteLine(string.Format("Error: unrecognised parameter: {0}", args[0]));
                    System.Environment.Exit(1); 
                }
                else if (args[0].ToLower() == "singleschemeupdate" && args.Length > 1)
                {
                    singleSchemeUpdate = true;
                    schemeRef = args[1];
                }
                else
                {
                    Console.WriteLine("Error: Unknown set up error.");
                    System.Environment.Exit(1);
                }
            }
            else
            {
                FusionTrackerDBContext.SetDropCreateDatabaseAlways();
            }

            var TestDataPath = ConfigurationManager.AppSettings["TestDataPath"];
            var TestParametersPath = Path.Combine(TestDataPath, ConfigurationManager.AppSettings["TestParametersFilename"]);
            var TrackerPath = Path.Combine(TestDataPath, "Fusion tracker.xlsm");

            var indexReader = new FinancialIndexRepository();

            var indices = new TrackerIndicesReader(new XlsFile(TrackerPath, false)).Read();

            indexReader.Save(indices);

            var parameters = GetParameters(singleSchemeUpdate, TestParametersPath, schemeRef);

            var writer = new TrackerWriter(TrackerPath);
            
            var parametersByScheme = parameters.GroupBy(parm => parm.SchemeName);

            foreach (var parameterByScheme in parametersByScheme)
            {
                var xmlSourcePath = Path.Combine(TestDataPath, "schemes", parameterByScheme.Key + ".xml");

                writer.InitialiseScheme(xmlSourcePath);

                var cleanup = new List<string>();

                foreach (var parameter in parameterByScheme)
                {
                    var scenario = new TrackerData
                        {
                            ScenarioId = parameter.Id,
                            SchemeRef = parameter.SchemeRef,
                            SchemeName = parameter.SchemeName,
                            BasisName = parameter.BasisName,
                        };

                    writer.InitialiseBasis(parameter.BasisName);
                    writer.LoadParameters(parameter);

                    var path = Path.Combine(TestDataPath, string.Format("{0}({1})-ID{2}.xlsm", parameterByScheme.Key, parameter.BasisName, parameter.Id.ToString()));

                    writer.Save(path);

                    var xls = new XlsFile(path, false);
                    var schemeReader = new TrackerSchemeReader(xls, indexReader);
                    var assumptionsReader = new TrackerAssumptionsReader(xls);
                    var resultsReader = new TrackerResultsReader(xls, indexReader);

                    scenario.BasisType = schemeReader.ReadSelectedBasisType();
                    scenario.Assumptions = assumptionsReader.Read();
                    scenario.Results = resultsReader.Read();

                    scenario.Results.AdjustedCashflows = null; // Otherwise the data takes waaaaaaay too long to save to db. Shame, as this is useful info to have for debugging.

                    new TrackerDataRepository().Save(scenario);

                    cleanup.Add(path);
                }

                writer.Quit(); //free up memory before starting next scheme

                bool keepWorkingCopies = false;
                if (!bool.TryParse(ConfigurationManager.AppSettings["KeepWorkingCopies"], out keepWorkingCopies) || !keepWorkingCopies)
                    foreach (var path in cleanup)
                        File.Delete(path);
            }

            stopwatch.Stop();

            Console.WriteLine(string.Format("Complete: {0} mins", stopwatch.Elapsed.TotalMinutes.ToString()));
        }

        private static IEnumerable<Parameter> GetParameters(bool singleSchemeUpdate, string TestParametersPath, string schemeRef)
        {
            if (singleSchemeUpdate)
            {
                var parameters = new ParameterReader(TestParametersPath).ReadAll().Where(s => s.SchemeRef == schemeRef).ToList();
                if (parameters == null || parameters.Count() == 0)
                {
                    Console.WriteLine("Error: Scheme ref not found as sheet name on test parameters file.");
                    System.Environment.Exit(1);
                }
                return parameters;
            }
            else
            {
                return new ParameterReader(TestParametersPath).ReadAll().ToList();
            }
        }
    }
}
