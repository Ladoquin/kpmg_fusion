﻿namespace FlightDeck.Domain.UnitTests
{
    using FlightDeck.DomainShared;
    using System;
    using Excel = Microsoft.Office.Interop.Excel;
    
    class TrackerWriterBase : TrackerBase
    {
        protected void SetBasis(BasisType basisType, Excel.Workbook xlWorkBook)
        {
            var listItemValue = string.Empty;
            var i = 1;
            do
            {
                var sheetName = string.Format("Liabs_{0}", i.ToString());
                var sheet = xlWorkBook.Worksheets.Item[sheetName] as Excel.Worksheet;

                var liabBasisType = GetBasisTypeFromString((sheet.Cells[6, GetCol("B")] as Excel.Range).Value.ToString());
                if (liabBasisType == basisType)
                {
                    listItemValue = (sheet.Cells[7, GetCol("B")] as Excel.Range).Value.ToString();
                }

                i++;
            }
            while (string.IsNullOrEmpty(listItemValue) && i <= xlWorkBook.Worksheets.Count);

            if (!string.IsNullOrEmpty(listItemValue))
                ((Excel.Worksheet)xlWorkBook.Worksheets.Item["CALCS"]).Cells[8, 2] = listItemValue;
            else
                throw new Exception(string.Format("Could not set basis type '{0}' on CALCS sheet", basisType.ToString()));
        }

        protected void SetBasis(string basisName, Excel.Workbook xlWorkBook)
        {
            ((Excel.Worksheet)xlWorkBook.Worksheets.Item["CALCS"]).Cells[8, 2] = basisName;
        }

        protected BasisType GetBasisTypeFromString(string value)
        {
            switch (value.ToUpper())
            {
                case "TECHNICAL PROVISIONS": return BasisType.TechnicalProvision;
                case "ACCOUNTING": return BasisType.Accounting;
                case "BUYOUT": return BasisType.Buyout;
                default:
                    throw new Exception("Could not convert basis type string: " + value);
            }
        }
    }
}
