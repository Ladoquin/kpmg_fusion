﻿alter table Bases add AmortisationPeriodForActGain tinyint null
go

update b
set b.AmortisationPeriodForActGain = p.USGAAPAmortActLossPeriod
from bases b
inner join BasisTypes bt on b.BasisType = bt.Id
inner join PensionSchemes p on b.PensionSchemeId = p.Id
where
bt.Name = 'Accounting'

alter table pensionschemes drop column [USGAAPAmortActLossPeriod]
alter table pensionschemes drop column [USGAAPAmortActLossPeriodInForecast]
go

