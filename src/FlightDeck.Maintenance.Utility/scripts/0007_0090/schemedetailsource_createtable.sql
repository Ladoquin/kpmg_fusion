﻿CREATE TABLE [dbo].[SchemeDetailSource](
	[SchemeDetailId] [int] NOT NULL,
	[Source] [xml] NULL,
 CONSTRAINT [PK_SchemeDetailSource] PRIMARY KEY CLUSTERED 
(
	[SchemeDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[SchemeDetailSource]  WITH CHECK ADD  CONSTRAINT [FK_SchemeDetailSource_SchemeDetail] FOREIGN KEY([SchemeDetailId])
REFERENCES [dbo].[SchemeDetail] ([SchemeDetailId])
GO

ALTER TABLE [dbo].[SchemeDetailSource] CHECK CONSTRAINT [FK_SchemeDetailSource_SchemeDetail]
GO


