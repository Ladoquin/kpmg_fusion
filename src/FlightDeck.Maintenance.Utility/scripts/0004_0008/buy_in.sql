﻿alter table LiabilityCashFlows 
add IsBuyIn bit null

GO

update LiabilityCashFlows set IsBuyIn = 0

GO

alter table LiabilityCashFlows 
alter column IsBuyIn bit not null

GO
