﻿ALTER TABLE UserProfile
ADD PasswordMustChange BIT NOT NULL
CONSTRAINT PasswordMustChange_Default DEFAULT 1
GO

UPDATE UserProfile SET PasswordMustChange = 0
