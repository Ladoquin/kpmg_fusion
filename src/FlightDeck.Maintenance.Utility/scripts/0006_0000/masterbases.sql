﻿CREATE TABLE [dbo].[MasterBases](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BasisType] [int] NOT NULL,
	[Tag] [int] NOT NULL,
	[PensionSchemeId] [int] NOT NULL,
 CONSTRAINT [PK_MasterBases] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MasterBases]  WITH CHECK ADD  CONSTRAINT [FK_MasterBases_BasisTypes] FOREIGN KEY([BasisType])
REFERENCES [dbo].[BasisTypes] ([Id])
GO

ALTER TABLE [dbo].[MasterBases] CHECK CONSTRAINT [FK_MasterBases_BasisTypes]
GO

ALTER TABLE [dbo].[MasterBases]  WITH CHECK ADD  CONSTRAINT [FK_MasterBases_PensionSchemes] FOREIGN KEY([PensionSchemeId])
REFERENCES [dbo].[PensionSchemes] ([Id])
GO

ALTER TABLE [dbo].[MasterBases] CHECK CONSTRAINT [FK_MasterBases_PensionSchemes]
GO

alter table bases add MasterBasisId int null
go

insert into MasterBases (PensionSchemeId, BasisType, Tag)
select PensionSchemeId, BasisType, 1 from bases group by PensionSchemeId, BasisType order by PensionSchemeId, BasisType

update b
set b.MasterBasisId = m.Id
from bases b
inner join MasterBases m on b.PensionSchemeId = m.PensionSchemeId and b.BasisType = m.BasisType

alter table bases alter column MasterBasisId int not null
go

alter table ClientAssumptions add MasterBasisId int null
go

update a
set a.MasterBasisId = b.MasterBasisId
from ClientAssumptions a
inner join SchemeDetail s on a.SchemeDetailId = s.SchemeDetailId
inner join PensionSchemes p on s.SchemeData_Id = p.Id
inner join Bases b on p.Id = b.PensionSchemeId and a.BasisType = b.BasisType

alter table ClientAssumptions drop constraint FK_ClientAssumptions_BasisTypes
alter table ClientAssumptions alter column MasterBasisId int not null
alter table ClientAssumptions drop column BasisType
go






