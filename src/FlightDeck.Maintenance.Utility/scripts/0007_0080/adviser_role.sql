﻿IF NOT EXISTS(SELECT * FROM [webpages_Roles] WHERE RoleName = 'Adviser')
BEGIN
	SET IDENTITY_INSERT webpages_Roles ON
	INSERT INTO [webpages_Roles] (RoleId, RoleName) VALUES (5, 'Adviser');
	SET IDENTITY_INSERT webpages_Roles OFF
END
GO