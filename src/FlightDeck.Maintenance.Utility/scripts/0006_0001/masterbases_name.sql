﻿alter table masterbases add Name varchar(100) null
go

update m
set m.Name = isnull(x.displayname, x.basistypename)
from MasterBases m
inner join (
select masterbasisid, displayname, BasisTypes.Name [basistypename]
from bases 
inner join BasisTypes on bases.BasisType = BasisTypes.Id
where bases.id = (select max(id) from bases as b where b.MasterBasisId = bases.MasterBasisId group by (MasterBasisId))
) x on m.Id = x.MasterBasisId



alter table masterbases alter column Name varchar(100) not null
go
alter table masterbases drop column Tag
go

