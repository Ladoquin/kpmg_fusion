﻿alter table PensionSchemes add ABOSalaryType tinyint null
alter table PensionSchemes add USGAAPAmortActLossPeriod tinyint null
alter table PensionSchemes add USGAAPAmortActLossPeriodInForecast tinyint null
go

alter table PensionSchemes add [AccountingIAS19Visible] bit not null default 1
alter table PensionSchemes add [AccountingFRS17Visible] bit not null default 1
alter table PensionSchemes add [AccountingUSGAAPVisible] bit not null default 0
go
alter table PensionSchemes add [AccountingDefaultStandard] tinyint not null default 1
go
ALTER TABLE dbo.PensionSchemes 
ADD CONSTRAINT FK_PensionSchemes_AccountingStandardTypes 
FOREIGN KEY ( AccountingDefaultStandard	) 
REFERENCES dbo.AccountingStandardTypes ( Id )
go

update ps
set 
ps.[AccountingIAS19Visible] = sd.[AccountingIAS19Visible],
ps.[AccountingFRS17Visible] = sd.[AccountingFRS17Visible],
ps.[AccountingUSGAAPVisible] = sd.[AccountingUSGAAPVisible],
ps.[AccountingDefaultStandard] = sd.[AccountingDefaultStandard]
from PensionSchemes ps
inner join SchemeDetail sd on ps.Id = sd.SchemeDetailId

declare @c varchar(200)
declare @sql nvarchar(100)

SELECT @c = default_constraints.name
FROM sys.all_columns
INNER JOIN sys.tables ON all_columns.object_id = tables.object_id
INNER JOIN sys.schemas ON tables.schema_id = schemas.schema_id
INNER JOIN sys.default_constraints ON all_columns.default_object_id = default_constraints.object_id
WHERE schemas.name = 'dbo' AND tables.name = 'SchemeDetail' AND all_columns.name = 'AccountingIAS19Visible'

if @c is not null
begin
set @sql = 'alter table SchemeDetail drop constraint ' + @c
exec sp_executesql @sql
end
select @c = null, @sql = null

SELECT @c = default_constraints.name
FROM sys.all_columns
INNER JOIN sys.tables ON all_columns.object_id = tables.object_id
INNER JOIN sys.schemas ON tables.schema_id = schemas.schema_id
INNER JOIN sys.default_constraints ON all_columns.default_object_id = default_constraints.object_id
WHERE schemas.name = 'dbo' AND tables.name = 'SchemeDetail' AND all_columns.name = 'AccountingFRS17Visible'

if @c is not null
begin
set @sql = 'alter table SchemeDetail drop constraint ' + @c
exec sp_executesql @sql
end
select @c = null, @sql = null

SELECT @c = default_constraints.name
FROM sys.all_columns
INNER JOIN sys.tables ON all_columns.object_id = tables.object_id
INNER JOIN sys.schemas ON tables.schema_id = schemas.schema_id
INNER JOIN sys.default_constraints ON all_columns.default_object_id = default_constraints.object_id
WHERE schemas.name = 'dbo' AND tables.name = 'SchemeDetail' AND all_columns.name = 'AccountingUSGAAPVisible'

if @c is not null
begin
set @sql = 'alter table SchemeDetail drop constraint ' + @c
exec sp_executesql @sql
end
select @c = null, @sql = null

SELECT @c = default_constraints.name
FROM sys.all_columns
INNER JOIN sys.tables ON all_columns.object_id = tables.object_id
INNER JOIN sys.schemas ON tables.schema_id = schemas.schema_id
INNER JOIN sys.default_constraints ON all_columns.default_object_id = default_constraints.object_id
WHERE schemas.name = 'dbo' AND tables.name = 'SchemeDetail' AND all_columns.name = 'AccountingDefaultStandard'

if @c is not null
begin
set @sql = 'alter table SchemeDetail drop constraint ' + @c
exec sp_executesql @sql
end
select @c = null, @sql = null

alter table SchemeDetail drop constraint FK_SchemeDetail_AccountingStandardTypes

alter table SchemeDetail drop column AccountingIAS19Visible
alter table SchemeDetail drop column AccountingFRS17Visible
alter table SchemeDetail drop column AccountingUSGAAPVisible
alter table SchemeDetail drop column AccountingDefaultStandard
go


