﻿alter table Bases add [AOCI] float not null default 0
alter table Bases add [UnrecognisedPriorServiceCost] float not null default 0
alter table Bases add [AmortisationOfPriorServiceCost] float not null default 0
go

