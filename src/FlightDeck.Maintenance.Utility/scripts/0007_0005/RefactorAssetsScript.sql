﻿-- Changes to Assets and Asset classes
-- 1. Create new tables
-- 2. Create foreign keys on new tables
-- 3. Remove existing foreign keys in existing Assets table
-- 4. Create new column(s) in Assets table
-- 5. Create foreign keys on Assets table
-- 6. Move data from Assets table to new tables
-- 7. Update SchemeAssetId in Assets table to establish a link between SchemeAssets table
-- 8. Drop existing column(s) in existing Assets table


BEGIN TRANSACTION

-- Step 1
	CREATE TABLE [dbo].[SchemeAssets] (
		[Id] [int] IDENTITY(1,1) PRIMARY KEY,
		[PensionSchemeId] [int] NOT NULL,
		[EffectiveDate] [datetime] NOT NULL,
		[LDIInForce] [bit] NULL,
		[InterestHedge] [float] NULL,
		[InflationHedge] [float] NULL,
		[PropSyntheticEquity] [float] NULL,
		[PropSyntheticCredit] [float] NULL,
		[GiltDuration] [int] NULL,
		[CorpDuration] [int] NULL,
	)
	GO

	CREATE TABLE [dbo].[AssetFunds] (
		[Id] [int] IDENTITY(1,1) PRIMARY KEY,
		[AssetId] [int] NOT NULL,
		[FinancialIndexId] [int] NOT NULL,
		[Name] [nvarchar](255) NOT NULL,
		[Amount] [float] NOT NULL,
		[IsLDIFund] [bit] NULL
	)
	GO

-- Step 2
	ALTER TABLE [dbo].[SchemeAssets]
	ADD CONSTRAINT FK_SchemeAssets_PensionSchemes
	FOREIGN KEY (PensionSchemeId)
	REFERENCES PensionSchemes(Id)
	GO

	ALTER TABLE [dbo].[AssetFunds]
	ADD CONSTRAINT FK_AssetFunds_Assets
	FOREIGN KEY (AssetId)
	REFERENCES Assets(Id)
	GO

-- Step 3
	ALTER TABLE [dbo].[Assets]
	DROP CONSTRAINT FK_Assets_PensionSchemes
	GO

	ALTER TABLE [dbo].[Assets]
	DROP CONSTRAINT FK_Assets_FinancialIndices
	GO

-- Step 4
	ALTER TABLE [dbo].[Assets]
	ADD SchemeAssetId INT
	GO

-- Step 5
	ALTER TABLE [dbo].[Assets]
	ADD CONSTRAINT FK_Assets_SchemeAssets
	FOREIGN KEY (SchemeAssetId)
	REFERENCES SchemeAssets(Id)
	GO

-- Step 6
	INSERT INTO [dbo].[SchemeAssets] (PensionSchemeId, EffectiveDate)
	SELECT DISTINCT PensionSchemeId, EffectiveDate FROM [dbo].[Assets]
	GO
	
	INSERT INTO [dbo].[AssetFunds] (AssetId, FinancialIndexId, Name, Amount, IsLDIFund)
	SELECT DISTINCT 
	a.Id As AssetId, FinancialIndexId, Name, Amount, 0
	FROM Assets a LEFT JOIN AssetClasses c ON a.AssetClassId = c.Id
	GO

-- Step 7	
	UPDATE Assets 
	SET Assets.SchemeAssetId = s.Id
	FROM 
		Assets a LEFT JOIN
		SchemeAssets s ON a.PensionSchemeId= s.PensionSchemeId AND a.EffectiveDate = s.EffectiveDate
	GO

 -- Step 8
	ALTER TABLE [dbo].[Assets]
	DROP COLUMN PensionSchemeId, FinancialIndexId, Amount, EffectiveDate
	GO

IF (@@ERROR <> 0)   -- Check if any error
    ROLLBACK TRANSACTION
ELSE
    COMMIT TRANSACTION

