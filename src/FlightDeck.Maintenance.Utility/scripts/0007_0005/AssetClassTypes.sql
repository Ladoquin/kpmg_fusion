﻿
insert into AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, Volatility) values 
(14, 'Diversified Credit', 1, -0.025, 0.095, 0.0005, 0),
(15, 'Private Markets', 1, -0.025, 0.095, 0.0005, 0.3)

update AssetClasses set Name = 'Diversified Growth' where Name = 'DGFs'
update AssetClasses set Name = 'Corporates' where Name = 'Corporate Bonds'
update AssetClasses set Category = 2 where Name = 'Cash'

