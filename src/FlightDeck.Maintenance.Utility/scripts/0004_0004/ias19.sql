﻿alter table PensionSchemes
add Ias19ExpenseLoading float null

go

update PensionSchemes set Ias19ExpenseLoading = 0.00015

go

alter table PensionSchemes
alter column Ias19ExpenseLoading float not null