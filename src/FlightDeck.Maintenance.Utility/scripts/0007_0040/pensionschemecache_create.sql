﻿/****** Object:  Table [dbo].[PensionSchemeCache]    Script Date: 07/07/2015 11:59:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PensionSchemeCache](
	[PensionSchemeId] [int] NOT NULL,
	[LDIEvolutionItems] [varchar](max) NULL,
 CONSTRAINT [PK_PensionSchemeCache] PRIMARY KEY CLUSTERED 
(
	[PensionSchemeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PensionSchemeCache]  WITH CHECK ADD  CONSTRAINT [FK_PensionSchemeCache_PensionSchemes] FOREIGN KEY([PensionSchemeId])
REFERENCES [dbo].[PensionSchemes] ([Id])
GO

ALTER TABLE [dbo].[PensionSchemeCache] CHECK CONSTRAINT [FK_PensionSchemeCache_PensionSchemes]
GO


