﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'IndexStartDate' and Object_ID = Object_ID(N'IndexImportDetail'))
BEGIN
	ALTER TABLE IndexImportDetail 
	ADD
	IndexStartDate DATETIME NULL,
	IndexEndDate DATETIME NULL;
END
GO
