﻿CREATE TABLE IndexImportIndices
(
	IndexImportDetailId INT NOT NULL FOREIGN KEY REFERENCES IndexImportDetail(IndexImportDetailId),
	IndexId INT NOT NULL FOREIGN KEY REFERENCES FinancialIndices(Id),
	CONSTRAINT PK_IndexImportDetail PRIMARY KEY (IndexImportDetailId, IndexId)
);
GO