﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP TABLE [dbo].[UserProfilePasswordHistory]

CREATE TABLE [dbo].[UserProfilePasswordHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Password] [varbinary](128) NOT NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_UserProfilePasswordHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[UserProfilePasswordHistory]  WITH CHECK ADD  CONSTRAINT [FK_UserProfilePasswordHistory_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO

ALTER TABLE [dbo].[UserProfilePasswordHistory] CHECK CONSTRAINT [FK_UserProfilePasswordHistory_UserProfile]
GO


