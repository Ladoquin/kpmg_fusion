﻿CREATE TABLE [dbo].[UserProfileSchemeDetail](
	[UserId] [int] NOT NULL,
	[SchemeDetailId] [int] NOT NULL,
 CONSTRAINT [PK_UserProfileSchemeDetail] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[SchemeDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserProfileSchemeDetail]  WITH CHECK ADD  CONSTRAINT [FK_UserProfileSchemeDetail_SchemeDetail] FOREIGN KEY([SchemeDetailId])
REFERENCES [dbo].[SchemeDetail] ([SchemeDetailId])
GO

ALTER TABLE [dbo].[UserProfileSchemeDetail] CHECK CONSTRAINT [FK_UserProfileSchemeDetail_SchemeDetail]
GO

ALTER TABLE [dbo].[UserProfileSchemeDetail]  WITH CHECK ADD  CONSTRAINT [FK_UserProfileSchemeDetail_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO

ALTER TABLE [dbo].[UserProfileSchemeDetail] CHECK CONSTRAINT [FK_UserProfileSchemeDetail_UserProfile]
GO

alter table UserProfile
add CreatedByUserId int null
go

alter table UserProfile
add ActiveSchemeDetailId int null
go

alter table SchemeDetail
add CreatedByUserId int null
go

insert into UserProfileSchemeDetail (UserId, SchemeDetailId)
select up.UserId, up.SchemeDetail_SchemeDetailId 
from UserProfile up
inner join webpages_UsersInRoles r on up.UserId = r.UserId
where up.SchemeDetail_SchemeDetailId is not null
and r.RoleId <> 1

update UserProfile set ActiveSchemeDetailId = SchemeDetail_SchemeDetailId

go

alter table UserProfile drop constraint [FK_dbo.UserProfile_dbo.SchemeDetail_SchemeDetail_SchemeDetailId]
go
alter table UserProfile drop column SchemeDetail_SchemeDetailId
go


update webpages_UsersInRoles 
set RoleId = (select RoleId from webpages_Roles where RoleName = 'Client')
where RoleId = (select RoleId from webpages_Roles where RoleName = 'Consultant')

update webpages_Roles set RoleName = 'Installer' where RoleName = 'Consultant'



