﻿alter table SchemeDetail add [AccountingIAS19Visible] bit not null default 1
alter table SchemeDetail add [AccountingFRS17Visible] bit not null default 1
alter table SchemeDetail add [AccountingUSGAAPVisible] bit not null default 0
go

alter table SchemeDetail drop FK_SchemeDetail_AccountingStandardTypes
go
alter table SchemeDetail drop column [AccountingDefaultStandard]
go
alter table SchemeDetail add [AccountingDefaultStandard] tinyint not null default 1
go
ALTER TABLE dbo.SchemeDetail 
ADD CONSTRAINT FK_SchemeDetail_AccountingStandardTypes 
FOREIGN KEY ( AccountingDefaultStandard	) 
REFERENCES dbo.AccountingStandardTypes ( Id )
go
