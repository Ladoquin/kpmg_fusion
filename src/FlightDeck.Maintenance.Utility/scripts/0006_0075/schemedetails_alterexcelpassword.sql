﻿alter table [dbo].[SchemeDetail]
add AccountingDownloadsPassword varchar(250) null
go

update [dbo].[SchemeDetail] set AccountingDownloadsPassword = [NewExcelEditPassword]

alter table [dbo].[SchemeDetail]
drop column [OldExcelEditPassword]

alter table [dbo].[SchemeDetail]
drop column [NewExcelEditPassword]

alter table [dbo].[SchemeDetail]
drop column [ExcelEditPasswordEffectiveDate]
go

alter table [dbo].[HistoricSchemeDetail]
add AccountingDownloadsPassword varchar(250) null
go

update [dbo].[HistoricSchemeDetail] set AccountingDownloadsPassword = [NewExcelEditPassword]

alter table [dbo].[HistoricSchemeDetail]
drop column [OldExcelEditPassword]

alter table [dbo].[HistoricSchemeDetail]
drop column [NewExcelEditPassword]

alter table [dbo].[HistoricSchemeDetail]
drop column [ExcelEditPasswordEffectiveDate]
go

