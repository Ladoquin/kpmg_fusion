﻿

if exists (select * from webpages_Roles where rolename = 'Consultant')
begin
	if (select roleid from webpages_Roles where RoleName = 'Consultant') <> 4
	begin
		begin try
		begin tran
			set identity_insert webpages_Roles on
		    insert into webpages_Roles (roleid, RoleName) values (4, 'tmp_Consultant')
			update webpages_UsersInRoles set RoleId = 4 where roleid = (select roleid from webpages_Roles where RoleName = 'Consultant')
			delete webpages_Roles where roleid = (select roleid from webpages_Roles where RoleName = 'Consultant')
			update webpages_Roles set RoleName = 'Consultant' where roleid = 4
			set identity_insert webpages_Roles off
			commit tran
		end try
		begin catch 
			rollback tran
		end catch
	end
end
else
begin
	set identity_insert webpages_Roles on
	insert into webpages_Roles (roleid, RoleName) values (4, 'Consultant')
	set identity_insert webpages_Roles off
end



