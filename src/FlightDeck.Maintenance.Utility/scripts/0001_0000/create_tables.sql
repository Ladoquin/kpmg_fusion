/*USE [FlightDeck]*/
GO
/****** Object:  Table [dbo].[AssetCategories]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetCategories](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_AssetCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssetClasses]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetClasses](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Category] [int] NOT NULL,
	[MinReturn] [float] NOT NULL,
	[MaxReturn] [float] NOT NULL,
	[ReturnIncrement] [float] NOT NULL,
	[DefaultReturn] [float] NOT NULL,
	[Volatility] [float] NOT NULL,
 CONSTRAINT [PK_AssetClasses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Assets]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assets](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PensionSchemeId] [int] NOT NULL,
	[AssetClassId] [int] NOT NULL,
	[FinancialIndexId] [int] NOT NULL,
	[Amount] [float] NOT NULL,
	[IncludeInIas] [bit] NOT NULL,
 CONSTRAINT [PK_Assets] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssumptionCategories]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssumptionCategories](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_AssumptionCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssumptionTypes]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssumptionTypes](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_AssumptionType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bases]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bases](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BasisType] [int] NOT NULL,
	[PensionSchemeId] [int] NOT NULL,
 CONSTRAINT [PK_Bases] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BasisTypes]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BasisTypes](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_BasisType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CashFlows]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CashFlows](
	[LiabilityCashFlowId] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[Cash] [float] NOT NULL,
 CONSTRAINT [PK_CashFlows] PRIMARY KEY CLUSTERED 
(
	[LiabilityCashFlowId] ASC,
	[Year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contributions]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contributions](
	[PensionSchemeId] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Employer] [float] NOT NULL,
	[Employee] [float] NOT NULL,
 CONSTRAINT [PK_Contributions] PRIMARY KEY CLUSTERED 
(
	[PensionSchemeId] ASC,
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FinancialAssumptions]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialAssumptions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BasisId] [int] NOT NULL,
	[InitialValue] [float] NOT NULL,
	[AssumptionType] [int] NOT NULL,
	[FinancialIndexId] [int] NOT NULL,
	[Name] [nvarchar](128) NULL,
	[IsVisible] [bit] NOT NULL,
 CONSTRAINT [PK_FinancialAssumptions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_FinancialAssumptions] UNIQUE NONCLUSTERED 
(
	[BasisId] ASC,
	[AssumptionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FinancialIndices]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialIndices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_FinancialIndices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_NameUnique] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IndexValues]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IndexValues](
	[FinancialIndexId] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[Date] [date] NOT NULL,
 CONSTRAINT [IX_IndexValuesUniqueDate] UNIQUE NONCLUSTERED 
(
	[FinancialIndexId] ASC,
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InflationTypes]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InflationTypes](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_InflationType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LiabilityCashFlows]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiabilityCashFlows](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BasisId] [int] NOT NULL,
	[MemberStatus] [int] NOT NULL,
	[Age] [int] NOT NULL,
	[RetirementAge] [int] NOT NULL,
	[PensionIncreaseReference] [int] NULL,
 CONSTRAINT [PK_LiabilityCashFlows] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MemberStatuses]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemberStatuses](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_MemberStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NamedProperties]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NamedProperties](
	[NamedPropertyGroupId] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Value] [nvarchar](128) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NamedPropertyGroups]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NamedPropertyGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NamedPropertyGroupType] [int] NOT NULL,
	[PensionSchemeId] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_NamedPropertyGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_NamedPropertyGroups] UNIQUE NONCLUSTERED 
(
	[PensionSchemeId] ASC,
	[NamedPropertyGroupType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NamedPropertyGroupTypes]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NamedPropertyGroupTypes](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_NamedPropertyGroupTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PensionIncreaseAssumptions]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PensionIncreaseAssumptions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BasisId] [int] NOT NULL,
	[InitialValue] [float] NOT NULL,
	[FinancialIndexId] [int] NULL,
	[ReferenceNumber] [int] NOT NULL,
	[Label] [nvarchar](128) NOT NULL,
	[Minimum] [float] NOT NULL,
	[Maximum] [float] NOT NULL,
	[FixedIncrease] [float] NOT NULL,
	[Category] [int] NOT NULL,
	[InflationVolatility] [float] NOT NULL,
	[InflationType] [int] NULL,
 CONSTRAINT [PK_PensionIncreaseAssumptions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PensionSchemes]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PensionSchemes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AnchorDate] [date] NOT NULL,
	[MarketValue] [float] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Reference] [nvarchar](64) NOT NULL,
	[AccountingDate] [date] NOT NULL,
	[ReturnOnAssets] [float] NOT NULL,
 CONSTRAINT [PK_PensionSchemes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RecoveryPayments]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecoveryPayments](
	[PensionSchemeId] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Amount] [float] NOT NULL,
 CONSTRAINT [PK_RecoveryPayments] PRIMARY KEY CLUSTERED 
(
	[PensionSchemeId] ASC,
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SalaryProgressions]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalaryProgressions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MinAge] [int] NOT NULL,
	[MaxAge] [int] NOT NULL,
	[RetirementAge] [int] NOT NULL,
	[BasisId] [int] NOT NULL,
 CONSTRAINT [PK_SalaryProgressions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SalaryRolls]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalaryRolls](
	[SalaryProgressionId] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[Salary] [float] NOT NULL,
 CONSTRAINT [PK_SalaryRolls] PRIMARY KEY CLUSTERED 
(
	[SalaryProgressionId] ASC,
	[Year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SchemeDataSources]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchemeDataSources](
	[PensionSchemeId] [int] NOT NULL,
	[Ias19Disclosure] [date] NULL,
	[ValuationReport] [date] NULL,
	[AssetData] [date] NULL,
	[MemberData] [date] NULL,
	[BenefitData] [date] NULL,
 CONSTRAINT [PK_SchemeDataSources] PRIMARY KEY CLUSTERED 
(
	[PensionSchemeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SimpleAssumptions]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SimpleAssumptions](
	[BasisId] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[Name] [nvarchar](128) NULL,
	[IsVisible] [bit] NOT NULL,
 CONSTRAINT [PK_SimpleAssumptions] PRIMARY KEY CLUSTERED 
(
	[BasisId] ASC,
	[Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SimpleAssumptionTypes]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SimpleAssumptionTypes](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_SimpleAssumptionTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TotalCosts]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TotalCosts](
	[BasisId] [int] NOT NULL,
	[Active] [float] NOT NULL,
	[Deferred] [float] NOT NULL,
	[Pension] [float] NOT NULL,
	[ServiceCost] [float] NOT NULL,
 CONSTRAINT [PK_TotalCosts] PRIMARY KEY CLUSTERED 
(
	[BasisId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VarCorrelations]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VarCorrelations](
	[AssetClassId] [int] NOT NULL,
	[RelatedClassId] [int] NOT NULL,
	[Variance] [float] NOT NULL,
 CONSTRAINT [PK_VarCorrelations] PRIMARY KEY CLUSTERED 
(
	[AssetClassId] ASC,
	[RelatedClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Version]    Script Date: 19/11/2013 15:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Version](
	[Major] [int] NOT NULL,
	[Minor] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[AssetClasses]  WITH CHECK ADD  CONSTRAINT [FK_AssetClasses_AssetCategories] FOREIGN KEY([Category])
REFERENCES [dbo].[AssetCategories] ([Id])
GO
ALTER TABLE [dbo].[AssetClasses] CHECK CONSTRAINT [FK_AssetClasses_AssetCategories]
GO
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [FK_Assets_AssetClasses] FOREIGN KEY([AssetClassId])
REFERENCES [dbo].[AssetClasses] ([Id])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [FK_Assets_AssetClasses]
GO
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [FK_Assets_FinancialIndices] FOREIGN KEY([FinancialIndexId])
REFERENCES [dbo].[FinancialIndices] ([Id])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [FK_Assets_FinancialIndices]
GO
ALTER TABLE [dbo].[Assets]  WITH CHECK ADD  CONSTRAINT [FK_Assets_PensionSchemes] FOREIGN KEY([PensionSchemeId])
REFERENCES [dbo].[PensionSchemes] ([Id])
GO
ALTER TABLE [dbo].[Assets] CHECK CONSTRAINT [FK_Assets_PensionSchemes]
GO
ALTER TABLE [dbo].[Bases]  WITH CHECK ADD  CONSTRAINT [FK_Bases_BasisType] FOREIGN KEY([BasisType])
REFERENCES [dbo].[BasisTypes] ([Id])
GO
ALTER TABLE [dbo].[Bases] CHECK CONSTRAINT [FK_Bases_BasisType]
GO
ALTER TABLE [dbo].[Bases]  WITH CHECK ADD  CONSTRAINT [FK_Bases_PensionSchemes] FOREIGN KEY([PensionSchemeId])
REFERENCES [dbo].[PensionSchemes] ([Id])
GO
ALTER TABLE [dbo].[Bases] CHECK CONSTRAINT [FK_Bases_PensionSchemes]
GO
ALTER TABLE [dbo].[CashFlows]  WITH CHECK ADD  CONSTRAINT [FK_CashFlows_LiabilityCashFlows] FOREIGN KEY([LiabilityCashFlowId])
REFERENCES [dbo].[LiabilityCashFlows] ([Id])
GO
ALTER TABLE [dbo].[CashFlows] CHECK CONSTRAINT [FK_CashFlows_LiabilityCashFlows]
GO
ALTER TABLE [dbo].[Contributions]  WITH CHECK ADD  CONSTRAINT [FK_Contributions_PensionSchemes] FOREIGN KEY([PensionSchemeId])
REFERENCES [dbo].[PensionSchemes] ([Id])
GO
ALTER TABLE [dbo].[Contributions] CHECK CONSTRAINT [FK_Contributions_PensionSchemes]
GO
ALTER TABLE [dbo].[FinancialAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_FinancialAssumptions_AssumptionType] FOREIGN KEY([AssumptionType])
REFERENCES [dbo].[AssumptionTypes] ([Id])
GO
ALTER TABLE [dbo].[FinancialAssumptions] CHECK CONSTRAINT [FK_FinancialAssumptions_AssumptionType]
GO
ALTER TABLE [dbo].[FinancialAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_FinancialAssumptions_Bases] FOREIGN KEY([BasisId])
REFERENCES [dbo].[Bases] ([Id])
GO
ALTER TABLE [dbo].[FinancialAssumptions] CHECK CONSTRAINT [FK_FinancialAssumptions_Bases]
GO
ALTER TABLE [dbo].[FinancialAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_FinancialAssumptions_FinancialIndices] FOREIGN KEY([FinancialIndexId])
REFERENCES [dbo].[FinancialIndices] ([Id])
GO
ALTER TABLE [dbo].[FinancialAssumptions] CHECK CONSTRAINT [FK_FinancialAssumptions_FinancialIndices]
GO
ALTER TABLE [dbo].[IndexValues]  WITH CHECK ADD  CONSTRAINT [FK_IndexValues_FinancialIndices] FOREIGN KEY([FinancialIndexId])
REFERENCES [dbo].[FinancialIndices] ([Id])
GO
ALTER TABLE [dbo].[IndexValues] CHECK CONSTRAINT [FK_IndexValues_FinancialIndices]
GO
ALTER TABLE [dbo].[LiabilityCashFlows]  WITH CHECK ADD  CONSTRAINT [FK_LiabilityCashFlows_Bases] FOREIGN KEY([BasisId])
REFERENCES [dbo].[Bases] ([Id])
GO
ALTER TABLE [dbo].[LiabilityCashFlows] CHECK CONSTRAINT [FK_LiabilityCashFlows_Bases]
GO
ALTER TABLE [dbo].[LiabilityCashFlows]  WITH CHECK ADD  CONSTRAINT [FK_LiabilityCashFlows_MemberStatus] FOREIGN KEY([MemberStatus])
REFERENCES [dbo].[MemberStatuses] ([Id])
GO
ALTER TABLE [dbo].[LiabilityCashFlows] CHECK CONSTRAINT [FK_LiabilityCashFlows_MemberStatus]
GO
ALTER TABLE [dbo].[NamedProperties]  WITH CHECK ADD  CONSTRAINT [FK_NamedProperties_NamedPropertyGroups] FOREIGN KEY([NamedPropertyGroupId])
REFERENCES [dbo].[NamedPropertyGroups] ([Id])
GO
ALTER TABLE [dbo].[NamedProperties] CHECK CONSTRAINT [FK_NamedProperties_NamedPropertyGroups]
GO
ALTER TABLE [dbo].[NamedPropertyGroups]  WITH CHECK ADD  CONSTRAINT [FK_NamedPropertyGroups_NamedPropertyGroupTypes] FOREIGN KEY([NamedPropertyGroupType])
REFERENCES [dbo].[NamedPropertyGroupTypes] ([Id])
GO
ALTER TABLE [dbo].[NamedPropertyGroups] CHECK CONSTRAINT [FK_NamedPropertyGroups_NamedPropertyGroupTypes]
GO
ALTER TABLE [dbo].[NamedPropertyGroups]  WITH CHECK ADD  CONSTRAINT [FK_NamedPropertyGroups_PensionSchemes] FOREIGN KEY([PensionSchemeId])
REFERENCES [dbo].[PensionSchemes] ([Id])
GO
ALTER TABLE [dbo].[NamedPropertyGroups] CHECK CONSTRAINT [FK_NamedPropertyGroups_PensionSchemes]
GO
ALTER TABLE [dbo].[PensionIncreaseAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_PensionIncreaseAssumptions_AssumptionCategories] FOREIGN KEY([Category])
REFERENCES [dbo].[AssumptionCategories] ([Id])
GO
ALTER TABLE [dbo].[PensionIncreaseAssumptions] CHECK CONSTRAINT [FK_PensionIncreaseAssumptions_AssumptionCategories]
GO
ALTER TABLE [dbo].[PensionIncreaseAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_PensionIncreaseAssumptions_Bases] FOREIGN KEY([BasisId])
REFERENCES [dbo].[Bases] ([Id])
GO
ALTER TABLE [dbo].[PensionIncreaseAssumptions] CHECK CONSTRAINT [FK_PensionIncreaseAssumptions_Bases]
GO
ALTER TABLE [dbo].[PensionIncreaseAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_PensionIncreaseAssumptions_FinancialIndices] FOREIGN KEY([FinancialIndexId])
REFERENCES [dbo].[FinancialIndices] ([Id])
GO
ALTER TABLE [dbo].[PensionIncreaseAssumptions] CHECK CONSTRAINT [FK_PensionIncreaseAssumptions_FinancialIndices]
GO
ALTER TABLE [dbo].[PensionIncreaseAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_PensionIncreaseAssumptions_InflationTypes] FOREIGN KEY([InflationType])
REFERENCES [dbo].[InflationTypes] ([Id])
GO
ALTER TABLE [dbo].[PensionIncreaseAssumptions] CHECK CONSTRAINT [FK_PensionIncreaseAssumptions_InflationTypes]
GO
ALTER TABLE [dbo].[RecoveryPayments]  WITH CHECK ADD  CONSTRAINT [FK_RecoveryPayments_PensionSchemes] FOREIGN KEY([PensionSchemeId])
REFERENCES [dbo].[PensionSchemes] ([Id])
GO
ALTER TABLE [dbo].[RecoveryPayments] CHECK CONSTRAINT [FK_RecoveryPayments_PensionSchemes]
GO
ALTER TABLE [dbo].[SalaryProgressions]  WITH CHECK ADD  CONSTRAINT [FK_SalaryProgressions_Bases] FOREIGN KEY([BasisId])
REFERENCES [dbo].[Bases] ([Id])
GO
ALTER TABLE [dbo].[SalaryProgressions] CHECK CONSTRAINT [FK_SalaryProgressions_Bases]
GO
ALTER TABLE [dbo].[SalaryRolls]  WITH CHECK ADD  CONSTRAINT [FK_SalaryRolls_SalaryProgressions] FOREIGN KEY([SalaryProgressionId])
REFERENCES [dbo].[SalaryProgressions] ([Id])
GO
ALTER TABLE [dbo].[SalaryRolls] CHECK CONSTRAINT [FK_SalaryRolls_SalaryProgressions]
GO
ALTER TABLE [dbo].[SchemeDataSources]  WITH CHECK ADD  CONSTRAINT [FK_SchemeDataSources_PensionSchemes] FOREIGN KEY([PensionSchemeId])
REFERENCES [dbo].[PensionSchemes] ([Id])
GO
ALTER TABLE [dbo].[SchemeDataSources] CHECK CONSTRAINT [FK_SchemeDataSources_PensionSchemes]
GO
ALTER TABLE [dbo].[SimpleAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_SimpleAssumptions_Bases] FOREIGN KEY([BasisId])
REFERENCES [dbo].[Bases] ([Id])
GO
ALTER TABLE [dbo].[SimpleAssumptions] CHECK CONSTRAINT [FK_SimpleAssumptions_Bases]
GO
ALTER TABLE [dbo].[SimpleAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_SimpleAssumptions_SimpleAssumptionTypes] FOREIGN KEY([Type])
REFERENCES [dbo].[SimpleAssumptionTypes] ([Id])
GO
ALTER TABLE [dbo].[SimpleAssumptions] CHECK CONSTRAINT [FK_SimpleAssumptions_SimpleAssumptionTypes]
GO
ALTER TABLE [dbo].[TotalCosts]  WITH CHECK ADD  CONSTRAINT [FK_TotalCosts_Bases] FOREIGN KEY([BasisId])
REFERENCES [dbo].[Bases] ([Id])
GO
ALTER TABLE [dbo].[TotalCosts] CHECK CONSTRAINT [FK_TotalCosts_Bases]
GO
ALTER TABLE [dbo].[VarCorrelations]  WITH CHECK ADD  CONSTRAINT [FK_VarCorrelations_AssetClasses] FOREIGN KEY([AssetClassId])
REFERENCES [dbo].[AssetClasses] ([Id])
GO
ALTER TABLE [dbo].[VarCorrelations] CHECK CONSTRAINT [FK_VarCorrelations_AssetClasses]
GO
ALTER TABLE [dbo].[VarCorrelations]  WITH CHECK ADD  CONSTRAINT [FK_VarCorrelations_AssetClasses1] FOREIGN KEY([RelatedClassId])
REFERENCES [dbo].[AssetClasses] ([Id])
GO
ALTER TABLE [dbo].[VarCorrelations] CHECK CONSTRAINT [FK_VarCorrelations_AssetClasses1]
GO

--User tables

/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 11/25/2013 2:32:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](255) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HistoricSchemeDetail]    Script Date: 11/25/2013 2:32:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistoricSchemeDetail](
	[HistoricSchemeDetailId] [int] IDENTITY(1,1) NOT NULL,
	[SourceSchemeDetailId] [int] NOT NULL,
	[SchemeName] [nvarchar](max) NULL,
	[WelcomeMessage] [nvarchar](max) NULL,
	[Logo] [varbinary](max) NULL,
	[ContactName] [nvarchar](max) NULL,
	[ContactNumber] [nvarchar](max) NULL,
	[ContactEmail] [nvarchar](max) NULL,
	[SchemeData_SchemeRef] [nvarchar](max) NULL,
	[SchemeData_AnchorDate] [datetime] NULL,
	[SchemeData_Id] [int] NULL,
	[SchemeData_ImportedOnDate] [datetime] NULL,
	[SchemeData_ImportedByUser] [nvarchar](max) NULL,
	[SchemeData_ProducedOnDate] [datetime] NULL,
	[SchemeData_ProducedByUser] [nvarchar](max) NULL,
	[SchemeData_Spreadsheet] [nvarchar](max) NULL,
	[SchemeData_Comment] [nvarchar](max) NULL,
	[UpdatedOnDate] [datetime] NOT NULL,
	[UpdatedByUser] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.HistoricSchemeDetail] PRIMARY KEY CLUSTERED 
(
	[HistoricSchemeDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IndexImportDetail]    Script Date: 11/25/2013 2:32:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IndexImportDetail](
	[IndexImportDetailId] [int] IDENTITY(1,1) NOT NULL,
	[ImportedOnDate] [datetime] NULL,
	[ImportedByUser] [nvarchar](max) NULL,
	[ProducedOnDate] [datetime] NULL,
	[ProducedByUser] [nvarchar](max) NULL,
	[Spreadsheet] [nvarchar](max) NULL,
	[Comment] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.IndexImportDetail] PRIMARY KEY CLUSTERED 
(
	[IndexImportDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SchemeDetail]    Script Date: 11/25/2013 2:32:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchemeDetail](
	[SchemeDetailId] [int] IDENTITY(1,1) NOT NULL,
	[SchemeName] [nvarchar](max) NULL,
	[WelcomeMessage] [nvarchar](max) NULL,
	[Logo] [varbinary](max) NULL,
	[ContactName] [nvarchar](max) NULL,
	[ContactNumber] [nvarchar](max) NULL,
	[ContactEmail] [nvarchar](max) NULL,
	[SchemeData_SchemeRef] [nvarchar](max) NULL,
	[SchemeData_AnchorDate] [datetime] NULL,
	[SchemeData_Id] [int] NULL,
	[SchemeData_ImportedOnDate] [datetime] NULL,
	[SchemeData_ImportedByUser] [nvarchar](max) NULL,
	[SchemeData_ProducedOnDate] [datetime] NULL,
	[SchemeData_ProducedByUser] [nvarchar](max) NULL,
	[SchemeData_Spreadsheet] [nvarchar](max) NULL,
	[SchemeData_Comment] [nvarchar](max) NULL,
	[UpdatedOnDate] [datetime] NOT NULL,
	[UpdatedByUser] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.SchemeDetail] PRIMARY KEY CLUSTERED 
(
	[SchemeDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 11/25/2013 2:32:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[EmailAddress] [nvarchar](max) NULL,
	[TermsAccepted] [bit] NOT NULL,
	[SchemeDetail_SchemeDetailId] [int] NULL,
 CONSTRAINT [PK_dbo.UserProfile] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


/****** Object:  Table [dbo].[webpages_Membership]    Script Date: 11/25/2013 2:32:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Membership](
	[UserId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ConfirmationToken] [nvarchar](128) NULL,
	[IsConfirmed] [bit] NULL,
	[LastPasswordFailureDate] [datetime] NULL,
	[PasswordFailuresSinceLastSuccess] [int] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordChangedDate] [datetime] NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[PasswordVerificationToken] [nvarchar](128) NULL,
	[PasswordVerificationTokenExpirationDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



/****** Object:  Table [dbo].[webpages_OAuthMembership]    Script Date: 11/25/2013 2:32:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_OAuthMembership](
	[Provider] [nvarchar](30) NOT NULL,
	[ProviderUserId] [nvarchar](100) NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Provider] ASC,
	[ProviderUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_Roles]    Script Date: 11/25/2013 2:32:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[RoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_UsersInRoles]    Script Date: 11/25/2013 2:32:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_UsersInRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[webpages_Membership] ADD  DEFAULT ((0)) FOR [IsConfirmed]
GO
ALTER TABLE [dbo].[webpages_Membership] ADD  DEFAULT ((0)) FOR [PasswordFailuresSinceLastSuccess]
GO
ALTER TABLE [dbo].[UserProfile]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserProfile_dbo.SchemeDetail_SchemeDetail_SchemeDetailId] FOREIGN KEY([SchemeDetail_SchemeDetailId])
REFERENCES [dbo].[SchemeDetail] ([SchemeDetailId])
GO
ALTER TABLE [dbo].[UserProfile] CHECK CONSTRAINT [FK_dbo.UserProfile_dbo.SchemeDetail_SchemeDetail_SchemeDetailId]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_RoleId]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_UserId]
GO



/****** Object:  Table [dbo].[SchemeDocuments]    Script Date: 04/12/2013 17:52:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SchemeDocuments](
	[SchemeDocumentId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[FileName] [nvarchar](max) NOT NULL,
	[Content] [varbinary](max) NOT NULL,
	[ContentType] [nvarchar](max) NOT NULL,
	[SchemeDetail_SchemeDetailId] [int] NULL,
 CONSTRAINT [PK_dbo.SchemeDocuments] PRIMARY KEY CLUSTERED 
(
	[SchemeDocumentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[SchemeDocuments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SchemeDocuments_dbo.SchemeDetail_SchemeDetail_SchemeDetailId] FOREIGN KEY([SchemeDetail_SchemeDetailId])
REFERENCES [dbo].[SchemeDetail] ([SchemeDetailId])
GO

ALTER TABLE [dbo].[SchemeDocuments] CHECK CONSTRAINT [FK_dbo.SchemeDocuments_dbo.SchemeDetail_SchemeDetail_SchemeDetailId]
GO

