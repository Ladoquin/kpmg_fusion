INSERT INTO MemberStatuses(Id, Name) VALUES (1, 'Active Past')
INSERT INTO MemberStatuses(Id, Name) VALUES (2, 'Active Future')
INSERT INTO MemberStatuses(Id, Name) VALUES (3, 'Deferred')
INSERT INTO MemberStatuses(Id, Name) VALUES (4, 'Pensioner')

GO

INSERT INTO AssumptionTypes(Id, Name) VALUES (1, 'Preretirement Discount Rate')
INSERT INTO AssumptionTypes(Id, Name) VALUES (2, 'PostRetirement Discount Rate')
INSERT INTO AssumptionTypes(Id, Name) VALUES (3, 'Pension Discount Rate')
INSERT INTO AssumptionTypes(Id, Name) VALUES (4, 'Salary Increase')
INSERT INTO AssumptionTypes(Id, Name) VALUES (5, 'Inflation Retail Price Index')
INSERT INTO AssumptionTypes(Id, Name) VALUES (6, 'Inflation Consumer Price Index')

GO

INSERT INTO BasisTypes(Id, Name) VALUES (1, 'Accounting')
INSERT INTO BasisTypes(Id, Name) VALUES (2, 'Buyout')
INSERT INTO BasisTypes(Id, Name) VALUES (3, 'Technical Provision')
INSERT INTO BasisTypes(Id, Name) VALUES (4, 'Gilts')

GO

INSERT INTO AssetCategories(Id, Name) VALUES (0, 'Liabilities')
INSERT INTO AssetCategories(Id, Name) VALUES (1, 'Growth')
INSERT INTO AssetCategories(Id, Name) VALUES (2, 'Matching')
INSERT INTO AssetCategories(Id, Name) VALUES (3, 'Other')

GO

INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (1, 'UK Equities',			1, -0.04, 0.05, 0.0005, 0.04, 0.2)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (2, 'Overseas Equities',		1, -0.04, 0.05, 0.0005, 0.04, 0.2)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (3, 'Gilts',					2, -0.04, 0.05, 0.0005, 0,    0.09)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (4, 'Property',				1, -0.04, 0.05, 0.0005, 0.035,0.13)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (5, 'Hedge Fund',				1, -0.04, 0.05, 0.0005, 0.04, 0.0)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (6, 'Corporate Bonds',		2, -0.04, 0.05, 0.0005, 0.012,0.105)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (7, 'Index Linked Gilts',		2, -0.04, 0.05, 0.0005, 0.00, 0.07)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (8, 'Cash',					3, -0.04, 0.05, 0.0005, -0.025,0.0)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (9, 'Asset Backed Fund',		2, -0.04, 0.05, 0.0005, 0.012, 0.105)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (10, 'Equities',				1, -0.04, 0.05, 0.0005, 0.04,  0.2)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (11, 'Non Gilts',				2, -0.04, 0.05, 0.0005, 0.04,  0)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (12, 'Fixed Interest Gilts',	2, -0.04, 0.05, 0.0005, 0.00,   0.09)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (13, 'DGFs',					1, -0.04, 0.05, 0.0005, 0.038, 0.125)

INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (101, 'Liability Interest',  0, 0.00, 0.00, 0.00, 0.0, 0.09)
INSERT INTO AssetClasses(Id, Name, Category, MinReturn, MaxReturn, ReturnIncrement, DefaultReturn, Volatility) VALUES (102, 'Liability Longevity', 0, 0.00, 0.00, 0.00, 0.0, 0.035)

GO

INSERT INTO AssumptionCategories(Id, Name) VALUES (1, 'Fixed')
INSERT INTO AssumptionCategories(Id, Name) VALUES (2, 'Curved')
INSERT INTO AssumptionCategories(Id, Name) VALUES (3, 'Black Scholes')

GO

INSERT INTO SimpleAssumptionTypes(Id, Name) VALUES (1, 'Expense Loading')
INSERT INTO SimpleAssumptionTypes(Id, Name) VALUES (2, 'Male Married Proportion')
INSERT INTO SimpleAssumptionTypes(Id, Name) VALUES (3, 'Female Married Proportion')
INSERT INTO SimpleAssumptionTypes(Id, Name) VALUES (4, 'Sacrifice Proportion')
INSERT INTO SimpleAssumptionTypes(Id, Name) VALUES (5, 'Life Expectancy 65')
INSERT INTO SimpleAssumptionTypes(Id, Name) VALUES (6, 'Life Expectancy 65 (45)')
INSERT INTO SimpleAssumptionTypes(Id, Name) VALUES (7, 'Commutation Allowance')
GO

INSERT INTO InflationTypes(Id, Name) VALUES (1, 'RPI')
INSERT INTO InflationTypes(Id, Name) VALUES (2, 'CPI')

GO

INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (101, 101, 1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (101, 102, 0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (101, 12,  0.9)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (101, 7,   0.9)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (101, 9,   0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (101, 6,	  0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (101, 10,  0.35)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (101, 13,  0.15)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (101, 4,   0.15)

INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (102, 101, 0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (102, 102, 1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (102, 12,  0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (102, 7,   0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (102, 9,   0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (102, 6,   0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (102, 10,  0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (102, 13,  0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (102, 4,   0)

INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (12, 101, 0.9)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (12, 102, 0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (12, 12,  1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (12, 7,   0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (12, 9,   0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (12, 6,   0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (12, 10,  0.35)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (12, 13,  0.25)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (12, 4,   0.20)

INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (7, 101, 0.9)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (7, 102, 0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (7, 12,  0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (7, 7,   1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (7, 9,   0.55)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (7, 6,   0.55)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (7, 10,  0.25)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (7, 13,  0.2)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (7, 4,   0.2)

INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (9, 101, 0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (9, 102, 0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (9, 12,  0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (9, 7,   0.55)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (9, 9,   1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (9, 6,   1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (9, 10,  0.5)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (9, 13,  0.4)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (9, 4,   0.2)

INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (6, 101, 0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (6, 102, 0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (6, 12,  0.75)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (6, 7,   0.55)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (6, 9,   1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (6, 6,   1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (6, 10,  0.5)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (6, 13,  0.4)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (6, 4,   0.2)

INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (10, 101, 0.35)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (10, 102, 0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (10, 12,  0.35)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (10, 7,   0.25)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (10, 9,   0.5)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (10, 6,   0.5)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (10, 10,  1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (10, 13,  0.8)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (10, 4,   0.25)

INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (13, 101, 0.15)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (13, 102, 0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (13, 12,  0.25)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (13, 7,   0.2)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (13, 9,   0.4)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (13, 6,   0.4)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (13, 10,  0.8)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (13, 13,  1)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (13, 4,   0.2)

INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (4, 101, 0.15)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (4, 102, 0)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (4, 12,  0.2)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (4, 7,   0.2)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (4, 9,   0.2)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (4, 6,   0.2)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (4, 10,  0.2)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (4, 13,  0.2)
INSERT INTO VarCorrelations(AssetClassId, RelatedClassId, Variance) VALUES (4, 4,   1)

GO

INSERT INTO NamedPropertyGroupTypes(Id, Name) VALUES (1, 'Basline Info')
INSERT INTO NamedPropertyGroupTypes(Id, Name) VALUES (2, 'Schedule of Conts')

GO

SET IDENTITY_INSERT [webpages_Roles] ON
INSERT INTO [webpages_Roles](RoleId, RoleName) VALUES (1, 'Admin')
INSERT INTO [webpages_Roles](RoleId, RoleName) VALUES (2, 'Consultant')
INSERT INTO [webpages_Roles](RoleId, RoleName) VALUES (3, 'Client')
SET IDENTITY_INSERT [webpages_Roles] OFF

INSERT INTO [Version](Major, Minor) VALUES (1, 0)

GO
