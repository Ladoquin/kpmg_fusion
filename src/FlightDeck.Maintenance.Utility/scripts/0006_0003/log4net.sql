﻿declare @created bit
declare @sql nvarchar(1000)

set @created = 0

begin try
	set @sql = 'create database FusionLogDB'
	exec sp_executesql @sql
	set @created = 1
end try
begin catch
	raiserror ('Could not create Logging database', 10, 1)
end catch

if @created = 1
begin
	set @sql = null
	set @sql = '
	use master; 
	CREATE LOGIN [FusionLogger] WITH PASSWORD=N''fusionlogger'', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF; 
	use FusionLogDB; 
	CREATE USER [FusionLogger] FOR LOGIN [FusionLogger] WITH DEFAULT_SCHEMA=[dbo];
	EXEC sp_addrolemember N''db_datareader'', N''FusionLogger''
	EXEC sp_addrolemember N''db_datawriter'', N''FusionLogger''
	CREATE TABLE [dbo].[Log] (
				[Id] [int] IDENTITY (1, 1) NOT NULL,
				[Date] [datetime] NOT NULL,
				[Thread] [varchar] (255) NOT NULL,
				[Level] [varchar] (50) NOT NULL,
				[Logger] [varchar] (255) NOT NULL,
				[Message] [varchar] (4000) NOT NULL,
				[Exception] [varchar] (2000) NULL
			);
	'
	exec sp_executesql @sql
end





