SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiabilityIncreases](
	[BasisId] [int] NOT NULL,
	[Age] [int] NOT NULL,
	[Increase] [float] NOT NULL
 ) ON [PRIMARY]
GO
create table #temp_default_increases(Age int,Increase float)

insert into #temp_default_increases (Age, Increase) values (0, 0.025)
insert into #temp_default_increases (Age, Increase) values (10, 0.025)
insert into #temp_default_increases (Age, Increase) values (20, 0.025)
insert into #temp_default_increases (Age, Increase) values (30, 0.025)
insert into #temp_default_increases (Age, Increase) values (40, 0.025)
insert into #temp_default_increases (Age, Increase) values (50, 0.025)
insert into #temp_default_increases (Age, Increase) values (60, 0.025)
insert into #temp_default_increases (Age, Increase) values (70, 0.025)
insert into #temp_default_increases (Age, Increase) values (80, 0.025)
insert into #temp_default_increases (Age, Increase) values (90, 0.025)
insert into #temp_default_increases (Age, Increase) values (100, 0.025)

insert into LiabilityIncreases (BasisId, Age, Increase)
select b.Id, i.Age, i.Increase from Bases b cross join #temp_default_increases i

drop table #temp_default_increases 

GO
ALTER TABLE [dbo].[LiabilityIncreases]  WITH CHECK ADD  CONSTRAINT [FK_LiabilityIncreases_Bases] FOREIGN KEY([BasisId])
REFERENCES [dbo].[Bases] ([Id])
GO
ALTER TABLE [dbo].[LiabilityIncreases] CHECK CONSTRAINT [FK_LiabilityIncreases_Bases]
GO