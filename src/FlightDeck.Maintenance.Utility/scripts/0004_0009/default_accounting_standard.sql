﻿CREATE TABLE [dbo].[AccountingStandardTypes](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](16) NOT NULL,
 CONSTRAINT [PK_AccountingStandardType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

alter table SchemeDetail
add AccountingDefaultStandard tinyint null

go

ALTER TABLE dbo.SchemeDetail 
ADD CONSTRAINT FK_SchemeDetail_AccountingStandardTypes 
FOREIGN KEY ( AccountingDefaultStandard	) 
REFERENCES dbo.AccountingStandardTypes ( Id )
go

insert into AccountingStandardTypes (Id, Name) values
(1, 'IAS19'),
(2, 'FRS17'),
(3, 'USGAAP')


