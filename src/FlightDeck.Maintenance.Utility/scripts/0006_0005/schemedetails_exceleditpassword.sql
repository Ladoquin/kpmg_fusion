﻿IF NOT EXISTS(SELECT * FROM sys.columns WHERE (Name = N'OldExcelEditPassword' OR Name = N'NewExcelEditPassword' OR Name = N'ExcelEditPasswordEffectiveDate') and Object_ID = Object_ID(N'SchemeDetail'))
BEGIN
	ALTER TABLE SchemeDetail 
	ADD
	OldExcelEditPassword NVARCHAR(250) NULL,
	NewExcelEditPassword NVARCHAR(250) NULL,
	ExcelEditPasswordEffectiveDate DATETIME NULL;
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE (Name = N'OldExcelEditPassword' OR Name = N'NewExcelEditPassword' OR Name = N'ExcelEditPasswordEffectiveDate') and Object_ID = Object_ID(N'HistoricSchemeDetail'))
BEGIN
	ALTER TABLE HistoricSchemeDetail 
	ADD
	OldExcelEditPassword NVARCHAR(250) NULL,
	NewExcelEditPassword NVARCHAR(250) NULL,
	ExcelEditPasswordEffectiveDate DATETIME NULL;
END
GO
