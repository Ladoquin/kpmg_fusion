﻿BEGIN TRY
   BEGIN TRANSACTION  
   
	declare @rollbackScript nvarchar(1000),
			@initialDiversifiedGrowth float,
			@initialCorporateBonds float,
			@initialFixedInterestGilts float,
			@initialIndexLinkedGilts float,
			@diversifiedGrowthName varchar(100),
			@corporateBondsName varchar(100),
			@fixedInterestGiltsName varchar(100),
			@indexLinkedGiltsName varchar(100)

	set @diversifiedGrowthName = 'Diversified Growth'
	set @corporateBondsName = 'Corporate Bonds'
	set @fixedInterestGiltsName = 'Fixed Interest Gilts'
	set @indexLinkedGiltsName = 'Index Linked Gilts'

	set @initialDiversifiedGrowth = (select Volatility from AssetClasses where name = @diversifiedGrowthName)
	set @initialCorporateBonds = (select Volatility from AssetClasses where name = @corporateBondsName)
	set @initialFixedInterestGilts = (select Volatility from AssetClasses where name = @fixedInterestGiltsName)
	set @initialIndexLinkedGilts = (select Volatility from AssetClasses where name = @indexLinkedGiltsName)

	set @rollbackScript = 'Rollback Script:

	BEGIN TRY
		BEGIN TRANSACTION  
			update AssetClasses set Volatility = ' + Convert(varchar(100), @initialDiversifiedGrowth) + ' where Name = ''' + @diversifiedGrowthName + '''
			update AssetClasses set Volatility = ' + Convert(varchar(100), @initialCorporateBonds) + ' where Name = ''' + @corporateBondsName + '''
			update AssetClasses set Volatility = ' + Convert(varchar(100), @initialFixedInterestGilts) + ' where Name = ''' + @fixedInterestGiltsName + '''
			update AssetClasses set Volatility = ' + Convert(varchar(100), @initialIndexLinkedGilts) + ' where Name = ''' + @indexLinkedGiltsName + '''			
		COMMIT
	END TRY
	BEGIN CATCH

	  IF @@TRANCOUNT > 0
		 ROLLBACK

	  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	  SELECT @ErrMsg = ERROR_MESSAGE(),
			 @ErrSeverity = ERROR_SEVERITY()

	  RAISERROR(@ErrMsg, @ErrSeverity, 1)
	END CATCH'

	update AssetClasses set Volatility = 0.12 where Name = @diversifiedGrowthName
	update AssetClasses set Volatility = 0.0855 where Name = @corporateBondsName
	update AssetClasses set Volatility = 0.079 where Name = @fixedInterestGiltsName
	update AssetClasses set Volatility = 0.079 where Name = @indexLinkedGiltsName

	print(@rollbackScript)

   COMMIT
END TRY
BEGIN CATCH

  IF @@TRANCOUNT > 0
     ROLLBACK

  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH




