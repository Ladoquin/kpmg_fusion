﻿declare @rollbackScript nvarchar(1000),
		@initialValue int,
		@Key varchar(100)

set @key = 'DefaultCorpDuration'
set @initialValue = (select Value from [Application] where[Key] = @key)
set @rollbackScript = 'Rollback Script:

update [Application]
set Value = ' + Convert(varchar(100), @initialValue) + '
where [Key] = ''' + @Key + '''
Go'

print(@rollbackScript)

update [Application]
set Value = 15
where [Key] = 'DefaultCorpDuration'
Go

