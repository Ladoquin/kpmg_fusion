﻿sp_RENAME 'SchemeDetail.[SchemeData_SourceFilePath]' , 'SchemeData_ExportFile', 'COLUMN'
go
sp_RENAME 'HistoricSchemeDetail.[SchemeData_SourceFilePath]' , 'SchemeData_ExportFile', 'COLUMN'
go

ALTER TABLE SchemeDetail
ALTER COLUMN SchemeData_ExportFile nvarchar(max)

ALTER TABLE HistoricSchemeDetail
ALTER COLUMN SchemeData_ExportFile nvarchar(max)

