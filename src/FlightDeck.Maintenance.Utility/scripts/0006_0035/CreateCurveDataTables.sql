﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CurveData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IndexName] [varchar](500) NOT NULL,
 CONSTRAINT [PK_CurveData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

--

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CurveDataSpotRates](
	[CurveDataId] [int] NOT NULL,
	[Maturity] [int] NOT NULL,
	[Rate] [decimal](21, 18) NOT NULL,
	[Date] [date] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CurveDataSpotRates]  WITH CHECK ADD  CONSTRAINT [FK_CurveDataSpotRates_CurveData] FOREIGN KEY([CurveDataId])
REFERENCES [dbo].[CurveData] ([Id])
GO

ALTER TABLE [dbo].[CurveDataSpotRates] CHECK CONSTRAINT [FK_CurveDataSpotRates_CurveData]
GO

--

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CurveImportDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ImportedOnDate] [datetime] NULL,
	[ImportedByUser] [nvarchar](max) NULL,
	[ProducedOnDate] [datetime] NULL,
	[ProducedByUser] [nvarchar](max) NULL,
	[Spreadsheet] [nvarchar](max) NULL,
	[Comment] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.CurveImportDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
