﻿CREATE TABLE [dbo].[UserProfileFavouriteSchemes](
	[UserId] [int] NOT NULL,
	[SchemeDetailId] [int] NOT NULL,
CONSTRAINT [PK_UserProfileFavouriteSchemes] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[SchemeDetailId] ASC
)WITH (IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserProfileFavouriteSchemes]  WITH CHECK ADD  CONSTRAINT [FK_UserProfileFavouriteSchemes_SchemeDetail] FOREIGN KEY([SchemeDetailId])
REFERENCES [dbo].[SchemeDetail] ([SchemeDetailId])
GO

ALTER TABLE [dbo].[UserProfileFavouriteSchemes] CHECK CONSTRAINT [FK_UserProfileFavouriteSchemes_SchemeDetail]
GO

ALTER TABLE [dbo].[UserProfileFavouriteSchemes]  WITH CHECK ADD  CONSTRAINT [FK_UserProfileFavouriteSchemes_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO

ALTER TABLE [dbo].[UserProfileFavouriteSchemes] CHECK CONSTRAINT [FK_UserProfileFavouriteSchemes_UserProfile]
GO


