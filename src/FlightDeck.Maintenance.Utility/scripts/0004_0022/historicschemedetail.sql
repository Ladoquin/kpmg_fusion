﻿
alter table HistoricSchemeDetail add HasVolatilities bit not null default 0
go
alter table HistoricSchemeDetail add HasVarCorrelations bit not null default 0
go
alter table HistoricSchemeDetail add IsRestricted bit not null default 0
go
