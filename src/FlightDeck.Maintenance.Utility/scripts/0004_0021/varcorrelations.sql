﻿create table [dbo].[VarCorrelationsLookup]
(
	[Id] int identity(1,1) not null,
	[Name] varchar(50) null,
constraint [PK_VarCorrelationsLookup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

set identity_insert VarCorrelationsLookup ON
insert into VarCorrelationsLookup (Id, Name) values (1, 'DEFAULT')
set identity_insert VarCorrelationsLookup OFF


alter table [dbo].[VarCorrelations] add [VarCorrelationId] int null
go

update VarCorrelations set VarCorrelationId = 1

alter table [dbo].[VarCorrelations] alter column [VarCorrelationId] int not null
go

alter table [dbo].[VarCorrelations] add constraint FK_VarCorrelations_VarCorrelationsLookup foreign key ([VarCorrelationId]) references [dbo].[VarCorrelationsLookup]([Id])
go

alter table [dbo].[VarCorrelations] drop constraint PK_VarCorrelations
go

alter table [dbo].[VarCorrelations] add constraint PK_VarCorrelations primary key nonclustered  ([AssetClassId], [RelatedClassId], [VarCorrelationId])
go

alter table [dbo].[PensionSchemes] add VarCorrelationId int null
go

update [PensionSchemes] set VarCorrelationId = 1

alter table [dbo].[PensionSchemes] alter column VarCorrelationId int not null
go

alter table [dbo].[PensionSchemes] add constraint FK_PensionSchemes_VarCorrelationsLookup foreign key ([VarCorrelationId]) references [VarCorrelationsLookup]([Id])
go

create table [dbo].[VolatilitiesLookup]
(
	[Id] int identity(1,1) not null,
	[Name] varchar(50) null,
constraint [PK_VolatilitiesLookup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

set identity_insert VolatilitiesLookup ON
insert into VolatilitiesLookup (Id, Name) values (1, 'DEFAULT')
set identity_insert VolatilitiesLookup OFF

create table [dbo].[Volatilities]
(
	[VolatilityId] int not null,
	[AssetClassId] int not null,
	[Volatility] float not null
constraint [PK_Volatilities] primary key clustered
(
	[VolatilityId], [AssetClassId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

alter table [dbo].[Volatilities] add constraint FK_Volatilities_VolatilitiesLookup foreign key ([VolatilityId]) references [dbo].[VolatilitiesLookup]([Id])
go
alter table [dbo].[Volatilities] add constraint FK_Volatilities_AssetClasses foreign key ([AssetClassId]) references [dbo].[AssetClasses]([Id])
go

insert into [Volatilities]
select 1, Id, Volatility from AssetClasses

alter table [dbo].[PensionSchemes] add [VolatilityId] int null
go

update [PensionSchemes] set [VolatilityId] = 1

alter table [dbo].[PensionSchemes] alter column [VolatilityId] int not null
go

alter table [dbo].[PensionSchemes] add constraint FK_PensionSchemes_VolatilitiesLookup foreign key ([VolatilityId]) references [VolatilitiesLookup]([Id])
go

alter table SchemeDetail add HasVolatilities bit null default 0
go
alter table SchemeDetail add HasVarCorrelations bit null default 0
go

update SchemeDetail set HasVolatilities = 0, HasVarCorrelations = 0

alter table SchemeDetail alter column HasVolatilities bit not null 
go
alter table SchemeDetail alter column HasVarCorrelations bit not null 
go







