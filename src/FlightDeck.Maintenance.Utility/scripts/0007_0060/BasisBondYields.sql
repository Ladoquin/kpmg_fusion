﻿/****** Object:  Table [dbo].[BasisBondYields]    Script Date: 22/09/2015 12:24:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BasisBondYields](
	[BasisId] [int] NOT NULL,
	[Index] [nvarchar](256) NOT NULL,
	[Label] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_BasisBondYields] PRIMARY KEY CLUSTERED 
(
	[BasisId] ASC,
	[Index] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO



