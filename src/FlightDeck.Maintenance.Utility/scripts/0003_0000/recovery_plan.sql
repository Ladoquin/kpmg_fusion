﻿alter table RecoveryPayments
add EndDate date null

alter table RecoveryPayments
add Gap int null

go

update RecoveryPayments set EndDate = [Date], Gap = 12

go

alter table RecoveryPayments
alter column EndDate date not null

alter table RecoveryPayments
alter column Gap int not null
