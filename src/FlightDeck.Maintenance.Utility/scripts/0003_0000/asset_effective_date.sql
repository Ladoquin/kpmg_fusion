﻿alter table Assets
add EffectiveDate date null

go

MERGE INTO Assets A
   USING (select PensionSchemeId, min(AnchorDate) AnchorDate from Bases group by PensionSchemeId) B 
      ON B.PensionSchemeId = A.PensionSchemeId
WHEN MATCHED THEN
   UPDATE 
      SET A.EffectiveDate = B.AnchorDate;
go

alter table Assets
alter column EffectiveDate date not null

go 

alter table PensionSchemes
drop column MarketValue


