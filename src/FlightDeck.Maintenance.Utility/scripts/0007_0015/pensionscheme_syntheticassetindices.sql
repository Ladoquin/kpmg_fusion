﻿ALTER TABLE dbo.PensionSchemes ADD SyntheticEquityIncomeIndexId int NULL
ALTER TABLE dbo.PensionSchemes ADD SyntheticEquityOutgoIndexId int NULL
ALTER TABLE dbo.PensionSchemes ADD SyntheticCreditIncomeIndexId int NULL
ALTER TABLE dbo.PensionSchemes ADD SyntheticCreditOutgoIndexId int NULL
GO
ALTER TABLE dbo.PensionSchemes ADD CONSTRAINT FK_PensionSchemes_SyntheticEquityIncomeIndex_FinancialIndices FOREIGN KEY (SyntheticEquityIncomeIndexId) REFERENCES dbo.FinancialIndices (Id) ON UPDATE  NO ACTION ON DELETE  NO ACTION
ALTER TABLE dbo.PensionSchemes ADD CONSTRAINT FK_PensionSchemes_SyntheticEquityOutgoIndex_FinancialIndices FOREIGN KEY (SyntheticEquityOutgoIndexId) REFERENCES dbo.FinancialIndices (Id) ON UPDATE  NO ACTION ON DELETE  NO ACTION
ALTER TABLE dbo.PensionSchemes ADD CONSTRAINT FK_PensionSchemes_SyntheticCreditIncomeIndex_FinancialIndices FOREIGN KEY (SyntheticCreditIncomeIndexId) REFERENCES dbo.FinancialIndices (Id) ON UPDATE  NO ACTION ON DELETE  NO ACTION
ALTER TABLE dbo.PensionSchemes ADD CONSTRAINT FK_PensionSchemes_SyntheticCreditOutgoIndex_FinancialIndices FOREIGN KEY (SyntheticCreditOutgoIndexId) REFERENCES dbo.FinancialIndices (Id) ON UPDATE  NO ACTION ON DELETE  NO ACTION
GO
