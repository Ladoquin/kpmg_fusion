﻿alter table Bases 
add LastPensionIncrease date null

GO

update Bases set LastPensionIncrease = AnchorDate

alter table Bases
alter column LastPensionIncrease date not null