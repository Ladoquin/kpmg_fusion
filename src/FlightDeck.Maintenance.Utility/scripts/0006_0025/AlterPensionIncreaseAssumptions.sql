﻿alter table PensionIncreaseAssumptions add IsFixed bit not null default 0
go

update PensionIncreaseAssumptions set IsFixed = 1 where FinancialIndexId is null 
