﻿alter table Bases
add RefGiltYieldId int null
go

ALTER TABLE [dbo].[Bases]  WITH CHECK ADD  CONSTRAINT [FK_Bases_FinancialIndices] FOREIGN KEY([RefGiltYieldId])
REFERENCES [dbo].[FinancialIndices] ([Id])
GO

insert into NamedPropertyGroupTypes (Id, Name) values (4, 'Ref Gilt Yield')

