﻿insert into NamedPropertyGroupTypes (Id, Name) values (3, 'Recovery Plan')

insert into NamedPropertyGroups (NamedPropertyGroupType, PensionSchemeId, Name)
select
3, 
Id,
'Recovery Plan Defaults'
from PensionSchemes

insert into NamedProperties (NamedPropertyGroupId, Name, Value)
select
Id,
'Length',
10
from NamedPropertyGroups where NamedPropertyGroupType = 3

insert into NamedProperties (NamedPropertyGroupId, Name, Value)
select
Id,
'AnnualIncs',
0
from NamedPropertyGroups where NamedPropertyGroupType = 3

insert into NamedProperties (NamedPropertyGroupId, Name, Value)
select
Id,
'MaxLumpSum',
30
from NamedPropertyGroups where NamedPropertyGroupType = 3

