﻿alter table Assets 
add DefaultReturn float null
go

update		a
set			a.DefaultReturn = ac.DefaultReturn
from		Assets a
inner join	AssetClasses ac on a.AssetClassId = ac.Id

alter table AssetClasses
drop column DefaultReturn
go

