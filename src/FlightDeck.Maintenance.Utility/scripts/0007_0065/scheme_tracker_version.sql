﻿
ALTER TABLE SchemeDetail
ADD DataCaptureVersion NVARCHAR(50) NULL
GO

ALTER TABLE HistoricSchemeDetail
ADD DataCaptureVersion NVARCHAR(50) NULL
GO

ALTER TABLE [Version]
ADD TrackerVersionSupported NVARCHAR(50) NULL
GO

UPDATE [Version] SET TrackerVersionSupported = '49.01'
GO