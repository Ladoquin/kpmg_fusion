﻿/****** Object:  Table [dbo].[ClientAssumptions]    Script Date: 30/04/2014 15:07:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ClientAssumptions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[SchemeDetailId] [int] NOT NULL,
	[BasisType] [int] NOT NULL,
	[AnalysisDate] [datetime] NOT NULL,
	[Data] [varchar](max) NOT NULL,
	[CreatedByUserId] [int] NOT NULL,
 CONSTRAINT [PK_ClientAssumptions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ClientAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_ClientAssumptions_BasisTypes] FOREIGN KEY([BasisType])
REFERENCES [dbo].[BasisTypes] ([Id])
GO

ALTER TABLE [dbo].[ClientAssumptions] CHECK CONSTRAINT [FK_ClientAssumptions_BasisTypes]
GO

ALTER TABLE [dbo].[ClientAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_ClientAssumptions_SchemeDetail] FOREIGN KEY([SchemeDetailId])
REFERENCES [dbo].[SchemeDetail] ([SchemeDetailId])
GO

ALTER TABLE [dbo].[ClientAssumptions] CHECK CONSTRAINT [FK_ClientAssumptions_SchemeDetail]
GO

ALTER TABLE [dbo].[ClientAssumptions]  WITH CHECK ADD  CONSTRAINT [FK_ClientAssumptions_UserProfile] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO

ALTER TABLE [dbo].[ClientAssumptions] CHECK CONSTRAINT [FK_ClientAssumptions_UserProfile]
GO


