﻿alter table pensionschemes add LDIBasisName nvarchar(128) null
alter table pensionschemes add LDIOverrideAssumptions bit null
alter table pensionschemes add LDIDiscRateIndex int null
alter table pensionschemes add LDIDiscRatePremium float null
alter table pensionschemes add LDIInflationIndex int null
alter table pensionschemes add LDIInflationGap float null
go

ALTER TABLE dbo.PensionSchemes ADD CONSTRAINT
	FK_PensionSchemes_FinancialIndices_LDIDiscRateIndex FOREIGN KEY
	(
	LDIDiscRateIndex
	) REFERENCES dbo.FinancialIndices
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PensionSchemes ADD CONSTRAINT
	FK_PensionSchemes_FinancialIndices_LDIInflationIndex FOREIGN KEY
	(
	LDIInflationIndex
	) REFERENCES dbo.FinancialIndices
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO


