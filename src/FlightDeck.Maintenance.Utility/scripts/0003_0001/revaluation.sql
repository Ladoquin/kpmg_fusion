﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RevaluationTypes](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_RevaluationTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO RevaluationTypes (Id, Name) VALUES (0, 'None')
INSERT INTO RevaluationTypes (Id, Name) VALUES (1, 'Cpi')
INSERT INTO RevaluationTypes (Id, Name) VALUES (2, 'Rpi')
INSERT INTO RevaluationTypes (Id, Name) VALUES (3, 'Fixed')

GO

alter table LiabilityCashFlows 
add MaxAge int null

alter table LiabilityCashFlows 
add RevaluationType int null

GO

update LiabilityCashFlows set MaxAge = Age, RevaluationType = 0

alter table LiabilityCashFlows 
alter column MaxAge int not null

alter table LiabilityCashFlows 
alter column RevaluationType int not null

GO

ALTER TABLE [dbo].[LiabilityCashFlows]  WITH CHECK ADD  CONSTRAINT [FK_LiabilityCashFlows_RevaluationType] FOREIGN KEY([RevaluationType])
REFERENCES [dbo].[RevaluationTypes] ([Id])
GO
ALTER TABLE [dbo].[LiabilityCashFlows] CHECK CONSTRAINT [FK_LiabilityCashFlows_RevaluationType]
GO