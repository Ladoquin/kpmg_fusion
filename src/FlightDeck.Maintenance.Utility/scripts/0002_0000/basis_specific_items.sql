﻿alter table Bases
add AnchorDate date null

go

MERGE INTO Bases B
   USING PensionSchemes S 
      ON B.PensionSchemeId = S.id
WHEN MATCHED THEN
   UPDATE 
      SET B.AnchorDate = S.AnchorDate;

go

alter table Bases
alter column AnchorDate date not null

go 

alter table PensionSchemes
drop column AnchorDate

alter table PensionSchemes
drop column AccountingDate

