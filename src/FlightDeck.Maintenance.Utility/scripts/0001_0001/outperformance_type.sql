﻿alter table PensionSchemes
add IsGrowthFixed bit null

go

update PensionSchemes set IsGrowthFixed = 0

go

alter table PensionSchemes
alter column IsGrowthFixed bit not null