﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using log4net;
using System.Configuration;

namespace FlightDeck.Maintenance.Utility
{
    static class Program
    {
        static int Main(string[] args)
        {
            var silent = false;
            if (args.Length > 0 && args.Any(x => x == "-silent"))
            {
                silent = true;
            }
            var ret = Run(args, silent);
            if (!silent)
                Console.ReadKey();

            return ret;
        }

        private static int Run(IList<string> args, bool silent)
        {
            var config = getConfiguration(args);
            var versionManager = new DbVersionManager(config);
            var log = LogManager.GetLogger("FlightDeck.Maintenance.Utility");
            try
            {
                if (args.Count > 0 && args[0].ToLower() == "-r")
                {
                    versionManager.Rebuild();
                }
                else
                {
                    var isCurrent = versionManager.Initialize();
                    if (!isCurrent)
                    {
                        if (!silent)
                        {
                            log.Info("Would you like to upgrade? y/n");
                            var key = Console.ReadKey();
                            Console.WriteLine();
                            if (key.KeyChar.ToString(CultureInfo.InvariantCulture).ToLower() == "y")
                            {
                                versionManager.Update();
                                versionManager.RunAlwaysRun();
                            }
                        }
                        else
                        {
                            versionManager.Update();
                            versionManager.RunAlwaysRun();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);

                return -1;
            }

            return 0;
        }

        private static IFusionMaintenanceSection getConfiguration(IList<string> args)
        {
            var config = (FusionMaintenanceSection)ConfigurationManager.GetSection("FusionMaintenanceSection");

            var settings = new UserDefinedConfiguration(config);

            foreach(var arg in args)
            {
                if (arg.ToLower().StartsWith("/s:"))
                    settings.Server = arg.Substring(3);
                if (arg.ToLower().StartsWith("/u:"))
                    settings.UserName = arg.Substring(3);
                if (arg.ToLower().StartsWith("/p:"))
                    settings.Password = arg.Substring(3);
                if (arg.ToLower().StartsWith("/d:"))
                    settings.DbName = arg.Substring(3);
                if (arg.ToLower().StartsWith("/c:"))
                    settings.SqlCmdPath = arg.Substring(3);
            }

            return settings;
        }

        private class UserDefinedConfiguration : IFusionMaintenanceSection
        {
            public string SqlCmdPath { get; set; }
            public string DbName { get; set; }
            public string Server { get; set; }
            public string ScriptsPath { get; set; }
            public string AlwaysRunPath { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }

            public UserDefinedConfiguration()
            {
            }
            public UserDefinedConfiguration(FusionMaintenanceSection defaults)
            {
                SqlCmdPath = defaults.SqlCmdPath;
                DbName = defaults.DbName;
                Server = defaults.Server;
                ScriptsPath = defaults.ScriptsPath;
                AlwaysRunPath = defaults.AlwaysRunPath;
                UserName = defaults.UserName;
                Password = defaults.Password;
            }
        }
    }
}
