﻿using FlightDeck.DomainShared;
using FlightDeck.Maintenance;
using FlightDeck.ServiceInterfaces;
using FlightDeck.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace FlightDeck.IntegrationTests
{
    [TestFixture]
    public class AccountServiceTests
    {
        //TODO slim this down
        //TODO should probably not depend on user service for results

        const string Admin = "admin-test-foo";
        const string Installer = "installer-test-foo";
        const string Client = "client-test-foo-uno";
        const string Client2 = "client-test-foo-dos";
        const string Client3 = "client-test-foo-tres";

        Dictionary<string, int> Ids;

        private List<string> _usersCreated = new List<string>();

        IRoleManager RoleMgr
        {
            get
            {
                var roleMgr = new Mock<IRoleManager>();
                roleMgr.Setup(r => r.GetRole(Admin)).Returns(RoleType.Admin);
                roleMgr.Setup(r => r.GetRole(Installer)).Returns(RoleType.Installer);
                roleMgr.Setup(r => r.GetRole(Client)).Returns(RoleType.Client);
                roleMgr.Setup(r => r.GetRole(Client2)).Returns(RoleType.Client);
                roleMgr.Setup(r => r.GetRole(Client3)).Returns(RoleType.Client);
                roleMgr
                    .Setup(x => x.Delete(It.IsAny<string>()))
                    .Callback((string username) =>
                    {
                        new DbVersionManager().RunScript(@"delete webpages_UsersInRoles where userId = (select UserId from UserProfile where username = '" + username + "')");
                    });
                roleMgr
                    .Setup(x => x.AddUserToRole(It.IsAny<string>(), It.IsAny<string>()))
                    .Callback((string username, string roleName) =>
                    {
                        var roleId = (int)(RoleType)Enum.Parse(typeof(RoleType), roleName);
                        new DbVersionManager().RunScript(@"insert webpages_UsersInRoles (UserId, RoleId) values ((select UserId from UserProfile where username = '" + username + "'), " + roleId.ToString() + ")");
                    });
                return roleMgr.Object;
            }
        }

        IPrincipal AdminPrincipal
        {
            get
            {
                var principal = new Mock<IPrincipal>();
                principal.Setup(p => p.Identity.Name).Returns(Admin);
                return principal.Object;
            }
        }

        [SetUp]
        public void SetUp()
        {
            DataAccess.DbInteractionScope.Configure(System.Configuration.ConfigurationManager.ConnectionStrings["FlightDeck"].ConnectionString);

            var VersionManager = new DbVersionManager();

            #region sql setup 

            VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-foo', '2014-01-01')");

            VersionManager.RunScript(@"
                insert into SchemeDocuments (Name, FileName, Content, ContentType, SchemeDetail_SchemeDetailId)
                values ('document-foo', 'document-foo.txt', 0x, 'text/plain', (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'))");

            VersionManager.RunScript(@"
                insert into SchemeDocuments (Name, FileName, Content, ContentType, SchemeDetail_SchemeDetailId)
                values ('document-foo-2', 'document-foo-2.txt', 0x, 'text/plain', (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'))");

            VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-bar', '2014-01-01')");

            VersionManager.RunScript(@"
                insert into SchemeDocuments (Name, FileName, Content, ContentType, SchemeDetail_SchemeDetailId)
                values ('document-bar', 'document1-bar.txt', 0x, 'text/plain', (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-bar'))");

            VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-raz', '2014-01-01')");

            UserHelper.CreateUserInDb(Admin, "scheme-foo", RoleType.Admin, "admin", "foo", "admin-foo@space01.co.uk");

            UserHelper.CreateUserInDb(Installer, "scheme-foo", RoleType.Installer, "installer", "foo", "installer-foo@space01.co.uk");
            UserHelper.AssosciateUserWithScheme(Installer, "scheme-foo");
            UserHelper.AssosciateUserWithScheme(Installer, "scheme-bar");

            UserHelper.CreateUserInDb(Client, "scheme-foo", RoleType.Client, "client", "foo", "client-foo@space01.co.uk");
            UserHelper.AssosciateUserWithScheme(Client, "scheme-foo");

            UserHelper.CreateUserInDb(Client2, "scheme-bar", RoleType.Client, "client2", "foo", "client2-foo@space01.co.uk");
            UserHelper.AssosciateUserWithScheme(Client2, "scheme-bar");

            UserHelper.CreateUserInDb(Client3, "scheme-raz", RoleType.Client, "client3", "foo", "client3-foo@space01.co.uk");
            UserHelper.AssosciateUserWithScheme(Client3, "scheme-raz");

            #endregion

            Ids = new Dictionary<string, int>(5);
            Ids.Add(Admin, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Admin + "'")));
            Ids.Add(Installer, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Installer + "'")));
            Ids.Add(Client, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Client + "'")));
            Ids.Add(Client2, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Client2 + "'")));
            Ids.Add(Client3, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Client3 + "'")));
        }

        [TearDown]
        public void TearDown()
        {
            var VersionManager = new DbVersionManager();
            var whereUsernameInBuilder = new StringBuilder(100);
            whereUsernameInBuilder.AppendFormat("where username in ('{0}', '{1}', '{2}', '{3}', '{4}'", Admin, Installer, Client, Client2, Client3);
            _usersCreated.ForEach(x => whereUsernameInBuilder.AppendFormat(", '{0}'", x));
            whereUsernameInBuilder.Append(")");
            var whereUsernameIn = whereUsernameInBuilder.ToString();

            VersionManager.RunScript(@"delete webpages_Membership where UserId in (select UserId from UserProfile " + whereUsernameIn + ")");
            VersionManager.RunScript(@"delete webpages_UsersInRoles where UserId in (select UserId from UserProfile " + whereUsernameIn + ")");
            VersionManager.RunScript(@"delete UserProfileSchemeDetail where UserId in (select UserId from UserProfile " + whereUsernameIn + ")");
            VersionManager.RunScript(@"delete UserProfile " + whereUsernameIn);
            VersionManager.RunScript(@"delete SchemeDocuments where Name in ('document-foo', 'document-foo-2', 'document-bar')");
            VersionManager.RunScript(@"delete SchemeDetail where SchemeName in ('scheme-foo', 'scheme-bar', 'scheme-raz')");
        }
        
        [Test]
        [Ignore("Replace with specflow test")]
        public void AcceptTerms()
        {
            new DbVersionManager().RunScript(@"update UserProfile set TermsAccepted = 0 where username = '" + Client + "'");
            var principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns(Client);
            var userSvc = new UserProfileService(principal.Object, RoleMgr);
            var svc = new AccountService();

            svc.AcceptTerms(Client);

            var user = userSvc.GetByUsername(Client);
            Assert.True(user.TermsAccepted);
        }

        [Test]
        public void SetPasswordFlag()
        {
            new DbVersionManager().RunScript(@"update UserProfile set PasswordMustChange = 0 where username = '" + Client + "'");
            var principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns(Client);
            var userSvc = new UserProfileService(principal.Object, RoleMgr);
            var svc = new AccountService();

            svc.SetPasswordFlag(Client, true);

            var user = userSvc.GetByUsername(Client);
            Assert.True(user.PasswordMustChange);
        }
    }
}
