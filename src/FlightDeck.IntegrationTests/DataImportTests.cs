﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using FlightDeck.Domain.UnitTests;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using FlightDeck.Services;
using NUnit.Framework;
using System.Configuration;
using FlightDeck.Maintenance;
using System.Security.Principal;
using Moq;

namespace FlightDeck.IntegrationTests
{
    [Ignore("Delete or replace with specflow tests")]
    public class DataImportTests : DbTestBase
    {
        private readonly TrackerReader testDataProvider;
        private readonly string testDataPath = @"..\..\..\..\test_data";
        private readonly string adminUsername = "admin-test-user";

        private IPrincipal admin
        {
            get
            {
                var principal = new Mock<IPrincipal>();
                principal.Setup(x => x.Identity.Name).Returns(adminUsername);
                return principal.Object;
            }
        }

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            var VersionManager = new DbVersionManager();

            VersionManager.RunScript(@"
                insert into UserProfile (UserName, FirstName, LastName, EmailAddress, TermsAccepted, ActiveSchemeDetailId)
                values ('" + adminUsername + "', 'admin', 'foo', 'admin-foo@space01.co.uk', 1, null)");
            VersionManager.RunScript(@"
                insert into webpages_UsersInRoles (UserId, RoleId) 
                values ((select UserId from UserProfile where UserName = '" + adminUsername + "'), 1)");
        }

        [TearDown]
        public void TearDown()
        {
            var VersionManager = new DbVersionManager();

            VersionManager.RunScript("delete webpages_UsersInRoles where UserId = (select UserId from UserProfile where UserName = '" + adminUsername + "')");
            VersionManager.RunScript("delete UserProfile where username = '" + adminUsername + "'");
        }

        public DataImportTests()
        {
            RebuildDb = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestDataPath"]))
            {
                testDataPath = ConfigurationManager.AppSettings["TestDataPath"];
            }
            testDataProvider = new TrackerReader(testDataPath);
        }

        [Test]
        public void PensionSchemeImport()
        {
            var expectedEvolution = testDataProvider.LiabilityEvolution.Zip(testDataProvider.AssetEvolution,
                (liab, asset) => new
                {
                    Date = liab.Key,
                    Liability = liab.Value.Liabs,
                    Asset = asset.Value.AssetValue
                })
                .ToDictionary(x => x.Date);

            PensionSchemeService pensionSchemeService = null;
            if (RebuildDb)
            {
                var indexService = ImportIndicesFromXml();

                pensionSchemeService = new PensionSchemeService(admin, indexService.FinancialIndexRepository);

                var doc = XDocument.Load(Path.Combine(testDataPath, "export.xml"));
                var result = pensionSchemeService.Save(null, new SchemeDetail() { SchemeName = "The Space01 Test Scheme" }, null, null, 1, doc);
                Assert.IsTrue(result.Success);
            }
            else
            {
                pensionSchemeService = new PensionSchemeService(admin, new IndexImportDetailService(false).FinancialIndexRepository);
            }

            new DbVersionManager().RunScript(string.Format("update UserProfile set ActiveSchemeDetailId = (select SchemeDetailId from SchemeDetail where SchemeName = 'The Space01 Test Scheme') where username = '{0}'", adminUsername));            

            var scheme = pensionSchemeService.GetActive().PensionScheme;

            RecoveryPlanTypes(scheme);

            Assert.That(scheme.GetBasisTypes().Contains(BasisType.Accounting));
            Assert.That(scheme.Reference, Is.EqualTo("Space01"));
            Assert.That(scheme.DataSource.ValuationReport, Is.EqualTo(new DateTime(2013, 10, 11)));
            Assert.That(scheme.DataSource.AssetData, Is.Null);
            
            Assert.That(scheme.NamedProperties[NamedPropertyGroupType.BaselineInfo].Name, Is.EqualTo("IAS P&L Charge 2012"));
            Assert.That(scheme.NamedProperties[NamedPropertyGroupType.ScheduleOfConts].Properties[2].Value, Is.EqualTo("20%"));
            Assert.That(scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.DiscountIncrement, Is.EqualTo(0.005));
            Assert.That(scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.InvestmentGrowth, Is.EqualTo(0.005));
            Assert.That(scheme.CurrentBasis.GetClientRecoveryAssumptions().Before.IsGrowthFixed, Is.False);
            
            var basis = scheme.GetDefaultBasis(BasisType.TechnicalProvision);
            var a = basis.Evolution.First().Value.TotalAssetsPlusBuyin;
            Assert.That(a, Is.EqualTo(45000000).Within(Utils.TestPrecision));
            //Assert.That(basis.GetMarketInfo().Yields.CorporateBonds, Is.Not.EqualTo(0));
            //Assert.That(basis.DeficitPayment, Is.EqualTo(new DateDoublePair(new DateTime(2013, 1, 1), 250000)));

            //testDataProvider.SetClientAssumptions(basis, scheme);
            //var asset = basis.Assets.First();
            //Assert.That(asset.ActualReturn, Is.EqualTo(0.024624462210366405).Within(Utils.Precision));
            //Assert.That(asset.Category, Is.EqualTo(AssetCategory.Growth));
            //Assert.That(Math.Abs(asset.Amount) < Utils.Precision);
            //Assert.That(basis.TotalCosts.Deficit, Is.EqualTo(27893419.0));

            //Assert.That(basis.AttributionEnd, Is.EqualTo(testDataProvider.AttributionEnd));
            var evolution = basis.Evolution;
            //TODO does this test get run anymore? Assert.That(tracker, Is.EqualTo(expectedTracker));
            
            
            //scheme.ResetAttributionPeriod();
            
            //basis.ResetClientAssetAssumptions();

            //basis.SetClientLiabilityAssumptions(new AssumptionData(0.044, 0.044, 0.044, 0.04, 0.03, 0.02, Utils.TempDouble, Utils.TempDouble));
            //basis.SetClientRecoveryAssumptions(new RecoveryPlanAssumptions(0.055, 0.00, 1000000, 100000000, 0.03, true, 12, 10));
            
            //var r = basis.GetJourneyPlan();
            //Assert.That(r.Count, Is.EqualTo(12));
            

            //var recovery = basis.GetRecoveryPlan();
            //Assert.That(recovery.Before[3], Is.EqualTo(750000));

            //basis.VarStrategy = VarStrategy.IncreaseHedging;
            //var varData = basis.GetVarWaterfall();
            //var varFunnel = basis.GetVarFunnel();
            //Assert.That(varFunnel.Count, Is.EqualTo(3));
            //var froData = basis.GetFroData();
            //var wdr = basis.GetWeightedReturnParameters();
            //var funding = basis.GetFundingLevelData();
            //Assert.That(funding.Liabilities - funding.Assets, Is.EqualTo(funding.Deficit));

            //basis = scheme.GetDefaultBasis(BasisType.TechnicalProvision);
            //Assert.That(basis.SimpleAssumptions[SimpleAssumptionType.LifeExpectancy65].Key, Is.EqualTo("Life expectancy at 65 (male)"));
            //Assert.That(basis.SimpleAssumptions[SimpleAssumptionType.CommutationAllowance].Value, Is.EqualTo(0));
            //Assert.That(basis.AssumptionLabels[AssumptionType.PreRetirementDiscountRate], Is.EqualTo("Pre retirement discount rate"));

            //basis.SetClientLiabilityAssumptions(new AssumptionData(0.044, 0.044, 0.044, 0.04, 0.03, 0.02, Utils.TempDouble, Utils.TempDouble));
            //basis.ClientRecoveryAssumptions.Set(new RecoveryPlanAssumptions(0.055, 0.00, 1000000, 100000000, 0.03, true, 12, Utils.MaxPlanTermInYears));
        }

        private void RecoveryPlanTypes(IPensionScheme scheme)
        {
            //scheme.RecoveryPlanType = RecoveryPlanType.NewFixed;
            //var basis = scheme.CurrentBasis;
            //Assert.That(basis.BasisType, Is.EqualTo(BasisType.TechnicalProvision));
            //var fundingRp = basis.GetRecoveryPlan();

            //basis = scheme.GetDefaultBasis(BasisType.Accounting);
            //var accountingRp = basis.GetRecoveryPlan();
            //Assert.That(fundingRp, Is.EqualTo(accountingRp));
            //basis.ClientRecoveryAssumptions.Set(new RecoveryPlanAssumptions(0.5, 0.05, 1000, 1000000, 0.03, false, 12, 40));
            //scheme.RecoveryPlanType = RecoveryPlanType.NewFixed;
            //accountingRp = basis.GetRecoveryPlan();
            //Assert.That(fundingRp, Is.EqualTo(accountingRp));
            //scheme.RecoveryPlanType = RecoveryPlanType.NewDynamic;
            //accountingRp = basis.GetRecoveryPlan();
            //Assert.That(fundingRp, Is.Not.EqualTo(accountingRp));


        }

        [Test]
        public void PensionSchemeImportShallow()
        {
            PensionSchemeService pensionSchemeService = null;
            if (RebuildDb)
            {
                var indexService = ImportIndicesFromXml();

                pensionSchemeService = new PensionSchemeService(admin, indexService.FinancialIndexRepository);

                var doc = XDocument.Load(Path.Combine(testDataPath, "export.xml"));
                var result = pensionSchemeService.Save(null, new SchemeDetail() { SchemeName = "The Space01 Test Scheme" }, null, null, 1, doc);

                if (!result.Success)
                {
                    Console.WriteLine(result.ValidationErrors[0]);
                    Console.WriteLine(result.DataImportException);
                }

                Assert.That(result.Success, Is.True);
            }
            else
            {
                pensionSchemeService = new PensionSchemeService(admin, new IndexImportDetailService(false).FinancialIndexRepository);
            }

            new DbVersionManager().RunScript(string.Format("update UserProfile set ActiveSchemeDetailId = (select SchemeDetailId from SchemeDetail where SchemeName = 'The Space01 Test Scheme') where username = '{0}'", adminUsername));

            var scheme = pensionSchemeService.GetActive().PensionScheme;

            var basis = scheme.GetDefaultBasis(BasisType.TechnicalProvision);

            //TODO does this test get run anymore? var tracker = basis.GetTrackerData();            
            var analysisPeriod = (int)(scheme.RefreshDate - basis.AnchorDate).TotalDays;
            
            //for (var i = 0; i <= analysisPeriod; i += 5)
            //{
                scheme.SetAttributionPeriod(basis.AnchorDate, basis.AnchorDate.AddDays(20));
                basis.GetSurplusAnalysisData();
                basis.GetJourneyPlan();
                basis.GetBalanceSheetEstimate();
            //}
            
            scheme.SetAttributionPeriod(basis.AnchorDate, new DateTime(2013, 7, 1));
            var jp = basis.GetJourneyPlan();
            var deficit = basis.GetSurplusAnalysisData();
            
            //basis.GetBalanceSheetEstimate();
            //basis.GetFroData();
            //basis.GetWeightedReturnParameters();
            //var r = basis.GetRecoveryPlan();
            ////Assert.That(r.After.Count, Is.Not.EqualTo(0));
            ////Assert.That(r.Before.Count, Is.Not.EqualTo(0));
            //var cf = basis.GetCashFlowData();
            //var fsc = basis.GetFutureServiceCost();
            //scheme.ClientFutureBenefitsParameters.Set(new FutureBenefitsParameters(10, BenefitAdjustmentType.DcImplementation, 10));
            //var fb = basis.GetFutureBenefits();
            //scheme.SetClientPieOptions(new PieOptions(0.1, 0.1, 0.1));
            //var pie = basis.GetPieData();
            //scheme.ClientAbfOptions.Set(new AbfOptions(0.5));
            //var abf = basis.GetAbfData();
            //Console.WriteLine("Done");
        }

        //[Test]
        public void XOldRecoveryPlan()
        {
            PensionSchemeService pensionSchemeService = null;
            if (RebuildDb)
            {
                var indexService = ImportIndicesFromXml();

                pensionSchemeService = new PensionSchemeService(admin, indexService.FinancialIndexRepository);

                var doc = XDocument.Load(Path.Combine(testDataPath, "export.xml"));
                var result = pensionSchemeService.Save(null, new SchemeDetail(), null, null, 1, doc);
                Assert.IsTrue(result.Success);
            }
            else
            {
                pensionSchemeService = new PensionSchemeService(admin, new IndexImportDetailService(false).FinancialIndexRepository);
            }

            new DbVersionManager().RunScript(string.Format("update UserProfile set ActiveSchemeDetailId = (select SchemeDetailId from SchemeDetail where SchemeName = 'The Space01 Test Scheme') where username = '{0}'", adminUsername));

            var scheme = pensionSchemeService.GetActive().PensionScheme;

            var basis = scheme.GetDefaultBasis(BasisType.TechnicalProvision);
            //scheme.RecoveryPlanType = RecoveryPlanType.NewDynamic;

            scheme.SetAttributionPeriod(new DateTime(2013, 04, 20), new DateTime(2013, 06, 30));
            var previousMonth = basis.GetRecoveryPlan().Before;
            
            scheme.SetAttributionPeriod(new DateTime(2013, 04, 20), new DateTime(2013, 07, 1));
            var firstDay = basis.GetRecoveryPlan().Before;
            
            scheme.SetAttributionPeriod(new DateTime(2013, 04, 20), new DateTime(2013, 07, 2));
            var secondDay = basis.GetRecoveryPlan().Before;
            
            scheme.SetAttributionPeriod(new DateTime(2013, 04, 20), new DateTime(2013, 07, 3));
            var thirdDay = basis.GetRecoveryPlan().Before;
            scheme.SetAttributionPeriod(new DateTime(2013, 04, 20), new DateTime(2013, 07, 4));
            var fourthDay = basis.GetRecoveryPlan().Before;
            
            Assert.That(previousMonth, Is.EqualTo(firstDay));
            Assert.That(fourthDay, Is.EqualTo(thirdDay));
            Assert.That(secondDay, Is.EqualTo(thirdDay));
            Assert.That(firstDay, Is.Not.EqualTo(secondDay));
            
            Assert.That(previousMonth.Sum(), Is.EqualTo(firstDay.Sum()));
            Assert.That(previousMonth.Sum(), Is.Not.EqualTo(secondDay.Sum()));
            Assert.That(secondDay.Sum(), Is.EqualTo(thirdDay.Sum()));
            for (var i=0; i < firstDay.Count -2; i++)
            {
                Assert.That(firstDay[i], Is.LessThan(firstDay[i+ 1]));
            }
        }

        [Test]
        public void IndexImport()
        {
            ImportIndicesFromXml();

            //Assert.Throws<SqlException>(() => ImportIndicesFromXml(pensionSchemeService));
        }

        [Test]
        public void IndexUpdate()
        {
            var indexService = ImportIndicesFromXml();

            var doc = XDocument.Load(Path.Combine(testDataPath, "stats_update.xml"));
            indexService.ImportIndices(doc);
        }

        private IndexImportDetailService ImportIndicesFromXml()
        {
            var indexService = new IndexImportDetailService(false);
            var doc = XDocument.Load(Path.Combine(testDataPath, "stats.xml"));
            indexService.ImportIndices(doc);
            return indexService;
            //doc = XDocument.Load(@"..\..\..\..\test_data\stats_update.xml");
            //pensionSchemeService.ImportIndices(doc);
            //TrimIndexValues();
            //doc = XDocument.Load(Path.Combine(testDataPath, "stats_trim.xml"));
            //pensionSchemeService.ImportIndices(doc);
        }

        private void TrimIndexValues()
        {
            VersionManager.RunScript("delete from IndexValues where date > CONVERT(date, '2013-08-21')");
        }
    }
}