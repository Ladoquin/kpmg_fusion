﻿using FlightDeck.Maintenance;
using NUnit.Framework;

namespace FlightDeck.IntegrationTests
{
    [TestFixture]
    [Ignore("Delete or replace with specflow tests.")]//Subclass DataImportTests permanently effect the database
    public class DbTestBase
    {
        protected DbVersionManager VersionManager;
        
        protected bool RebuildDb;

        public DbTestBase()
        {
            RebuildDb = true;
            DataAccess.DbInteractionScope.Configure(System.Configuration.ConfigurationManager.ConnectionStrings["FlightDeck"].ConnectionString);
        }

        [SetUp]
        public virtual void SetUp()
        {
            if (!RebuildDb)
                return;
            
            VersionManager = new DbVersionManager();
            VersionManager.Rebuild();
        }
    }
}
