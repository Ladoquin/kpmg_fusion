﻿using FlightDeck.Maintenance;
using FlightDeck.Services;
using NUnit.Framework;

namespace FlightDeck.IntegrationTests
{
    [TestFixture]
    public class ApplicationSettingsTests
    {
        private DbVersionManager VersionManager = new DbVersionManager();

        [SetUp]
        public void SetUp()
        {
            DataAccess.DbInteractionScope.Configure(System.Configuration.ConfigurationManager.ConnectionStrings["FlightDeck"].ConnectionString);

            VersionManager.RunQuery("delete Application where [key] = 'ImportantMessage'");
            VersionManager.RunQuery("insert Application ([key], value) values ('ImportantMessage', '1[::]We''re Just Friends')");
        }

        [Test]
        public void GetImportantMessage()
        {
            var svc = new ApplicationSettings();

            var message = svc.GetImportantMessage();

            Assert.AreEqual("We\'re Just Friends", message.Message);
        }

        [Test]
        public void SaveImportantMessage()
        {
            var svc = new ApplicationSettings();

            svc.SaveImportantMessage("I fell into a burning ring of fire");

            Assert.True(VersionManager.RunQuery("select Value from Application where [key] = 'ImportantMessage'").Contains("I fell into a burning ring of fire"));
        }
    }
}
