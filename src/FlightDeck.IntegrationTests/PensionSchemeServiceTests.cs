﻿using FlightDeck.Maintenance;
using FlightDeck.Services;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Security.Principal;

namespace FlightDeck.IntegrationTests
{
    [TestFixture]
    public class PensionSchemeServiceTests
    {
        private IPrincipal admin
        {
            get
            {
                var principal = new Mock<IPrincipal>();
                principal.Setup(x => x.Identity.Name).Returns("admin-test-foo");
                return principal.Object;
            }
        }
        private IPrincipal installer
        {
            get
            {
                var principal = new Mock<IPrincipal>();
                principal.Setup(x => x.Identity.Name).Returns("installer-test-foo");
                return principal.Object;
            }
        }

        private int getSchemeBarId()
        {
            return int.Parse(new DbVersionManager().RunQuery("select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-bar'"));
        }

        [SetUp]
        public void SetUp()
        {
            DataAccess.DbInteractionScope.Configure(System.Configuration.ConfigurationManager.ConnectionStrings["FlightDeck"].ConnectionString);

            var VersionManager = new DbVersionManager();

            #region sql setup

           VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-foo', '2014-01-01')");

            VersionManager.RunScript(@"
                insert into SchemeDocuments (Name, FileName, Content, ContentType, SchemeDetail_SchemeDetailId)
                values ('document-foo', 'document-foo.txt', 0x, 'text/plain', (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'))");

            VersionManager.RunScript(@"
                insert into SchemeDocuments (Name, FileName, Content, ContentType, SchemeDetail_SchemeDetailId)
                values ('document-foo-2', 'document-foo-2.txt', 0x, 'text/plain', (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'))");

            VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-bar', '2014-01-01')");

            VersionManager.RunScript(@"
                insert into SchemeDocuments (Name, FileName, Content, ContentType, SchemeDetail_SchemeDetailId)
                values ('document-bar', 'document1-bar.txt', 0x, 'text/plain', (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-bar'))");

            VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-raz', '2014-01-01')");

            VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-qux', '2014-01-01')");

            // ***
            // admin
            // ***
            VersionManager.RunScript(@"
                insert into UserProfile (UserName, FirstName, LastName, EmailAddress, TermsAccepted, ActiveSchemeDetailId)
                values ('admin-test-foo', 'admin', 'foo', 'admin-foo@space01.co.uk', 1, (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'))");

            VersionManager.RunScript(@"
                insert into webpages_UsersInRoles (UserId, RoleId) 
                values ((select UserId from UserProfile where UserName = 'admin-test-foo'), 1)");

            // ***
            // installer
            // ***
            VersionManager.RunScript(@"
                insert into UserProfile (UserName, FirstName, LastName, EmailAddress, TermsAccepted, ActiveSchemeDetailId)
                values ('installer-test-foo', 'installer', 'foo', 'installer-foo@space01.co.uk', 1, (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'))");

            VersionManager.RunScript(@"
                insert into webpages_UsersInRoles (UserId, RoleId) 
                values ((select UserId from UserProfile where UserName = 'installer-test-foo'), 2)");

            VersionManager.RunScript(@"
                insert into UserProfileSchemeDetail (UserId, SchemeDetailId)
                values ((select UserId from UserProfile where UserName = 'installer-test-foo'), (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'))");

            #endregion
        }

        [TearDown]
        public void TearDown()
        {
            var VersionManager = new DbVersionManager();
            var whereUsernameIn = "where username in ('admin-test-foo', 'installer-test-foo')";

            VersionManager.RunScript("delete webpages_UsersInRoles where UserId in (select UserId from UserProfile " + whereUsernameIn + ")");
            VersionManager.RunScript("delete UserProfileSchemeDetail where UserId in (select UserId from UserProfile " + whereUsernameIn + ")");
            VersionManager.RunScript("delete UserProfile " + whereUsernameIn);
            VersionManager.RunScript("delete SchemeDocuments where Name in ('document-foo', 'document-foo-2', 'document-bar')");
            VersionManager.RunScript("delete SchemeDetail where SchemeName in ('scheme-foo', 'scheme-bar', 'scheme-raz')");
        }

        [Test]
        public void GetAllAdmin()
        {
            var svc = new PensionSchemeService(admin, new IndexImportDetailService(false).FinancialIndexRepository);

            var schemes = svc.GetAllSummaries();

            Assert.NotNull(schemes);
            Assert.GreaterOrEqual(schemes.Count(), 3);
            Assert.AreEqual(1, schemes.Count(x => x.SchemeName == "scheme-foo"));
        }


        [Test]
        [Ignore("Replace with specflow test")]
        public void GetAllInstaller()
        {
            var svc = new PensionSchemeService(installer, new IndexImportDetailService(false).FinancialIndexRepository);

            var schemes = svc.GetAllSummaries();

            Assert.NotNull(schemes);
            Assert.AreEqual(1, schemes.Count());
            Assert.AreEqual(1, schemes.Count(x => x.SchemeName == "scheme-foo"));
        }

        [Test]
        public void GetSummmary()
        {
            var svc = new PensionSchemeService(admin, new IndexImportDetailService(true).FinancialIndexRepository);
            var id = getSchemeBarId();

            var scheme = svc.GetSummary(id);

            Assert.NotNull(scheme);
            Assert.AreEqual("scheme-bar", scheme.SchemeName);
            Assert.NotNull(scheme.Documents);
            Assert.AreEqual(1, scheme.Documents.Count());
            Assert.AreEqual("document-bar", scheme.Documents.Single().Name);
        }

        [Test]
        [Ignore("Replace with specflow test")]
        public void GetSummaryAuthorizationFAIL()
        {
            var svc = new PensionSchemeService(installer, new IndexImportDetailService(false).FinancialIndexRepository);
            var id = getSchemeBarId();

            var scheme = svc.GetSummary(id);

            Assert.IsNull(scheme);
        }

        [Test]
        public void GetSummaryIdFAIL()
        {
            var svc = new PensionSchemeService(admin, new IndexImportDetailService(false).FinancialIndexRepository);

            var scheme = svc.GetSummary(99999999);

            Assert.IsNull(scheme);
        }

        [Test]
        public void GetSummaryByName()
        {
            var svc = new PensionSchemeService(admin, new IndexImportDetailService(false).FinancialIndexRepository);

            var scheme = svc.GetSummaryByName("scheme-foo");

            Assert.NotNull(scheme);
            Assert.AreEqual("scheme-foo", scheme.SchemeName);
            Assert.NotNull(scheme.Documents);
            Assert.AreEqual(2, scheme.Documents.Count());
            Assert.AreEqual(1, scheme.Documents.Count(x => x.Name == "document-foo"));
        }

        [Test]
        [Ignore("Replace with specflow test")]
        public void GetSummaryByNameAuthorizationFAIL()
        {
            var svc = new PensionSchemeService(installer, new IndexImportDetailService(false).FinancialIndexRepository);

            var scheme = svc.GetSummaryByName("scheme-bar");

            Assert.Null(scheme);
        }

        [Test]
        public void Delete()
        {
            var svc = new PensionSchemeService(admin, new IndexImportDetailService(false).FinancialIndexRepository);

            // delete by name
            svc.Delete("scheme-foo");

            // delete by id
            var scheme = svc.GetSummaryByName("scheme-qux");
            svc.DeleteById(scheme.SchemeDetailId);

            Assert.IsNullOrEmpty(new DbVersionManager().RunQuery("select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'"));
            Assert.IsNullOrEmpty(new DbVersionManager().RunQuery("select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-qux'"));
        }

        [Test]
        public void DeleteDocument()
        {
            var svc = new PensionSchemeService(admin, new IndexImportDetailService(false).FinancialIndexRepository);
            var scheme = svc.GetSummaryByName("scheme-foo");
            Assert.NotNull(scheme);

            var docs = scheme.Documents;

            var doc = docs.Where(x => x.Name == "document-foo").Single();
            
            svc.DeleteDocument(scheme.SchemeDetailId, doc.SchemeDocumentId);

            Assert.IsNullOrEmpty(new DbVersionManager().RunQuery("select SchemeDocumentId from SchemeDocuments where Name = 'document-foo'"));
        }
    }
}
