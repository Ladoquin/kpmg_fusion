﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Text;
using NUnit.Framework;
using FlightDeck.Domain.UnitTests;
using FlightDeck.DomainShared;
using FlightDeck.ServiceInterfaces;
using FlightDeck.Services;
using System.Configuration;
using FlightDeck.Maintenance;
using Moq;
using System.Security.Principal;

namespace FlightDeck.IntegrationTests
{
    [TestFixture]
    public class DataValidation
    {
        private readonly string testDataPath = @"..\..\..\..\test_data";

        public XDocument testBaseStatsDoc;
        public XDocument testIncrementalDoc;
        List<string> indices;
        IndexImportDetailService _indexService;
        IndexImportDetail _importDetail;

        [TearDown]
        public void TearDown()
        {
            ClearTestIndexData(indices);
        }

        [SetUp]
        public void Setup()
        {
            // get list of indices from test XML file
            indices = GetIndexListFromXML(); // new List<string>() {"Austin Reed post DR", "30% Emerging M. 70% SL DGF"};

            ClearTestIndexData(indices);
            // import test indices into database table - main setup
            _indexService = new IndexImportDetailService(false);
            _indexService.ImportIndices(testBaseStatsDoc);

            //Mock User
            Mock<IPrincipal> mockPrincipal = new Mock<IPrincipal>();
            var mockIdentity = new GenericIdentity("Test user");
            mockPrincipal.Setup(x => x.Identity).Returns(mockIdentity);
            var user = mockPrincipal.Object;

            // Save import details
            _indexService.Save(testBaseStatsDoc, user);

            // import process for incremental test file
            _indexService.ImportIndices(testIncrementalDoc);

            // Save import details
            _importDetail = _indexService.Save(testIncrementalDoc, user);

            // Assert - Index 1 - not updated existing values for 8th sep
            var index1 = _indexService.FinancialIndexRepository.Value.GetByName("TEST INDEX 1");
            Assert.That(index1.GetValue(new DateTime(2014, 9, 8)), Is.EqualTo(1.4));
        }

        public DataValidation()
        {
            DataAccess.DbInteractionScope.Configure(System.Configuration.ConfigurationManager.ConnectionStrings["FlightDeck"].ConnectionString);
        }

        [Test]
        public void FillGapEnd()
        {
            // fill gaps
            _indexService.FillEndGap(_importDetail.IndexImportDetailId);

            // scenario tests
            // Assert - We have missing values for Index 2
            var index2 = _indexService.FinancialIndexRepository.Value.GetByName("TEST INDEX 2");
            Assert.That(index2.GetValue(new DateTime(2014, 9, 8)), Is.EqualTo(2.15));

            // Assert - We have missing values for index 2 (for 8th Sep)
            EndGapTest(index2, new DateTime(2014, 9, 7), new DateTime(2014, 9, 8), 2.15);

            // Assert - We have missing values for index 5 (from 5th till 8th Sep)
            var index5 = _indexService.FinancialIndexRepository.Value.GetByName("TEST INDEX 5");
            EndGapTest(index5, new DateTime(2014, 9, 5), new DateTime(2014, 9, 8), 5.4);

            // Assert - Index 1 - not updated existing values for 8th sep
            var index1 = _indexService.FinancialIndexRepository.Value.GetByName("TEST INDEX 1");
            Assert.That(index1.GetValue(new DateTime(2014, 9, 8)), Is.EqualTo(1.4));
        }

        [Test]
        public void LoadTestStatsFiles()
        {
            testBaseStatsDoc = XDocument.Load(Path.Combine(testDataPath, "test_base_stats.xml"));

            testIncrementalDoc = XDocument.Load(Path.Combine(testDataPath, "test_incremental_stats.xml"));

            Assert.IsTrue(testBaseStatsDoc != null && testIncrementalDoc != null);
        }

        private void EndGapTest(Domain.FinancialIndex index, DateTime FromDate, DateTime ToDate, double testValue)
        {
            Assert.That(index.ContainsValue(FromDate), Is.EqualTo(true));
            
            for(var day = FromDate.Date; day.Date <= ToDate.Date; day = day.AddDays(1))
                Assert.That(index.GetValue(day), Is.EqualTo(testValue));
        }

        private void ClearTestIndexData(List<string> indices)
        {
            // delete import details from database table
            DataImportHelper.DeleteIndexImportDetails("test_base_stats.xml");
            DataImportHelper.DeleteIndexImportDetails("test_incremental_stats.xml");

            // delete test indices from database table
            DataImportHelper.DeleteIndices(indices);
        }


        private List<string> GetIndexListFromXML()
        {
            List<string> indexList = new List<string>();

            // load test stats xml files
            LoadTestStatsFiles();

            // read index names from xml files
            indexList.AddRange(testBaseStatsDoc.Descendants("IndexName")
                .Select(x => x.Value).Distinct().ToList());

            indexList.AddRange(testIncrementalDoc.Descendants("IndexName")
                .Select(x => x.Value).Distinct().ToList());

            // return list
            return indexList.Distinct().ToList();
        }
    }
}
