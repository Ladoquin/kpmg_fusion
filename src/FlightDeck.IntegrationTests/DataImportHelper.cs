﻿namespace FlightDeck.IntegrationTests
{
    using FlightDeck.DomainShared;
    using FlightDeck.Maintenance;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class DataImportHelper
    {
        public static void DeleteIndices(List<string> indices)
        {

            if (indices.Any())
            {
                var VersionManager = new DbVersionManager();

                indices.ForEach(x => x = x.Replace("'", "''"));
                string indexIDs = "'" + string.Join("','", indices.ToArray()) + "'";
                string subQuery = string.Format("SELECT [ID] FROM FinancialIndices WHERE NAME IN ({0})", indexIDs);

                //// Assets
                //VersionManager.RunScript(string.Format(@"DELETE FROM Assets WHERE Financialindexid IN ({0})", subQuery));

                //// Liabilities
                //VersionManager.RunScript(string.Format("DELETE FROM PensionIncreaseAssumptions WHERE FinancialIndexId IN ({0})", subQuery));
                //VersionManager.RunScript(string.Format("DELETE FROM FinancialAssumptions WHERE FinancialIndexId IN ({0})", subQuery));

                // Index values
                VersionManager.RunScript(string.Format("DELETE FROM IndexValues WHERE FinancialIndexId IN ({0})", subQuery));

                // Financial Index
                VersionManager.RunScript(string.Format("DELETE FROM FinancialIndices WHERE [Id] IN ({0})", subQuery));

            }

        }
        
        public static void DeleteIndexImportDetails(string fileName)
        {
            if (! string.IsNullOrWhiteSpace(fileName)) 
            { 
                var VersionManager = new DbVersionManager();
                var subQuery = string.Format("SELECT IndexImportDetailId FROM IndexImportDetail WHERE Spreadsheet = '{0}'", fileName);
                VersionManager.RunScript(string.Format("DELETE FROM IndexImportIndices WHERE IndexImportDetailId IN ({0})", subQuery));
                VersionManager.RunScript(string.Format("DELETE FROM IndexImportDetail WHERE IndexImportDetailId IN ({0})", subQuery));
            }
        }

    }
}
