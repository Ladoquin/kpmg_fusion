﻿namespace FlightDeck.IntegrationTests
{
    using FlightDeck.DomainShared;
    using FlightDeck.Maintenance;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    class UserHelper
    {
        public static void CreateUserInDb(string username, string schemeName, RoleType role, string forename, string surname, string email)
        {
            var VersionManager = new DbVersionManager();

            VersionManager.RunScript(@"
                insert into UserProfile (UserName, FirstName, LastName, EmailAddress, PasswordMustChange, TermsAccepted, ActiveSchemeDetailId)
                values ('" + username + "', '" + forename + "', '" + surname + "', '" + email + "', 0, 1, (select SchemeDetailId from SchemeDetail where SchemeName = '" + schemeName + "'))");

            VersionManager.RunScript(@"
                insert into webpages_UsersInRoles (UserId, RoleId) 
                values ((select UserId from UserProfile where UserName = '" + username + "'), " + ((int)role).ToString() + ")");

            VersionManager.RunScript(@"
                insert into webpages_Membership (UserId, CreateDate, IsConfirmed, PasswordFailuresSinceLastSuccess, Password, PasswordSalt)
                values ((select UserId from UserProfile where UserName = '" + username + "'), '" + DateTime.Now.ToString("yyyy-MM-dd") + "', 1, 0, 'ANE0Ny7d323/i77bFLYPCUyu5gOyy8ZSHqhGVqpmYRFu/VTg2V27ilgqGaWPgEoZ4A==', '')");
        }

        public static void AssosciateUserWithScheme(string username, string schemeName)
        {
            new DbVersionManager().RunScript(@"
                insert into UserProfileSchemeDetail (UserId, SchemeDetailId)
                values ((select UserId from UserProfile where UserName = '" + username + "'), (select SchemeDetailId from SchemeDetail where SchemeName = '" + schemeName + "'))");
        }
    }
}
