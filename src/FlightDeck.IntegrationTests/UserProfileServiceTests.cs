﻿using FlightDeck.DomainShared;
using FlightDeck.Maintenance;
using FlightDeck.ServiceInterfaces;
using FlightDeck.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace FlightDeck.IntegrationTests
{
    [TestFixture]
    public class UserProfileServiceTests
    {
        const string Admin = "admin-test-foo";
        const string Installer = "installer-test-foo";
        const string Client = "client-test-foo-uno";
        const string Client2 = "client-test-foo-dos";
        const string Client3 = "client-test-foo-tres";

        Dictionary<string, int> Ids;

        private List<string> _usersCreated = new List<string>();

        IRoleManager RoleMgr
        {
            get
            {
                var roleMgr = new Mock<IRoleManager>();
                roleMgr.Setup(r => r.GetRole(Admin)).Returns(RoleType.Admin);
                roleMgr.Setup(r => r.GetRole(Installer)).Returns(RoleType.Installer);
                roleMgr.Setup(r => r.GetRole(Client)).Returns(RoleType.Client);
                roleMgr.Setup(r => r.GetRole(Client2)).Returns(RoleType.Client);
                roleMgr.Setup(r => r.GetRole(Client3)).Returns(RoleType.Client);
                roleMgr
                    .Setup(x => x.Delete(It.IsAny<string>()))
                    .Callback((string username) =>
                    {
                        new DbVersionManager().RunScript(@"delete webpages_UsersInRoles where userId = (select UserId from UserProfile where username = '" + username + "')");
                    });
                roleMgr
                    .Setup(x => x.AddUserToRole(It.IsAny<string>(), It.IsAny<string>()))
                    .Callback((string username, string roleName) =>
                    {
                        var roleId = (int)(RoleType)Enum.Parse(typeof(RoleType), roleName);
                        new DbVersionManager().RunScript(@"insert webpages_UsersInRoles (UserId, RoleId) values ((select UserId from UserProfile where username = '" + username + "'), " + roleId.ToString() + ")");
                    });
                return roleMgr.Object;
            }
        }

        IPrincipal AdminPrincipal
        {
            get
            {
                var principal = new Mock<IPrincipal>();
                principal.Setup(p => p.Identity.Name).Returns(Admin);
                return principal.Object;
            }
        }

        [SetUp]
        public void SetUp()
        {
            DataAccess.DbInteractionScope.Configure(System.Configuration.ConfigurationManager.ConnectionStrings["FlightDeck"].ConnectionString);

            var VersionManager = new DbVersionManager();

            #region sql setup 

            VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-foo', '2014-01-01')");

            VersionManager.RunScript(@"
                insert into SchemeDocuments (Name, FileName, Content, ContentType, SchemeDetail_SchemeDetailId)
                values ('document-foo', 'document-foo.txt', 0x, 'text/plain', (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'))");

            VersionManager.RunScript(@"
                insert into SchemeDocuments (Name, FileName, Content, ContentType, SchemeDetail_SchemeDetailId)
                values ('document-foo-2', 'document-foo-2.txt', 0x, 'text/plain', (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-foo'))");

            VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-bar', '2014-01-01')");

            VersionManager.RunScript(@"
                insert into SchemeDocuments (Name, FileName, Content, ContentType, SchemeDetail_SchemeDetailId)
                values ('document-bar', 'document1-bar.txt', 0x, 'text/plain', (select SchemeDetailId from SchemeDetail where SchemeName = 'scheme-bar'))");

            VersionManager.RunScript(@"
                insert into SchemeDetail (SchemeName, UpdatedOnDate)
                values ('scheme-raz', '2014-01-01')");

            UserHelper.CreateUserInDb(Admin, "scheme-foo", RoleType.Admin, "admin", "00000", "admin-foo@space01.co.uk");

            UserHelper.CreateUserInDb(Installer, "scheme-foo", RoleType.Installer, "installer", "00000", "installer-foo@space01.co.uk");
            UserHelper.AssosciateUserWithScheme(Installer, "scheme-foo");
            UserHelper.AssosciateUserWithScheme(Installer, "scheme-bar");

            UserHelper.CreateUserInDb(Client, "scheme-foo", RoleType.Client, "client", "00000", "client-foo@space01.co.uk");
            UserHelper.AssosciateUserWithScheme(Client, "scheme-foo");

            UserHelper.CreateUserInDb(Client2, "scheme-bar", RoleType.Client, "client2", "00000", "client2-foo@space01.co.uk");
            UserHelper.AssosciateUserWithScheme(Client2, "scheme-bar");

            UserHelper.CreateUserInDb(Client3, "scheme-raz", RoleType.Client, "client3", "zzzzz", "client3-foo@space01.co.uk");
            UserHelper.AssosciateUserWithScheme(Client3, "scheme-raz");

            #endregion

            Ids = new Dictionary<string, int>(5);
            Ids.Add(Admin, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Admin + "'")));
            Ids.Add(Installer, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Installer + "'")));
            Ids.Add(Client, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Client + "'")));
            Ids.Add(Client2, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Client2 + "'")));
            Ids.Add(Client3, int.Parse(VersionManager.RunQuery("select UserId from UserProfile where Username = '" + Client3 + "'")));
        }

        [TearDown]
        public void TearDown()
        {
            var VersionManager = new DbVersionManager();
            var whereUsernameInBuilder = new StringBuilder(100);
            whereUsernameInBuilder.AppendFormat("where username in ('{0}', '{1}', '{2}', '{3}', '{4}'", Admin, Installer, Client, Client2, Client3);
            _usersCreated.ForEach(x => whereUsernameInBuilder.AppendFormat(", '{0}'", x));
            whereUsernameInBuilder.Append(")");
            var whereUsernameIn = whereUsernameInBuilder.ToString();

            VersionManager.RunScript(@"delete webpages_Membership where UserId in (select UserId from UserProfile " + whereUsernameIn + ")");
            VersionManager.RunScript(@"delete webpages_UsersInRoles where UserId in (select UserId from UserProfile " + whereUsernameIn + ")");
            VersionManager.RunScript(@"delete UserProfileSchemeDetail where UserId in (select UserId from UserProfile " + whereUsernameIn + ")");
            VersionManager.RunScript(@"delete UserProfile " + whereUsernameIn);
            VersionManager.RunScript(@"delete SchemeDocuments where Name in ('document-foo', 'document-foo-2', 'document-bar')");
            VersionManager.RunScript(@"delete SchemeDetail where SchemeName in ('scheme-foo', 'scheme-bar', 'scheme-raz')");
        }

        [Test]
        public void GetAdminById()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var result = svc.GetById(Ids[Admin]);

            Assert.NotNull(result);
            Assert.NotNull(result.ActiveSchemeDetail);
            Assert.AreEqual("scheme-foo", result.ActiveSchemeDetail.SchemeName);
            Assert.NotNull(result.ActiveSchemeDetail.Documents);
            Assert.AreEqual(2, result.ActiveSchemeDetail.Documents.Count());
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo"));
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo-2"));
            Assert.True(result.SchemeDetails.IsNullOrEmpty());
            Assert.AreEqual(Admin, result.UserName);
            Assert.AreEqual(RoleType.Admin, result.Role);
        }

        [Test]
        public void GetAdminByUsername()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var result = svc.GetByUsername(Admin);

            Assert.NotNull(result);
            Assert.NotNull(result.ActiveSchemeDetail);
            Assert.AreEqual("scheme-foo", result.ActiveSchemeDetail.SchemeName);
            Assert.NotNull(result.ActiveSchemeDetail.Documents);
            Assert.AreEqual(2, result.ActiveSchemeDetail.Documents.Count());
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo"));
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo-2"));
            Assert.True(result.SchemeDetails.IsNullOrEmpty());
            Assert.AreEqual(Admin, result.UserName);
            Assert.AreEqual(RoleType.Admin, result.Role);
        }

        [Test]
        public void GetInstallerByUsername()
        {
            var scheme = "scheme-foo";
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var result = svc.GetByUsername(Installer);

            Assert.NotNull(result);
            Assert.NotNull(result.ActiveSchemeDetail);
            Assert.NotNull(result.SchemeDetails);
            Assert.AreEqual(Installer, result.UserName);
            Assert.AreEqual(RoleType.Installer, result.Role);
            Assert.AreEqual(scheme, result.ActiveSchemeDetail.SchemeName);
            Assert.NotNull(result.ActiveSchemeDetail.Documents);
            Assert.AreEqual(2, result.ActiveSchemeDetail.Documents.Count());
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo"));
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo-2"));
            Assert.AreEqual(2, result.SchemeDetails.Count());
            Assert.True(result.SchemeDetails.Any(x => scheme == x.SchemeName));
            Assert.True(result.SchemeDetails.Any(x => "scheme-bar" == x.SchemeName));
        }

        [Test]
        public void GetClientByUsername()
        {
            var scheme = "scheme-foo";
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var result = svc.GetByUsername(Client);

            Assert.NotNull(result);
            Assert.NotNull(result.ActiveSchemeDetail);
            Assert.NotNull(result.SchemeDetails);
            Assert.AreEqual(Client, result.UserName);
            Assert.AreEqual(RoleType.Client, result.Role);
            Assert.AreEqual(scheme, result.ActiveSchemeDetail.SchemeName);
            Assert.NotNull(result.ActiveSchemeDetail.Documents);
            Assert.AreEqual(2, result.ActiveSchemeDetail.Documents.Count());
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo"));
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo-2"));
            Assert.AreEqual(1, result.SchemeDetails.Count());
            Assert.AreEqual(scheme, result.SchemeDetails.First().SchemeName);
        }

        [Test]
        public void GetByEmail()
        {
            var scheme = "scheme-foo";
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var result = svc.GetByEmail("client-foo@space01.co.uk");

            Assert.That(result != null);
            Assert.That(result.UserName == Client);
            Assert.AreEqual(result.Role, RoleType.Client);
            Assert.That(result.ActiveSchemeDetail != null);
            Assert.That(result.ActiveSchemeDetail.SchemeName == scheme);
            Assert.NotNull(result.ActiveSchemeDetail.Documents);
            Assert.AreEqual(2, result.ActiveSchemeDetail.Documents.Count());
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo"));
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo-2"));
        }

        [Test]
        public void GetByEmailAnonymous()
        {
            var principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns(string.Empty);
            var svc = new UserProfileService(principal.Object, RoleMgr);

            var result = svc.GetByEmail("admin-foo@space01.co.uk", true);

            Assert.NotNull(result);
            Assert.NotNull(result.ActiveSchemeDetail);
            Assert.AreEqual("scheme-foo", result.ActiveSchemeDetail.SchemeName);
            Assert.NotNull(result.ActiveSchemeDetail.Documents);
            Assert.AreEqual(2, result.ActiveSchemeDetail.Documents.Count());
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo"));
            Assert.True(result.ActiveSchemeDetail.Documents.Any(x => x.Name == "document-foo-2"));
            Assert.True(result.SchemeDetails.IsNullOrEmpty());
            Assert.AreEqual(Admin, result.UserName);
            Assert.AreEqual(RoleType.Admin, result.Role);
        }

        [Test]
        public void GetByEmailAnonymousFAIL()
        {
            var principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns(string.Empty);
            var svc = new UserProfileService(principal.Object, RoleMgr);

            var result = svc.GetByEmail("admin-foo@space01.co.uk", false);

            Assert.Null(result);
        }

        [Test]
        public void ClientSchemePropertyPermisions()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var user = svc.GetByUsername(Client);

            Assert.That(user != null);
            Assert.That(user.SchemeDetails != null);
            Assert.AreEqual(1, user.SchemeDetails.Count());
            Assert.AreEqual(user.SchemeDetails.Single().SchemeName, "scheme-foo");
        }

        [Test]
        public void InstallerSchemePermissions()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var user = svc.GetByUsername(Installer);

            Assert.NotNull(user);
            Assert.NotNull(user.SchemeDetails);
            Assert.AreEqual(2, user.SchemeDetails.Count());
            Assert.True(user.SchemeDetails.Any(x => "scheme-foo" == x.SchemeName));
            Assert.True(user.SchemeDetails.Any(x => "scheme-bar" == x.SchemeName));
        }

        [Test]
        [Ignore("Replace with specflow test")]
        public void GetPaged()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var resultsSet1 = svc.Get(new UserProfileSearchCriteria(), 0, 4).UserPageSet;
            var resultsSet2 = svc.Get(new UserProfileSearchCriteria(), 0, 2).UserPageSet;
            var resultsSet3 = svc.Get(new UserProfileSearchCriteria(), 1, 2).UserPageSet;
            var resultsSet4 = svc.Get(new UserProfileSearchCriteria { NameDescending = true }, 0, 20).UserPageSet;

            Assert.NotNull(resultsSet1);
            Assert.NotNull(resultsSet2);
            Assert.NotNull(resultsSet3);
            Assert.NotNull(resultsSet4);
            // set 1
            Assert.AreEqual(4, resultsSet1.Count());
            Assert.AreEqual(4, resultsSet1.Select(r => r.UserName).Distinct().Count());
            Assert.True(resultsSet1.All(r => r.LastName.Substring(0, 5) == "00000"));            
            // set 2 & 3
            Assert.AreEqual(2, resultsSet2.Count());
            Assert.AreEqual(2, resultsSet3.Count());
            Assert.AreEqual(4, resultsSet2.Select(x => x.UserId).Union(resultsSet3.Select(x => x.UserId)).Distinct().Count());
            Assert.True(resultsSet1.Skip(1).Take(1).Single().UserId == resultsSet2.Last().UserId);
            Assert.True(resultsSet1.Skip(2).Take(1).Single().UserId == resultsSet3.First().UserId);
            // set 4
            Assert.IsTrue(resultsSet4.Any());
            Assert.IsTrue(resultsSet4.Any(x => x.UserName == Client3));
        }

        [Test]
        public void GetWithFilter()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var resultsSet1 = svc.Get(new UserProfileSearchCriteria { RoleFilter = RoleType.Installer }, 0, 25).UserPageSet;
            var resultsSet2 = svc.Get(new UserProfileSearchCriteria { RoleFilter = RoleType.Client, NameDescending = true }, 0, 10).UserPageSet;
            var resultsSet3 = svc.Get(new UserProfileSearchCriteria { RoleFilter = RoleType.Client, SchemeFilter = "scheme-foo" }, 0, 10).UserPageSet;

            Assert.NotNull(resultsSet1);
            Assert.NotNull(resultsSet2);
            Assert.NotNull(resultsSet3);
            Assert.IsTrue(resultsSet1.All(x => x.Role == RoleType.Installer));
            Assert.IsTrue(resultsSet2.All(x => x.Role == RoleType.Client));
            Assert.IsTrue(resultsSet2.Any(x => x.LastName == "zzzzz"));
            Assert.IsTrue(resultsSet3.All(x => x.SchemeDetails.All(sd => sd.SchemeName == "scheme-foo")));
            Assert.AreEqual(1, resultsSet3.Count(x => x.UserName == Client));
            Assert.IsFalse(resultsSet3.Any(x => x.UserName == Installer));
            Assert.IsFalse(resultsSet3.Any(x => x.UserName == Client2));
            Assert.IsFalse(resultsSet3.Any(x => x.UserName == Client3));
        }

        [Test]
        public void AdminUserPermissions()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var users = svc.Get(new UserProfileSearchCriteria(), 0, 25).UserPageSet;

            Assert.That(!users.IsNullOrEmpty());
            Assert.That(users.Any(u => u.UserName == Installer));
            Assert.That(users.Any(u => u.UserName == Client));
            Assert.That(users.Any(u => u.UserName == Client2));
        }

        [Test]
        [Ignore("Replace with specflow test")]
        public void InstallerUserPermissionsOK()
        {
            var principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns(Installer);
            var svc = new UserProfileService(principal.Object, RoleMgr);

            var users = svc.Get(new UserProfileSearchCriteria(), 0, 25).UserPageSet;

            Assert.That(!users.IsNullOrEmpty());
            Assert.AreEqual(3, users.Count());
            Assert.True(users.Any(x => x.UserName == Installer));
            Assert.True(users.Any(x => x.UserName == Client));
            Assert.True(users.Any(x => x.UserName == Client2));
        }

        [Test]
        [Ignore("Replace with specflow test")]
        public void InstallerUserPermissionsFAIL()
        {
            var principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns(Installer);
            var svc = new UserProfileService(principal.Object, RoleMgr);

            var user = svc.GetById(Ids[Client3]);

            Assert.IsNull(user);
        }

        [Test]
        [Ignore("Replace with specflow test")]
        public void ClientUserPermissions()
        {
            var principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns(Client);
            var svc = new UserProfileService(principal.Object, RoleMgr);

            var users = svc.Get(new UserProfileSearchCriteria(), 0, 25).UserPageSet;

            Assert.False(users.IsNullOrEmpty());
            Assert.AreEqual(1, users.Count());
            Assert.AreEqual(Client, users.Single().UserName);
        }
        
        [Test]
        public void Delete()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);
            var admin = svc.GetByUsername(Admin);            

            svc.DeleteUser(Client);

            Assert.False(svc.Get(new UserProfileSearchCriteria(), 0, 25).UserPageSet.Any(x => x.UserName == Client));
        }

        [Test]
        public void ChangeActiveScheme()
        {
            var principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns(Installer);
            var svc = new UserProfileService(principal.Object, RoleMgr);

            var result = svc.ChangeActiveScheme(Installer, "scheme-bar");
            var installer = svc.GetByUsername(Installer);

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.NotNull(installer);
            Assert.NotNull(installer.ActiveSchemeDetail);
            Assert.AreEqual("scheme-bar", installer.ActiveSchemeDetail.SchemeName);
        }

        [Test]
        public void Create()
        {
            var username = "darth-helmet";
            _usersCreated.Add(username);
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var result = svc.Create(username, RoleType.Client, new string[] { "scheme-foo", "scheme-bar" }, "dude", "smith", "dudesmith@space01.co.uk");
            var user = svc.GetByUsername(username);
            var allUsers = svc.Get(new UserProfileSearchCriteria(), 0, 25).UserPageSet;

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.NotNull(user);
            Assert.False(allUsers.IsNullOrEmpty());
            Assert.AreEqual(username, user.UserName);
            Assert.AreEqual(RoleType.Client, user.Role);
            Assert.NotNull(user.ActiveSchemeDetail);
            Assert.NotNull(user.SchemeDetails);
            Assert.True(new string[] { "scheme-foo", "scheme-bar" }.Contains(user.ActiveSchemeDetail.SchemeName));
            Assert.AreEqual(2, user.SchemeDetails.Count());
        }

        [Test]
        public void UpdateMix1()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var result = svc.Update(Client, role: RoleType.Installer, firstname: "dudley", schemeNames: new string[] { "scheme-foo" });
            var user = svc.GetByUsername(Client);

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.NotNull(user);
            Assert.AreEqual(RoleType.Installer, user.Role);
            Assert.AreEqual("dudley", user.FirstName);
            Assert.NotNull(user.ActiveSchemeDetail);
            Assert.AreEqual("scheme-foo", user.ActiveSchemeDetail.SchemeName);
            Assert.False(user.SchemeDetails.IsNullOrEmpty());
            Assert.AreEqual(1, user.SchemeDetails.Count());
        }

        [Test]
        [Ignore("Replace with specflow test")]
        public void UpdateMix2()
        {
            var svc = new UserProfileService(AdminPrincipal, RoleMgr);

            var result = svc.Update(Client, role: RoleType.Installer, email: "darthhelmet@spaceballs1.co.uk", schemeNames: new string[] { "scheme-bar" });
            var user = svc.GetByUsername(Client);

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.NotNull(user);
            Assert.AreEqual(RoleType.Installer, user.Role);
            Assert.AreEqual("darthhelmet@spaceballs1.co.uk", user.EmailAddress);
            Assert.Null(user.ActiveSchemeDetail);
            Assert.False(user.SchemeDetails.IsNullOrEmpty());
            Assert.AreEqual(1, user.SchemeDetails.Count());
            Assert.AreEqual("scheme-bar", user.SchemeDetails.Single().SchemeName);
        }

        [Test]
        public void UpdateUsername()
        {
            var username = "darth-helmet";
            _usersCreated.Add(username);
            var principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns(Client);
            var svc = new UserProfileService(principal.Object, RoleMgr);

            var result = svc.Update(Client, updatedusername: username);
            principal = new Mock<IPrincipal>();
            principal.Setup(p => p.Identity.Name).Returns("darth-helmet");
            var user = new UserProfileService(principal.Object, RoleMgr).GetByUsername(username);

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.NotNull(user);
            Assert.AreEqual(RoleType.Client, user.Role);
            Assert.NotNull(user.ActiveSchemeDetail);
            Assert.AreEqual("scheme-foo", user.ActiveSchemeDetail.SchemeName);
            Assert.False(user.SchemeDetails.IsNullOrEmpty());
            Assert.AreEqual(1, user.SchemeDetails.Count());
            Assert.AreEqual("scheme-foo", user.SchemeDetails.Single().SchemeName);
        }
    }
}
