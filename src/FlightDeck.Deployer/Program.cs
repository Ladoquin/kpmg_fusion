﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Xml;

namespace fusion_deployer
{
    class Program
    {
        private static string PropertiesFilePath = @"src\Fusion.WebApp\Properties\AssemblyInfo.cs";
        private static string _jiraUrl = "https://space02.atlassian.net/rest/api/latest";
        private static string _credentials = "Jenkins:<Gr@vitee!";
        private static string _nextReleaseLabel = "NextRelease";

        private static string PackagesFolder { get { return Args.ContainsKey(ArgType.PackagesFolder) ? Args[ArgType.PackagesFolder] : ConfigurationManager.AppSettings["packages"]; } }
        private static string SourceFolder { get { return Args.ContainsKey(ArgType.SourceFolder) ? Args[ArgType.SourceFolder] : Path.Combine(PackagesFolder, "latest"); } }
        private static string RepoFolder { get { return Args.ContainsKey(ArgType.RepoFolder) ? Args[ArgType.RepoFolder] : ConfigurationManager.AppSettings["repo"]; } }
        private static string TargetFolder { get { return Args.ContainsKey(ArgType.TargetFolder) ? Args[ArgType.TargetFolder] : Path.Combine(PackagesFolder, "Fusion_v" + GetVersion().ToString()); } }
        private static string WebsitesFolder { get { return Args.ContainsKey(ArgType.WebsitesFolder) ? Args[ArgType.WebsitesFolder] : ConfigurationManager.AppSettings["websites"]; } }
        private static string FixVersion { get { return Args[ArgType.FixVersion]; } }

        private enum ArgType { PackagesFolder, SourceFolder, TargetFolder, WebsitesFolder, RepoFolder, Version, IISConfigFile, FixVersion }

        private static Dictionary<ArgType, string> Args = new Dictionary<ArgType, string>();

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                ReadArgs(args);

                if (argsContain(args, "tag"))
                {
                    TagBuild();
                }

                if (argsContain(args, "v"))
                {
                    Console.WriteLine(GetVersion().ToString());
                }

                if (argsContain(args, "v+1"))
                {
                    Console.WriteLine(IncrementVersion(GetVersion()).ToString());
                }

                if (argsContain(args, "db"))
                {
                    RunDbScripts();
                }

                if (argsContain(args, "package"))
                {
                    PackageLatest();
                }

                if (argsContain(args, "publish"))
                {
                    Publish();
                }

                if (argsContain(args, "iisconfig"))
                {
                    UpdateIIS();
                }

                if (argsContain(args, "jira"))
                {
                    UpdateJira();
                }

                if (argsContain(args, "test"))
                    Test();
            }
        }

        private static void ReadArgs(string[] args)
        {
            foreach (var arg in args)
            {
                if (arg.Trim().ToUpper().StartsWith("/PF:"))
                    Args.Add(ArgType.PackagesFolder, arg.Substring(4));
                if (arg.Trim().ToUpper().StartsWith("/SF:"))
                    Args.Add(ArgType.SourceFolder, arg.Substring(4));
                if (arg.Trim().ToUpper().StartsWith("/TF:"))
                    Args.Add(ArgType.TargetFolder, arg.Substring(4));
                if (arg.Trim().ToUpper().StartsWith("/V:"))
                    Args.Add(ArgType.Version, arg.Substring(3));
                if (arg.Trim().ToUpper().StartsWith("/WF:"))
                    Args.Add(ArgType.WebsitesFolder, arg.Substring(4));
                if (arg.Trim().ToUpper().StartsWith("/RF:"))
                    Args.Add(ArgType.RepoFolder, arg.Substring(4));
                if (arg.Trim().ToUpper().StartsWith("/IIS:"))
                    Args.Add(ArgType.IISConfigFile, arg.Substring(5));
                if (arg.Trim().ToUpper().StartsWith("/FV:"))
                    Args.Add(ArgType.FixVersion, arg.Substring(4));
            }
        }

        private static void TagBuild()
        {
            var lastTag = GetVersion();

            var targetVersion = IncrementVersion(new Version(lastTag.Major, lastTag.Minor, Math.Max(lastTag.Build, 0), Math.Max(lastTag.Revision, 0)));

            updateAppVersion(targetVersion);

            //commitAndPushAppVersion();

            //createAndPushTag(targetVersion);
        }

        private static Version IncrementVersion(Version v)
        {
            return new Version(v.Major, v.Minor, Math.Max(v.Build, 0), Math.Max(v.Revision, 0) + 1);
        }

        private static void RunDbScripts()
        {
            // assumes FusionDeploymentUtility has been built to packages/latest/FusionDeploymentUtility folder

            var configNew = Path.Combine(RepoFolder, "ci", "FusionDeploymentUtility.exe.config");
            var configOriginal = Path.Combine(SourceFolder, "FusionDeploymentUtility", "FusionDeploymentUtility.exe.config");

            File.Copy(configOriginal, configOriginal + ".bak");
            File.Copy(configNew, configOriginal, true);

            var p = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = Path.Combine(SourceFolder, "FusionDeploymentUtility", "FusionDeploymentUtility.exe"),
                    Arguments = "-silent",
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };

            p.Start();
            p.WaitForExit();

            File.Delete(configOriginal);
            File.Move(configOriginal + ".bak", configOriginal);
        }

        private static void PackageLatest()
        {
            var version = GetVersion();

            var workingTarget = Path.Combine(SourceFolder, string.Format("Fusion_v{0}", version.ToString()));

            if (Directory.Exists(workingTarget))
                Directory.Delete(workingTarget, true);
            Directory.CreateDirectory(workingTarget);

            zipAndCopyDirToTarget(Path.Combine(SourceFolder, "FusionWeb"), workingTarget);
            zipAndCopyDirToTarget(Path.Combine(SourceFolder, "FusionDeploymentUtility"), workingTarget);

            zipAndCopyDirToTarget(workingTarget, PackagesFolder);

            Directory.Delete(workingTarget, true);
        }

        private static void Publish()
        {
            var version = GetVersion();

            var source = Path.Combine(SourceFolder, "FusionWeb");
            var target = Path.Combine(WebsitesFolder, string.Format("v{0}", version.ToString()));

            copyDir(source, target);
        }

        private static void zipAndCopyDirToTarget(string dir, string destinationFolder)
        {
            if (!Directory.Exists(destinationFolder))
                Directory.CreateDirectory(destinationFolder);

            var name = dir.Split('\\').Last() + ".zip";

            ZipFile.CreateFromDirectory(dir, Path.Combine(destinationFolder, name));
        }

        private static bool argsContain(string[] args, string val)
        {
            return args.Any(x => string.Equals(x.Replace("/", "").Replace(" ", ""), val, StringComparison.InvariantCultureIgnoreCase));
        }

        private static Version GetVersion()
        {
            if (Args.ContainsKey(ArgType.Version))
                return new Version(Args[ArgType.Version].Replace("v", ""));

            var tag = git("describe --abbrev=0 --tags");

            var t = tag.ReadLine();

            return new Version(t.Replace("v", ""));
        }

        private static void updateAppVersion(Version v)
        {
            var path = Path.Combine(RepoFolder, PropertiesFilePath);
            var content = File.ReadAllText(path);
            var start = content.IndexOf("AssemblyVersion(\"") + "AssemblyVersion(\"".Length;
            var end = content.IndexOf("\"", start);
            var editted = content.Substring(0, start) + v.ToString() + content.Substring(end);
            File.WriteAllText(path, editted);
        }

        //private static void commitAndPushAppVersion()
        //{
        //    git("add " + PropertiesFilePath.Replace("\\", "/"));
        //    git("commit -m \"Update AssemblyVersion\"");
        //    git("push origin master");
        //}

        //private static void createAndPushTag(Version v)
        //{
        //    git(string.Format("tag -a v{0} -m \"v{0} deployment\"", v.ToString()));
        //    git("push origin v" + v.ToString());
        //}

        private static void Test()
        {
            try
            {
                var v = GetVersion().ToString();

                Console.WriteLine(v);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //private static void ReportGitCommits()
        //{
        //    var rdr = git("for-each-ref refs/tags --sort=-taggerdate --format='%(refname)' --count=2");

        //    var newest = rdr.ReadLine().Split('/').Last();
        //    var prev = rdr.ReadLine().Split('/').Last();

        //    rdr = git(string.Format("log --pretty=format:%s {0}..{1}", prev, newest));


        //}

        private static StreamReader git(string command)
        {
            var p = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = ConfigurationManager.AppSettings["exe"],
                    Arguments = command,
                    WorkingDirectory = RepoFolder,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                }
            };

            try
            {
                p.Start();
                p.WaitForExit();
                return p.StandardOutput;
            }
            catch (Exception ex)
            {
                var standardErr = string.Empty;
                try
                {
                    standardErr = p.StandardError.ReadToEnd();
                }
                catch
                {
                    standardErr = "Standard Error has not been redirected";
                }
                throw new Exception(string.Format("Command '{0}' failed: {1}. Exception: {2}", command, standardErr, ex.ToString()));
            }
        }

        private static void copyDir(string sourceDirName, string destDirName)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                copyDir(subdir.FullName, temppath);
            }
        }

        private static void UpdateIIS()
        {
            var config = new XmlDocument();

            config.Load(Args[ArgType.IISConfigFile]);

            var vd = config.DocumentElement.SelectSingleNode("system.applicationHost/sites/site[@name='Fusion']/application/virtualDirectory");

            vd.Attributes["physicalPath"].Value = Path.Combine(Args[ArgType.WebsitesFolder], "v" + GetVersion().ToString());

            config.Save(Args[ArgType.IISConfigFile]);
        }

        private static void UpdateJira()
        {
            var client = new HttpClient();
            byte[] cred = UTF8Encoding.UTF8.GetBytes(_credentials);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(cred));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var version = GetVersion();

            var query = _jiraUrl + string.Format("/search?jql=project%20%3D%20{0}%20AND%20fixVersion%20%3D%20{1}%20AND%20labels%20%3D%20{2}&maxResults=999", "FUSN", FixVersion, _nextReleaseLabel);
            var response = client.GetStringAsync(query).Result;
            dynamic d = JsonConvert.DeserializeObject(response);

            for (var i = 0; i < d.total.Value; i++)
            {
                var key = d.issues[i].key.Value;
                var data = "{ \"update\": { \"labels\": [ {\"remove\": \"" + _nextReleaseLabel + "\"}, {\"add\": \"v" + version.ToString() + "\"} ] } }";
                var content = new StringContent(data.ToString(), Encoding.UTF8, "application/json");
                var result = client.PutAsync(_jiraUrl + "/issue/" + key, content).Result;
            }
        }
    }
}
